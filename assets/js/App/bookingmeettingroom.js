(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define('/App/Calendar', ['exports', 'Site', 'Config'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('Site'), require('Config'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.Site, global.Config);
    global.AppCalendar = mod.exports;
  }
})(this, function (exports, _Site2, _Config) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.getInstance = exports.run = exports.AppCalendar = undefined;

  var _Site3 = babelHelpers.interopRequireDefault(_Site2);

  var Config = babelHelpers.interopRequireWildcard(_Config);

  var AppCalendar = function (_Site) {
    babelHelpers.inherits(AppCalendar, _Site);

    function AppCalendar() {
      babelHelpers.classCallCheck(this, AppCalendar);
      return babelHelpers.possibleConstructorReturn(this, (AppCalendar.__proto__ || Object.getPrototypeOf(AppCalendar)).apply(this, arguments));
    }

    babelHelpers.createClass(AppCalendar, [{
      key: 'processed',
      value: function processed() {
        babelHelpers.get(AppCalendar.prototype.__proto__ || Object.getPrototypeOf(AppCalendar.prototype), 'processed', this).call(this);

        this.$actionToggleBtn = $('.site-action-toggle');
        this.$addNewCalendarForm = $('#addNewCalendar').modal({
          show: false
        });

        this.handleFullcalendar();
        this.handleListItem();
        this.handleEventList();
      }
    }, {
      key: 'handleFullcalendar',
      value: function handleFullcalendar() {
        var myEvents = [{
          title: 'ส่ง รศ.ดร.ใจดี ซื่อตรง ที่สนามบิน',
          start: '2016-10-01T10:30:00',
          backgroundColor: '#0033cc',
          borderColor: '#0033cc'
        }, {
          title: 'อบรม สัมมนา ปราจีนบุรี',
          start: '2016-10-01T07:30:00',
          end: '2016-10-04T16:30:00',
          backgroundColor: '#990099',
          borderColor: '#990099'
        }, {
          title: 'ศึกษาดูงาน จ.นครนายก',
          start: '2016-10-03T08:00:00',
          end: '2016-10-04T16:30:00',
          backgroundColor: '#00cc66',
          borderColor: '#00cc66'
        }, {
          title: 'ประชุมที่ศูนย์ราชการนนทบุรี',
          start: '2016-10-03T07:30:00',
          backgroundColor: '#0066ff',
          borderColor: '#0066ff'
        }, {
          title: 'รับ-ส่ง คณะที่ปรึกษาโครงการ',
          start: '2016-10-03T09:00:00',
          end: '2016-10-03T16:30:00',
          backgroundColor: '#669900',
          borderColor: '#669900'
        }, {
          title: 'รับ-ส่ง ผอ.มารวย ใจกล้า',
          start: '2016-10-03T09:00:00',
          end: '2016-10-07T16:30:00',
          backgroundColor: '#990099',
          borderColor: '#990099'
        }, {
          title: 'รับ-ส่ง ผอ.มารวย ใจกล้า',
          start: '2016-10-17T09:00:00',
          end: '2016-10-19T16:30:00',
          backgroundColor: '#990099',
          borderColor: '#990099'
        }, {
          title: 'รับ คณะกรรมการ ที่สนามบิน',
          start: '2016-10-10T09:00:00',
          end: '2016-10-10T11:30:00',
          backgroundColor: '#336600',
          borderColor: '#336600'
        }, {
          title: 'ส่ง คณะกรรมการ ที่สนามบิน',
          start: '2016-10-14T14:00:00',
          end: '2016-10-14T16:30:00',
          backgroundColor: '#336600',
          borderColor: '#336600'
        }, {
          title: 'ประชุมที่ศูนย์ราชการนนทบุรี',
          start: '2016-10-13T10:00:00',
          end: '2016-10-13T14:30:00',
          backgroundColor: '#33cc33',
          borderColor: '#33cc33'
        }, {
          title: 'รับ-ส่ง คณะกรรมการ',
          start: '2016-10-24T08:00:00',
          end: '2016-10-28T15:30:00',
          backgroundColor: '#00cc66',
          borderColor: '#00cc66'
        }, {
          title: 'ศึกษาดูงาน จ.ชัยภูมิ',
          start: '2016-10-20T08:00:00',
          end: '2016-10-26T15:30:00',
          backgroundColor: '#cc6600',
          borderColor: '#cc6600'
        }, {
          title: 'ประชุมที่ศูนย์ราชการนนทบุรี',
          start: '2016-10-11T08:00:00',
          end: '2016-10-11T15:30:00',
          backgroundColor: '#0066ff',
          borderColor: '#0066ff'
        }, {
          title: 'รับ-ส่ง ผอ.มารวย ใจกล้า',
          start: '2016-10-12T08:00:00',
          end: '2016-10-13T15:30:00',
          backgroundColor: '#0066ff',
          borderColor: '#0066ff'
        }, {
          title: 'รับ-ส่ง ผอ.มารวย ใจกล้า',
          start: '2016-10-18T08:00:00',
          end: '2016-10-19T15:30:00',
          backgroundColor: '#0066ff',
          borderColor: '#0066ff'
        }, {
          title: 'รับ-ส่ง ผอ.มารวย ใจกล้า',
          start: '2016-10-28T08:00:00',
          end: '2016-10-28T15:30:00',
          backgroundColor: '#0066ff',
          borderColor: '#0066ff'
        }];

        var myOptions = {
          header: {
            left: null,
            center: 'prev,title,next',
            right: 'month,agendaWeek,agendaDay,listYear'
          },
          defaultDate: '2016-10-12',
          selectable: true,
          selectHelper: true,
          select: function select() {
            $.slidePanel.hide();
          },

          editable: true,
          eventLimit: true,
          windowResize: function windowResize(view) {
            var width = $(window).outerWidth();
            var options = Object.assign({}, myOptions);
            options.events = view.calendar.getEventCache();
            options.aspectRatio = width < 667 ? 0.5 : 1.35;

            $('#calendar').fullCalendar('destroy');
            $('#calendar').fullCalendar(options);
          },
          eventClick: function eventClick(event) {
            $('#booking-detail').modal('show');
          },
          eventDragStart: function eventDragStart() {
            $('.site-action').data('actionBtn').show();
          },
          eventDragStop: function eventDragStop() {
            $('.site-action').data('actionBtn').hide();
          },

          events: myEvents,
          timeFormat: 'H:mm',
          droppable: true
        };

        var _options = void 0;
        var myOptionsMobile = Object.assign({}, myOptions);

        myOptionsMobile.aspectRatio = 0.5;
        _options = $(window).outerWidth() < 667 ? myOptionsMobile : myOptions;

        $('#calendar').fullCalendar(_options);
      }
    }, {
      key: 'handleEventList',
      value: function handleEventList() {

        $('.calendar-list .calendar-event').each(function () {
          var $this = $(this),
              color = $this.data('color').split('-');
          $this.data('event', {
            title: $this.data('title'),
            stick: $this.data('stick'),
            backgroundColor: Config.colors(color[0], color[1]),
            borderColor: Config.colors(color[0], color[1])
          });
          $this.draggable({
            zIndex: 999,
            revert: true,
            revertDuration: 0,
            appendTo: '.page',
            helper: function helper() {
              return '<a class="fc-day-grid-event fc-event fc-start fc-end" style="background-color:' + Config.colors(color[0], color[1]) + ';border-color:' + Config.colors(color[0], color[1]) + '">\n          <div class="fc-content">\n            <span class="fc-title">' + $this.data('title') + '</span>\n          </div>\n          </a>';
            }
          });
        });
      }
    }, {
      key: 'handleListItem',
      value: function handleListItem() {
        this.$actionToggleBtn.on('click', function (e) {
          $('#addNewCalendar').modal('show');
          e.stopPropagation();
        });

        $(document).on('click', '[data-tag=list-delete]', function (e) {
          bootbox.dialog({
            message: 'Do you want to delete the calendar?',
            buttons: {
              success: {
                label: 'Delete',
                className: 'btn-danger',
                callback: function callback() {
                  // $(e.target).closest('.list-group-item').remove();
                }
              }
            }
          });
        });
      }
    }]);
    return AppCalendar;
  }(_Site3.default);

  var instance = null;

  function getInstance() {
    if (!instance) {
      instance = new AppCalendar();
    }
    return instance;
  }

  function run() {
    var app = getInstance();
    app.run();
  }

  exports.default = AppCalendar;
  exports.AppCalendar = AppCalendar;
  exports.run = run;
  exports.getInstance = getInstance;
});
