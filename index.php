<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="./assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="./assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="./global/css/bootstrap.min.css">
  <link rel="stylesheet" href="./global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="./assets/css/site.css">
  <link rel="stylesheet" href="./assets/skins/teal.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="./global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="./global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="./global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="./global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="./global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="./global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="./global/vendor/waves/waves.css">
  <link rel="stylesheet" href="./assets/examples/css/pages/login-v2.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="./global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="./global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
  <!--[if lt IE 9]>
    <script src="./global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="./global/vendor/media-match/media.match.min.js"></script>
    <script src="./global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <style type="text/css">
    #img {
    -webkit-transition: all 0.5s ease-in-out;
    -moz-transition: all 0.5s ease-in-out; 
    -o-transition: all 0.5s ease-in-out; 
    -ms-transition: all 0.5s ease-in-out; 
    }
  </style>
  <!-- Import By Na! -->
  <style>
    .lockbox{
      background: url('/css_lock/lock_bg.png') center center no-repeat;
      width: 200px;
      height: 220px;
      position: relative;
      margin: 0 auto;
      overflow: hidden;
    }
    .lockbox > img{
      position: absolute;
      left: 0;
      right: 0;
      top: -10px;
      bottom: 0;
      margin: auto;
    }
  </style>
  <!-- End Import By Na! -->
</head>
<body class="animsition page-login-v2 layout-full page-dark">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <!-- Page -->
  <div class="page" data-animsition-in="fade-in" data-animsition-out="fade-out" style="padding-top: 1px;">
    <div class="page-content">
      <div class="page-brand-info" style="text-align:center;">
        

      </div>
      <div class="page-login-main" style="overflow: hidden;">
        <div class="brand" style="text-align: center; ">
          <img class="brand-img" style="margin-left:-20px; height: 150px" src="./assets/images/logo.png" alt="...">
        </div>
        <div style="position: relative; z-index: 200;">
          <form method="post" action="login.php" autocomplete="off">
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="email" class="form-control empty" id="inputEmail" name="email">
              <label class="floating-label" for="inputEmail">Email</label>
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="password" class="form-control empty KeyAutoLock" id="inputPassword" name="password">
              <label class="floating-label" for="inputPassword">Password</label>
            </div>
            <div class="form-group clearfix">
              <div class="checkbox-custom checkbox-inline checkbox-primary float-left">
                <input type="checkbox" id="remember" name="checkbox">
                <label for="inputCheckbox">Remember me</label>
              </div>
              <a class="float-right" href="forgot-password.php">Forgot password?</a>
            </div>
            <button type="sbumit" class="btn btn-primary btn-block mt-10">Sign in</button>
          </form>
          <p>No account? <a href="register.php">Sign Up</a></p>
        </div>
        <div class="clerfix"></div>
        <div class="lockbox">
          <img id="imgRotate" src="/css_lock/strongbox.png" alt="strongbox">
        </div>
      </div>
    </div>
  </div>
  <!-- End Page -->
  <script src="./global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
  <!-- Core  -->
  <script src="./global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="./global/vendor/jquery/jquery.js"></script>
  <script src="./global/vendor/tether/tether.js"></script>
  <script src="./global/vendor/bootstrap/bootstrap.js"></script>
  <script src="./global/vendor/animsition/animsition.js"></script>
  <script src="./global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="./global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="./global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="./global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="./global/vendor/switchery/switchery.min.js"></script>
  <script src="./global/vendor/intro-js/intro.js"></script>
  <script src="./global/vendor/screenfull/screenfull.js"></script>
  <script src="./global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="./global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
  <!-- Scripts -->
  <script src="./global/js/State.js"></script>
  <script src="./global/js/Component.js"></script>
  <script src="./global/js/Plugin.js"></script>
  <script src="./global/js/Base.js"></script>
  <script src="./global/js/Config.js"></script>
  <script src="./assets/js/Section/Menubar.js"></script>
  <script src="./assets/js/Section/GridMenu.js"></script>
  <script src="./assets/js/Section/Sidebar.js"></script>
  <script src="./assets/js/Section/PageAside.js"></script>
  <script src="./assets/js/Plugin/menu.js"></script>
  <script src="./global/js/config/colors.js"></script>
  <script src="./assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', './assets');
  </script>
  <script src="./assets/js/Site.js"></script>
  <script src="./global/js/Plugin/asscrollable.js"></script>
  <script src="./global/js/Plugin/slidepanel.js"></script>
  <script src="./global/js/Plugin/switchery.js"></script>
  <script src="./global/js/Plugin/jquery-placeholder.js"></script>
  <script src="./global/js/Plugin/material.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    var deg = 0;
    $(document).ready(function() {
      Site.run();

    });

    function rotate(deg){
        $('#img').css('-moz-transform', 'rotate('+deg+'deg)');
        $('#img').css('-webkit-transform', 'rotate('+deg+'deg)');
        $('#img').css('-o-transform:', 'rotate('+deg+'deg)');
        $('#img').css('-ms-transform:', 'rotate('+deg+'deg)');
    }
  })(document, window, jQuery);

  </script>
  <!-- Import By:Na! -->
  <script src="/css_lock/jQueryRotate.js"></script>
  <script>
    jQuery(document).ready(function($) {
      var lastKey = 0;
      $(".KeyAutoLock").keypress(function(event) {
        var nRand = Math.floor((Math.random() * 360) + 1);
        $('#imgRotate').rotate({
            angle: lastKey,
            animateTo:nRand,
            easing: $.easing.easeInOutBack
            });
        lastKey = nRand;
      });
    });
  </script>
  <!-- Exit Import By:Na! -->
</body>
</html>