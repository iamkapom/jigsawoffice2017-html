function sysGenUrlSearch(){

    var urlString='';
    if($('.page-content .search-wrapper .token').size()>0){
        $('.page-content .search-wrapper .clear_all').show();
    }else{
        $('.page-content .search-wrapper .clear_all').hide();
    }
    $('.page-content .search-wrapper .token').each(function(){
        var dataValue = $(this).data('value');
        var dataKey = $(this).data('key');
        var dataText=$(this).find('.token-label').text();

        if(dataKey=='keywords'){
            urlString+=dataKey+'[]='+dataValue;
        }else{
            urlString+=dataKey+'[]='+dataValue+'-'+dataText;
        }

        urlString+='&';

    });


    window.history.pushState(null,null, location.href.split('/')[ location.href.split('/').length - 1 ]);
    //window.history.pushState(null,null, "users.php?"+urlString);
    //window.history.pushState(null,null, location.href.split('/')[ location.href.split('/').length - 1 ]+"?"+urlString);
  // sysTextSearch(urlString);

}

function sysGenUrlSearch2(){

    var urlString='';
    if($('#searchFillIn .search-wrapper .token').size()>0){
        $('#searchFillIn .search-wrapper .clear_all').show();
    }else{
        $('#searchFillIn .search-wrapper .clear_all').hide();
    }
    $('#searchFillIn .search-wrapper .token').each(function(){
        var dataValue = $(this).data('value');
        var dataKey = $(this).data('key');
        var dataText=$(this).find('.token-label').text();

        if(dataKey=='keywords'){
            urlString+=dataKey+'[]='+dataValue;
        }else{
            urlString+=dataKey+'[]='+dataValue+'-'+dataText;
        }

        urlString+='&';

    });


    //window.history.pushState(null,null, location.href.split('/')[ location.href.split('/').length - 1 ]);

}


function sysFilterSearch(elmstring){

    $(elmstring+' .dropdown-menu .dropdown-item').on('click', function(e){
        var dataValue = $(this).data('value');
        var dataKey = $(this).data('key');
        var dataText=$(this).text();

        var flag=true;
        $('.page-content .search-wrapper .token[data-key="permission"]').each(function(){
            if(dataValue==$(this).data('value')){
                flag=false;
            }
        });


        if(flag==true) {
            var stoken = '';
            stoken = '<div class="token" data-key="'+dataKey+'" data-value="' + dataValue + '"><span class="token-label" >' + dataText + '</span><a  href="#" class="close" tabindex="-1">×</a></div>';
            $('.page-content .search-wrapper .currently-showing').append(stoken);
            sysGenUrlSearch();
        }

    });

}

function sysFilterSearch2(elmstring){

    $(elmstring+' .dropdown-menu .dropdown-item').on('click', function(e){
        var dataValue = $(this).data('value');
        var dataKey = $(this).data('key');
        var dataText=$(this).text();

        var flag=true;
        $('#searchFillIn .search-wrapper .token[data-key="permission"]').each(function(){
            if(dataValue==$(this).data('value')){
                flag=false;
            }
        });


        if(flag==true) {
            var stoken = '';
            stoken = '<div class="token" data-key="'+dataKey+'" data-value="' + dataValue + '"><span class="token-label" >' + dataText + '</span><a  href="#" class="close" tabindex="-1">×</a></div>';
            $('#searchFillIn .search-wrapper .currently-showing').append(stoken);
            sysGenUrlSearch2();
        }

    });

}

function chooseSearchAllDateRange(start, end) {
  var DrangeValue = start.format('MM/DD/YYYY') + '-' + end.format('MM/DD/YYYY');
  $('#SearchAllDateRange span#daterange-value').html(DrangeValue);
  var dataKey = "123";
  var dataValue = "xxx";
  var stoken = '';
    $('#searchFillIn .search-wrapper .currently-showing .chooseDateRange').remove();
    stoken = '<div class="token chooseDateRange" data-key="'+dataKey+'" data-value="' + dataValue + '"><span class="token-label" >' + DrangeValue + '</span><a  href="#" class="close" tabindex="-1">×</a></div>';
    $('#searchFillIn .search-wrapper .currently-showing').append(stoken);
    sysGenUrlSearch2();
}

$(document).ready(function() {



    sysFilterSearch('.page-content  .filter_permission');

    $('.page-content .search-wrapper .clear_all').hide();

    $('.page-content .search-box').on('click', function(e){
        $('.page-content .search-wrapper .data_entry input').val('');
        $('.page-content .data_entry').slideUp();
        $('.page-content .data_entry').slideDown("fast", function(e){
            $('.page-content input.keyword-input').focus();
        });
    });

    $('.page-content input.keyword-input').focusout(function() {
        $('.data_entry').slideUp("fast");
    });

    $('.page-content .search-wrapper .data_entry input').keypress(function (e) {
        if (e.which == 13) {
            if($(this).val()!='') {
                var dataValue=$(this).val();
                var flag=true;
                $('.page-content .search-wrapper .token[data-key="keywords"]').each(function(){
                    if(dataValue==$(this).data('value')){
                        flag=false;
                    }
                });
                if(flag==true) {
                    var stoken = '';
                    stoken = '<div class="token" data-key="keywords" data-value="' + $(this).val() + '"><span class="token-label" >' + $(this).val() + '</span><a  href="#" class="close" tabindex="-1">×</a></div>';
                    $('.page-content .search-wrapper .currently-showing').append(stoken);
                    sysGenUrlSearch();

                }
                $(this).val('');
            }
        }
    });

    $('.page-content .search-wrapper .currently-showing').on('click','a.close', function(e){
        $(this).parent('.token').remove();
        $('.page-content .data_entry').slideUp();
        sysGenUrlSearch();
        return false;
    });

    $('.page-content .search-wrapper a.clear_all').on('click', function(e){
        $('.page-content .search-wrapper .currently-showing').html('');
        sysGenUrlSearch();
    });


    sysFilterSearch2('#searchFillIn .filter_permission');
    $('#searchFillIn .search-wrapper .clear_all').hide();
    $('#searchFillIn .search-box2').on('click', function(e){
        $('#searchFillIn .search-wrapper .data_entry input').val('');
        $('#searchFillIn .data_entry').slideUp();
        $('#searchFillIn .data_entry').slideDown("fast", function(e){
            $('#searchFillIn input.keyword-input').focus();
        });
    });

    $('#searchFillIn input.keyword-input').focusout(function() {
        $('#searchFillIn .data_entry').slideUp("fast");
    });

    $('#searchFillIn .search-wrapper .data_entry input').keypress(function (e) {
        if (e.which == 13) {
            if($(this).val()!='') {
                var dataValue=$(this).val();
                var flag=true;
                $('#searchFillIn .search-wrapper .token[data-key="keywords"]').each(function(){
                    if(dataValue==$(this).data('value')){
                        flag=false;
                    }
                });
                if(flag==true) {
                    var stoken = '';
                    stoken = '<div class="token" data-key="keywords" data-value="' + $(this).val() + '"><span class="token-label" >' + $(this).val() + '</span><a  href="#" class="close" tabindex="-1">×</a></div>';
                    $('#searchFillIn .search-wrapper .currently-showing').append(stoken);
                    sysGenUrlSearch2();

                }
                $(this).val('');
            }
        }
    });

    $('#searchFillIn .search-wrapper .currently-showing').on('click','a.close', function(e){
        $(this).parent('.token').remove();
        $('#searchFillIn .data_entry').slideUp();
        sysGenUrlSearch2();
        return false;
    });

    $('#searchFillIn .search-wrapper a.clear_all').on('click', function(e){
        $('#searchFillIn .search-wrapper .currently-showing').html('');
        sysGenUrlSearch2();
    });

});
