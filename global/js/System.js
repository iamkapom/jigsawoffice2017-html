function sysListSearch(){ 
  $('#Sys_Page').val(1);
  sysListLoadDatalist($('#mySearch').serialize());
}
function sysListSort(FSort,Sort){
  $('#Sys_FSort').val(FSort);
  $('#Sys_Sort').val(Sort); 
  sysListLoadDatalist($('#mySearch').serialize());
}

function sysListLoadDatalist(val){
  
  var Vars = "ModuleAction=Datalist&"+val;
  $.ajax({
    url : $('#sysModURL').val(),
    data : Vars,
    type : "post",
    cache : false ,
    success : function(resp) {
      $("#datalist-content").html(resp);
    }
  }); 
}


function sysListPage(page){ 
  
  if(parseInt(page)>parseInt($('#SysTotalPageCount').val())){
    page=$('#SysTotalPageCount').val();
  }
  
  $('#SysPage').val(page);
  
  sysListLoadDatalist($('#mySearch').serialize());
}

function sysListGOPage(){ 
  var page=$('#GoToPage').val();
  if(parseInt(page)>parseInt($('#SysTotalPageCount').val())){
    page=$('#SysTotalPageCount').val();
  }
  
  $('#SysPage').val(page);
  sysListLoadDatalist($('#mySearch').serialize());
}

function sysListPerPage(PageSize){
  $('#SysPageSize').val(PageSize);
  $('#SysPage').val(1);
  sysListLoadDatalist($('#mySearch').serialize());
}

function sysTextSearch(str){

    $('#sysTextSearch').val(str);
    $('#sysPage').val(1);
    sysListLoadDatalist($('#modSearch').serialize());
}

