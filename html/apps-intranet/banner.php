<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition app-documents">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-10">Banner</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item active">Apps</li>
        <li class="breadcrumb-item active"><a href="index.php">Intranet</a></li>
        <li class="breadcrumb-item active">Banner</li>
      </ol>
      <?php include("mini-nav.php");?>
    </div>

    <div class="page-content container-fluid " style="position: relative;">

      <div class="row ml-0 mr-0">
        <div class="col-md-12">

          <div class="page-main">
          <div class="pt-0 pb-20">
            <div class="search-wrapper">
              <div class="search-box">
                <div class="icon md-search"></div>
                <div class="currently-showing">
                  <?=$search['tokenhtml']?>
                </div>
              </div>
              <a href="javascript:void(0);" class="clear_all btn btn-flat btn-default btn-block waves-effect waves-classic">Clear All</a>
              <div class="data_entry">
                <input class="input keyword-input" placeholder="Enter a keyword" type="text">
                <div class="icon md-close-circle close"></div>
              </div>
              <div class="filters">
                <div class="dropdown filter_permission">
                  <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                    Display on
                    <span class="icon md-chevron-down" aria-hidden="true"></span>
                  </button>
                  <div class="dropdown-menu" role="menu">
                    <a class="dropdown-item" href="javascript:void(0)">Top Graphic</a>
                    <a class="dropdown-item" href="javascript:void(0)">Banner Link 1</a>
                    <a class="dropdown-item" href="javascript:void(0)">Banner Link 2</a>
                  </div>
                </div>
                <div class="dropdown filter_permission">
                  <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                    Status
                    <span class="icon md-chevron-down" aria-hidden="true"></span>
                  </button>
                  <div class="dropdown-menu" role="menu">
                    <a class="dropdown-item" href="javascript:void(0)">Enable</a>
                    <a class="dropdown-item" href="javascript:void(0)">Disable</a>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <section>
            <div class="pb-20">
              <div class="float-right">
                <div class="dropdown">
                  <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                    <i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Name
                    <span class="icon md-chevron-down" aria-hidden="true"></span>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right dropdown-menu-media w-100" role="menu">
                    <h6 class="pl-10" aria-hidden="false">Sory by</h6>
                    <a class="dropdown-item active" href="javascript:void(0)">Name</a>
                    <a class="dropdown-item" href="javascript:void(0)">Click</a>
                    <a class="dropdown-item" href="javascript:void(0)">Create Date</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-sort-amount-asc" aria-hidden="true"></i> Ascending</a>
                    <a class="dropdown-item active" href="javascript:void(0)"><i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Descending</a>
                  </div>
                </div>
              </div>

              <div class="pt-10">Total <strong>26</strong> rows</div>
            </div>
            <div class="row">
              <?php
              $_disable = "-webkit-filter: grayscale(100%); filter: grayscale(100%);opacity: 0.5;filter: alpha(opacity=50);";
              $b=1;
              for($a=1;$a<=30;$a++){
                if($a==21) $b=1;
                $_randisable = rand(1,5);
              ?>
              <div class="col-lg-6 col-md-6">
                <div class="card card-shadow card-bordered">
                  <div class="card-header bg-grey-100 p-0 clearfix" style="padding-bottom: 0px;">
                    <div class="more-action" style="">
                      <button type="button" class="btn btn-icon btn-dark btn-round" data-toggle="dropdown" aria-hidden="true">
                        <i class="icon md-more-vert" aria-hidden="true"></i>
                      </button>
                      <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <a class="dropdown-item <?=($_randisable)>=4?"":"active";?>" href="javascript:void(0)" role="menuitem">
                          Enable
                        </a>
                        <a class="dropdown-item <?=($_randisable)>=4?"active":"";?>" href="javascript:void(0)" role="menuitem">
                          Disable
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                          Edit
                        </a>
                        <a class="dropdown-item text-danger" href="javascript:void(0)" role="menuitem">
                          Delete
                        </a>
                      </div>
                    </div>

                    
                    <img width="100%" src="../../assets/images/banner-1.jpg" style="<?=($_randisable)>=4?$_disable:"";?>" alt="">
                    
                  </div>
                  <div class="card-block" style="padding: 10px 20px;">
                    <section>
                      <?php
                      $aa[] = "Top Graphic";
                      $aa[] = "Banner Link 1";
                      $aa[] = "Banner Link 2";
                      $aa[] = "Banner Link 3";
                      for($ab=0;$ab<rand(1,4);$ab++){
                      ?>
                      <a href="#">
                        <span class="badge mb-5 badge-<?=($_randisable)>=4?"default":"dark";?>"><?=$aa[$ab];?></span>
                      </a>
                      <?php }?>
                    </section>
                    <div class="row pt-10 <?=($_randisable)>=4?"grey-400":"black";?>" >
                      <div class="col-md-12">
                        <p data-info-type="phone" class="mb-10 text-nowrap font-size-14">
                          <i class="icon md-link mr-10"></i>
                          <span class="text-break">http://www.goog.co.th
                            <span>
                        </span></span></p>
                      </div>
                      <div class="col-md-12">
                        <p data-info-type="email" class="mb-10 text-nowrap font-size-14">
                          <i class="icon md-calendar-alt mr-10"></i>
                          <span class="text-break">12/6/2018 - 5/10/2018
                            <span>
                        </span></span></p>
                      </div>
                      <div class="col-md-12">
                        <p data-info-type="email" class="mb-10 text-nowrap font-size-14">
                          
                          <span class="text-break"><?=rand(25,100)?> Click
                            <span>
                        </span></span></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php }?>

            </div>
            <div class="data-pagination">
                <div class="pagination-per-page">
                    <span class="page-list">
                      <span class="btn-group dropup">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                          <span class="page-size">10</span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-bullet" role="menu">
                          <li role="menuitem" class="dropdown-item active"><a href="#">10</a></li>
                          <li role="menuitem" class="dropdown-item"><a href="#">25</a></li>
                        </ul>
                    </span> rows per page
                  </span>
                </div>
                <nav>
                  <ul class="pagination justify-content-center pagination-gap">
                    <li class="disabled page-item">
                      <a class="page-link" href="javascript:void(0)" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                      </a>
                    </li>
                    <?php
                    $page = 4;
                    for($aa=1;$aa<=7;$aa++){?>
                    <li class="<?=($aa==$page?"active":"")?> page-item <?=($aa<($page-2) || $aa>($page+2)?"hidden-sm-down":"")?> <?=($aa==($page-2) || $aa==($page+2)?"hidden-xs-down":"")?>"><a class="page-link" href="javascript:void(0)"><?=$aa?></a></li>
                    <?php }?>
                    <li class="page-item">
                      <a class="page-link" href="javascript:void(0)" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
          </section>
        </div>

        </div>
      </div>

    </div>
  </div>
  <div class="site-action" data-plugin="actionBtn">
    <button type="button" class="btn-raised btn btn-success btn-floating">
      <i class="front-icon md-plus" aria-hidden="true"></i>
    </button>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>
