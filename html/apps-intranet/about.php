<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../assets/examples/css/apps/documents.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <style type="text/css">
  .slick-dots li button::before{font-size: 14px;}
  .btn-comment-post .btn-select{
    border: 0px !important;
  }
  .btn-comment-post, .btn-comment-post .bootstrap-select{
    width: 150px !important;
  }
  .fix-mini-nav + .page-content{
    padding-top: 0;
  }
  .fix-mini-nav .page-header{
    margin-bottom: 93px;
  }
  .fix-mini-nav .fix-mini-nav{
    margin-bottom: 0;
    margin-right: 0;
    margin-top: 0;
    padding: 0;
    position: fixed;
    top: 65px;
    width: 100%;
    z-index: 10;
    left: 0;
  }
  .btn-comment-post .btn-select{
    border: 0px !important;
  }
  .btn-comment-post, .btn-comment-post .bootstrap-select{
    width: 150px !important;
  }
  .page-content{
    padding: 0;
  }
  </style>

</head>
<body class="animsition app-documents">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-10">About Company</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item active">Apps</li>
        <li class="breadcrumb-item active"><a href="index.php">Intranet</a></li>
        <li class="breadcrumb-item active">About Company</li>
      </ol>
<?php include("mini-nav.php");?>
    </div>

    <div class="page-content bg-white">
      <div class="p-20">
        <div class="panel-body container-fluid">
          <section style="text-align:center;">
            <img src="../../assets/images/logo-jigsaw2.png" width="50%" title="Jigsaw Office">
          </section>
          <section>
            <div class="card mt-80">
              <div class="card-img-left">
                <div class="cover-background" style="background-image: url('../../assets/examples/images/lockscreen.jpg')"></div>
              </div>
              <div class="card-block">
                <h2 class="card-title text-primary"><span class="drop-cap drop-cap-reversed" style="font-size: 2.143rem; padding: 3px; line-height: 2.143rem; font-family: Roboto,sans-serif;">WHO</span> WE ARE</h2>
                <p class="font">We build awesome websites with minimal design that sells a lot.</p>
                <div class="row mt-40">
                  <div class="col-lg-6 col-md-12">
                    <!-- Example Body Copy -->
                    <div class="example-wrap">
                      <h4 class="example-title">SLEEK DESIGN</h4>
                      <p>Lorem Ipsum is simply dumiti my text of the printing and is type esetting let.</p>
                    </div>
                    <!-- End Example Body Copy -->
                  </div>
                  <div class="col-lg-6 col-md-12">
                    <!-- Example Highlight -->
                    <div class="example-wrap">
                      <h4 class="example-title">BRAND IDENTITY</h4>
                      <p>Lorem Ipsum is simply dumiti my text of the printing and is type esetting let.</p>
                    </div>
                    <!-- End Example Highlight -->
                  </div>
                  <div class="col-lg-6 col-md-12">
                    <!-- Example Body Copy -->
                    <div class="example-wrap">
                      <h4 class="example-title">BRAND IDENTITY</h4>
                      <p>Lorem Ipsum is simply dumiti my text of the printing and is type esetting let.</p>
                    </div>
                    <!-- End Example Body Copy -->
                  </div>
                  <div class="col-lg-6 col-md-12">
                    <!-- Example Highlight -->
                    <div class="example-wrap">
                      <h4 class="example-title">PIXEL PERFECT</h4>
                      <p>Lorem Ipsum is simply dumiti my text of the printing and is type esetting let.</p>
                    </div>
                    <!-- End Example Highlight -->
                  </div>

                </div>
              </div>
            </div>
          </section>
          <section>
            <div class="card mt-80">
              <div class="card-img-right">
                <div class="cover-background" style="background-image: url('../../assets/examples/images/login.jpg')"></div>
              </div>
              <div class="card-block">
                <h2 class="card-title text-primary"><span class="drop-cap drop-cap-reversed" style="font-size: 2.143rem; padding: 3px; line-height: 2.143rem; font-family: Roboto,sans-serif;">WHAT</span> WE DO</h2>
                <p class="font">We build awesome websites with minimal design that sells a lot.</p>
                <div class="row mt-40">
                  <div class="col-md-12">
                    <!-- Example Body Copy -->
                    <div class="example-wrap">
                      <h4 class="example-title"><i class="icon md-book" aria-hidden="true"></i>IMPORTANT NOTES</h4>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum ita pellesque imperdiet. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <!-- End Example Body Copy -->
                  </div>
                  <div class="col-md-12">
                    <!-- Example Body Copy -->
                    <div class="example-wrap">
                      <h4 class="example-title"><i class="icon md-book" aria-hidden="true"></i>HANDLE WITH LOVE</h4>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum ita pellesque imperdiet. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <!-- End Example Body Copy -->
                  </div>
                  <div class="col-md-12">
                    <!-- Example Body Copy -->
                    <div class="example-wrap">
                      <h4 class="example-title"><i class="icon md-book" aria-hidden="true"></i>GRAPHIC DESIGN</h4>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum ita pellesque imperdiet. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <!-- End Example Body Copy -->
                  </div>

                </div>
              </div>
            </div>
          </section>
          <section>
            <div class="mt-80" style="text-align:center;">
              <h2 class="card-title text-primary"><span class="text-muted">OUR</span> SERVICES</h2>
                <p class="font">Lorem Ipsum is simply has been the industry's standrd dummy text everdummy texto the printing and typesetting industry lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            </div>
            <div class="row mt-40">
              <div class="col-md-6 col-lg-3">
                <div class="card card-bordered card-outline-info">
                  <div class="card-block">
                    <div class="text-info" style="font-size:100px; text-align:center;"><i class="icon fa-youtube-play" aria-hidden="true"></i></div>
                    <div style="text-align:center;">
                        <h3 class="mb-20">VIDEO MARKETING</h3>
                    Lorem Ipsum is simply dummy text of the printing and auiao typesetting let.
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-3">
                <div class="card card-bordered card-outline-info">
                  <div class="card-block">
                    <div class="text-info" style="font-size:100px; text-align:center;"><i class="icon fa-file-text-o" aria-hidden="true"></i></div>
                    <div style="text-align:center;">
                        <h3 class="mb-20">CONTENT MARKETING</h3>
                    Lorem Ipsum is simply dummy text of the printing and auiao typesetting let.
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-3">
                <div class="card card-bordered card-outline-info">
                  <div class="card-block">
                    <div class="text-info" style="font-size:100px; text-align:center;"><i class="icon fa-line-chart" aria-hidden="true"></i></div>
                    <div style="text-align:center;">
                        <h3 class="mb-20">QUALITY SUPPORT</h3>
                    Lorem Ipsum is simply dummy text of the printing and auiao typesetting let.
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-3">
                <div class="card card-bordered card-outline-info">
                  <div class="card-block">
                    <div class="text-info" style="font-size:100px; text-align:center;"><i class="icon fa-rocket" aria-hidden="true"></i></div>
                    <div style="text-align:center;">
                        <h3 class="mb-20">SOCIAL MEDIA MARKETING</h3>
                    Lorem Ipsum is simply dummy text of the printing and auiao typesetting let.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section>
            <div class="mt-80" style="text-align:center;">
              <h2 class="card-title text-primary"><span class="text-muted">OUR</span> History</h2>
                <p class="font">We’re proud to bring our creative thinking to those in need in the community. our values and the organizations we’re doing good.</p>
            </div>
            <div class="row mt-40">
              <div class="col-md-12">
                <div class="media">
                  <div class="pr-40">
                    <span class="avatar avatar-100">
                      <img src="../../../../global/portraits/1.jpg" alt="...">
                    </span>
                  </div>
                  <div class="media-body">
                    <h2 class="media-heading">2016</h2>
                    <div class="card card-bordered card-outline-info">
                      <div class="card-block">
                    When you need game-changing ideas, strategic positioning, fresh perspective and a new approach too cultivated use solicitude frequently. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus,
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row mt-40">
              <div class="col-md-12">
                <div class="media">
                  <div class="pr-40">
                    <span class="avatar avatar-100">
                      <img src="../../../../global/portraits/2.jpg" alt="...">
                    </span>
                  </div>
                  <div class="media-body">
                    <h2 class="media-heading">2015</h2>
                    <div class="card card-bordered card-outline-info">
                      <div class="card-block">
                    When you need game-changing ideas, strategic positioning, fresh perspective and a new approach too cultivated use solicitude frequently. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus,
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row mt-40">
              <div class="col-md-12">
                <div class="media">
                  <div class="pr-40">
                    <span class="avatar avatar-100">
                      <img src="../../../../global/portraits/3.jpg" alt="...">
                    </span>
                  </div>
                  <div class="media-body">
                    <h2 class="media-heading">2014</h2>
                    <div class="card card-bordered card-outline-info">
                      <div class="card-block">
                    When you need game-changing ideas, strategic positioning, fresh perspective and a new approach too cultivated use solicitude frequently. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus,
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row mt-40">
              <div class="col-md-12">
                <div class="media">
                  <div class="pr-40">
                    <span class="avatar avatar-100">
                      <img src="../../../../global/portraits/4.jpg" alt="...">
                    </span>
                  </div>
                  <div class="media-body">
                    <h2 class="media-heading">2013</h2>
                    <div class="card card-bordered card-outline-info">
                      <div class="card-block">
                    When you need game-changing ideas, strategic positioning, fresh perspective and a new approach too cultivated use solicitude frequently. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus,
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

        </div>
      </div>


    </div>
  </div>
  <div class="site-action" data-plugin="actionBtn">
    <button type="button" onclick="location.href='about-form.php';" class="btn-raised btn btn-success btn-floating">
      <i class="front-icon md-edit" aria-hidden="true"></i>
    </button>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../assets/js/App/Documents.js"></script>
  <script src="../../assets/examples/js/apps/documents.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();

    });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>
