<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../assets/images/favicon.ico">
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../assets/examples/css/pages/profile_v3.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
</head>
<body class="animsition">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <!-- Page -->
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-10">Intranet</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item active">Apps</li>
        <li class="breadcrumb-item active">Intranet</li>
      </ol>
      <?php include("mini-nav.php");?>
      <div class="page-header-actions">
        <button type="button" onClick="location.href='index-setting.php';" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Setting">
          <i class="icon md-settings" aria-hidden="true"></i>
        </button>
      </div>
    </div>

    <div class="page-content container-fluid">

      <div class="row ml-0 mr-0">
        <div class="col-md-7">
          <section>
            <div class="pb-10">
              <div class="search-wrapper">
                <div class="search-box">
                  <div class="icon md-search"></div>
                  <div class="currently-showing">
                    <?=$search['tokenhtml']?>
                  </div>
                </div>
                <a href="javascript:void(0);" class="clear_all btn btn-flat btn-default btn-block waves-effect waves-classic">Clear All</a>
                <div class="data_entry">
                  <input class="input keyword-input" placeholder="Enter a keyword" type="text">
                  <div class="icon md-close-circle close"></div>
                </div>
                <div class="filters">
                    <div class="dropdown filter_permission">
                      <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Modules
                        <span class="icon md-chevron-down" aria-hidden="true"></span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)"> News</a>
                        <a class="dropdown-item" href="javascript:void(0)"> Announcement</a>
                        <a class="dropdown-item" href="javascript:void(0)"> Photos</a>
                        <a class="dropdown-item" href="javascript:void(0)"> Videos</a>
                        <a class="dropdown-item" href="javascript:void(0)"> Events</a>
                        <a class="dropdown-item" href="javascript:void(0)"> Policy</a>
                      </div>
                    </div>
                    <div class="dropdown filter_permission">
                      <button type="button" id="SearchDateRange" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Time <i class="icon md-chevron-down" aria-hidden="true"></i>
                        <span id="daterange-value"></span>
                      </button>
                    </div>

                </div>
              </div>
            </div>

            </section>
            <section>

              <div class="pt-10 mb-20">Pinned Post</div>

              <div class="card card-shadow card-bordered post-wall">
                <div class="card-block clearfix p-25">
                  <div class="media-avatar">
                    <a href="#" class="avatar avatar-lg">
                      <img class="img-fluid" src="../../../global/portraits/<?=rand(1,20)?>.jpg">
                    </a>
                  </div>
                  <div class="media-body text-middle">
                    <h5 class="mt-0 mb-0">
                      <a href="#">Mallinda Hollaway</a> to <a href="#">News</a>
                    </h5>
                    <small>30th July 2017</small>
                  </div>
                </div>
                <div class="card-block px-25 pt-0 pb-0">
                  <div class="card-text mb-20 grey-800">
                    <div class="row">
                      <div class="col-md-5">
                        <a href="#"><img class="cover-image" align="left" src="<?=get_img("H")?>" alt="..." /></a>
                      </div>
                      <div class="col-md-7">
                        <a href="#"><h4 class="m-0 mb-10">News : example title</h4></a>
                        Ex quis excepteur exercitation incididunt ea amet commodo deserunt nulla. Anim
                        sit duis cupidatat ipsum deserunt reprehenderit fugiat cupidatat.
                        In do eu sint Lorem qui eu eu.
                      </div>
                    </div>
                  </div>

                  <ul class="wall-attrs clearfix p-0 m-0 mt-10">
                    <li class="attrs-meta float-left font-size-16">
                      <a href="javascript:void(0)" class="mr-20">
                        <i class="icon md-thumb-up"></i>
                        <span>5</span>
                      </a>
                      <a href="javascript:void(0)" onclick="$('#wall-comment-reply-1').toggle();">
                        <i class="icon md-comment"></i>
                        <span>2</span>
                      </a>
                    </li>
                    <li class="float-right">
                      <div class="btn-group bootstrap-select btn-comment-post">
                        <select data-plugin="selectpicker">
                          <option>Top Comments</option>
                          <option>Most Recent</option>
                        </select>
                      </div>
                    </li>
                  </ul>
                </div>

              </div>
            </section>
            <section>
              <div class="pb-20">
                <div class="actions-inner float-right">
                  <div class="dropdown">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      <i class="icon fa-sort-amount-desc" aria-hidden="true"></i> New Post
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-media w-100" role="menu">
                      <h6 class="pl-10" aria-hidden="false">Sory by</h6>
                      <a class="dropdown-item active" href="javascript:void(0)">New Post</a>
                      <a class="dropdown-item" href="javascript:void(0)">Recent Activity</a>
                      <a class="dropdown-item" href="javascript:void(0)">Top Comments</a>
                      <a class="dropdown-item" href="javascript:void(0)">Top Likes</a>
                      <a class="dropdown-item" href="javascript:void(0)">Attach File</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-sort-amount-asc" aria-hidden="true"></i> Ascending</a>
                      <a class="dropdown-item active" href="javascript:void(0)"><i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Descending</a>
                    </div>
                  </div>
                </div>

                <div class="pt-10">About <strong>26</strong> results</div>
              </div>
            </section>
            <section>

              <div class="card card-shadow card-bordered post-wall">
                <div class="card-block clearfix p-25">
                  <div class="media-avatar">
                    <a href="#" class="avatar avatar-lg">
                      <img class="img-fluid" src="../../../global/portraits/<?=rand(1,20)?>.jpg">
                    </a>
                  </div>
                  <div class="media-body text-middle">
                    <h5 class="mt-0 mb-0">
                      <a href="#">Mallinda Hollaway</a> to <a href="#">News</a>
                    </h5>
                    <small>30th July 2017</small>
                  </div>
                </div>
                <div class="card-block px-25 pt-0 pb-0">
                  <div class="card-text mb-20 grey-800">
                    <div class="row">
                      <div class="col-md-5">
                        <a href="#"><img class="cover-image" align="left" src="<?=get_img("R")?>" alt="..." /></a>
                      </div>
                      <div class="col-md-7">
                        <a href="#"><h4 class="m-0 mb-10">News : example title</h4></a>
                        Ex quis excepteur exercitation incididunt ea amet commodo deserunt nulla. Anim
                        sit duis cupidatat ipsum deserunt reprehenderit fugiat cupidatat.
                        In do eu sint Lorem qui eu eu. Id ad non pariatur culpa. Duis proident
                        cupidatat laborum pariatur sit eu eiusmod. Cillum consectetur exercitation
                        ex ipsum. Ullamco commodo anim ut aliqua ex incididunt commodo
                        incididunt reprehenderit. Sit nisi deserunt fugiat eu qui nisi
                        nulla.
                      </div>
                    </div>
                  </div>

                  <ul class="wall-attrs clearfix p-0 m-0 mt-10">
                    <li class="attrs-meta float-left font-size-16">
                      <a href="javascript:void(0)" class="mr-20">
                        <i class="icon md-thumb-up"></i>
                        <span>5</span>
                      </a>
                      <a href="javascript:void(0)" onclick="$('#wall-comment-reply-1').toggle();">
                        <i class="icon md-comment"></i>
                        <span>2</span>
                      </a>
                    </li>
                    <li class="float-right">
                      <div class="btn-group bootstrap-select btn-comment-post">
                        <select data-plugin="selectpicker">
                          <option>Top Comments</option>
                          <option>Most Recent</option>
                        </select>
                      </div>
                    </li>
                  </ul>
                </div>
                <div class="wall-comment-attrs">
                  <div id="wall-comment-reply-1" class="wall-comment-reply clearfix" style="display: none">
                    <div href="#" class="avatar avatar-md float-left">
                      <img src="../../../global/portraits/6.jpg">
                    </div>
                    <div class="ml-60 wall-comment-form">
                      <textarea rows="1" class="autoHeight" placeholder="Write a comment..."></textarea>
                      <div class="wall-comment-reply-attach">
                        <button class="btn btn-icon btn-dark btn-round bg-grey-400">
                          <i class="icon md-attachment-alt rotate" aria-hidden="true"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                  <div class="wall-comment">
                    <div class="wall-comment-action">
                    <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                      <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                    </button>

                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                        Edit Comment
                      </a>
                      <a class="dropdown-item text-danger" href="javascript:void(0)" role="menuitem">
                        Delete
                      </a>
                    </div>
                  </div>
                    <a href="#" class="avatar avatar-md float-left">
                      <img src="../../../global/portraits/3.jpg">
                    </a>
                    <div class="ml-60 box-post-comment">
                      <a href="#">Stacey Hunt</a>
                      <span class="ml-10">Do incididunt elit ex incididunt ut. Aute velit proident cupidatat qui et consectetur
                        do. Consequat esse quis ut cupidatat ea. Sint dolore ea culpa
                        dolore velit enim.</span>
                        <p class="font-size-12 mt-5 grey-600">
                          <a href="javascript:void(0)">
                            Like
                          </a>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <a href="javascript:void(0)" class="action-reply">
                            Reply
                          </a>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <span >
                            <i class="icon md-favorite"></i>
                            <span>5</span>
                          </span>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <span>
                            30th July 2017
                          </span>
                        </p>

                      <div class="wall-comment">
                        <a href="#" class="avatar avatar-md float-left">
                          <img src="../../../global/portraits/11.jpg">
                        </a>
                        <div class="ml-60">
                          <a href="#">Crystal Bates</a>
                          <span class="ml-10">Do incididunt elit ex incididunt ut. Aute velit proident cupidatat qui et consectetur
                            do. Consequat esse quis ut cupidatat ea.</span>
                          <p class="font-size-12 mt-5 grey-500">
                            <a href="javascript:void(0)">
                              Like
                            </a>
                            <span class="mr-5 ml-5">&#8226;</span>
                            <a href="javascript:void(0)" class="action-reply">
                              Reply
                            </a>
                            <span class="mr-5 ml-5">&#8226;</span>
                            <span >
                              <i class="icon md-favorite"></i>
                              <span>5</span>
                            </span>
                            <span class="mr-5 ml-5">&#8226;</span>
                            <span>
                              30th July 2017
                            </span>
                          </p>
                        </div>

                      </div>
                      <div class="display-reply"></div>

                    </div>

                  </div>
                  <div class="wall-comment">
                    <a href="#" class="avatar avatar-md float-left">
                      <img src="../../../global/portraits/5.jpg">
                    </a>
                    <div class="ml-60 box-post-comment">
                      <a href="#">Sam Anderson</a>
                      <span class="ml-10">Ut velit ipsum elit ut mollit elit. Proident officia eu ex consectetur ullamco
                        magna. Enim cillum voluptate sint ipsum ad voluptate exercitation.
                        Ex est amet magna occaecat eu.</span>
                      <p class="font-size-12 mt-5 grey-500">
                        <a href="javascript:void(0)">
                          Like
                        </a>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <a href="javascript:void(0)" class="action-reply">
                          Reply
                        </a>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <span >
                          <i class="icon md-favorite"></i>
                          <span>5</span>
                        </span>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <span>
                          30th July 2017
                        </span>
                      </p>

                      <div class="display-reply"></div>
                    </div>

                  </div>
                </div>
              </div>

              <div class="card card-shadow card-bordered post-wall">
                <div class="card-block clearfix p-25">
                  <div class="media-avatar">
                    <a href="#" class="avatar avatar-lg">
                      <img class="img-fluid" src="../../../global/portraits/<?=rand(1,20)?>.jpg">
                    </a>
                  </div>
                  <div class="media-body text-middle">
                    <h5 class="mt-0 mb-0">
                      <a href="#">Mallinda Hollaway</a> to <a href="#">Announcement</a>
                    </h5>
                    <small>30th July 2017</small>
                  </div>
                </div>
                <div class="card-block px-25 pt-0 pb-0">
                  <div class="card-text mb-20 grey-800">
                    <div class="row">
                      <div class="col-md-5">
                    <a href="#"><img class="cover-image" align="left" src="<?=get_img("R")?>" alt="..." /></a>
                      </div>
                      <div class="col-md-7">
                        <a href="#"><h4 class="m-0 mb-10">Announcement : example title</h4></a>
                        Ex quis excepteur exercitation incididunt ea amet commodo deserunt nulla. Anim
                        sit duis cupidatat ipsum deserunt reprehenderit fugiat cupidatat.
                        In do eu sint Lorem qui eu eu. Id ad non pariatur culpa. Duis proident
                        cupidatat laborum pariatur sit eu eiusmod. Cillum consectetur exercitation
                        ex ipsum. Ullamco commodo anim ut aliqua ex incididunt commodo
                        incididunt reprehenderit. Sit nisi deserunt fugiat eu qui nisi
                        nulla.
                      </div>
                    </div>
                  </div>

                  <ul class="wall-attrs clearfix p-0 m-0 mt-10">
                    <li class="attrs-meta float-left font-size-16">
                      <a href="javascript:void(0)" class="mr-20">
                        <i class="icon md-thumb-up"></i>
                        <span>5</span>
                      </a>
                      <a href="javascript:void(0)" onclick="$('#wall-comment-reply-1').toggle();">
                        <i class="icon md-comment"></i>
                        <span>2</span>
                      </a>
                    </li>
                    <li class="float-right">
                      <div class="btn-group bootstrap-select btn-comment-post">
                        <select data-plugin="selectpicker">
                          <option>Top Comments</option>
                          <option>Most Recent</option>
                        </select>
                      </div>
                    </li>
                  </ul>
                </div>

              </div>

              <div class="card card-shadow card-bordered post-wall">
                <div class="card-block clearfix p-25">
                  <div class="media-avatar">
                    <a href="#" class="avatar avatar-lg">
                      <img class="img-fluid" src="../../../global/portraits/<?=rand(1,20)?>.jpg">
                    </a>
                  </div>
                  <div class="media-body text-middle">
                    <h5 class="mt-0 mb-0">
                      <a href="#">Mallinda Hollaway</a> to <a href="#">Photos</a>
                    </h5>
                    <small>30th July 2017</small>
                  </div>
                </div>
                <div class="card-block px-25 pt-0 pb-0">
                  <div class="card-text mb-20 grey-800">

                    <div class="row">
                      <div class="col-md-5 wall-attach">
                        <?=display_imageContainer(rand(3,30))?>
                      </div>
                      <div class="col-md-7">
                        <a href="#"><h4 class="m-0 mb-10">Photos Album-Name</h4></a>
                        Munere dictum dissentio dicturam mediocriterne honesta.
                      </div>
                    </div>
                  </div>

                  <ul class="wall-attrs clearfix p-0 m-0 mt-10">
                    <li class="attrs-meta float-left font-size-16">
                      <a href="javascript:void(0)" class="mr-20">
                        <i class="icon md-thumb-up"></i>
                        <span>5</span>
                      </a>
                      <a href="javascript:void(0)" onclick="$('#wall-comment-reply-1').toggle();">
                        <i class="icon md-comment"></i>
                        <span>2</span>
                      </a>
                    </li>
                    <li class="float-right">
                      <div class="btn-group bootstrap-select btn-comment-post">
                        <select data-plugin="selectpicker">
                          <option>Top Comments</option>
                          <option>Most Recent</option>
                        </select>
                      </div>
                    </li>
                  </ul>
                </div>

              </div>

              <div class="card card-shadow card-bordered post-wall">
                <div class="card-block clearfix p-25">
                  <div class="media-avatar">
                    <a href="#" class="avatar avatar-lg">
                      <img class="img-fluid" src="../../../global/portraits/<?=rand(1,20)?>.jpg">
                    </a>
                  </div>
                  <div class="media-body text-middle">
                    <h5 class="mt-0 mb-0">
                      <a href="#">Mallinda Hollaway</a> to <a href="#">Videos</a>
                    </h5>
                    <small>30th July 2017</small>
                  </div>
                </div>
                <div class="card-block px-25 pt-0 pb-0">
                  <div class="card-text mb-20 grey-800">

                    <div class="row">
                      <div class="col-md-5 wall-attach">
                        <div class="row">
                          <div class="col-md-12 col-sm-12 col-12">
                            <button type="button" class="btn btn-floating btn-sm btn-pyr">
                              <i class="icon md-play white" aria-hidden="true"></i>
                            </button>
                            <img width="100%" src="<?=get_img("H")?>" alt="...">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-7">
                        <a href="#"><h4 class="m-0 mb-10">Video-Name</h4></a>
                        Munere dictum dissentio dicturam mediocriterne honesta.
                      </div>
                    </div>
                  </div>

                  <ul class="wall-attrs clearfix p-0 m-0 mt-10">
                    <li class="attrs-meta float-left font-size-16">
                      <a href="javascript:void(0)" class="mr-20">
                        <i class="icon md-thumb-up"></i>
                        <span>5</span>
                      </a>
                      <a href="javascript:void(0)" onclick="$('#wall-comment-reply-1').toggle();">
                        <i class="icon md-comment"></i>
                        <span>2</span>
                      </a>
                    </li>
                    <li class="float-right">
                      <div class="btn-group bootstrap-select btn-comment-post">
                        <select data-plugin="selectpicker">
                          <option>Top Comments</option>
                          <option>Most Recent</option>
                        </select>
                      </div>
                    </li>
                  </ul>
                </div>

              </div>

              <div class="card card-shadow card-bordered post-wall">
                <div class="card-block clearfix p-25">
                  <div class="media-avatar">
                    <a href="#" class="avatar avatar-lg">
                      <img class="img-fluid" src="../../../global/portraits/<?=rand(1,20)?>.jpg">
                    </a>
                  </div>
                  <div class="media-body text-middle">
                    <h5 class="mt-0 mb-0">
                      <a href="#">Mallinda Hollaway</a> to <a href="#">Events</a>
                    </h5>
                    <small>30th July 2017</small>
                  </div>
                </div>
                <div class="card-block px-25 pt-0 pb-0">
                  <div class="card-text mb-20 grey-800">

                    <div class="row">
                      <div class="col-md-5 wall-attach">
                        <img width="100%" src="<?=get_img("R")?>" alt="...">
                      </div>
                      <div class="col-md-7">
                        <div class="card-block p-0">
                          <div class="card-actions card-actions-sidebar" style="margin-top:0;width: auto;">
                            <div class="text-danger font-size-18" style="margin-top:0;text-align:center;">May</div>
                            <div class="text-dark font-size-50 card-title" style="text-align: center; margin: 0px; line-height: 55px;">7</div>
                          </div>
                          <div class="card-content">
                            <a href="events-grid-detail.php">
                              <h4 class="card-title">Possumus fugiendum verborum</h4>
                            </a>
                            <p class="card-text type-link font-size-14">Sunday | 55 Warren Street</p>
                            <div class="row no-space bg-grey-100 p-5">
                              <div class="col-6">
                                <div class="counter">
                                  <span class="counter-number font-size-14">60</span>
                                  <div class="counter-label font-size-11">interested</div>
                                </div>
                              </div>
                              <div class="col-6">
                                <div class="counter">
                                  <span class="counter-number font-size-14">59</span>
                                  <div class="counter-label font-size-11">going</div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <ul class="wall-attrs clearfix p-0 m-0 mt-10">
                    <li class="attrs-meta float-left font-size-16">
                      <a href="javascript:void(0)" class="mr-20">
                        <i class="icon md-thumb-up"></i>
                        <span>5</span>
                      </a>
                      <a href="javascript:void(0)" onclick="$('#wall-comment-reply-1').toggle();">
                        <i class="icon md-comment"></i>
                        <span>2</span>
                      </a>
                    </li>
                    <li class="float-right">
                      <div class="btn-group bootstrap-select btn-comment-post">
                        <select data-plugin="selectpicker">
                          <option>Top Comments</option>
                          <option>Most Recent</option>
                        </select>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>


            </section>



        </div>
        <div class="col-md-5">
          <div class="panel-group mb-0" id="exampleAccordionDefault" aria-multiselectable="true" role="tablist">
            <div class="panel">
              <div class="panel-heading" id="exampleHeadingDefaultOne" role="tab">
                <a class="panel-title" data-toggle="collapse" href="#exampleCollapseDefaultOne" data-parent="#exampleAccordionDefault" aria-expanded="true" aria-controls="exampleCollapseDefaultOne">
                  <strong>News</strong>
                </a>
                <div class="panel-actions mr-10">
                  <div class="dropdown">
                    <a class="panel-action" data-toggle="dropdown" href="#" aria-expanded="true"><i class="icon md-settings" aria-hidden="true"></i></a>
                    <div class="dropdown-menu dropdown-menu-bullet dropdown-menu-right" role="menu">
                      <?php for($aa=1;$aa<5;$aa++){?>
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem"> Action</a>
                      <?php }?>
                    </div>
                  </div>
                </div>
              </div>

              <div class="panel-collapse in collapse show" id="exampleCollapseDefaultOne" aria-labelledby="exampleHeadingDefaultOne" ole="tabpanel">
                <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                  <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#exampleTopHome" aria-controls="exampleTopHome" role="tab" aria-expanded="true"> Recent</a></li>
                  <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopComponents" aria-controls="exampleTopComponents" role="tab" aria-expanded="false"> Update</a></li>
                  <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopCss" aria-controls="exampleTopCss" role="tab" aria-expanded="false"> Views</a></li>
                  <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopJavascript" aria-controls="exampleTopJavascript" role="tab" aria-expanded="false"> Like</a></li>
                  <li class="dropdown nav-item" style="display: none;">
                    <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#" aria-expanded="false" aria-haspopup="true">Dropdown </a>
                    <div class="dropdown-menu" role="menu">
                      <a class="dropdown-item" data-toggle="tab" href="#exampleTopHome" aria-controls="exampleTopHome" role="tab"> Recent</a>
                      <a class="dropdown-item" data-toggle="tab" href="#exampleTopComponents" aria-controls="exampleTopComponents" role="tab"> Update</a>
                      <a class="dropdown-item" data-toggle="tab" href="#exampleTopCss" aria-controls="exampleTopCss" role="tab"> Views</a>
                      <a class="dropdown-item" data-toggle="tab" href="#exampleTopJavascript" aria-controls="exampleTopJavascript" role="tab"> Like</a>
                    </div>
                  </li>
                </ul>
                <div class="panel-body">
                  <div class="tab-content">
                    <div class="tab-pane active" id="exampleTopHome" role="tabpanel" aria-expanded="true">
                      <ul class="list-group">
                        <?php for($aa=1;$aa<=5;$aa++){?>
                        <li class="list-group-item px-0">
                          <div class="media">
                            <div class="pr-20">
                              <a class="widget-news-img widget-wh-50" href="../apps-intranet/news-detail.php" target="_blank">
                                <img class="" src="<?=get_img('R')?>" alt="...">
                              </a>
                            </div>
                            <div class="media-body">
                              <a href="../apps-intranet/news-detail.php" target="_blank"><h5 class="mt-0 mb-0">News Recent example title</h5></a>
                              <small>30th July 2017</small>
                              <div>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus.
                              </div>
                            </div>
                          </div>
                        </li>
                        <?php }?>
                      </ul>
                    </div>
                    <div class="tab-pane" id="exampleTopComponents" role="tabpanel" aria-expanded="false">
                      <ul class="list-group">
                        <?php for($aa=1;$aa<=5;$aa++){?>
                        <li class="list-group-item px-0">
                          <div class="media">
                            <div class="pr-20">
                              <a class="widget-news-img widget-wh-50" href="../apps-intranet/news-detail.php" target="_blank">
                                <img class="" src="<?=get_img('R')?>" alt="...">
                              </a>
                            </div>
                            <div class="media-body">
                              <a href="../apps-intranet/news-detail.php" target="_blank"><h5 class="mt-0 mb-0">News Update example title</h5></a>
                              <small>30th July 2017</small>
                              <div>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus.
                              </div>
                            </div>
                          </div>
                        </li>
                        <?php }?>
                      </ul>
                    </div>
                    <div class="tab-pane" id="exampleTopCss" role="tabpanel" aria-expanded="false">
                      <ul class="list-group">
                        <?php for($aa=1;$aa<=5;$aa++){?>
                        <li class="list-group-item px-0">
                          <div class="media">
                            <div class="pr-20">
                              <a class="widget-news-img widget-wh-50" href="../apps-intranet/news-detail.php" target="_blank">
                                <img class="" src="<?=get_img('R')?>" alt="...">
                              </a>
                            </div>
                            <div class="media-body">
                              <a href="../apps-intranet/news-detail.php" target="_blank"><h5 class="mt-0 mb-0">News View example title</h5></a>
                              <small>30th July 2017</small>
                              <div>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus.
                              </div>
                            </div>
                          </div>
                        </li>
                        <?php }?>
                      </ul>
                    </div>
                    <div class="tab-pane" id="exampleTopJavascript" role="tabpanel" aria-expanded="false">
                      <ul class="list-group">
                        <?php for($aa=1;$aa<=5;$aa++){?>
                        <li class="list-group-item px-0">
                          <div class="media">
                            <div class="pr-20">
                              <a class="widget-news-img widget-wh-50" href="../apps-intranet/news-detail.php" target="_blank">
                                <img class="" src="<?=get_img('R')?>" alt="...">
                              </a>
                            </div>
                            <div class="media-body">
                              <a href="../apps-intranet/news-detail.php" target="_blank"><h5 class="mt-0 mb-0">News Like example title</h5></a>
                              <small>30th July 2017</small>
                              <div>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus.
                              </div>
                            </div>
                          </div>
                        </li>
                        <?php }?>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel">
              <div class="panel-heading" id="exampleHeadingDefaultOne" role="tab">
                <a class="panel-title" data-toggle="collapse" href="#exampleCollapseDefaultOne1" data-parent="#exampleAccordionDefault" aria-expanded="false" aria-controls="exampleCollapseDefaultOne1">
                  <strong>Photo</strong>
                </a>
                <div class="panel-actions mr-10">
                  <div class="dropdown">
                    <a class="panel-action" data-toggle="dropdown" href="#" aria-expanded="false"><i class="icon md-settings" aria-hidden="true"></i></a>
                    <div class="dropdown-menu dropdown-menu-bullet dropdown-menu-right" role="menu">
                      <?php for($aa=1;$aa<5;$aa++){?>
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem"> Action</a>
                      <?php }?>
                    </div>
                  </div>
                </div>
              </div>
              <div class="panel-collapse collapse in" id="exampleCollapseDefaultOne1" aria-labelledby="exampleHeadingDefaultOne1" role="tabpanel">
                <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                  <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#exampleTopHome1" aria-controls="exampleTopHome1" role="tab" aria-expanded="true"> Recent</a></li>
                  <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopComponents1" aria-controls="exampleTopComponents1" role="tab" aria-expanded="false"> Update</a></li>
                  <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopCss1" aria-controls="exampleTopCss1" role="tab" aria-expanded="false"> Views</a></li>
                  <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopJavascript1" aria-controls="exampleTopJavascript1" role="tab" aria-expanded="false"> Like</a></li>
                  <li class="dropdown nav-item" style="display: none;">
                    <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#" aria-expanded="false" aria-haspopup="true">Dropdown </a>
                    <div class="dropdown-menu" role="menu">
                      <a class="dropdown-item" data-toggle="tab" href="#exampleTopHome1" aria-controls="exampleTopHome1" role="tab"> Recent</a>
                      <a class="dropdown-item" data-toggle="tab" href="#exampleTopComponents1" aria-controls="exampleTopComponents1" role="tab"> Update</a>
                      <a class="dropdown-item" data-toggle="tab" href="#exampleTopCss1" aria-controls="exampleTopCss1" role="tab"> Views</a>
                      <a class="dropdown-item" data-toggle="tab" href="#exampleTopJavascript1" aria-controls="exampleTopJavascript1" role="tab"> Like</a>
                    </div>
                  </li>
                </ul>
                <div class="panel-body">
                  <div class="tab-content">
                    <div class="tab-pane active" id="exampleTopHome1" role="tabpanel" aria-expanded="true">
                      <ul class="list-group">
                        <?php for($aa=1;$aa<=3;$aa++){?>
                        <li class="list-group-item px-0">
                          <div class="media">
                            <div class="pr-20">
                              <a class="" href="javascript:void(0)" href="../apps-intranet/photos-detail.php" target="_blank">
                                  <div class="widget-news-img" style="width: 101px; height: 50px; margin-bottom: 1px;">
                                    <img class="" src="<?=get_img('R')?>" alt="...">
                                  </div>
                                  <div class="widget-news-img widget-wh-50 float-left" style="margin-right: 1px;">
                                    <img class="" src="<?=get_img('R')?>" alt="...">
                                  </div>
                                  <div class="widget-news-img widget-wh-50 float-left">
                                    <img class="" src="<?=get_img('R')?>" alt="...">
                                  </div>
                              </a>
                            </div>
                            <div class="media-body">
                              <a  href="../apps-intranet/photos-detail.php" target="_blank"><h5 class="mt-0 mb-0">Photo Recent example title</h5></a>
                              <small>30th July 2017</small>
                              <div>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus.
                              </div>
                            </div>
                          </div>
                        </li>
                        <?php }?>
                      </ul>
                    </div>
                    <div class="tab-pane" id="exampleTopComponents1" role="tabpanel" aria-expanded="false">
                      <ul class="list-group">
                        <?php for($aa=1;$aa<=3;$aa++){?>
                        <li class="list-group-item px-0">
                          <div class="media">
                            <div class="pr-20">
                              <a class="" href="javascript:void(0)"  href="../apps-intranet/photos-detail.php" target="_blank">
                                  <div class="widget-news-img" style="width: 101px; height: 50px; margin-bottom: 1px;">
                                    <img class="" src="<?=get_img('R')?>" alt="...">
                                  </div>
                                  <div class="widget-news-img widget-wh-50 float-left" style="margin-right: 1px;">
                                    <img class="" src="<?=get_img('R')?>" alt="...">
                                  </div>
                                  <div class="widget-news-img widget-wh-50 float-left">
                                    <img class="" src="<?=get_img('R')?>" alt="...">
                                  </div>
                              </a>
                            </div>
                            <div class="media-body">
                              <a  href="../apps-intranet/photos-detail.php" target="_blank"><h5 class="mt-0 mb-0">Photo Update example title</h5></a>
                              <small>30th July 2017</small>
                              <div>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus.
                              </div>
                            </div>
                          </div>
                        </li>
                        <?php }?>
                      </ul>
                    </div>
                    <div class="tab-pane" id="exampleTopCss1" role="tabpanel" aria-expanded="false">
                      <ul class="list-group">
                        <?php for($aa=1;$aa<=3;$aa++){?>
                        <li class="list-group-item px-0">
                          <div class="media">
                            <div class="pr-20">
                              <a class="" href="javascript:void(0)" href="../apps-intranet/photos-detail.php" target="_blank">
                                  <div class="widget-news-img" style="width: 101px; height: 50px; margin-bottom: 1px;">
                                    <img class="" src="<?=get_img('R')?>" alt="...">
                                  </div>
                                  <div class="widget-news-img widget-wh-50 float-left" style="margin-right: 1px;">
                                    <img class="" src="<?=get_img('R')?>" alt="...">
                                  </div>
                                  <div class="widget-news-img widget-wh-50 float-left">
                                    <img class="" src="<?=get_img('R')?>" alt="...">
                                  </div>
                              </a>
                            </div>
                            <div class="media-body">
                             <a href="../apps-intranet/photos-detail.php" target="_blank"> <h5 class="mt-0 mb-0">Photo Views example title</h5></a>
                              <small>30th July 2017</small>
                              <div>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus.
                              </div>
                            </div>
                          </div>
                        </li>
                        <?php }?>
                      </ul>
                    </div>
                    <div class="tab-pane" id="exampleTopJavascript1" role="tabpanel" aria-expanded="false">
                      <ul class="list-group">
                        <?php for($aa=1;$aa<=3;$aa++){?>
                        <li class="list-group-item px-0">
                          <div class="media">
                            <div class="pr-20">
                              <a class="" href="javascript:void(0)" href="../apps-intranet/photos-detail.php" target="_blank">
                                  <div class="widget-news-img" style="width: 101px; height: 50px; margin-bottom: 1px;">
                                    <img class="" src="<?=get_img('R')?>" alt="...">
                                  </div>
                                  <div class="widget-news-img widget-wh-50 float-left" style="margin-right: 1px;">
                                    <img class="" src="<?=get_img('R')?>" alt="...">
                                  </div>
                                  <div class="widget-news-img widget-wh-50 float-left">
                                    <img class="" src="<?=get_img('R')?>" alt="...">
                                  </div>
                              </a>
                            </div>
                            <div class="media-body">
                              <a  href="../apps-intranet/photos-detail.php" target="_blank"><h5 class="mt-0 mb-0">Photo Like example title</h5></a>
                              <small>30th July 2017</small>
                              <div>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus.
                              </div>
                            </div>
                          </div>
                        </li>
                        <?php }?>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="p-10">
            <a href="javascript:void(0);" class="mr-10">Add Widget</a>
            <span class="icon md-minus"></span>
            <a href="javascript:void(0);" class="ml-10">Soring</a>
          </div>

        </div>
      </div>


    </div>
  </div>
  <!-- End Page -->
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/imagesloaded/imagesloaded.pkgd.js"></script>
  <script src="../../global/vendor/masonry/masonry.pkgd.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/GridMenu.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../global/js/Plugin/masonry.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script>
  (function(document, window, $) {
  'use strict';
  var Site = window.Site;
  $(document).ready(function() {
    Site.run();

    function chooseDateRange(start, end) {
          var DrangeValue = start.format('MM/DD/YYYY') + '-' + end.format('MM/DD/YYYY');
          $('#SearchDateRange span#daterange-value').html(DrangeValue);
          var dataKey = "123";
          var dataValue = "xxx";
          var stoken = '';
            $('.search-wrapper .currently-showing .chooseDateRange').remove();
            stoken = '<div class="token chooseDateRange" data-key="'+dataKey+'" data-value="' + dataValue + '"><span class="token-label" >' + DrangeValue + '</span><a  href="#" class="close" tabindex="-1">×</a></div>';
            $('.search-wrapper .currently-showing').append(stoken);
            sysGenUrlSearch();
      }

      $('#SearchDateRange').daterangepicker({
          "autoApply": true,
          "opens": "center",
          ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
      }, chooseDateRange);
  });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>
