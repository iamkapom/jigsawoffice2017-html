<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../../assets/examples/css/apps/documents.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <style type="text/css">
  .slick-dots li button::before{font-size: 14px;}
  .btn-comment-post .btn-select{
    border: 0px !important;
  }
  .btn-comment-post, .btn-comment-post .bootstrap-select{
    width: 150px !important;
  }
  .fix-mini-nav + .page-content{
    padding-top: 0;
  }
  .fix-mini-nav .page-header{
    margin-bottom: 93px;
  }
  .fix-mini-nav .fix-mini-nav{
    margin-bottom: 0;
    margin-right: 0;
    margin-top: 0;
    padding: 0;
    position: fixed;
    top: 65px;
    width: 100%;
    z-index: 10;
    left: 0;
  }
  .btn-comment-post .btn-select{
    border: 0px !important;
  }
  .btn-comment-post, .btn-comment-post .bootstrap-select{
    width: 150px !important;
  }
  .page-content{
    padding: 0;
  }
  </style>
  <script src="../../../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition app-documents page-aside-left">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header">
      <h1 class="page-title mb-10">Getting Started
        <small>( 8 Articles )</small></h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item active">Apps</li>
        <li class="breadcrumb-item active"><a href="index.php">Intranet</a></li>
        <li class="breadcrumb-item active"><a href="policy-categories.php">Policy</a></li>
        <li class="breadcrumb-item active">Getting started</li>
      </ol>
      <div class="page-header-actions">
        <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Setting">
          <i class="icon md-settings" aria-hidden="true"></i>
        </button>
      </div>
    </div>
    <?php include("mini-nav.php");?>
    <div class="page-content bg-white" style="position: relative;">
      <div class="page-aside">
        <!-- Contacts Sidebar -->
        <div class="page-aside-switch">
          <i class="icon md-chevron-left" aria-hidden="true"></i>
          <i class="icon md-chevron-right" aria-hidden="true"></i>
        </div>
        <div class="page-aside-inner page-aside-scroll">
          <div data-role="container">
            <div data-role="content">
              <section class="page-aside-section">
                <h5 class="page-aside-title">Categories</h5>
                <div class="list-group">
                  <a class="list-group-item active" href="policy-articles.php">
                    <span class="list-group-item-content">Getting started</span>
                  </a>
                  <a class="list-group-item" href="policy-articles.php">
                    <span class="list-group-item-content">Divitias modis</span>
                  </a>
                  <a class="list-group-item" href="policy-articles.php">
                    <span class="list-group-item-content">Developer Tourials</span>
                  </a>
                  <a class="list-group-item" href="policy-articles.php">
                    <span class="list-group-item-content">Compatible Themes</span>
                  </a>
                  <a class="list-group-item" href="policy-articles.php">
                    <span class="list-group-item-content">Commodis clamat</span>
                  </a>
                  <a class="list-group-item" href="policy-articles.php">
                    <span class="list-group-item-content">Numeris imperio</span>
                  </a>
                  <a class="list-group-item" href="policy-articles.php">
                    <span class="list-group-item-content">Generis acutum</span>
                  </a>
                  <a class="list-group-item" href="policy-articles.php">
                    <span class="list-group-item-content">Distinctio arbitrium</span>
                  </a>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      
      <div class="page-main">
        <div class="page-header">
          <form class="mt-20" action="#" role="search">
            <div class="input-search input-search-dark">
              <input type="text" class="form-control w-full" placeholder="Search the knowledge base..." name="">
              <button type="submit" class="input-search-btn">
                <i class="icon md-search" aria-hidden="true"></i>
              </button>
            </div>
          </form>
        </div>
        <div class="documents-wrap articles" style="position:relative;">
          <ul class="blocks blocks-100 blocks-xxl-4 blocks-lg-3 blocks-sm-2" data-plugin="matchHeight">
            <li>
              <div class="articles-item">
                <i class="icon md-file" aria-hidden="true"></i>
                <h4 class="title"><a href="policy-article.php">Divitias modis theophrastus intuemur.</a></h4>
                <p>Iudico officia morati mortis ipsas bene navigandi nemini, convicia.</p>
              </div>
            </li>
            <li>
              <div class="articles-item">
                <i class="icon md-file" aria-hidden="true"></i>
                <h4 class="title"><a href="policy-article.php">Defensa depravatum amorem fodere.</a></h4>
                <p>Primo, munere existunt nominata succumbere magnum meminerunt epicuro
                  iactare.
                </p>
              </div>
            </li>
            <li>
              <div class="articles-item">
                <i class="icon md-file" aria-hidden="true"></i>
                <h4 class="title"><a href="policy-article.php">Placatae acute labore maiorem.</a></h4>
                <p>Tibi, importari libido referenda, hunc mediocris conversam civitas
                  venire.
                </p>
              </div>
            </li>
            <li>
              <div class="articles-item">
                <i class="icon md-file" aria-hidden="true"></i>
                <h4 class="title"><a href="policy-article.php">Inter eoque retinent andriam.</a></h4>
                <p>Propositum chremes quod magna securi sapiens. Impendere qualisque.
                  Affirmatis.
                </p>
              </div>
            </li>
            <li>
              <div class="articles-item">
                <i class="icon md-file" aria-hidden="true"></i>
                <h4 class="title"><a href="policy-article.php">Firmam praeterea totam finibus.</a></h4>
                <p>Percipiatur depulsa motu permulta tempus desistemus dicent, loquerer
                  turpe.
                </p>
              </div>
            </li>
            <li>
              <div class="articles-item">
                <i class="icon md-file" aria-hidden="true"></i>
                <h4 class="title"><a href="policy-article.php">Hanc cuius maiores satisfacit.</a></h4>
                <p>Appareat cuiquam senectutem verentur, exedunt acuti oportet ex afferre.
                </p>
              </div>
            </li>
            <li>
              <div class="articles-item">
                <i class="icon md-file" aria-hidden="true"></i>
                <h4 class="title"><a href="policy-article.php">Parta meis reprehensione bona.</a></h4>
                <p>Appareat cuiquam senectutem verentur, exedunt acuti oportet ex afferre.
                </p>
              </div>
            </li>
            <li>
              <div class="articles-item">
                <i class="icon md-file" aria-hidden="true"></i>
                <h4 class="title"><a href="policy-article.php">Generis acutum omittam deterritum.</a></h4>
                <p>Discordant aspernari praeda nullam fuissent nostris, antipatrum caecilii
                  nocet.</p>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../../../global/vendor/jquery/jquery.js"></script>
  <script src="../../../../global/vendor/tether/tether.js"></script>
  <script src="../../../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../../../global/vendor/animsition/animsition.js"></script>
  <script src="../../../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../../../global/vendor/intro-js/intro.js"></script>
  <script src="../../../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <!-- Scripts -->
  <script src="../../../../global/js/State.js"></script>
  <script src="../../../../global/js/Component.js"></script>
  <script src="../../../../global/js/Plugin.js"></script>
  <script src="../../../../global/js/Base.js"></script>
  <script src="../../../../global/js/Config.js"></script>
  <script src="../../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/GridMenu.js"></script>
  <script src="../../../assets/js/Section/Sidebar.js"></script>
  <script src="../../../assets/js/Section/PageAside.js"></script>
  <script src="../../../assets/js/Plugin/menu.js"></script>
  <script src="../../../../global/js/config/colors.js"></script>
  <script src="../../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../../assets');
  </script>
  <script src="../../../assets/js/Site.js"></script>
  <script src="../../../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../../../global/js/Plugin/switchery.js"></script>
  <script src="../../../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../../../global/js/Plugin/matchheight.js"></script>
  <script src="../../../assets/js/Site.js"></script>
  <script src="../../../assets/js/App/Documents.js"></script>
  <script src="../../../assets/examples/js/apps/documents.js"></script>
  <script>
  (function(document, window, $) {
  'use strict';
  var Site = window.Site;
  $(document).ready(function() {
    Site.run();
    var wrap = $('.page');
      $(window).scroll(function () {
        if ($(this).scrollTop() > 125) {
          wrap.addClass("fix-mini-nav");
        } else {
          wrap.removeClass("fix-mini-nav");
        }
      });
  });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>