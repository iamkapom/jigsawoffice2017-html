<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/summernote/summernote.css">
  <link rel="stylesheet" href="../../global/vendor/blueimp-file-upload/jquery.fileupload.css">
  <link rel="stylesheet" href="../../global/vendor/dropify/dropify.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition page-aside-right">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-10">News</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item active">Apps</li>
        <li class="breadcrumb-item active"><a href="index.php">Intranet</a></li>
        <li class="breadcrumb-item active">News</li>
      </ol>
      <?php include("mini-nav.php");?>
    </div>
    
    <div class="page-content container-fluid" style="position: relative;">
      
      <div class="row ml-0 mr-0">
        <div class="col-md-12">
          <div class="page-aside" style="border-right:0;">
        <!-- Contacts Sidebar -->
        <div class="page-aside-switch">
          <i class="icon md-chevron-left" aria-hidden="true"></i>
          <i class="icon md-chevron-right" aria-hidden="true"></i>
        </div>
        <div class="page-aside-inner page-aside-scroll">
          <div data-role="container">
            <div data-role="content">
              <section class="page-aside-section">
                <div class="panel">
                  <div class="panel-heading">
                    <h3 class="panel-title pl-20">Information</h3>
                  </div>
                  <div class="panel-body p-20">
                      <div class="mb-20">
                        <div class="grey-500">Stutus</div>
                        <span>Public</span>
                      </div>
                      <div class="mb-20">
                        <div class="grey-500">Created</div>
                        <span>May 5 at 11:00 AM</span>
                      </div>
                      <div class="mb-20">
                        <div class="grey-500">Created by</div>
                        <span><img style="width:20px;" class="avatar" src="../../global/portraits/1.jpg" title="Herman Beck"> Herman Beck
                        </span>
                      </div>
                      <div class="mb-20">
                        <div class="grey-500">Last Updated</div>
                        <span>May 15 at 11:00 AM</span>
                      </div>
                      <div>
                        <div class="grey-500">Update by</div>
                        <span><img style="width:20px;" class="avatar" src="../../global/portraits/1.jpg" title="Herman Beck"> Herman Beck
                        </span>
                      </div>

                  </div>
                  
                </div>
                
                <div class="panel">
                  <div class="panel-heading">
                    <h3 class="panel-title pl-20">Tags</h3>
                  </div>
                  <div class="panel-body p-20">
                      <span class="badge badge-round badge-warning">Primary</span>
                    <span class="badge badge-round badge-warning">Success</span>
                    <span class="badge badge-round badge-warning">Warning</span>
                  </div>
                </div>

                <div class="panel">
                  <div class="panel-heading">
                    <h3 class="panel-title pl-20">Activities</h3>
                  </div>
                  <div class="panel-body p-20">
                      <ul class="timeline timeline-single">
                        <li class="timeline-item mb-30 pl-20">
                          <div class="timeline-dot" rip-style-bordercolor-backup="" style="" rip-style-borderstyle-backup="" rip-style-borderwidth-backup=""></div>
                          <div class="timeline-content">
                            <div class="media">
                              <div class="pr-10">
                                <a class="avatar avatar-sm" href="javascript:void(0)">
                                  <img class="img-fluid" src="../../../global/portraits/5.jpg" alt="...">
                                </a>
                              </div>
                              <div class="media-body">
                                <h5 class="mt-0 mb-0">June Lane</h5>
                                <small>16 minutes ago</small>
                                <div class="profile-brief">
                                  Update Status
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                        <li class="timeline-item mb-30 pl-20">
                          <div class="timeline-dot"></div>
                          <div class="timeline-content">
                            <div class="media">
                              <div class="pr-10">
                                <a class="avatar avatar-sm" href="javascript:void(0)">
                                  <img class="img-fluid" src="../../../global/portraits/5.jpg" alt="...">
                                </a>
                              </div>
                              <div class="media-body">
                                <h5 class="mt-0 mb-0">June Lane</h5>
                                <small>16 minutes ago</small>
                                <div class="profile-brief">
                                  Edit Content
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                      </ul>
                  </div>
                </div>

                <div class="card card-block pt-0 mb-0">
                      <h3 class="mt-0">This article helpful</h3>
                        <div class="pt-20 pb-20">
                          <div class="card card-block p-10 mb-0">
                            <div class="counter counter-lg">
                              <span class="counter-number"><i class="icon md-mood" aria-hidden="true"></i> 9.5</span>
                              <div class="counter-label text-uppercase">Happiness</div>
                            </div>
                          </div>
                          <div class="pt-20 pb-0">
                            <div id="chart_over" style="width: 100%; height: 200px;"></div>
                          </div>

                        </div>
                    </div>
              </section>
              
            </div>
          </div>
        </div>
      </div>
          <div class="page-main">
          <div class="panel">
            <div class="panel-body container-fluid">
              <div class="row">
                <div class="col-md-12">
                  <!-- Example Basic Form -->
                  <div class="example-wrap">
                    <div class="example">
                      <form autocomplete="off">
                        <div class="row">
                          <div class="form-group form-material col-md-12">
                            <h1>What Is SEO / Search Engine Optimization?</h1>
                          </div>
                        </div>
                        <div class="form-group form-material">
                          <div id="summernote" data-plugin="summernote">
                            <h2>It's Simpler Than You Think!</h2>
  SEO or Search Engine Optimisation is the name given to activity that attempts to improve search engine rankings.
  In search results Google™ displays links to pages it considers relevant and authoritative. Authority is mostly measured by analysing the number and quality of links from other web pages.
  In simple terms your web pages have the potential to rank in Google™ so long as other web pages link to them.
                              <div class="mt-10">
                                    <div class="row">
                              <div class="col-sm-5"><img src="https://www.redevolution.com/images/what_we_do/seo/seo-aberdeen.png" alt="SEO Aberdeen" class="img-responsive" style="display: block; margin-left: auto; margin-right: auto;" title="SEO Aberdeen"></div>
                              <div class="col-sm-7">
                              <h2>Make Your Site Appear in Google&trade;</h2>
                              <p>Great Content encourages people to link to your pages and shows Google&trade; your pages are interesting and authoritative. This leads to search engine success because Google&trade; wants to show interesting and authoritative pages in its search results. It's that <em>simple!</em></p>
                              <p>Check our <a href="/seo-explained">SEO Explained in Pictures</a> page for a quick, simple overview. It's guaranteed to help you understand SEO.</p>
                              </div>
                              </div>
                              <div class="row">
                              <div class="col-sm-7">
                              <h2>How Does Google&trade; Rank Pages?</h2>
                              <p>Google&trade; promotes authority pages to the top of its rankings so it's your job to create pages that become authority pages. This involves writing content people find useful because useful content is shared in blogs, twitter feeds etc., and over time Google&trade; picks up on these authority signals. This virtuous circle creates strong and sustainable Google&trade; rankings.</p>
                              </div>
                              <div class="col-sm-5"><img src="https://www.redevolution.com/images/what_we_do/seo/google-ranks.png" alt="Google Ranks" class="img-responsive" style="display: block; margin-left: auto; margin-right: auto;" title="Google Ranks"></div>
                              </div>
                              
                              
                              </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <!-- End Example Basic Form -->
                </div>
              </div>

              <section>
                <h3>Documents/Photos/Media</h3>
                <table class="table table-hover example-wrap">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th width="20%">Download</th>
                      <th class="hidden-sm-down" width="20%">Created</th>
                      <th class="hidden-sm-down" width="20%">File size</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <div class="media" style="flex-direction:initial;">
                          <div class="pr-20">
                            <a data-url="panel-file.html" data-toggle="slidePanel" class="font-size-24" href="javascript:void(0)">
                              <i style="color:#ff9800" class="icon fa-file-powerpoint-o" aria-hidden="true"></i>
                            </a>
                          </div>
                          <div class="media-body pt-10">
                            <h5><a style="color:#616161;text-decoration: none;" data-url="panel-folder.html" data-toggle="slidePanel" href="javascript:void(0)">TOR_Checklist</a></h5>
                            <p class="hidden-md-up mb-0 grey-500">
                              <small>
                                Mar 3, 2017 <span class="ml-10 mr-10">&mdash;</span> 2.85 MB                            </small>
                            </p>
                          </div>
                        </div>
                      </td>
                      <td class="pt-15">12</td>
                      <td class="hidden-sm-down pt-15">Mar 3, 2017</td>
                      <td class="hidden-sm-down pt-15">2.85 MB</td>
                    </tr>
                    <tr>
                      <td>
                        <div class="media" style="flex-direction:initial;">
                          <div class="pr-20">
                            <a data-url="panel-file.html" data-toggle="slidePanel" class="font-size-24" href="javascript:void(0)">
                              <i style="color:#f44336" class="icon fa-file-pdf-o" aria-hidden="true"></i>
                            </a>
                          </div>
                          <div class="media-body pt-10">
                            <h5><a style="color:#616161;text-decoration: none;" data-url="panel-folder.html" data-toggle="slidePanel" href="javascript:void(0)">wireframe inner</a></h5>
                            <p class="hidden-md-up mb-0 grey-500">
                              <small>
                                Mar 3, 2017 <span class="ml-10 mr-10">&mdash;</span> 4.26 KB                            </small>
                            </p>
                          </div>
                        </div>
                      </td>
                      <td class="pt-15">5</td>
                      <td class="hidden-sm-down pt-15">Mar 3, 2017</td>
                      <td class="hidden-sm-down pt-15">4.26 KB</td>
                    </tr>
                                      <tr>
                      <td>
                        <div class="media" style="flex-direction:initial;">
                          <div class="pr-20">
                            <a data-url="panel-file.html" data-toggle="slidePanel" class="font-size-24" href="javascript:void(0)">
                              <i style="color:#4caf50" class="icon fa-file-excel-o" aria-hidden="true"></i>
                            </a>
                          </div>
                          <div class="media-body pt-10">
                            <h5><a style="color:#616161;text-decoration: none;" data-url="panel-folder.html" data-toggle="slidePanel" href="javascript:void(0)">เอกสารรวบรวม Data</a></h5>
                            <p class="hidden-md-up mb-0 grey-500">
                              <small>
                                Mar 3, 2017 <span class="ml-10 mr-10">&mdash;</span> 1.52 MB                            </small>
                            </p>
                          </div>
                        </div>
                      </td>
                      <td class="pt-15">32</td>
                      <td class="hidden-sm-down pt-15">Mar 3, 2017</td>
                      <td class="hidden-sm-down pt-15">1.52 MB</td>
                    </tr>
                     <tr>
                      <td>
                        <div class="media" style="flex-direction:initial;">
                          <div class="pr-20">
                            <a data-url="panel-file.html" data-toggle="slidePanel" class="font-size-24" href="javascript:void(0)">
                              <i style="color:#ffab00" class="icon fa-file-picture-o" aria-hidden="true"></i>
                            </a>
                          </div>
                          <div class="media-body pt-10">
                            <h5><a style="color:#616161;text-decoration: none;" data-url="panel-folder.html" data-toggle="slidePanel" href="javascript:void(0)">WEB Design Trend</a></h5>
                            <p class="hidden-md-up mb-0 grey-500">
                              <small>
                                Mar 3, 2017 <span class="ml-10 mr-10">&mdash;</span> 4.25 MB                            </small>
                            </p>
                          </div>
                        </div>
                      </td>
                      <td class="pt-15">8</td>
                      <td class="hidden-sm-down pt-15">Mar 3, 2017</td>
                      <td class="hidden-sm-down pt-15">4.25 MB</td>
                    </tr>
                    </tbody>
                </table>
              </section>

              <div class="p-30 bg-grey-200 example-wrap">
                <h3 class="float-left m-0">Was this article helpful to you ?</h3>
                <div class="float-right">
                  <button type="button" class="btn btn-primary mr-10">Yes</button>
                  <button type="button" class="btn btn-primary">No</button>
                </div>
                <div class="clearfix"></div>
              </div>
              
              <ul class="wall-attrs clearfix p-0 m-0 mt-10">
                  <li class="attrs-meta float-left font-size-16">
                    <a href="javascript:void(0)" class="mr-20">
                      <i class="icon md-thumb-up"></i>
                      <span>5</span>
                    </a>
                    <a href="javascript:void(0)" onclick="$('#wall-comment-reply-1').toggle();">
                      <i class="icon md-comment"></i>
                      <span>2</span>
                    </a>
                  </li>
                  <li class="float-right">
                    <div class="btn-group bootstrap-select btn-comment-post">
                      <select data-plugin="selectpicker">
                        <option>Top Comments</option>
                        <option>Newest</option>
                        <option>Oldest</option>
                      </select>
                    </div>
                  </li>
                </ul>
            </div>
          </div>
          
          <div class="panel">
            <div class="panel-body container-fluid">
              <h3>Comments</h3>
              <div class="wall-comment-attrs">
                <div id="wall-comment-reply-1" class="wall-comment-reply clearfix" style="display: none">
                  <div href="#" class="avatar avatar-md float-left">
                    <img src="../../../global/portraits/6.jpg">
                  </div>
                  <div class="ml-60 wall-comment-form">
                    <textarea rows="1" class="autoHeight" placeholder="Write a comment..."></textarea>
                    <div class="wall-comment-reply-attach">
                      <button class="btn btn-icon btn-dark btn-round bg-grey-400">
                        <i class="icon md-attachment-alt rotate" aria-hidden="true"></i>
                      </button>
                    </div>
                  </div>
                </div>
                <div class="wall-comment">
                  <div class="wall-comment-action">
                  <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                    <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                  </button>
                  
                  <div class="dropdown-menu dropdown-menu-right" role="menu">
                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                      Edit Comment
                    </a>
                    <a class="dropdown-item text-danger" href="javascript:void(0)" role="menuitem">
                      Delete
                    </a>
                  </div>
                </div>
                  <a href="#" class="avatar avatar-md float-left">
                    <img src="../../../global/portraits/3.jpg">
                  </a>
                  <div class="ml-60 box-post-comment">
                    <a href="#">Stacey Hunt</a>
                    <span class="ml-10">Do incididunt elit ex incididunt ut. Aute velit proident cupidatat qui et consectetur
                      do. Consequat esse quis ut cupidatat ea. Sint dolore ea culpa
                      dolore velit enim.</span>
                      <p class="font-size-12 mt-5 grey-600">
                        <a href="javascript:void(0)">
                          Like
                        </a>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <a href="javascript:void(0)" class="action-reply">
                          Reply
                        </a>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <span >
                          <i class="icon md-favorite"></i>
                          <span>5</span>
                        </span>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <span>
                          30th July 2017
                        </span>
                      </p>

                    <div class="wall-comment">
                      <a href="#" class="avatar avatar-md float-left">
                        <img src="../../../global/portraits/11.jpg">
                      </a>
                      <div class="ml-60">
                        <a href="#">Crystal Bates</a>
                        <span class="ml-10">Do incididunt elit ex incididunt ut. Aute velit proident cupidatat qui et consectetur
                          do. Consequat esse quis ut cupidatat ea.</span>
                        <p class="font-size-12 mt-5 grey-500">
                          <a href="javascript:void(0)">
                            Like
                          </a>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <a href="javascript:void(0)" class="action-reply">
                            Reply
                          </a>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <span >
                            <i class="icon md-favorite"></i>
                            <span>5</span>
                          </span>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <span>
                            30th July 2017
                          </span>
                        </p>
                      </div>
                      
                    </div>
                    <div class="display-reply"></div>

                  </div>
                  
                </div>
                <div class="wall-comment">
                  <a href="#" class="avatar avatar-md float-left">
                    <img src="../../../global/portraits/5.jpg">
                  </a>
                  <div class="ml-60 box-post-comment">
                    <a href="#">Sam Anderson</a>
                    <span class="ml-10">Ut velit ipsum elit ut mollit elit. Proident officia eu ex consectetur ullamco
                      magna. Enim cillum voluptate sint ipsum ad voluptate exercitation.
                      Ex est amet magna occaecat eu.</span>
                    <p class="font-size-12 mt-5 grey-500">
                      <a href="javascript:void(0)">
                        Like
                      </a>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <a href="javascript:void(0)" class="action-reply">
                        Reply
                      </a>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <span >
                        <i class="icon md-favorite"></i>
                        <span>5</span>
                      </span>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <span>
                        30th July 2017
                      </span>
                    </p>

                    <div class="display-reply"></div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
          
          <div class="panel">
            <div class="panel-body container-fluid">
              <h3>See Also</h3>
              <div id="home-announcement" class="row">
                    <?php
                    $arr_detail[0]="Dolemus late utriusque fore eveniet provincia spernat dissentiet. Fit intemperantes.";
                    $arr_detail[1]="Geometria eae servare dicat, amicitiam, dixissem principio coniuncta adhibuit quantumcumque placeat easque, fidem gloriatur statuat perspecta ferantur greges propemodum quieti, turpius vituperatoribus complectitur leguntur vidisse assiduitas insolens audeam urbes futuris.";
                    $arr_detail[2]="Munere dictum dissentio dicturam mediocriterne honesta, morbi delectus rationibus periculum opinor propterea intuemur poetarum efficeretur interpretaris, labefactant aeternum reformidans.";
                    $arr_detail[3]="Adiungimus acutum iudicatum aliud aegritudinem, tritani ignavi incidant quaeritur transferrem loqueretur delectatio, appetere, eruditi. Eamque quaeso diuturnum. Atomis quietae quamquam cadere arbitrantur magnam referuntur utramque aristippi, filium eidola, iudicat veniamus, noctesque invenerit, alia factorum aristoteli phaedrum parta quicquid morbi animadversionem.";
                    $arr_detail[4]="Geometria eae servare dicat, amicitiam, dixissem principio coniuncta adhibuit quantumcumque placeat easque, fidem gloriatur statuat perspecta ferantur greges propemodum quieti, turpius vituperatoribus complectitur leguntur vidisse assiduitas insolens audeam urbes futuris.";

                    for($a=1;$a<=4;$a++){?>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-6 <?=($a>2?"hidden-sm-down":"")?> <?=($a>3?"hidden-lg-down":"")?>  mt-20">
                      <div class="relate-img-cover">
                        <img class="cover-image" src="<?=get_img('R')?>" alt="..." />
                      </div>
                      <h4 class="card-title mb-0 mt-10">
                        <a href="#" onclick="javascript:void(0);">Announcement : example title<?=$a?></a>
                      </h4>
                      <p class="card-text type-link mb-5">
                        <small>
                          By
                          <a href="javascript:void(0)">Nathan Watts</a>
                          <a href="javascript:void(0)">05, 2017</a>
                        </small>
                      </p>
                      <p class="card-text ellipsis" style="height:50px;"><?=$arr_detail[$a]?>
                      <a href="#" onclick="javascript:void(0);" title="read more" class="readmore">Read more</a></p>
                      <div>
                        <span class="badge badge-round badge-warning">Primary</span>
                        <span class="badge badge-round badge-warning">Success</span>
                        <span class="badge badge-round badge-warning">Warning</span>
                      </div>
                      <div class="clearfix">
                        <div class="card-actions float-left font-size-12 mt-5">
                          <a href="javascript:void(0)">
                            <i class="icon md-favorite"></i>
                            <span rip-style-bordercolor-backup="" style="" rip-style-borderstyle-backup="" rip-style-borderwidth-backup="">253</span>
                          </a>
                          <a href="javascript:void(0)">
                            <i class="icon md-comment"></i>
                            <span>115</span>
                          </a>
                        </div>
                      </div>
                    </div>
                    <?php }?>
                  </div>
              
            </div>
          </div>
        
        </div>
          


        </div>
      </div>
    </div>
  </div>
  <div class="site-action" data-plugin="actionBtn">
    <button type="button" onclick="location.href='news-form.php';" class="btn-raised btn btn-success btn-floating">
      <i class="front-icon md-plus" aria-hidden="true"></i>
    </button>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/imagesloaded/imagesloaded.pkgd.js"></script>
  <script src="../../global/vendor/jquery.dotdotdot/jquery.dotdotdot.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>

  <script src="../../global/vendor/amcharts/amcharts.js" type="text/javascript"></script>
  <script src="../../global/vendor/amcharts/serial.js" type="text/javascript"></script>
  <script src="../../global/vendor/amcharts/pie.js" type="text/javascript"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
      $(".ellipsis").dotdotdot({
        after: "a.readmore"
      });
    });
  })(document, window, jQuery);
  </script>
  <script type="text/javascript">
    
    var _ssource;
    var chartData_ssource = [
        {
            "title": "Yes",
            "value": 107,
            "color": "#00bcd4"
        },
        {
            "title": "No",
            "value": 5,
            "color": "#ff9800"
        }
    ];

    AmCharts.ready(function () {
        // PIE CHART
        _ssource = new AmCharts.AmPieChart();
        _ssource.dataProvider = chartData_ssource;
        _ssource.titleField = "title";
        _ssource.valueField = "value";
        _ssource.colorField = "color";
        _ssource.radius = "30%";
        _ssource.innerRadius = "40%";
        _ssource.labelRadius = 5;
        _ssource.labelText = "[[title]]";

        // WRITE
        _ssource.write("chart_over");
    });
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>