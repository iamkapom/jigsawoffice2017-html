<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../assets/examples/css/advanced/masonry.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <style type="text/css">
  .slick-dots li button::before{font-size: 14px;}
  .btn-comment-post .btn-select{
    border: 0px !important;
  }
  .btn-comment-post, .btn-comment-post .bootstrap-select{
    width: 150px !important;
  }
  .fix-mini-nav + .page-content{
    padding-top: 0;
  }
  .fix-mini-nav .page-header{
    margin-bottom: 93px;
  }
  .fix-mini-nav .fix-mini-nav{
    margin-bottom: 0;
    margin-right: 0;
    margin-top: 0;
    padding: 0;
    position: fixed;
    top: 65px;
    width: 100%;
    z-index: 10;
    left: 0;
  }
  .btn-comment-post .btn-select{
    border: 0px !important;
  }
  .btn-comment-post, .btn-comment-post .bootstrap-select{
    width: 150px !important;
  }
  </style>
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition page-aside-left">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header">
      <h1 class="page-title mb-10">Possumus fugiendum verborum</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item active">Apps</li>
        <li class="breadcrumb-item active"><a href="index.php">Intranet</a></li>
        <li class="breadcrumb-item active"><a href="events-grid.php">Events</a></li>
        <li class="breadcrumb-item active">Possumus fugiendum verborum</li>
      </ol>
      <div class="page-header-actions">
        <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Setting">
          <i class="icon md-settings" aria-hidden="true"></i>
        </button>
      </div>
    </div>
    <?php include("mini-nav.php");?>
    <div class="page-content container-fluid bg-white" style="position: relative;">
      <div class="page-aside">
        <!-- Contacts Sidebar -->
        <div class="page-aside-switch">
          <i class="icon md-chevron-left" aria-hidden="true"></i>
          <i class="icon md-chevron-right" aria-hidden="true"></i>
        </div>
        <div class="page-aside-inner page-aside-scroll">
          <div data-role="container">
            <div data-role="content">
              <section class="page-aside-section">
                <h5 class="page-aside-title">Events</h5>
                <div class="list-group">
                  <a class="list-group-item" data-toggle="tab" href="#category-2" aria-controls="category-2"
                    role="tab">
                    <span class="list-group-item-content">Pass Events</span>
                  </a>
                  <a class="list-group-item" data-toggle="tab" href="#category-2" aria-controls="category-2"
                    role="tab">
                    <span class="list-group-item-content">Today</span>
                  </a>
                  <a class="list-group-item" data-toggle="tab" href="#category-2" aria-controls="category-2"
                    role="tab">
                    <span class="list-group-item-content">Tomorrow</span>
                  </a>
                  <a class="list-group-item active" data-toggle="tab" href="#category-3" aria-controls="category-3"
                    role="tab">
                    <span class="list-group-item-content">This Week</span>
                  </a>
                  <a class="list-group-item" data-toggle="tab" href="#category-3" aria-controls="category-3"
                    role="tab">
                    <span class="list-group-item-content">Next Week</span>
                  </a>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      <div class="page-main">
        <div class="card card-shadow pt-20">
          <div class="card-header cover">
            <img class="cover-image" src="../../assets/examples/images/login.jpg" alt="...">
          </div>
          <div class="card-block">
            <div class="card-actions card-actions-sidebar" style="margin-top:0;">
              <div class="text-danger font-size-18" style="margin-top:0;text-align:center;">May</div>
              <div class="text-dark font-size-50 card-title" style="text-align: center; margin: 0px; line-height: 55px;">7</div>
            </div>
            <div class="card-content">
              <h3 class="card-title">Possumus fugiendum verborum</h3>
              <div class="row">
                <div class="col-md-6">
                  <p class="card-text type-link">
                    <small>
                      By
                      <a href="javascript:void(0)">Nathan Watts</a>
                      <a href="javascript:void(0)">05, 2017</a>
                    </small>
                  </p>
                  <ul class="list-group list-group-full">
                    <li class="list-group-item">
                      <i class="icon fa-clock-o text-warning" aria-hidden="true"></i>
                      Sunday | 11:18 am
                    </li>
                    <li class="list-group-item">
                      <i class="icon fa-map-marker text-dark" aria-hidden="true"></i>
                      55 Warren Street, New York, NY 10007, USA
                    </li>
                  </ul>
                  <div class="clearfix">
                    <div class="btn-group" aria-label="Basic example" role="group">
                      <button type="button" class="btn btn-warning waves-effect waves-classic"><i class="icon fa-star" aria-hidden="true"></i> Interested</button>
                      <button type="button" class="btn btn-success waves-effect waves-classic"><i class="icon fa-check" aria-hidden="true"></i>Going</button>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="panel" style="border: none;box-shadow: none;">
                    <div class="panel-heading">
                      <h3 class="panel-title pt-5">2.7K Going · 5.5K Interested
                        <small><a href="#" class="panel-actions panel-actions-keep">Sell All</a></small>
                      </h3>
                    </div>
                    <div class="panel-body">
                      <ul class="list-unstyled list-inline">
                        <?php for($a=7;$a<=16;$a++){?>
                        <li class="list-inline-item">
                          <a class="avatar avatar-m m-5" href="javascript:void(0)">
                            <img src="../../global/portraits/<?php echo $a;?>.jpg" alt="">
                          </a>
                        </li>
                        <?php }?>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              
            </div>
          </div>

          <div class="nav-tabs-horizontal" data-plugin="tabs">
            <ul class="nav nav-tabs nav-tabs-line" role="tablist">
              <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" href="#exampleTabsLineOne" aria-controls="exampleTabsLineOne" role="tab" aria-expanded="true">Information</a></li>
              <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleTabsLineTwo" aria-controls="exampleTabsLineTwo" role="tab" aria-expanded="false">Comments</a></li>
              <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleTabsLineThree" aria-controls="exampleTabsLineThree" role="tab">Documents</a></li>
            </ul>
          </div>
        </div>
        
        <div class="">
          
          <div class="tab-content">
            <div class="tab-pane active" id="exampleTabsLineOne" role="tabpanel" aria-expanded="true">
              <div class="card-block">
                <h5 class="card-title">
                  <i class="icon md-quote"></i>
                  <span>Summary</span>
                </h5>
                <p class="card-text">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                  incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                  occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                  mollit anim id est laborum.
                </p>
              </div>
            </div>
            <div class="tab-pane" id="exampleTabsLineTwo" role="tabpanel" aria-expanded="false">
              <section>
                  <div class="comment media" style="border-bottom:1px #eee solid;flex-direction:initial;">
                    <div class="pr-20">
                      <a class="avatar avatar-lg" href="javascript:void(0)">
                        <img src="../../global/portraits/1.jpg" alt="...">
                      </a>
                    </div>
                    <div class="media-body">
                      <div class="comment-body">
                        <a class="comment-author" href="javascript:void(0)">Caleb Richards</a>
                        <div class="comment-meta">
                          <span class="date">5 days ago</span>
                        </div>
                        <div class="comment-content">
                          <p>Elliot you are always so right :)</p>
                        </div>
                        <div class="comment-actions">
                          <a class="text-like icon md-favorite" href="javascript:void(0)" role="button"></a>
                          <a href="javascript:void(0)" role="button">Reply</a>
                        </div>
                      </div>
                      <div class="comment m-0 p-0">
                        <div class="comment media" style="border-top:1px #eee solid;flex-direction:initial;">
                          <div class="pr-20">
                            <a class="avatar avatar-lg" href="javascript:void(0)">
                              <img src="../../global/portraits/3.jpg" alt="...">
                            </a>
                          </div>
                          <div class="comment-body media-body">
                            <a class="comment-author" href="javascript:void(0)">Nathan Watts</a>
                            <div class="comment-meta">
                              <span class="date">5 days ago</span>
                            </div>
                            <div class="comment-content">
                              <p>Elliot you are always so right :)</p>
                            </div>
                            <div class="comment-actions">
                              <a class="text-like icon md-favorite" href="javascript:void(0)" role="button"></a>
                              <a href="javascript:void(0)" role="button">Reply</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="comment media" style="border-bottom:1px #eee solid;flex-direction:initial;">
                    <div class="pr-20">
                      <a class="avatar avatar-lg" href="javascript:void(0)">
                        <img src="../../global/portraits/11.jpg" alt="...">
                      </a>
                    </div>
                    <div class="media-body">
                      <div class="comment-body">
                        <a class="comment-author" href="javascript:void(0)">Crystal Bates</a>
                        <div class="comment-meta">
                          <span class="date">5 days ago</span>
                        </div>
                        <div class="comment-content">
                          <p>Dude, this is awesome. Thanks so much</p>
                        </div>
                        <div class="comment-actions">
                          <a class="text-like icon md-favorite" href="javascript:void(0)" role="button"></a>
                          <a href="javascript:void(0)" role="button">Reply</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="comment media" style="border-bottom:1px #eee solid;flex-direction:initial;">
                    <div class="pr-20">
                      <a class="avatar avatar-lg" href="javascript:void(0)">
                        <img src="../../global/portraits/6.jpg" alt="...">
                      </a>
                    </div>
                    <div class="media-body">
                      <div class="comment-body">
                        <a class="comment-author" href="javascript:void(0)">Crystal Bates</a>
                        <div class="comment-meta">
                          <span class="date">5 days ago</span>
                        </div>
                        <div class="comment-content">
                          <p>Yes, That's really awesome!</p>
                        </div>
                        <div class="comment-actions">
                          <a class="text-like icon md-favorite" href="javascript:void(0)" role="button"></a>
                          <a href="javascript:void(0)" role="button">Reply</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <form class="comment-reply" action="#" method="post">
                    <div class="form-group">
                      <textarea class="form-control" rows="5" placeholder="Comment here"></textarea>
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary waves-effect waves-classic">Comment</button>
                      <button type="button" class="btn btn-link grey-600 waves-effect waves-classic">close</button>
                    </div>
                  </form>
                </section>
            </div>
            <div class="tab-pane" id="exampleTabsLineThree" role="tabpanel">
              <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th width="20%">Owner</th>
                        <th class="hidden-sm-down" width="20%">Created</th>
                        <th class="hidden-sm-down" width="20%">File size</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          <div class="media" style="flex-direction:initial;">
                            <div class="pr-20">
                              <a data-url="panel-file.html" data-toggle="slidePanel" class="font-size-24" href="javascript:void(0)">
                                <i style="color:#ff9800" class="icon fa-file-powerpoint-o" aria-hidden="true"></i>
                              </a>
                            </div>
                            <div class="media-body">
                              <h5><a style="color:#616161;text-decoration: none;" data-url="panel-folder.html" data-toggle="slidePanel" href="javascript:void(0)">TOR_Checklist</a></h5>
                              <p class="hidden-md-up mb-0 grey-500">
                                <small>
                                  Mar 3, 2017 <span class="ml-10 mr-10">&mdash;</span> 2.85 MB                            </small>
                              </p>
                            </div>
                          </div>
                        </td>
                        <td class="pt-15"><img style="width:20px;" class="avatar" src="../../global/portraits/1.jpg" title="Herman Beck">
                        <span class="hidden-md-down">Herman Beck</span></td>
                        <td class="hidden-sm-down pt-15">Mar 3, 2017</td>
                        <td class="hidden-sm-down pt-15">2.85 MB</td>
                      </tr>
                      <tr>
                        <td>
                          <div class="media" style="flex-direction:initial;">
                            <div class="pr-20">
                              <a data-url="panel-file.html" data-toggle="slidePanel" class="font-size-24" href="javascript:void(0)">
                                <i style="color:#f44336" class="icon fa-file-pdf-o" aria-hidden="true"></i>
                              </a>
                            </div>
                            <div class="media-body">
                              <h5><a style="color:#616161;text-decoration: none;" data-url="panel-folder.html" data-toggle="slidePanel" href="javascript:void(0)">wireframe inner</a></h5>
                              <p class="hidden-md-up mb-0 grey-500">
                                <small>
                                  Mar 3, 2017 <span class="ml-10 mr-10">&mdash;</span> 4.26 KB                            </small>
                              </p>
                            </div>
                          </div>
                        </td>
                        <td class="pt-15">me</td>
                        <td class="hidden-sm-down pt-15">Mar 3, 2017</td>
                        <td class="hidden-sm-down pt-15">4.26 KB</td>
                      </tr>
                                        <tr>
                        <td>
                          <div class="media" style="flex-direction:initial;">
                            <div class="pr-20">
                              <a data-url="panel-file.html" data-toggle="slidePanel" class="font-size-24" href="javascript:void(0)">
                                <i style="color:#4caf50" class="icon fa-file-excel-o" aria-hidden="true"></i>
                              </a>
                            </div>
                            <div class="media-body">
                              <h5><a style="color:#616161;text-decoration: none;" data-url="panel-folder.html" data-toggle="slidePanel" href="javascript:void(0)">เอกสารรวบรวม Data</a></h5>
                              <p class="hidden-md-up mb-0 grey-500">
                                <small>
                                  Mar 3, 2017 <span class="ml-10 mr-10">&mdash;</span> 1.52 MB                            </small>
                              </p>
                            </div>
                          </div>
                        </td>
                        <td class="pt-15"><img style="width:20px;" class="avatar" src="../../global/portraits/1.jpg" title="Herman Beck">
                        <span class="hidden-md-down">Herman Beck</span></td>
                        <td class="hidden-sm-down pt-15">Mar 3, 2017</td>
                        <td class="hidden-sm-down pt-15">1.52 MB</td>
                      </tr>
                                        <tr>
                        <td>
                          <div class="media" style="flex-direction:initial;">
                            <div class="pr-20">
                              <a data-url="panel-file.html" data-toggle="slidePanel" class="font-size-24" href="javascript:void(0)">
                                <i style="color:#ffab00" class="icon fa-file-picture-o" aria-hidden="true"></i>
                              </a>
                            </div>
                            <div class="media-body">
                              <h5><a style="color:#616161;text-decoration: none;" data-url="panel-folder.html" data-toggle="slidePanel" href="javascript:void(0)">WEB Design Trend</a></h5>
                              <p class="hidden-md-up mb-0 grey-500">
                                <small>
                                  Mar 3, 2017 <span class="ml-10 mr-10">&mdash;</span> 4.25 MB                            </small>
                              </p>
                            </div>
                          </div>
                        </td>
                        <td class="pt-15">me</td>
                        <td class="hidden-sm-down pt-15">Mar 3, 2017</td>
                        <td class="hidden-sm-down pt-15">4.25 MB</td>
                      </tr>
                                      </tbody>
                  </table>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/imagesloaded/imagesloaded.pkgd.js"></script>
  <script src="../../global/vendor/masonry/masonry.pkgd.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/GridMenu.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/masonry.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
      var wrap = $('.page');
      $(window).scroll(function () {
        if ($(this).scrollTop() > 125) {
          wrap.addClass("fix-mini-nav");
        } else {
          wrap.removeClass("fix-mini-nav");
        }
      });
    });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>