<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <style type="text/css">
  .slick-dots li button::before{font-size: 14px;}
  .btn-comment-post .btn-select{
    border: 0px !important;
  }
  .btn-comment-post, .btn-comment-post .bootstrap-select{
    width: 150px !important;
  }
  .fix-mini-nav + .page-content{
    padding-top: 0;
  }
  .fix-mini-nav .page-header{
    margin-bottom: 93px;
  }
  .fix-mini-nav .fix-mini-nav{
    margin-bottom: 0;
    margin-right: 0;
    margin-top: 0;
    padding: 0;
    position: fixed;
    top: 65px;
    width: 100%;
    z-index: 10;
    left: 0;
  }
  .btn-comment-post .btn-select{
    border: 0px !important;
  }
  .btn-comment-post, .btn-comment-post .bootstrap-select{
    width: 150px !important;
  }
  .page-content{
    padding: 0;
  }
  </style>
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition app-documents page-aside-left">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header">
      <h1 class="page-title mb-10">Staff Directory</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item active">Apps</li>
        <li class="breadcrumb-item active"><a href="index.php">Intranet</a></li>
        <li class="breadcrumb-item active">Staff Directory</li>
      </ol>
      <div class="page-header-actions">
        <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Setting">
          <i class="icon md-settings" aria-hidden="true"></i>
        </button>
      </div>
    </div>
    <?php include("mini-nav.php");?>
    <div class="page-content container-fluid bg-white" style="position: relative;">
      <div class="page-aside">
        <!-- Contacts Sidebar -->
        <div class="page-aside-switch">
          <i class="icon md-chevron-left" aria-hidden="true"></i>
          <i class="icon md-chevron-right" aria-hidden="true"></i>
        </div>
        <div class="page-aside-inner page-aside-scroll">
          <div data-role="container">
            <div data-role="content">
              <section class="page-aside-section">
                <h5 class="page-aside-title">Department</h5>
                <div class="list-group">
                  <a class="list-group-item" data-toggle="tab" href="#category-2" aria-controls="category-2"
                    role="tab">
                    <span class="list-group-item-content">Human Resources <span class="badge badge-pill badge-dark">5</span></span>
                  </a>
                  <a class="list-group-item" data-toggle="tab" href="#category-3" aria-controls="category-3"
                    role="tab">
                    <span class="list-group-item-content">IT <span class="badge badge-pill badge-dark">44</span></span>
                  </a>
                  <a class="list-group-item" data-toggle="tab" href="#category-4" aria-controls="category-4"
                    role="tab">
                    <span class="list-group-item-content">MD<span class="badge badge-pill badge-dark">1</span></span>
                  </a>
                  <a class="list-group-item" data-toggle="tab" href="#category-4" aria-controls="category-4"
                    role="tab">
                    <span class="list-group-item-content">Sales <span class="badge badge-pill badge-dark">10</span></span>
                  </a>
                  <a class="list-group-item" data-toggle="tab" href="#category-4" aria-controls="category-4"
                    role="tab">
                    <span class="list-group-item-content">Accounting <span class="badge badge-pill badge-dark">2</span></span>
                  </a>
                  <a class="list-group-item" data-toggle="tab" href="#category-4" aria-controls="category-4"
                    role="tab">
                    <span class="list-group-item-content">Marketing <span class="badge badge-pill badge-dark">5</span></span>
                  </a>
                </div>
              </section>
              <section class="page-aside-section">
                <h5 class="page-aside-title">Offices</h5>
                <div class="list-group">
                  <a class="list-group-item" data-toggle="tab" href="#category-2" aria-controls="category-2"
                    role="tab">
                    <span class="list-group-item-content">Bangkok <span class="badge badge-pill badge-dark">34</span></span>
                  </a>
                  <a class="list-group-item" data-toggle="tab" href="#category-3" aria-controls="category-3"
                    role="tab">
                    <span class="list-group-item-content">New york <span class="badge badge-pill badge-dark">12</span></span>
                  </a>
                  <a class="list-group-item" data-toggle="tab" href="#category-4" aria-controls="category-4"
                    role="tab">
                    <span class="list-group-item-content">India <span class="badge badge-pill badge-dark">11</span></span>
                  </a>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      <div class="page-main">
        <div class="page-header">
          <form class="mt-20" action="#" role="search">
            <div class="input-search input-search-dark">
              <input type="text" class="form-control w-full" placeholder="Search..." name="">
              <button type="submit" class="input-search-btn">
                <i class="icon md-search" aria-hidden="true"></i>
              </button>
            </div>
          </form>
        </div>
        <div class="panel">
          <div class="panel-body container-fluid">
            <div class="row">
              <?php for($a=5;$a<11;$a++){?>
              <div class="col-md-6">
                <div class="card card-shadow card-bordered">
                  <div class="card-header bg-blue-500 white p-15 clearfix">
                    <a class="avatar avatar-lg float-left mr-20 img-bordered bg-white" href="javascript:void(0)">
                      <img src="../../../global/portraits/16.jpg" alt="">
                    </a>
                    <div class="font-size-18">Robin Ahrens</div>
                    <div class="grey-300 font-size-14">Web Designer</div>
                  </div>
                  <div class="card-block p-20">
                    <h4 class="text-info">Robin</h4>
                    <div class="page-aside-section" >
                      <p data-info-type="phone" class="mb-10 text-nowrap font-size-16">
                        <i class="icon fa-mobile mr-10"></i>
                        <span class="text-break">9192372533
                          <span>
                      </span></span></p>
                      <p data-info-type="phone" class="mb-10 text-nowrap font-size-20">
                        <i class="fa-phone mr-10"></i>
                        <span class="text-break">123
                          <span>
                      </span></span></p>
                    </div>
                    <div class="page-aside-section" >
                      <p data-info-type="email" class="mb-10 text-nowrap">
                        <i class="icon md-email mr-10"></i>
                        <span class="text-break">malinda.h@gmail.com
                          <span>
                      </span></span></p>
                      <p data-info-type="fb" class="mb-10 text-nowrap">
                        <i class="icon bd-facebook mr-10"></i>
                        <span class="text-break">malinda.hollaway
                          <span>
                      </span></span></p>
                      <p data-info-type="twitter" class="mb-10 text-nowrap">
                        <i class="icon bd-twitter mr-10"></i>
                        <span class="text-break">@malinda (twitter.com/malinda)
                          <span>
                      </span></span></p>
                    </div>
                  </div>
                  <div class="card-footer bg-grey-300 p-15 text-center">
                    <div class="row no-space">
                      <div class="col-6">
                        <a class="btn btn-info card-link waves-effect waves-classic" href="javascript:void(0)">
                          <i class="icon fa-universal-access" aria-hidden="true"></i> View Profile
                        </a>
                      </div>
                      <div class="col-6">
                        <a class="btn btn-primary card-link waves-effect waves-classic" href="javascript:void(0)">
                          <i class="icon md-comments" aria-hidden="true"></i> Messenger
                        </a>
                      </div>
                    </div>
                  </div>
                  
                  
                </div>
              </div>
              <?php }?>
              
            </div>
          </div>  
        </div>
      </div>
      
    </div>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../assets/js/App/Documents.js"></script>
  <script src="../../assets/examples/js/apps/documents.js"></script>
  <script>
  (function(document, window, $) {
  'use strict';
  var Site = window.Site;
  $(document).ready(function() {
    Site.run();
    var wrap = $('.page');
      $(window).scroll(function () {
        if ($(this).scrollTop() > 125) {
          wrap.addClass("fix-mini-nav");
        } else {
          wrap.removeClass("fix-mini-nav");
        }
      });
  });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>