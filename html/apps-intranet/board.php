<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <style type="text/css">
  .slick-dots li button::before{font-size: 14px;}
  .btn-comment-post .btn-select{
    border: 0px !important;
  }
  .btn-comment-post, .btn-comment-post .bootstrap-select{
    width: 150px !important;
  }
  .fix-mini-nav + .page-content{
    padding-top: 0;
  }
  .fix-mini-nav .page-header{
    margin-bottom: 93px;
  }
  .fix-mini-nav .fix-mini-nav{
    margin-bottom: 0;
    margin-right: 0;
    margin-top: 0;
    padding: 0;
    position: fixed;
    top: 65px;
    width: 100%;
    z-index: 10;
    left: 0;
  }
  .btn-comment-post .btn-select{
    border: 0px !important;
  }
  .btn-comment-post, .btn-comment-post .bootstrap-select{
    width: 150px !important;
  }
  .page-content{
    padding: 0;
  }
  </style>

</head>
<body class="animsition app-documents">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-10">About Company</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item active">Apps</li>
        <li class="breadcrumb-item active"><a href="index.php">Intranet</a></li>
        <li class="breadcrumb-item active">About Company</li>
      </ol>
<?php include("mini-nav.php");?>
    </div>

    <div class="page-content bg-white">
      <div class="p-20">
        <div class="panel-body container-fluid">
          <section>
            <div class="row">
              <div class="col-md-3">
                <div class="user-info card card-shadow text-center">
                  <div class="user-base card-block">
                    <a class="avatar img-bordered " style="width: 200px; height: 200px;" href="javascript:void(0)">
                      <img src="../../global/portraits/15.jpg" alt="...">
                    </a>
                    <h4 class="user-name">Mallinda Hollaway</h4>
                    <p class="user-job">Baord director</p>

                    <section>
                      <div class="row pt-10 pb-20 black">
                      <div class="col-md-12 text-left">
                        <p data-info-type="phone" class="mb-10 text-nowrap font-size-20">
                          <i class="icon fa-mobile mr-15"></i>
                          <span class="text-break">9192372533<span>
                        </span></span></p>
                        <p data-info-type="phone" class="mb-10 text-nowrap font-size-20">
                          <i class="icon fa-phone mr-10"></i>
                          <span class="text-break">0254312345<span>
                        </span></span></p>
                      </div>
                      <div class="col-md-12 text-left">
                        <p data-info-type="email" class="mb-10 text-nowrap font-size-14">
                          <i class="icon md-email mr-10"></i>
                          <span class="text-break">malinda.h@gmail.com<span>
                        </span></span></p>
                        <p data-info-type="fb" class="mb-10 text-nowrap font-size-14">
                          <i class="icon md-facebook mr-15"></i>
                          <span class="text-break">facebook.com/malinda.hollaway<span>
                        </span></span></p>
                        <p data-info-type="twitter" class="mb-10 text-nowrap font-size-14">
                          <i class="icon md-twitter mr-10"></i>
                          <span class="text-break">@malinda (twitter.com/malinda)<span>
                        </span></span></p>
                      </div>
                    </div>
                    </section>
                  </div>
                </div>
              </div>
              <div class="col-md-1"> </div>
              <div class="col-md-8">
                <div class="user-background card">
                  <div class="card-block">
                    <h3 class="card-title">
                      <i class="icon md-quote"></i>
                      <span>Summary</span>
                    </h3>
                    <p class="card-text">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                      incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                      commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                      velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                      occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                      mollit anim id est laborum.
                    </p>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                        <div class="card-block">
                          <h5 class="card-title">
                            <i class="icon md-badge-check"></i>
                            <span>Experience</span>
                          </h5>
                          <ul class="timeline timeline-single">
                            <li class="timeline-item mb-30">
                              <div class="timeline-dot"></div>
                              <div class="timeline-content">
                                <span class="block mb-5"><strong>2013 President</strong></span>
                                <span class="block font-size-18 mb-5">Co-founder, Chairman</span>
                                <span class="block mb-5">Company Name</span>
                              </div>
                            </li>
                            <li class="timeline-item mb-30">
                              <div class="timeline-dot"></div>
                              <div class="timeline-content">
                                <span class="block mb-5"><strong>2010 President</strong></span>
                                <span class="block font-size-18 mb-5">Co-founder, Chairman</span>
                                <span class="block mb-5">Company Name</span>
                              </div>
                            </li>
                            <li class="timeline-item mb-30">
                              <div class="timeline-dot"></div>
                              <div class="timeline-content">
                                <span class="block mb-5"><strong>2008 President</strong></span>
                                <span class="block font-size-18 mb-5">Co-founder, Chairman</span>
                                <span class="block mb-5">Company Name</span>
                              </div>
                            </li>
                            <li class="timeline-item mb-30">
                              <div class="timeline-dot"></div>
                              <div class="timeline-content">
                                <span class="block mb-5"><strong>2005 President</strong></span>
                                <span class="block font-size-18 mb-5">Co-founder, Chairman</span>
                                <span class="block mb-5">Company Name</span>
                              </div>
                            </li>
                          </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                      <div class="card-block">
                        <h5 class="card-title">
                          <i class="icon md-graduation-cap"></i>
                          <span>Edication</span>
                        </h5>
                        <ul class="timeline timeline-single">
                          <li class="timeline-item mb-30">
                            <div class="timeline-dot"></div>
                            <div class="timeline-content">
                              <span class="block font-size-15 mb-5">2006 President</span>
                              <span class="block mb-5">BS Computer Science</span>
                              <span class="block mb-5">Harvard University</span>
                            </div>
                          </li>
                          <li class="timeline-item mb-30">
                            <div class="timeline-dot"></div>
                            <div class="timeline-content">
                              <span class="block font-size-15 mb-5">2004 - 2005</span>
                              <span class="block mb-5">BS Computer Science</span>
                              <span class="block mb-5">Harvard University</span>
                            </div>
                          </li>
                          <li class="timeline-item mb-30">
                            <div class="timeline-dot"></div>
                            <div class="timeline-content">
                              <span class="block font-size-15 mb-5">2001 - 2003</span>
                              <span class="block mb-5">BS Computer Science</span>
                              <span class="block mb-5">Harvard University</span>
                            </div>
                          </li>
                          <li class="timeline-item mb-30">
                            <div class="timeline-dot"></div>
                            <div class="timeline-content">
                              <span class="block font-size-15 mb-5">1996 - 2000</span>
                              <span class="block mb-5">B.E</span>
                              <span class="block mb-5">Tsinghua University</span>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  
                  <div class="card-block">
                    <h5 class="card-title">
                      <i class="icon md-star"></i>
                      <span>More interest</span>
                    </h5>
                    <span class="badge mb-5 mr-5 badge-default">Movie</span>
                    <span class="badge mb-5 mr-5 badge-default">Games</span>
                    <span class="badge mb-5 mr-5 badge-primary">Music</span>
                    <span class="badge mb-5 mr-5 badge-warning">Pokemon go</span>
                    <span class="badge mb-5 mr-5 badge-success">Football</span>
                    <span class="badge mb-5 mr-5 badge-info">Tennis</span>
                    <span class="badge mb-5 mr-5 badge-danger">Hiking</span>
                    <span class="badge mb-5 mr-5 badge-dark">Skating</span>
                    <span class="badge mb-5 mr-5 badge-success">Table tennis</span>
                    <span class="badge mb-5 mr-5 badge-default">Reading</span>
                    <span class="badge mb-5 mr-5 badge-dark">Blogging/writing</span>
                    <span class="badge mb-5 mr-5 badge-warning">DIY</span>
                    <span class="badge mb-5 mr-5 badge-default">Singing</span>
                    <span class="badge mb-5 mr-5 badge-info">Dancing</span>
                  </div>
                </div>
              </div>
            </div>
          </section>
          
        </div>
      </div>


    </div>
  </div>
  <div class="site-action" data-plugin="actionBtn">
    <button type="button" class="btn-raised btn btn-success btn-floating">
      <i class="front-icon md-edit" aria-hidden="true"></i>
    </button>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../assets/js/App/Documents.js"></script>
  <script src="../../assets/examples/js/apps/documents.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();

    });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>
