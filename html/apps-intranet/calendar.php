<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/fullcalendar/fullcalendar.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-touchspin/bootstrap-touchspin.css">
  <link rel="stylesheet" href="../../global/vendor/jquery-selective/jquery-selective.css">
  <link rel="stylesheet" href="../../assets/examples/css/apps/calendar.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <style type="text/css">
  .slick-dots li button::before{font-size: 14px;}
  .btn-comment-post .btn-select{
    border: 0px !important;
  }
  .btn-comment-post, .btn-comment-post .bootstrap-select{
    width: 150px !important;
  }
  .fix-mini-nav + .page-content{
    padding-top: 0;
  }
  .fix-mini-nav .page-header{
    margin-bottom: 93px;
  }
  .fix-mini-nav .fix-mini-nav{
    margin-bottom: 0;
    margin-right: 0;
    margin-top: 0;
    padding: 0;
    position: fixed;
    top: 65px;
    width: 100%;
    z-index: 10;
    left: 0;
  }
  .btn-comment-post .btn-select{
    border: 0px !important;
  }
  .btn-comment-post, .btn-comment-post .bootstrap-select{
    width: 150px !important;
  }
  .page-content{
    padding: 0;
  }
  .app-calendar .page{
    margin-top: 0px;
  }
  </style>
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition app-calendar page-aside-left">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
      <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title">Calendar</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../home/">Home</a></li>
        <li class="breadcrumb-item">Calendar</li>
      </ol>
        <?php include("mini-nav.php");?>
    </div>

    <div class="page-content container-fluid bg-white" style="position: relative;">
      <div class="page-aside">
        <div class="page-aside-switch">
          <i class="icon md-chevron-left" aria-hidden="true"></i>
          <i class="icon md-chevron-right" aria-hidden="true"></i>
        </div>
        <div class="page-aside-inner page-aside-scroll">
          <div data-role="container">
            <div data-role="content">
              <section class="page-aside-section">
                <h5 class="page-aside-title">Calendars</h5>
                <div class="pl-30 pr-20">
                  <div class="checkbox-custom checkbox-primary">
                    <input id="inputChecked11" checked="" type="checkbox">
                    <label for="inputChecked11">Office Calendar</label>
                  </div>
                  <div class="checkbox-custom checkbox-primary">
                    <input id="inputChecked11" checked="" type="checkbox">
                    <label for="inputChecked11">Board Calendar</label>
                  </div>
                  <div class="checkbox-custom checkbox-primary">
                    <input id="inputChecked11" checked="" type="checkbox">
                    <label for="inputChecked11">Holiday Calendar</label>
                  </div>
                </div>
              </section>
              
            </div>
          </div>
        </div>
      </div>
      <div class="page-main">
        <div class="calendar-container">
          <div id="calendar"></div>
          <!--AddEvent Dialog -->
          <div class="modal fade" id="addNewEvent" aria-hidden="true" aria-labelledby="addNewEvent"
          role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple">
              <form class="modal-content form-horizontal" action="#" method="post" role="form">
                <div class="modal-header">
                  <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                  <h4 class="modal-title">New Event</h4>
                </div>
                <div class="modal-body">
                  <div class="form-group row">
                    <label class="col-md-2 form-control-label" for="ename">Name:</label>
                    <div class="col-md-10">
                      <input type="text" class="form-control" id="ename" name="ename">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-2 form-control-label" for="starts">Starts:</label>
                    <div class="col-md-10">
                      <div class="input-group">
                        <input type="text" class="form-control" id="starts" data-container="#addNewEvent"
                        data-plugin="datepicker">
                        <span class="input-group-addon">
                          <i class="icon md-calendar" aria-hidden="true"></i>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-2 form-control-label" for="ends">Ends:</label>
                    <div class="col-md-10">
                      <div class="input-group">
                        <input type="text" class="form-control" id="ends" data-container="#addNewEvent"
                        data-plugin="datepicker">
                        <span class="input-group-addon">
                          <i class="icon md-calendar" aria-hidden="true"></i>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-2 form-control-label" for="repeats">Repeats:</label>
                    <div class="col-md-10">
                      <input type="text" class="form-control" id="repeats" name="repeats" data-plugin="TouchSpin"
                      data-min="0" data-max="10" value="0" />
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="form-control-label col-md-2">Color:</label>
                    <div class="col-md-10">
                      <ul class="color-selector">
                        <li class="bg-blue-600">
                          <input type="radio" checked name="eventColorChosen" id="eventColorChosen2">
                          <label for="eventColorChosen2"></label>
                        </li>
                        <li class="bg-green-600">
                          <input type="radio" name="eventColorChosen" id="eventColorChosen3">
                          <label for="eventColorChosen3"></label>
                        </li>
                        <li class="bg-cyan-600">
                          <input type="radio" name="eventColorChosen" id="eventColorChosen4">
                          <label for="eventColorChosen4"></label>
                        </li>
                        <li class="bg-orange-600">
                          <input type="radio" name="eventColorChosen" id="eventColorChosen5">
                          <label for="eventColorChosen5"></label>
                        </li>
                        <li class="bg-red-600">
                          <input type="radio" name="eventColorChosen" id="eventColorChosen6">
                          <label for="eventColorChosen6"></label>
                        </li>
                        <li class="bg-blue-grey-600">
                          <input type="radio" name="eventColorChosen" id="eventColorChosen7">
                          <label for="eventColorChosen7"></label>
                        </li>
                        <li class="bg-purple-600">
                          <input type="radio" name="eventColorChosen" id="eventColorChosen8">
                          <label for="eventColorChosen8"></label>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-2 form-control-label" for="people">People:</label>
                    <div class="col-md-10">
                      <select id="eventPeople" multiple="multiple" class="plugin-selective"></select>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <div class="form-actions">
                    <button class="btn btn-primary" data-dismiss="modal" type="button">Add this event</button>
                    <a class="btn btn-sm btn-white btn-pure" data-dismiss="modal" href="javascript:void(0)">Cancel</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <!-- End AddEvent Dialog -->
          <!-- Edit Dialog -->
          <div class="modal fade" id="editNewEvent" aria-hidden="true" aria-labelledby="editNewEvent"
          role="dialog" tabindex="-1" data-show="false">
            <div class="modal-dialog modal-simple">
              <form class="modal-content form-horizontal" action="#" method="post" role="form">
                <div class="modal-header">
                  <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                  <h4 class="modal-title">Edit Event</h4>
                </div>
                <div class="modal-body">
                  <div class="form-group row">
                    <label class="col-md-2 form-control-label" for="editEname">Name:</label>
                    <div class="col-md-10">
                      <input type="text" class="form-control" id="editEname" name="editEname">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-2 form-control-label" for="editStarts">Starts:</label>
                    <div class="col-md-10">
                      <div class="input-group">
                        <input type="text" class="form-control" id="editStarts" name="editStarts" data-container="#editNewEvent"
                        data-plugin="datepicker">
                        <span class="input-group-addon">
                          <i class="icon md-calendar" aria-hidden="true"></i>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-2 form-control-label" for="editEnds">Ends:</label>
                    <div class="col-md-10">
                      <div class="input-group">
                        <input type="text" class="form-control" id="editEnds" data-container="#editNewEvent"
                        data-plugin="datepicker">
                        <span class="input-group-addon">
                          <i class="icon md-calendar" aria-hidden="true"></i>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-2 form-control-label" for="editRepeats">Repeats:</label>
                    <div class="col-md-10">
                      <input type="text" class="form-control" id="editRepeats" name="repeats" data-plugin="TouchSpin"
                      data-min="0" data-max="10" value="0" />
                    </div>
                  </div>
                  <div class="form-group row" id="editColor">
                    <label class="form-control-label col-md-2">Color:</label>
                    <div class="col-md-10">
                      <ul class="color-selector">
                        <li class="bg-blue-600">
                          <input type="radio" data-color="blue|600" name="colorChosen" id="editColorChosen2">
                          <label for="editColorChosen2"></label>
                        </li>
                        <li class="bg-green-600">
                          <input type="radio" data-color="green|600" name="colorChosen" id="editColorChosen3">
                          <label for="editColorChosen3"></label>
                        </li>
                        <li class="bg-cyan-600">
                          <input type="radio" data-color="cyan|600" name="colorChosen" id="editColorChosen4">
                          <label for="editColorChosen4"></label>
                        </li>
                        <li class="bg-orange-600">
                          <input type="radio" data-color="orange|600" name="colorChosen" id="editColorChosen5">
                          <label for="editColorChosen4"></label>
                        </li>
                        <li class="bg-red-600">
                          <input type="radio" data-color="red|600" name="colorChosen" id="editColorChosen6">
                          <label for="editColorChosen6"></label>
                        </li>
                        <li class="bg-blue-grey-600">
                          <input type="radio" data-color="blue-grey|600" name="colorChosen" id="editColorChosen7">
                          <label for="editColorChosen7"></label>
                        </li>
                        <li class="bg-purple-600">
                          <input type="radio" data-color="purple|600" name="colorChosen" id="editColorChosen8">
                          <label for="editColorChosen8"></label>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-2 form-control-label" for="editPeople">People:</label>
                    <div class="col-md-10">
                      <select id="editPeople" multiple="multiple" class="plugin-selective"></select>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <div class="form-actions">
                    <button class="btn btn-primary" data-dismiss="modal" type="button">Save</button>
                    <button class="btn btn-danger" data-dismiss="modal" type="button">Delete</button>
                    <a class="btn btn-sm btn-white btn-pure" data-dismiss="modal" href="javascript:void(0)">Cancel</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <!-- End EditEvent Dialog -->
          <!--AddCalendar Dialog -->
          <div class="modal fade" id="addNewCalendar" aria-hidden="true" aria-labelledby="addNewCalendar"
          role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple">
              <form class="modal-content form-horizontal" action="#" method="post" role="form">
                <div class="modal-header">
                  <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                  <h4 class="modal-title">New Calendar</h4>
                </div>
                <div class="modal-body">
                  <div class="form-group row">
                    <label class="col-md-2 form-control-label" for="ename">Name:</label>
                    <div class="col-md-10">
                      <input type="text" class="form-control" id="ename" name="ename">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="form-control-label col-md-2">Color:</label>
                    <div class="col-md-10">
                      <ul class="color-selector">
                        <li class="bg-blue-600">
                          <input type="radio" checked name="colorChosen" id="colorChosen2">
                          <label for="colorChosen2"></label>
                        </li>
                        <li class="bg-green-600">
                          <input type="radio" name="colorChosen" id="colorChosen3">
                          <label for="colorChosen3"></label>
                        </li>
                        <li class="bg-cyan-600">
                          <input type="radio" name="colorChosen" id="colorChosen4">
                          <label for="colorChosen4"></label>
                        </li>
                        <li class="bg-orange-600">
                          <input type="radio" name="colorChosen" id="colorChosen5">
                          <label for="colorChosen5"></label>
                        </li>
                        <li class="bg-red-600">
                          <input type="radio" name="colorChosen" id="colorChosen6">
                          <label for="colorChosen6"></label>
                        </li>
                        <li class="bg-blue-grey-600">
                          <input type="radio" name="colorChosen" id="colorChosen7">
                          <label for="colorChosen7"></label>
                        </li>
                        <li class="bg-purple-600">
                          <input type="radio" name="colorChosen" id="colorChosen8">
                          <label for="colorChosen8"></label>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-2 form-control-label" for="people">People:</label>
                    <div class="col-md-10">
                      <select id="people" multiple="multiple" class="plugin-selective"></select>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <div class="form-actions">
                    <button class="btn btn-primary" data-dismiss="modal" type="button">Create</button>
                    <a class="btn btn-sm btn-white btn-pure" data-dismiss="modal" href="javascript:void(0)">Cancel</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <!-- End AddCalendar Dialog -->
        </div>
      </div>
    </div>
  </div>
  <!-- Site Action -->
  <div class="site-action" data-plugin="actionBtn" style="display:none;">
    <button type="button" class="site-action-toggle btn-raised btn btn-success btn-floating">
      <i class="front-icon md-plus animation-scale-up" aria-hidden="true"></i>
      <i class="back-icon md-delete animation-scale-up" aria-hidden="true"></i>
    </button>
  </div>
  <!-- End Site Action -->
  <!-- Add Calendar Form -->
  <div class="modal fade" id="addNewCalendarForm" aria-hidden="true" aria-labelledby="addNewCalendarForm"
  role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple">
      <form class="modal-content" action="#" method="post" role="form">
        <div class="modal-header">
          <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
          <h4 class="modal-title">Create New Calendar</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label class="form-control-label mb-15" for="name">Calendar name:</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Calendar name">
          </div>
          <div class="form-group">
            <label class="form-control-label mb-15" for="name">Choice people to your project:</label>
            <select multiple="multiple" class="plugin-selective"></select>
          </div>
        </div>
        <div class="modal-footer">
          <div class="form-actions">
            <button class="btn btn-primary" data-dismiss="modal" type="button">Create</button>
            <a class="btn btn-sm btn-white btn-pure" data-dismiss="modal" href="javascript:void(0)">Cancel</a>
          </div>
        </div>
      </form>
    </div>
  </div>
  <!-- End Add Calendar Form -->
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/jquery-ui/jquery-ui.min.js"></script>
  <script src="../../global/vendor/moment/moment.min.js"></script>
  <script src="../../global/vendor/fullcalendar/fullcalendar.js"></script>
  <script src="../../global/vendor/jquery-selective/jquery-selective.min.js"></script>
  <script src="../../global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
  <script src="../../global/vendor/bootstrap-touchspin/bootstrap-touchspin.min.js"></script>
  <script src="../../global/vendor/bootbox/bootbox.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-touchspin.js"></script>
  <script src="../../global/js/Plugin/bootstrap-datepicker.js"></script>
  <script src="../../global/js/Plugin/material.js"></script>
  <script src="../../global/js/Plugin/action-btn.js"></script>
  <script src="../../global/js/Plugin/editlist.js"></script>
  <script src="../../global/js/Plugin/bootbox.js"></script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../assets/js/App/officecalendar.js"></script>
  <script src="../../assets/examples/js/apps/calendar.js"></script>
  <script>
  $(document).ready(function() {
    var wrap = $('.page');

    $(window).scroll(function () {
      if ($(this).scrollTop() > 125) {
        wrap.addClass("fix-mini-nav");
      } else {
        wrap.removeClass("fix-mini-nav");
      }
    });
  });
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>
