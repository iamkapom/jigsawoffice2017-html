<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../../assets/examples/css/apps/documents.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <style type="text/css">
  .slick-dots li button::before{font-size: 14px;}
  .btn-comment-post .btn-select{
    border: 0px !important;
  }
  .btn-comment-post, .btn-comment-post .bootstrap-select{
    width: 150px !important;
  }
  .fix-mini-nav + .page-content{
    padding-top: 0;
  }
  .fix-mini-nav .page-header{
    margin-bottom: 93px;
  }
  .fix-mini-nav .fix-mini-nav{
    margin-bottom: 0;
    margin-right: 0;
    margin-top: 0;
    padding: 0;
    position: fixed;
    top: 65px;
    width: 100%;
    z-index: 10;
    left: 0;
  }
  .btn-comment-post .btn-select{
    border: 0px !important;
  }
  .btn-comment-post, .btn-comment-post .bootstrap-select{
    width: 150px !important;
  }
  .page-content{
    padding: 0;
  }
  </style>
  <script src="../../../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition app-documents">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header">
      <h1 class="page-title mb-10">Policy Category</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item active">Apps</li>
        <li class="breadcrumb-item active"><a href="index.php">Intranet</a></li>
        <li class="breadcrumb-item active">Policy Category</li>
      </ol>
      <div class="page-header-actions">
        <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Setting">
          <i class="icon md-settings" aria-hidden="true"></i>
        </button>
      </div>
    </div>
    <?php include("mini-nav.php");?>
    <div class="page-content bg-white">
      <div class="page-header">
          <form class="mt-20" action="#" role="search">
            <div class="input-search input-search-dark">
              <input type="text" class="form-control w-full" placeholder="Search the knowledge base..." name="">
              <button type="submit" class="input-search-btn">
                <i class="icon md-search" aria-hidden="true"></i>
              </button>
            </div>
          </form>
        </div>
      <div class="documents-wrap categories" data-plugin="animateList" data-child="li">
        <ul class="blocks blocks-100 blocks-xxl-4 blocks-lg-3 blocks-sm-2" data-plugin="matchHeight">
          <li>
            <div class="category">
              <div class="icon-wrap">
                <i class="icon md-desktop-windows" aria-hidden="true"></i>
              </div>
              <h4>Getting Started</h4>
              <p>Sapientium sis excelsus atilii patientiamque percipi splendido dum
                optabilem. Efficiantur.</p>
              <a href="policy-articles.php">3 Articles</a>
            </div>
          </li>
          <li>
            <div class="category">
              <div class="icon-wrap">
                <i class="icon md-desktop-windows" aria-hidden="true"></i>
              </div>
              <h4>Divitias modis</h4>
              <p>Facerem impendere exitum fortunae expetendam astris architecto. Vendibiliora
                suum, peccant.</p>
              <a href="policy-articles.php">2 Articles</a>
            </div>
          </li>
          <li>
            <div class="category">
              <div class="icon-wrap">
                <i class="icon md-desktop-windows" aria-hidden="true"></i>
              </div>
              <h4>Developer Tourials</h4>
              <p>Oportere seditiones perveniri modo expetendum ignota istis timeam
                plus consequatur.</p>
              <a href="policy-articles.php">4 Articles</a>
            </div>
          </li>
          <li>
            <div class="category">
              <div class="icon-wrap">
                <i class="icon md-desktop-windows" aria-hidden="true"></i>
              </div>
              <h4>Compatible Themes</h4>
              <p>Faciendum sale quid, cognitionem, hominibus minimum movere, atomum
                sive. Omnes.</p>
              <a href="policy-articles.php">5 Articles</a>
            </div>
          </li>
          <li>
            <div class="category">
              <div class="icon-wrap">
                <i class="icon md-desktop-windows" aria-hidden="true"></i>
              </div>
              <h4>Commodis clamat</h4>
              <p>Debilitatem simul domus desistemus aptior infanti. Praeterierunt
                quem tenuit inquam.</p>
              <a href="policy-articles.php">2 Articles</a>
            </div>
          </li>
          <li>
            <div class="category">
              <div class="icon-wrap">
                <i class="icon md-desktop-windows" aria-hidden="true"></i>
              </div>
              <h4>Numeris imperio</h4>
              <p>Ignorant divitiarum, affirmatis multam assidua maxime, possum consuetudinum
                habeat utraque.</p>
              <a href="policy-articles.php">6 Articles</a>
            </div>
          </li>
          <li>
            <div class="category">
              <div class="icon-wrap">
                <i class="icon md-desktop-windows" aria-hidden="true"></i>
              </div>
              <h4>Generis acutum</h4>
              <p>Physici ea scribi disseruerunt expetendis eximiae, faciendum. Primo,
                copulatas detracto.</p>
              <a href="policy-articles.php">5 Articles</a>
            </div>
          </li>
          <li>
            <div class="category">
              <div class="icon-wrap">
                <i class="icon md-desktop-windows" aria-hidden="true"></i>
              </div>
              <h4>Distinctio arbitrium</h4>
              <p>Principes ratione veri monet confirmare consuetudine, incurrunt,
                iracundiae oblique, inopem.</p>
              <a href="policy-articles.php">3 Articles</a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../../../global/vendor/jquery/jquery.js"></script>
  <script src="../../../../global/vendor/tether/tether.js"></script>
  <script src="../../../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../../../global/vendor/animsition/animsition.js"></script>
  <script src="../../../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../../../global/vendor/intro-js/intro.js"></script>
  <script src="../../../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <!-- Scripts -->
  <script src="../../../../global/js/State.js"></script>
  <script src="../../../../global/js/Component.js"></script>
  <script src="../../../../global/js/Plugin.js"></script>
  <script src="../../../../global/js/Base.js"></script>
  <script src="../../../../global/js/Config.js"></script>
  <script src="../../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/GridMenu.js"></script>
  <script src="../../../assets/js/Section/Sidebar.js"></script>
  <script src="../../../assets/js/Section/PageAside.js"></script>
  <script src="../../../assets/js/Plugin/menu.js"></script>
  <script src="../../../../global/js/config/colors.js"></script>
  <script src="../../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../../assets');
  </script>
  <script src="../../../assets/js/Site.js"></script>
  <script src="../../../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../../../global/js/Plugin/switchery.js"></script>
  <script src="../../../../global/js/Plugin/matchheight.js"></script>
  <script src="../../../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../../../global/js/Plugin/animate-list.js"></script>
  <script src="../../../assets/js/Site.js"></script>
  <script src="../../../assets/js/App/Documents.js"></script>
  <script src="../../../assets/examples/js/apps/documents.js"></script>
  <script>
  (function(document, window, $) {
  'use strict';
  var Site = window.Site;
  $(document).ready(function() {
    Site.run();
    var wrap = $('.page');
      $(window).scroll(function () {
        if ($(this).scrollTop() > 125) {
          wrap.addClass("fix-mini-nav");
        } else {
          wrap.removeClass("fix-mini-nav");
        }
      });
  });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>