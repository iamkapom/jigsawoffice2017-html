<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/summernote/summernote.css">
  <link rel="stylesheet" href="../../global/vendor/blueimp-file-upload/jquery.fileupload.css">
  <link rel="stylesheet" href="../../global/vendor/dropify/dropify.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition page-aside-right">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-10">News</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item active">Apps</li>
        <li class="breadcrumb-item active"><a href="index.php">Intranet</a></li>
        <li class="breadcrumb-item active">News</li>
      </ol>
      <?php include("mini-nav.php");?>
      <div class="page-header-actions">
        <button type="button" onClick="location.href='news-setting.php';" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Setting">
          <i class="icon md-settings" aria-hidden="true"></i>
        </button>
      </div>
    </div>
    
    <div class="page-content container-fluid" style="position: relative;">
      
      <div class="row ml-0 mr-0">
        <div class="col-md-12">
          <div class="page-aside" style="none;border-right:0;">
        <!-- Contacts Sidebar -->
        <div class="page-aside-switch">
          <i class="icon md-chevron-left" aria-hidden="true"></i>
          <i class="icon md-chevron-right" aria-hidden="true"></i>
        </div>
        <div class="page-aside-inner page-aside-scroll">
          <div data-role="container">
            <div data-role="content">
              <section class="page-aside-section">
                <div class="panel" style="margin-top: -20px;">
                  <div class="panel-heading">
                    <h3 class="panel-title pl-20">Content Status</h3>
                  </div>
                  <div class="panel-body p-20">
                      <button type="button" class="btn active waves-effect waves-classic ml-10">Save Draft</button>
                      <button type="button" class="btn active waves-effect waves-classic ml-10">Preview</button>
                      <p class="mt-15">
                        <span class="w-60" style="display:inline-block;">Status: </span><strong>Draft</strong> <a class="pl-10" href="#">Edit</a>
                      </p>
                  </div>
                  <div class="panel-footer pl-20">
                    <a class="text-danger btn btn-pure" href="#">Delete</a>
                    <button style="float:right" type="button" class="btn btn-info active waves-effect waves-classic ml-10">Public</button>
                    <div class="clearfix"></div>
                  </div>
                </div>
                
                <div class="panel">
                  <div class="panel-heading">
                    <h3 class="panel-title pl-20 py-0">Who should see this?</h3>
                  </div>
                  <div class="panel-body p-20">
                      <div class="mb-20">
                        <ul class="list-unstyled">
                          <li class="mb-15">
                            <input type="radio" class="icheckbox-primary" id="inputRadiosPermis" name="inputRadiosPermis"
                            data-plugin="iCheck" data-radio-class="iradio_flat-blue" value="Public" checked />
                            <label class="ml-5 mr-30" for="inputRadiosPermis">Public</label>
                          </li>
                          <li class="mb-15">
                            <input type="radio" class="icheckbox-primary" id="inputRadiosPermis2" name="inputRadiosPermis"
                            data-plugin="iCheck" data-radio-class="iradio_flat-blue" value="Custom" />
                            <label class="ml-5 mr-30" for="inputRadiosPermis2">Custom</label>
                          </li>
                        </ul>
                      </div>
                      <div id="per_Custom" style="display:none;">
                        <select id="select_permiss" class="form-control" multiple="" style="width:100%;"></select>
                      </div>
                  </div>
                </div>

                <div class="panel">
                  <div class="panel-heading">
                    <h3 class="panel-title pl-20 py-0">Comment</h3>
                  </div>
                  <div class="panel-body p-20">
                      
                      <div class="grey-600">
                        <input type="checkbox" id="m11" name="m11" data-plugin="switchery" data-size="small"/>
                        <label for="m11" style="cursor: pointer;">
                           On/Off Comment
                        </label>
                      </div>
                  </div>
                </div>

                <div class="panel">
                  <div class="panel-heading">
                    <h3 class="panel-title pl-20 py-0">Categories</h3>
                  </div>
                  <div class="panel-body p-20">
                    <?php for($ax=1;$ax<=6;$ax++){?>
                      <div class="checkbox-custom checkbox-primary">
                        <input type="checkbox" id="inputUnchecked<?php echo $ax;?>" />
                        <label for="inputUnchecked<?php echo $ax;?>">Categories <?php echo $ax;?></label>
                      </div>
                    <?php }?>
                      
                  </div>
                </div>

                <div class="panel">
                  <div class="panel-heading">
                    <h3 class="panel-title pl-20 py-0">Tags</h3>
                  </div>
                  <div class="panel-body p-20">
                      <input type="text" class="form-control" data-plugin="tokenfield" value="Primary,Success,blue"/>
                  </div>
                </div>

                <div class="panel">
                  <div class="panel-heading">
                    <h3 class="panel-title pl-20 py-0">Activities</h3>
                  </div>
                  <div class="panel-body p-20">
                      <ul class="timeline timeline-single">
                        <li class="timeline-item mb-30 pl-20">
                          <div class="timeline-dot" rip-style-bordercolor-backup="" style="" rip-style-borderstyle-backup="" rip-style-borderwidth-backup=""></div>
                          <div class="timeline-content">
                            <div class="media">
                              <div class="pr-10">
                                <a class="avatar avatar-sm" href="javascript:void(0)">
                                  <img class="img-fluid" src="../../../global/portraits/5.jpg" alt="...">
                                </a>
                              </div>
                              <div class="media-body">
                                <h5 class="mt-0 mb-0">June Lane</h5>
                                <small>16 minutes ago</small>
                                <div class="profile-brief">
                                  Update Status
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                        <li class="timeline-item mb-30 pl-20">
                          <div class="timeline-dot"></div>
                          <div class="timeline-content">
                            <div class="media">
                              <div class="pr-10">
                                <a class="avatar avatar-sm" href="javascript:void(0)">
                                  <img class="img-fluid" src="../../../global/portraits/5.jpg" alt="...">
                                </a>
                              </div>
                              <div class="media-body">
                                <h5 class="mt-0 mb-0">June Lane</h5>
                                <small>16 minutes ago</small>
                                <div class="profile-brief">
                                  Edit Content
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                      </ul>
                  </div>
                </div>
              </section>
              
            </div>
          </div>
        </div>
      </div>
          <div class="page-main">
          <div class="panel">
            <div class="panel-body container-fluid">
              <div class="row">
                <div class="col-md-12">
                  <!-- Example Basic Form -->
                  <div class="example-wrap">
                    <div class="example">
                      <form autocomplete="off">
                        <div class="row">
                          <div class="form-group col-md-12">
                            <input class="form-control form-control-lg" id="inputBasicFirstName" name="inputFirstName" value="What Is SEO / Search Engine Optimization?" placeholder="Enter title here " autocomplete="off" type="text">
                          </div>
                        </div>
                        <div class="form-group form-material">
                          <div id="summernote" data-plugin="summernote">
                            <h2>It's Simpler Than You Think!</h2>
  SEO or Search Engine Optimisation is the name given to activity that attempts to improve search engine rankings.
  In search results Google™ displays links to pages it considers relevant and authoritative. Authority is mostly measured by analysing the number and quality of links from other web pages.
  In simple terms your web pages have the potential to rank in Google™ so long as other web pages link to them.
                              <div class="mt-10">
                                    <div class="row">
                              <div class="col-sm-5"><img src="https://www.redevolution.com/images/what_we_do/seo/seo-aberdeen.png" alt="SEO Aberdeen" class="img-responsive" style="display: block; margin-left: auto; margin-right: auto;" title="SEO Aberdeen"></div>
                              <div class="col-sm-7">
                              <h2>Make Your Site Appear in Google&trade;</h2>
                              <p>Great Content encourages people to link to your pages and shows Google&trade; your pages are interesting and authoritative. This leads to search engine success because Google&trade; wants to show interesting and authoritative pages in its search results. It's that <em>simple!</em></p>
                              <p>Check our <a href="/seo-explained">SEO Explained in Pictures</a> page for a quick, simple overview. It's guaranteed to help you understand SEO.</p>
                              </div>
                              </div>
                              <div class="row">
                              <div class="col-sm-7">
                              <h2>How Does Google&trade; Rank Pages?</h2>
                              <p>Google&trade; promotes authority pages to the top of its rankings so it's your job to create pages that become authority pages. This involves writing content people find useful because useful content is shared in blogs, twitter feeds etc., and over time Google&trade; picks up on these authority signals. This virtuous circle creates strong and sustainable Google&trade; rankings.</p>
                              </div>
                              <div class="col-sm-5"><img src="https://www.redevolution.com/images/what_we_do/seo/google-ranks.png" alt="Google Ranks" class="img-responsive" style="display: block; margin-left: auto; margin-right: auto;" title="Google Ranks"></div>
                              </div>
                              
                              
                              </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <!-- End Example Basic Form -->
                </div>
              </div>
            </div>
          </div>
          <div class="panel">
            <div class="panel-body container-fluid">
              <h3>Documents/Photos/Media</h3>
              <div class="example">
                <input type="file" id="input-file-now" data-plugin="dropify" data-default-file=""
                />
              </div>
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th width="20%">Download</th>
                    <th class="hidden-sm-down" width="20%">Created</th>
                    <th class="hidden-sm-down" width="20%">File size</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <div class="media" style="flex-direction:initial;">
                        <div class="pr-20">
                          <a data-url="panel-file.html" data-toggle="slidePanel" class="font-size-24" href="javascript:void(0)">
                            <i style="color:#ff9800" class="icon fa-file-powerpoint-o" aria-hidden="true"></i>
                          </a>
                        </div>
                        <div class="media-body pt-10">
                          <h5><a style="color:#616161;text-decoration: none;" data-url="panel-folder.html" data-toggle="slidePanel" href="javascript:void(0)">TOR_Checklist</a></h5>
                          <p class="hidden-md-up mb-0 grey-500">
                            <small>
                              Mar 3, 2017 <span class="ml-10 mr-10">&mdash;</span> 2.85 MB                            </small>
                          </p>
                        </div>
                      </div>
                    </td>
                    <td class="pt-15">12</td>
                    <td class="hidden-sm-down pt-15">Mar 3, 2017</td>
                    <td class="hidden-sm-down pt-15">2.85 MB</td>
                  </tr>
                  <tr>
                    <td>
                      <div class="media" style="flex-direction:initial;">
                        <div class="pr-20">
                          <a data-url="panel-file.html" data-toggle="slidePanel" class="font-size-24" href="javascript:void(0)">
                            <i style="color:#f44336" class="icon fa-file-pdf-o" aria-hidden="true"></i>
                          </a>
                        </div>
                        <div class="media-body pt-10">
                          <h5><a style="color:#616161;text-decoration: none;" data-url="panel-folder.html" data-toggle="slidePanel" href="javascript:void(0)">wireframe inner</a></h5>
                          <p class="hidden-md-up mb-0 grey-500">
                            <small>
                              Mar 3, 2017 <span class="ml-10 mr-10">&mdash;</span> 4.26 KB                            </small>
                          </p>
                        </div>
                      </div>
                    </td>
                    <td class="pt-15">5</td>
                    <td class="hidden-sm-down pt-15">Mar 3, 2017</td>
                    <td class="hidden-sm-down pt-15">4.26 KB</td>
                  </tr>
                                    <tr>
                    <td>
                      <div class="media" style="flex-direction:initial;">
                        <div class="pr-20">
                          <a data-url="panel-file.html" data-toggle="slidePanel" class="font-size-24" href="javascript:void(0)">
                            <i style="color:#4caf50" class="icon fa-file-excel-o" aria-hidden="true"></i>
                          </a>
                        </div>
                        <div class="media-body pt-10">
                          <h5><a style="color:#616161;text-decoration: none;" data-url="panel-folder.html" data-toggle="slidePanel" href="javascript:void(0)">เอกสารรวบรวม Data</a></h5>
                          <p class="hidden-md-up mb-0 grey-500">
                            <small>
                              Mar 3, 2017 <span class="ml-10 mr-10">&mdash;</span> 1.52 MB                            </small>
                          </p>
                        </div>
                      </div>
                    </td>
                    <td class="pt-15">32</td>
                    <td class="hidden-sm-down pt-15">Mar 3, 2017</td>
                    <td class="hidden-sm-down pt-15">1.52 MB</td>
                  </tr>
                                    <tr>
                    <td>
                      <div class="media" style="flex-direction:initial;">
                        <div class="pr-20">
                          <a data-url="panel-file.html" data-toggle="slidePanel" class="font-size-24" href="javascript:void(0)">
                            <i style="color:#ffab00" class="icon fa-file-picture-o" aria-hidden="true"></i>
                          </a>
                        </div>
                        <div class="media-body pt-10">
                          <h5><a style="color:#616161;text-decoration: none;" data-url="panel-folder.html" data-toggle="slidePanel" href="javascript:void(0)">WEB Design Trend</a></h5>
                          <p class="hidden-md-up mb-0 grey-500">
                            <small>
                              Mar 3, 2017 <span class="ml-10 mr-10">&mdash;</span> 4.25 MB                            </small>
                          </p>
                        </div>
                      </div>
                    </td>
                    <td class="pt-15">8</td>
                    <td class="hidden-sm-down pt-15">Mar 3, 2017</td>
                    <td class="hidden-sm-down pt-15">4.25 MB</td>
                  </tr>
                                  </tbody>
              </table>
              
            </div>
          </div>
        
        </div>
          


        </div>
      </div>
    </div>
  </div>
  <div class="site-action" data-plugin="actionBtn">
    <button type="button" onclick="location.href='news-form.php';" class="btn-raised btn btn-success btn-floating">
      <i class="front-icon md-plus" aria-hidden="true"></i>
    </button>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/imagesloaded/imagesloaded.pkgd.js"></script>
  <script src="../../global/vendor/summernote/summernote.min.js"></script>
  <script src="../../global/vendor/blueimp-canvas-to-blob/canvas-to-blob.js"></script>
  <script src="../../global/vendor/blueimp-load-image/load-image.all.min.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-process.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-image.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-audio.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-video.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-validate.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-ui.js"></script>
  <script src="../../global/vendor/dropify/dropify.min.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../assets/examples/js/forms/editor-summernote.js"></script>
  <script src="../../global/js/Plugin/dropify.js"></script>
  <script src="../../assets/examples/js/forms/uploads.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
      
      $('input[name=inputRadiosPermis]').on('ifChecked', function(event){
        if (this.value == 'Custom') {
            $("#per_Custom").show();
        }else {
            $("#per_Custom").hide();
        }
      });

    });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>