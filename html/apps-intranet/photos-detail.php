<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../assets/examples/css/advanced/masonry.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">

  <link rel="stylesheet" href="../../global/css/jquery.fancybox.min.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition page-aside-right">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
       <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-10">News</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item active">Apps</li>
        <li class="breadcrumb-item active"><a href="index.php">Intranet</a></li>
        <li class="breadcrumb-item active">News</li>
      </ol>
      <?php include("mini-nav.php");?>
    </div>

    <div class="page-content container-fluid" style="position: relative;">

      <div class="row ml-0 mr-0">
        <div class="col-md-12">
          <div class="page-aside" style="border-right:0;">
        <!-- Contacts Sidebar -->
        <div class="page-aside-switch">
          <i class="icon md-chevron-left" aria-hidden="true"></i>
          <i class="icon md-chevron-right" aria-hidden="true"></i>
        </div>
        <div class="page-aside-inner page-aside-scroll">
          <div data-role="container">
            <div data-role="content">
              <section class="page-aside-section">
                <div class="panel">
                  <div class="panel-heading">
                    <h3 class="panel-title pl-20">Information</h3>
                  </div>
                  <div class="panel-body p-20">
                      <div class="mb-20">
                        <div class="grey-500">Stutus</div>
                        <span>Public</span>
                      </div>
                      <div class="mb-20">
                        <div class="grey-500">Created</div>
                        <span>May 5 at 11:00 AM</span>
                      </div>
                      <div class="mb-20">
                        <div class="grey-500">Created by</div>
                        <span><img style="width:20px;" class="avatar" src="../../global/portraits/1.jpg" title="Herman Beck"> Herman Beck
                        </span>
                      </div>
                      <div class="mb-20">
                        <div class="grey-500">Last Updated</div>
                        <span>May 15 at 11:00 AM</span>
                      </div>
                      <div>
                        <div class="grey-500">Update by</div>
                        <span><img style="width:20px;" class="avatar" src="../../global/portraits/1.jpg" title="Herman Beck"> Herman Beck
                        </span>
                      </div>

                  </div>

                </div>

                <div class="panel">
                  <div class="panel-heading">
                    <h3 class="panel-title pl-20">Tags</h3>
                  </div>
                  <div class="panel-body p-20">
                      <span class="badge badge-round badge-warning">Primary</span>
                    <span class="badge badge-round badge-warning">Success</span>
                    <span class="badge badge-round badge-warning">Warning</span>
                  </div>
                </div>

                <div class="panel">
                  <div class="panel-heading">
                    <h3 class="panel-title pl-20">Activities</h3>
                  </div>
                  <div class="panel-body p-20">
                      <ul class="timeline timeline-single">
                        <li class="timeline-item mb-30 pl-20">
                          <div class="timeline-dot" rip-style-bordercolor-backup="" style="" rip-style-borderstyle-backup="" rip-style-borderwidth-backup=""></div>
                          <div class="timeline-content">
                            <div class="media">
                              <div class="pr-10">
                                <a class="avatar avatar-sm" href="javascript:void(0)">
                                  <img class="img-fluid" src="../../../global/portraits/5.jpg" alt="...">
                                </a>
                              </div>
                              <div class="media-body">
                                <h5 class="mt-0 mb-0">June Lane</h5>
                                <small>16 minutes ago</small>
                                <div class="profile-brief">
                                  Update Status
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                        <li class="timeline-item mb-30 pl-20">
                          <div class="timeline-dot"></div>
                          <div class="timeline-content">
                            <div class="media">
                              <div class="pr-10">
                                <a class="avatar avatar-sm" href="javascript:void(0)">
                                  <img class="img-fluid" src="../../../global/portraits/5.jpg" alt="...">
                                </a>
                              </div>
                              <div class="media-body">
                                <h5 class="mt-0 mb-0">June Lane</h5>
                                <small>16 minutes ago</small>
                                <div class="profile-brief">
                                  Edit Content
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                      </ul>
                  </div>
                </div>
              </section>

            </div>
          </div>
        </div>
      </div>
          <div class="page-main">
          <div class="panel">
            <div class="panel-body container-fluid">
              <div class="row">
                <div class="col-md-12">
                  <!-- Example Basic Form -->
                  <div class="example-wrap">
                    <div class="example">
                        <div class="row">
                          <div class="form-group form-material col-md-12">
                            <h1>What Is SEO / Search Engine Optimization?</h1>
                            <small>SEO or Search Engine Optimisation is the name given to activity</small>
                          </div>
                        </div>
                        <ul class="blocks blocks-100 blocks-xxl-4 blocks-lg-4 blocks-md-3 blocks-sm-2" data-plugin="masonry">
                          <?php
                          $cover[]="login";
                          $cover[]="login1";
                          $cover[]="lockscreen";
                          $cover[]="lockscreen1";
                          $cover[]="poster";
                          $cover[]="poster1";
                          $cover[]="dashboard-header";
                          $cover[]="dashboard-header1";

                          for($a=1;$a<=20;$a++){
                          ?>
                          <li class="masonry-item">
                            <a href="../../assets/examples/images/<?=$cover[rand(0,7)];?>.jpg"  data-fancybox="gallery" >
                            <img width="100%" src="../../assets/examples/images/<?=$cover[rand(0,7)];?>.jpg" alt="...">
                          </a>
                          </li>
                          <?php }?>
                        </ul>

                    </div>
                  </div>
                  <!-- End Example Basic Form -->
                </div>
              </div>

              <ul class="wall-attrs clearfix p-0 m-0 mt-10">
                  <li class="attrs-meta float-left font-size-16">
                    <a href="javascript:void(0)" class="mr-20">
                      <i class="icon md-thumb-up"></i>
                      <span>5</span>
                    </a>
                    <a href="javascript:void(0)" onclick="$('#wall-comment-reply-1').toggle();">
                      <i class="icon md-comment"></i>
                      <span>2</span>
                    </a>
                  </li>
                  <li class="float-right">
                    <div class="btn-group bootstrap-select btn-comment-post">
                      <select data-plugin="selectpicker">
                        <option>Top Comments</option>
                        <option>Newest</option>
                        <option>Oldest</option>
                      </select>
                    </div>
                  </li>
                </ul>
            </div>
          </div>

          <div class="panel">
            <div class="panel-body container-fluid">
              <h3>Comments</h3>
              <div class="wall-comment-attrs">
                <div id="wall-comment-reply-1" class="wall-comment-reply clearfix" style="display: none">
                  <div href="#" class="avatar avatar-md float-left">
                    <img src="../../../global/portraits/6.jpg">
                  </div>
                  <div class="ml-60 wall-comment-form">
                    <textarea rows="1" class="autoHeight" placeholder="Write a comment..."></textarea>
                    <div class="wall-comment-reply-attach">
                      <button class="btn btn-icon btn-dark btn-round bg-grey-400">
                        <i class="icon md-attachment-alt rotate" aria-hidden="true"></i>
                      </button>
                    </div>
                  </div>
                </div>
                <div class="wall-comment">
                  <div class="wall-comment-action">
                  <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                    <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                  </button>

                  <div class="dropdown-menu dropdown-menu-right" role="menu">
                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                      Edit Comment
                    </a>
                    <a class="dropdown-item text-danger" href="javascript:void(0)" role="menuitem">
                      Delete
                    </a>
                  </div>
                </div>
                  <a href="#" class="avatar avatar-md float-left">
                    <img src="../../../global/portraits/3.jpg">
                  </a>
                  <div class="ml-60 box-post-comment">
                    <a href="#">Stacey Hunt</a>
                    <span class="ml-10">Do incididunt elit ex incididunt ut. Aute velit proident cupidatat qui et consectetur
                      do. Consequat esse quis ut cupidatat ea. Sint dolore ea culpa
                      dolore velit enim.</span>
                      <p class="font-size-12 mt-5 grey-600">
                        <a href="javascript:void(0)">
                          Like
                        </a>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <a href="javascript:void(0)" class="action-reply">
                          Reply
                        </a>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <span >
                          <i class="icon md-favorite"></i>
                          <span>5</span>
                        </span>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <span>
                          30th July 2017
                        </span>
                      </p>

                    <div class="wall-comment">
                      <a href="#" class="avatar avatar-md float-left">
                        <img src="../../../global/portraits/11.jpg">
                      </a>
                      <div class="ml-60">
                        <a href="#">Crystal Bates</a>
                        <span class="ml-10">Do incididunt elit ex incididunt ut. Aute velit proident cupidatat qui et consectetur
                          do. Consequat esse quis ut cupidatat ea.</span>
                        <p class="font-size-12 mt-5 grey-500">
                          <a href="javascript:void(0)">
                            Like
                          </a>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <a href="javascript:void(0)" class="action-reply">
                            Reply
                          </a>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <span >
                            <i class="icon md-favorite"></i>
                            <span>5</span>
                          </span>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <span>
                            30th July 2017
                          </span>
                        </p>
                      </div>

                    </div>
                    <div class="display-reply"></div>

                  </div>

                </div>
                <div class="wall-comment">
                  <a href="#" class="avatar avatar-md float-left">
                    <img src="../../../global/portraits/5.jpg">
                  </a>
                  <div class="ml-60 box-post-comment">
                    <a href="#">Sam Anderson</a>
                    <span class="ml-10">Ut velit ipsum elit ut mollit elit. Proident officia eu ex consectetur ullamco
                      magna. Enim cillum voluptate sint ipsum ad voluptate exercitation.
                      Ex est amet magna occaecat eu.</span>
                    <p class="font-size-12 mt-5 grey-500">
                      <a href="javascript:void(0)">
                        Like
                      </a>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <a href="javascript:void(0)" class="action-reply">
                        Reply
                      </a>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <span >
                        <i class="icon md-favorite"></i>
                        <span>5</span>
                      </span>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <span>
                        30th July 2017
                      </span>
                    </p>

                    <div class="display-reply"></div>
                  </div>

                </div>
              </div>
            </div>
          </div>

          <div class="panel">
            <div class="panel-body container-fluid">
              <h3>See Also</h3>
              <div class="row">
              <?php
              for($a=1;$a<=4;$a++){?>
              <div class="col-lg-3 col-md-4 col-sm-6 col-6 <?=($a>2?"hidden-sm-down":"")?> <?=($a>3?"hidden-lg-down":"")?>  mt-20">
                <div class="cover">
                  <div class="wall-attach">
                    <a href="photos-detail.php">
                      <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                          <img width="100%" src="../../assets/examples/images/<?=$cover[rand(0,6)];?>.jpg" alt="...">
                        </div>
                      </div>
                      <div class="row" style="max-height: 55px; overflow: hidden;">
                        <div class="col-md-4 col-sm-4 col-4">
                          <img width="100%" src="../../assets/examples/images/<?=$cover[rand(1,6)];?>.jpg" alt="...">
                        </div>
                        <div class="col-md-4 col-sm-4 col-4">
                          <img width="100%" src="../../assets/examples/images/<?=$cover[rand(2,5)];?>.jpg" alt="...">
                        </div>
                        <div class="col-md-4 col-sm-4 col-4">
                          <img width="100%" src="../../assets/examples/images/<?=$cover[rand(6,7)];?>.jpg" alt="...">
                        </div>
                      </div>
                    </a>
                  </div>
                </div>
                <div class="p-5 pt-20">
                    <a href="photos-detail.php">
                    <h4 class="card-title">“Thailand ICT Excellence Award 2017”</h4>
                    </a>
                    <p class="card-text type-link">
                      <small>
                        By
                        <a href="javascript:void(0)">Nathan Watts</a>
                        <a href="javascript:void(0)">05, 2017</a>
                      </small>
                    </p>
                    <p class="card-text"><?=$arr_detail[rand(0,7)]?></p>
                    <div>
                      <span class="badge badge-round badge-warning">Primary</span>
                      <span class="badge badge-round badge-warning">Success</span>
                      <span class="badge badge-round badge-warning">Warning</span>
                    </div>
                  </div>
                  <div class="card-block clearfix">
                    <div class="card-actions float-left font-size-16 mt-5">
                      <a href="javascript:void(0)">
                        <i class="icon md-favorite"></i>
                        <span>253</span>
                      </a>
                      <a href="javascript:void(0)">
                        <i class="icon md-comment"></i>
                        <span>115</span>
                      </a>
                    </div>
                  </div>
                </div>
              <?php }?>
              </div>
            </div>
          </div>

        </div>



        </div>
      </div>
    </div>
  </div>
  <div class="site-action" data-plugin="actionBtn">
    <button type="button" class="btn-raised btn btn-success btn-floating">
      <i class="front-icon md-plus" aria-hidden="true"></i>
    </button>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/jquery.dotdotdot/jquery.dotdotdot.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/masonry/masonry.pkgd.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/masonry.js"></script>
  <script src="../../global/js/jquery.fancybox.min.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
      $(".ellipsis").dotdotdot({
        after: "a.readmore"
      });
    });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>
