<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../global/vendor/webui-popover/webui-popover.css">
  <link rel="stylesheet" href="../../global/vendor/toolbar/toolbar.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition app-documents  page-aside-right">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
  <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-10">Staff Directory</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item active">Apps</li>
        <li class="breadcrumb-item active"><a href="index.php">Intranet</a></li>
        <li class="breadcrumb-item active">Staff Directory</li>
      </ol>
  <?php include("mini-nav.php");?>
    </div>

    <div class="page-content container-fluid " style="position: relative;">

      <div class="row ml-0 mr-0">
        <div class="col-md-12">
          <div class="page-aside">
            <!-- Contacts Sidebar -->
            <div class="page-aside-switch">
              <i class="icon md-chevron-left" aria-hidden="true"></i>
              <i class="icon md-chevron-right" aria-hidden="true"></i>
            </div>
            <div class="page-aside-inner page-aside-scroll">
              <div data-role="container">
                <div data-role="content">
                  <section class="page-aside-section">
                    
                    <div class="card card-block pt-0" style="min-height: 310px;">
                      <h3 class="mt-0">Birthdays Today</h3>
                      <div style="position: relative;">
                        <img class="avatar avatar-lg" src="../../../global/portraits/<?=rand(1,20)?>.jpg" style="width: 75%; position: absolute; z-index: 1; left:35px; top: 65px;">
                        <img src="img/hbd-1.png" style="width: 100%; position: absolute; z-index: 2;">
                      </div>
                      
                    </div>

                    <div class="card card-block pt-0" style="min-height: 235px;">
                      <h3 class="mt-0">Birthdays Today</h3>
                      <div style="position: relative;">
                        <img class="avatar avatar-lg" src="../../../global/portraits/<?=rand(1,20)?>.jpg" style="width: 60%; position: absolute; z-index: 1; left:95px; top: 20px;">
                        <img src="img/hbd-2.png" style="width: 100%; position: absolute; z-index: 2;">
                      </div>

                    </div>

                    <ul class="wall-attrs clearfix p-10 m-0 mt-10">
                        <li class="attrs-meta float-left font-size-16">
                          <a href="javascript:void(0)" class="mr-20">
                            <i class="icon md-thumb-up"></i>
                            <span>5</span>
                          </a>
                          <a href="javascript:void(0)" onclick="$('#wall-comment-reply-1').toggle();">
                            <i class="icon md-comment"></i>
                            <span>2</span>
                          </a>
                        </li>
                        
                      </ul>
                    <div class="wall-comment-attrs  bg-white">
                      <div id="wall-comment-reply-1" class="wall-comment-reply clearfix" style="display: none;">
                        <div href="#" class="avatar avatar-md float-left">
                          <img src="../../../global/portraits/6.jpg">
                        </div>
                        <div class="ml-60 wall-comment-form">
                          <textarea rows="1" class="autoHeight" placeholder="Write a comment..."></textarea>
                          <div class="wall-comment-reply-attach">
                            
                          </div>
                        </div>
                      </div>
                      <?php for($ab=1;$ab<5;$ab++){?>
                        <div class="wall-comment">
                          <a href="#" class="avatar avatar-md float-left">
                            <img src="../../../global/portraits/<?=rand(1,20)?>.jpg">
                          </a>
                          <div class="ml-60 box-post-comment">
                            <div >HBD!!</div>
                            <small>10 min ago</small>
                          </div>
                        </div>
                      <?php }?>
                      
                    </div>


                  </section>
                </div>
              </div>
            </div>
          </div>
          <div class="page-main">
          <section>
            
            <div class="row">
               <div class="col-md-12">
                <div class="card card-shadow card-bordered">
                  <div class="card-header bg-grey-200 p-15 font-size-18 grey-700 clearfix">
                    Upcoming Birthdays
                  </div>
                  <div class="card-block p-20">

                    <h6>SUNDAY, JULY 1</h6>
                    <div style="border-bottom:1px #e0e0e0 solid;"></div>
                    <div class="row pt-10 pb-30">
                      <div class="col-md-6">
                        <a class="avatar avatar-lg float-left mr-20 img-bordered bg-white" href="javascript:void(0)">
                          <img src="../../../global/portraits/<?=rand(1,20)?>.jpg" alt="">
                        </a>
                        <div class="font-size-18 blue-600">Robin Ahrens (Rob)</div>
                        <div class="font-size-14 grey-600">Web Designer</div>
                      </div>
                    </div>
                    
                    <h6>MONDAY, JULY 7</h6>
                    <div style="border-bottom:1px #e0e0e0 solid;"></div>
                    <div class="row pt-10 pb-30">
                      <div class="col-md-6">
                        <a class="avatar avatar-lg float-left mr-20 img-bordered bg-white" href="javascript:void(0)">
                          <img src="../../../global/portraits/<?=rand(1,20)?>.jpg" alt="">
                        </a>
                        <div class="font-size-18 blue-600">Robin Ahrens (Rob)</div>
                        <div class="font-size-14 grey-600">Web Designer</div>
                      </div>
                      <div class="col-md-6">
                        <a class="avatar avatar-lg float-left mr-20 img-bordered bg-white" href="javascript:void(0)">
                          <img src="../../../global/portraits/<?=rand(1,20)?>.jpg" alt="">
                        </a>
                        <div class="font-size-18 blue-600">Robin Ahrens (Rob)</div>
                        <div class="font-size-14 grey-600">Web Designer</div>
                      </div>
                    </div>

                  </div>

                </div>
              </div>
              <?php 
              $mt= date("n");
              for($m=1; $m<=12; ++$m){
                $mm = $m+$mt;
                if($mm>12) $mm = $mm-12;
              ?>
              <div class="col-md-6">
                <div class="card card-shadow card-bordered">
                  <div class="card-header bg-grey-200 p-15 font-size-18 grey-700 clearfix">
                    <?=date('F', mktime(0, 0, 0, $mm, 1))?>
                  </div>
                  <div class="card-block p-20">
                    <?php
                    for($ab=0;$ab<rand(5,10);$ab++){
                      ?>
                        <a data-toggle="tooltip" data-placement="top"
                      data-trigger="hover" data-original-title="Herman Beck" class="avatar avatar-lg img-bordered bg-white float-left m-5" href="javascript:void(0)">
                          <img class="img-fluid" src="../../../global/portraits/<?=rand(1,20)?>.jpg" alt="...">
                        </a>
                    <?php }?>

                  </div>

                </div>
              </div>
              <?php }?>

            </div>
            
          </section>
        </div>

        </div>
      </div>

    </div>
  </div>
  
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <script src="../../global/vendor/webui-popover/jquery.webui-popover.min.js"></script>
  <script src="../../global/vendor/toolbar/jquery.toolbar.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>

  <script src="../../global/js/Plugin/webui-popover.js"></script>
  <script src="../../global/js/Plugin/toolbar.js"></script>

  <script src="../../assets/examples/js/uikit/tooltip-popover.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>
