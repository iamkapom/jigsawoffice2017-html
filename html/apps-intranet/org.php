<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css">
  <link rel="stylesheet" href="../../global/vendor/jquery-orgchart/jquery.orgchart.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <style type="text/css">
  .bootstrap-select{
    width: auto !important;
  }

  </style>
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition app-documents">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-10">Org Chart</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item active">Apps</li>
        <li class="breadcrumb-item active"><a href="index.php">Intranet</a></li>
        <li class="breadcrumb-item active">Org Chart</li>
      </ol>
      <?php include("mini-nav.php");?>
      <div class="page-header-actions">
        <button type="button" onClick="location.href='org-setting.php';" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Setting">
          <i class="icon md-settings" aria-hidden="true"></i>
        </button>
      </div>
    </div>
    <div class="page-content container-fluid" style="position: relative;">
      
      <div class="page-main">
        <section class="pl-10 pr-10">
          <div class="row">
            <div class="col-md-12">
              <div id="orgChart"></div>
            </div>
          </div>
        </section>
      </div>
    </div>
  </div>

<div class="modal fade" id="OrgDetail" aria-hidden="true" aria-labelledby="CalendarForm" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple ">
        <form class="modal-content modal-content-no-border">
            <div class="modal-header">
              <h4 class="modal-title" id="exampleFillInModalTitle">Staff</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="icon md-close" aria-hidden="true"></i>
              </button>
            </div>
            <div class="modal-body">
                <div class="row mt-10">
                  <?php for($ab=1;$ab<=10;$ab++){?>
                    <div class="col-md-6 mb-20">
                      <div class="media p-5" style="border: 1px #eee solid;">
                          <div class="pr-10">
                            <a class="avatar avatar-sm" href="javascript:void(0)">
                              <img class="img-fluid" src="../../../global/portraits/<?=$ab?>.jpg" alt="...">
                            </a>
                          </div>
                          <div class="media-body">
                            <h5 class="mt-3 mb-0"> Herman Beck </h5>
                            <small style="display: block;">Sales <span class="px-10">-</span>Sales Deportment</small>
                            <small class="grey-700">Mobile 08-1234-5678<span class="px-10">-</span>ext. 1234</small>
                          </div>
                        </div>
                    </div>
                  <?php }?>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-pure waves-effect waves-classic" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <script src="../../global/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../assets/js/BaseApp.js"></script>
  <script src="../../assets/js/App/Contacts.js"></script>
  <script src="../../assets/examples/js/apps/contacts.js"></script>
  <script src="../../global/js/Plugin/bootstrap-tagsinput.js"></script>
  <script src="../../global/vendor/jquery-orgchart/jquery.orgchart.js"></script>
  
  <script type="text/javascript">
    var testData = [
        {id: 1, name: 'CEO Office', members:0, parent: 0},
        {id: 2, name: 'Sales', members:1, parent: 1},
        {id: 3, name: 'Manager', members:2, parent: 2},
        {id: 4, name: 'Staff', members:10, parent: 3},
        {id: 5, name: 'Purchasing', members:1, parent: 1},
        {id: 6, name: 'Manager', members:1, parent: 5},
        {id: 7, name: 'Staff', members:3, parent: 6},
        {id: 8, name: 'Warehouse', members:1, parent: 1},
        {id: 9, name: 'Manager', members:2, parent: 8},
        {id: 10, name: 'Staff', members:10, parent: 9},
        {id: 11, name: 'Shipping', members:1, parent: 1},
        {id: 12, name: 'Manager', members:5, parent: 11},
        {id: 13, name: 'Staff', members:20, parent: 12},
        {id: 14, name: 'Accounting', members:1, parent: 1},
        {id: 15, name: 'Manager', members:1, parent: 14},
        {id: 16, name: 'Staff', members:3, parent: 15},
        {id: 17, name: 'marketing', members:1, parent: 1},
        {id: 18, name: 'Manager', members:1, parent: 17},
        {id: 19, name: 'Staff', members:3, parent: 18},
    ];
    $(function(){
      org_chart = $('#orgChart').orgChart({
        data: testData, // your data
        showControls: false // display add or remove node button.
    });
  });


    /*
    Demo 
    https://www.jqueryscript.net/chart-graph/Create-An-Editable-Organization-Chart-with-jQuery-orgChart-Plugin.html
    */
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>
