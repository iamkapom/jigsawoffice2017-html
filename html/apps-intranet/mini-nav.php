<ul class="nav nav-tabs nav-tabs-line bg-white fix-mini-nav" role="tablist" id="exampleFilter">
  <li class="nav-item dropdown hidden-sm-down">
    <?php $fil_staff = array("about.php", "about-setting.php", "board.php","board-setting.php");?>
    <a class="dropdown-toggle nav-link <?=(in_array(basename($_SERVER["SCRIPT_FILENAME"]), $fil_staff)=="staff.php"?"active":"")?>" data-toggle="dropdown" href="#" aria-expanded="false">Company</a>
    <div class="dropdown-menu dropdown-menu-right" role="menu">
      <a class="dropdown-item <?=(basename($_SERVER["SCRIPT_FILENAME"])=="about.php"||basename($_SERVER["SCRIPT_FILENAME"])=="about-setting.php"?"active":"")?>" href="about.php" aria-expanded="true">About Company</a>
      <a class="dropdown-item <?=(basename($_SERVER["SCRIPT_FILENAME"])=="board.php"?"active":"")?>" href="board.php" aria-expanded="true">Board of Director</a>
      <a class="dropdown-item <?=(basename($_SERVER["SCRIPT_FILENAME"])=="policy.php"?"active":"")?>" href="policy.php" aria-expanded="true">Policy</a>
    </div>
  </li>
  <li class="nav-item dropdown hidden-sm-down">
    <?php $fil_staff = array("staff.php", "org.php", "org-setting.php","staff.php","staff-bd.php");?>
    <a class="dropdown-toggle nav-link <?=(in_array(basename($_SERVER["SCRIPT_FILENAME"]), $fil_staff)=="staff.php"?"active":"")?>" data-toggle="dropdown" href="#" aria-expanded="false">Staff</a>
    <div class="dropdown-menu dropdown-menu-right" role="menu">
      <a class="dropdown-item <?=(basename($_SERVER["SCRIPT_FILENAME"])=="org.php"||basename($_SERVER["SCRIPT_FILENAME"])=="org-setting.php"?"active":"")?>" href="org.php" aria-expanded="true">Org Chart</a>
      <a class="dropdown-item <?=(basename($_SERVER["SCRIPT_FILENAME"])=="staff.php"?"active":"")?>" href="staff.php" aria-expanded="true">Staff Directory</a>
      <a class="dropdown-item <?=(basename($_SERVER["SCRIPT_FILENAME"])=="staff-bd.php"?"active":"")?>" href="staff-bd.php" aria-expanded="true">Staff's birthday</a>
      <a class="dropdown-item <?=(basename($_SERVER["SCRIPT_FILENAME"])=="staff-promote-welcome.php"?"active":"")?>" href="staff-promote-welcome.php" aria-expanded="true">Promote/Welcome Staff</a>
    </div>
  </li>
  <li class="nav-item hidden-sm-down">
    <a class="nav-link <?=(substr(basename($_SERVER["SCRIPT_FILENAME"]),0,4)=="news"?"active":"")?>" href="news.php" aria-expanded="false">News</a>
  </li>
  <li class="nav-item hidden-sm-down">
    <a class="nav-link <?=(substr(basename($_SERVER["SCRIPT_FILENAME"]),0,7)=="announc"?"active":"")?>" href="announcement.php" aria-expanded="false">Announcement</a>
  </li>

  <li class="nav-item dropdown hidden-sm-down">
    <?php $fil_staff = array("photos.php", "videos.php","staff.php","e-book.php");?>
    <a class="dropdown-toggle nav-link <?=(in_array(basename($_SERVER["SCRIPT_FILENAME"]), $fil_staff)=="staff.php"?"active":"")?>" data-toggle="dropdown" href="#" aria-expanded="false">Media</a>
    <div class="dropdown-menu dropdown-menu-right" role="menu">
      <a class="dropdown-item <?=(basename($_SERVER["SCRIPT_FILENAME"])=="photos.php"?"active":"")?>" href="photos.php" aria-expanded="true">Photos</a>
      <a class="dropdown-item <?=(basename($_SERVER["SCRIPT_FILENAME"])=="videos.php"?"active":"")?>" href="videos.php" aria-expanded="true">Videos</a>
    </div>
  </li>
  <li class="nav-item hidden-sm-down">
    <a class="nav-link <?=(basename($_SERVER["SCRIPT_FILENAME"])=="drive.php"?"active":"")?>" href="drive.php" aria-expanded="false">Drive Center</a>
  </li>
  <li class="nav-item dropdown hidden-sm-down">
    <?php $fil_event = array("calendar.php", "events-grid.php");?>
    <a class="dropdown-toggle nav-link <?=(in_array(basename($_SERVER["SCRIPT_FILENAME"]), $fil_event)=="calendar.php"?"active":"")?>" data-toggle="dropdown" href="#" aria-expanded="false">Calendar/Events</a>
    <div class="dropdown-menu dropdown-menu-right" role="menu">
      <a class="dropdown-item <?=(basename($_SERVER["SCRIPT_FILENAME"])=="calendar.php"?"active":"")?>" href="calendar.php" aria-expanded="true">Calendar</a>
      <a class="dropdown-item <?=(basename($_SERVER["SCRIPT_FILENAME"])=="events-grid.php"?"active":"")?>" href="events-grid.php" aria-expanded="true">Events</a>
    </div>
  </li>
  <li class="nav-item hidden-sm-down">
    <a class="nav-link <?=(basename($_SERVER["SCRIPT_FILENAME"])=="links.php"?"active":"")?>" href="links.php" aria-expanded="false">Links</a>
  </li>
  <li class="nav-item hidden-sm-down">
    <a class="nav-link <?=(basename($_SERVER["SCRIPT_FILENAME"])=="poll.php"?"active":"")?>" href="poll.php" aria-expanded="false">Vote/Poll</a>
  </li>
  <li class="nav-item hidden-sm-down">
    <a class="nav-link <?=(basename($_SERVER["SCRIPT_FILENAME"])=="banner.php"?"active":"")?>" href="banner.php" aria-expanded="false">Banner</a>
  </li>
  <li class="nav-item dropdown hidden-md-up">
    <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#" aria-expanded="false">Menu </a>
    <div class="dropdown-menu dropdown-menu-right" role="menu">
      <a class="dropdown-item" href="leadership.php" aria-expanded="true">Org Chart</a>
      <a class="dropdown-item" href="staff.php" aria-expanded="true">Staff</a>
      <a class="dropdown-item" href="news.php" aria-expanded="true">News</a>
      <a class="dropdown-item" href="announcement.php" aria-expanded="true">Announcement</a>
      <a class="dropdown-item" href="photos.php" aria-expanded="true">Photos</a>
      <a class="dropdown-item" href="videos.php" aria-expanded="true">Videos</a>
      <a class="dropdown-item" href="events-grid.php" aria-expanded="true">Events</a>
      <a class="dropdown-item" href="calendar.php" aria-expanded="true">Calendar</a>
      <a class="dropdown-item" href="policy.php" aria-expanded="true">Policy</a>
    </div>
  </li>


</ul>
