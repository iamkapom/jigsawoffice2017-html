<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../assets/examples/css/forms/layouts.css">
  <link rel="stylesheet" href="../../global/vendor/icheck/icheck.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition page-aside-left">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-10">Setting</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item active">Apps</li>
        <li class="breadcrumb-item active"><a href="index.php">Intranet</a></li>
        <li class="breadcrumb-item active"><a href="news.php">News</a></li>
        <li class="breadcrumb-item active">Setting</li>
      </ol>
      <div class="page-header-actions">
        <button type="button" onclick="location.href='news-setting.php';" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Setting">
          <i class="icon md-settings" aria-hidden="true"></i>
        </button>
      </div>

      <?php include("mini-nav.php");?>
    </div>
    <div class="page-content container-fluid" style="position: relative;">
      <div class="page-aside">
          <div class="page-aside-switch">
            <i class="icon md-chevron-left" aria-hidden="true"></i>
            <i class="icon md-chevron-right" aria-hidden="true"></i>
          </div>
          <div class="page-aside-inner" data-plugin="pageAsideScroll">
            <div data-role="container">
              <div data-role="content">
                <section class="page-aside-section pt-0">
                  <h4 class="page-aside-title " style="white-space:normal;">Intranet Settings</h4>
                  <div class="list-group">
                    
                    <a class="list-group-item" href="index-setting.php">
                      <span class="">Admin menu</span>
                    </a>
                    <a class="list-group-item" href="org-setting.php">
                      <span class="">Org</span>
                    </a>
                    <a class="list-group-item" href="news-setting.php">
                      <span class="">News</span>
                    </a>
                    <a class="list-group-item" href="announcement-setting.php">
                      <span class="">Announcement</span>
                    </a>
                    <a class="list-group-item" href="photos-setting.php">
                      <span class="">Photos</span>
                    </a>
                    <a class="list-group-item active" href="videos-setting.php">
                      <span class="">Videos</span>
                    </a>
                    <a class="list-group-item" href="drive-setting.php">
                      <span class="">Drive</span>
                    </a>
                    <a class="list-group-item" href="calendar-setting.php">
                      <span class="">Event/Calendar</span>
                    </a>
                    <a class="list-group-item" href="links-setting.php">
                      <span class="">Links</span>
                    </a>
                    <a class="list-group-item" href="vote-setting.php">
                      <span class="">Vote/Poll</span>
                    </a>
                    <a class="list-group-item" href="policy-setting.php">
                      <span class="">Policy</span>
                    </a>

                  </div>
                </section>
              </div>
            </div>
          </div>
        </div>
      <div class="page-main">
        <div class="row pt-20 ml-0 mr-0">
          <div class="col-md-6">
            <div class="card card-shadow card-bordered">
              <div class="card-header p-15 clearfix">
                <h4>Category</h4>
              </div>
              <div class="card-block p-20">
                  <ul class="list-group list-group-dividered list-group-full">
                    <?php 
                    for($aa=1;$aa<5;$aa++){?>
                    <li class="list-group-item clearfix">
                      <strong>Category <?=$aa?></strong>
                      <div class="float-right">
                        <span class="btn btn-pure btn-icon waves-effect waves-classic" data-toggle="list-editable"><i class="icon md-edit" aria-hidden="true"></i></span>
                        <span class="btn btn-pure btn-icon waves-effect waves-classic" data-toggle="list-delete"><i class="icon md-delete" aria-hidden="true"></i></span>
                      </div>
                    </li>
                    <?php }?>
                  </ul>
                
                  <a class="btn btn-default btn-pure text-primary" href="#" onclick="$('#cat-frm').slideDown();"><i class="icon md-plus" aria-hidden="true"></i>Add Category</a>
                  <div id="cat-frm" class="pt-20" style="display: none;">
                    <form class="add-item" role="form" method="post" action="#">
                      <div class="form-group">
                        <h5>Category name:</h5>
                        <input class="form-control" placeholder="Category name" name="name" type="text">
                      </div>
                      <div class="form-group text-right">
                        <a class="btn btn-default btn-pure mr-10">Cancel</a>
                        <button type="button" class="btn btn-primary">Add</button>
                      </div>
                    </form>
                  </div>
                
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card card-shadow card-bordered">
              <div class="card-header p-15 clearfix">
                <h4>Admin News</h4>
              </div>
              <div class="card-block p-20">
                  <ul class="list-group list-group-dividered list-group-full">
                    <?php 
                    for($aa=1;$aa<=3;$aa++){?>
                    <li class="list-group-item clearfix">
                      <a class="avatar avatar-lg float-left mr-20" href="javascript:void(0)">
                        <img src="../../../global/portraits/<?=$aa++?>.jpg" alt="">
                      </a>
                      <div class="float-left text-primary">
                        Robin Ahrens
                        <div class="font-size-12 grey-600">robin.a@jigsawoffice.com</div>
                      </div>
                      <div class="float-right">
                        <span class="btn btn-pure btn-icon waves-effect waves-classic" data-toggle="list-delete"><i class="icon md-delete" aria-hidden="true"></i></span>
                      </div>
                    </li>
                    <?php }?>
                  </ul>
                  <a class="btn btn-default btn-pure text-primary" href="#" onclick="$('#admin-frm').slideDown();"><i class="icon md-plus" aria-hidden="true"></i>Add Admin</a>
                  <div id="admin-frm" class="pt-20" style="display: none;">
                    <form class="add-item" role="form" method="post" action="#">
                      <div class="form-group">
                        <h5>Select to add Admin News:</h5>
                        <select id="selectTeam4" class="form-control" multiple style="width:100%">
                        <?php
                        $teams_arr[] = "Ada.Hoppe";
                        $teams_arr[] = "Adrianna_Durgan";
                        $teams_arr[] = "Albin.Kreiger";
                        $teams_arr[] = "Alisa";
                        $teams_arr[] = "August";
                        $teams_arr[] = "Bell.Mueller";
                        $teams_arr[] = "Bret";
                        $teams_arr[] = "Ceasara_Orn";
                        $teams_arr[] = "Chester";
                        $teams_arr[] = "Citlalli_Wehner";
                        $teams_arr[] = "Clementina";
                        $teams_arr[] = "Coby";
                        $teams_arr[] = "Colin";
                        $teams_arr[] = "Damon";
                        $teams_arr[] = "Davin";
                        $teams_arr[] = "Elliott_Becker";
                        $teams_arr[] = "Emerson";
                        $teams_arr[] = "Gerhard";
                        $teams_arr[] = "Gunnar";
                        $teams_arr[] = "Gunner_Jakubowski";
                        $teams_arr[] = "Heath.Ryan";
                        $teams_arr[] = "Herta";
                        $teams_arr[] = "Hubert";
                        $teams_arr[] = "Jarvis.Simonis";
                        $teams_arr[] = "Jennie";
                        $teams_arr[] = "Johanna.Thiel";
                        $teams_arr[] = "Johnathan_Mraz";
                        $teams_arr[] = "Josephine";
                        $teams_arr[] = "Lacey";
                        $teams_arr[] = "Marjorie.Orn";
                        $teams_arr[] = "Mckenna.Herman";
                        $teams_arr[] = "Melany_Gerhold";
                        $teams_arr[] = "Miracle";
                        $teams_arr[] = "Monica";
                        $teams_arr[] = "Monique_Whitea";
                        $teams_arr[] = "Myriam_Nicolas";
                        $teams_arr[] = "Myrtie.Gerhold";
                        $teams_arr[] = "Raina";
                        $teams_arr[] = "Ruben.Reilly";
                        $teams_arr[] = "Sammie";
                        $teams_arr[] = "Shanel";
                        $teams_arr[] = "Stone_Deckow";
                        $teams_arr[] = "Terrance.Borer";
                        $teams_arr[] = "Thea";
                        $teams_arr[] = "Torrey";
                        $teams_arr[] = "Treva";
                        $teams_arr[] = "Wilhelmine";
                        $teams_arr[] = "Yasmine";
                        
                        for($bb=0;$bb<count($teams_arr);$bb++){
                          echo '<option value="'.$teams_arr[$bb].'">'.$teams_arr[$bb].'</option>';
                        }
                        ?>
                        </select>
                      </div>
                      <div class="form-group text-right">
                        <a class="btn btn-default btn-pure mr-10">Cancel</a>
                        <button type="button" class="btn btn-primary">Add</button>
                      </div>
                    </form>
                  </div>
              </div>
            </div>

            
          </div>
          <div class="col-md-12">
            <div class="card card-shadow card-bordered">
              <div class="card-header p-15 clearfix">
                <h4>News Permission</h4>
              </div>
              <div class="card-block p-20">
                <div class="form-group form-material">
                  <label class="form-control-label">Who can create news: </label>
                  <div >
                    <ul class="list-unstyled example">
                      <li class="mb-15">
                        <input type="radio" class="icheckbox-primary" id="inputRadiosUnchecked11" name="inputRadios11"
                        data-plugin="iCheck" data-radio-class="iradio_flat-blue" />
                        <label for="inputRadiosUnchecked11">Anyone can create.</label>
                      </li>
                      <li class="mb-15">
                        <input type="radio" class="icheckbox-primary" id="inputRadiosChecked12" name="inputRadios11"
                        data-plugin="iCheck" data-radio-class="iradio_flat-blue"/>
                        <label for="inputRadiosChecked12">Only admin can create.</label>
                      </li>
                      <li class="mb-15">
                        <input type="radio" class="icheckbox-primary" id="inputRadiosUnchecked11" name="inputRadios11"
                        data-plugin="iCheck" data-radio-class="iradio_flat-blue" checked />
                        <label for="inputRadiosUnchecked11">Anyone can create and all news must be approved by admin. (Recommended)</label>
                      </li>
                    </ul>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <script src="../../global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
  <script src="../../global/vendor/icheck/icheck.min.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../assets/js/BaseApp.js"></script>
  <script src="../../assets/js/App/Contacts.js"></script>
  <script src="../../assets/examples/js/apps/contacts.js"></script>
  <script src="../../global/js/Plugin/jquery-placeholder.js"></script>
  <script src="../../global/js/Plugin/icheck.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
<?php include("../_footer-form.php");?>
</body>
</html>