<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../global/vendor/webui-popover/webui-popover.css">
  <link rel="stylesheet" href="../../global/vendor/toolbar/toolbar.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
  <style type="text/css">
    .timeline-content {
      overflow: inherit;
  }
  </style>
</head>
<body class="animsition">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
  <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-10">Staff Directory</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item active">Apps</li>
        <li class="breadcrumb-item active"><a href="index.php">Intranet</a></li>
        <li class="breadcrumb-item active">Staff Directory</li>
      </ol>
  <?php include("mini-nav.php");?>
    </div>

    <div class="page-content container-fluid " style="position: relative;">

      <div class="row ml-0 mr-0">
        <div class="col-md-12">
              
          <div class="page-main">
            <div class="pt-0 pb-20">
              <div class="search-wrapper">
                <div class="search-box">
                  <div class="icon md-search"></div>
                  <div class="currently-showing">
                    <?=$search['tokenhtml']?>
                  </div>
                </div>
                <a href="javascript:void(0);" class="clear_all btn btn-flat btn-default btn-block waves-effect waves-classic">Clear All</a>
                <div class="data_entry">
                  <input class="input keyword-input" placeholder="Enter a keyword" type="text">
                  <div class="icon md-close-circle close"></div>
                </div>
                <div class="filters">
                  <div class="dropdown filter_permission">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      Department
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu" role="menu">
                      <a class="dropdown-item" href="javascript:void(0)">Accounting</a>
                      <a class="dropdown-item" href="javascript:void(0)">Human Resources</a>
                      <a class="dropdown-item" href="javascript:void(0)">IT</a>
                      <a class="dropdown-item" href="javascript:void(0)">Marketing</a>
                      <a class="dropdown-item" href="javascript:void(0)">Sales</a>
                    </div>
                  </div>
                  <div class="dropdown filter_permission">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      Type
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu" role="menu">
                      <a class="dropdown-item" href="javascript:void(0)">Promote</a>
                      <a class="dropdown-item" href="javascript:void(0)">Welcome</a>
                    </div>
                  </div>
                  <div class="dropdown filter_permission">
                      <button type="button" id="SearchDateRange" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Time <i class="icon md-chevron-down" aria-hidden="true"></i>
                        <span id="daterange-value"></span>
                      </button>
                    </div>
                </div>
              </div>
            </div>
            
            <ul class="timeline timeline-icon">
              <?php for($aa=1;$aa<10;$aa++){?>
              <li class="timeline-item">
                <div class="timeline-dot">
                  <i class="icon fa-address-card-o" aria-hidden="true"></i>
                </div>
                <div class="timeline-content">
                  <div class="card card-article card-bordered card-shadow post-wall">
                    <div class="card-header white bg-blue-600 p-30 clearfix">
                      <a class="avatar avatar-100 float-left mr-20 bg-white img-bordered" href="javascript:void(0)">
                        <img src="../../../global/portraits/5.jpg" alt="">
                      </a>
                      <div class="float-left">
                        <div class="font-size-20">Robin Ahrens (Rob)</div>
                        <div class="font-size-14  mb-15">Web Designer</div>

                        <p class="mb-5 text-nowrap"><i class="icon md-email mr-10" aria-hidden="true"></i>
                          <span class="text-break">malinda.h@gmail.com</span>
                        </p>
                        <p class="mb-5 text-nowrap"><i class="icon fa-mobile mr-10" aria-hidden="true"></i>
                          <span class="text-break">9192372533</span>
                        </p>
                        <p class="mb-5 text-nowrap"><i class="icon fa-phone mr-10" aria-hidden="true"></i>
                          <span class="text-break">123</span>
                        </p>
                      </div>
                    </div>
                    <div class="card-block pb-0">
                      <h3 class="card-title">Welcome aboard Rob!</h3>
                      <p class="card-text type-link">
                      <small>
                        <a href="javascript:void(0)">05, 2017</a>
                      </small>
                    </p>
                      <blockquote class="blockquote cover-quote vertical-align-middle font-size-20">Our office is very honour to welcome you. Thank you for choosing us as your work company. Our team is very happy to have you. We believed that you can help a lot to move our company to its next level.</blockquote>

                      <ul class="wall-attrs clearfix p-0 m-0 mt-10">
                        <li class="attrs-meta float-left font-size-16">
                          <a href="javascript:void(0)" class="mr-20">
                            <i class="icon md-thumb-up"></i>
                            <span>5</span>
                          </a>
                          <a href="javascript:void(0)" onclick="$('#wall-comment-reply-1').toggle();">
                            <i class="icon md-comment"></i>
                            <span>2</span>
                          </a>
                        </li>
                        <li class="float-right">
                          <div class="btn-group bootstrap-select btn-comment-post">
                            <select data-plugin="selectpicker">
                              <option>Top Comments</option>
                              <option>Most Recent</option>
                            </select>
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="wall-comment-attrs">
                      <div id="wall-comment-reply-1" class="wall-comment-reply clearfix" style="display: none">
                        <div href="#" class="avatar avatar-md float-left">
                          <img src="../../../global/portraits/6.jpg">
                        </div>
                        <div class="ml-60 wall-comment-form">
                          <textarea rows="1" class="autoHeight" placeholder="Write a comment..."></textarea>
                          <div class="wall-comment-reply-attach">
                            <button class="btn btn-icon btn-dark btn-round bg-grey-400">
                              <i class="icon md-attachment-alt rotate" aria-hidden="true"></i>
                            </button>
                          </div>
                        </div>
                      </div>
                      <div class="wall-comment">
                        <div class="wall-comment-action">
                          <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                            <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                          </button>

                          <div class="dropdown-menu dropdown-menu-right" role="menu">
                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                              Edit Comment
                            </a>
                            <a class="dropdown-item text-danger" href="javascript:void(0)" role="menuitem">
                              Delete
                            </a>
                          </div>
                        </div>
                          <a href="#" class="avatar avatar-md float-left">
                            <img src="../../../global/portraits/3.jpg">
                          </a>
                          <div class="ml-60 box-post-comment">
                            <a href="#">Stacey Hunt</a>
                            <span class="ml-10">Having you in our company is really a great honour! Indeed, all of us here are very excited to work with you. Welcome aboard new buddy!</span>
                              <p class="font-size-12 mt-5 grey-600">
                                <a href="javascript:void(0)">
                                  Like
                                </a>
                                <span class="mr-5 ml-5">&#8226;</span>
                                <a href="javascript:void(0)" class="action-reply">
                                  Reply
                                </a>
                                <span class="mr-5 ml-5">&#8226;</span>
                                <span >
                                  <i class="icon md-favorite"></i>
                                  <span>5</span>
                                </span>
                                <span class="mr-5 ml-5">&#8226;</span>
                                <span>
                                  30th July 2017
                                </span>
                              </p>
                            <div class="display-reply"></div>

                          </div>

                        </div>
                        <div class="wall-comment">
                          <a href="#" class="avatar avatar-md float-left">
                            <img src="../../../global/portraits/4.jpg">
                          </a>
                          <div class="ml-60 box-post-comment">
                            <a href="#">Sam Anderson</a>
                            <span class="ml-10">Have fun and enjoy our company.</span>
                            <p class="font-size-12 mt-5 grey-500">
                              <a href="javascript:void(0)">
                                Like
                              </a>
                              <span class="mr-5 ml-5">&#8226;</span>
                              <a href="javascript:void(0)" class="action-reply">
                                Reply
                              </a>
                              <span class="mr-5 ml-5">&#8226;</span>
                              <span >
                                <i class="icon md-favorite"></i>
                                <span>5</span>
                              </span>
                              <span class="mr-5 ml-5">&#8226;</span>
                              <span>
                                30th July 2017
                              </span>
                            </p>

                            <div class="display-reply"></div>
                          </div>

                        </div>
                      </div>
                  </div>
                  
                </div>
              </li>

              <li class="timeline-item timeline-reverse">
                <div class="timeline-dot bg-green-500">
                  <i class="icon fa-bullhorn" aria-hidden="true"></i>
                </div>
                <div class="timeline-content">
                  <div class="card card-bordered card-shadow post-wall">
                    <div class="card-header card-header-transparent cover overlay bg-green-500" style="height: calc(100% - 100px); max-height: 250px;">
                      <img class="cover-image" src="img/celebrate-<?=rand(1,3)?>.jpg" alt="..." style="height: 100%; opacity: 0.25;">
                      <div class="overlay-panel vertical-align text-center">
                        <div class="vertical-align-middle">
                          <a class="avatar avatar-100 bg-white mb-10 m-xs-0 img-bordered" href="javascript:void(0)">
                            <img src="../../../global/portraits/1.jpg" alt="">
                          </a>
                          <div class="font-size-20">Herman Beck</div>
                          <div class="font-size-14">Assistant Program Manager</div>
                        </div>
                      </div>
                    </div>
                    <div class="card-block">
                      <h3 class="card-title">Congratulations!</h3>
                      <p class="card-text type-link">
                      <small>
                        <a href="javascript:void(0)">05, 2017</a>
                      </small>
                    </p>
                    <blockquote class="blockquote cover-quote vertical-align-middle font-size-20">Your ability has taken you to the new height of success, this great job is all because of your dedication and hard work and yes confidence too, many congrats to you, stay blessed!</blockquote>

                      <ul class="wall-attrs clearfix p-0 m-0 mt-10">
                        <li class="attrs-meta float-left font-size-16">
                          <a href="javascript:void(0)" class="mr-20">
                            <i class="icon md-thumb-up"></i>
                            <span>5</span>
                          </a>
                          <a href="javascript:void(0)" onclick="$('#wall-comment-reply-1').toggle();">
                            <i class="icon md-comment"></i>
                            <span>2</span>
                          </a>
                        </li>
                        <li class="float-right">
                          <div class="btn-group bootstrap-select btn-comment-post">
                            <select data-plugin="selectpicker">
                              <option>Top Comments</option>
                              <option>Most Recent</option>
                            </select>
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="wall-comment-attrs">
                      <div id="wall-comment-reply-1" class="wall-comment-reply clearfix" style="display: none">
                        <div href="#" class="avatar avatar-md float-left">
                          <img src="../../../global/portraits/6.jpg">
                        </div>
                        <div class="ml-60 wall-comment-form">
                          <textarea rows="1" class="autoHeight" placeholder="Write a comment..."></textarea>
                          <div class="wall-comment-reply-attach">
                            <button class="btn btn-icon btn-dark btn-round bg-grey-400">
                              <i class="icon md-attachment-alt rotate" aria-hidden="true"></i>
                            </button>
                          </div>
                        </div>
                      </div>
                      <div class="wall-comment">
                        <div class="wall-comment-action">
                          <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                            <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                          </button>

                          <div class="dropdown-menu dropdown-menu-right" role="menu">
                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                              Edit Comment
                            </a>
                            <a class="dropdown-item text-danger" href="javascript:void(0)" role="menuitem">
                              Delete
                            </a>
                          </div>
                        </div>
                          <a href="#" class="avatar avatar-md float-left">
                            <img src="../../../global/portraits/13.jpg">
                          </a>
                          <div class="ml-60 box-post-comment">
                            <a href="#">Stacey Hunt</a>
                            <span class="ml-10">Congratulations! Working with you was a bliss to all of us.</span>
                              <p class="font-size-12 mt-5 grey-600">
                                <a href="javascript:void(0)">
                                  Like
                                </a>
                                <span class="mr-5 ml-5">&#8226;</span>
                                <a href="javascript:void(0)" class="action-reply">
                                  Reply
                                </a>
                                <span class="mr-5 ml-5">&#8226;</span>
                                <span >
                                  <i class="icon md-favorite"></i>
                                  <span>5</span>
                                </span>
                                <span class="mr-5 ml-5">&#8226;</span>
                                <span>
                                  30th July 2017
                                </span>
                              </p>
                            <div class="display-reply"></div>

                          </div>

                        </div>
                        <div class="wall-comment">
                          <a href="#" class="avatar avatar-md float-left">
                            <img src="../../../global/portraits/6.jpg">
                          </a>
                          <div class="ml-60 box-post-comment">
                            <a href="#">Sam Anderson</a>
                            <span class="ml-10">Congratulations!</span>
                            <p class="font-size-12 mt-5 grey-500">
                              <a href="javascript:void(0)">
                                Like
                              </a>
                              <span class="mr-5 ml-5">&#8226;</span>
                              <a href="javascript:void(0)" class="action-reply">
                                Reply
                              </a>
                              <span class="mr-5 ml-5">&#8226;</span>
                              <span >
                                <i class="icon md-favorite"></i>
                                <span>5</span>
                              </span>
                              <span class="mr-5 ml-5">&#8226;</span>
                              <span>
                                30th July 2017
                              </span>
                            </p>

                            <div class="display-reply"></div>
                          </div>

                        </div>
                      </div>
                  </div>
                </div>
              </li>
            <?php }?>

        </ul>

            
          </div>
          </div>
        </div>

    </div>
  </div>
  <div class="site-action" data-plugin="actionBtn">
    <button type="button"  class="btn-raised btn btn-success btn-floating">
      <i class="front-icon md-plus" aria-hidden="true"></i>
    </button>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <script src="../../global/vendor/webui-popover/jquery.webui-popover.min.js"></script>
  <script src="../../global/vendor/toolbar/jquery.toolbar.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../assets/examples/js/structure/timeline.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
      function chooseDateRange(start, end) {
          var DrangeValue = start.format('MM/DD/YYYY') + '-' + end.format('MM/DD/YYYY');
          $('#SearchDateRange span#daterange-value').html(DrangeValue);
          var dataKey = "123";
          var dataValue = "xxx";
          var stoken = '';
            $('.search-wrapper .currently-showing .chooseDateRange').remove();
            stoken = '<div class="token chooseDateRange" data-key="'+dataKey+'" data-value="' + dataValue + '"><span class="token-label" >' + DrangeValue + '</span><a  href="#" class="close" tabindex="-1">×</a></div>';
            $('.search-wrapper .currently-showing').append(stoken);
            sysGenUrlSearch();
      }

      $('#SearchDateRange').daterangepicker({
          "autoApply": true,
          "opens": "center",
          ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
      }, chooseDateRange);
    });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>
