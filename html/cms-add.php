<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../assets/examples/css/forms/layouts.css">
  <link rel="stylesheet" href="../../global/vendor/summernote/summernote.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.css">
  <link rel="stylesheet" href="../../assets/examples/css/forms/advanced.css">
  <link rel="stylesheet" href="../../global/vendor/aspieprogress/asPieProgress.css">
  <link rel="stylesheet" href="../../assets/examples/css/charts/pie-progress.css">
  <link rel="stylesheet" href="../../global/vendor/blueimp-file-upload/jquery.fileupload.css">
  <link rel="stylesheet" href="../../global/vendor/dropify/dropify.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition site-menubar-fold site-menubar-keep page-aside-right">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header">
      <h1 class="page-title">Add/Edit Post</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../home/home.php">Home</a></li>
        <li class="breadcrumb-item active">Modules</li>
        <li class="breadcrumb-item active">CMS</li>
        <li class="breadcrumb-item active">Add/Edit Post</li>
      </ol>
      <div class="page-header-actions">
        <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Setting">
          <i class="icon md-settings" aria-hidden="true"></i>
        </button>
      </div>
    </div>
    <div class="page-content container-fluid" style="position: relative;">
      <div class="page-aside" style="background:none;border-right:0;">
        <!-- Contacts Sidebar -->
        <div class="page-aside-switch">
          <i class="icon md-chevron-left" aria-hidden="true"></i>
          <i class="icon md-chevron-right" aria-hidden="true"></i>
        </div>
        <div class="page-aside-inner page-aside-scroll">
          <div data-role="container">
            <div data-role="content">
              <section class="page-aside-section">
                <div class="panel panel-bordered" style="margin-top: -20px;">
                  <div class="panel-heading">
                    <h3 class="panel-title pl-20">Public</h3>
                  </div>
                  <div class="panel-body p-20">
                      <button type="button" class="btn active waves-effect waves-classic ml-10">Save Draft</button>
                      <button type="button" class="btn active waves-effect waves-classic ml-10">Preview</button>
                      <p class="mt-15">
                        <span class="w-60" style="display:inline-block;">Status: </span><strong>Draft</strong> <a class="pl-10" href="#">Edit</a>
                      </p>
                      <p class="mt-15">
                        Publish <strong>immediately</strong> <a class="pl-10" href="#">Edit</a>
                      </p>
                  </div>
                  <div class="panel-footer pl-20">
                    <a class="text-danger" href="#">Move to Trash</a>
                    <button style="float:right" type="button" class="btn btn-info active waves-effect waves-classic ml-10">Public</button>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="panel panel-bordered">
                  <div class="panel-heading">
                    <h3 class="panel-title pl-20">Categories</h3>
                  </div>
                  <div class="panel-body p-20">
                      <ul class="nav nav-pills mb-15" role="tablist">
                        <li class="nav-item"><a style="padding:5px 10px;" class="nav-link active" data-toggle="tab" href="#cardTab1" aria-controls="cardTab1" role="tab" aria-expanded="true" rip-style-bordercolor-backup="" style="" rip-style-borderstyle-backup="" rip-style-borderwidth-backup="">All Categories</a></li>
                        <li class="nav-item"><a style="padding:5px 10px;" class="nav-link" data-toggle="tab" href="#cardTab2" aria-controls="cardTab2" role="tab" aria-expanded="false">Most Used</a></li>
                      </ul>
                      <div class="tab-content">
                        <div class="tab-pane active" id="cardTab1" role="tabpanel" aria-expanded="true">
                            <?php for($ax=1;$ax<=6;$ax++){?>
                            <div class="checkbox-custom checkbox-primary">
                              <input type="checkbox" id="inputUnchecked<?php echo $ax;?>" />
                              <label for="inputUnchecked<?php echo $ax;?>">Categories <?php echo $ax;?></label>
                            </div>
                            <?php }?>
                        </div>
                        <div class="tab-pane" id="cardTab2" role="tabpanel" aria-expanded="false">
                            <div class="checkbox-custom checkbox-primary">
                              <input type="checkbox" id="inputUnchecked11" />
                              <label for="inputUnchecked11">Categories 2</label>
                            </div>
                            <div class="checkbox-custom checkbox-primary">
                              <input type="checkbox" id="inputUnchecked12" />
                              <label for="inputUnchecked12">Categories 3</label>
                            </div>
                        </div>
                      </div>
                  </div>
                  <div class="panel-footer pl-20">
                    <a class="pl-10" href="#">+ Add New Category</a>
                  </div>
                </div>
                <div class="panel panel-bordered">
                  <div class="panel-heading">
                    <h3 class="panel-title pl-20">Tags</h3>
                  </div>
                  <div class="panel-body p-20">
                      <input type="text" class="form-control" data-plugin="tokenfield" value="red,green,blue"/>
                  </div>
                </div>
              </section>
              
            </div>
          </div>
        </div>
      </div>
      <div class="page-main">
        <div class="panel">
          <div class="panel-body container-fluid">
            <div class="row">
              <div class="col-md-12">
                <!-- Example Basic Form -->
                <div class="example-wrap">
                  <div class="example">
                    <form autocomplete="off">
                      <div class="row">
                        <div class="form-group form-material col-md-12">
                          <input class="form-control form-control-lg" id="inputBasicFirstName" name="inputFirstName" value="What Is SEO / Search Engine Optimization?" placeholder="Enter title here " autocomplete="off" type="text">
                          <p class="mt-5">
                            <strong>Permalink:</strong> http://www.yourdomain.com/what-is-seo/
                            <button type="button" class="btn btn-xs active waves-effect waves-classic ml-10">Edit</button>
                            <button type="button" class="btn btn-xs active waves-effect waves-classic ml-10">View Post</button>
                          </p>
                        </div>
                      </div>
                      <div class="form-group form-material">
                        <div id="summernote" data-plugin="summernote">
                          <h2>It's Simpler Than You Think!</h2>
SEO or Search Engine Optimisation is the name given to activity that attempts to improve search engine rankings.
In search results Google™ displays links to pages it considers relevant and authoritative. Authority is mostly measured by analysing the number and quality of links from other web pages.
In simple terms your web pages have the potential to rank in Google™ so long as other web pages link to them.
                            <div class="mt-10">
                                  <div class="row">
                            <div class="col-sm-5"><img src="https://www.redevolution.com/images/what_we_do/seo/seo-aberdeen.png" alt="SEO Aberdeen" class="img-responsive" style="display: block; margin-left: auto; margin-right: auto;" title="SEO Aberdeen"></div>
                            <div class="col-sm-7">
                            <h2>Make Your Site Appear in Google&trade;</h2>
                            <p>Great Content encourages people to link to your pages and shows Google&trade; your pages are interesting and authoritative. This leads to search engine success because Google&trade; wants to show interesting and authoritative pages in its search results. It's that <em>simple!</em></p>
                            <p>Check our <a href="/seo-explained">SEO Explained in Pictures</a> page for a quick, simple overview. It's guaranteed to help you understand SEO.</p>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-sm-7">
                            <h2>How Does Google&trade; Rank Pages?</h2>
                            <p>Google&trade; promotes authority pages to the top of its rankings so it's your job to create pages that become authority pages. This involves writing content people find useful because useful content is shared in blogs, twitter feeds etc., and over time Google&trade; picks up on these authority signals. This virtuous circle creates strong and sustainable Google&trade; rankings.</p>
                            </div>
                            <div class="col-sm-5"><img src="https://www.redevolution.com/images/what_we_do/seo/google-ranks.png" alt="Google Ranks" class="img-responsive" style="display: block; margin-left: auto; margin-right: auto;" title="Google Ranks"></div>
                            </div>
                            <div class="row">
                            <div class="col-sm-12">
                            <h2 class="grey">A Simple <span class="red">1-2-3</span> Guide to Getting Better Search Results</h2>
                            </div>
                            <div class="col-sm-5">
                            <ul>
                            <li class="one">Write fantastic, useful content that uses words and phrases used by people who search for your products and services.</li>
                            <li class="two">Make it easy for people to share and link to it.</li>
                            <li class="three">Keep doing it!</li>
                            </ul>
                            </div>
                            <div class="col-sm-7">
                            <p><strong>Search Engine Optimisation</strong> or <strong><acronym title="search engine optimisation">SEO</acronym></strong> is the simple activity of ensuring a website can be found in search engines for words and phrases relevant to what the site is offering. In many respects it's simply quality control for websites. Having said that, if there was ever an industry that was little understood by <em>outsiders</em> it's SEO.</p>
                            <p>Ask some SEO companies about SEO and they'll try and blind you with science and confuse you into thinking it's a black art. Say to some companies <strong>what is SEO</strong> and two hours later you'll be none the wiser. Not so here at Red Evolution. We love seeing the light bulb go on when our clients get it. We prefer our clients to not only know what we are doing for them, but why!</p>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-sm-7">
                            <h2>How Do I Get Links?</h2>
                            <p>Links are important but don't confuse quality with quantity and don't think about links in isolation from your content. It's vital to understand that having great content massively increases your chances of securing natural links from quality relevant pages. These links will help you the most.</p>
                            <p>In simple language if you have something worth linking to you might secure links worth having. Great sites don't link to poor quality content, why would they?</p>
                            </div>
                            <div class="col-sm-5"><img src="https://www.redevolution.com/images/what_we_do/seo/link-building.png" alt="Link Building" class="img-responsive" style="display: block; margin-left: auto; margin-right: auto;" title="Link Building"></div>
                            </div>
                            <div class="row">
                            <div class="col-sm-5"><img src="https://www.redevolution.com/images/what_we_do/seo/summary-icon.png" alt="SEO Summary" class="img-responsive" style="display: block; margin-left: auto; margin-right: auto;" title="SEO Summary"></div>
                            <div class="col-sm-7">
                            <h2>In Summary</h2>
                            <p>Do your homework and understand what kind of content your potential customers are looking for. Create a great site, create content people need and make it easy for them to share it. Do this and you'll start to see your website traffic increase.</p>
                            <p>One last thing, if your website's not performing and you need some help, why not book a <a href="http://offers.redevolution.com/online-marketing-review" target="_blank">FREE 30 minute chat</a> to find out why. It's a 100% FREE no obligation offer to chat with one of our experts.</p>
                            </div>
                            </div>    </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
                <!-- End Example Basic Form -->
              </div>
            </div>
          </div>
        </div>
        <div class="panel">
          <div class="panel-body container-fluid">
            <h3>Documents/Photos/Media</h3>
            <div class="example">
              <input type="file" id="input-file-now" data-plugin="dropify" data-default-file=""
              />
            </div>
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>Name</th>
                  <th width="20%">Download</th>
                  <th class="hidden-sm-down" width="20%">Created</th>
                  <th class="hidden-sm-down" width="20%">File size</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <div class="media" style="flex-direction:initial;">
                      <div class="pr-20">
                        <a data-url="panel-file.html" data-toggle="slidePanel" class="font-size-24" href="javascript:void(0)">
                          <i style="color:#ff9800" class="icon fa-file-powerpoint-o" aria-hidden="true"></i>
                        </a>
                      </div>
                      <div class="media-body pt-10">
                        <h5><a style="color:#616161;text-decoration: none;" data-url="panel-folder.html" data-toggle="slidePanel" href="javascript:void(0)">TOR_Checklist</a></h5>
                        <p class="hidden-md-up mb-0 grey-500">
                          <small>
                            Mar 3, 2017 <span class="ml-10 mr-10">&mdash;</span> 2.85 MB                            </small>
                        </p>
                      </div>
                    </div>
                  </td>
                  <td class="pt-15">12</td>
                  <td class="hidden-sm-down pt-15">Mar 3, 2017</td>
                  <td class="hidden-sm-down pt-15">2.85 MB</td>
                </tr>
                <tr>
                  <td>
                    <div class="media" style="flex-direction:initial;">
                      <div class="pr-20">
                        <a data-url="panel-file.html" data-toggle="slidePanel" class="font-size-24" href="javascript:void(0)">
                          <i style="color:#f44336" class="icon fa-file-pdf-o" aria-hidden="true"></i>
                        </a>
                      </div>
                      <div class="media-body pt-10">
                        <h5><a style="color:#616161;text-decoration: none;" data-url="panel-folder.html" data-toggle="slidePanel" href="javascript:void(0)">wireframe inner</a></h5>
                        <p class="hidden-md-up mb-0 grey-500">
                          <small>
                            Mar 3, 2017 <span class="ml-10 mr-10">&mdash;</span> 4.26 KB                            </small>
                        </p>
                      </div>
                    </div>
                  </td>
                  <td class="pt-15">5</td>
                  <td class="hidden-sm-down pt-15">Mar 3, 2017</td>
                  <td class="hidden-sm-down pt-15">4.26 KB</td>
                </tr>
                                  <tr>
                  <td>
                    <div class="media" style="flex-direction:initial;">
                      <div class="pr-20">
                        <a data-url="panel-file.html" data-toggle="slidePanel" class="font-size-24" href="javascript:void(0)">
                          <i style="color:#4caf50" class="icon fa-file-excel-o" aria-hidden="true"></i>
                        </a>
                      </div>
                      <div class="media-body pt-10">
                        <h5><a style="color:#616161;text-decoration: none;" data-url="panel-folder.html" data-toggle="slidePanel" href="javascript:void(0)">เอกสารรวบรวม Data</a></h5>
                        <p class="hidden-md-up mb-0 grey-500">
                          <small>
                            Mar 3, 2017 <span class="ml-10 mr-10">&mdash;</span> 1.52 MB                            </small>
                        </p>
                      </div>
                    </div>
                  </td>
                  <td class="pt-15">32</td>
                  <td class="hidden-sm-down pt-15">Mar 3, 2017</td>
                  <td class="hidden-sm-down pt-15">1.52 MB</td>
                </tr>
                                  <tr>
                  <td>
                    <div class="media" style="flex-direction:initial;">
                      <div class="pr-20">
                        <a data-url="panel-file.html" data-toggle="slidePanel" class="font-size-24" href="javascript:void(0)">
                          <i style="color:#ffab00" class="icon fa-file-picture-o" aria-hidden="true"></i>
                        </a>
                      </div>
                      <div class="media-body pt-10">
                        <h5><a style="color:#616161;text-decoration: none;" data-url="panel-folder.html" data-toggle="slidePanel" href="javascript:void(0)">WEB Design Trend</a></h5>
                        <p class="hidden-md-up mb-0 grey-500">
                          <small>
                            Mar 3, 2017 <span class="ml-10 mr-10">&mdash;</span> 4.25 MB                            </small>
                        </p>
                      </div>
                    </div>
                  </td>
                  <td class="pt-15">8</td>
                  <td class="hidden-sm-down pt-15">Mar 3, 2017</td>
                  <td class="hidden-sm-down pt-15">4.25 MB</td>
                </tr>
                                </tbody>
            </table>
            
          </div>
        </div>
        <div class="panel">
          <div class="panel-body container-fluid">
            <div class="row">
              <div class="col-md-12">
                <h3>Snippet preview</h3>
                <div class="card card-bordered card-outline-primary">
                  <div class="card-block">
                    <h4 class="card-title">What is SEO? Here's A Simple Plain English Answer, SEO in a Nutshell</h4>
                    <p class="card-text">
                    <a href="#">https://www.yourcompanydomain.com/basic-seo/</a><br>
                    Wondering what search engine optimisation is all about. Here's a simple explanation of SEO. A straightforward answer to the question, what is SEO?
                    </p>
                  </div>
                </div>
                <button type="button" class="btn btn-dark waves-effect waves-classic"><i class="icon md-edit" aria-hidden="true"></i> Edit snippet</button>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="alert dark alert-alt alert-dismissible mt-30 mb-0" role="alert">
                  <i class="icon md-format-list-bulleted mr-10" aria-hidden="true"></i> Analysis
                </div>
                <div class="row">
                  <div class="col-md-3 pt-40">
                    <div style="max-width:80%;" class="pie-progress pie-progress" data-plugin="pieProgress" data-barcolor="#f3273c"
                    data-goal="95" aria-valuenow="95" data-size="100" data-barsize="8"
                    role="progressbar">
                      <div class="pie-progress-content">
                        <div class="pie-progress-number">95%</div>
                        <div class="pie-progress-label">SEO Score</div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-9">
                    <div class="p-10">
                      <div class="media pt-10">
                        <div>
                          <i class="icon md-circle orange-600 mr-10" aria-hidden="true"></i>
                        </div>
                        <div class="media-body">
                          25% of the sentences contain a transition word or phrase, which is less than the recommended minimum of 30%.
                        </div>
                      </div>
                      <div class="media pt-10">
                        <div>
                          <i class="icon md-circle green-600 mr-10" aria-hidden="true"></i>
                        </div>
                        <div class="media-body">
                          The copy scores 65.4 in the Flesh Reading Ease test, which is considered ok to read.
                        </div>
                      </div>
                      <div class="media pt-10">
                        <div>
                          <i class="icon md-circle green-600 mr-10" aria-hidden="true"></i>
                        </div>
                        <div class="media-body">
                          The amount of words following each of the subheadings doesn’t exceed the recommended maximum of 300 words, which is great.
                        </div>
                      </div>
                      <div class="media pt-10">
                        <div>
                          <i class="icon md-circle green-600 mr-10" aria-hidden="true"></i>
                        </div>
                        <div class="media-body">
                          None of the paragraphs are too long, which is great.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
  <script src="../../global/vendor/summernote/summernote.min.js"></script>
  <script src="../../global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.min.js"></script>
  <script src="../../global/vendor/aspieprogress/jquery-asPieProgress.js"></script>
  <script src="../../global/vendor/blueimp-tmpl/tmpl.js"></script>
  <script src="../../global/vendor/blueimp-canvas-to-blob/canvas-to-blob.js"></script>
  <script src="../../global/vendor/blueimp-load-image/load-image.all.min.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-process.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-image.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-audio.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-video.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-validate.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-ui.js"></script>
  <script src="../../global/vendor/dropify/dropify.min.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/GridMenu.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/jquery-placeholder.js"></script>
  <script src="../../assets/examples/js/forms/editor-summernote.js"></script>
  <script src="../../assets/examples/js/uikit/panel-structure.js"></script>
  <script src="../../global/js/Plugin/bootstrap-tokenfield.js"></script>
  <script src="../../assets/examples/js/forms/advanced.js"></script>
  <script src="../../global/js/Plugin/aspieprogress.js"></script>
  <script src="../../assets/examples/js/charts/pie-progress.js"></script>
  <script src="../../global/js/Plugin/dropify.js"></script>
  <script src="../../assets/examples/js/forms/uploads.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
  </script>
</body>
</html>