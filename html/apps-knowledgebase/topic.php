<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../assets/images/favicon.ico">
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../assets/examples/css/pages/profile_v3.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <style type="text/css">
    .list-group-item .list-group-item-content{
      font-weight: bold;
    }
  </style>
</head>
<body class="animsition page-aside-left">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <!-- Page -->
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-10">Knowledgebase Categories</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item active">Knowledgebase</li>
      </ol>
      <div class="page-header-actions">
        <button type="button" onclick="location.href='setting.php';" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Setting">
          <i class="icon md-settings" aria-hidden="true"></i>
        </button>
      </div>
    </div>
    
    
    <div class="page-content container-fluid" style="position: relative;">
      
      <div class="row ml-0 mr-0 mb-30">
        <div class="col-md-12">
          <section>
            <div class="pb-10">
              <div class="search-wrapper">
                <div class="search-box">
                  <div class="icon md-search"></div>
                  <div class="currently-showing">
                    <?=$search['tokenhtml']?>
                  </div>
                </div>
                <a href="javascript:void(0);" class="clear_all btn btn-flat btn-default btn-block waves-effect waves-classic">Clear All</a>
                <div class="data_entry">
                  <input class="input keyword-input" placeholder="Enter a keyword" type="text">
                  <div class="icon md-close-circle close"></div>
                </div>
                <div class="filters">  
                </div>
              </div>
            </div>
            </section>
      
        </div>
      </div>

      <div style="position: relative;">
        
        <div class="page-aside">
          <div class="page-aside-switch">
            <i class="icon md-chevron-left" aria-hidden="true"></i>
            <i class="icon md-chevron-right" aria-hidden="true"></i>
          </div>
          <div class="page-aside-inner" data-plugin="pageAsideScroll">
            <div data-role="container">
              <div data-role="content">
                <section class="page-aside-section pt-0">
                  <h4 class="page-aside-title " style="white-space:normal;">Main Categories</h4>
                  <div class="list-group">
                    <?php
                    $catt[] = "Getting Started";
                    $catt[] = "Security";
                    $catt[] = "Workspace";
                    $catt[] = "Drive";
                    $catt[] = "Task";
                    $catt[] = "Intranet";
                    $catt[] = "CRM";
                    $catt[] = "Calendar";
                    $catt[] = "Mini Apps";
                    $catt[] = "Billing";
                    $catt[] = "Other";
                    for($aa=0;$aa<count($catt);$aa++){
                    ?>
                    <a class="list-group-item <?=($aa<1?"active":"")?>" href="javascript:void(0)">
                      <span class="item-right"><?=rand(5,15)?></span>
                      <span class=""><?=$catt[$aa]?></span>
                    </a>
                    <?php }?>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </div>

        <div class="page-main">
          <div class="page-content px-30">
            <h1>Getting Started</h1>
            <p>If you're having trouble logging in, learn what you can do.</p>
            <div class="panel-group pb-30" id="exampleAccordionDefault" aria-multiselectable="true" role="tablist">
                <?php
                $head[] = "How do I log into my JigsawOffice account?";
                $head[] = "How do I log out of JigsawOffice?";
                $head[] = "How do I add or remove a saved account on my phone?";
                $head[] = "I don't know if I still have a JigsawOffice account.";
                $head[] = "I have two accounts. Can I merge them?";
                $head[] = "How can I make sure I don’t lose access to my account?";

                for($aa=0;$aa<count($head);$aa++){?>  
                  <div class="panel">
                    <div class="panel-heading" id="exampleHeadingDefaultOne" role="tab">
                      <a class="panel-title collapsed" data-toggle="collapse" href="#exampleCollapseDefaultOne<?=$aa?>" data-parent="#exampleAccordionDefault" aria-expanded="false" aria-controls="exampleCollapseDefaultOne<?=$aa?>">
                      <strong><?=$head[$aa]?></strong>
                      <div class="card-text type-link clearfix">
                        <small>
                        <div class=" float-left mr-20">
                          <span>Nathan Watts</span>
                          <span class="pl-10">05 May, 2017</span>
                        </div>
                        <div class="card-actions float-left mt-0">
                          <span class="pl-10">
                            <i class="icon md-favorite"></i>
                            <span>253</span>
                          </span>
                          <span class="pl-10">
                            <i class="icon md-comment"></i>
                            <span>115</span>
                          </span>
                          <span class="pl-10">
                          <i class="icon md-eye grey-500" aria-hidden="true"></i> <span class="grey-500">11</span>
                        </span>
                          
                        </div>

                        </small>
                      </div>
                      
                    </a>
                    </div>
                    <div class="panel-collapse collapse" id="exampleCollapseDefaultOne<?=$aa?>" aria-labelledby="exampleHeadingDefaultOne" role="tabpanel" aria-expanded="false" style="">
                      <div class="panel-body">
                        <div class="mb-30 panel-content-help" style="line-height: 30px;">

                          <div class="_z6x" id="u_b_0">
                            <div class="_4ng0">To <a class="_5dwo" href="/login">log into your JigsawOffice account</a>:</div>
                            <ol class="_1_ex">
                              <li class="_1_ez">Make sure no one else is logged into JigsawOffice on your computer 
                                <ul class="uiList _1_ey _4kg _6-h _6-j _6-i">
                                  <li class="_1_ez">To log someone else out, click <img class="_4nf_ _254 img" src="https://scontent.fbkk7-3.fna.fbcdn.net/v/t39.2365-6/851557_364200877036449_574807949_n.gif?oh=432a4cfffa456cee0bac095214a2d8dc&amp;oe=5AD03F27" alt=""> at the top right of your JigsawOffice homepage and select <b>Log Out</b></li>
                                </ul>
                              </li>
                              <li class="_1_ez">Go to the top of <a class="_5dwo" href="/" target="blank">www.JigsawOffice.com</a> and enter one of the following: 
                                <ul class="uiList _1_ey _4kg _6-h _6-j _6-i">
                                  <li class="_1_ez"><b>Email address:</b> You can log in with any email address that's listed on your JigsawOffice account</li>
                                  <li class="_1_ez"><b>Username:</b> You can also log in with your <a class="_5dwo" href="/help/211813265517027?helpref=faq_content">username</a></li>
                                  <li class="_1_ez"><b>Phone number:</b> If you have a mobile phone number confirmed on your account you can enter it here (skip the zeros before the country code and any symbols)</li>
                                </ul>
                              </li>
                                <li class="_1_ez">Enter your password</li>
                                <li class="_1_ez">Click <a class="_5dwo" href="/login"><b>Log In</b></a></li>
                              </ol>
                            </div>

                        </div>
                        <div class="px-30 py-15 bg-grey-200">
                          <div class="float-left w-p75">
                            <h5 class="m-0 mb-10">Was this article helpful to you ?</h5>
                            <button type="button" class="btn btn-xs px-20 btn-primary mr-10">Yes</button>
                            <button type="button" class="btn btn-xs px-20 btn-dark">No</button>
                          </div>
                          <div class="float-right pt-15">
                            <a href="javascript:void(0);">Permalink</a>
                          </div>
                          <div class="clearfix"></div>
                        </div>

                        <div class="wall-comment-attrs mt-20">
                          <div id="wall-comment-reply-1" class="wall-comment-reply clearfix">
                            <div href="#" class="avatar avatar-md float-left">
                              <img src="../../../global/portraits/6.jpg">
                            </div>
                            <div class="ml-60 wall-comment-form">
                              <textarea rows="1" class="autoHeight" placeholder="Write a comment..."></textarea>
                            </div>
                          </div>
                          <div class="wall-comment">
                            <div class="wall-comment-action">
                            <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                              <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                            </button>
                            
                            <div class="dropdown-menu dropdown-menu-right" role="menu">
                              <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                                Edit Comment
                              </a>
                              <a class="dropdown-item text-danger" href="javascript:void(0)" role="menuitem">
                                Delete
                              </a>
                            </div>
                          </div>
                            <a href="#" class="avatar avatar-md float-left">
                              <img src="../../../global/portraits/3.jpg">
                            </a>
                            <div class="ml-60 box-post-comment">
                              <a href="#">Stacey Hunt</a>
                              <span class="ml-10">Do incididunt elit ex incididunt ut. Aute velit proident cupidatat qui et consectetur
                                do. Consequat esse quis ut cupidatat ea. Sint dolore ea culpa
                                dolore velit enim.</span>
                                <p class="font-size-12 mt-5 grey-600">
                                  <a href="javascript:void(0)">
                                    Like
                                  </a>
                                  <span class="mr-5 ml-5">&#8226;</span>
                                  <a href="javascript:void(0)" class="action-reply">
                                    Reply
                                  </a>
                                  <span class="mr-5 ml-5">&#8226;</span>
                                  <span >
                                    <i class="icon md-favorite"></i>
                                    <span>5</span>
                                  </span>
                                  <span class="mr-5 ml-5">&#8226;</span>
                                  <span>
                                    30th July 2017
                                  </span>
                                </p>

                              <div class="wall-comment">
                                <a href="#" class="avatar avatar-md float-left">
                                  <img src="../../../global/portraits/11.jpg">
                                </a>
                                <div class="ml-60">
                                  <a href="#">Crystal Bates</a>
                                  <span class="ml-10">Do incididunt elit ex incididunt ut. Aute velit proident cupidatat qui et consectetur
                                    do. Consequat esse quis ut cupidatat ea.</span>
                                  <p class="font-size-12 mt-5 grey-500">
                                    <a href="javascript:void(0)">
                                      Like
                                    </a>
                                    <span class="mr-5 ml-5">&#8226;</span>
                                    <a href="javascript:void(0)" class="action-reply">
                                      Reply
                                    </a>
                                    <span class="mr-5 ml-5">&#8226;</span>
                                    <span >
                                      <i class="icon md-favorite"></i>
                                      <span>5</span>
                                    </span>
                                    <span class="mr-5 ml-5">&#8226;</span>
                                    <span>
                                      30th July 2017
                                    </span>
                                  </p>
                                </div>
                                
                              </div>
                              <div class="display-reply"></div>

                            </div>
                            
                          </div>
                          <div class="wall-comment">
                            <a href="#" class="avatar avatar-md float-left">
                              <img src="../../../global/portraits/5.jpg">
                            </a>
                            <div class="ml-60 box-post-comment">
                              <a href="#">Sam Anderson</a>
                              <span class="ml-10">Ut velit ipsum elit ut mollit elit. Proident officia eu ex consectetur ullamco
                                magna. Enim cillum voluptate sint ipsum ad voluptate exercitation.
                                Ex est amet magna occaecat eu.</span>
                              <p class="font-size-12 mt-5 grey-500">
                                <a href="javascript:void(0)">
                                  Like
                                </a>
                                <span class="mr-5 ml-5">&#8226;</span>
                                <a href="javascript:void(0)" class="action-reply">
                                  Reply
                                </a>
                                <span class="mr-5 ml-5">&#8226;</span>
                                <span >
                                  <i class="icon md-favorite"></i>
                                  <span>5</span>
                                </span>
                                <span class="mr-5 ml-5">&#8226;</span>
                                <span>
                                  30th July 2017
                                </span>
                              </p>

                              <div class="display-reply"></div>
                            </div>
                            
                          </div>
                        </div>

                      </div>

                    </div>
                  </div>
                <?php }?>  
              </div>
              
              <div class="card card-block">
                <h4>Followers</h4>
                <div class="media" style="flex-direction:initial;">
                  <div class="media-body">
                    <ul class="addMember-items">
                      <?php for($a=1;$a<=15;$a++){?>
                      <li class="addMember-item mr-10 mb-10">
                        <img class="avatar" src="../../global/portraits/<?=rand(1,15)?>.jpg" title="Herman Beck">
                      </li>
                      <?php }?>
                    </ul>
                    <div class="addMember-trigger font-size-16 avatar" style="margin-top: -5px; line-height: 40px; text-align: center; width: 40px; height: 40px; background: #f1f1f1;">
                      <a href="#" data-target="#followers" data-toggle="modal"><strong>+112</strong></a>
                    </div>
                  </div>
                </div>

              </div>

          </div>
        </div>

      </div>

    </div>
  </div>
  <!-- End Page -->
  <div class="site-action" data-plugin="actionBtn">
    <button type="button" onClick="location.href='form.php';" class="btn-raised btn btn-success btn-floating">
      <i class="front-icon md-plus" aria-hidden="true"></i>
    </button>
  </div>
  <div class="modal fade" id="followers" aria-hidden="true" aria-labelledby="Contetn Log" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple ">
        <form class="modal-content modal-content-no-border">
            <div class="modal-header">
              <h4 class="modal-title" id="exampleFillInModalTitle">Followers</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="icon md-close" aria-hidden="true"></i>
              </button>
            </div>
            <div class="modal-body">
                <div class="input-search input-search-dark m-10 ml-10">
                  <i class="input-search-icon md-search" aria-hidden="true" style="left:0;"></i>
                  <input class="form-control" name="" placeholder="Search Name" style="border-radius:0.215rem;" type="text">
                  <button type="button" class="input-search-close icon md-close" aria-label="Close" style="right:0;"></button>
                </div>
                <div data-role="content"  style="max-height: 300px; overflow-y: scroll;">
                      
                    <ul class="list-group">
                      <?php for($aa=1;$aa<=43;$aa++){?>
                          <li class="list-group-item p-5 pr-10 pl-10">
                            <div class="media" style="flex-direction:initial;">
                              <div class="pr-10">
                                <a class="avatar" href="javascript:void(0)">
                                  <img class="img-fluid" src="../../global/portraits/<?=rand(1,9)?>.jpg" alt="...">
                                </a>
                              </div>
                              <div class="media-body">
                                <div class="mt-0 black">Edward Fletcher</div>
                                <small>Designer</small>
                              </div>
                            </div>
                          </li>
                      <?php }?>
                    </ul>
                </div>
                
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-pure waves-effect waves-classic" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/imagesloaded/imagesloaded.pkgd.js"></script>
  <script src="../../global/vendor/masonry/masonry.pkgd.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/GridMenu.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../global/js/Plugin/masonry.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script>
  (function(document, window, $) {
  'use strict';
  var Site = window.Site;
  $(document).ready(function() {
    Site.run();

    $('.page-main').css("min-height",($('.page-aside-inner').height()+150)+"px");

    function chooseDateRange(start, end) {
          var DrangeValue = start.format('MM/DD/YYYY') + '-' + end.format('MM/DD/YYYY');
          $('#SearchDateRange span#daterange-value').html(DrangeValue);
          var dataKey = "123";
          var dataValue = "xxx";
          var stoken = '';
            $('.search-wrapper .currently-showing .chooseDateRange').remove();
            stoken = '<div class="token chooseDateRange" data-key="'+dataKey+'" data-value="' + dataValue + '"><span class="token-label" >' + DrangeValue + '</span><a  href="#" class="close" tabindex="-1">×</a></div>';
            $('.search-wrapper .currently-showing').append(stoken);
            sysGenUrlSearch();
      }

      $('#SearchDateRange').daterangepicker({
          "autoApply": true,
          "opens": "center",
          ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
      }, chooseDateRange);
  });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>