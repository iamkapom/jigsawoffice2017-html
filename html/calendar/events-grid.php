<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../assets/examples/css/advanced/masonry.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition page-aside-left">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header">
      <h1 class="page-title">Events Grid</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../home/">Home</a></li>
        <li class="breadcrumb-item active">Apps</li>
        <li class="breadcrumb-item active">Calendar</li>
        <li class="breadcrumb-item active">Events</li>
      </ol>
    </div>
    <div class="page-content container-fluid bg-white" style="position: relative;">
      <div class="page-aside">
        <!-- Contacts Sidebar -->
        <div class="page-aside-switch">
          <i class="icon md-chevron-left" aria-hidden="true"></i>
          <i class="icon md-chevron-right" aria-hidden="true"></i>
        </div>
        <div class="page-aside-inner page-aside-scroll">
          <div data-role="container">
            <div data-role="content">
              <section class="page-aside-section">
                <h5 class="page-aside-title">Events</h5>
                <div class="list-group">
                  <a class="list-group-item" data-toggle="tab" href="#category-2" aria-controls="category-2"
                    role="tab">
                    <span class="list-group-item-content">Pass Events</span>
                  </a>
                  <a class="list-group-item" data-toggle="tab" href="#category-2" aria-controls="category-2"
                    role="tab">
                    <span class="list-group-item-content">Today</span>
                  </a>
                  <a class="list-group-item" data-toggle="tab" href="#category-2" aria-controls="category-2"
                    role="tab">
                    <span class="list-group-item-content">Tomorrow</span>
                  </a>
                  <a class="list-group-item active" data-toggle="tab" href="#category-3" aria-controls="category-3"
                    role="tab">
                    <span class="list-group-item-content">This Week</span>
                  </a>
                  <a class="list-group-item" data-toggle="tab" href="#category-3" aria-controls="category-3"
                    role="tab">
                    <span class="list-group-item-content">Next Week</span>
                  </a>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      <div class="page-main">
        <div class="page-header">
          <form class="mt-20" action="#" role="search">
            <div class="input-search input-search-dark">
              <input type="text" class="form-control w-full" placeholder="Search..." name="">
              <button type="submit" class="input-search-btn">
                <i class="icon md-search" aria-hidden="true"></i>
              </button>
            </div>
          </form>
        </div>
        <div class="projects-sort mb-30">
          <span class="projects-sort-label">Sorted by : </span>
          <div class="inline-block dropdown">
            <span id="projects-menu" data-toggle="dropdown" aria-expanded="false" role="button">
              Dropdown
              <i class="icon md-chevron-down" aria-hidden="true"></i>
            </span>
            <div class="dropdown-menu animation-scale-up animation-top-left animation-duration-250" aria-labelledby="projects-menu" role="menu">
              <a class="dropdown-item" href="javascript:void(0)" role="menuitem" tabindex="-1">Title Ascending</a>
              <a class="active dropdown-item" href="javascript:void(0)" role="menuitem" tabindex="-1">Title Descending</a>
              <a class="dropdown-item" href="javascript:void(0)" role="menuitem" tabindex="-1">Publish Date Ascending</a>
              <a class="dropdown-item" href="javascript:void(0)" role="menuitem" tabindex="-1">Publish Date Descending</a>
              <a class="dropdown-item" href="javascript:void(0)" role="menuitem" tabindex="-1">Start Date low to high</a>
              <a class="dropdown-item" href="javascript:void(0)" role="menuitem" tabindex="-1">Start Date high to low</a>
            </div>
          </div>
        </div>
        <ul class="blocks blocks-100 blocks-xxl-4 blocks-lg-2" data-plugin="masonry">
          <?php 
          $arr_img = array("lockscreen","login","poster","coming-soon");
          $arr_subject = array("Genus Quaerimus","Possumus fugiendum verborum","Expleantur manu graecam","Singulos flagitem cupiditatibus");
          $arr_detail[0]="Dolemus late utriusque fore eveniet provincia spernat dissentiet. Fit intemperantes.";
          $arr_detail[1]="Geometria eae servare dicat, amicitiam, dixissem principio coniuncta adhibuit quantumcumque placeat easque, fidem gloriatur statuat perspecta ferantur greges propemodum quieti, turpius vituperatoribus complectitur leguntur vidisse assiduitas insolens audeam urbes futuris.";
          $arr_detail[2]="Munere dictum dissentio dicturam mediocriterne honesta, morbi delectus rationibus periculum opinor propterea intuemur poetarum efficeretur interpretaris, labefactant aeternum reformidans.";
          $arr_detail[3]="Adiungimus acutum iudicatum aliud aegritudinem, tritani ignavi incidant quaeritur transferrem loqueretur delectatio, appetere, eruditi. Eamque quaeso diuturnum. Atomis quietae quamquam cadere arbitrantur magnam referuntur utramque aristippi, filium eidola, iudicat veniamus, noctesque invenerit, alia factorum aristoteli phaedrum parta quicquid morbi animadversionem.";
          for($a=1;$a<=10;$a++){?>
          <li class="masonry-item">
            <div class="card card-shadow card-bordered">
              <div class="card-header cover">
                <a href="events-grid-detail.php"><img class="cover-image" src="../../assets/examples/images/<?php echo $arr_img[rand(0,3)];?>.jpg" alt="..."></a>
              </div>
              <div class="card-block">
                <div class="card-actions card-actions-sidebar" style="margin-top:0;">
                  <div class="text-danger font-size-18" style="margin-top:0;text-align:center;">May</div>
                  <div class="text-dark font-size-50 card-title" style="text-align: center; margin: 0px; line-height: 55px;">7</div>
                </div>
                <div class="card-content">
                  <a href="events-grid-detail.php">
                    <h4 class="card-title"><?php echo $arr_subject[rand(0,3)];?></h4>
                  </a>
                  <p class="card-text type-link font-size-14">Sunday | 55 Warren Street</p>
                  <div class="row no-space bg-grey-100 p-5">
                    <div class="col-6">
                      <div class="counter">
                        <span class="counter-number font-size-14">2,460</span>
                        <div class="counter-label font-size-11">people interested</div>
                      </div>
                    </div>
                    <div class="col-6">
                      <div class="counter">
                        <span class="counter-number font-size-14">569</span>
                        <div class="counter-label font-size-11">people going</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="clearfix card-footer-bordered p-15">
                <a class="btn btn-warning card-link waves-effect waves-classic" href="javascript:void(0)">
                  <i class="icon fa-star" aria-hidden="true"></i> Interested
                </a>
              </div>
            </div>
          </li>
          <?php }?>
          

        </ul>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/imagesloaded/imagesloaded.pkgd.js"></script>
  <script src="../../global/vendor/masonry/masonry.pkgd.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/GridMenu.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/masonry.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>