<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../assets/examples/css/advanced/masonry.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition page-aside-left">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header">
      <h1 class="page-title">Events Grid</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../home/">Home</a></li>
        <li class="breadcrumb-item active">Apps</li>
        <li class="breadcrumb-item active">Calendar</li>
        <li class="breadcrumb-item active">Events</li>
      </ol>
    </div>
    <div class="page-content container-fluid bg-white" style="position: relative;">
      <div class="page-aside">
        <!-- Contacts Sidebar -->
        <div class="page-aside-switch">
          <i class="icon md-chevron-left" aria-hidden="true"></i>
          <i class="icon md-chevron-right" aria-hidden="true"></i>
        </div>
        <div class="page-aside-inner page-aside-scroll">
          <div data-role="container">
            <div data-role="content">
              <section class="page-aside-section pl-10">
                <div class="alert alert-alt alert-danger alert-dismissible" role="alert">
                  <strong>Possumus fugiendum verborum</strong>
                </div>
              </section>
              <section class="page-aside-section">
                <h5 class="page-aside-title">Events</h5>
                <div class="list-group">
                  <a class="list-group-item" data-toggle="tab" href="#category-2" aria-controls="category-2"
                    role="tab">
                    <span class="list-group-item-content">Pass Events</span>
                  </a>
                  <a class="list-group-item" data-toggle="tab" href="#category-2" aria-controls="category-2"
                    role="tab">
                    <span class="list-group-item-content">Today</span>
                  </a>
                  <a class="list-group-item" data-toggle="tab" href="#category-2" aria-controls="category-2"
                    role="tab">
                    <span class="list-group-item-content">Tomorrow</span>
                  </a>
                  <a class="list-group-item active" data-toggle="tab" href="#category-3" aria-controls="category-3"
                    role="tab">
                    <span class="list-group-item-content">This Week</span>
                  </a>
                  <a class="list-group-item" data-toggle="tab" href="#category-3" aria-controls="category-3"
                    role="tab">
                    <span class="list-group-item-content">Next Week</span>
                  </a>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      <div class="page-main">
        <div class="card card-shadow pt-20">
          <div class="card-header cover">
            <img class="cover-image" src="../../assets/examples/images/login.jpg" alt="...">
          </div>
          <div class="card-block">
            <div class="card-actions card-actions-sidebar" style="margin-top:0;">
              <div class="text-danger font-size-18" style="margin-top:0;text-align:center;">May</div>
              <div class="text-dark font-size-50 card-title" style="text-align: center; margin: 0px; line-height: 55px;">7</div>
            </div>
            <div class="card-content">
              <h3 class="card-title">Possumus fugiendum verborum</h3>
              <div class="row">
                <div class="col-md-6">
                  <p class="card-text type-link">
                    <small>
                      By
                      <a href="javascript:void(0)">Nathan Watts</a>
                      <a href="javascript:void(0)">05, 2017</a>
                    </small>
                  </p>
                  <ul class="list-group list-group-full">
                    <li class="list-group-item">
                      <i class="icon fa-clock-o text-warning" aria-hidden="true"></i>
                      Sunday | 11:18 am
                    </li>
                    <li class="list-group-item">
                      <i class="icon fa-map-marker text-dark" aria-hidden="true"></i>
                      55 Warren Street, New York, NY 10007, USA
                    </li>
                  </ul>
                  <div class="clearfix">
                    <div class="btn-group" aria-label="Basic example" role="group">
                      <button type="button" class="btn btn-warning waves-effect waves-classic"><i class="icon fa-star" aria-hidden="true"></i> Interested</button>
                      <button type="button" class="btn btn-success waves-effect waves-classic"><i class="icon fa-check" aria-hidden="true"></i>Going</button>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="panel" style="border: none;box-shadow: none;">
                    <div class="panel-heading">
                      <h3 class="panel-title pt-5">2.7K Going · 5.5K Interested
                        <small><a href="#" class="panel-actions panel-actions-keep">Sell All</a></small>
                      </h3>
                    </div>
                    <div class="panel-body">
                      <ul class="list-unstyled list-inline">
                        <?php for($a=7;$a<=16;$a++){?>
                        <li class="list-inline-item">
                          <a class="avatar avatar-m m-5" href="javascript:void(0)">
                            <img src="../../global/portraits/<?php echo $a;?>.jpg" alt="">
                          </a>
                        </li>
                        <?php }?>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              
            </div>
          </div>

          <div class="nav-tabs-horizontal" data-plugin="tabs">
            <ul class="nav nav-tabs nav-tabs-line" role="tablist">
              <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" href="#exampleTabsLineOne" aria-controls="exampleTabsLineOne" role="tab" aria-expanded="true">Home</a></li>
              <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleTabsLineTwo" aria-controls="exampleTabsLineTwo" role="tab" aria-expanded="false">Components</a></li>
              <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleTabsLineThree" aria-controls="exampleTabsLineThree" role="tab">Css</a></li>
              <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleTabsLineFour" aria-controls="exampleTabsLineFour" role="tab">Javascript</a></li>
              <li class="dropdown nav-item" role="presentation" style="display: none;">
                <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#" aria-expanded="false">Dropdown </a>
                <div class="dropdown-menu" role="menu">
                  <a class="dropdown-item" data-toggle="tab" href="#exampleTabsLineOne" aria-controls="exampleTabsLineOne" role="tab">Home</a>
                  <a class="dropdown-item" data-toggle="tab" href="#exampleTabsLineTwo" aria-controls="exampleTabsLineTwo" role="tab">Components</a>
                  <a class="dropdown-item" data-toggle="tab" href="#exampleTabsLineThree" aria-controls="exampleTabsLineThree" role="tab">Css</a>
                  <a class="dropdown-item" data-toggle="tab" href="#exampleTabsLineFour" aria-controls="exampleTabsLineFour" role="tab">Javascript</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
        
        <div class="pt-30">
          
          <div class="tab-content pt-20">
            <div class="tab-pane active" id="exampleTabsLineOne" role="tabpanel" aria-expanded="true">
              Quoquo timeret omne redeamus ratione monet nosque requietis afferrent iste, pertinerent.
              Postremo frustra oportet pueriliter finxerat eos offendit
              fecerint, iudicem quieti scribi animumque pondere ancillae,
              timeret stoicos iustitia parvam.
            </div>
            <div class="tab-pane" id="exampleTabsLineTwo" role="tabpanel" aria-expanded="false">
              Sole, latinas incurreret optari vivatur, porro delectu epicurus cadere impedit
              fit ferreum concludaturque varias, omnium gloriosis vivendo
              via filio contentam expeteretur fonte expectata, quosque
              praetor quid iucunditatis fortitudinem esse.
            </div>
            <div class="tab-pane" id="exampleTabsLineThree" role="tabpanel">
              Maestitiam quamquam iudicare veterum contineri ipse cognoscerem se omittantur dialectica,
              dixit poterit nondum tempora corpora claudicare ratione percipitur.
              Earum comprehenderit laudem platonis allevatio graeci, invidus
              coercendi valetudinis numen deseruisse.
            </div>
            <div class="tab-pane" id="exampleTabsLineFour" role="tabpanel">
              Adiit optime intemperantiam ostendis doctus brevi provincia suscepi. Eos efficitur
              inprobis negent turbulenta consentientis ingeniis natura,
              desperantes quisque concessum theophrasti, torquatus detracto,
              deinde, intellegentium fruitur errorem nulli vivatur, operis.
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/imagesloaded/imagesloaded.pkgd.js"></script>
  <script src="../../global/vendor/masonry/masonry.pkgd.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/GridMenu.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/masonry.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>