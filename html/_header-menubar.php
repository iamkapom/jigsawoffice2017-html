<div class="site-menubar site-menubar-dark">
  <div class="site-menubar-body">
    <ul class="site-menu" data-plugin="menu">
      <li class="site-menu-item">
        <a class="animsition-link" href="../home/">
          <i class="site-menu-icon md-widgets" aria-hidden="true"></i>
          <span class="site-menu-title">Home</span>
        </a>
      </li>
      <li class="site-menu-item">
        <a href="../my-space/index.php" dropdown-badge="false">
          <i class="site-menu-icon md-collection-item" aria-hidden="true"></i>
          <span class="site-menu-title">My Space</span>
        </a>
      </li>
      <li class="site-menu-item">
        <a href="../messenger-v2/messenger.php" dropdown-badge="false">
          <i class="site-menu-icon md-comments" aria-hidden="true"></i>
          <span class="site-menu-title">Messenger</span>
        </a>
      </li>
      <li class="site-menu-item">
        <a href="../workspace/workspace.php" dropdown-badge="false">
          <i class="site-menu-icon md-share" aria-hidden="true"></i>
          <span class="site-menu-title">Workspaces</span>
        </a>
      </li>
      <li class="site-menu-item">
        <a href="../apps-intranet/news.php">
          <i class="site-menu-icon md-grain" aria-hidden="true"></i>
          <span class="site-menu-title">Intranet</span>
        </a>
      </li>
      <li class="site-menu-item">
        <a href="../apps-tickets/index.php">
          <i class="site-menu-icon fa-ticket" aria-hidden="true"></i>
          <span class="site-menu-title">Support Tickets</span>
        </a>
      </li>
      <li class="site-menu-item">
        <a href="../apps-knowledgebase/index.php">
          <i class="site-menu-icon fa-lightbulb-o" aria-hidden="true"></i>
          <span class="site-menu-title">Knowledgebase</span>
        </a>
      </li>
      <li class="site-menu-item">
        <a href="../apps-bookingcar/">
          <i class="site-menu-icon md-car" aria-hidden="true"></i>
          <span class="site-menu-title">Booking Car</span>
        </a>
      </li>
      <li class="site-menu-item">
        <a href="../apps-charteditor/">
          <i class="site-menu-icon fa-ticket" aria-hidden="true"></i>
          <span class="site-menu-title">Chart Editor</span>
        </a>
      </li>
      </ul>
    
  </div>
</div>