<ul class="nav nav-tabs nav-tabs-line fix-mini-nav" role="tablist">
  <li class="nav-item">
    <a class="nav-link <?=(basename($_SERVER["SCRIPT_FILENAME"])=="workspace-home2.php"?"active":"")?>" href="workspace-home2.php" aria-expanded="true"><i class="md-home" aria-hidden="true"></i> Workspace Feed</a>
  </li>
  <li class="nav-item">
    <a class="nav-link <?=(basename($_SERVER["SCRIPT_FILENAME"])=="workspace-task.php"?"active":"")?>" href="workspace-task.php" aria-expanded="false"><i class="md-check-all" aria-hidden="true"></i> Task</a>
  </li>
  <li class="nav-item hidden-sm-down">
    <a class="nav-link <?=(basename($_SERVER["SCRIPT_FILENAME"])=="workspace-drive.php"?"active":"")?>" href="workspace-drive.php" aria-expanded="false"><i class="md-cloud" aria-hidden="true"></i> Drive</a>
  </li>
  <li class="nav-item hidden-sm-down">
    <a class="nav-link" target="_blank" href="../messenger-v2/messenger.php" aria-expanded="false"><i class="md-cloud" aria-hidden="true"></i> Messenger</a>
  </li>
</ul>