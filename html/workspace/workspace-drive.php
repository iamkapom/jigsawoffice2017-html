<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../global/vendor/jquery-selective/jquery-selective.css">
  <link rel="stylesheet" href="../../global/vendor/slick-carousel/slick.css">
  <link rel="stylesheet" href="../../assets/examples/css/pages/profile_v3.css">
  <link rel="stylesheet" href="../../global/vendor/aspieprogress/asPieProgress.css">
  <link rel="stylesheet" href="../../assets/examples/css/charts/pie-progress.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-daterangepicker/daterangepicker.css" />
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header">
      
      <h1 class="page-title mb-10">Jigsaw Office Co-Working</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item"><a href="workspace.php">Workspace</a></li>
        <li class="breadcrumb-item active">Jigsaw Office Co-Working</li>
      </ol>
      
    </div>
    
    <?php include("mini-nav.php");?>
    <div class="page-content container-fluid bg-white" style="position: relative;">
      
      <div class="row ml-0 mr-0">
        <div class="col-md-12 pt-30">
          <section>
            <div class="pb-10">
              <div class="search-wrapper">
                <div class="search-box">
                  <div class="icon md-search"></div>
                  <div class="currently-showing">
                    <?=$search['tokenhtml']?>
                  </div>
                </div>
                <a href="javascript:void(0);" class="clear_all btn btn-flat btn-default btn-block waves-effect waves-classic">Clear All</a>
                <div class="data_entry">
                  <input class="input keyword-input" placeholder="Enter a keyword" type="text">
                  <div class="icon md-close-circle close"></div>
                </div>
                <div class="filters">
                    <div class="dropdown filter_permission">
                      <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Owner
                        <span class="icon md-chevron-down" aria-hidden="true"></span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)">Owned by me</a>
                        <a class="dropdown-item" href="javascript:void(0)">Not owned by me</a>
                      </div>
                    </div>
                    <div class="dropdown filter_permission">
                      <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Mode
                        <span class="icon md-chevron-down" aria-hidden="true"></span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)"><i class="icon md-accounts-alt mr-5" aria-hidden="true"></i>Internal Mode</a>
                        <a class="dropdown-item" href="javascript:void(0)"><i class="icon md-globe-alt mr-5" aria-hidden="true"></i>External Mode</a>
                      </div>
                    </div>
                    <div class="dropdown filter_permission">
                      <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Type
                        <span class="icon md-chevron-down" aria-hidden="true"></span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" style="color:#f44336;" href="javascript:void(0)"><i class="icon fa-file-pdf-o mr-5" aria-hidden="true"></i>PDF</a>
                        <a class="dropdown-item" style="color:#00bcd4;" href="javascript:void(0)"><i class="icon fa-file-zip-o mr-5" aria-hidden="true"></i>Zip</a>
                        <a class="dropdown-item" style="color:#ffab00;" href="javascript:void(0)"><i class="icon fa-file-picture-o mr-5" aria-hidden="true"></i>Picture</a>
                        <a class="dropdown-item" style="color:#004d40;" href="javascript:void(0)"><i class="icon fa-file-video-o mr-5" aria-hidden="true"></i>Video</a>
                        <a class="dropdown-item" style="color:#9c27b0;" href="javascript:void(0)"><i class="icon fa-file-sound-o mr-5" aria-hidden="true"></i>Sound</a>
                        <a class="dropdown-item" style="color:#3f51b5;" href="javascript:void(0)"><i class="icon fa-file-word-o mr-5" aria-hidden="true"></i>Word</a>
                        <a class="dropdown-item" style="color:#ff9800;" href="javascript:void(0)"><i class="icon fa-file-powerpoint-o mr-5" aria-hidden="true"></i>Powerpoint</a>
                        <a class="dropdown-item" style="color:#4caf50;" href="javascript:void(0)"><i class="icon fa-file-excel-o mr-5" aria-hidden="true"></i>Excel</a>
                        <a class="dropdown-item" style="color:#616161;" href="javascript:void(0)"><i class="icon fa-file-text-o mr-5" aria-hidden="true"></i>Text</a>
                        <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-folder mr-5 grey-500" aria-hidden="true"></i>Folder</a>
                      </div>
                    </div>
                    
                    
                    <div class="dropdown filter_permission">
                      <button type="button" id="SearchDateRange" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Time <i class="icon md-chevron-down" aria-hidden="true"></i>
                        <span id="daterange-value"></span>
                      </button>
                    </div>
                    
                </div>
              </div>
            </div>
            <div class="pb-20">
                <div class="actions-inner float-right">
                  <div class="dropdown">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      <i class="icon md-view-module" aria-hidden="true"></i> <i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Name
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-media w-100" role="menu">
                      <h6 class="pl-10" aria-hidden="false">View</h6>
                      <a class="dropdown-item" href="drive-list.php"><i class="icon md-view-list" aria-hidden="true"></i> List</a>
                      <a class="dropdown-item active" href="drive-grid.php"><i class="icon md-view-module" aria-hidden="true"></i> Grid</a>
                      <div class="dropdown-divider"></div>
                      <h6 class="pl-10" aria-hidden="false">Sory by</h6>
                      <a class="dropdown-item active" href="javascript:void(0)">Name</a>
                      <a class="dropdown-item" href="javascript:void(0)">Date Created</a>
                      <a class="dropdown-item" href="javascript:void(0)">Size</a>
                      <a class="dropdown-item" href="javascript:void(0)">Activity</a>
                      <a class="dropdown-item" href="javascript:void(0)">Comment</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-sort-amount-asc" aria-hidden="true"></i> Ascending</a>
                      <a class="dropdown-item active" href="javascript:void(0)"><i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Descending</a>
                    </div>
                  </div>
                </div>
                

                <div class="pt-10">About <strong>26</strong> results</div>
              </div>
            </section>
          <section>
            
            <div class="row">
              <div class="col-dm-12 mb-10 grey-500 pl-15">Folders System Application</div>
            </div>
            <div class="row">
              <?php 
              $color[] = "3f51b5";
              $color[] = "00bcd4";
              $color[] = "004d40";
              $color[] = "9c27b0";
              $color[] = "ff9800";
              $color[] = "ffab00";
              $color[] = "f44336";
              $color[] = "4caf50";
              $color[] = "616161";

              $filename[] = "ผลการสำรวจเว็บไซต์";
              $filename[] = "Link example & Research";
              $filename[] = "Milestone";
              $filename[] = "New Sitemap";
              $filename[] = "TOR_Checklist";
              $filename[] = "WEB Design Trend";
              $filename[] = "wireframe inner";
              $filename[] = "เอกสารรวบรวม Data";
              $filename[] = "Meeting Memo";

              $foldername[] = "Messenger";
              $foldername[] = "Task";
              $foldername[] = "Calendar";
              $foldername[] = "Project Document";
              $foldername[] = "HTML";
              $foldername[] = "Picture";
              $foldername[] = "Design";
              for($a=0;$a<2;$a++){
              ?>
              <div class="col-md-3">
                <div class="card card-shadow card-bordered">
                  <div class="card-header bg-grey-200 p-10 clearfix" style="padding-bottom: 0px;">
                    <div class="media" style="flex-direction:initial;">
                      <div class="pr-10">
                        <a data-url="panel-folder.html" data-toggle="slidePanel"  class="font-size-30" href="javascript:void(0)">
                          <i class="icon fa-folder" aria-hidden="true"></i>
                        </a>
                      </div>
                      <div class="media-body">
                        <h5 class="mt-15 mb-0">
                          <a href="javascript:void(0)"><?=$foldername[$a]?></a>
                        </h5>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php }?>
            </div>
            <div class="row">
              <div class="col-dm-12 mb-10 grey-500 pl-15">Folders</div>
            </div>
            <div class="row">
              <?php
              for($a=3;$a<count($foldername);$a++){
              ?>
              <div class="col-md-3">
                <div class="card card-shadow card-bordered">
                  <div class="card-header bg-grey-200 p-10 clearfix" style="padding-bottom: 0px;">
                    <div class="media" style="flex-direction:initial;">
                      <div class="pr-10">
                        <a class="font-size-30" href="javascript:void(0)">
                          <i class="icon fa-folder grey-500" aria-hidden="true"></i>
                        </a>
                      </div>
                      <div class="media-body">
                        <h5 class="mt-15 mb-0">
                          <a href="javascript:void(0)"><?=$foldername[$a]?></a>
                        </h5>
                        <div class="wall-action" style="top:0;">
                          <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                            <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                          </button>
                          <div class="dropdown-menu dropdown-menu-right" role="menu">
                            <a data-url="panel-folder.html" data-toggle="slidePanel" class="dropdown-item" href="javascript:void(0)" role="menuitem">
                              View detail
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
              <?php }?>
            </div>
            <div class="row">
              <div class="col-dm-12 mb-10 grey-500 pl-15">Files</div>
            </div>
            <div class="row">
              <?php 
              $file[] = "fa-file-word-o";
              $file[] = "fa-file-zip-o";
              $file[] = "fa-file-video-o";
              $file[] = "fa-file-sound-o";
              $file[] = "fa-file-powerpoint-o";
              $file[] = "fa-file-picture-o";
              $file[] = "fa-file-pdf-o";
              $file[] = "fa-file-excel-o";
              $file[] = "fa-file-text-o";

              for($a=1;$a<=30;$a++){
                $index = rand(0,8);
              ?>
              <div class="col-md-3">
                <div class="card card-shadow card-bordered">
                  <div class="bg-grey-100 pt-20 pb-20 clearfix text-center" style="padding-bottom: 0px;">
                    <a style="color:#<?=$color[$index]?>;" href="javascript:void(0)">
                      <i class="icon <?=$file[$index]?> font-size-60" aria-hidden="true"></i>
                    </a>
                    <div class="wall-action" style="top:0;">
                      <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                        <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                      </button>
                      <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                          Download
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                          Rename
                        </a>
                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                          Delete
                        </a>
                        <div class="dropdown-divider"></div>
                        <a data-url="panel-file.html" data-toggle="slidePanel"  class="dropdown-item" href="javascript:void(0)" role="menuitem">
                          View detail
                        </a>
                      </div>
                    </div>
                  </div>
                  <div class="card-block text-center" style="padding: 10px 20px;">
                    <h5>
                      <a href="javascript:void(0)" style="color:#<?=$color[$index]?>;">
                      <?=$filename[$index]?>
                      </a>
                    </h5>
                    <p class="mb-5 grey-500">
                      <small>
                        July 30, 2017 <span class="ml-10 mr-10">&mdash;</span> 4.25 MB</small>
                    </p>
                  </div>
                </div>
              </div>
              <?php }?>
              
            </div>
          </section>
        </div>
      </div>
      
    </div>
  </div>

  <div class="site-action" data-plugin="actionBtn">
    <button type="button" data-target="#postFormQuick" data-toggle="modal" class="btn-raised btn btn-success btn-floating">
      <i class="front-icon md-plus" aria-hidden="true"></i>
    </button>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/slick-carousel/slick.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <script src="../../global/vendor/aspieprogress/jquery-asPieProgress.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/masonry.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../assets/examples/js/pages/profile-v2.js"></script>
  <script src="../../global/js/Plugin/aspieprogress.js"></script>
  <script src="../../assets/examples/js/charts/pie-progress.js"></script>
  <script src="../../global/js/Plugin/responsive-tabs.js"></script>
  <script src="../../assets/js/BaseApp.js"></script>
  <script src="../../assets/js/App/Contacts.js"></script>
  <script src="../../assets/examples/js/apps/contacts.js"></script>
  
  <script src="../../global/vendor/bootstrap-daterangepicker/moment.min.js"></script>
  <script src="../../global/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();


      function chooseDateRange(start, end) {
          var DrangeValue = start.format('MM/DD/YYYY') + '-' + end.format('MM/DD/YYYY');
          $('#SearchDateRange span#daterange-value').html(DrangeValue);
          var dataKey = "123";
          var dataValue = "xxx";
          var stoken = '';
            $('.search-wrapper .currently-showing .chooseDateRange').remove();
            stoken = '<div class="token chooseDateRange" data-key="'+dataKey+'" data-value="' + dataValue + '"><span class="token-label" >' + DrangeValue + '</span><a  href="#" class="close" tabindex="-1">×</a></div>';
            $('.search-wrapper .currently-showing').append(stoken);
            sysGenUrlSearch();
      }

      $('#SearchDateRange').daterangepicker({
          "autoApply": true,
          "opens": "center",
          ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
      }, chooseDateRange);

    });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>