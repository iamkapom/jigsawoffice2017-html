<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../global/vendor/jquery-selective/jquery-selective.css">
  <link rel="stylesheet" href="../../global/vendor/slick-carousel/slick.css">
  <link rel="stylesheet" href="../../assets/examples/css/pages/profile-v2.css">
  <link rel="stylesheet" href="../../global/vendor/aspieprogress/asPieProgress.css">
  <link rel="stylesheet" href="../../assets/examples/css/charts/pie-progress.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition page-profile-v2 page-aside-left">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header">
      <h1 class="page-title mb-10">Jigsaw Office Co-Working</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item"><a href="workspace.php">Workspace</a></li>
        <li class="breadcrumb-item active">Jigsaw Office Co-Working</li>
      </ol>
      <div class="page-header-actions">
        <a class="btn btn-sm btn-outline btn-warning btn-round waves-effect waves-classic collapsed" data-toggle="collapse" href="#exampleCollapseExample" aria-expanded="false" aria-controls="exampleCollapseExample">
        <span class="text hidden-sm-down">Workspace Performance</span>
        <i class="icon md-chevron-right" aria-hidden="true"></i>
        </a>
      </div>
    </div>
    <div class="collapse" id="exampleCollapseExample" aria-expanded="false" style="">
      <div class="row">
        <div class="col-md-4">
          <!-- Card -->
          <div class="card p-20">
            <h3 class="mt-0">Summary</h3>
            <div class="counter-label text-uppercase mb-5">42 Tasks</div>
            <div class="pt-20 pb-20">
              <div class="pie-progress pie-progress" data-plugin="pieProgress" data-barcolor="#4caf50"
              data-goal="72" aria-valuenow="72" data-size="100" data-barsize="8"
              role="progressbar">
                <div class="pie-progress-content">
                  <div class="pie-progress-number">72 %</div>
                  <div class="pie-progress-label">Progress</div>
                </div>
              </div>
            </div>
          </div>
          <!-- End Card -->
        </div>
        <div class="col-md-4">
          <!-- Card -->
          <div class="card p-20 justify-content-between">
            <h3 class="mt-0">Tasks Priority</h3>
            <div class="contextual-progress mt-0 mb-20">
              <div class="clearfix">
                <div class="progress-title">Highest</div>
                <div class="progress-label">60%</div>
              </div>
              <div class="progress" data-labeltype="percentage" data-goal="-40" data-plugin="progress">
                <div class="progress-bar progress-bar-danger" aria-valuemin="-100" aria-valuemax="0" aria-valuenow="-40" role="progressbar" style="width: 60%;"></div>
              </div>
            </div>
            <div class="contextual-progress mt-0 mb-20">
              <div class="clearfix">
                <div class="progress-title">High</div>
                <div class="progress-label">70%</div>
              </div>
              <div class="progress" data-labeltype="percentage" data-goal="-40" data-plugin="progress">
                <div class="progress-bar progress-bar-warning" aria-valuemin="-100" aria-valuemax="0" aria-valuenow="-40" role="progressbar" style="width: 70%;"></div>
              </div>
            </div>
            <div class="contextual-progress mt-0 mb-20">
              <div class="clearfix">
                <div class="progress-title">Normal</div>
                <div class="progress-label">80%</div>
              </div>
              <div class="progress" data-labeltype="percentage" data-goal="-40" data-plugin="progress">
                <div class="progress-bar progress-bar-success" aria-valuemin="-100" aria-valuemax="0" aria-valuenow="-40" role="progressbar" style="width: 80%;"></div>
              </div>
            </div>
            <div class="contextual-progress mt-0 mb-20">
              <div class="clearfix">
                <div class="progress-title">Low</div>
                <div class="progress-label">50%</div>
              </div>
              <div class="progress" data-labeltype="percentage" data-goal="-40" data-plugin="progress">
                <div class="progress-bar" aria-valuemin="-100" aria-valuemax="0" aria-valuenow="-40" role="progressbar" style="width: 50%;background-color:#757575;"></div>
              </div>
            </div>
          </div>
          <!-- End Card -->
        </div>
        <div class="col-md-4">
          <!-- Card -->
          <div class="card p-20">
            <h3 class="mt-0">Tasks in Progress</h3>
            <div class="counter counter-md text-left">
              <div class="contextual-progress">
                <div class="clearfix">
                  <div class="progress-title">Order due</div>
                  <div class="progress-label">2 / 5</div>
                </div>
                <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                  <div class="progress-bar progress-bar-danger" aria-valuemin="0" aria-valuemax="100" style="width: 40%;" aria-valuenow="60" role="progressbar">
                  </div>
                </div>
              </div>
              
            </div>
            <div class="counter counter-md text-left">
              <div class="contextual-progress">
                <div class="clearfix">
                  <div class="progress-title">Today</div>
                  <div class="progress-label">5 / 8</div>
                </div>
                <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                  <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 60%;" aria-valuenow="60" role="progressbar">
                  </div>
                </div>
              </div>
              
            </div>
            <div class="counter counter-md text-left">
              <div class="contextual-progress">
                <div class="clearfix">
                  <div class="progress-title">This week</div>
                  <div class="progress-label">14 / 41</div>
                </div>
                <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                  <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 34%;" aria-valuenow="60" role="progressbar">
                  </div>
                </div>
              </div>
              
            </div>
          </div>
          <!-- End Card -->
        </div>
        
      </div>
    </div>
    <div class="page-content container-fluid" style="position: relative;">
      <div class="page-aside">
        <!-- Contacts Sidebar -->
        <div class="page-aside-switch">
          <i class="icon md-chevron-left" aria-hidden="true"></i>
          <i class="icon md-chevron-right" aria-hidden="true"></i>
        </div>
        <div class="page-aside-inner page-aside-scroll">
          <div data-role="container">
            <div data-role="content">
              <section class="page-aside-section pl-10">
                <a href="../workspace/workspace-home.php">
                <div class="alert alert-alt alert-danger alert-dismissible" role="alert">
                  <strong>Jigsaw Office Co-Working</strong>
                </div>
                </a>
              </section>
              <div class="page-aside-section">
                <div class="list-group">
                  <div class="list-group-item active">
                    <div class="list-content">
                      <span class="list-text">Activity</span>
                    </div>
                  </div>
                  <div class="list-group-item">
                    <a href="../drive/workspace-drive-grid.php" style="text-decoration:none;">
                    <div class="list-content">
                      <span class="list-text grey-600">Drive</span>
                    </div>
                    </a>
                  </div>
                  <div class="list-group-item">
                    <div class="list-content">
                      <span class="list-text">Task</span>
                    </div>
                  </div>
                  <div class="list-group-item">
                    <div class="list-content">
                      <span class="list-text">Setting</span>
                    </div>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
      <div class="page-main" style="margin-top:-30px;">
        <div class="row">
          <div class="col-md-7">
            <h3 class="panel-title pl-0 pt-0">Activities</h3>
             <ul class="list-group">
              <?php for($b=1;$b<4;$b++){?>
                  <li class="list-group-item">
                    <div class="media">
                      <div class="pr-20">
                        <a class="avatar" href="javascript:void(0)">
                          <img class="img-fluid" src="../../../global/portraits/2.jpg" alt="...">
                        </a>
                      </div>
                      <div class="media-body">
                        <h5 class="mt-0 mb-5">Ida Fleming
                          <small class="ml-10">14 minutes ago</small>
                        </h5>
                        <div class="profile-brief">
                          added a new task <a href="#" class="grey-900">"Wireframe & HTML JigsawOffice2017"</a>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="list-group-item">
                    <div class="media">
                      <div class="pr-20">
                        <a class="avatar" href="javascript:void(0)">
                          <img class="img-fluid" src="../../../global/portraits/5.jpg" alt="...">
                        </a>
                      </div>
                      <div class="media-body">
                        <h5 class="mt-0 mb-5">June Lane
                          <small class="ml-10">16 minutes ago</small>
                        </h5>
                        <div class="profile-brief">
                          Changed Task Status <a href="#" class="grey-900">"Create landing page for a new app"</a> to Complete
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="list-group-item">
                    <div class="media">
                      <div class="pr-20">
                        <a class="avatar" href="javascript:void(0)">
                          <img class="img-fluid" src="../../../global/portraits/1.jpg" alt="...">
                        </a>
                      </div>
                      <div class="media-body">
                        <h5 class="mt-0 mb-5">Caleb Richards
                          <small class="ml-10">22 minutes ago</small>
                        </h5>
                        <div class="profile-brief">
                          added the comment Task <a href="#" class="grey-900">"Create landing page for a new app"</a>
                          <span class="block mb-5">
                            <blockquote class="cover-quote grey-400">It had to end sometime. Apple’s incredible growth that saw the company report record quarterly earnings over a span of 13 years was untenable. Any one company would be thrilled by the success brought by the Apple II, the Mac, the iPod, the iPhone, or even the iPad.
                            </blockquote>
                          </span>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li class="list-group-item">
                    <div class="media">
                      <div class="pr-20">
                        <a class="avatar" href="javascript:void(0)">
                          <img class="img-fluid" src="../../../global/portraits/1.jpg" alt="...">
                        </a>
                      </div>
                      <div class="media-body">
                        <h5 class="mt-0 mb-5">Caleb Richards
                          <small class="ml-10">26 minutes ago</small>
                        </h5>
                        <div class="profile-brief">
                          attached file on Task <a href="#" class="grey-900">"Wireframe inner page"</a>
                          <p class="mt-5">
                            <i class="icon fa-file-pdf-o" aria-hidden="true"></i>
                            <a style="text-decoration: none;" href="javascript:void(0)">wireframe inner</a>
                          </p>
                        </div>
                      </div>
                    </div>
                  </li>
                 <?php }?> 
                </ul>
            <a class="btn btn-block btn-default" href="javascript:void(0)" role="button">Show more</a>
            
          </div>
          <div class="col-md-5">
            <div class="panel mb-30" style="border:1px #e0e0e0 solid;">
              <div class="page-header p-20">
                <form action="#" role="search">
                  <div class="input-search input-search-dark">
                    <input type="text" class="form-control w-full" placeholder="Search this workspace..." name="">
                    <button type="submit" class="input-search-btn">
                      <i class="icon md-search" aria-hidden="true"></i>
                    </button>
                  </div>
                </form>
              </div>
              <div class="panel-heading">
                <h3 class="panel-title">Information</h3>
              </div>
              <div class="panel-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce velit
                  tortor, dictum in gravida nec, aliquet non lorem. pellentesque.ipiscing
                  elit. Fusce velit tortor.</p>
                <section>
                    <ul class="list-group list-group-full">
                      <li class="list-group-item">
                        <span style="display:inline-block;" class="w-100 grey-500">Start</span>
                        <span>15/03/2017</span>
                      </li>
                      <li class="list-group-item">
                        <span style="display:inline-block;" class="w-100 grey-500">Finish</span>
                        <span>4/07/2017</span>
                      </li>
                      <li class="list-group-item clearfix">
                        <div class="w-100 grey-500 float-left">Workflow</div>
                        <div class="dropdown float-left pl-10">
                          <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                            Wireframe
                            <span class="icon md-chevron-down" aria-hidden="true"></span>
                          </button>
                          <div class="dropdown-menu" role="menu">
                            <a class="dropdown-item" href="javascript:void(0)">Budding</a>
                            <a class="dropdown-item active" href="javascript:void(0)">Wireframe</a>
                            <a class="dropdown-item" href="javascript:void(0)">Design</a>
                            <a class="dropdown-item" href="javascript:void(0)">HTML</a>
                            <a class="dropdown-item" href="javascript:void(0)">Coding</a>
                            <a class="dropdown-item" href="javascript:void(0)">Setup</a>
                            <a class="dropdown-item" href="javascript:void(0)">MA</a>
                          </div>
                        </div>
                      </li>
                      <li class="list-group-item clearfix">
                        <div class="w-100 grey-500 float-left">Priority</div>
                        <div class="dropdown float-left pl-10">
                          <button type="button" class="btn btn-pure waves-effect waves-classic text-warning" data-toggle="dropdown" aria-expanded="false">
                            <i class="icon fa-circle" aria-hidden="true"></i>High  Priority 
                            <span class="icon md-chevron-down" aria-hidden="true"></span>
                          </button>
                          <div class="dropdown-menu" role="menu">
                            <a class="dropdown-item text-danger" href="javascript:void(0)"><i class="icon fa-circle mr-5" aria-hidden="true"></i>Highest</a>
                            <a class="dropdown-item active text-warning" href="javascript:void(0)"><i class="icon fa-circle mr-5" aria-hidden="true"></i>High</a>
                            <a class="dropdown-item text-success" href="javascript:void(0)"><i class="icon fa-circle mr-5" aria-hidden="true"></i>Normal</a>
                            <a class="dropdown-item text-default" href="javascript:void(0)"><i class="icon fa-circle mr-5" aria-hidden="true"></i>Low</a>
                          </div>
                        </div>
                      </li>
                      <li class="list-group-item clearfix">
                        <div class="w-100 grey-500 float-left">Stutus</div>
                        <div class="dropdown float-left pl-10">
                          <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                            <i class="icon fa-check-square" aria-hidden="true"></i> Status
                            <span class="icon md-chevron-down" aria-hidden="true"></span>
                          </button>
                          <div class="dropdown-menu" role="menu">
                            <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-flash mr-5" aria-hidden="true"></i>Overdue</a>
                            <a class="dropdown-item active" href="javascript:void(0)"><i class="icon fa-cogs mr-5" aria-hidden="true"></i>Progress</a>
                            <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-check-square-o mr-5" aria-hidden="true"></i>Complete</a>
                            <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-adjust mr-5" aria-hidden="true"></i>Hold</a>
                          </div>
                        </div>
                      </li>
                      <li class="list-group-item">
                        <span style="display:inline-block;" class="w-100 grey-500">Created</span>
                        <span>May 5 at 11:00 AM</span>
                      </li>
                    </ul>
                  </section>
                <section>
                   <a href="#">
                  <span class="badge p-5 mb-5 badge-outline" style="border-color:#F2543B; color:#F2543B;">Designer</span>
                </a>
                                    <a href="#">
                  <span class="badge p-5 mb-5 badge-outline" style="border-color:#E8AD73; color:#E8AD73;">jQuery</span>
                </a>
                                    <a href="#">
                  <span class="badge p-5 mb-5 badge-outline" style="border-color:#C6EF3D; color:#C6EF3D;">Javascript</span>
                </a>
                                    <a href="#">
                  <span class="badge p-5 mb-5 badge-outline" style="border-color:#CF9650; color:#CF9650;">HTML5</span>
                </a>
              </section>
              </div>
                
            </div>
            <div class="panel mb-30" style="border:1px #e0e0e0 solid;">
              <div class="panel-heading">
                <h3 class="panel-title">Member</h3>
              </div>
              <div class="panel-body">
                  <h5>Admin</h5>
                  <ul class="list-unstyled list-inline">
                    <?php for($a=1;$a<=2;$a++){?>
                    <li class="list-inline-item m-5">
                      <div style="position:relative;">
                        <a class="avatar avatar-sm" href="javascript:void(0)" data-toggle="dropdown">
                          <img src="../../global/portraits/<?=rand(1,20)?>.jpg" alt="">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" role="menu">
                          <div class="dropdown-header" role="presentation">Caleb Richards</div>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                            Message
                          </a>
                          <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                            View Profile
                          </a>
                        </div>
                      </div>
                    </li>
                    <?php }?>
                  </ul>
                  <h5>Teams</h5>
                  <ul class="list-unstyled list-inline">
                    <?php for($a=1;$a<=4;$a++){?>
                    <li class="list-inline-item m-5">
                      <div style="position:relative;">
                        <a class="avatar avatar-sm" href="javascript:void(0)" data-toggle="dropdown">
                          <img src="../../global/portraits/<?=rand(1,20)?>.jpg" alt="">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" role="menu">
                          <div class="dropdown-header" role="presentation">Caleb Richards</div>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                            Message
                          </a>
                          <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                            View Profile
                          </a>
                        </div>
                      </div>
                    </li>
                    <?php }?>
                  </ul>
                  <h5>Report to..</h5>
                  <ul class="list-unstyled list-inline">
                    <li class="list-inline-item m-5">
                      <div style="position:relative;">
                        <a class="avatar avatar-sm" href="javascript:void(0)" data-toggle="dropdown">
                          <img src="../../global/portraits/<?=rand(1,20)?>.jpg" alt="">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" role="menu">
                          <div class="dropdown-header" role="presentation">Caleb Richards</div>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                            Message
                          </a>
                          <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                            View Profile
                          </a>
                        </div>
                      </div>
                    </li>
                  </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/slick-carousel/slick.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <script src="../../global/vendor/aspieprogress/jquery-asPieProgress.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/GridMenu.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/masonry.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/animate-list.js"></script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../assets/js/App/Documents.js"></script>
  <script src="../../assets/examples/js/apps/documents.js"></script>
  <script src="../../assets/examples/js/pages/profile-v2.js"></script>
  <script src="../../global/js/Plugin/aspieprogress.js"></script>
  <script src="../../assets/examples/js/charts/pie-progress.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>