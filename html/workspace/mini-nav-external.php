<ul class="nav nav-tabs nav-tabs-line fix-mini-nav" role="tablist">
  <li class="nav-item">
    <a class="nav-link <?=(basename($_SERVER["SCRIPT_FILENAME"])=="workspace-home2-external.php"?"active":"")?>" href="workspace-home2-external.php" aria-expanded="true"><i class="md-home" aria-hidden="true"></i> Workspace Feed</a>
  </li>
  <li class="nav-item">
    <a class="nav-link <?=(basename($_SERVER["SCRIPT_FILENAME"])=="workspace-task-external.php"?"active":"")?>" href="workspace-task-external.php" aria-expanded="false"><i class="md-check-all" aria-hidden="true"></i> Task</a>
  </li>
  <li class="nav-item hidden-sm-down">
    <a class="nav-link <?=(basename($_SERVER["SCRIPT_FILENAME"])=="workspace-drive-external.php"?"active":"")?>" href="workspace-drive-external.php" aria-expanded="false"><i class="md-cloud" aria-hidden="true"></i> Drive</a>
  </li>
  <li class="nav-item hidden-sm-down">
    <a class="nav-link mode-public" href="<?=basename($_SERVER["SCRIPT_FILENAME"],"-external.php").".php"?>" aria-expanded="true"><i class="md-accounts-alt" aria-hidden="true"></i> Internal Mode</a>
  </li>
  <li class="nav-item dropdown hidden-md-up">
    <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#" aria-expanded="false">Menu </a>
    <div class="dropdown-menu dropdown-menu-right" role="menu">
      <a class="dropdown-item" href="workspace-drive-external.php" aria-expanded="false"><i class="md-cloud" aria-hidden="true"></i> Drive</a>
      <a class="dropdown-item" href="javascript:void(0)" aria-expanded="false"><i class="md-comments" aria-hidden="true"></i> Messenger</a>
      <a class="dropdown-item" href="<?=basename($_SERVER["SCRIPT_FILENAME"],"-external.php").".php"?>" aria-expanded="false"><i class="md-accounts-alt" aria-hidden="true"></i> Internal Mode</a>
    </div>
  </li>
</ul>