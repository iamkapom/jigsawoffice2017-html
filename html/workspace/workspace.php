<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../assets/examples/css/advanced/masonry.css">
  <link rel="stylesheet" href="../../global/vendor/aspieprogress/asPieProgress.css">
  <link rel="stylesheet" href="../../assets/examples/css/charts/pie-progress.css">
  <link rel="stylesheet" href="../../assets/examples/css/structure/ribbon.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition page-aside-right">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-5">Workspace</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../home/index.php">Home</a></li>
        <li class="breadcrumb-item active">Workspace</li>
      </ol>
      <div class="page-header-actions">
        <button type="button" onClick="location.href='ws-setting.php';" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Setting">
          <i class="icon md-settings" aria-hidden="true"></i>
        </button>
      </div>
    </div>

    <div class="page-content container-fluid" style="position: relative;">

      <div class="row ml-0 mr-0">
        <div class="col-md-12">
          <div class="page-aside">
            <!-- Contacts Sidebar -->
            <div class="page-aside-switch">
              <i class="icon md-chevron-left" aria-hidden="true"></i>
              <i class="icon md-chevron-right" aria-hidden="true"></i>
            </div>
            <div class="page-aside-inner page-aside-scroll">
              <div data-role="container">
                <div data-role="content">
                  <section class="page-aside-section">

                    <div class="card card-block pt-0" style="min-height: 250px;">
                      <h3 class="mt-0">Summary All Task</h3>
                        <div class="pt-20 pb-20">
                          <div class="pie-progress pie-progress" data-plugin="pieProgress" data-barcolor="#4caf50"
                          data-goal="72" aria-valuenow="72" data-size="100" data-barsize="8"
                          role="progressbar">
                            <div class="pie-progress-content">
                              <div class="pie-progress-number">72 %</div>
                              <div class="pie-progress-label">Progress</div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="card card-block">
                      <h3 class="mt-0">Tasks in Progress</h3>
                      <div class="counter counter-md text-left">
                        <div class="contextual-progress mb-0 mt-10">
                          <div class="clearfix">
                            <div class="progress-title">Order due</div>
                            <div class="progress-label">2 / 5</div>
                          </div>
                          <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                            <div class="progress-bar progress-bar-danger" aria-valuemin="0" aria-valuemax="100" style="width: 40%;" aria-valuenow="60" role="progressbar">
                            </div>
                          </div>
                        </div>

                      </div>
                      <div class="counter counter-md text-left">
                        <div class="contextual-progress mb-0 mt-10">
                          <div class="clearfix">
                            <div class="progress-title">Today</div>
                            <div class="progress-label">5 / 8</div>
                          </div>
                          <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                            <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 60%;" aria-valuenow="60" role="progressbar">
                            </div>
                          </div>
                        </div>

                      </div>
                      <div class="counter counter-md text-left">
                        <div class="contextual-progress mb-0 mt-10">
                          <div class="clearfix">
                            <div class="progress-title">This week</div>
                            <div class="progress-label">14 / 41</div>
                          </div>
                          <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                            <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 34%;" aria-valuenow="60" role="progressbar">
                            </div>
                          </div>
                        </div>

                      </div>
                      <div class="counter counter-md text-left">
                        <div class="contextual-progress mb-0 mt-10">
                          <div class="clearfix">
                            <div class="progress-title">This month</div>
                            <div class="progress-label">45 / 122</div>
                          </div>
                          <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                            <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 36%;" aria-valuenow="60" role="progressbar">
                            </div>
                          </div>
                        </div>

                      </div>

                    </div>



                  </section>
                </div>
              </div>
            </div>
          </div>
          <div class="page-main">
          <div class="pt-0 pb-20">
            <div class="search-wrapper">
              <div class="search-box">
                <div class="icon md-search"></div>
                <div class="currently-showing">
                  <?=$search['tokenhtml']?>
                </div>
              </div>
              <a href="javascript:void(0);" class="clear_all btn btn-flat btn-default btn-block waves-effect waves-classic">Clear All</a>
              <div class="data_entry">
                <input class="input keyword-input" placeholder="Enter a keyword" type="text">
                <div class="icon md-close-circle close"></div>
              </div>
              <div class="filters">
                  <div class="dropdown filter_permission">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      Priority
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu" role="menu">
                      <a class="dropdown-item text-danger" href="javascript:void(0)"><i class="icon fa-circle mr-5" aria-hidden="true"></i>High</a>
                      <a class="dropdown-item text-warning" href="javascript:void(0)"><i class="icon fa-circle mr-5" aria-hidden="true"></i>Normal</a>
                      <a class="dropdown-item text-info" href="javascript:void(0)"><i class="icon fa-circle mr-5" aria-hidden="true"></i>Low</a>
                    </div>
                  </div>
                  <div class="dropdown filter_permission">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      Status
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu" role="menu">
                      <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-cogs mr-5" aria-hidden="true"></i>Progress</a>
                      <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-check-square-o mr-5" aria-hidden="true"></i>Complete</a>
                      <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-adjust mr-5" aria-hidden="true"></i>Hold</a>
                    </div>
                  </div>
                  <div class="dropdown filter_permission">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      Category
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu" role="menu">
                      <a class="dropdown-item" href="javascript:void(0)"> Category 1</a>
                      <a class="dropdown-item" href="javascript:void(0)"> Category 2</a>
                      <a class="dropdown-item" href="javascript:void(0)"> Category 3</a>
                    </div>
                  </div>

              </div>
            </div>
          </div>

          <div >
            <div class="pb-20">
              <div class="actions-inner float-right">
                <div class="dropdown">
                  <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                    <i class="icon md-view-module" aria-hidden="true"></i> <i class="icon fa-sort-amount-desc" aria-hidden="true"></i> First name
                    <span class="icon md-chevron-down" aria-hidden="true"></span>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right dropdown-menu-media w-100" role="menu">
                    <h6 class="pl-10" aria-hidden="false">View</h6>
                    <a class="dropdown-item" href="workspace-list.php"><i class="icon md-view-list" aria-hidden="true"></i> List</a>
                    <a class="dropdown-item active" href="workspace.php"><i class="icon md-view-module" aria-hidden="true"></i> Grid</a>
                    <div class="dropdown-divider"></div>
                    <h6 class="pl-10" aria-hidden="false">Sory by</h6>
                    <a class="dropdown-item active" href="javascript:void(0)">Update</a>
                    <a class="dropdown-item" href="javascript:void(0)">Deadline</a>
                    <a class="dropdown-item" href="javascript:void(0)">Progress</a>
                    <a class="dropdown-item" href="javascript:void(0)">Start Date</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-sort-amount-asc" aria-hidden="true"></i> Ascending</a>
                    <a class="dropdown-item active" href="javascript:void(0)"><i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Descending</a>
                  </div>
                </div>
              </div>


              <div class="pt-10">Total <strong>26</strong> rows</div>
            </div>

            <ul class="blocks blocks-100 blocks-md-3 blocks-sm-2" data-plugin="masonry">
              <?php
              $flow[] ="Budding";
              $flow[] ="Wireframe";
              $flow[] ="Design";
              $flow[] ="HTML";
              $flow[] ="Coding";
              $flow[] ="Setup";
              $flow[] ="MA";

              $priority = array("Highest","High","Normal","Low");
              $priority_color = array("#f3273c","#ff9800","#4caf50","#757575");
              $pri = array("danger","warning","success","dark");
              $arr_detail[0]="Dolemus late utriusque fore eveniet provincia spernat dissentiet. Fit intemperantes.";
              $arr_detail[1]="Geometria eae servare dicat, amicitiam, dixissem principio coniuncta adhibuit quantumcumque placeat easque.";
              $arr_detail[2]="Munere dictum dissentio dicturam mediocriterne honesta, morbi delectus rationibus periculum opinor propterea intuemur poetarum efficeretur interpretaris, labefactant aeternum reformidans.";
              $arr_detail[3]="Adiungimus acutum iudicatum aliud aegritudinem, tritani ignavi incidant quaeritur transferrem loqueretur delectatio, appetere, eruditi.";

              for($a=1;$a<=20;$a++){
                $progress = rand(20,80);
                $rand_pri = rand(0,3);
              ?>
              <li  class="masonry-item">
                <div class="panel panel-<?=$pri[$rand_pri]?> panel-line mb-0" style="border:1px #e0e0e0 solid;">
                  <div class="panel-heading">

                    <div class="more-action" style="">
                      <button type="button" class="btn btn-icon btn-dark btn-round" data-toggle="dropdown" aria-hidden="true">
                        <i class="icon md-more-vert" aria-hidden="true"></i>
                      </button>

                      <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                          Edit
                        </a>
                        <a class="dropdown-item text-danger" href="javascript:void(0)" role="menuitem">
                          Delete
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item text-success" href="javascript:void(0)" role="menuitem">
                          Mark as Complete
                        </a>
                        <a class="dropdown-item text-default" href="javascript:void(0)" role="menuitem">
                          Mark as Hold
                        </a>
                      </div>
                    </div>
                    <div class="p-15" >
                      <a href="workspace-home2.php" target="_blank" style="text-decoration:none;">
                        <div class="clearfix">
                          <div class="progress w-150 mb-0 mt-5 float-left" style="height:12px;" data-labeltype="percentage" data-plugin="progress">
                            <div class="progress-bar progress-bar-<?=$pri[$rand_pri]?>" role="progressbar" style="width: <?=$progress?>%; line-height:12px;">
                              <span class="progress-label font-size-10"><?=$progress?>%</span>
                            </div>
                          </div>
                          <?php
                          if(rand(0,1)){
                          ?>
                          <span class="badge badge-dark float-left ml-10 mt-3">External</span>
                          <?php }?>
                        </div>
                        </a>
                    </div>
                    <h3 class="panel-title px-15 pt-0">
                      <a href="workspace-home2.php" target="_blank">Project Jigsaw Office 2017</a>
                    </h3>
                  </div>
                  <div class="panel-body" style="padding:0 15px 15px;">
                    <?=$arr_detail[rand(0,3)]?>
                    <section>
                      <h6 class="font-size-12 mb-5 mt-20">Strat<span class="ml-30" style="font-weight: 100;">September 30, 2016</span></h6>
                      <h6 class="font-size-12 mb-5 mt-20">Finish<span class="ml-30" style="font-weight: 100;">September 23, 2017</span></h6>
                    </section>
                    <section>
                      <h6 class="font-size-12 mb-5 mt-20">Members</h6>
                      <ul class="addMember-items">
                        <?php for($aa=1;$aa<=rand(3,7);$aa++){?>
                        <li class="addMember-item mr-0">
                          <img style="width:20px;" class="avatar" src="../../global/portraits/<?php echo rand(1,20);?>.jpg" title="Herman Beck">
                        </li>
                        <?php }?>
                      </ul>
                    </section>
                    <section class="mt-20">
                      <span class="badge p-5 mb-5 badge-outline" style="border-color:#7136CB; color:#7136CB;">Design</span>
                      <span class="badge p-5 mb-5 badge-outline" style="border-color:#07C2BF; color:#07C2BF;">Development</span>
                      <span class="badge p-5 mb-5 badge-outline" style="border-color:#F25D94; color:#F25D94;">front-end</span>
                    </section>
                    <section class="mt-20">
                      <ul class="list-inline float-right mt-15">
                        <li class="list-inline-item mr-20">
                          <i class="icon md-check-all"></i> 28
                        </li>
                        <li class="list-inline-item mr-20">
                          <i class="icon fa-comments-o"></i> 96
                        </li>
                        <li class="list-inline-item mr-20">
                          <i class="icon md-attachment-alt"></i> 8
                        </li>
                      </ul>
                    </section>
                  </div>
                </div>
              </li>
              <?php }?>
            </ul>
          </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <div class="site-action" data-plugin="actionBtn">
    <button type="button" data-target="#quickAddWorkspace" data-toggle="modal" class="btn-raised btn btn-success btn-floating">
      <i class="front-icon md-plus" aria-hidden="true"></i>
    </button>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/imagesloaded/imagesloaded.pkgd.js"></script>
  <script src="../../global/vendor/masonry/masonry.pkgd.js"></script>
    <script src="../../global/vendor/aspieprogress/jquery-asPieProgress.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/masonry.js"></script>
    <script src="../../global/js/Plugin/aspieprogress.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>
