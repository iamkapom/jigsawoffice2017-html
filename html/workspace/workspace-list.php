<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../assets/examples/css/advanced/masonry.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../assets/examples/css/apps/documents.css">
  <link rel="stylesheet" href="../../global/vendor/jquery-selective/jquery-selective.css">
  <link rel="stylesheet" href="../../global/vendor/aspieprogress/asPieProgress.css">
  <link rel="stylesheet" href="../../assets/examples/css/charts/pie-progress.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <style type="text/css">
  .fix-mini-nav + .page-content{
    padding-top: 0;
  }
  .fix-mini-nav .page-header{
    margin-bottom: 93px;
  }
  .fix-mini-nav .fix-mini-nav{
    margin-bottom: 0;
    margin-right: 0;
    margin-top: 0;
    padding: 0;
    position: fixed;
    top: 65px;
    width: 100%;
    z-index: 10;
    left: 0;
  }
  .btn-comment-post .btn-select{
    border: 0px !important;
  }
  .btn-comment-post, .btn-comment-post .bootstrap-select{
    width: 150px !important;
  }
  </style>
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition app-documents page-aside-right">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
        <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-10">Workspace</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../home/index.php">Home</a></li>
        <li class="breadcrumb-item active">Workspace</li>
      </ol>
  <?php include("mini-nav-ws.php");?>
    </div>

    <div class="page-content container-fluid" style="position: relative;">

      <div class="row ml-0 mr-0">
        <div class="col-md-12">
          <div class="page-aside">
            <!-- Contacts Sidebar -->
            <div class="page-aside-switch">
              <i class="icon md-chevron-left" aria-hidden="true"></i>
              <i class="icon md-chevron-right" aria-hidden="true"></i>
            </div>
            <div class="page-aside-inner page-aside-scroll">
              <div data-role="container">
                <div data-role="content">
                  <section class="page-aside-section">

                    <div class="card card-block pt-0" style="min-height: 250px;">
                      <h3 class="mt-0">Summary All Task</h3>
                        <div class="pt-20 pb-20">
                          <div class="pie-progress pie-progress" data-plugin="pieProgress" data-barcolor="#4caf50"
                          data-goal="72" aria-valuenow="72" data-size="100" data-barsize="8"
                          role="progressbar">
                            <div class="pie-progress-content">
                              <div class="pie-progress-number">72 %</div>
                              <div class="pie-progress-label">Progress</div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="card card-block">
                      <h3 class="mt-0">Tasks in Progress</h3>
                      <div class="counter counter-md text-left">
                        <div class="contextual-progress mb-0 mt-10">
                          <div class="clearfix">
                            <div class="progress-title">Order due</div>
                            <div class="progress-label">2 / 5</div>
                          </div>
                          <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                            <div class="progress-bar progress-bar-danger" aria-valuemin="0" aria-valuemax="100" style="width: 40%;" aria-valuenow="60" role="progressbar">
                            </div>
                          </div>
                        </div>

                      </div>
                      <div class="counter counter-md text-left">
                        <div class="contextual-progress mb-0 mt-10">
                          <div class="clearfix">
                            <div class="progress-title">Today</div>
                            <div class="progress-label">5 / 8</div>
                          </div>
                          <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                            <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 60%;" aria-valuenow="60" role="progressbar">
                            </div>
                          </div>
                        </div>

                      </div>
                      <div class="counter counter-md text-left">
                        <div class="contextual-progress mb-0 mt-10">
                          <div class="clearfix">
                            <div class="progress-title">This week</div>
                            <div class="progress-label">14 / 41</div>
                          </div>
                          <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                            <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 34%;" aria-valuenow="60" role="progressbar">
                            </div>
                          </div>
                        </div>

                      </div>
                      <div class="counter counter-md text-left">
                        <div class="contextual-progress mb-0 mt-10">
                          <div class="clearfix">
                            <div class="progress-title">This month</div>
                            <div class="progress-label">45 / 122</div>
                          </div>
                          <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                            <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 36%;" aria-valuenow="60" role="progressbar">
                            </div>
                          </div>
                        </div>

                      </div>

                    </div>



                  </section>
                </div>
              </div>
            </div>
          </div>
            <div class="page-main">
        <div class="pt-0 pb-20">
          <div class="search-wrapper">
            <div class="search-box">
              <div class="icon md-search"></div>
              <div class="currently-showing">
                <?=$search['tokenhtml']?>
              </div>
            </div>
            <a href="javascript:void(0);" class="clear_all btn btn-flat btn-default btn-block waves-effect waves-classic">Clear All</a>
            <div class="data_entry">
              <input class="input keyword-input" placeholder="Enter a keyword" type="text">
              <div class="icon md-close-circle close"></div>
            </div>
            <div class="filters">
                <div class="dropdown filter_permission">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      Mode
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu" role="menu">
                      <a class="dropdown-item" href="javascript:void(0)"><i class="icon md-accounts-alt mr-5" aria-hidden="true"></i>Internal Mode</a>
                      <a class="dropdown-item" href="javascript:void(0)"><i class="icon md-globe-alt mr-5" aria-hidden="true"></i>External Mode</a>
                    </div>
                  </div>
                <div class="dropdown filter_permission">
                  <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                    Priority
                    <span class="icon md-chevron-down" aria-hidden="true"></span>
                  </button>
                  <div class="dropdown-menu" role="menu">
                    <a class="dropdown-item text-danger" href="javascript:void(0)"><i class="icon fa-circle mr-5" aria-hidden="true"></i>Highest</a>
                    <a class="dropdown-item text-warning" href="javascript:void(0)"><i class="icon fa-circle mr-5" aria-hidden="true"></i>High</a>
                    <a class="dropdown-item text-success" href="javascript:void(0)"><i class="icon fa-circle mr-5" aria-hidden="true"></i>Normal</a>
                    <a class="dropdown-item text-default" href="javascript:void(0)"><i class="icon fa-circle mr-5" aria-hidden="true"></i>Low</a>
                  </div>
                </div>
                <div class="dropdown filter_permission">
                  <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                    Status
                    <span class="icon md-chevron-down" aria-hidden="true"></span>
                  </button>
                  <div class="dropdown-menu" role="menu">
                    <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-cogs mr-5" aria-hidden="true"></i>Progress</a>
                    <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-check-square-o mr-5" aria-hidden="true"></i>Complete</a>
                    <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-adjust mr-5" aria-hidden="true"></i>Hold</a>
                  </div>
                </div>
                <div class="dropdown filter_permission">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      Category
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu" role="menu">
                      <a class="dropdown-item" href="javascript:void(0)"> Category 1</a>
                      <a class="dropdown-item" href="javascript:void(0)"> Category 2</a>
                      <a class="dropdown-item" href="javascript:void(0)"> Category 3</a>
                    </div>
                  </div>
            </div>
          </div>
        </div>

        <div >
          <div class="pb-20">
            <div class="actions-inner float-right">
              <div class="dropdown">
                <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                  <i class="icon md-view-module" aria-hidden="true"></i> <i class="icon fa-sort-amount-desc" aria-hidden="true"></i> First name
                  <span class="icon md-chevron-down" aria-hidden="true"></span>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-media w-100" role="menu">
                  <h6 class="pl-10" aria-hidden="false">View</h6>
                  <a class="dropdown-item active" href="workspace-list.php"><i class="icon md-view-list" aria-hidden="true"></i> List</a>
                  <a class="dropdown-item" href="workspace.php"><i class="icon md-view-module" aria-hidden="true"></i> Grid</a>
                  <div class="dropdown-divider"></div>
                  <h6 class="pl-10" aria-hidden="false">Sory by</h6>
                  <a class="dropdown-item active" href="javascript:void(0)">Update</a>
                  <a class="dropdown-item" href="javascript:void(0)">Deadline</a>
                  <a class="dropdown-item" href="javascript:void(0)">Progress</a>
                  <a class="dropdown-item" href="javascript:void(0)">Start Date</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-sort-amount-asc" aria-hidden="true"></i> Ascending</a>
                  <a class="dropdown-item active" href="javascript:void(0)"><i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Descending</a>
                </div>
              </div>
            </div>


            <div class="pt-10">Total <strong>26</strong> rows</div>
          </div>

          <section >
              <div class="bg-white p-10">
          <table class="table table-hover" data-plugin="selectable" data-row-selectable="true">
            <thead>
              <tr>
                <th>
                  Project
                </th>
                <th width="35%" class="hidden-xs-down">
                  Strat/Finish
                </th>
                <th width="25%" class="hidden-md-down">
                  Member/More
                </th>
                <th width="5%">&nbsp;</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $priority_color = array("#f3273c","#ff9800","#4caf50","#757575");
              $pri = array("danger","warning","success","dark");
              $b=1;
              for($a=1;$a<=30;$a++){
                if($a==21) $b=1;
                $progress = rand(20,80);
                $rand_pri = rand(0,3);
              ?>
              <tr class="">
                <td>
                  <a href="workspace-home2.php" target="_blank" style="text-decoration:none;">
                  <div class="clearfix mb-5">
                    <div class="text-<?=$pri[$rand_pri]?> float-left"><i class="icon fa-circle mr-5" aria-hidden="true"></i></div>
                    <div class="progress w-150 mb-0 mt-5 float-left" style="height:12px;" data-labeltype="percentage" data-plugin="progress">
                      <div class="progress-bar progress-bar-<?=$pri[$rand_pri]?>" role="progressbar" style="width: <?=$progress?>%; line-height:12px;">
                        <span class="progress-label font-size-10"><?=$progress?>%</span>
                      </div>
                    </div>
                    <?php
                    if(rand(0,1)){
                    ?>
                    <span class="badge badge-dark float-left ml-10 mt-3">External</span>
                    <?php }?>
                  </div>
                  <p class="mb-10 font-size-16 text-nowrap"><strong>Project Jigsaw Office 2017</strong></p>
                  </a>
                  <div class="hidden-lg-up">
                    <div class="hidden-sm-up">
                      <h6 class="font-size-12 mb-5 mt-3">
                        <span class="w-50 grey-600" style="display: inline-table;">Strat</span>
                        <span style="font-weight: 100;">September 30, 2016</span></h6>
                      <h6 class="font-size-12 mb-10">
                        <span class="w-50 grey-600" style="display: inline-table;">Finish</span>
                        <span style="font-weight: 100;">September 23, 2017</span></h6>
                    </div>
                    <ul class="addMember-items mb-10">
                      <?php for($aa=1;$aa<=rand(3,7);$aa++){?>
                      <li class="addMember-item mr-0">
                        <img style="width:20px;" class="avatar" src="../../global/portraits/<?php echo rand(1,20);?>.jpg" title="Herman Beck">
                      </li>
                      <?php }?>
                    </ul>
                    <ul class="list-inline hidden-sm-up">
                      <li class="list-inline-item mr-10">
                        <i class="icon fa-tasks"></i> 28
                      </li>
                      <li class="list-inline-item mr-10">
                        <i class="icon fa-comments-o"></i> 96
                      </li>
                      <li class="list-inline-item mr-10">
                        <i class="icon md-attachment-alt"></i> 8
                      </li>
                      <li class="list-inline-item">
                        <i class="icon fa-sticky-note"></i> 8
                      </li>
                    </ul>
                  </div>

                </td>
                <td class="hidden-xs-down">
                  <h6 class="font-size-12 mb-5 mt-3">
                    <span class="w-50 grey-600" style="display: inline-table;">Strat</span>
                    <span style="font-weight: 100;">September 30, 2016</span></h6>
                  <h6 class="font-size-12 mb-5">
                    <span class="w-50 grey-600" style="display: inline-table;">Finish</span>
                    <span style="font-weight: 100;">September 23, 2017</span></h6>
                  <div class="hidden-lg-up">
                      <ul class="list-inline mt-20 mb-0">
                        <li class="list-inline-item mr-10">
                          <i class="icon fa-tasks"></i> 28
                        </li>
                        <li class="list-inline-item mr-10">
                          <i class="icon fa-comments-o"></i> 96
                        </li>
                        <li class="list-inline-item mr-10">
                          <i class="icon md-attachment-alt"></i> 8
                        </li>
                        <li class="list-inline-item">
                          <i class="icon fa-sticky-note"></i> 8
                        </li>
                      </ul>
                    </div>
                </td>
                <td class="hidden-md-down">
                    <ul class="addMember-items mb-10">
                      <?php for($aa=1;$aa<=rand(3,7);$aa++){?>
                      <li class="addMember-item mr-0">
                        <img style="width:20px;" class="avatar" src="../../global/portraits/<?php echo rand(1,20);?>.jpg" title="Herman Beck">
                      </li>
                      <?php }?>
                    </ul>
                    <ul class="list-inline">
                      <li class="list-inline-item mr-10">
                        <i class="icon fa-tasks"></i> 28
                      </li>
                      <li class="list-inline-item mr-10">
                        <i class="icon fa-comments-o"></i> 96
                      </li>
                      <li class="list-inline-item mr-10">
                        <i class="icon md-attachment-alt"></i> 8
                      </li>
                    </ul>
                </td>
                <td align="center">
                  <div class="dropdown">
                    <button class="btn btn-icon btn-round waves-effect waves-classic p-5 btn-pure" data-toggle="dropdown" href="#" aria-expanded="false">
                    <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                        Edit
                      </a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item text-danger" href="javascript:void(0)" role="menuitem">
                        Delete
                      </a>
                    </div>
                  </div>
                </td>
              </tr>
            <?php }?>
            </tbody>
          </table>
        </div>
          <div class="data-pagination">
              <div class="pagination-per-page">
                  <span class="page-list">
                    <span class="btn-group dropup">
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <span class="page-size">10</span>
                      </button>
                      <ul class="dropdown-menu dropdown-menu-bullet" role="menu">
                        <li role="menuitem" class="dropdown-item active"><a href="#">10</a></li>
                        <li role="menuitem" class="dropdown-item"><a href="#">25</a></li>
                      </ul>
                  </span> rows per page
                </span>
              </div>
              <nav>
                <ul class="pagination justify-content-center pagination-gap">
                  <li class="disabled page-item">
                    <a class="page-link" href="javascript:void(0)" aria-label="Previous">
                      <span aria-hidden="true">&laquo;</span>
                    </a>
                  </li>
                  <?php
                  $page = 4;
                  for($aa=1;$aa<=7;$aa++){?>
                  <li class="<?=($aa==$page?"active":"")?> page-item <?=($aa<($page-2) || $aa>($page+2)?"hidden-sm-down":"")?> <?=($aa==($page-2) || $aa==($page+2)?"hidden-xs-down":"")?>"><a class="page-link" href="javascript:void(0)"><?=$aa?></a></li>
                  <?php }?>
                  <li class="page-item">
                    <a class="page-link" href="javascript:void(0)" aria-label="Next">
                      <span aria-hidden="true">&raquo;</span>
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
        </section>
</div>

        </div>
        </div>
      </div>
    </div>
  </div>
  <div class="site-action" data-plugin="actionBtn">
    <button type="button" data-target="#quickAddWorkspace" data-toggle="modal" class="btn-raised btn btn-success btn-floating">
      <i class="front-icon md-plus" aria-hidden="true"></i>
    </button>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <script src="../../global/vendor/masonry/masonry.pkgd.js"></script>
  <script src="../../global/vendor/aspieprogress/jquery-asPieProgress.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/masonry.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/aspieprogress.js"></script>
  <script src="../../assets/js/App/Documents.js"></script>
  <script src="../../assets/examples/js/apps/documents.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
      var wrap = $('.page');
      $(window).scroll(function () {
        if ($(this).scrollTop() > 125) {
          wrap.addClass("fix-mini-nav");
        } else {
          wrap.removeClass("fix-mini-nav");
        }
      });
    });

  })(document, window, jQuery);
  </script>
  <?php include("../_footer-form.php");?>
</body>
</html>
