<section>
  <div class="bg-grey-600 white p-15 clearfix" style="padding-bottom: 0px;">
    <h3 class="white"><strong>Jigsaw Office application design mockups</strong></h3>
    <p class="font-size-14">via Jigsaw Office Co-working</p>
    <div style="position:absolute;right:0;top:0;">
        <button type="button" class="btn btn-pure btn-inverse font-size-30 slidePanel-close icon md-close" aria-hidden="true"></button>
    </div>
    <div>
        <button type="button" class="btn btn-pure btn-inverse font-size-20 icon md-edit" aria-hidden="true"></button>
        <button type="button" class="btn btn-pure btn-inverse font-size-20 icon md-delete" aria-hidden="true"></button>
    </div>
  </div>
</section>
<div class="site-sidebar-tab-content tab-content">
  <div class="tab-pane fade active show">
    <div>
      <div>
        <div class="list-group p-15">
          <section>
               <a href="#">
              <span class="badge p-5 mb-5 badge-outline" style="border-color:#F2543B; color:#F2543B;">Designer</span>
            </a>
                                <a href="#">
              <span class="badge p-5 mb-5 badge-outline" style="border-color:#E8AD73; color:#E8AD73;">jQuery</span>
            </a>
                                <a href="#">
              <span class="badge p-5 mb-5 badge-outline" style="border-color:#C6EF3D; color:#C6EF3D;">Javascript</span>
            </a>
                                <a href="#">
              <span class="badge p-5 mb-5 badge-outline" style="border-color:#CF9650; color:#CF9650;">HTML5</span>
            </a>
          </section>
          <section>
            <div class="black mt-20">
              <p class="bg-grey-200 p-20">For highlighting a run of text due to its relevance in another
                        context, use the <code>&lt;mark&gt;</code> tag. Like this:</p>
            </div>
          </section>
          <section>
            <div class="row">
              <div class="col-md-7">
                <h5>Information</h5>
                <ul class="list-group list-group-full">
                  <li class="list-group-item">
                    <span style="display:inline-block;" class="w-150 grey-500">Workspace</span>
                    <span><a href="#"><strong>Jigsaw Office Co-working</strong></a></span>
                  </li>
                  <li class="list-group-item">
                    <span style="display:inline-block;" class="w-150 grey-500">Start</span>
                    <span>15/05/2017</span>
                  </li>
                  <li class="list-group-item">
                    <span style="display:inline-block;" class="w-150 grey-500">Duration</span>
                    <span>3 days</span>
                  </li>
                  <li class="list-group-item clearfix">
                    <div class="w-120 grey-500 float-left">Priority</div>
                    <div class="dropdown float-left pl-10">
                      <button type="button" class="btn btn-pure waves-effect waves-classic text-warning" data-toggle="dropdown" aria-expanded="false">
                        <i class="icon fa-circle" aria-hidden="true"></i>High  Priority 
                        <span class="icon md-chevron-down" aria-hidden="true"></span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item text-danger" href="javascript:void(0)"><i class="icon fa-circle mr-5" aria-hidden="true"></i>Highest</a>
                        <a class="dropdown-item active text-warning" href="javascript:void(0)"><i class="icon fa-circle mr-5" aria-hidden="true"></i>High</a>
                        <a class="dropdown-item text-success" href="javascript:void(0)"><i class="icon fa-circle mr-5" aria-hidden="true"></i>Normal</a>
                        <a class="dropdown-item text-default" href="javascript:void(0)"><i class="icon fa-circle mr-5" aria-hidden="true"></i>Low</a>
                      </div>
                    </div>
                  </li>
                  <li class="list-group-item clearfix">
                    <div class="w-120 grey-500 float-left">Stutus</div>
                    <div class="dropdown float-left pl-10">
                      <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        <i class="icon fa-check-square" aria-hidden="true"></i> Status
                        <span class="icon md-chevron-down" aria-hidden="true"></span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-flash mr-5" aria-hidden="true"></i>Overdue</a>
                        <a class="dropdown-item active" href="javascript:void(0)"><i class="icon fa-cogs mr-5" aria-hidden="true"></i>Progress</a>
                        <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-check-square-o mr-5" aria-hidden="true"></i>Complete</a>
                        <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-adjust mr-5" aria-hidden="true"></i>Hold</a>
                      </div>
                    </div>
                  </li>
                  <li class="list-group-item">
                    <span style="display:inline-block;" class="w-150 grey-500">Created</span>
                    <span>May 5 at 11:00 AM</span>
                  </li>
                </ul>
              </div>
              <div class="col-md-5">
                <h5>Owner</h5>
                <ul class="list-unstyled list-inline">
                  <li class="list-inline-item m-5">
                    <div style="position:relative;">
                      <a class="avatar" href="javascript:void(0)" data-toggle="dropdown">
                        <img src="../../global/portraits/1.jpg" alt="">
                      </a>
                      <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <div class="dropdown-header" role="presentation">Caleb Richards</div>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                          Message
                        </a>
                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                          View Profile
                        </a>
                      </div>
                    </div>
                  </li>
                </ul>
                <h5>Assign to...</h5>
                <ul class="list-unstyled list-inline">
                  <li class="list-inline-item m-5">
                    <div style="position:relative;">
                      <a class="avatar" href="javascript:void(0)" data-toggle="dropdown">
                        <img src="../../global/portraits/11.jpg" alt="">
                      </a>
                      <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <div class="dropdown-header" role="presentation">Caleb Richards</div>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                          Message
                        </a>
                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                          View Profile
                        </a>
                      </div>
                    </div>
                  </li>
                  
                  
                </ul>
              </div>
            </div>
          </section>
          
          <section>
            <div class="row mt-30">
              <div class="col-md-12">
                <div class="nav-tabs-horizontal" data-plugin="tabs">
                  <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" href="#exampleTabsOne" aria-controls="exampleTabsOne" role="tab" aria-expanded="true">Comments</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleTabsTwo" aria-controls="exampleTabsTwo" role="tab" aria-expanded="false">Files</a></li>
                  </ul>
                  <div class="tab-content pt-20">
                    <div class="tab-pane active" id="exampleTabsOne" role="tabpanel" aria-expanded="true">
                    
                      <section>
                        <div class="comment media" style="border-bottom:1px #eee solid;flex-direction:initial;">
                          <div class="pr-20">
                            <a class="avatar avatar-lg" href="javascript:void(0)">
                              <img src="../../global/portraits/1.jpg" alt="...">
                            </a>
                          </div>
                          <div class="media-body">
                            <div class="comment-body">
                              <a class="comment-author" href="javascript:void(0)">Caleb Richards</a>
                              <div class="comment-meta">
                                <span class="date">5 days ago</span>
                              </div>
                              <div class="comment-content">
                                <p>Elliot you are always so right :)</p>
                              </div>
                              <div class="comment-actions">
                                <a class="text-like icon md-favorite" href="javascript:void(0)" role="button"></a>
                                <a href="javascript:void(0)" role="button">Reply</a>
                              </div>
                            </div>
                            <div class="comment m-0 p-0">
                              <div class="comment media" style="border-top:1px #eee solid;flex-direction:initial;">
                                <div class="pr-20">
                                  <a class="avatar avatar-lg" href="javascript:void(0)">
                                    <img src="../../global/portraits/3.jpg" alt="...">
                                  </a>
                                </div>
                                <div class="comment-body media-body">
                                  <a class="comment-author" href="javascript:void(0)">Nathan Watts</a>
                                  <div class="comment-meta">
                                    <span class="date">5 days ago</span>
                                  </div>
                                  <div class="comment-content">
                                    <p>Elliot you are always so right :)</p>
                                  </div>
                                  <div class="comment-actions">
                                    <a class="text-like icon md-favorite" href="javascript:void(0)" role="button"></a>
                                    <a href="javascript:void(0)" role="button">Reply</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="comment media" style="border-bottom:1px #eee solid;flex-direction:initial;">
                          <div class="pr-20">
                            <a class="avatar avatar-lg" href="javascript:void(0)">
                              <img src="../../global/portraits/11.jpg" alt="...">
                            </a>
                          </div>
                          <div class="media-body">
                            <div class="comment-body">
                              <a class="comment-author" href="javascript:void(0)">Crystal Bates</a>
                              <div class="comment-meta">
                                <span class="date">5 days ago</span>
                              </div>
                              <div class="comment-content">
                                <p>Dude, this is awesome. Thanks so much</p>
                              </div>
                              <div class="comment-actions">
                                <a class="text-like icon md-favorite" href="javascript:void(0)" role="button"></a>
                                <a href="javascript:void(0)" role="button">Reply</a>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="comment media" style="border-bottom:1px #eee solid;flex-direction:initial;">
                          <div class="pr-20">
                            <a class="avatar avatar-lg" href="javascript:void(0)">
                              <img src="../../global/portraits/6.jpg" alt="...">
                            </a>
                          </div>
                          <div class="media-body">
                            <div class="comment-body">
                              <a class="comment-author" href="javascript:void(0)">Crystal Bates</a>
                              <div class="comment-meta">
                                <span class="date">5 days ago</span>
                              </div>
                              <div class="comment-content">
                                <p>Yes, That's really awesome!</p>
                              </div>
                              <div class="comment-actions">
                                <a class="text-like icon md-favorite" href="javascript:void(0)" role="button"></a>
                                <a href="javascript:void(0)" role="button">Reply</a>
                              </div>
                            </div>
                          </div>
                        </div>
                        <form class="comment-reply" action="#" method="post">
                          <div class="form-group">
                            <textarea class="form-control" rows="5" placeholder="Comment here"></textarea>
                          </div>
                          <div class="form-group">
                            <button type="submit" class="btn btn-primary waves-effect waves-classic">Comment</button>
                            <button type="button" class="btn btn-link grey-600 waves-effect waves-classic">close</button>
                          </div>
                        </form>
                      </section>
                    </div>
                    <div class="tab-pane" id="exampleTabsTwo" role="tabpanel" aria-expanded="false">
                      <table class="table table-hover">
                          <thead>
                            <tr>
                              <th>Name</th>
                              <th width="20%">Owner</th>
                              <th class="hidden-sm-down" width="20%">Created</th>
                              <th class="hidden-sm-down" width="20%">File size</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>
                                <div class="media" style="flex-direction:initial;">
                                  <div class="pr-20">
                                    <a data-url="panel-file.html" data-toggle="slidePanel" class="font-size-24" href="javascript:void(0)">
                                      <i style="color:#ff9800" class="icon fa-file-powerpoint-o" aria-hidden="true"></i>
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <h5><a style="color:#616161;text-decoration: none;" data-url="panel-folder.html" data-toggle="slidePanel" href="javascript:void(0)">TOR_Checklist</a></h5>
                                    <p class="hidden-md-up mb-0 grey-500">
                                      <small>
                                        Mar 3, 2017 <span class="ml-10 mr-10">&mdash;</span> 2.85 MB                            </small>
                                    </p>
                                  </div>
                                </div>
                              </td>
                              <td class="pt-15"><img style="width:20px;" class="avatar" src="../../global/portraits/1.jpg" title="Herman Beck">
                              <span class="hidden-md-down">Herman Beck</span></td>
                              <td class="hidden-sm-down pt-15">Mar 3, 2017</td>
                              <td class="hidden-sm-down pt-15">2.85 MB</td>
                            </tr>
                            <tr>
                              <td>
                                <div class="media" style="flex-direction:initial;">
                                  <div class="pr-20">
                                    <a data-url="panel-file.html" data-toggle="slidePanel" class="font-size-24" href="javascript:void(0)">
                                      <i style="color:#f44336" class="icon fa-file-pdf-o" aria-hidden="true"></i>
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <h5><a style="color:#616161;text-decoration: none;" data-url="panel-folder.html" data-toggle="slidePanel" href="javascript:void(0)">wireframe inner</a></h5>
                                    <p class="hidden-md-up mb-0 grey-500">
                                      <small>
                                        Mar 3, 2017 <span class="ml-10 mr-10">&mdash;</span> 4.26 KB                            </small>
                                    </p>
                                  </div>
                                </div>
                              </td>
                              <td class="pt-15">me</td>
                              <td class="hidden-sm-down pt-15">Mar 3, 2017</td>
                              <td class="hidden-sm-down pt-15">4.26 KB</td>
                            </tr>
                                              <tr>
                              <td>
                                <div class="media" style="flex-direction:initial;">
                                  <div class="pr-20">
                                    <a data-url="panel-file.html" data-toggle="slidePanel" class="font-size-24" href="javascript:void(0)">
                                      <i style="color:#4caf50" class="icon fa-file-excel-o" aria-hidden="true"></i>
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <h5><a style="color:#616161;text-decoration: none;" data-url="panel-folder.html" data-toggle="slidePanel" href="javascript:void(0)">เอกสารรวบรวม Data</a></h5>
                                    <p class="hidden-md-up mb-0 grey-500">
                                      <small>
                                        Mar 3, 2017 <span class="ml-10 mr-10">&mdash;</span> 1.52 MB                            </small>
                                    </p>
                                  </div>
                                </div>
                              </td>
                              <td class="pt-15"><img style="width:20px;" class="avatar" src="../../global/portraits/1.jpg" title="Herman Beck">
                              <span class="hidden-md-down">Herman Beck</span></td>
                              <td class="hidden-sm-down pt-15">Mar 3, 2017</td>
                              <td class="hidden-sm-down pt-15">1.52 MB</td>
                            </tr>
                                              <tr>
                              <td>
                                <div class="media" style="flex-direction:initial;">
                                  <div class="pr-20">
                                    <a data-url="panel-file.html" data-toggle="slidePanel" class="font-size-24" href="javascript:void(0)">
                                      <i style="color:#ffab00" class="icon fa-file-picture-o" aria-hidden="true"></i>
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <h5><a style="color:#616161;text-decoration: none;" data-url="panel-folder.html" data-toggle="slidePanel" href="javascript:void(0)">WEB Design Trend</a></h5>
                                    <p class="hidden-md-up mb-0 grey-500">
                                      <small>
                                        Mar 3, 2017 <span class="ml-10 mr-10">&mdash;</span> 4.25 MB                            </small>
                                    </p>
                                  </div>
                                </div>
                              </td>
                              <td class="pt-15">me</td>
                              <td class="hidden-sm-down pt-15">Mar 3, 2017</td>
                              <td class="hidden-sm-down pt-15">4.25 MB</td>
                            </tr>
                                            </tbody>
                        </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          

        </div>
      </div>
    </div>
  </div>

  <div class="tab-pane fade" id="sidebar-user">
    <div>
      <div>
        <div class="list-group pr-10">
          <?php 
          $b=1;
          for($a=1;$a<=25;$a++){
            $online = rand(0,1);
            if($a==21) $b=1;
          ?>
          <a  href="javascript:void(0)" class="list-group-item p-5 pr-10 pl-10">
            <div class="media" style="flex-direction:initial;">
              <div class="pr-10">
                <div class="avatar avatar-online">
                  <img class="img-fluid" src="../../global/portraits/<?=$b++?>.jpg" alt="...">
                  <?php if($online){?>
                  <i></i>
                  <?php }?>
                </div>
              </div>
              <div class="media-body">
                <div class="black">Edward Fletcher</div>
                <div class="grey-600 font-size-12">Robin</div>
              </div>
            </div>
          </a>
          <?php }?>
        </div>
      </div>
    </div>
  </div>

  <div class="tab-pane fade" id="sidebar-group">
    <div>
      <div>
        <div class="list-group pr-10">
          <?php
          $b=1;
          for($a=1;$a<=10;$a++){
            $online = rand(0,1);
            $tmpC = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
          ?>
          <a href="javascript:void(0)" class="list-group-item p-5 pr-10 pl-10">
            <div class="media" style="flex-direction:initial;">
              <div class="pr-10">
                <button type="button" style="background:<?php echo $tmpC;?>;" class="btn btn-icon btn-round waves-effect waves-classic">
                <i class="icon fa-group mr-0 white" aria-hidden="true"></i>
                </button>
              </div>
              <div class="media-body">
                <div class="black">Jigsaw Office R&D</div>
                <ul class="addMember-items">
                  <?php for($aa=1;$aa<=rand(3,7);$aa++){?>
                  <li class="addMember-item mr-0">
                    <img style="width:20px;" class="avatar" src="../../global/portraits/<?php echo rand(1,20);?>.jpg" title="Herman Beck">
                  </li>
                  <?php }?>
                </ul>
                <div class="addMember-trigger pt-1 font-size-10">
                  <strong>+<?=rand(2,10)?></strong>
                </div>
              </div>
            </div>
          </a>
          <?php }?>
        </div>
      </div>
    </div>
  </div>
</div>