<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../global/vendor/jquery-selective/jquery-selective.css">
  <link rel="stylesheet" href="../../global/vendor/slick-carousel/slick.css">
  <link rel="stylesheet" href="../../assets/examples/css/pages/profile_v3.css">
  <link rel="stylesheet" href="../../global/vendor/aspieprogress/asPieProgress.css">
  <link rel="stylesheet" href="../../assets/examples/css/charts/pie-progress.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-daterangepicker/daterangepicker.css" />
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition page-profile-v2">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../home2/_header.php");?>
  <?php include("../home2/_header-menubar.php");?>
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      
      <h1 class="page-title mb-5">Jigsaw Office Co-Working</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item grey-400">Jigsaw Office Co-Working</li>
      </ol>

      <?php include("mini-nav-external2.php");?>
    </div>
    
    
    <div class="page-content container-fluid " style="position: relative;">
      
      <div class="page-main">
        
        <div class="row ml-0 mr-0">
          <div class="col-lg-12 col-xl-6 push-xl-3">

            <section>
              <div class="card card-shadow wall-posting">
                <div class="card-block post-display" data-target="#postFormQuick" data-toggle="modal">
                  <div class="avatar avatar-lg">
                      <img src="../../global/portraits/6.jpg" alt="">
                  </div>
                  <div class="grey-400 post-area">
                    Whats in your mind today?
                    <div class="post-attach">
                      <button class="btn btn-icon btn-dark btn-round bg-grey-400">
                        <i class="icon md-attachment-alt rotate" aria-hidden="true"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              
            </section>
            
            <div class="pb-10">
            <div class="search-wrapper">
              <div class="search-box">
                <div class="icon md-search"></div>
                <div class="currently-showing">
                  <?=$search['tokenhtml']?>
                </div>
              </div>
              <a href="javascript:void(0);" class="clear_all btn btn-flat btn-default btn-block waves-effect waves-classic">Clear All</a>
              <div class="data_entry">
                <input class="input keyword-input" placeholder="Enter a keyword" type="text">
                <div class="icon md-close-circle close"></div>
              </div>
              <div class="filters">
                  
                  <div class="dropdown filter_permission">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      Filter
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu" role="menu">
                      <a class="dropdown-item" href="javascript:void(0)"><i class="icon md-bookmark mr-5" aria-hidden="true"></i>Bookmark</a>
                      <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-line-chart mr-5" aria-hidden="true"></i>Report</a>
                    </div>
                  </div>
                  <div class="dropdown filter_permission">
                    <button type="button" id="SearchDateRange" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      Time <i class="icon md-chevron-down" aria-hidden="true"></i>
                      <span id="daterange-value"></span>
                    </button>
                  </div>
                  
              </div>
            </div>
          </div>
          <div class="pb-20">
              <div class="actions-inner float-right">
                <div class="dropdown">
                  <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                    <i class="icon fa-sort-amount-desc" aria-hidden="true"></i> New Post
                    <span class="icon md-chevron-down" aria-hidden="true"></span>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right dropdown-menu-media w-100" role="menu">
                    <h6 class="pl-10" aria-hidden="false">Sory by</h6>
                    <a class="dropdown-item active" href="javascript:void(0)">New Post</a>
                    <a class="dropdown-item" href="javascript:void(0)">Recent Activity</a>
                    <a class="dropdown-item" href="javascript:void(0)">Top Comments</a>
                    <a class="dropdown-item" href="javascript:void(0)">Top Likes</a>
                    <a class="dropdown-item" href="javascript:void(0)">Attach File</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-sort-amount-asc" aria-hidden="true"></i> Ascending</a>
                    <a class="dropdown-item active" href="javascript:void(0)"><i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Descending</a>
                  </div>
                </div>
              </div>
              

              <div class="pt-10">About <strong>26</strong> results</div>
            </div>

            
              <div>
              <!-- Start Post Section -->
              <div class="" >Text Post</div>
              <div class="card card-shadow card-bordered post-wall">
                <div class="card-block clearfix p-25">
                  <div class="media-avatar">
                    <a href="#" class="avatar avatar-lg">
                      <img class="img-fluid" src="../../../global/portraits/6.jpg">
                    </a>
                  </div>
                  <div class="media-body text-middle">
                    <h5 class="mt-0 mb-0">
                      <a href="#">Mallinda Hollaway</a> to <a href="#">JigsawOffice2017</a> 
                      <span class="badge badge-dark">External</span>
                      
                    </h5>
                    <small>30th July 2017</small>
                  </div>

                  <div class="wall-action">
                    <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                      <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                    </button>
                    
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                        Edit Post
                      </a>
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                        Bookmark
                      </a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item text-danger" href="javascript:void(0)" role="menuitem">
                        Delete
                      </a>
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                        Report Post
                      </a>
                    </div>
                  </div>
                </div>
                <div class="card-block px-25 pt-0 pb-0">
                  <p class="card-text mb-20 grey-800">
                    Ex quis excepteur exercitation incididunt ea amet commodo deserunt nulla. Anim
                    sit duis cupidatat ipsum deserunt reprehenderit fugiat cupidatat.
                    In do eu sint Lorem qui eu eu. Id ad non pariatur culpa. Duis proident
                    cupidatat laborum pariatur sit eu eiusmod. Cillum consectetur exercitation
                    ex ipsum. Ullamco commodo anim ut aliqua ex incididunt commodo
                    incididunt reprehenderit. Sit nisi deserunt fugiat eu qui nisi
                    nulla.
                  </p>
                  
                  <ul class="wall-attrs clearfix p-0 m-0 mt-10">
                    <li class="attrs-meta float-left font-size-16">
                      <a href="javascript:void(0)" class="mr-20">
                        <i class="icon md-thumb-up"></i>
                        <span>5</span>
                      </a>
                      <a href="javascript:void(0)" onclick="$('#wall-comment-reply-1').toggle();">
                        <i class="icon md-comment"></i>
                        <span>2</span>
                      </a>
                    </li>
                    <li class="float-right">
                      <div class="btn-group bootstrap-select btn-comment-post">
                        <select data-plugin="selectpicker">
                          <option>Top Comments</option>
                          <option>Most Recent</option>
                        </select>
                      </div>
                    </li>
                  </ul>
                </div>
                <div class="wall-comment-attrs">
                  <div id="wall-comment-reply-1" class="wall-comment-reply clearfix" style="display: none">
                    <div href="#" class="avatar avatar-md float-left">
                      <img src="../../../global/portraits/6.jpg">
                    </div>
                    <div class="ml-60 wall-comment-form">
                      <textarea rows="1" class="autoHeight" placeholder="Write a comment..."></textarea>
                      <div class="wall-comment-reply-attach">
                        <button class="btn btn-icon btn-dark btn-round bg-grey-400">
                          <i class="icon md-attachment-alt rotate" aria-hidden="true"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                  <div class="wall-comment">
                    <div class="wall-comment-action">
                    <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                      <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                    </button>
                    
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                        Edit Comment
                      </a>
                      <a class="dropdown-item text-danger" href="javascript:void(0)" role="menuitem">
                        Delete
                      </a>
                    </div>
                  </div>
                    <a href="#" class="avatar avatar-md float-left">
                      <img src="../../../global/portraits/3.jpg">
                    </a>
                    <div class="ml-60 box-post-comment">
                      <a href="#">Stacey Hunt</a>
                      <span class="ml-10">Do incididunt elit ex incididunt ut. Aute velit proident cupidatat qui et consectetur
                        do. Consequat esse quis ut cupidatat ea. Sint dolore ea culpa
                        dolore velit enim.</span>
                        <p class="font-size-12 mt-5 grey-600">
                          <a href="javascript:void(0)">
                            Like
                          </a>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <a href="javascript:void(0)" class="action-reply">
                            Reply
                          </a>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <span >
                            <i class="icon md-favorite"></i>
                            <span>5</span>
                          </span>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <span>
                            30th July 2017
                          </span>
                        </p>

                      <div class="wall-comment">
                        <a href="#" class="avatar avatar-md float-left">
                          <img src="../../../global/portraits/11.jpg">
                        </a>
                        <div class="ml-60">
                          <a href="#">Crystal Bates</a>
                          <span class="ml-10">Do incididunt elit ex incididunt ut. Aute velit proident cupidatat qui et consectetur
                            do. Consequat esse quis ut cupidatat ea.</span>
                          <p class="font-size-12 mt-5 grey-500">
                            <a href="javascript:void(0)">
                              Like
                            </a>
                            <span class="mr-5 ml-5">&#8226;</span>
                            <a href="javascript:void(0)" class="action-reply">
                              Reply
                            </a>
                            <span class="mr-5 ml-5">&#8226;</span>
                            <span >
                              <i class="icon md-favorite"></i>
                              <span>5</span>
                            </span>
                            <span class="mr-5 ml-5">&#8226;</span>
                            <span>
                              30th July 2017
                            </span>
                          </p>
                        </div>
                        
                      </div>
                      <div class="display-reply"></div>

                    </div>
                    
                  </div>
                  <div class="wall-comment">
                    <a href="#" class="avatar avatar-md float-left">
                      <img src="../../../global/portraits/5.jpg">
                    </a>
                    <div class="ml-60 box-post-comment">
                      <a href="#">Sam Anderson</a>
                      <span class="ml-10">Ut velit ipsum elit ut mollit elit. Proident officia eu ex consectetur ullamco
                        magna. Enim cillum voluptate sint ipsum ad voluptate exercitation.
                        Ex est amet magna occaecat eu.</span>
                      <p class="font-size-12 mt-5 grey-500">
                        <a href="javascript:void(0)">
                          Like
                        </a>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <a href="javascript:void(0)" class="action-reply">
                          Reply
                        </a>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <span >
                          <i class="icon md-favorite"></i>
                          <span>5</span>
                        </span>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <span>
                          30th July 2017
                        </span>
                      </p>

                      <div class="display-reply"></div>
                    </div>
                    
                  </div>
                </div>
              </div>
              <!-- End Post Section-->

              <!-- Start Post Section -->
              <div class="pt-10">Text Auto Post Weekly Report</div>
              <div class="card card-shadow card-bordered post-wall">
                <div class="card-block media clearfix p-25">
                  <div class="media-avatar">
                    <a href="#" class="avatar avatar-lg">
                      <img class="img-fluid" src="../../../global/portraits/6.jpg">
                    </a>
                  </div>
                  <div class="media-body text-middle">
                    <h5 class="mt-0 mb-0">
                      <a href="#">Mallinda Hollaway</a> to <a href="#">JigsawOffice2017</a> 
                      <?php if(rand(0,2)){?>
                      <span class="badge badge-dark">External</span>
                      <?php }?>
                    </h5>
                    <small>30th July 2017</small>
                  </div>

                  <div class="wall-action">
                    <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                      <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                    </button>
                    
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                        Bookmark
                      </a>
                    </div>
                  </div>
                </div>
                <div class="card-block px-25 pt-0 pb-0">
                  <p class="card-text mb-20 text-center text-danger font-size-14">
                    <i class="icon fa-line-chart " style="font-size:50px;" aria-hidden="true"></i><br>
                    Auto Post "<strong>JigsawOffice2017</strong>" Weekly report on <strong>July 30, 2017</strong>.
                  </p>
                  
                  <div class="wall-attach">
                    <div class="row ml-0 mr-0">
                      <div class="col-md-6">
                        <!-- Card -->
                        <div class="card p-20">
                          <h3 class="mt-0">Summary All Task</h3>
                          <div class="pt-20 pb-20">
                            <div class="pie-progress pie-progress" data-plugin="pieProgress" data-barcolor="#4caf50"
                            data-goal="72" aria-valuenow="72" data-size="100" data-barsize="8"
                            role="progressbar">
                              <div class="pie-progress-content">
                                <div class="pie-progress-number">72 %</div>
                                <div class="pie-progress-label">Progress</div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Card -->
                      </div>
                      
                      <div class="col-md-6">
                        <!-- Card -->
                        <div class="card p-20">
                          <h3 class="mt-0">Tasks in Progress</h3>
                          <div class="counter counter-md text-left">
                            <div class="contextual-progress">
                              <div class="clearfix">
                                <div class="progress-title">Order due</div>
                                <div class="progress-label">2 / 5</div>
                              </div>
                              <div class="progress" style="height:15px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                                <div class="progress-bar progress-bar-danger" aria-valuemin="0" aria-valuemax="100" style="width: 40%;" aria-valuenow="60" role="progressbar">
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="counter counter-md text-left">
                            <div class="contextual-progress">
                              <div class="clearfix">
                                <div class="progress-title">This week</div>
                                <div class="progress-label">14 / 25</div>
                              </div>
                              <div class="progress" style="height:15px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                                <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 56%;" aria-valuenow="60" role="progressbar">
                                </div>
                              </div>
                            </div>
                            
                          </div>
                        </div>
                        <!-- End Card -->
                      </div>
                      
                    </div>
                    
                  </div>

                  <ul class="wall-attrs clearfix p-0 m-0 mt-10">
                    <li class="attrs-meta float-left font-size-16">
                      <a href="javascript:void(0)" class="mr-20">
                        <i class="icon md-thumb-up"></i>
                        <span>5</span>
                      </a>
                      <a href="javascript:void(0)" onclick="$('#wall-comment-reply-2').toggle();">
                        <i class="icon md-comment"></i>
                        <span>2</span>
                      </a>
                    </li>
                    <li class="float-right">
                      <div class="btn-group bootstrap-select btn-comment-post">
                        <select data-plugin="selectpicker">
                          <option>Top Comments</option>
                          <option>Most Recent</option>
                        </select>
                      </div>
                    </li>
                  </ul>
                </div>
                <div class="wall-comment-attrs">
                  <div id="wall-comment-reply-2" class="wall-comment-reply clearfix" style="display:none;">
                    <div href="#" class="avatar avatar-md float-left">
                      <img src="../../../global/portraits/6.jpg">
                    </div>
                    <div class="ml-60 wall-comment-form">
                      <textarea rows="1" class="autoHeight" placeholder="Write a comment..."></textarea>
                      <div class="wall-comment-reply-attach">
                        <button class="btn btn-icon btn-dark btn-round bg-grey-400">
                          <i class="icon md-attachment-alt rotate" aria-hidden="true"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                  <div class="wall-comment">
                    <a href="#" class="avatar avatar-md float-left">
                      <img src="../../../global/portraits/5.jpg">
                    </a>
                    <div class="ml-60 box-post-comment">
                      <a href="#">Sam Anderson</a>
                      <span class="ml-10">Ut velit ipsum elit ut mollit elit. Proident officia eu ex.</span>
                      <p class="font-size-12 mt-5 grey-500">
                        <a href="javascript:void(0)">
                          Like
                        </a>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <a href="javascript:void(0)" class="action-reply">
                          Reply
                        </a>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <span >
                          <i class="icon md-favorite"></i>
                          <span>5</span>
                        </span>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <span>
                          30th July 2017
                        </span>
                      </p>

                      <div class="display-reply"></div>
                    </div>
                    
                  </div>
                </div>
              </div>
              <!-- End Post Section-->

              <!-- Start Post Section -->
              <div class="pt-10">Text Post+Photo</div>
              <div class="card card-shadow card-bordered post-wall">
                <div class="card-block media clearfix p-25">
                  <div class="media-avatar">
                    <a href="#" class="avatar avatar-lg">
                      <img class="img-fluid" src="../../../global/portraits/6.jpg">
                    </a>
                  </div>
                  <div class="media-body text-middle">
                    <h5 class="mt-0 mb-0">
                      <a href="#">Mallinda Hollaway</a> to <a href="#">JigsawOffice2017</a> 
                      <?php if(rand(0,2)){?>
                      <span class="badge badge-dark">External</span>
                      <?php }?>
                    </h5>
                    <small>30th July 2017</small>
                  </div>

                  <div class="wall-action">
                    <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                      <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                    </button>
                    
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                        Edit Post
                      </a>
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                        Bookmark
                      </a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item text-danger" href="javascript:void(0)" role="menuitem">
                        Delete
                      </a>
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                        Report Post
                      </a>
                    </div>
                  </div>
                </div>
                <div class="card-block px-25 pt-0 pb-0">
                  <p class="card-text mb-20 grey-800">
                    Ex quis excepteur exercitation incididunt ea amet commodo deserunt nulla. Anim
                    sit duis cupidatat ipsum deserunt reprehenderit fugiat cupidatat.
                  </p>
                  <h5 class="mt-20"><a href="#">4 (1-3) Design master</a></h5>
                  <div class="wall-attach">
                    <div class="row">
                      <div class="col-md-12 col-sm-12 col-12">
                        <img width="100%" src="../../assets/examples/images/login.jpg" alt="...">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-4">
                        <img width="100%" src="../../assets/examples/images/login.jpg" alt="...">
                      </div>
                      <div class="col-md-4 col-sm-4 col-4">
                        <img width="100%" src="../../assets/examples/images/login.jpg" alt="...">
                      </div>
                      <div class="col-md-4 col-sm-4 col-4">
                        <img width="100%" src="../../assets/examples/images/login.jpg" alt="...">
                      </div>
                    </div>
                  </div>

                  <ul class="wall-attrs clearfix p-0 m-0 mt-10">
                    <li class="attrs-meta float-left font-size-16">
                      <a href="javascript:void(0)" class="mr-20">
                        <i class="icon md-thumb-up"></i>
                        <span>5</span>
                      </a>
                      <a href="javascript:void(0)" onclick="$('#wall-comment-reply-2').toggle();">
                        <i class="icon md-comment"></i>
                        <span>2</span>
                      </a>
                    </li>
                    <li class="float-right">
                      <div class="btn-group bootstrap-select btn-comment-post">
                        <select data-plugin="selectpicker">
                          <option>Top Comments</option>
                          <option>Most Recent</option>
                        </select>
                      </div>
                    </li>
                  </ul>
                </div>
                <div class="wall-comment-attrs">
                  <div id="wall-comment-reply-2" class="wall-comment-reply clearfix" style="display:none;">
                    <div href="#" class="avatar avatar-md float-left">
                      <img src="../../../global/portraits/6.jpg">
                    </div>
                    <div class="ml-60 wall-comment-form">
                      <textarea rows="1" class="autoHeight" placeholder="Write a comment..."></textarea>
                      <div class="wall-comment-reply-attach">
                        <button class="btn btn-icon btn-dark btn-round bg-grey-400">
                          <i class="icon md-attachment-alt rotate" aria-hidden="true"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                  <div class="wall-comment">
                    <a href="#" class="avatar avatar-md float-left">
                      <img src="../../../global/portraits/5.jpg">
                    </a>
                    <div class="ml-60 box-post-comment">
                      <a href="#">Sam Anderson</a>
                      <span class="ml-10">Ut velit ipsum elit ut mollit elit. Proident officia eu ex.</span>
                      <p class="font-size-12 mt-5 grey-500">
                        <a href="javascript:void(0)">
                          Like
                        </a>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <a href="javascript:void(0)" class="action-reply">
                          Reply
                        </a>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <span >
                          <i class="icon md-favorite"></i>
                          <span>5</span>
                        </span>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <span>
                          30th July 2017
                        </span>
                      </p>

                      <div class="display-reply"></div>
                    </div>
                    
                  </div>
                </div>
              </div>
              <!-- End Post Section-->

              <!-- Start Post Section -->
              <div class="pt-10">Text Post+Video</div>
              <div class="card card-shadow card-bordered post-wall">
                <div class="card-block media clearfix p-25">
                  <div class="media-avatar">
                    <a href="#" class="avatar avatar-lg">
                      <img class="img-fluid" src="../../../global/portraits/6.jpg">
                    </a>
                  </div>
                  <div class="media-body text-middle">
                    <h5 class="mt-0 mb-0">
                      <a href="#">Mallinda Hollaway</a> to <a href="#">JigsawOffice2017</a> 
                      <?php if(rand(0,2)){?>
                      <span class="badge badge-dark">External</span>
                      <?php }?>
                    </h5>
                    <small>30th July 2017</small>
                  </div>

                  <div class="wall-action">
                    <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                      <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                    </button>
                    
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                        Edit Post
                      </a>
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                        Bookmark
                      </a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item text-danger" href="javascript:void(0)" role="menuitem">
                        Delete
                      </a>
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                        Report Post
                      </a>
                    </div>
                  </div>
                </div>
                <div class="card-block px-25 pt-0 pb-0">
                  <p class="card-text mb-20 grey-800">
                    Ex quis excepteur exercitation incididunt ea amet commodo deserunt nulla. Anim
                    sit duis cupidatat ipsum deserunt reprehenderit fugiat cupidatat.
                  </p>

                  <div class="wall-attach">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="card-header cover player p-0" data-plugin="plyr">
                          <video poster="../../assets/examples/images/poster.jpg" controls crossorigin>
                            <!-- Video Files -->
                            <source type="video/mp4" src="https://cdn.selz.com/plyr/1.0/movie.mp4">
                            <source type="video/webm" src="https://cdn.selz.com/plyr/1.0/movie.webm">
                            <!-- Text Track File -->
                            <track kind="captions" label="English" srclang="en" src="//cdn.selz.com/plyr/1.0/en.vtt"
                            default>
                              <!-- Fallback For Browsers That Don'T Support The <Video> Element -->
                              <a href="https://cdn.selz.com/plyr/1.0/movie.mp4">Download</a>
                          </video>
                        </div>
                      </div>
                    </div>
                    
                  </div>

                  <ul class="wall-attrs clearfix p-0 m-0 mt-10">
                    <li class="attrs-meta float-left font-size-16">
                      <a href="javascript:void(0)" class="mr-20">
                        <i class="icon md-thumb-up"></i>
                        <span>5</span>
                      </a>
                      <a href="javascript:void(0)" onclick="$('#wall-comment-reply-2').toggle();">
                        <i class="icon md-comment"></i>
                        <span>2</span>
                      </a>
                    </li>
                    <li class="float-right">
                      <div class="btn-group bootstrap-select btn-comment-post">
                        <select data-plugin="selectpicker">
                          <option>Top Comments</option>
                          <option>Most Recent</option>
                        </select>
                      </div>
                    </li>
                  </ul>
                </div>
                <div class="wall-comment-attrs">
                  <div id="wall-comment-reply-2" class="wall-comment-reply clearfix" style="display:none;">
                    <div href="#" class="avatar avatar-md float-left">
                      <img src="../../../global/portraits/6.jpg">
                    </div>
                    <div class="ml-60 wall-comment-form">
                      <textarea rows="1" class="autoHeight" placeholder="Write a comment..."></textarea>
                      <div class="wall-comment-reply-attach">
                        <button class="btn btn-icon btn-dark btn-round bg-grey-400">
                          <i class="icon md-attachment-alt rotate" aria-hidden="true"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                  <div class="wall-comment">
                    <a href="#" class="avatar avatar-md float-left">
                      <img src="../../../global/portraits/5.jpg">
                    </a>
                    <div class="ml-60 box-post-comment">
                      <a href="#">Sam Anderson</a>
                      <span class="ml-10">Ut velit ipsum elit ut mollit elit. Proident officia eu ex.</span>
                      <p class="font-size-12 mt-5 grey-500">
                        <a href="javascript:void(0)">
                          Like
                        </a>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <a href="javascript:void(0)" class="action-reply">
                          Reply
                        </a>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <span >
                          <i class="icon md-favorite"></i>
                          <span>5</span>
                        </span>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <span>
                          30th July 2017
                        </span>
                      </p>

                      <div class="display-reply"></div>
                    </div>
                    
                  </div>
                </div>
              </div>
              <!-- End Post Section-->

              <!-- Start Post Section -->
              <div class="pt-10">Text Post+Document</div>
              <div class="card card-shadow card-bordered post-wall">
                <div class="card-block media clearfix p-25">
                  <div class="media-avatar">
                    <a href="#" class="avatar avatar-lg">
                      <img class="img-fluid" src="../../../global/portraits/6.jpg">
                    </a>
                  </div>
                  <div class="media-body text-middle">
                    <h5 class="mt-0 mb-0">
                      <a href="#">Mallinda Hollaway</a> to <a href="#">JigsawOffice2017</a> 
                      <?php if(rand(0,2)){?>
                      <span class="badge badge-dark">External</span>
                      <?php }?>
                    </h5>
                    <small>30th July 2017</small>
                  </div>

                  <div class="wall-action">
                    <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                      <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                    </button>
                    
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                        Edit Post
                      </a>
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                        Bookmark
                      </a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item text-danger" href="javascript:void(0)" role="menuitem">
                        Delete
                      </a>
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                        Report Post
                      </a>
                    </div>
                  </div>
                </div>
                <div class="card-block px-25 pt-0 pb-0">
                  <p class="card-text mb-20 grey-800">
                    Ex quis excepteur exercitation incididunt ea amet commodo deserunt nulla. Anim
                    sit duis cupidatat ipsum deserunt reprehenderit fugiat cupidatat.
                  </p>
                  
                  <div class="wall-attach">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="card card-shadow card-bordered mb-5">
                          <div class="bg-grey-100 pt-10 pb-10 clearfix text-center" style="padding-bottom: 0px;">
                            <a style="color:#f44336;" href="javascript:void(0)">
                              <i class="icon fa-file-pdf-o " style="font-size:34px;" aria-hidden="true"></i>
                            </a>
                            <div class="wall-action" style="top:0;">
                              <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                                <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                              </button>
                              <div class="dropdown-menu dropdown-menu-right" role="menu">
                                <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                                  Download
                                </a>
                                <a class="dropdown-item" href="javascript:void(0)" role="menuitem" data-url="../drive/panel-file.html" data-toggle="slidePanel">
                                  View detail
                                </a>
                              </div>
                            </div>
                          </div>
                          <div class="card-block" style="padding: 10px;">
                            <h5 class="m-0">
                              <a href="javascript:void(0)" style="color:#f44336;">Wireframe inner</a>
                            </h5>
                            <p class="mb-10 grey-500">
                              <small>
                                July 30, 2017 <span class="ml-10 mr-10">&mdash;</span> 4.25 MB</small>
                            </p>
                            <p class="mb-0 grey-500 font-size-12">
                              <a href="#">JigsawOffice2017</a>
                              <i class="icon md-chevron-right" aria-hidden="true"></i>
                              <a href="#">Drive</a>
                              <i class="icon md-chevron-right" aria-hidden="true"></i>
                              <a href="#">Design</a>
                            </p>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="card card-shadow card-bordered mb-5">
                          <div class="bg-grey-100 pt-10 pb-10 clearfix text-center" style="padding-bottom: 0px;">
                            <a style="color:#4caf50;"  href="javascript:void(0)">
                              <i class="icon fa-file-excel-o " style="font-size:34px;" aria-hidden="true"></i>
                            </a>
                            <div class="wall-action" style="top:0;">
                              <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                                <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                              </button>
                              <div class="dropdown-menu dropdown-menu-right" role="menu">
                                <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                                  Download
                                </a>
                                <a class="dropdown-item" href="javascript:void(0)" role="menuitem" data-url="../drive/panel-file.html" data-toggle="slidePanel">
                                  View detail
                                </a>
                              </div>
                            </div>
                          </div>
                          <div class="card-block" style="padding: 10px;">
                            <h5 class="m-0 text-center">
                              <a href="javascript:void(0)" style="color:#4caf50;">TOR Checklist</a>
                            </h5>
                            <p class="mb-10 grey-500">
                              <small>
                                July 30, 2017 <span class="ml-10 mr-10">&mdash;</span> 0.51 MB</small>
                            </p>
                            <p class="mb-0 grey-500 font-size-12">
                              <a href="#">JigsawOffice2017</a>
                              <i class="icon md-chevron-right" aria-hidden="true"></i>
                              <a href="#">Drive</a>
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                  </div>

                  <ul class="wall-attrs clearfix p-0 m-0 mt-10">
                    <li class="attrs-meta float-left font-size-16">
                      <a href="javascript:void(0)" class="mr-20">
                        <i class="icon md-thumb-up"></i>
                        <span>5</span>
                      </a>
                      <a href="javascript:void(0)" onclick="$('#wall-comment-reply-2').toggle();">
                        <i class="icon md-comment"></i>
                        <span>2</span>
                      </a>
                    </li>
                    <li class="float-right">
                      <div class="btn-group bootstrap-select btn-comment-post">
                        <select data-plugin="selectpicker">
                          <option>Top Comments</option>
                          <option>Most Recent</option>
                        </select>
                      </div>
                    </li>
                  </ul>
                </div>
                <div class="wall-comment-attrs">
                  <div id="wall-comment-reply-2" class="wall-comment-reply clearfix" style="display:none;">
                    <div href="#" class="avatar avatar-md float-left">
                      <img src="../../../global/portraits/6.jpg">
                    </div>
                    <div class="ml-60 wall-comment-form">
                      <textarea rows="1" class="autoHeight" placeholder="Write a comment..."></textarea>
                      <div class="wall-comment-reply-attach">
                        <button class="btn btn-icon btn-dark btn-round bg-grey-400">
                          <i class="icon md-attachment-alt rotate" aria-hidden="true"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                  <div class="wall-comment">
                    <a href="#" class="avatar avatar-md float-left">
                      <img src="../../../global/portraits/5.jpg">
                    </a>
                    <div class="ml-60 box-post-comment">
                      <a href="#">Sam Anderson</a>
                      <span class="ml-10">Ut velit ipsum elit ut mollit elit. Proident officia eu ex.</span>
                      <p class="font-size-12 mt-5 grey-500">
                        <a href="javascript:void(0)">
                          Like
                        </a>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <a href="javascript:void(0)" class="action-reply">
                          Reply
                        </a>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <span >
                          <i class="icon md-favorite"></i>
                          <span>5</span>
                        </span>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <span>
                          30th July 2017
                        </span>
                      </p>

                      <div class="display-reply"></div>
                    </div>
                    
                  </div>
                </div>
              </div>
              <!-- End Post Section-->

              
              


              <a class="btn btn-block btn-default" href="javascript:void(0)" role="button">Show more</a>
            </div>
            
          </div>

          <div class="col-lg-6 col-xl-3 pull-xl-6">
            <div class="panel mb-30" style="border:1px #e0e0e0 solid;">
              <div class="panel-heading p-15 bg-grey-500 clearfix">
                <div class="font-size-18 white"><strong>Jigsaw Office Co-Working</strong></div>

                <div style="position:absolute;right:0px;top:7px;">
                  <button type="button" class="btn btn-pure white font-size-18" data-toggle="dropdown" aria-hidden="true">
                    <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                  </button>
                  
                  <div class="dropdown-menu dropdown-menu-right" role="menu">
                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                      Edit
                    </a>
                    <a class="dropdown-item text-danger" href="javascript:void(0)" role="menuitem">
                      Delete
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item text-success" href="javascript:void(0)" role="menuitem">
                      Mark as Complete
                    </a>
                    <a class="dropdown-item text-default" href="javascript:void(0)" role="menuitem">
                      Mark as Hold
                    </a>
                  </div>
                </div>
              </div>
              <div class="panel-body pt-20">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce velit
                  tortor, dictum in gravida nec, aliquet non lorem. pellentesque.ipiscing
                  elit. Fusce velit tortor.</p>
                <section>
                    <ul class="list-group list-group-full">
                      <li class="list-group-item">
                        <span style="display:inline-block;" class="w-100 grey-500">Start</span>
                        <span>15/03/2017</span>
                      </li>
                      <li class="list-group-item">
                        <span style="display:inline-block;" class="w-100 grey-500">Finish</span>
                        <span>4/07/2017</span>
                      </li>
                      <li class="list-group-item clearfix">
                         <span style="display:inline-block;" class="w-100 grey-500">Priority</span>
                        <span>High  Priority</span>
                      </li>
                      <li class="list-group-item clearfix">
                         <span style="display:inline-block;" class="w-100 grey-500">Stutus</span>
                        <span>Progress</span>
                      </li>
                      <li class="list-group-item">
                        <span style="display:inline-block;" class="w-100 grey-500">Created</span>
                        <span>May 5 at 11:00 AM</span>
                      </li>
                    </ul>
                  </section>
                <section>
                   <a href="#">
                  <span class="badge p-5 mb-5 badge-outline" style="border-color:#F2543B; color:#F2543B;">Designer</span>
                </a>
                                    <a href="#">
                  <span class="badge p-5 mb-5 badge-outline" style="border-color:#E8AD73; color:#E8AD73;">jQuery</span>
                </a>
                                    <a href="#">
                  <span class="badge p-5 mb-5 badge-outline" style="border-color:#C6EF3D; color:#C6EF3D;">Javascript</span>
                </a>
                                    <a href="#">
                  <span class="badge p-5 mb-5 badge-outline" style="border-color:#CF9650; color:#CF9650;">HTML5</span>
                </a>
              </section>
              </div>
                
            </div>

            <div class="panel mb-30">
              <div class="panel-heading">
                <h3 class="panel-title ">Member</h3>
              </div>
              <div class="panel-body">
                  <h5 class="">Teams</h5>
                  <ul class="list-unstyled list-inline">
                    <?php for($a=1;$a<=4;$a++){?>
                    <li class="list-inline-item m-5">
                      <div style="position:relative;">
                        <a class="avatar avatar-sm" href="javascript:void(0)" data-toggle="dropdown">
                          <img src="../../global/portraits/<?=rand(1,20)?>.jpg" alt="">
                        </a>
                        <div class="dropdown-menu" role="menu">
                          <div class="dropdown-header" role="presentation">Caleb Richards</div>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                            Message
                          </a>
                          <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                            View Profile
                          </a>
                        </div>
                      </div>
                    </li>
                    <?php }?>
                  </ul>
                  <h5 class="">Report to..</h5>
                  <ul class="list-unstyled list-inline">
                    <li class="list-inline-item m-5">
                      <div style="position:relative;">
                        <a class="avatar avatar-sm" href="javascript:void(0)" data-toggle="dropdown">
                          <img src="../../global/portraits/<?=rand(1,20)?>.jpg" alt="">
                        </a>
                        <div class="dropdown-menu" role="menu">
                          <div class="dropdown-header" role="presentation">Caleb Richards</div>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                            Message
                          </a>
                          <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                            View Profile
                          </a>
                        </div>
                      </div>
                    </li>
                  </ul>
              </div>
            </div>
            
          </div>

          <div class="col-lg-6 col-xl-3 ">
          
            <div class="card card-block bg-white" style="border:1px #e0e0e0 solid;">
              <h3 class="mt-0">Summary All Task</h3>
                <div class="pt-20 pb-20">
                  <div class="pie-progress pie-progress" data-plugin="pieProgress" data-barcolor="#4caf50"
                  data-goal="72" aria-valuenow="72" data-size="100" data-barsize="8"
                  role="progressbar">
                    <div class="pie-progress-content">
                      <div class="pie-progress-number">72 %</div>
                      <div class="pie-progress-label">Progress</div>
                    </div>
                  </div>
                </div>
            </div>
            
            <div class="card card-block bg-white" style="border:1px #e0e0e0 solid;">
              <h3 class="mt-0">Tasks in Progress</h3>
              <div class="counter counter-md text-left">
                <div class="contextual-progress mb-0 mt-10">
                  <div class="clearfix">
                    <div class="progress-title">Order due</div>
                    <div class="progress-label">2 / 5</div>
                  </div>
                  <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                    <div class="progress-bar progress-bar-danger" aria-valuemin="0" aria-valuemax="100" style="width: 40%;" aria-valuenow="60" role="progressbar">
                    </div>
                  </div>
                </div>
                
              </div>
              <div class="counter counter-md text-left">
                <div class="contextual-progress mb-0 mt-10">
                  <div class="clearfix">
                    <div class="progress-title">Today</div>
                    <div class="progress-label">5 / 8</div>
                  </div>
                  <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                    <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 60%;" aria-valuenow="60" role="progressbar">
                    </div>
                  </div>
                </div>
                
              </div>
              <div class="counter counter-md text-left">
                <div class="contextual-progress mb-0 mt-10">
                  <div class="clearfix">
                    <div class="progress-title">This week</div>
                    <div class="progress-label">14 / 41</div>
                  </div>
                  <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                    <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 34%;" aria-valuenow="60" role="progressbar">
                    </div>
                  </div>
                </div>
                
              </div>
              <div class="counter counter-md text-left">
                <div class="contextual-progress mb-0 mt-10">
                  <div class="clearfix">
                    <div class="progress-title">This month</div>
                    <div class="progress-label">45 / 122</div>
                  </div>
                  <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                    <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 36%;" aria-valuenow="60" role="progressbar">
                    </div>
                  </div>
                </div>
                
              </div>
            
            </div>


            <div class="card card-block bg-white" style="border:1px #e0e0e0 solid;">
              <h3 class="mt-0">Tasks Priority</h3>
              <div class="contextual-progress mb-0 mt-10">
                <div class="clearfix">
                  <div class="progress-title">Highest</div>
                  <div class="progress-label">60%</div>
                </div>
                <div class="progress" style="height:5px;" data-labeltype="percentage" data-goal="-40" data-plugin="progress">
                  <div class="progress-bar progress-bar-danger" aria-valuemin="-100" aria-valuemax="0" aria-valuenow="-40" role="progressbar" style="width: 60%;"></div>
                </div>
              </div>
              <div class="contextual-progress mb-0 mt-10">
                <div class="clearfix">
                  <div class="progress-title">High</div>
                  <div class="progress-label">70%</div>
                </div>
                <div class="progress" style="height:5px;" data-labeltype="percentage" data-goal="-40" data-plugin="progress">
                  <div class="progress-bar progress-bar-warning" aria-valuemin="-100" aria-valuemax="0" aria-valuenow="-40" role="progressbar" style="width: 70%;"></div>
                </div>
              </div>
              <div class="contextual-progress mb-0 mt-10">
                <div class="clearfix">
                  <div class="progress-title">Normal</div>
                  <div class="progress-label">80%</div>
                </div>
                <div class="progress" style="height:5px;" data-labeltype="percentage" data-goal="-40" data-plugin="progress">
                  <div class="progress-bar progress-bar-success" aria-valuemin="-100" aria-valuemax="0" aria-valuenow="-40" role="progressbar" style="width: 80%;"></div>
                </div>
              </div>
              <div class="contextual-progress mb-0 mt-10">
                <div class="clearfix">
                  <div class="progress-title">Low</div>
                  <div class="progress-label">50%</div>
                </div>
                <div class="progress" style="height:5px;" data-labeltype="percentage" data-goal="-40" data-plugin="progress">
                  <div class="progress-bar" aria-valuemin="-100" aria-valuemax="0" aria-valuenow="-40" role="progressbar" style="width: 50%;background-color:#757575;"></div>
                </div>
              </div>
            
            </div>
            
            
          </div>
          
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/slick-carousel/slick.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <script src="../../global/vendor/aspieprogress/jquery-asPieProgress.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/masonry.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../assets/examples/js/pages/profile-v2.js"></script>
  <script src="../../global/js/Plugin/aspieprogress.js"></script>
  <script src="../../assets/examples/js/charts/pie-progress.js"></script>
  <script src="../../global/js/Plugin/responsive-tabs.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();


      function chooseDateRange(start, end) {
          var DrangeValue = start.format('MM/DD/YYYY') + '-' + end.format('MM/DD/YYYY');
          $('#SearchDateRange span#daterange-value').html(DrangeValue);
          var dataKey = "123";
          var dataValue = "xxx";
          var stoken = '';
            $('.search-wrapper .currently-showing .chooseDateRange').remove();
            stoken = '<div class="token chooseDateRange" data-key="'+dataKey+'" data-value="' + dataValue + '"><span class="token-label" >' + DrangeValue + '</span><a  href="#" class="close" tabindex="-1">×</a></div>';
            $('.search-wrapper .currently-showing').append(stoken);
            sysGenUrlSearch();
      }

      $('#SearchDateRange').daterangepicker({
          "autoApply": true,
          "opens": "center",
          ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
      }, chooseDateRange);

    });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>