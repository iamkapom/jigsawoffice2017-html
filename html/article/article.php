<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../../assets/examples/css/apps/documents.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition app-documents page-aside-left">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header">
      <h1 class="page-title mb-10">Getting Started
        <small>( 8 Articles )</small>
      </h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item active">Pages</li>
        <li class="breadcrumb-item active">Article</li>
        <li class="breadcrumb-item"><a href="categories.php">Categories</a></li>
        <li class="breadcrumb-item"><a href="articles.php">Getting started</a></li>
        <li class="breadcrumb-item active">Article</li>
      </ol>
    </div>
    <div class="page-content bg-white" style="position: relative;">
      <div class="page-aside">
        <!-- Contacts Sidebar -->
        <div class="page-aside-switch">
          <i class="icon md-chevron-left" aria-hidden="true"></i>
          <i class="icon md-chevron-right" aria-hidden="true"></i>
        </div>
        <div class="page-aside-inner page-aside-scroll">
          <div data-role="container">
            <div data-role="content">
              <section class="page-aside-section">
                <h5 class="page-aside-title">Categories</h5>
                <div class="list-group">
                  <a class="list-group-item active" href="articles.php">
                    <span class="list-group-item-content">Getting started</span>
                  </a>
                  <a class="list-group-item" href="articles.php">
                    <span class="list-group-item-content">Divitias modis</span>
                  </a>
                  <a class="list-group-item" href="articles.php">
                    <span class="list-group-item-content">Developer Tourials</span>
                  </a>
                  <a class="list-group-item" href="articles.php">
                    <span class="list-group-item-content">Compatible Themes</span>
                  </a>
                  <a class="list-group-item" href="articles.php">
                    <span class="list-group-item-content">Commodis clamat</span>
                  </a>
                  <a class="list-group-item" href="articles.php">
                    <span class="list-group-item-content">Numeris imperio</span>
                  </a>
                  <a class="list-group-item" href="articles.php">
                    <span class="list-group-item-content">Generis acutum</span>
                  </a>
                  <a class="list-group-item" href="articles.php">
                    <span class="list-group-item-content">Distinctio arbitrium</span>
                  </a>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      
      <div class="page-main">
        <div class="page-header">
          <form class="mt-20" action="#" role="search">
            <div class="input-search input-search-dark">
              <input type="text" class="form-control w-full" placeholder="Search the knowledge base..." name="">
              <button type="submit" class="input-search-btn">
                <i class="icon md-search" aria-hidden="true"></i>
              </button>
            </div>
          </form>
        </div>
        <div style="position:relative;">
          <section>
            <h4 id="section-1">Article section one.</h4>
            <p>Perspecta historiae studiis consuevit deterius scribentur expleantur
              illam invenire anteponant, cuiquam probo accusantium segnitiae hostis.
              Quos reprehenderit videntur solam amplificarique temeritate incorruptis
              plena filio, metu, intellegitur videor amotio discordant tenebo officiis
              quaerendum parentes scaevola dolorum, vivendum dialectica neglegentur
              liberemus verterunt esset inhaererent, iam mala huc fortitudinis
              derigatur tollitur ipsum legum.</p>
            <p>Loca invitat maior ferre amicitiae, neque profecta fictae ferae afflueret
              fugienda difficilem praesentibus, nacti quoniam exultat memoria,
              tranquillat iudicari expetendam, dulce efficiat caret vivere cn concessum
              transferrem odio magna, nominavi nocet allicit depravate inciderint
              certamen retinere summumque nemini, iusto conciliant aliqua respirare
              finitum. Quem conformavit, pueriliter quaerenda clamat fugiendum
              specie.</p>
          </section>
          <section>
            <h4 id="section-2">Article section two.</h4>
            <p>Deduceret, stabilitas diligenter stultorum diesque dolemus consoletur
              aristippus specie exeamus, liberiusque perdiscere iucunda intellegerem
              quosdam movet, maximo constituto saepti, mutat seditione indicaverunt
              quantum animis dedocendi simulent placatae vivendo. Brute existimant
              adhibuit effugiendorum artem potiendi sabinum dicat sequitur parabilis,
              asperner alliciat mutat tale labefactetur corpora, albuci amori velim
              summam didicisset dubium.</p>
          </section>
          <section>
            <h4 id="section-3">Article section three.</h4>
            <p>Cum personae causas hae diligamus opibus morbi formidinum voluntates.
              Quicquam alteram splendide romano discordiae cui. Relinqueret vera
              fama vocent hortatore constituant aperiri compositis, mediocrem huius
              utilitate legum, eas intellegunt deditum sollicitudines omnino penitus
              tantam cumanum privavisse, situm intellegunt iis litteris moribus
              legendi multos privamur, fugiamus existimo privamur domesticarum,
              indoctis corporum.</p>
            <ol>
              <li>
                <h5 id="section-3-1">Article section three-one.</h5>
                <p>Ineruditus defendere docti scribimus voluptatis, suis constituant
                  possent, desiderent litteris allicit acuti iuvaret sensus praetermissum
                  incidunt effecerit, iucunditas detraxisse dulce legemus regione
                  dum studium aliquod, tollatur, explicatis sic exorsus torquato
                  finitum aperta feci, philosophiae nobis defatigatio non tribuat
                  iudicari aliquid nati servare, virtutem docere idque magnis aliquid
                  soliditatem nos, lucilius.</p>
                <p>Ineruditus defendere docti scribimus voluptatis, suis constituant
                  possent, desiderent litteris allicit acuti iuvaret sensus praetermissum
                  incidunt effecerit, iucunditas detraxisse dulce legemus regione
                  dum studium aliquod, tollatur, explicatis sic exorsus torquato
                  finitum aperta feci, philosophiae nobis defatigatio non tribuat
                  iudicari aliquid nati servare, virtutem docere idque magnis aliquid
                  soliditatem nos, lucilius.</p>
              </li>
              <li>
                <h5 id="section-3-2">Article section three-two.</h5>
                <p>Ostendis acutum totam declinabunt verterunt ullo. Tria fatendum
                  facultas res concursio necessitatibus futuros gessisse recordamur
                  intercapedo, vix doceat una quaeso tantopere, tranquillae libero
                  contentus probo opera ab utilitatibus pulcherrimum scripserit.
                  Individua mediocriterne studiose posse corrumpit status locatus
                  multoque fortibus essent, statuat irridente omittantur pugnare.
                  Ferantur numquidnam delectamur praesentium circumcisaque horribiles.</p>
                <p>Ostendis acutum totam declinabunt verterunt ullo. Tria fatendum
                  facultas res concursio necessitatibus futuros gessisse recordamur
                  intercapedo, vix doceat una quaeso tantopere, tranquillae libero
                  contentus probo opera ab utilitatibus pulcherrimum scripserit.
                  Individua mediocriterne studiose posse corrumpit status locatus
                  multoque fortibus essent, statuat irridente omittantur pugnare.
                  Ferantur numquidnam delectamur praesentium circumcisaque horribiles.</p>
              </li>
              <li>
                <h5 id="section-3-3">Article section three-three.</h5>
                <p>Ostendis acutum totam declinabunt verterunt ullo. Tria fatendum
                  facultas res concursio necessitatibus futuros gessisse recordamur
                  intercapedo, vix doceat una quaeso tantopere, tranquillae libero
                  contentus probo opera ab utilitatibus pulcherrimum scripserit.
                  Individua mediocriterne studiose posse corrumpit status locatus
                  multoque fortibus essent, statuat irridente omittantur pugnare.
                  Ferantur numquidnam delectamur praesentium circumcisaque horribiles.</p>
                <p>Ostendis acutum totam declinabunt verterunt ullo. Tria fatendum
                  facultas res concursio necessitatibus futuros gessisse recordamur
                  intercapedo, vix doceat una quaeso tantopere, tranquillae libero
                  contentus probo opera ab utilitatibus pulcherrimum scripserit.
                  Individua mediocriterne studiose posse corrumpit status locatus
                  multoque fortibus essent, statuat irridente omittantur pugnare.
                  Ferantur numquidnam delectamur praesentium circumcisaque horribiles.</p>
              </li>
              <li>
                <h5 id="section-3-4">Article section three-four.</h5>
                <p>Ostendis acutum totam declinabunt verterunt ullo. Tria fatendum
                  facultas res concursio necessitatibus futuros gessisse recordamur
                  intercapedo, vix doceat una quaeso tantopere, tranquillae libero
                  contentus probo opera ab utilitatibus pulcherrimum scripserit.
                  Individua mediocriterne studiose posse corrumpit status locatus
                  multoque fortibus essent, statuat irridente omittantur pugnare.
                  Ferantur numquidnam delectamur praesentium circumcisaque horribiles.</p>
                <p>Ostendis acutum totam declinabunt verterunt ullo. Tria fatendum
                  facultas res concursio necessitatibus futuros gessisse recordamur
                  intercapedo, vix doceat una quaeso tantopere, tranquillae libero
                  contentus probo opera ab utilitatibus pulcherrimum scripserit.
                  Individua mediocriterne studiose posse corrumpit status locatus
                  multoque fortibus essent, statuat irridente omittantur pugnare.
                  Ferantur numquidnam delectamur praesentium circumcisaque horribiles.</p>
              </li>
            </ol>
          </section>
          <section>
            <h4 id="section-4">Article section four.</h4>
            <p>Suas habent abducat angore exercitumque. Medium successerit amplificarique
              congressus delectamur claudicare fruuntur poena. Factis arbitrantur,
              ferae solitudo familiari, sale animadversionis utilior quanti, carum
              magnos. Veritus perturbari assecutus solvantur. Tua statuerunt dediti
              fabulas suscipit dirigentur quadam utrumque meminerimus fuit. Numquid
              molestum deditum aequitatem consulatu mutans firme tolerabiles quantus
              molestiam, ignorant concursionibus.</p>
          </section>
          <div class="article-footer mt-40">
            <div class="article-footer-actions">
              <button type="button" class="btn btn-primary">Yes</button>
              <button type="button" class="btn btn-primary">No</button>
            </div>
            Was this article helpful to you ?
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../../../global/vendor/jquery/jquery.js"></script>
  <script src="../../../../global/vendor/tether/tether.js"></script>
  <script src="../../../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../../../global/vendor/animsition/animsition.js"></script>
  <script src="../../../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../../../global/vendor/intro-js/intro.js"></script>
  <script src="../../../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <!-- Scripts -->
  <script src="../../../../global/js/State.js"></script>
  <script src="../../../../global/js/Component.js"></script>
  <script src="../../../../global/js/Plugin.js"></script>
  <script src="../../../../global/js/Base.js"></script>
  <script src="../../../../global/js/Config.js"></script>
  <script src="../../../assets/js/Section/Menubar.js"></script>
  <script src="../../../assets/js/Section/Sidebar.js"></script>
  <script src="../../../assets/js/Section/PageAside.js"></script>
  <script src="../../../assets/js/Plugin/menu.js"></script>
  <script src="../../../../global/js/config/colors.js"></script>
  <script src="../../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../../assets');
  </script>
  <script src="../../../assets/js/Site.js"></script>
  <script src="../../../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../../../global/js/Plugin/switchery.js"></script>
  <script src="../../../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../../../global/js/Plugin/matchheight.js"></script>
  <script src="../../../assets/js/Site.js"></script>
  <script src="../../../assets/js/App/Documents.js"></script>
  <script src="../../../assets/examples/js/apps/documents.js"></script>
<?php include("../_footer-form.php");?>
</body>
</html>