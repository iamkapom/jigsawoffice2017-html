<section>
  <div class="row p-20 bg-grey-600">
    <div class="col-md-10" style="padding-top: 3px;">
      <h5 class="p-0 m-0 white"><i class="icon md-star" aria-hidden="true"></i> BOOKMARK</h5>
    </div>
    <div class="col-md-2 text-right ">
      <a style="display:block;" href="javascript:void(0)" class="white" role="button">
        <i class="icon md-settings" aria-hidden="true"></i>
      </a>
    </div>
  </div>
  <div class="input-search input-search-dark m-10 ml-10">
    <i class="input-search-icon md-search" aria-hidden="true" style="left:0;"></i>
    <input class="form-control" name="" placeholder="Search Bookmark" type="text" style="border-radius:0.215rem;">
    <button type="button" class="input-search-close icon md-close" aria-label="Close" style="right:0;"></button>
  </div>
</section>
<div class="site-sidebar-tab-content tab-content">
  <div class="tab-pane active" >
    <div>
      <div>
        <div class="list-group list-group-dividered pr-10">
          <?php 

          $priority_color = array("#f3273c","#ff9800","#4caf50","#757575");
          $pri = array("danger","warning","success","dark");
          for($aa=1;$aa<=4;$aa++){
            $progress = rand(20,80);
            $rand_pri = rand(0,3);
          ?>
            <a class="list-group-item pr-10 pl-15" href="javascript:void(0)">
              <div class="media">
                <div class="media-body">
                  <h6 class="media-heading">Project Jigsaw Office 2017</h6>
                  <small><strong>URL :</strong> /workspace/24159250<?=rand(111111,999999);?>/</small>
                </div>
              </div>
            </a>
          <?php }?>

            <a class="list-group-item pr-10 pl-15" href="javascript:void(0)">
              <div class="media">
                <div class="media-body">
                  <h6 class="media-heading">My Task</h6>
                  <small><strong>URL :</strong> /tasks/tasks-list.php</small>
                </div>
              </div>
            </a>
            <a class="list-group-item pr-10 pl-15" href="javascript:void(0)">
              <div class="media">
                <div class="media-body">
                  <h6 class="media-heading">My Calendar</h6>
                  <small><strong>URL :</strong> /calendar/calendar.php</small>
                </div>
              </div>
            </a>
            <a class="list-group-item pr-10 pl-15" href="javascript:void(0)">
              <div class="media">
                <div class="media-body">
                  <h6 class="media-heading">CRM</h6>
                  <small><strong>URL :</strong> /apps-crm/index.php</small>
                </div>
              </div>
            </a>
            <a class="list-group-item pr-10 pl-15" href="javascript:void(0)">
              <div class="media">
                <div class="media-body">
                  <h6 class="media-heading">News</h6>
                  <small><strong>URL :</strong> /apps-intranet/news.php</small>
                </div>
              </div>
            </a>
            <a class="list-group-item pr-10 pl-15" href="javascript:void(0)">
              <div class="media">
                <div class="media-body">
                  <h6 class="media-heading">Photos</h6>
                  <small><strong>URL :</strong> /apps-intranet/photos.php</small>
                </div>
              </div>
            </a>

        </div>
      </div>
    </div>
  </div>
</div>