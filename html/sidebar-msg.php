<section>
  <div class="row p-20 bg-grey-600">
    <div class="col-md-12" style="padding-top: 3px;">
      <h5 class="p-0 m-0 white"><i class="icon md-comments" aria-hidden="true"></i> MESSENGER</h5>
    </div>    
  </div>
</section>
<div class="site-sidebar-tab-content tab-content">
  <div class="tab-pane fade active show" id="sidebar-chat">
    <div>
      <div>
        <div class="list-group list-group-dividered pr-10">
          <?php 
          $b=1;
          for($a=1;$a<=15;$a++){
            $online = rand(0,1);
            $tmpC = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
          ?>
          <a  href="javascript:void(0)" class="list-group-item p-5 pr-10 pl-10">
            <div class="media" style="flex-direction:initial;">
              <div class="pr-10">
                <div class="avatar avatar-online">
                  <img class="img-fluid" src="../../global/portraits/<?=$b++?>.jpg" alt="...">
                  <?php if($online){?>
                  <i></i>
                  <?php }?>
                </div>
              </div>
              <div class="media-body">
                <?php if($a<4){?>
                <div class="mt-0 black"><strong>Edward Fletcher</strong></div>
                <small class="black"><strong>Consectetuorem ipsum dolor sit?</strong></small>
                <?php }else{?>
                <div class="mt-0 grey-700">Caleb Richards</div>
                <small class="grey-400">Consectetuorem ipsum dolor sit?</small>
                <?php }?>
              </div>
              <div class="font-size-10 <?php echo ($a<4?"blue-600":"grey-500");?>" style="position:absolute;right:10px;top:10px;">
                1:44pm
              </div>
            </div>
          </a>
          <?php if(rand(0,1)){?>
          <a href="javascript:void(0)" class="list-group-item p-5 pr-10 pl-10">
            <div class="media" style="flex-direction:initial;">
              <div class="pr-10">
                <button type="button" style="background:<?php echo $tmpC;?>;" class="btn btn-icon btn-round waves-effect waves-classic">
                <i class="icon fa-group mr-0 white" aria-hidden="true"></i>
                </button>
              </div>
              <div class="media-body">
                <div class="mt-0 black"><strong>Jigsaw Office R&D</strong></div>
                <ul class="addMember-items">
                  <?php for($aa=1;$aa<=rand(3,7);$aa++){?>
                  <li class="addMember-item mr-0">
                    <img style="width:20px;" class="avatar" src="../../global/portraits/<?php echo rand(1,20);?>.jpg" title="Herman Beck">
                  </li>
                  <?php }?>
                </ul>
                <div class="addMember-trigger pt-1 font-size-10">
                  <strong>+<?=rand(2,10)?></strong>
                </div>
              </div>
              <div class="font-size-10 <?php echo ($a<4?"blue-600":"grey-500");?>" style="position:absolute;right:10px;top:10px;">
                1:44pm
              </div>
            </div>
          </a>
          <?php }?>
          <?php 
          }
          ?>
        </div>
      </div>
    </div>
  </div>

  <section>
    <div class="row p-20 bg-grey-200">
      <div class="col-md-12">
        <a href="javascript:void(0)" role="menuitem">
          All Messenger
        </a>
      </div>
    </div>
  </section>
</div>