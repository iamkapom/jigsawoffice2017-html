<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../global/vendor/slick-carousel/slick.css">
  <link rel="stylesheet" href="../../global/vendor/plyr/plyr.css">
  <link rel="stylesheet" href="../../global/vendor/aspieprogress/asPieProgress.css">
  <link rel="stylesheet" href="../../assets/examples/css/charts/pie-progress.css">
  
  <link rel="stylesheet" href="../../global/vendor/bootstrap-daterangepicker/daterangepicker.css" />
  <!-- Fonts -->
  <link rel="stylesheet" href="../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition page-profile-v2">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <!-- Page -->
  <div class="page">
    <div class="page-header">
      <h1 class="page-title mb-10">Search</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../home/index.php">Home</a></li>
        <li class="breadcrumb-item active">Search</li>
      </ol>
      
    </div>
    <div class="page-content container-fluid">
      <div class="row ml-0 mr-0">
        <!-- Middle Column -->
        <div class="col-md-12 col-lg-8">
          
          
          <section>
            <div class="pb-10">
              <div class="search-wrapper">
                <div class="search-box">
                  <div class="icon md-search"></div>
                  <div class="currently-showing">
                    <?=$search['tokenhtml']?>
                  </div>
                </div>
                <a href="javascript:void(0);" class="clear_all btn btn-flat btn-default btn-block waves-effect waves-classic">Clear All</a>
                <div class="data_entry">
                  <input class="input keyword-input" placeholder="Enter a keyword" type="text">
                  <div class="icon md-close-circle close"></div>
                </div>
                <div class="filters">
                    <div class="dropdown filter_permission">
                      <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Module
                        <span class="icon md-chevron-down" aria-hidden="true"></span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)">Workspace</a>
                        <a class="dropdown-item" href="javascript:void(0)">Task</a>
                        <a class="dropdown-item" href="javascript:void(0)">Policy</a>
                        <a class="dropdown-item" href="javascript:void(0)">Staff Directory</a>
                        <a class="dropdown-item" href="javascript:void(0)">News</a>
                        <a class="dropdown-item" href="javascript:void(0)">Announcement</a>
                        <a class="dropdown-item" href="javascript:void(0)">Photo</a>
                        <a class="dropdown-item" href="javascript:void(0)">Video</a>
                        <a class="dropdown-item" href="javascript:void(0)">Drive Center</a>
                        <a class="dropdown-item" href="javascript:void(0)">Calendar</a>
                        <a class="dropdown-item" href="javascript:void(0)">Links</a>
                        <a class="dropdown-item" href="javascript:void(0)">Vote/Poll</a>
                        <a class="dropdown-item" href="javascript:void(0)">Banner</a>
                        <a class="dropdown-item" href="javascript:void(0)">Knowledgebase</a>
                      </div>
                    </div>
                    <div id="time_select" class="dropdown filter_permission">
                      <button type="button" id="SearchDateRange" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Time <i class="icon md-chevron-down" aria-hidden="true"></i>
                        <span id="daterange-value"></span>
                      </button>
                    </div>
                    
                </div>
              </div>
            </div>
            <div class="pb-20">
                <div class="actions-inner float-right">
                  <div class="dropdown">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      <i class="icon fa-sort-amount-desc" aria-hidden="true"></i> New Post
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-media w-100" role="menu">
                      <h6 class="pl-10" aria-hidden="false">Sory by</h6>
                      <a class="dropdown-item active" href="javascript:void(0)">New Post</a>
                      <a class="dropdown-item" href="javascript:void(0)">Recent Activity</a>
                      <a class="dropdown-item" href="javascript:void(0)">Top Comments</a>
                      <a class="dropdown-item" href="javascript:void(0)">Top Likes</a>
                      <a class="dropdown-item" href="javascript:void(0)">Attach File</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-sort-amount-asc" aria-hidden="true"></i> Ascending</a>
                      <a class="dropdown-item active" href="javascript:void(0)"><i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Descending</a>
                    </div>
                  </div>
                </div>
                

                <div class="pt-10">About <strong>26</strong> results</div>
              </div>
            </section>

          <div class="">
            <!-- Start Post Section -->
            <div class="card card-shadow card-bordered post-wall">
              <div class="card-block clearfix p-25">
                <div class="media-avatar">
                  <a href="#" class="avatar avatar-lg">
                    <img class="img-fluid" src="../../../global/portraits/16.jpg">
                  </a>
                </div>
                <div class="media-body text-middle">
                  <h5 class="mt-0 mb-0">
                    <a href="#">Mallinda Hollaway</a> to <a href="#">JigsawOffice2017</a> 
                    <?php if(rand(0,2)){?>
                    <span class="badge badge-dark">External</span>
                    <?php }?>
                  </h5>
                  <small>30th July 2017</small>
                  <p class="card-text mb-20 grey-800">
                    Ex quis excepteur exercitation incididunt ea amet commodo deserunt nulla. Anim
                    sit duis cupidatat ipsum deserunt reprehenderit fugiat cupidatat.
                    In do eu sint Lorem qui eu eu. Id ad non pariatur culpa.
                  </p>
                </div>
              </div>
            </div>
            <!-- End Post Section-->

            <!-- Start Post Section -->
            <div class="card card-shadow card-bordered post-wall">
              <div class="card-block clearfix p-25">
                <div class="media-avatar">
                  <a href="#" class="avatar avatar-lg">
                    <img class="img-fluid" src="../../../global/portraits/16.jpg">
                  </a>
                </div>
                <div class="media-body text-middle">
                  <h5 class="mt-0 mb-0">
                    <a href="#">Mallinda Hollaway</a> to <a href="#">JigsawOffice2017</a> 
                    <?php if(rand(0,2)){?>
                    <span class="badge badge-dark">External</span>
                    <?php }?>
                  </h5>
                  <small>30th July 2017</small>
                  <p class="card-text mb-20 grey-800">
                    <img width="150" src="../../assets/examples/images/login.jpg" align="left" class="mr-10 mb-10" alt="..."> Ex quis excepteur exercitation incididunt ea amet commodo deserunt nulla. Anim
                    sit duis cupidatat ipsum deserunt reprehenderit fugiat cupidatat.
                    In do eu sint Lorem qui eu eu. Id ad non pariatur culpa.
                  </p>
                </div>
              </div>
            </div>
            <!-- End Post Section-->

            <!-- Start Post Section -->
            <div class="card card-shadow card-bordered post-wall">
              <div class="card-block clearfix p-25">
                <div class="media-avatar">
                  <a href="#" class="avatar avatar-lg">
                    <img class="img-fluid" src="../../../global/portraits/16.jpg">
                  </a>
                </div>
                <div class="media-body text-middle">
                  <h5 class="mt-0 mb-0">
                    <a href="#">Mallinda Hollaway</a> to <a href="#">JigsawOffice2017</a> 
                    <?php if(rand(0,2)){?>
                    <span class="badge badge-dark">External</span>
                    <?php }?>
                  </h5>
                  <small>30th July 2017</small>
                  <p class="card-text mb-20 grey-800">
                    <div class="w-150 mr-10 mb-10 float-left" style="position: relative;">
                      <i class="icon md-play-circle-outline white font-size-50" style="position: absolute; left: 35%; top: 30%; z-index: 2;" aria-hidden="true"></i>
                      <button type="button" class="btn btn-floating btn-sm" style="position: absolute; left: 37%; top: 37%;z-index: 1; background: #3f51b5;width: 2.5rem;height: 2.5rem;"> </button>
                      <img width="100%" src="../../assets/examples/images/poster.jpg" alt="...">
                    </div>
                    Ex quis excepteur exercitation incididunt ea amet commodo deserunt nulla. Anim
                    sit duis cupidatat ipsum deserunt reprehenderit fugiat cupidatat.
                    In do eu sint Lorem qui eu eu. Id ad non pariatur culpa.
                  </p>
                </div>
              </div>
            </div>
            <!-- End Post Section-->

            <!-- Start Post Section -->
            <div class="card card-shadow card-bordered post-wall">
              <div class="card-block clearfix p-25">
                <div class="media-avatar">
                  <a href="#" class="avatar avatar-lg">
                    <img class="img-fluid" src="../../../global/portraits/16.jpg">
                  </a>
                </div>
                <div class="media-body text-middle">
                  <h5 class="mt-0 mb-0">
                    <a href="#">Mallinda Hollaway</a> to <a href="#">JigsawOffice2017</a> 
                    <?php if(rand(0,2)){?>
                    <span class="badge badge-dark">External</span>
                    <?php }?>
                  </h5>
                  <small>30th July 2017</small>
                  <p class="card-text mb-20 grey-800">
                    Ex quis excepteur exercitation incididunt ea amet commodo deserunt nulla. Anim
                    sit duis cupidatat ipsum deserunt reprehenderit fugiat cupidatat.
                    In do eu sint Lorem qui eu eu. Id ad non pariatur culpa.
                  </p>
                  <div class="card">
                    <div class="card-block" style="padding: 10px;">
                      <i class="icon fa-file-pdf-o float-left mr-10" style="font-size:34px; color:#f44336;" aria-hidden="true"></i> 
                      <h5 class="m-0 float-left mr-10">
                        <a href="javascript:void(0)" style="color:#f44336;">Wireframe inner</a>
                        <p class="mb-10 grey-500">
                          <small>
                            July 30, 2017 <span class="ml-10 mr-10">&mdash;</span> 4.25 MB</small>
                        </p>
                        <p class="mb-0 grey-500 font-size-12">
                          <a href="#">JigsawOffice2017</a>
                          <i class="icon md-chevron-right" aria-hidden="true"></i>
                          <a href="#">Drive</a>
                          <i class="icon md-chevron-right" aria-hidden="true"></i>
                          <a href="#">Design</a>
                        </p>
                      </h5>
                      
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- End Post Section-->

            <!-- Start Post Section -->
            <div class="card card-shadow card-bordered post-wall">
              <div class="card-block clearfix p-25">
                <table class="table m-0">
                  <tr class="">
                    <td style="border-top: 0;">
                      <a href="javascript:void(0)" style="text-decoration:none;" data-url="panel2.html" data-toggle="slidePanel" >
                      <span class="avatar avatar-s float-left mr-20">
                        <img src="../../../global/portraits/8.jpg" alt="">
                      </span>
                      <p class="blue-600 mb-5 font-size-16 text-nowrap"><strong>Robin Ahrens</strong></p>
                      <p class="grey-600 mb-10 text-nowrap">Robin</p>
                      </a>
                    </td>
                    <td width="30%" style="border-top: 0;">
                      <p data-info-type="phone" class="mb-10 text-nowrap">
                        <i class="icon fa-mobile mr-10"></i>
                        <span class="text-break">9192372533</span>
                      </p>
                      <p data-info-type="email" class="mb-10 text-nowrap">
                        <i class="icon md-email mr-10"></i>
                        <span class="text-break">malinda.h@gmail.com<span>
                      </p>
                    </td>
                    <td width="30%" class="hidden-md-down" style="border-top: 0;">
                      <?php
                      $aa[] = "Designer";
                      $aa[] = "jQuery";
                      $aa[] = "Javascript";
                      $aa[] = "HTML5";
                      $aa[] = "CSS";
                      $aa[] = "WordPress";
                      for($ab=0;$ab<6;$ab++){
                        $tmpC = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
                      ?>
                      <a href="#" style="text-decoration:none;">
                        <span class="badge mb-5 badge-outline" style="border-color:<?php echo $tmpC;?>; color:<?php echo $tmpC;?>;"><?=$aa[$ab];?></span>
                      </a>
                      <?php }?>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
            <!-- End Post Section-->

            <!-- Start Post Section -->
            <div class="card card-shadow card-bordered post-wall">
              <div class="card-block clearfix p-25">
                <table class="table m-0">
                  <tr class="">
                    <td style="border-top: 0;">
                      <a href="#" target="_blank" style="text-decoration:none;">
                      <div class="clearfix mb-5">
                        <div class="text-warning float-left"><i class="icon fa-circle mr-5" aria-hidden="true"></i></div>
                        <div class="progress w-150 mb-0 mt-5 float-left" style="height:12px;" data-labeltype="percentage" data-plugin="progress">
                          <div class="progress-bar progress-bar-warning" role="progressbar" style="width: 43%; line-height:12px;">
                            <span class="progress-label font-size-10">43%</span>
                          </div>
                        </div>
                        <span class="badge badge-dark float-left ml-10 mt-3">External</span>
                      </div>
                      <p class="mb-10 font-size-16 text-nowrap"><strong>Project Jigsaw Office 2017</strong></p>
                      </a>
                      <div class="hidden-lg-up">
                        <div class="hidden-sm-up">
                          <h6 class="font-size-12 mb-5 mt-3">
                            <span class="w-50 grey-600" style="display: inline-table;">Strat</span>
                            <span style="font-weight: 100;">September 30, 2016</span></h6>
                          <h6 class="font-size-12 mb-10">
                            <span class="w-50 grey-600" style="display: inline-table;">Finish</span>
                            <span style="font-weight: 100;">September 23, 2017</span></h6>
                        </div>
                        <ul class="addMember-items mb-10">
                          <?php for($aa=1;$aa<=rand(3,7);$aa++){?>
                          <li class="addMember-item mr-0">
                            <img style="width:20px;" class="avatar" src="../../global/portraits/<?php echo rand(1,20);?>.jpg" title="Herman Beck">
                          </li>
                          <?php }?>
                        </ul>
                        <ul class="list-inline hidden-sm-up">
                          <li class="list-inline-item mr-10">
                            <i class="icon fa-tasks"></i> 28
                          </li>
                          <li class="list-inline-item mr-10">
                            <i class="icon fa-comments-o"></i> 96
                          </li>
                          <li class="list-inline-item mr-10">
                            <i class="icon md-attachment-alt"></i> 8
                          </li>
                        </ul>
                      </div>
                      
                    </td>
                    <td width="30%" style="border-top: 0;" class="hidden-xs-down">
                      <h6 class="font-size-12 mb-5 mt-3">
                        <span class="w-50 grey-600" style="display: inline-table;">Strat</span>
                        <span style="font-weight: 100;">September 30, 2016</span></h6>
                      <h6 class="font-size-12 mb-5">
                        <span class="w-50 grey-600" style="display: inline-table;">Finish</span>
                        <span style="font-weight: 100;">September 23, 2017</span></h6>
                      <div class="hidden-lg-up">
                          <ul class="list-inline mt-20 mb-0">
                            <li class="list-inline-item mr-10">
                              <i class="icon fa-tasks"></i> 28
                            </li>
                            <li class="list-inline-item mr-10">
                              <i class="icon fa-comments-o"></i> 96
                            </li>
                            <li class="list-inline-item mr-10">
                              <i class="icon md-attachment-alt"></i> 8
                            </li>
                          </ul>
                        </div>
                    </td>
                    <td width="30%" style="border-top: 0;" class="hidden-md-down">
                        <ul class="addMember-items mb-10">
                          <?php for($aa=1;$aa<=rand(3,7);$aa++){?>
                          <li class="addMember-item mr-0">
                            <img style="width:20px;" class="avatar" src="../../global/portraits/<?php echo rand(1,20);?>.jpg" title="Herman Beck">
                          </li>
                          <?php }?>
                        </ul>
                        <ul class="list-inline">
                          <li class="list-inline-item mr-10">
                            <i class="icon fa-tasks"></i> 28
                          </li>
                          <li class="list-inline-item mr-10">
                            <i class="icon fa-comments-o"></i> 96
                          </li>
                          <li class="list-inline-item mr-10">
                            <i class="icon md-attachment-alt"></i> 8
                          </li>
                        </ul>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
            <!-- End Post Section-->

            <!-- Start Post Section -->
            <div class="card card-shadow card-bordered post-wall">
              <div class="card-block clearfix p-25">
                <div class="text-middle">
                  <h5 class="mt-0 mb-0">
                    <a href="#">Intranet</a> to <a href="#">News</a> 
                  </h5>
                  <small>30th July 2017</small>
                  <p class="card-text mb-20 grey-800">
                    <img width="150" src="../../assets/examples/images/login.jpg" align="left" class="mr-10 mb-10" alt="..."> Ex quis excepteur exercitation incididunt ea amet commodo deserunt nulla. Anim
                    sit duis cupidatat ipsum deserunt reprehenderit fugiat cupidatat.
                    In do eu sint Lorem qui eu eu. Id ad non pariatur culpa.
                  </p>
                </div>
              </div>
            </div>
            <!-- End Post Section-->


            <a class="btn btn-block btn-default mb-20" href="javascript:void(0)" role="button">Show more</a>
          </div>
          <!-- User Posts -->
        </div>
        <!-- End Middle Column -->
        <!-- Left Column -->
        <div class="col-md-12 col-lg-4">
          <div class="user-info card card-shadow">
            <div class="user-base card-block">
              <h4 class="user-name">Searches related</h4>
              <div class="row">
               <div class="col-md-6"><a href="#">web design tutorial</a></div>
               <div class="col-md-6"><a href="#">web design course</a></div>
               <div class="col-md-6"><a href="#">web design definition</a></div>
               <div class="col-md-6"><a href="#">web design templates</a></div>
               <div class="col-md-6"><a href="#">web design examples</a></div>
               <div class="col-md-6"><a href="#">web design pdf</a></div>
               <div class="col-md-6"><a href="#">learn web design</a></div>
               <div class="col-md-6"><a href="#">web design company</a></div>
              </div>
            </div>
          </div>
          
          <div class="user-info card card-shadow">
            <div class="user-base card-block">
              <h4 class="user-name">Tags related</h4>
              <div class="row">
               <div class="col-md-6"><a href="#">#webdesign</a></div>
               <div class="col-md-6"><a href="#">#web</a></div>
               <div class="col-md-6"><a href="#">#design</a></div>
               <div class="col-md-6"><a href="#">#designtemplates</a></div>
               <div class="col-md-6"><a href="#">#webexamples</a></div>
               <div class="col-md-6"><a href="#">#website</a></div>
               <div class="col-md-6"><a href="#">#webdesign</a></div>
               <div class="col-md-6"><a href="#">#webcompany</a></div>
              </div>
            </div>
          </div>

          <div class="user-info card card-shadow">
            <div class="user-base card-block">
              <h4 class="user-name">Top keyword search</h4>
              <div class="row">
               <div class="col-md-6"><a href="#">UI/UX</a></div>
               <div class="col-md-6"><a href="#">Web Design</a></div>
               <div class="col-md-6"><a href="#">Interaction Design</a></div>
               <div class="col-md-6"><a href="#">Front-end</a></div>
               <div class="col-md-6"><a href="#">Intranet</a></div>
               <div class="col-md-6"><a href="#">Workspace</a></div>
               <div class="col-md-6"><a href="#">Task</a></div>
               <div class="col-md-6"><a href="#">Jigsaw office</a></div>
              </div>
            </div>
          </div>

          <div class="user-info card card-shadow">
            <div class="user-base card-block">
              <h4 class="user-name">Top tags search</h4>
              <div class="row">
               <div class="col-md-6"><a href="#">#marketing</a></div>
               <div class="col-md-6"><a href="#">#website</a></div>
               <div class="col-md-6"><a href="#">#intranet</a></div>
               <div class="col-md-6"><a href="#">#supper</a></div>
               <div class="col-md-6"><a href="#">#html</a></div>
               <div class="col-md-6"><a href="#">#oursource</a></div>
               <div class="col-md-6"><a href="#">#coding</a></div>
               <div class="col-md-6"><a href="#">#webcompany</a></div>
              </div>
            </div>
          </div>
          
          <!-- End User Info -->
        </div>
        <!-- End Left Column -->

      </div>
    </div>
  </div>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/slick-carousel/slick.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/plyr/plyr.js"></script>
  <script src="../../global/vendor/aspieprogress/jquery-asPieProgress.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../assets/examples/js/pages/profile-v2.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/plyr.js"></script>
  <script src="../../global/js/Plugin/aspieprogress.js"></script>
  <script src="../../assets/examples/js/charts/pie-progress.js"></script>

  <script src="../../global/vendor/bootstrap-daterangepicker/moment.min.js"></script>
  <script src="../../global/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();


      function chooseDateRange(start, end) {
          var DrangeValue = start.format('MM/DD/YYYY') + '-' + end.format('MM/DD/YYYY');
          $('#SearchDateRange span#daterange-value').html(DrangeValue);
          var dataKey = "123";
          var dataValue = "xxx";
          var stoken = '';
            $('.search-wrapper .currently-showing .chooseDateRange').remove();
            stoken = '<div class="token chooseDateRange" data-key="'+dataKey+'" data-value="' + dataValue + '"><span class="token-label" >' + DrangeValue + '</span><a  href="#" class="close" tabindex="-1">×</a></div>';
            $('.search-wrapper .currently-showing').append(stoken);
            sysGenUrlSearch();
      }

      $('#SearchDateRange').daterangepicker({
          "autoApply": true,
          "opens": "center",
          "parentEl": ".search-wrapper",
          ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
      }, chooseDateRange);

    });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>