<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../assets/examples/css/forms/layouts.css">
  <link rel="stylesheet" href="../../global/vendor/icheck/icheck.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
  <link rel="stylesheet" href="../../global/vendor/timepicker/jquery-timepicker.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
  <style type="text/css">
    table a{
      text-decoration: none!important;
    }
  </style>
</head>
<body class="animsition page-aside-right">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-10">Teams</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item active">Apps</li>
        <li class="breadcrumb-item active">Support Tickets</li>
        <li class="breadcrumb-item active">Teams</li>
      </ol>
      <?php include("mini-nav.php");?>
      
    </div>
    
    <div class="page-content container-fluid" style="position: relative;">
      <div class="page-aside">
            <!-- Contacts Sidebar -->
            <div class="page-aside-switch">
              <i class="icon md-chevron-left" aria-hidden="true"></i>
              <i class="icon md-chevron-right" aria-hidden="true"></i>
            </div>
            <div class="page-aside-inner page-aside-scroll">
              <div data-role="container">
                <div data-role="content">
                  <section class="page-aside-section">
                    <div class="card card-block p-10">
                      <div class="p-10">
                        <h3 class="card-title">Response/Resolution</h3>

                        <div class="card card-block p-10 mb-0">
                          <div class="counter counter-lg">
                            <span class="counter-number">02:30 hrs</span>
                            <div class="counter-label text-uppercase">Average Response Time</div>
                            <div><i class="icon md-trending-up text-danger" aria-hidden="true"></i> 00:23</div>
                          </div>
                        </div>
                        <div class="card card-block p-10 mb-0">
                          <div class="counter counter-lg">
                            <span class="counter-number">25:43 hrs</span>
                            <div class="counter-label text-uppercase">Average Resolution Time</div>
                            <div><i class="icon md-trending-down text-success" aria-hidden="true"></i> 00:41</div>
                          </div>
                        </div>

                        <div class="counter counter-md text-left">
                          <div class="contextual-progress mb-0 mt-10">
                            <div class="clearfix">
                              <div class="progress-title">First Response time</div>
                              <div class="progress-label">01:19</div>
                            </div>
                            <div class="progress" style="height:5px;" data-plugin="progress">
                              <div class="progress-bar progress-bar-info" aria-valuemin="0" style="width: 4%;"  role="progressbar">
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="counter counter-md text-left">
                          <div class="contextual-progress mb-0 mt-10">
                            <div class="clearfix">
                              <div class="progress-title">Average Response Time</div>
                              <div class="progress-label">02:30</div>
                            </div>
                            <div class="progress" style="height:5px;" data-plugin="progress">
                              <div class="progress-bar progress-bar-info" aria-valuemin="0" style="width: 9%;"  role="progressbar">
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="counter counter-md text-left">
                          <div class="contextual-progress mb-0 mt-10">
                            <div class="clearfix">
                              <div class="progress-title">Average Resolution Time</div>
                              <div class="progress-label">25:43</div>
                            </div>
                            <div class="progress" style="height:5px;" data-plugin="progress">
                              <div class="progress-bar progress-bar-info" aria-valuemin="0" style="width: 100%;"  role="progressbar">
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>

                    <div class="card card-block pt-0 mb-0">
                      <h3 class="mt-0">Happiness Ratings</h3>
                        <div class="pt-20 pb-20">
                          <div class="card card-block p-10 mb-0">
                            <div class="counter counter-lg">
                              <span class="counter-number"><i class="icon md-mood" aria-hidden="true"></i> 4.8</span>
                              <div class="counter-label text-uppercase">Happiness Ratings</div>
                              <div><i class="icon md-trending-up text-success" aria-hidden="true"></i> 1.3%</div>
                            </div>
                          </div>
                          <div class="example-wrap">
                            <h5 class=" mt-30">Ratings <div data-half="true" class="rating rating-lg mx-10" data-score="4.5">
                            <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                            <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                            <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                            <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                            <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                          </div>4.8/5 (112)</h5>
                          <table class="table table-striped">
                            <tbody><tr>
                              <td class="bg-grey-100" width="25%">Response</td>
                              <td>
                               <div data-half="true" class="rating ml-10" data-score="4.5">
                                  <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                  <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                  <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                  <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                                  <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td class="bg-grey-100" width="25%">Resolution</td>
                              <td>
                                <div data-half="true" class="rating ml-10" data-score="4.5">
                                  <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                  <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                  <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                  <i data-alt="4" class="icon md-star orange-600 m-0" title="good"></i>
                                  <i data-alt="5" class="icon md-star orange-500 m-0" title="gorgeous"></i>
                                </div>
                              </td>
                            </tr>
                            
                            
                          </tbody></table>
                          </div>
                        </div>
                    </div>

                  </section>
                </div>
              </div>
            </div>
          </div>

      <div class="page-main">
        <div class="row pt-20 ml-0 mr-0 bg-white">
          <div class="col-md-12">
            <section>
              <div class="pb-10">
              <div class="search-wrapper">
                <div class="search-box">
                  <div class="icon md-search"></div>
                  <div class="currently-showing">
                    <?=$search['tokenhtml']?>
                  </div>
                </div>
                <a href="javascript:void(0);" class="clear_all btn btn-flat btn-default btn-block waves-effect waves-classic">Clear All</a>
                <div class="data_entry">
                  <input class="input keyword-input" placeholder="Enter a keyword" type="text">
                  <div class="icon md-close-circle close"></div>
                </div>
                <div class="filters">
                    
                </div>
              </div>
            </div>

              <div class="py-20 mb-20">
                <div class="float-right">
                  <div class="dropdown">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      <i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Sort by
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-media w-100" role="menu">
                      <h6 class="pl-10" aria-hidden="false">Sort by</h6>
                      <a class="dropdown-item" href="javascript:void(0)">Name</a>
                      <a class="dropdown-item" href="javascript:void(0)">Review</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-sort-amount-asc" aria-hidden="true"></i> Ascending</a>
                      <a class="dropdown-item active" href="javascript:void(0)"><i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Descending</a>
                    </div>
                  </div>
                </div>
                
                <div class="pt-10">Total <strong>30</strong> rows</div>
              </div>
              
            </section>
            <section>
            <div class="tab-pane" id="exampleTabsTwo" style="min-height: 750px;" role="tabpanel" aria-expanded="false">
              <table class="table table-hover">
                  <thead>
                    <tr>
                      <th >Name</th>
                      <th width="20%">AVG Response Time</th>
                      <th width="20%">AVG Resolution Time</th>
                      <th width="20%">Review</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    for($a=1;$a<=5;$a++){
                      $index = rand(0,6);
                    ?>
                    <tr>
                      <td>
                        <div class="media">
                          <div class="pr-10">
                            <a class="avatar" href="javascript:void(0)" data-target="#driver-detail" data-toggle="modal">
                              <img class="img-fluid" src="../../../global/portraits/8.jpg" alt="...">
                            </a>
                          </div>
                          <div class="media-body pt-5">
                            <h5 class="mt-0 mb-0"><a href="javascript:void(0);" data-target="#driver-detail" data-toggle="modal">Edward Fletcher</a></h5>
                            <small>IT Support</small>
                          </div>
                        </div>
                        
                        </td>
                      <td>
                        <div class="pt-5">01:21 hrs</div>
                        <small><i class="icon md-trending-up text-danger" aria-hidden="true"></i> 00:23</small>
                      </td>
                      <td>
                        <div class="pt-5">21:21 hrs</div>
                        <small><i class="icon md-trending-down text-success" aria-hidden="true"></i> 00:42</small>
                      </td>
                      <td>
                        <small>4.8/5 (123)</small>
                        <div data-half="true" data-plugin="rating" class="rating rating-lg" data-score="4.5">
                          <i data-alt="1" class="icon md-star orange-600" title="bad"></i>
                          <i data-alt="2" class="icon md-star orange-600" title="poor"></i>
                          <i data-alt="3" class="icon md-star orange-600" title="regular"></i>
                          <i data-alt="3" class="icon md-star orange-600" title="good"></i>
                          <i data-alt="4" class="icon md-star-half orange-500" title="gorgeous"></i>
                        </div>
                      </td>
                    </tr>
                    <?php }?>
                  </tbody>
                </table>
            </div>
          </section>
          </div>
        </div>

      </div>
    </div>
  </div>
  <div class="modal fade" id="driver-detail" aria-hidden="true" aria-labelledby="booking-detail"
          role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple">
      <form class="modal-content form-horizontal" action="#" method="post" role="form">
        <div class="modal-header py-20" style="background: #660066 url(img/bg-car.png);background-size:cover;">
          <button type="button" class="close white close-bookingcar" data-dismiss="modal" aria-label="Close">
            <i class="icon md-close" aria-hidden="true"></i>
          </button>
          <h1 class="modal-title white w-full text-center">Support Detail</h1>
        </div>
        <div class="modal-body py-20">
          <div class="row">
              <div class="col-md-12">
                <h5>Infomation</h5>
                <table class="table table-striped">
                  <tr>
                    <td class="bg-grey-100" width="25%">Name</td>
                    <td>
                      <div class="media">
                          <div class="pr-10">
                            <a class="avatar" href="javascript:void(0)" data-target="#driver-detail" data-toggle="modal">
                              <img class="img-fluid" src="../../../global/portraits/8.jpg" alt="...">
                            </a>
                          </div>
                          <div class="media-body pt-5">
                            <h5 class="mt-0 mb-0"><a href="javascript:void(0);" data-target="#driver-detail" data-toggle="modal">Edward Fletcher</a></h5>
                            <small>IT Support</small>
                          </div>
                        </div>
                    </td>
                  </tr>
                  
                  <tr>
                    <td class="bg-grey-100" width="25%">Ticket Statistics</td>
                    <td>
                      <div id="chart_over" style="width: 100%; height: 200px;"></div>
                      <div class="card-group">
                        <div class="card card-inverse card-info m-0">
                          <div class="card-block text-center p-5">
                            <h5 class="card-title m-0">10</h5>
                          </div>
                        </div>
                        <div class="card card-inverse card-warning m-0">
                          <div class="card-block text-center p-5">
                            <h5 class="card-title m-0">14</h5>
                          </div>
                        </div>
                        <div class="card card-inverse card-success m-0">
                          <div class="card-block text-center p-5">
                            <h5 class="card-title m-0">234</h5>
                          </div>
                        </div>
                        <div class="card card-inverse bg-grey-600 m-0">
                          <div class="card-block text-center p-5">
                            <h5 class="card-title m-0">3</h5>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td class="bg-grey-100" width="25%">Average Response Time</td>
                    <td>
                      <div class="pt-5">01:21 hrs</div>
                      <small><i class="icon md-trending-up text-danger" aria-hidden="true"></i> 00:23</small>
                    </td>
                  </tr>
                  <tr>
                    <td class="bg-grey-100" width="25%">Average Resolution Time</td>
                    <td>
                      <div class="pt-5">21:21 hrs</div>
                        <small><i class="icon md-trending-down text-success" aria-hidden="true"></i> 00:42</small>
                    </td>
                  </tr>
                </table>

                <h5 class=" mt-30">Ratings <div data-half="true" data-plugin="rating" class="rating rating-lg mx-10" data-score="4.5">
                        <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                        <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                        <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                        <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                        <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                      </div>4.8/5 (112)</h5>
                <table class="table table-striped">
                  <tr>
                    <td class="bg-grey-100" width="25%">Response</td>
                    <td>
                     <div data-half="true" class="rating ml-10" data-score="4.5">
                        <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                        <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                        <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                        <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                        <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td class="bg-grey-100" width="25%">Resolution</td>
                    <td>
                      <div data-half="true" class="rating ml-10" data-score="4.5">
                        <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                        <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                        <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                        <i data-alt="4" class="icon md-star orange-600 m-0" title="good"></i>
                        <i data-alt="5" class="icon md-star orange-500 m-0" title="gorgeous"></i>
                      </div>
                    </td>
                  </tr>
                  
                  
                </table>

              </div>
          </div>
          

          
        </div>
      </form>
    </div>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <script src="../../global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
  <script src="../../global/vendor/icheck/icheck.min.js"></script>
  <script src="../../global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
  <script src="../../global/vendor/timepicker/jquery.timepicker.min.js"></script>
  <script src="../../global/vendor/datepair/datepair.min.js"></script>
  <script src="../../global/vendor/datepair/jquery.datepair.min.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../assets/js/BaseApp.js"></script>
  <script src="../../assets/js/App/Contacts.js"></script>
  <script src="../../assets/examples/js/apps/contacts.js"></script>
  <script src="../../global/js/Plugin/jquery-placeholder.js"></script>
  <script src="../../global/js/Plugin/icheck.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-datepicker.js"></script>
  <script src="../../global/js/Plugin/jt-timepicker.js"></script>
  <script src="../../global/js/Plugin/datepair.js"></script>


  <script src="../../global/vendor/amcharts/amcharts.js" type="text/javascript"></script>
  <script src="../../global/vendor/amcharts/serial.js" type="text/javascript"></script>
  <script src="../../global/vendor/amcharts/pie.js" type="text/javascript"></script>

  <script>
    // initialize input widgets first
    $('#frm_datepair .time').timepicker({
        'showDuration': true,
        'timeFormat': 'H:i'
    });

    $('#frm_datepair .date').datepicker({
        'format': 'm/d/yyyy',
        'autoclose': true
    });

    // initialize datepair
    var basicExampleEl = document.getElementById('frm_datepair');
    var datepair = new Datepair(basicExampleEl);

</script>

  <script type="text/javascript">
    
    var _ssource;
    var chartData_ssource = [
        {
            "title": "New",
            "value": 10,
            "color": "#00bcd4"
        },
        {
            "title": "in Progress",
            "value": 14,
            "color": "#ff9800"
        },
        {
            "title": "Closed",
            "value": 234,
            "color": "#55ce63"
        },
        {
            "title": "On-hold",
            "value": 3,
            "color": "#616161"
        }
    ];

    AmCharts.ready(function () {
        // PIE CHART
        _ssource = new AmCharts.AmPieChart();
        _ssource.dataProvider = chartData_ssource;
        _ssource.titleField = "title";
        _ssource.valueField = "value";
        _ssource.colorField = "color";
        _ssource.radius = "30%";
        _ssource.innerRadius = "40%";
        _ssource.labelRadius = 5;
        _ssource.labelText = "[[title]]";

        // WRITE
        _ssource.write("chart_over");
    });
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>