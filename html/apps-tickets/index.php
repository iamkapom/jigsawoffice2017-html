<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../global/vendor/jquery-selective/jquery-selective.css">
  <link rel="stylesheet" href="../../global/vendor/aspieprogress/asPieProgress.css">
  <link rel="stylesheet" href="../../assets/examples/css/charts/pie-progress.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-daterangepicker/daterangepicker.css" />
  <!-- Fonts -->
  <link rel="stylesheet" href="../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
  <style type="text/css">
    .task-subject{
      float: left;
      width: 90%;
    }
    .task-content{
      float: right;
      width: 10%;
    }
    .sub-task .task-subject{
      float: left;
      width: 85%;
    }
    .sub-task .task-content{
      float: right;
      width: 15%;
    }
    .task-content .assign{
      float: left;
      padding-top:10px;
    }
    .task-content .task-icon{
      float: right;
      padding-top:10px;
    }
    .success .task-subject, .success .task-subject .grey-900, .success .task-content, .success .sub-task .task-subject, .success .sub-task .task-subject .grey-900,{
      color: #bdbdbd!important;
    }
    .success .task-subject, .success .sub-task .task-subject{
      text-decoration:line-through;
    }
    @media (max-width: 1024px) {
      .task-subject{
        width: 65%;
      }
      .task-content{
        width: 35%;
      }
    }
    @media (max-width: 980px) {
      .task-subject{
        width: 100%;
        float: none;
      }
      .task-content{
        width: 100%;
        float: none;
      }
      .sub-task .task-content .assign{
        float: none;
        text-align: center;
        padding-top:0px;
      }
      .sub-task .task-content .task-icon{
        float: none;
        text-align: center;
        padding-top:0px;
      }
    }
  </style>
</head>
<body class="animsition page-aside-right">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page no-headnav">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-10">Tickets list</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item active">Apps</li>
        <li class="breadcrumb-item active">Support Tickets</li>
        <li class="breadcrumb-item active">Tickets list</li>
      </ol>
      <?php include("mini-nav.php");?>
      
    </div>

    <div class="page-content container-fluid" style="position: relative;">
      <div class="row ml-0 mr-0">
        <div class="col-md-12">

          <div class="page-aside">
            <!-- Contacts Sidebar -->
            <div class="page-aside-switch">
              <i class="icon md-chevron-left" aria-hidden="true"></i>
              <i class="icon md-chevron-right" aria-hidden="true"></i>
            </div>
            <div class="page-aside-inner page-aside-scroll">
              <div data-role="container">
                <div data-role="content">
                  <section class="page-aside-section">
                    
                    <div class="card card-block pt-0 mb-0">
                      <h3 class="mt-0">Ticket Statistics</h3>
                        <div class="pt-20 pb-0">
                          <div id="chart_over" style="width: 100%; height: 200px;"></div>
                        </div>
                    </div>

                    <div class="card card-block p-10">
                      <div class="p-10">
                        <h3 class="card-title">Response/Resolution</h3>

                        <div class="card card-block p-10 mb-0">
                          <div class="counter counter-lg">
                            <span class="counter-number">02:30 hrs</span>
                            <div class="counter-label text-uppercase">Average Response Time</div>
                            <div><i class="icon md-trending-up text-danger" aria-hidden="true"></i> 00:23</div>
                          </div>
                        </div>
                        <div class="card card-block p-10 mb-0">
                          <div class="counter counter-lg">
                            <span class="counter-number">25:43 hrs</span>
                            <div class="counter-label text-uppercase">Average Resolution Time</div>
                            <div><i class="icon md-trending-down text-success" aria-hidden="true"></i> 00:41</div>
                          </div>
                        </div>

                        <div class="counter counter-md text-left">
                          <div class="contextual-progress mb-0 mt-10">
                            <div class="clearfix">
                              <div class="progress-title">First Response time</div>
                              <div class="progress-label">01:19</div>
                            </div>
                            <div class="progress" style="height:5px;" data-plugin="progress">
                              <div class="progress-bar progress-bar-info" aria-valuemin="0" style="width: 4%;"  role="progressbar">
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="counter counter-md text-left">
                          <div class="contextual-progress mb-0 mt-10">
                            <div class="clearfix">
                              <div class="progress-title">Average Response Time</div>
                              <div class="progress-label">02:30</div>
                            </div>
                            <div class="progress" style="height:5px;" data-plugin="progress">
                              <div class="progress-bar progress-bar-info" aria-valuemin="0" style="width: 9%;"  role="progressbar">
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="counter counter-md text-left">
                          <div class="contextual-progress mb-0 mt-10">
                            <div class="clearfix">
                              <div class="progress-title">Average Resolution Time</div>
                              <div class="progress-label">25:43</div>
                            </div>
                            <div class="progress" style="height:5px;" data-plugin="progress">
                              <div class="progress-bar progress-bar-info" aria-valuemin="0" style="width: 100%;"  role="progressbar">
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>

                    <div class="card card-block pt-0 mb-0">
                      <h3 class="mt-0">Happiness Ratings</h3>
                        <div class="pt-20 pb-20">
                          <div class="card card-block p-10 mb-0">
                            <div class="counter counter-lg">
                              <span class="counter-number"><i class="icon md-mood" aria-hidden="true"></i> 4.8</span>
                              <div class="counter-label text-uppercase">Happiness Ratings</div>
                              <div><i class="icon md-trending-up text-success" aria-hidden="true"></i> 1.3%</div>
                            </div>
                          </div>
                          <div class="example-wrap">
                            <h5 class=" mt-30">Ratings <div data-half="true" class="rating rating-lg mx-10" data-score="4.5">
                            <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                            <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                            <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                            <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                            <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                          </div>4.8/5 (112)</h5>
                          <table class="table table-striped">
                            <tbody><tr>
                              <td class="bg-grey-100" width="25%">Response</td>
                              <td>
                               <div data-half="true" class="rating ml-10" data-score="4.5">
                                  <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                  <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                  <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                  <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                                  <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td class="bg-grey-100" width="25%">Resolution</td>
                              <td>
                                <div data-half="true" class="rating ml-10" data-score="4.5">
                                  <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                  <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                  <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                  <i data-alt="4" class="icon md-star orange-600 m-0" title="good"></i>
                                  <i data-alt="5" class="icon md-star orange-500 m-0" title="gorgeous"></i>
                                </div>
                              </td>
                            </tr>
                            
                            
                          </tbody></table>
                          </div>
                        </div>
                    </div>

                  </section>
                </div>
              </div>
            </div>
          </div>
          
          <div class="page-main">
            <div class="pb-10">
              <div class="search-wrapper">
                <div class="search-box">
                  <div class="icon md-search"></div>
                  <div class="currently-showing">
                    <?=$search['tokenhtml']?>
                  </div>
                </div>
                <a href="javascript:void(0);" class="clear_all btn btn-flat btn-default btn-block waves-effect waves-classic">Clear All</a>
                <div class="data_entry">
                  <input class="input keyword-input" placeholder="Enter a keyword" type="text">
                  <div class="icon md-close-circle close"></div>
                </div>
                <div class="filters">
                    <div class="dropdown filter_permission">
                      <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Category
                        <span class="icon md-chevron-down" aria-hidden="true"></span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)"> Category 1</a>
                        <a class="dropdown-item" href="javascript:void(0)"> Category 2</a>
                        <a class="dropdown-item" href="javascript:void(0)"> Category 3</a>
                        <a class="dropdown-item" href="javascript:void(0)"> Category 4</a>
                      </div>
                    </div>
                    <div class="dropdown filter_permission">
                      <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Status
                        <span class="icon md-chevron-down" aria-hidden="true"></span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)">Open</a>
                        <a class="dropdown-item" href="javascript:void(0)">In Progress</a>
                        <a class="dropdown-item" href="javascript:void(0)">Closed</a>
                        <a class="dropdown-item" href="javascript:void(0)">On-hold</a>
                      </div>
                    </div>
                    
                    <div class="dropdown filter_permission">
                      <button type="button" id="SearchDateRange" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Time <i class="icon md-chevron-down" aria-hidden="true"></i>
                        <span id="daterange-value"></span>
                      </button>
                    </div>
                    
                </div>
              </div>
            </div>
            <div class="pb-20">
                <div class="actions-inner float-right">
                  <div class="dropdown">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      <i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Due date
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-media w-100" role="menu">
                      <h6 class="pl-10" aria-hidden="false">Sort by</h6>
                      <a class="dropdown-item active" href="javascript:void(0)">Create date</a>
                      <a class="dropdown-item" href="javascript:void(0)">Updated</a>
                      <a class="dropdown-item" href="javascript:void(0)">Subject</a>
                      <a class="dropdown-item" href="javascript:void(0)">Status</a>
                      <a class="dropdown-item" href="javascript:void(0)">Attachments</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-sort-amount-asc" aria-hidden="true"></i> Ascending</a>
                      <a class="dropdown-item active" href="javascript:void(0)"><i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Descending</a>
                    </div>
                  </div>
                </div>
                

                <div class="pt-10">About <strong>26</strong> results</div>
              </div>
            </section>
            
            <section>
              <div class="tab-pane bg-white px-20" style="min-height: 1100px;" role="tabpanel" aria-expanded="false">
                <table class="table table-hover">
                    <thead>
                      <tr>
                        <th width="10%">#ID</th>
                        <th >Subject</th>
                        <th width="10%">Status</th>
                        <th width="20%">Support/Due Date</th>
                        <th width="10%">Updated</th>
                        <th width="3%">&nbsp</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                      $_colorb[] = "info";
                      $_type[] = "Open";
                      $_subj[] = "How to change colors";
                      $_cat[] = "Category 1";


                      $_colorb[] = "warning";
                      $_type[] = "In Progress";
                      $_subj[] = "How to set Horizontal nav";
                      $_cat[] = "Category 2";

                      $_colorb[] = "dark";
                      $_type[] = "Closed";
                      $_subj[] = "How to customize the template?";
                      $_cat[] = "Category 3";
                      

                      $_color = array("#4caf50","#ff9800");
                      $_status = array("อ่านแล้ว","ยังไม่ได้อ่าน");
                      for($a=1;$a<=15;$a++){
                        $index = rand(0,2);
                        $_index = rand(0,1);
                      ?>
                      <tr>
                        <td class=" pt-10 <?=$_type[$index]=="Closed"?"grey-400":""?>">
                          #123456
                        </td>
                        <td class="<?=$_type[$index]=="Closed"?"grey-400":"grey-700"?> pt-10">
                          <h5 class="mt-0 mb-10">
                            <a class="<?=$_type[$index]=="Closed"?"grey-400":""?>" href="javascript:void(0);" style="text-decoration: none;" data-target="#ticket-detail" data-toggle="modal">
                              <?=$_subj[$index]?>
                            </a>
                          </h5>
                          <small>
                            <span><strong><img class="avatar avatar-xs" src="../../global/portraits/5.jpg" alt="Edward Fletcher">  Nathan Watts</strong></span><span class="ml-5 mr-5">—</span>4/07/2018 11:30<span class="ml-5 mr-5">—</span><?=$_cat[$index]?>
                            
                          </small>
                          </td>
                        <td class=" pt-10 <?=$_type[$index]=="Closed"?"grey-400":""?>">
                          <span class="badge badge-<?=$_colorb[$index]?>"><?=$_type[$index]?></span>
                          <ul class="list-inline mb-0 mt-5">
                            <li class="list-inline-item mr-10">
                              <i class="icon fa-comments-o"></i> 6
                            </li>
                            <li class="list-inline-item mr-10">
                              <i class="icon md-attachment-alt"></i> 1
                            </li>
                          </ul>
                        </td>
                        <td class=" pt-10 <?=$_type[$index]=="Closed"?"grey-400":""?>">
                          <?php
                          if($_type[$index]!="Open"){
                          ?>
                          <div class="mb-5">
                            <strong><img class="avatar avatar-xs" src="../../global/portraits/12.jpg" alt="Edward Fletcher">  Herman Beck</strong>
                          </div>
                          <div class="collapsed font-size-12">
                            15/05/2018<span class="ml-5 mr-5">—</span>Due in 1 days
                          </div>
                          <?php }?>
                        </td>
                        <td>
                          5 min ago
                        </td>
                        <td style="position:relative;">
                          <div class="wall-action" style="top:0; display: <?=$_type[$index]=="Closed"?"none":"block"?>;">
                            <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                              <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" role="menu">
                              <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                                Edit
                              </a>
                              <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                                Delete
                              </a>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                                Closed
                              </a>
                            </div>
                          </div>
                        </td>
                      </tr>
                      <?php }?>
                    </tbody>
                  </table>
              </div>
            </section>

            
            
          </div>

        </div>
      </div>
    </div>
  </div>
  <div class="site-action" data-plugin="actionBtn">
    <button type="button" data-target="#ticket-form" data-toggle="modal" class="btn-raised btn btn-success btn-floating">
      <i class="front-icon md-plus" aria-hidden="true"></i>
    </button>
  </div>

  <div class="modal fade" id="ticket-form" aria-hidden="true" aria-labelledby="booking-detail"
          role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple">
      <form class="modal-content form-horizontal" action="#" method="post" role="form">
        <div class="modal-header py-20 bg-orange-600">
          <button type="button" class="close white close-bookingcar" data-dismiss="modal" aria-label="Close">
            <i class="icon md-close" aria-hidden="true"></i>
          </button>
          <h1 class="modal-title white w-full text-center">Open Ticket</h1>
        </div>
        <div class="modal-body py-20">
            
            <div class="row justify-content-md-center">
                <div class="col-md-12">
                  <h5>Subject</h5>
                  <input type="text" class="form-control" id="inputnetwork" placeholder="เรื่อง" />
                </div>
            </div>
            <div class="row justify-content-md-center mt-10">
                <div class="col-md-12">
                  <h5>Detail</h5>
                  <textarea class="form-control" id="textareaDefault" placeholder="รายละเอียด..." rows="3"></textarea>
                </div>
            </div>
            <div class="row justify-content-md-center mt-10">
                <div class="col-md-12 post-quick-form">
                  <h5 class="float-left mt-15">Category</h5>
                  <div class="postfromarea">
                    <ul class="list-unstyled list-inline">
                      <li class="float-left  mb-5">
                        <input type="radio" class="icheckbox-orange" id="inputRadiosUnchecked3" name="inputRadios2"
                        data-plugin="iCheck" data-radio-class="iradio_flat-orange" checked />
                        <label class="ml-5 mr-30" for="inputRadiosUnchecked3">Category 1</label>
                      </li>
                      <li class="float-left mb-5">
                        <input type="radio" class="icheckbox-orange" id="inputRadiosChecked4" name="inputRadios2"
                        data-plugin="iCheck" data-radio-class="iradio_flat-orange"/>
                        <label class="ml-5 mr-30" for="inputRadiosChecked4">Category 2</label>
                      </li>
                      <li class="float-left mb-5">
                        <input type="radio" class="icheckbox-orange" id="inputRadiosChecked5" name="inputRadios2"
                        data-plugin="iCheck" data-radio-class="iradio_flat-orange"/>
                        <label class="ml-5 mr-30" for="inputRadiosChecked5">Category 3</label>
                      </li>
                    </ul>
                  </div>
                </div>
            </div>
            <div class="row mt-10">
                <div class="col-md-12 post-quick-form">
                  <h5 class="float-left mr-20">Attach</h5>
                  <div class="postfromarea">
                    <button type="button" class="btn btn-dark waves-effect waves-classic mr-20">
                      <i class="icon md-attachment-alt" aria-hidden="true"></i> Attach
                    </button>
                  </div>
                </div>
            </div>
            <div class="row mt-20 mb-30">
                <div class="col-md-12 post-quick-form">
                  <h5 class="float-left">From</h5>
                  <div class="postfromarea">
                    <select id="selectTeam2" class="form-control" style="width:100%">
                      <?php
                      $teams_arr[] = "Ada.Hoppe";
                      $teams_arr[] = "Adrianna_Durgan";
                      $teams_arr[] = "Albin.Kreiger";
                      $teams_arr[] = "Alisa";
                      $teams_arr[] = "August";
                      $teams_arr[] = "Bell.Mueller";
                      $teams_arr[] = "Bret";
                      $teams_arr[] = "Ceasara_Orn";
                      $teams_arr[] = "Chester";
                      $teams_arr[] = "Citlalli_Wehner";
                      $teams_arr[] = "Clementina";
                      $teams_arr[] = "Coby";
                      $teams_arr[] = "Colin";
                      $teams_arr[] = "Damon";
                      $teams_arr[] = "Davin";
                      $teams_arr[] = "Elliott_Becker";
                      $teams_arr[] = "Emerson";
                      $teams_arr[] = "Gerhard";
                      $teams_arr[] = "Gunnar";
                      $teams_arr[] = "Gunner_Jakubowski";
                      $teams_arr[] = "Heath.Ryan";
                      $teams_arr[] = "Herta";
                      $teams_arr[] = "Hubert";
                      $teams_arr[] = "Jarvis.Simonis";
                      $teams_arr[] = "Jennie";
                      $teams_arr[] = "Johanna.Thiel";
                      $teams_arr[] = "Johnathan_Mraz";
                      $teams_arr[] = "Josephine";
                      $teams_arr[] = "Lacey";
                      $teams_arr[] = "Marjorie.Orn";
                      $teams_arr[] = "Mckenna.Herman";
                      $teams_arr[] = "Melany_Gerhold";
                      $teams_arr[] = "Miracle";
                      $teams_arr[] = "Monica";
                      $teams_arr[] = "Monique_Whitea";
                      $teams_arr[] = "Myriam_Nicolas";
                      $teams_arr[] = "Myrtie.Gerhold";
                      $teams_arr[] = "Raina";
                      $teams_arr[] = "Ruben.Reilly";
                      $teams_arr[] = "Sammie";
                      $teams_arr[] = "Shanel";
                      $teams_arr[] = "Stone_Deckow";
                      $teams_arr[] = "Terrance.Borer";
                      $teams_arr[] = "Thea";
                      $teams_arr[] = "Torrey";
                      $teams_arr[] = "Treva";
                      $teams_arr[] = "Wilhelmine";
                      $teams_arr[] = "Yasmine";
                      
                      for($bb=0;$bb<count($teams_arr);$bb++){
                        echo '<option value="'.$teams_arr[$bb].'">'.$teams_arr[$bb].'</option>';
                      }
                      ?>
                      </select>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="modal-footer">
              <button type="button" class="btn btn-primary waves-effect waves-classic">Submit</button>
              <button type="button" class="btn btn-default btn-pure waves-effect waves-classic" data-dismiss="modal">Close</button>
            </div>
      </form>
    </div>
  </div>

  <div class="modal fade" id="ticket-detail" aria-hidden="true" aria-labelledby="booking-detail"
          role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple modal-mid-screen">
      <form class="modal-content form-horizontal" action="#" method="post" role="form">
        <div class="modal-header py-20" style="background: #660066;background-size:cover;">
          <button type="button" class="close white close-bookingcar" data-dismiss="modal" aria-label="Close">
            <i class="icon md-close" aria-hidden="true"></i>
          </button>
          <h1 class="modal-title white w-full text-center">How to set Horizontal nav</h1>
        </div>
        <div class="modal-body py-20">
          <div class="row">
              <div class="col-md-12">
                <table class="table table-striped">
                  <tr>
                    <td class="bg-grey-100" width="15%">Ticket ID</td>
                    <td colspan="3">
                      <h5 class="mt-3 mb-0">#2580</h5>
                    </td>
                  </tr>
                  <tr>
                    <td class="bg-grey-100" width="15%">Owner</td>
                    <td colspan="3">
                      <h5 class="mt-3 mb-0"><img class="avatar avatar-xs" src="../../global/portraits/5.jpg" alt="Edward Fletcher">  Nathan Watts</h5>
                    </td>
                  </tr>
                  <tr>
                    <td class="bg-grey-100" width="15%">Status</td>
                    <td width="35%">
                      <div class="row">
                        <div class="col-md-6">
                          <h5 class="mt-3 mb-0"><span class="badge badge-warning">In Progress</span></h5>
                        </div>
                        <div class="col-md-6 text-center">
                          <button type="button" onclick="$('#ticketclose').toggle();$('#ticketdetail').toggle();" class="btn btn-success btn-sm">Close Ticket</button>
                        </div>
                      </div>
                    </td>
                    <td class="bg-grey-100" width="15%">Category</td>
                    <td width="35%">
                      <h5 class="mt-3 mb-0">Category 1</h5>
                    </td>
                  </tr>
                  <tr>
                    <td class="bg-grey-100" width="15%">Attach</td>
                    <td colspan="3">
                      <h5 class="mt-3 mb-0">-</h5>
                    </td>
                  </tr>
                  <tr>
                    <td class="bg-grey-100" width="15%">Time opened</td>
                    <td  colspan="3">
                      <h5 class="mt-3 mb-0">4-07-2018 11:30</h5>
                    </td>
                  </tr>
                </table>
                <table class="table table-striped mt-20">
                  <tr>
                    <td class="bg-grey-100" width="15%">Support</td>
                    <td width="35%">
                      <h5 class="mt-3 mb-0"><img class="avatar avatar-xs" src="../../global/portraits/12.jpg" alt="Edward Fletcher">  Herman Beck</h5>
                    </td>
                    <td class="bg-grey-100" width="15%">Received</td>
                    <td width="35%">
                      <h5 class="mt-3 mb-0">4/07/2018 13:21 <span class="pl-20">(01:51 hrs)</span></h5>
                    </td>
                  </tr>
                  <tr>
                    <td class="bg-grey-100" width="15%">Due Date</td>
                    <td width="35%">
                      <h5 class="mt-3 mb-0">4/07/2018 <span class="pl-20">Due in 3 days</span></h5>
                    </td>
                  </tr>
                </table>
                <div class="card bg-grey-200 p-20" id="ticketclose" style="display: none;">
                  <h2 class="m-0">Feedback</h2>
                  <div class="black p-20">
                    We are constantly reviewing our performance and services to make improvements where we can. If you could please take a moment to rate your recent experience with the support team, by clicking below:
                  </div>
                  <table class="table mt-20">
                    <tbody>
                      <tr class=" bg-white">
                      <td width="25% black">Response</td>
                      <td>
                       <div class="">
                        <div class="rating rating-lg" data-plugin="rating" data-target="#exampleHintTarget"
                          data-hints="Bad,Poor,Regular,Good,Gorgeous"></div>
                        <span class="rating-hint pl-20" id="exampleHintTarget"></span>
                      </div>
                      </td>
                    </tr>
                    <tr class=" bg-white">
                      <td width="25% black">Resolution</td>
                      <td>
                        <div class="">
                        <div class="rating rating-lg" data-plugin="rating" data-target="#exampleHintTarget1"
                          data-hints="Bad,Poor,Regular,Good,Gorgeous"></div>
                        <span class="rating-hint pl-20" id="exampleHintTarget1"></span>
                      </div>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2">
                          <div class="form-group pt-20">
                            <textarea class="form-control" rows="5" placeholder="Comment here"></textarea>
                          </div>
                          <div class="form-group">
                            <button type="button" onclick="$('#ticketclose').toggle();$('#ticketdetail').toggle();" class="btn btn-primary waves-effect waves-classic">Close Ticket</button>
                            <button type="button" onclick="$('#ticketclose').toggle();$('#ticketdetail').toggle();" class="btn btn-link grey-600 waves-effect waves-classic">Cancel</button>
                          </div>
                      </td>
                    </tr>
                    
                    
                  </tbody></table>
                </div>
                <div class="card" id="ticketdetail">
                  <div class="card-block px-0">
                    <h2>How to set Horizontal nav</h2>
                    <div class="black mt-20">
                      <p class="bg-grey-200 p-20">Dediti iniuste vitae dedecora victi, pueros vulgo adhibenda at gravis epicurus alii facerem, aetatis. Rectas, splendide sensus ruinae dolendum audire ipse, loca mollis manilium concordia depravate congressus fruenda.</p>
                    </div>
                    
                    <div class="comment media" style="border-bottom:1px #eee solid;flex-direction:initial;">
                      <div class="pr-20">
                        <a class="avatar avatar-lg" href="javascript:void(0)">
                          <img src="../../global/portraits/12.jpg" alt="...">
                        </a>
                      </div>
                      <div class="media-body">
                        <div class="comment-body">
                          <a class="comment-author" href="javascript:void(0)"> Herman Beck</a>
                          <div class="comment-meta">
                            <span class="date">1 days ago</span>
                          </div>
                          <div class="comment-content">
                            <p>Dude, this is awesome. Thanks so much</p>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="comment media" style="border-bottom:1px #eee solid;flex-direction:initial;">
                      <div class="pr-20">
                        <a class="avatar avatar-lg" href="javascript:void(0)">
                          <img src="../../global/portraits/5.jpg" alt="...">
                        </a>
                      </div>
                      <div class="media-body">
                        <div class="comment-body">
                          <a class="comment-author" href="javascript:void(0)"> Nathan Watts</a>
                          <div class="comment-meta">
                            <span class="date">1 days ago</span>
                          </div>
                          <div class="comment-content">
                            <p>Dude, this is awesome. Thanks so much</p>
                          </div>
                        </div>
                      </div>
                    </div>

                    <form class="comment-reply" action="#" method="post">
                      <div class="form-group pt-20">
                        <textarea class="form-control" rows="5" placeholder="Comment here"></textarea>
                      </div>
                      <div class="form-group">
                        <button type="submit" class="btn btn-primary waves-effect waves-classic">Comment</button>
                        <button type="button" class="btn btn-link grey-600 waves-effect waves-classic">close</button>
                      </div>
                    </form>


                  </div>
                </div>
              </div>
          </div>
          

          
        </div>
      </form>
    </div>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <script src="../../global/vendor/aspieprogress/jquery-asPieProgress.js"></script>
  <script src="../../global/vendor/raty/jquery.raty.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../global/js/Plugin/aspieprogress.js"></script>
  <script src="../../assets/examples/js/charts/pie-progress.js"></script>
  <script src="../../global/js/Plugin/raty.js"></script>

  <script src="../../global/vendor/amcharts/amcharts.js" type="text/javascript"></script>
  <script src="../../global/vendor/amcharts/serial.js" type="text/javascript"></script>
  <script src="../../global/vendor/amcharts/pie.js" type="text/javascript"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();


      function chooseDateRange(start, end) {
          var DrangeValue = start.format('MM/DD/YYYY') + '-' + end.format('MM/DD/YYYY');
          $('#SearchDateRange span#daterange-value').html(DrangeValue);
          var dataKey = "123";
          var dataValue = "xxx";
          var stoken = '';
            $('.search-wrapper .currently-showing .chooseDateRange').remove();
            stoken = '<div class="token chooseDateRange" data-key="'+dataKey+'" data-value="' + dataValue + '"><span class="token-label" >' + DrangeValue + '</span><a  href="#" class="close" tabindex="-1">×</a></div>';
            $('.search-wrapper .currently-showing').append(stoken);
            sysGenUrlSearch();
      }

      $('#SearchDateRange').daterangepicker({
          "autoApply": true,
          "opens": "center",
          ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'This week': [moment().startOf('week'), moment().endOf('week')],
             'Next week': [moment().add(1, 'weeks').startOf('week'), moment().add(1, 'weeks').endOf('week')],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Next Month': [moment().add(1, 'month').startOf('month'), moment().add(1, 'month').endOf('month')]
          }
      }, chooseDateRange);



    });
  })(document, window, jQuery);
  </script>
  <script type="text/javascript">
    
    var _ssource;
    var chartData_ssource = [
        {
            "title": "New",
            "value": 530,
            "color": "#00bcd4"
        },
        {
            "title": "in Progress",
            "value": 1964,
            "color": "#ff9800"
        },
        {
            "title": "Closed",
            "value": 1713,
            "color": "#55ce63"
        },
        {
            "title": "On-hold",
            "value": 43,
            "color": "#616161"
        }
    ];

    AmCharts.ready(function () {
        // PIE CHART
        _ssource = new AmCharts.AmPieChart();
        _ssource.dataProvider = chartData_ssource;
        _ssource.titleField = "title";
        _ssource.valueField = "value";
        _ssource.colorField = "color";
        _ssource.radius = "30%";
        _ssource.innerRadius = "40%";
        _ssource.labelRadius = 5;
        _ssource.labelText = "[[title]]";

        // WRITE
        _ssource.write("chart_over");
    });
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>