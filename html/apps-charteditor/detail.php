<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../assets/examples/css/advanced/masonry.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition page-aside-right">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
     <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-10">Chart Editor</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../home/">Home</a></li>
        <li class="breadcrumb-item active">Chart Editor</li>
      </ol>
    </div>
    
    <div class="page-content container-fluid" style="position: relative;">
      
      <div class="row ml-0 mr-0">
        <div class="col-md-12">
        
          <div class="page-aside">
            <!-- Contacts Sidebar -->
            <div class="page-aside-switch">
              <i class="icon md-chevron-left" aria-hidden="true"></i>
              <i class="icon md-chevron-right" aria-hidden="true"></i>
            </div>
            <div class="page-aside-inner page-aside-scroll">
              <div data-role="container">
                <div data-role="content">
                  <section class="page-aside-section">
                    <div class="panel">
                      <div class="panel-heading">
                        <h3 class="panel-title pl-20">Information</h3>
                      </div>
                      <div class="panel-body p-20">
                          <div class="mb-20">
                            <div class="grey-500">Stutus</div>
                            <span>Public</span>
                          </div>
                          <div class="mb-20">
                            <div class="grey-500">Created</div>
                            <span>May 5 at 11:00 AM</span>
                          </div>
                          <div class="mb-20">
                            <div class="grey-500">Created by</div>
                            <span><img style="width:20px;" class="avatar" src="../../global/portraits/1.jpg" title="Herman Beck"> Herman Beck
                            </span>
                          </div>
                          <div class="mb-20">
                            <div class="grey-500">Last Updated</div>
                            <span>May 15 at 11:00 AM</span>
                          </div>
                          <div class="mb-20">
                            <div class="grey-500">Update by</div>
                            <span><img style="width:20px;" class="avatar" src="../../global/portraits/1.jpg" title="Herman Beck"> Herman Beck
                            </span>
                          </div>

                          <div class="mb-20">
                            <div class="grey-500">Shaered</div>
                            <ul class="addMember-items pb-20">
                              <?php for($aa=1;$aa<=rand(3,5);$aa++){?>
                              <li class="addMember-item mr-0">
                                <img style="width:20px;" class="avatar" src="../../global/portraits/<?php echo rand(1,20);?>.jpg" title="Herman Beck">
                              </li>
                              <?php }?>
                            </ul>
                          </div>

                      </div>
                      
                    </div>
                    
                    <div class="panel">
                      <div class="panel-heading">
                        <h3 class="panel-title pl-20">Tags</h3>
                      </div>
                      <div class="panel-body p-20">
                          <span class="badge badge-round badge-warning">Primary</span>
                        <span class="badge badge-round badge-warning">Success</span>
                        <span class="badge badge-round badge-warning">Warning</span>
                      </div>
                    </div>

                    <div class="panel">
                      <div class="panel-heading">
                        <h3 class="panel-title pl-20">Activities</h3>
                      </div>
                      <div class="panel-body p-20">
                          <ul class="timeline timeline-single">
                            <li class="timeline-item mb-30 pl-20">
                              <div class="timeline-dot" rip-style-bordercolor-backup="" style="" rip-style-borderstyle-backup="" rip-style-borderwidth-backup=""></div>
                              <div class="timeline-content">
                                <div class="media">
                                  <div class="pr-10">
                                    <a class="avatar avatar-sm" href="javascript:void(0)">
                                      <img class="img-fluid" src="../../../global/portraits/5.jpg" alt="...">
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <h5 class="mt-0 mb-0">June Lane</h5>
                                    <small>16 minutes ago</small>
                                    <div class="profile-brief">
                                      Update Status
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>
                            <li class="timeline-item mb-30 pl-20">
                              <div class="timeline-dot"></div>
                              <div class="timeline-content">
                                <div class="media">
                                  <div class="pr-10">
                                    <a class="avatar avatar-sm" href="javascript:void(0)">
                                      <img class="img-fluid" src="../../../global/portraits/5.jpg" alt="...">
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <h5 class="mt-0 mb-0">June Lane</h5>
                                    <small>16 minutes ago</small>
                                    <div class="profile-brief">
                                      Edit Content
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </li>
                          </ul>
                      </div>
                    </div>
                  </section>

                </div>
              </div>
            </div>
          </div>
        
          <div class="page-main">
            <div class="panel">
              <div class="panel-body container-fluid">
                <h1>Impact of new product on sales volume</h1>
                

                <div class="mb-50 mt-20"  id="chart_annualsales" style="width: 100%; height: 550px;"></div>

                <section>
                  <h3>Documents</h3>
                  <table class="table table-hover example-wrap">
                    <thead>
                      <tr>
                        <th>Name</th>
                        <th width="20%">Download</th>
                        <th class="hidden-sm-down" width="20%">File size</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          <div class="media" style="flex-direction:initial;">
                            <div class="pr-20">
                              <a data-url="panel-file.html" data-toggle="slidePanel" class="font-size-24" href="javascript:void(0)">
                                <i style="color:#ff9800" class="icon fa-file-pdf-o" aria-hidden="true"></i>
                              </a>
                            </div>
                            <div class="media-body pt-10">
                              <h5><a style="color:#616161;text-decoration: none;" data-url="panel-folder.html" data-toggle="slidePanel" href="javascript:void(0)">Business Development & B2B Sales</a></h5>
                              <p class="hidden-md-up mb-0 grey-500">
                                <small>
                                  Mar 3, 2017 <span class="ml-10 mr-10">&mdash;</span> 2.85 MB                            </small>
                              </p>
                            </div>
                          </div>
                        </td>
                        <td class="pt-15">12</td>
                        <td class="hidden-sm-down pt-15">2.85 MB</td>
                      </tr>
                      </tbody>
                  </table>
                </section>

                <ul class="wall-attrs clearfix p-0 m-0 mt-10">
                  <li class="attrs-meta float-left font-size-16">
                    <a href="javascript:void(0)" onclick="$('#wall-comment-reply-1').toggle();">
                      <i class="icon md-comment"></i>
                      <span>2</span>
                    </a>
                  </li>
                  <li class="float-right">
                    <div class="btn-group bootstrap-select btn-comment-post">
                      <select data-plugin="selectpicker">
                        <option>Top Comments</option>
                        <option>Newest</option>
                        <option>Oldest</option>
                      </select>
                    </div>
                  </li>
                </ul>
              </div>
            </div>

            <div class="panel">
            <div class="panel-body container-fluid">
              <h3>Comments</h3>
              <div class="wall-comment-attrs">
                <div id="wall-comment-reply-1" class="wall-comment-reply clearfix" style="display: none">
                  <div href="#" class="avatar avatar-md float-left">
                    <img src="../../../global/portraits/6.jpg">
                  </div>
                  <div class="ml-60 wall-comment-form">
                    <textarea rows="1" class="autoHeight" placeholder="Write a comment..."></textarea>
                    <div class="wall-comment-reply-attach">
                      <button class="btn btn-icon btn-dark btn-round bg-grey-400">
                        <i class="icon md-attachment-alt rotate" aria-hidden="true"></i>
                      </button>
                    </div>
                  </div>
                </div>
                <div class="wall-comment">
                  <div class="wall-comment-action">
                  <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                    <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                  </button>
                  
                  <div class="dropdown-menu dropdown-menu-right" role="menu">
                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                      Edit Comment
                    </a>
                    <a class="dropdown-item text-danger" href="javascript:void(0)" role="menuitem">
                      Delete
                    </a>
                  </div>
                </div>
                  <a href="#" class="avatar avatar-md float-left">
                    <img src="../../../global/portraits/3.jpg">
                  </a>
                  <div class="ml-60 box-post-comment">
                    <a href="#">Stacey Hunt</a>
                    <span class="ml-10">Do incididunt elit ex incididunt ut. Aute velit proident cupidatat qui et consectetur
                      do. Consequat esse quis ut cupidatat ea. Sint dolore ea culpa
                      dolore velit enim.</span>
                      <p class="font-size-12 mt-5 grey-600">
                        <a href="javascript:void(0)">
                          Like
                        </a>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <a href="javascript:void(0)" class="action-reply">
                          Reply
                        </a>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <span >
                          <i class="icon md-favorite"></i>
                          <span>5</span>
                        </span>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <span>
                          30th July 2017
                        </span>
                      </p>

                    <div class="wall-comment">
                      <a href="#" class="avatar avatar-md float-left">
                        <img src="../../../global/portraits/11.jpg">
                      </a>
                      <div class="ml-60">
                        <a href="#">Crystal Bates</a>
                        <span class="ml-10">Do incididunt elit ex incididunt ut. Aute velit proident cupidatat qui et consectetur
                          do. Consequat esse quis ut cupidatat ea.</span>
                        <p class="font-size-12 mt-5 grey-500">
                          <a href="javascript:void(0)">
                            Like
                          </a>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <a href="javascript:void(0)" class="action-reply">
                            Reply
                          </a>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <span >
                            <i class="icon md-favorite"></i>
                            <span>5</span>
                          </span>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <span>
                            30th July 2017
                          </span>
                        </p>
                      </div>
                      
                    </div>
                    <div class="display-reply"></div>

                  </div>
                  
                </div>
                <div class="wall-comment">
                  <a href="#" class="avatar avatar-md float-left">
                    <img src="../../../global/portraits/5.jpg">
                  </a>
                  <div class="ml-60 box-post-comment">
                    <a href="#">Sam Anderson</a>
                    <span class="ml-10">Ut velit ipsum elit ut mollit elit. Proident officia eu ex consectetur ullamco
                      magna. Enim cillum voluptate sint ipsum ad voluptate exercitation.
                      Ex est amet magna occaecat eu.</span>
                    <p class="font-size-12 mt-5 grey-500">
                      <a href="javascript:void(0)">
                        Like
                      </a>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <a href="javascript:void(0)" class="action-reply">
                        Reply
                      </a>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <span >
                        <i class="icon md-favorite"></i>
                        <span>5</span>
                      </span>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <span>
                        30th July 2017
                      </span>
                    </p>

                    <div class="display-reply"></div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>

          <div class="panel">
            <div class="panel-body container-fluid">
              <h3>See Also</h3>
              <div id="home-announcement" class="row">
                    <?php

                    for($a=1;$a<=4;$a++){?>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-6 <?=($a>2?"hidden-sm-down":"")?> <?=($a>3?"hidden-lg-down":"")?>  mt-20">
                      <div class="relate-img-cover">
                        <img width="100%" src="img/chart-<?=rand(1,44)?>.jpg">  
                      </div>
                      <h4 class="card-title mb-0 mt-10">
                        <a href="#" onclick="javascript:void(0);">Impact of new product on sales volume</a>
                      </h4>
                      <p class="card-text type-link">
                        <small>
                          By
                          <a href="javascript:void(0)">Nathan Watts</a>
                          <a href="javascript:void(0)">Mar 05, 2017</a>
                        </small>
                      </p>
                      <section>
                        <h6 class="font-size-12 mb-5 mt-20">Shared</h6>
                        <ul class="addMember-items pb-20">
                          <?php for($aa=1;$aa<=rand(3,5);$aa++){?>
                          <li class="addMember-item mr-0">
                            <img style="width:20px;" class="avatar" src="../../global/portraits/<?php echo rand(1,20);?>.jpg" title="Herman Beck">
                          </li>
                          <?php }?>
                        </ul>
                      </section>
                        <div>
                        <span class="badge badge-round badge-warning">Sales</span>
                      </div>
                      <p class="card-text type-link pb-10">
                        <small>
                          Lasted update Mar 05, 2017
                        </small>
                      </p>
                      <div class="clearfix">
                        <div class="card-actions float-left font-size-12 mt-5">
                          <a href="javascript:void(0)">
                            <i class="icon md-comment"></i>
                            <span>115</span>
                          </a>
                          <i class="icon md-attachment-alt grey-500" aria-hidden="true"></i> <span class="grey-500">2</span>
                        </div>
                      </div>
                    </div>
                    <?php }?>
                  </div>
              
            </div>
            

          </div>
            
            
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/imagesloaded/imagesloaded.pkgd.js"></script>
  <script src="../../global/vendor/masonry/masonry.pkgd.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/masonry.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/vendor/amcharts/amcharts.js" type="text/javascript"></script>
  <script src="../../global/vendor/amcharts/serial.js" type="text/javascript"></script>

  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
      function chooseDateRange(start, end) {
          var DrangeValue = start.format('MM/DD/YYYY') + '-' + end.format('MM/DD/YYYY');
          $('#SearchDateRange span#daterange-value').html(DrangeValue);
          var dataKey = "123";
          var dataValue = "xxx";
          var stoken = '';
            $('.search-wrapper .currently-showing .chooseDateRange').remove();
            stoken = '<div class="token chooseDateRange" data-key="'+dataKey+'" data-value="' + dataValue + '"><span class="token-label" >' + DrangeValue + '</span><a  href="#" class="close" tabindex="-1">×</a></div>';
            $('.search-wrapper .currently-showing').append(stoken);
            sysGenUrlSearch();
      }

      $('#SearchDateRange').daterangepicker({
          "autoApply": true,
          "opens": "center",
          ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
      }, chooseDateRange);
    });
  })(document, window, jQuery);
  </script>
  <script type="text/javascript">
    var _annualsales;
    var chartData_annualsales = [
        {
            "year": 2009,
            "income": 23.5,
            "expenses": 22.1
        },
        {
            "year": 2010,
            "income": 26.2,
            "expenses": 22.8
        },
        {
            "year": 2011,
            "income": 23.5,
            "expenses": 24.1
        },
        {
            "year": 2012,
            "income": 29.5,
            "expenses": 25.1
        },
        {
            "year": 2013,
            "income": 23.5,
            "expenses": 25.1
        },
        {
            "year": 2014,
            "income": 26.2,
            "expenses": 25.8
        },
        {
            "year": 2015,
            "income": 30.1,
            "expenses": 27.9
        },
        {
            "year": 2016,
            "income": 29.5,
            "expenses": 28.1
        },
        {
            "year": 2017,
            "income": 30.6,
            "expenses": 28.2,
            "dashLengthLine": 5
        },
        {
            "year": 2018,
            "income": 34.1,
            "expenses": 29.9,
            "dashLengthColumn": 5,
            "alpha":0.2,
            "additional":"(forcash)"
        }

    ];


    AmCharts.ready(function () {
        // SERIAL CHART
        _annualsales = new AmCharts.AmSerialChart();

        _annualsales.dataProvider = chartData_annualsales;
        _annualsales.categoryField = "year";
        _annualsales.startDuration = 1;

        // AXES
        // category
        var categoryAxis_annualsales = _annualsales.categoryAxis;
        categoryAxis_annualsales.gridPosition = "start";

        // value
        var valueAxis_annualsales = new AmCharts.ValueAxis();
        valueAxis_annualsales.axisAlpha = 0;
        _annualsales.addValueAxis(valueAxis_annualsales);

        // GRAPHS
        // column graph
        var graph1_annualsales = new AmCharts.AmGraph();
        graph1_annualsales.type = "column";
        graph1_annualsales.title = "Sales";
        graph1_annualsales.lineColor = "#a668d5";
        graph1_annualsales.valueField = "income";
        graph1_annualsales.lineAlpha = 1;
        graph1_annualsales.fillAlphas = 1;
        graph1_annualsales.dashLengthField = "dashLengthColumn";
        graph1_annualsales.alphaField = "alpha";
        graph1_annualsales.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
        _annualsales.addGraph(graph1_annualsales);

        // line
        var graph2_annualsales = new AmCharts.AmGraph();
        graph2_annualsales.type = "line";
        graph2_annualsales.title = "Goal";
        graph2_annualsales.lineColor = "#fcd202";
        graph2_annualsales.valueField = "expenses";
        graph2_annualsales.lineThickness = 3;
        graph2_annualsales.bullet = "round";
        graph2_annualsales.bulletBorderThickness = 3;
        graph2_annualsales.bulletBorderColor = "#fcd202";
        graph2_annualsales.bulletBorderAlpha = 1;
        graph2_annualsales.bulletColor = "#ffffff";
        graph2_annualsales.dashLengthField = "dashLengthLine";
        graph2_annualsales.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
        _annualsales.addGraph(graph2_annualsales);

        // LEGEND
        var legend_annualsales = new AmCharts.AmLegend();
        legend_annualsales.useGraphSettings = true;
        _annualsales.addLegend(legend_annualsales);

        // WRITE
        _annualsales.write("chart_annualsales");
    });
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>