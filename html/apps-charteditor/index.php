<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-daterangepicker/daterangepicker.css" />
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition  page-aside-right">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-10">Chart Editor</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../home/">Home</a></li>
        <li class="breadcrumb-item active">Chart Editor</li>
      </ol>
      <div class="page-header-actions">
        <button type="button" onClick="location.href='setting.php';" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Setting">
          <i class="icon md-settings" aria-hidden="true"></i>
        </button>
      </div>
    </div>

    <div class="page-content container-fluid" style="position: relative;">

      <div class="row ml-0 mr-0">
        <div class="col-md-12">
          <div class="page-aside">
            <!-- Contacts Sidebar -->
            <div class="page-aside-switch">
              <i class="icon md-chevron-left" aria-hidden="true"></i>
              <i class="icon md-chevron-right" aria-hidden="true"></i>
            </div>
            <div class="page-aside-inner page-aside-scroll">
              <div data-role="container">
                <div data-role="content">
                  <section class="page-aside-section">

                    <div class="card card-block pt-0">
                      <h3 class="mt-0">5 Latest Update</h3>
                        <div class="pt-20">
                          <ul class="list-group">
                          <?php
                            for($ab=0;$ab<5;$ab++){
                            ?>
                            <li class="list-group-item px-0 py-10">
                              <div class="row">
                                <div class="col-md-4">
                                  <a href="detail.php">
                                   <img width="100%" src="img/chart-<?=rand(1,44)?>.jpg">  
                                  </a>
                                </div>
                                <div class="col-md-8 pl-0">
                                  <a href="detail.php">
                                    <h6 class="mt-0 mb-0" style="word-wrap: break-word;">Impact of new product on sales volume</h6>
                                  </a>
                                  <small>Mar 05, 2017</small>
                                </div>
                              </div>
                              
                            </li>
                          <?php }?>
                          </ul>
                        </div>
                    </div>

                    <div class="card card-block pt-0">
                      <h3 class="mt-0">5 Latest Comment</h3>
                        <div class="pt-20">
                          <ul class="list-group">
                          <?php
                            for($ab=0;$ab<5;$ab++){
                            ?>
                            <li class="list-group-item px-0 py-10">
                              <div class="row">
                                <div class="col-md-4">
                                  <a href="detail.php">
                                   <img width="100%" src="img/chart-<?=rand(1,44)?>.jpg">  
                                  </a>
                                </div>
                                <div class="col-md-8 pl-0">
                                  <a href="detail.php">
                                    <h6 class="mt-0 mb-0" style="word-wrap: break-word;">Impact of new product on sales volume</h6>
                                  </a>
                                  <small>Mar 05, 2017</small>
                                </div>
                              </div>
                              
                            </li>
                          <?php }?>
                          </ul>
                        </div>
                    </div>
                    



                  </section>
                </div>
              </div>
            </div>
          </div>
          <div class="page-main ">
          <section>
            <div class="pb-10">
              <div class="search-wrapper">
                <div class="search-box">
                  <div class="icon md-search"></div>
                  <div class="currently-showing">
                    <?=$search['tokenhtml']?>
                  </div>
                </div>
                <a href="javascript:void(0);" class="clear_all btn btn-flat btn-default btn-block waves-effect waves-classic">Clear All</a>
                <div class="data_entry">
                  <input class="input keyword-input" placeholder="Enter a keyword" type="text">
                  <div class="icon md-close-circle close"></div>
                </div>
                <div class="filters">
                    <div class="dropdown filter_permission">
                      <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Filter
                        <span class="icon md-chevron-down" aria-hidden="true"></span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)">Owned by me</a>
                        <a class="dropdown-item" href="javascript:void(0)">Not owned by me</a>
                        <a class="dropdown-item" href="javascript:void(0)">Shared with me</a>
                      </div>
                    </div>
                    <div class="dropdown filter_permission">
                      <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Category
                        <span class="icon md-chevron-down" aria-hidden="true"></span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)">Category 1</a>
                        <a class="dropdown-item" href="javascript:void(0)">Category 2</a>
                        <a class="dropdown-item" href="javascript:void(0)">Category 3</a>
                      </div>
                    </div>
                    <div class="dropdown filter_permission">
                      <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Status
                        <span class="icon md-chevron-down" aria-hidden="true"></span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)"> Enable</a>
                        <a class="dropdown-item" href="javascript:void(0)"> Disable</a>
                      </div>
                    </div>
                    <div class="dropdown filter_permission">
                      <button type="button" id="SearchDateRange" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Time <i class="icon md-chevron-down" aria-hidden="true"></i>
                        <span id="daterange-value"></span>
                      </button>
                    </div>

                </div>
              </div>
            </div>
            <div class="pb-20">
                <div class="actions-inner float-right">
                  <div class="dropdown">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      <i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Name
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-media w-100" role="menu">
                      <h6 class="pl-10" aria-hidden="false">Sory by</h6>
                      <a class="dropdown-item active" href="javascript:void(0)">Name</a>
                      <a class="dropdown-item" href="javascript:void(0)">Date Created</a>
                      <a class="dropdown-item" href="javascript:void(0)">Last Update</a>
                      <a class="dropdown-item" href="javascript:void(0)">Comment</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-sort-amount-asc" aria-hidden="true"></i> Ascending</a>
                      <a class="dropdown-item active" href="javascript:void(0)"><i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Descending</a>
                    </div>
                  </div>
                </div>


                <div class="pt-10">About <strong>26</strong> results</div>
              </div>
            </section>
          <section class="">
            
            <div class="row">
              <?php
              for($ab=1;$ab<=20;$ab++){
                $index = rand(0,8);
              ?>
              <div class="col-md-4">
                <div class="card card-shadow card-bordered">
                  <div class="bg-grey-100 p-5 clearfix text-center" style="padding-bottom: 0px;">
                    <a href="detail.php">
                      <img width="100%" src="img/chart-<?=rand(1,44)?>.jpg">
                    </a>
                    <div class="wall-action" style="top:0;">
                      <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                        <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                      </button>
                      <div class="dropdown-menu dropdown-menu-right" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                          Enable
                        </a>
                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                          Disable
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                          Edit
                        </a>
                        <a class="dropdown-item text-danger" href="javascript:void(0)" role="menuitem">
                          Delete
                        </a>
                      </div>
                    </div>
                  </div>
                  <div class="card-block" style="padding: 10px 20px;">
                    <a href="detail.php">
                      <h4 class="card-title">Impact of new product on sales volume</h4>
                    </a>
                    <p class="card-text type-link">
                      <small>
                        By
                        <a href="javascript:void(0)">Nathan Watts</a>
                        <a href="javascript:void(0)">Mar 05, 2017</a>
                      </small>
                    </p>
                    <section>
                      <h6 class="font-size-12 mb-5 mt-20">Shared</h6>
                      <ul class="addMember-items pb-20">
                        <?php for($aa=1;$aa<=rand(3,5);$aa++){?>
                        <li class="addMember-item mr-0">
                          <img style="width:20px;" class="avatar" src="../../global/portraits/<?php echo rand(1,20);?>.jpg" title="Herman Beck">
                        </li>
                        <?php }?>
                      </ul>
                    </section>
                    <div>
                      <span class="badge badge-round badge-warning">Sales</span>
                    </div>
                    <p class="card-text type-link pb-10">
                      <small>
                        Lasted update Mar 05, 2017
                      </small>
                    </p>

                  </div>
                  <div class="card-block clearfix">
                  <div class="card-actions float-left font-size-16 mt-5">
                    <a href="javascript:void(0)">
                      <i class="icon md-comment"></i>
                      <span>115</span>
                    </a>
                    <i class="icon md-attachment-alt grey-500" aria-hidden="true"></i> <span class="grey-500">2</span>
                  </div>
                </div>
                </div>
              </div>
              <?php }?>

            </div>
          </section>
            </div>
        </div>
      </div>

    </div>
  </div>
  <div class="site-action" data-plugin="actionBtn">
    <button type="button" data-target="#chartForm1" data-toggle="modal" class="btn-raised btn btn-success btn-floating">
      <i class="front-icon md-plus" aria-hidden="true"></i>
    </button>
  </div>
  <div class="modal fade" id="chartForm1" aria-hidden="true" aria-labelledby="chartForm1" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple ">
        <form class="modal-content modal-content-no-border">
            <div class="modal-header">
              <h4 class="modal-title" id="exampleFillInModalTitle">Chart Type</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="icon md-close" aria-hidden="true"></i>
              </button>
            </div>
            <div class="modal-body">
                <div class="p-0">
                 <div class="example-wrap mb-0">
                  <div class="nav-tabs-vertical" data-plugin="tabs">
                    <ul class="nav nav-tabs mr-25" role="tablist">
                      <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" href="#Column" aria-controls="Column" role="tab" aria-expanded="true">Column</a></li>
                      <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#Bar" aria-controls="Bar" role="tab" aria-expanded="false">Bar</a></li>
                      <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#Line" aria-controls="Line" role="tab" aria-expanded="false">Line</a></li>
                      <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#Area" aria-controls="Area" role="tab" aria-expanded="false">Area</a></li>
                      <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#Pie" aria-controls="Pie" role="tab" aria-expanded="false">Pie&Donut</a></li>
                      <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#Other" aria-controls="Other" role="tab" aria-expanded="false">Other chart types</a></li>
                    </ul>
                    <?php

                    $_column_name  = array("clustered","stacked","100% stacked","clustered and stacked","3D clustered","3D stacked","3D 100% stacked","3D column","column and line");
                    $_column_pic  = array("clustered.png","stacked.png","100-percent-stacked.png","clustered-and-stacked.png","3D-clustered.png","3D-stacked.png","3D-100-percent-stacked.png","3D-column.png","column-and-line.png");

                    $_bar_name = array("clustered","stacked","100% stacked","clustered and stacked","3D clustered","3D stacked","3D 100% stacked","3D bar","bar and line");
                    $_bar_pic  = array("clustered.png","stacked.png","100-percent-stacked.png","clustered-and-stacked.png","3D-clustered.png","3D-stacked.png","3D-100-percent-stacked.png","3D-bar.png","bar-and-line.png");

                    $_line_name = array("line","stacked line","100% stacked line","rotated line","smoothed line","step line");
                    $_line_pic  = array("line.png","stacked-line.png","100-percent-stacked-line.png","rotated-line.png","smoothed-line.png","step-line.png");

                    $_area_name = array("area","stacked area","100% stacked area","rotated area");
                    $_area_pic  = array("area.png","stacked-area.png","100-percent-stacked-area.png","rotated-area.png");

                    $_pie_name = array("pie","3D pie","donut","3D donut");
                    $_pie_pic  = array("pie.png","3D-pie.png","donut.png","3D-donut.png");

                    $_other_name = array("angular gauge","funnel","pyramid","radar","polar");
                    $_other_pic  = array("angular-gauge.png","funnel.png","pyramid.png","radar.png","polar.png");
                    ?>
                    <div class="tab-content py-15">
                      <div class="tab-pane active" id="Column" role="tabpanel" aria-expanded="true">
                        <div class="row">
                        <?php 
                        for($ab=0;$ab<count($_column_name);$ab++){
                        ?>
                        <a href="form.php" class="list-group-item col-md-4 text-center pb-10">
                          <img width="100%" src="img/column/<?=$_column_pic[$ab]?>">
                          <small class="grey-500"><?=$_column_name[$ab]?></small>
                        </a>
                        <?php }?>
                        </div>
                      </div>
                      <div class="tab-pane" id="Bar" role="tabpanel" aria-expanded="false">
                        <div class="row">
                        <?php 
                        for($ab=0;$ab<count($_bar_name);$ab++){
                        ?>
                        <a href="form.php" class="list-group-item col-md-4 text-center pb-10">
                          <img width="100%" src="img/bar/<?=$_bar_pic[$ab]?>">
                          <small class="grey-500"><?=$_bar_name[$ab]?></small>
                        </a>
                        <?php }?>
                        </div>
                      </div>
                      <div class="tab-pane" id="Line" role="tabpanel" aria-expanded="false">
                        <div class="row">
                        <?php 
                        for($ab=0;$ab<count($_line_name);$ab++){
                        ?>
                        <a href="form.php" class="list-group-item col-md-4 text-center pb-10">
                          <img width="100%" src="img/line/<?=$_line_pic[$ab]?>">
                          <small class="grey-500"><?=$_line_name[$ab]?></small>
                        </a>
                        <?php }?>
                        </div>
                      </div>
                      <div class="tab-pane" id="Area" role="tabpanel" aria-expanded="false">
                        <div class="row">
                          <?php 
                          for($ab=0;$ab<count($_area_name);$ab++){
                          ?>
                          <a href="form.php" class="list-group-item col-md-4 text-center pb-10">
                            <img width="100%" src="img/area/<?=$_area_pic[$ab]?>">
                            <small class="grey-500"><?=$_area_name[$ab]?></small>
                          </a>
                          <?php }?>
                        </div>
                      </div>
                      <div class="tab-pane" id="Pie" role="tabpanel" aria-expanded="false">
                        <div class="row">
                          <?php 
                          for($ab=0;$ab<count($_pie_name);$ab++){
                          ?>
                          <a href="form.php" class="list-group-item col-md-4 text-center pb-10">
                            <img width="100%" src="img/pie/<?=$_pie_pic[$ab]?>">
                            <small class="grey-500"><?=$_pie_name[$ab]?></small>
                          </a>
                          <?php }?>
                        </div>
                      </div>
                      <div class="tab-pane" id="Other" role="tabpanel" aria-expanded="false">
                        <div class="row">
                          <?php 
                          for($ab=0;$ab<count($_other_name);$ab++){
                          ?>
                          <a href="form.php" class="list-group-item col-md-4 text-center pb-10">
                              <img width="100%" src="img/other/<?=$_other_pic[$ab]?>">
                              <small class="grey-500"><?=$_other_name[$ab]?></small>
                          </a>
                          <?php }?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                  
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default btn-pure waves-effect waves-classic" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../assets/js/BaseApp.js"></script>
  <script src="../../assets/js/App/Contacts.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
      AppContacts.run();

      function chooseDateRange(start, end) {
          var DrangeValue = start.format('MM/DD/YYYY') + '-' + end.format('MM/DD/YYYY');
          $('#SearchDateRange span#daterange-value').html(DrangeValue);
          var dataKey = "123";
          var dataValue = "xxx";
          var stoken = '';
            $('.search-wrapper .currently-showing .chooseDateRange').remove();
            stoken = '<div class="token chooseDateRange" data-key="'+dataKey+'" data-value="' + dataValue + '"><span class="token-label" >' + DrangeValue + '</span><a  href="#" class="close" tabindex="-1">×</a></div>';
            $('.search-wrapper .currently-showing').append(stoken);
            sysGenUrlSearch();
      }

      $('#SearchDateRange').daterangepicker({
          "autoApply": true,
          "opens": "center",
          ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
      }, chooseDateRange);

    });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>
