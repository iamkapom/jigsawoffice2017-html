<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/summernote/summernote.css">
  <link rel="stylesheet" href="../../global/vendor/blueimp-file-upload/jquery.fileupload.css">
  <link rel="stylesheet" href="../../global/vendor/dropify/dropify.css">
  <link rel="stylesheet" href="../../global/vendor/editable-table/editable-table.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
        <link rel="stylesheet" href="../../../global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
  <style type="text/css">
    .table td, .table th{ vertical-align: middle; }
    tr.hide{
      display: none;
    }
  </style>
</head>
<body class="animsition page-aside-right">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-10">Chart Editor</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../home/">Home</a></li>
        <li class="breadcrumb-item active">Chart Editor</li>
      </ol>
    </div>
    
    <div class="page-content container-fluid" style="position: relative;">
      
      <div class="row ml-0 mr-0">
        <div class="col-md-12">
          <div class="page-aside" style="none;border-right:0;">
        <!-- Contacts Sidebar -->
        <div class="page-aside-switch">
          <i class="icon md-chevron-left" aria-hidden="true"></i>
          <i class="icon md-chevron-right" aria-hidden="true"></i>
        </div>
        <div class="page-aside-inner page-aside-scroll">
          <div data-role="container">
            <div data-role="content">
              <section class="page-aside-section">
                <div class="panel" style="margin-top: -20px;">
                  <div class="panel-heading">
                    <h3 class="panel-title pl-20">Public</h3>
                  </div>
                  <div class="panel-body p-20">
                      <button type="button" class="btn active waves-effect waves-classic ml-10">Save Draft</button>
                      <button type="button" class="btn active waves-effect waves-classic ml-10">Preview</button>
                      <p class="mt-15">
                        <span class="w-60" style="display:inline-block;">Status: </span><strong>Draft</strong> <a class="pl-10" href="#">Edit</a>
                      </p>
                  </div>
                  <div class="panel-footer pl-20">
                    <a class="text-danger btn btn-pure" href="#">Delete</a>
                    <button style="float:right" type="button" class="btn btn-info active waves-effect waves-classic ml-10">Public</button>
                    <div class="clearfix"></div>
                  </div>
                </div>

                <div class="panel">
                  <div class="panel-heading">
                    <h3 class="panel-title pl-20">Categories</h3>
                  </div>
                  <div class="panel-body p-20">
                    <?php for($ax=1;$ax<=6;$ax++){?>
                      <div class="checkbox-custom checkbox-primary">
                        <input type="checkbox" id="inputUnchecked<?php echo $ax;?>" />
                        <label for="inputUnchecked<?php echo $ax;?>">Categories <?php echo $ax;?></label>
                      </div>
                    <?php }?>
                      
                  </div>
                  <div class="panel-footer pl-20">
                    <a class="pl-10" href="#">+ Add New Category</a>
                  </div>
                </div>

                <div class="panel">
                  <div class="panel-heading">
                    <h3 class="panel-title pl-20">Tags</h3>
                  </div>
                  <div class="panel-body p-20">
                      <input type="text" class="form-control" data-plugin="tokenfield" value="Primary,Success,blue"/>
                  </div>
                </div>

                <div class="panel">
                  <div class="panel-heading">
                    <h3 class="panel-title pl-20">Activities</h3>
                  </div>
                  <div class="panel-body p-20">
                      <ul class="timeline timeline-single">
                        <li class="timeline-item mb-30 pl-20">
                          <div class="timeline-dot" rip-style-bordercolor-backup="" style="" rip-style-borderstyle-backup="" rip-style-borderwidth-backup=""></div>
                          <div class="timeline-content">
                            <div class="media">
                              <div class="pr-10">
                                <a class="avatar avatar-sm" href="javascript:void(0)">
                                  <img class="img-fluid" src="../../../global/portraits/5.jpg" alt="...">
                                </a>
                              </div>
                              <div class="media-body">
                                <h5 class="mt-0 mb-0">June Lane</h5>
                                <small>16 minutes ago</small>
                                <div class="profile-brief">
                                  Update Status
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                        <li class="timeline-item mb-30 pl-20">
                          <div class="timeline-dot"></div>
                          <div class="timeline-content">
                            <div class="media">
                              <div class="pr-10">
                                <a class="avatar avatar-sm" href="javascript:void(0)">
                                  <img class="img-fluid" src="../../../global/portraits/5.jpg" alt="...">
                                </a>
                              </div>
                              <div class="media-body">
                                <h5 class="mt-0 mb-0">June Lane</h5>
                                <small>16 minutes ago</small>
                                <div class="profile-brief">
                                  Edit Content
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                      </ul>
                  </div>
                </div>
              </section>
              
            </div>
          </div>
        </div>
      </div>
          <div class="page-main">
          <div class="panel">
            <div class="panel-body container-fluid">
              <div class="row">
                <div class="col-md-12">
                  <!-- Example Basic Form -->
                  <div class="example-wrap">
                    <div class="example">
                      <form autocomplete="off">
                        <div class="row">
                          <div class="form-group col-md-12">
                            <input class="form-control form-control-lg" id="inputBasicFirstName" name="inputFirstName" value="What Is SEO / Search Engine Optimization?" placeholder="Enter title here " autocomplete="off" type="text">
                          </div>
                        </div>
                        <div class="form-group form-material">
                          <div class="mb-50 mt-20"  id="chart_annualsales" style="width: 100%; height: 350px;"></div>
                        </div>
                        <div class="form-group">
                          <div class="mb-10">
                            <button type="button" id="btnaddrow" class="btn btn-primary"><i class="icon fa-reorder" aria-hidden="true"></i> Add Row</button>
                            <button type="button" id="btnaddcolumn" class="btn btn-primary ml-10"><i class="icon fa-reorder" aria-hidden="true" style="-ms-transform: rotate(90deg); -webkit-transform: rotate(90deg); transform: rotate(90deg);"></i> Add Column</button>
                          </div>
                          <table class="table table-striped table-bordered" id="editableTable">
                            <thead>
                              <tr>
                                <th width="5%">&nbsp;</th>
                                <th width="30%">Category</th>
                                <th contenteditable="true">Sales</th>
                                <th contenteditable="true">Goal</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr class="hide">
                                <td><button type="button" class="table-remove btn text-danger btn-pure btn-default icon md-close"></button></td>
                                <td contenteditable="true">Category</td>
                                <td contenteditable="true">0</td>
                                <td contenteditable="true">0</td>
                              </tr>
                              <tr>
                                <td>&nbsp;</td>
                                <td contenteditable="true">2016</td>
                                <td contenteditable="true">29.5</td>
                                <td contenteditable="true">28.1</td>
                              </tr>
                              <tr>
                                <td><button type="button" class="table-remove text-danger btn btn-pure btn-default icon md-close"></button></td>
                                <td contenteditable="true">2017</td>
                                <td contenteditable="true">30.6</td>
                                <td contenteditable="true">28.2</td>
                              </tr>
                              
                            </tbody>
                          </table>
                        </div>
                        
                        <div class="form-group pt-15">
                            <h5>Share to ... (Can edit)</h5>
                            <select id="selectTeam4" class="form-control" multiple style="width:100%">
                            <?php
                            $teams_arr[] = "Ada.Hoppe";
                            $teams_arr[] = "Adrianna_Durgan";
                            $teams_arr[] = "Albin.Kreiger";
                            $teams_arr[] = "Alisa";
                            $teams_arr[] = "August";
                            $teams_arr[] = "Bell.Mueller";
                            $teams_arr[] = "Bret";
                            $teams_arr[] = "Ceasara_Orn";
                            $teams_arr[] = "Chester";
                            $teams_arr[] = "Citlalli_Wehner";
                            $teams_arr[] = "Clementina";
                            $teams_arr[] = "Coby";
                            $teams_arr[] = "Colin";
                            $teams_arr[] = "Damon";
                            $teams_arr[] = "Davin";
                            $teams_arr[] = "Elliott_Becker";
                            $teams_arr[] = "Emerson";
                            $teams_arr[] = "Gerhard";
                            $teams_arr[] = "Gunnar";
                            $teams_arr[] = "Gunner_Jakubowski";
                            $teams_arr[] = "Heath.Ryan";
                            $teams_arr[] = "Herta";
                            $teams_arr[] = "Hubert";
                            $teams_arr[] = "Jarvis.Simonis";
                            $teams_arr[] = "Jennie";
                            $teams_arr[] = "Johanna.Thiel";
                            $teams_arr[] = "Johnathan_Mraz";
                            $teams_arr[] = "Josephine";
                            $teams_arr[] = "Lacey";
                            $teams_arr[] = "Marjorie.Orn";
                            $teams_arr[] = "Mckenna.Herman";
                            $teams_arr[] = "Melany_Gerhold";
                            $teams_arr[] = "Miracle";
                            $teams_arr[] = "Monica";
                            $teams_arr[] = "Monique_Whitea";
                            $teams_arr[] = "Myriam_Nicolas";
                            $teams_arr[] = "Myrtie.Gerhold";
                            $teams_arr[] = "Raina";
                            $teams_arr[] = "Ruben.Reilly";
                            $teams_arr[] = "Sammie";
                            $teams_arr[] = "Shanel";
                            $teams_arr[] = "Stone_Deckow";
                            $teams_arr[] = "Terrance.Borer";
                            $teams_arr[] = "Thea";
                            $teams_arr[] = "Torrey";
                            $teams_arr[] = "Treva";
                            $teams_arr[] = "Wilhelmine";
                            $teams_arr[] = "Yasmine";
                            
                            for($bb=0;$bb<count($teams_arr);$bb++){
                              echo '<option value="'.$teams_arr[$bb].'">'.$teams_arr[$bb].'</option>';
                            }
                            ?>
                            </select>
                          </div>

                          <div class="form-group pt-15">
                            <h5>Report to ... (View only)</h5>
                            <select id="selectTeam4" class="form-control" multiple style="width:100%">
                            <?php
                            $teams_arr[] = "Ada.Hoppe";
                            $teams_arr[] = "Adrianna_Durgan";
                            $teams_arr[] = "Albin.Kreiger";
                            $teams_arr[] = "Alisa";
                            $teams_arr[] = "August";
                            $teams_arr[] = "Bell.Mueller";
                            $teams_arr[] = "Bret";
                            $teams_arr[] = "Ceasara_Orn";
                            $teams_arr[] = "Chester";
                            $teams_arr[] = "Citlalli_Wehner";
                            $teams_arr[] = "Clementina";
                            $teams_arr[] = "Coby";
                            $teams_arr[] = "Colin";
                            $teams_arr[] = "Damon";
                            $teams_arr[] = "Davin";
                            $teams_arr[] = "Elliott_Becker";
                            $teams_arr[] = "Emerson";
                            $teams_arr[] = "Gerhard";
                            $teams_arr[] = "Gunnar";
                            $teams_arr[] = "Gunner_Jakubowski";
                            $teams_arr[] = "Heath.Ryan";
                            $teams_arr[] = "Herta";
                            $teams_arr[] = "Hubert";
                            $teams_arr[] = "Jarvis.Simonis";
                            $teams_arr[] = "Jennie";
                            $teams_arr[] = "Johanna.Thiel";
                            $teams_arr[] = "Johnathan_Mraz";
                            $teams_arr[] = "Josephine";
                            $teams_arr[] = "Lacey";
                            $teams_arr[] = "Marjorie.Orn";
                            $teams_arr[] = "Mckenna.Herman";
                            $teams_arr[] = "Melany_Gerhold";
                            $teams_arr[] = "Miracle";
                            $teams_arr[] = "Monica";
                            $teams_arr[] = "Monique_Whitea";
                            $teams_arr[] = "Myriam_Nicolas";
                            $teams_arr[] = "Myrtie.Gerhold";
                            $teams_arr[] = "Raina";
                            $teams_arr[] = "Ruben.Reilly";
                            $teams_arr[] = "Sammie";
                            $teams_arr[] = "Shanel";
                            $teams_arr[] = "Stone_Deckow";
                            $teams_arr[] = "Terrance.Borer";
                            $teams_arr[] = "Thea";
                            $teams_arr[] = "Torrey";
                            $teams_arr[] = "Treva";
                            $teams_arr[] = "Wilhelmine";
                            $teams_arr[] = "Yasmine";
                            
                            for($bb=0;$bb<count($teams_arr);$bb++){
                              echo '<option value="'.$teams_arr[$bb].'">'.$teams_arr[$bb].'</option>';
                            }
                            ?>
                            </select>
                          </div>

                          <div class="form-group pt-15">
                            <h3>Reminders</h3>
                            <div class="form-group clearfix">
                              <h5 class="float-left w-100 mt-10">Repeat</h5>
                              <div class="float-left mr-10">
                                <input type="text" class="form-control" name="name" value="1" />
                              </div>
                              <div class="btn-group bootstrap-select float-left">
                                  <select id="select-repeat" data-plugin="selectpicker">
                                    <option value="day" selected="">Day</option>
                                    <option value="week">Week</option>
                                    <option value="month">Month</option>
                                    <option value="year">Year</option>
                                  </select>
                                </div>
                            </div>
                            <div class="form-group clearfix">
                              <h5 class="float-left w-100 mt-10">Repeat on</h5>
                              <div class="float-left mr-10">
                                <ul class="list-unstyled list-inline">
                                    <?php 
                                    $_day = array("Mon","Tue","Wed","Thu","Fri","Sat","Sun");
                                    for($ax=0;$ax<count($_day);$ax++){?>
                                      <li class="list-inline-item">
                                      <div class="checkbox-custom checkbox-primary">
                                        <input type="checkbox" id="inputUnchecked<?php echo $ax;?>" <?=$ax<1?"checked":""?> />
                                        <label for="inputUnchecked<?php echo $ax;?>"><?php echo $_day[$ax];?></label>
                                      </div>
                                      </li>
                                    <?php }?>
                                    </ul>
                              </div>
                            </div>
                            
                            <div class="form-group clearfix">
                              <h5 class="float-left w-100 mt-5">Ends</h5>
                              <div class="float-left mr-10">
                                  <input type="radio" class="icheckbox-primary" id="inputRadiosEnds1" name="inputRadiosEnds"
                                  data-plugin="iCheck" data-radio-class="iradio_flat-blue" value="Public" checked />
                                  <label class="ml-5 mr-30" for="inputRadiosEnds1">Never</label>
                                </div>
                              
                                <div class="float-left">
                                    <input type="radio" class="icheckbox-primary" id="inputRadiosEnds2" name="inputRadiosEnds"
                                  data-plugin="iCheck" data-radio-class="iradio_flat-blue" value="Public" checked />
                                  <label class="ml-5 mr-10" for="inputRadiosEnds2">On</label>
                                </div>
                                  <div class="float-left">
                                    <input type="text" class="form-control" data-plugin="datepicker" value=""/>
                                  </div>
                            </div>
                            

                          </div>
                      </form>
                    </div>
                  </div>
                  <!-- End Example Basic Form -->
                </div>
              </div>
            </div>
          </div>
          <div class="panel">
            <div class="panel-body container-fluid">
              <h3>Documents/Photos/Media</h3>
              <div class="example">
                <input type="file" id="input-file-now" data-plugin="dropify" data-default-file=""
                />
              </div>
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th width="20%">Download</th>
                    <th class="hidden-sm-down" width="20%">Created</th>
                    <th class="hidden-sm-down" width="20%">File size</th>
                  </tr>
                </thead>
                <tbody>
                   <tr>
                    <td>
                      <div class="media" style="flex-direction:initial;">
                        <div class="pr-20">
                          <a data-url="panel-file.html" data-toggle="slidePanel" class="font-size-24" href="javascript:void(0)">
                            <i style="color:#4caf50" class="icon fa-file-excel-o" aria-hidden="true"></i>
                          </a>
                        </div>
                        <div class="media-body pt-10">
                          <h5><a style="color:#616161;text-decoration: none;" data-url="panel-folder.html" data-toggle="slidePanel" href="javascript:void(0)">เอกสารรวบรวม Data</a></h5>
                          <p class="hidden-md-up mb-0 grey-500">
                            <small>
                              Mar 3, 2017 <span class="ml-10 mr-10">&mdash;</span> 1.52 MB                            </small>
                          </p>
                        </div>
                      </div>
                    </td>
                    <td class="pt-15">32</td>
                    <td class="hidden-sm-down pt-15">Mar 3, 2017</td>
                    <td class="hidden-sm-down pt-15">1.52 MB</td>
                  </tr>
                                    
                                  </tbody>
              </table>
              
            </div>
          </div>
        
        </div>
          


        </div>
      </div>
    </div>
  </div>
  <div class="site-action" data-plugin="actionBtn">
    <button type="button" onclick="location.href='news-form.php';" class="btn-raised btn btn-success btn-floating">
      <i class="front-icon md-plus" aria-hidden="true"></i>
    </button>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/imagesloaded/imagesloaded.pkgd.js"></script>
  <script src="../../global/vendor/summernote/summernote.min.js"></script>
  <script src="../../global/vendor/blueimp-canvas-to-blob/canvas-to-blob.js"></script>
  <script src="../../global/vendor/blueimp-load-image/load-image.all.min.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-process.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-image.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-audio.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-video.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-validate.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-ui.js"></script>
  <script src="../../global/vendor/dropify/dropify.min.js"></script>
  <script src="../../global/vendor/editable-table/mindmup-editabletable.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../assets/examples/js/forms/editor-summernote.js"></script>
  <script src="../../global/js/Plugin/dropify.js"></script>
  <script src="../../assets/examples/js/forms/uploads.js"></script>
  <script src="../../global/vendor/amcharts/amcharts.js" type="text/javascript"></script>
  <script src="../../global/vendor/amcharts/serial.js" type="text/javascript"></script>
  <script src="../../global/js/Plugin/bootstrap-datepicker.js"></script>
  <script>
    
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
      
    });


    /* 
      HTML5 Editable Table

      1
      https://codepen.io/ashblue/pen/mCtuA

      2
      http://jsfiddle.net/learner73/9dh5Lvb0/3/
      */

      var $TABLE = $('#editableTable');

      $('#btnaddrow').click(function () {
        var $clone = $TABLE.find('tr.hide').clone(true).removeClass('hide table-line');
        $TABLE.append($clone);
      });
      $('.table-remove').click(function () {
        $(this).parents('tr').detach();
      });

      $('#btnaddcolumn').click(function () {
        $TABLE.find('thead tr').append('<th contenteditable="true">Column</th>');
        $TABLE.find('tbody tr').append('<td contenteditable="true">0</td>');
      });



  })(document, window, jQuery);
  </script>
  <script type="text/javascript">
    var _annualsales;
    var chartData_annualsales = [
        {
            "year": 2016,
            "income": 29.5,
            "expenses": 28.1
        },
        {
            "year": 2017,
            "income": 30.6,
            "expenses": 28.2,
            "dashLengthLine": 5
        },
        {
            "year": 2018,
            "income": 34.1,
            "expenses": 29.9,
            "dashLengthColumn": 5,
            "alpha":0.2,
            "additional":"(forcash)"
        }

    ];


    AmCharts.ready(function () {
        // SERIAL CHART
        _annualsales = new AmCharts.AmSerialChart();

        _annualsales.dataProvider = chartData_annualsales;
        _annualsales.categoryField = "year";
        _annualsales.startDuration = 1;

        // AXES
        // category
        var categoryAxis_annualsales = _annualsales.categoryAxis;
        categoryAxis_annualsales.gridPosition = "start";

        // value
        var valueAxis_annualsales = new AmCharts.ValueAxis();
        valueAxis_annualsales.axisAlpha = 0;
        _annualsales.addValueAxis(valueAxis_annualsales);

        // GRAPHS
        // column graph
        var graph1_annualsales = new AmCharts.AmGraph();
        graph1_annualsales.type = "column";
        graph1_annualsales.title = "Sales";
        graph1_annualsales.lineColor = "#a668d5";
        graph1_annualsales.valueField = "income";
        graph1_annualsales.lineAlpha = 1;
        graph1_annualsales.fillAlphas = 1;
        graph1_annualsales.dashLengthField = "dashLengthColumn";
        graph1_annualsales.alphaField = "alpha";
        graph1_annualsales.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
        _annualsales.addGraph(graph1_annualsales);

        // line
        var graph2_annualsales = new AmCharts.AmGraph();
        graph2_annualsales.type = "line";
        graph2_annualsales.title = "Goal";
        graph2_annualsales.lineColor = "#fcd202";
        graph2_annualsales.valueField = "expenses";
        graph2_annualsales.lineThickness = 3;
        graph2_annualsales.bullet = "round";
        graph2_annualsales.bulletBorderThickness = 3;
        graph2_annualsales.bulletBorderColor = "#fcd202";
        graph2_annualsales.bulletBorderAlpha = 1;
        graph2_annualsales.bulletColor = "#ffffff";
        graph2_annualsales.dashLengthField = "dashLengthLine";
        graph2_annualsales.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
        _annualsales.addGraph(graph2_annualsales);

        // LEGEND
        var legend_annualsales = new AmCharts.AmLegend();
        legend_annualsales.useGraphSettings = true;
        _annualsales.addLegend(legend_annualsales);

        // WRITE
        _annualsales.write("chart_annualsales");
    });
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>