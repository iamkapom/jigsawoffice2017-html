<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.min.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
</head>
<body class="animsition">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <!-- Page -->
  <div class="page">
    <div class="page-content">
      <h2>Blank</h2>
      <p>Page content goes here</p>

      <div class="card card-shadow card-bordered">
                  <div class="card-block media clearfix p-25">
                    <div class="pr-20">
                      <a href="#" class="avatar avatar-lg">
                        <img class="img-fluid" src="../../../global/portraits/6.jpg">
                      </a>
                    </div>
                    <div class="media-body text-middle">
                      <h4 class="mt-0 mb-5">
                        Mallinda Hollaway
                      </h4>
                      <small>Linda</small>
                    </div>
                  </div>
                  <div class="card-block px-25  pt-0">
                    <p class="card-text mb-20">
                      Ex quis excepteur exercitation incididunt ea amet commodo deserunt nulla. Anim
                      sit duis cupidatat ipsum deserunt reprehenderit fugiat cupidatat.
                      In do eu sint Lorem qui eu eu. Id ad non pariatur culpa. Duis proident
                      cupidatat laborum pariatur sit eu eiusmod. Cillum consectetur exercitation
                      ex ipsum. Ullamco commodo anim ut aliqua ex incididunt commodo
                      incididunt reprehenderit. Sit nisi deserunt fugiat eu qui nisi
                      nulla.
                    </p>
                    
                    <ul class="wall-attrs clearfix p-0 m-0">
                      <li class="attrs-meta float-left font-size-16">
                        <a href="javascript:void(0)" class="mr-20">
                          <i class="icon md-favorite"></i>
                          <span>5</span>
                        </a>
                        <a href="javascript:void(0)">
                          <i class="icon md-comment"></i>
                          <span>2</span>
                        </a>
                      </li>
                      <li class="float-right">
                        <div class="btn-group bootstrap-select btn-comment-post">
                          <select data-plugin="selectpicker">
                            <option>Top Comments</option>
                            <option>Most Recent</option>
                          </select>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="card-footer p-20">
                    <div class="wall-comment">
                      <a href="#" class="avatar avatar-md float-left">
                        <img src="../../../global/portraits/3.jpg">
                      </a>
                      <div class="ml-60">
                        <a href="#">Stacey Hunt</a>
                        <span class="ml-10">Do incididunt elit ex incididunt ut. Aute velit proident cupidatat qui et consectetur
                          do. Consequat esse quis ut cupidatat ea. Sint dolore ea culpa
                          dolore velit enim.</span>
                        <p class="font-size-12 mt-5 grey-500">
                          <a href="javascript:void(0)">
                            Like
                          </a>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <a href="javascript:void(0)">
                            Reply
                          </a>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <span >
                            <i class="icon md-favorite"></i>
                            <span>5</span>
                          </span>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <span>
                            30th July 2017
                          </span>
                        </p>

                        <div class="wall-comment">
                          <a href="#" class="avatar avatar-md float-left">
                            <img src="../../../global/portraits/11.jpg">
                          </a>
                          <div class="ml-60">
                            <a href="#">Crystal Bates</a>
                            <span class="ml-10">Do incididunt elit ex incididunt ut. Aute velit proident cupidatat qui et consectetur
                              do. Consequat esse quis ut cupidatat ea.</span>
                            <p class="font-size-12 mt-5 grey-500">
                              <a href="javascript:void(0)">
                                Like
                              </a>
                              <span class="mr-5 ml-5">&#8226;</span>
                              <a href="javascript:void(0)">
                                Reply
                              </a>
                              <span class="mr-5 ml-5">&#8226;</span>
                              <span >
                                <i class="icon md-favorite"></i>
                                <span>5</span>
                              </span>
                              <span class="mr-5 ml-5">&#8226;</span>
                              <span>
                                30th July 2017
                              </span>
                            </p>
                          </div>
                          
                        </div>
                      </div>
                      
                    </div>
                    <div class="wall-comment">
                      <a href="#" class="avatar avatar-md float-left">
                        <img src="../../../global/portraits/5.jpg">
                      </a>
                      <div class="ml-60">
                        <a href="#">Sam Anderson</a>
                        <span class="ml-10">Ut velit ipsum elit ut mollit elit. Proident officia eu ex consectetur ullamco
                          magna. Enim cillum voluptate sint ipsum ad voluptate exercitation.
                          Ex est amet magna occaecat eu.</span>
                        <p class="font-size-12 mt-5 grey-500">
                          <a href="javascript:void(0)">
                            Like
                          </a>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <a href="javascript:void(0)">
                            Reply
                          </a>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <span >
                            <i class="icon md-favorite"></i>
                            <span>5</span>
                          </span>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <span>
                            30th July 2017
                          </span>
                        </p>
                      </div>
                      
                    </div>
                    <form class="wall-comment-reply">
                      <input class="form-control" placeholder="Write Something..." type="text">
                      <div class="reply-operation active">
                        <button class="btn btn-sm btn-primary reply-post waves-effect waves-classic" type="button">POST</button>
                        <button class="btn btn-sm btn-pure btn-default reply-cancel waves-effect waves-classic" type="button">CANCEL</button>
                      </div>
                    </form>
                  </div>
                </div>
    </div>
  </div>
  <!-- End Page -->
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <!-- Config -->
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <!-- Page -->
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>