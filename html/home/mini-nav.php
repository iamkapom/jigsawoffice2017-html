<div class="mb-30 fix-mini-nav">
  <nav class="navbar navbar-default" role="navigation">
    <div class="text-center" data-plugin="tabs">
      <ul class="nav nav-pills mt-10" style="display:inline-flex;">
        <li class="nav-item hidden-sm-down">
          <a class="nav-link <?=(basename($_SERVER["SCRIPT_FILENAME"])=="index.php"?"active":"")?>" href="index.php" aria-expanded="true"><i class="md-home" aria-hidden="true"></i> Feed</a>
        </li>
        <li class="nav-item hidden-sm-down">
          <a class="nav-link <?=(basename($_SERVER["SCRIPT_FILENAME"])=="dashboard.php"?"active":"")?>" href="dashboard.php" aria-expanded="false"><i class="md-tab" aria-hidden="true"></i> Dashbaord</a>
        </li>
        <li class="nav-item hidden-sm-down">
          <a class="nav-link" href="javascript:void(0)" aria-expanded="false"><i class="md-settings" aria-hidden="true"></i> Setting</a>
        </li>
        <li class="nav-item dropdown hidden-md-up">
          <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#" aria-expanded="false">Menu </a>
          <div class="dropdown-menu" role="menu">
            <a class="dropdown-item active" href="javascript:void(0)" aria-expanded="true"><i class="md-home" aria-hidden="true"></i> Feed</a>
            <a class="dropdown-item" href="javascript:void(0)" aria-expanded="false"><i class="md-tab" aria-hidden="true"></i> Dashbaord</a>
            <a class="dropdown-item" href="javascript:void(0)" aria-expanded="false"><i class="md-settings" aria-hidden="true"></i> Setting</a>
          </div>
        </li>
      </ul>
    </div>
  </nav>
</div>