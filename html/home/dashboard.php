<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.min.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/slick-carousel/slick.css">
  <link rel="stylesheet" href="../../global/vendor/chartist/chartist.css">
  <link rel="stylesheet" href="../../global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
  <link rel="stylesheet" href="../../assets/examples/css/pages/profile_v3.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500">
  <style type="text/css">
  .slick-dots li button::before{font-size: 14px;}
  .btn-comment-post .btn-select{
    border: 0px !important;
  }
  .btn-comment-post, .btn-comment-post .bootstrap-select{
    width: 150px !important;
  }
  .fix-mini-nav + .page-content{
    padding-top: 0;
  }
  .fix-mini-nav .page-header{
    margin-bottom: 93px;
  }
  .fix-mini-nav .fix-mini-nav{
    margin-bottom: 0;
    margin-right: 0;
    margin-top: 0;
    padding: 0;
    position: fixed;
    top: 65px;
    width: 100%;
    z-index: 10;
    left: 0;
  }
  .btn-comment-post .btn-select{
    border: 0px !important;
  }
  .btn-comment-post, .btn-comment-post .bootstrap-select{
    width: 150px !important;
  }
  .page-content{
    padding: 0;
  }
  </style>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
</head>
<body class="animsition">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <!-- Page -->
  <div class="page">
    <div class="page-header">
      <h1 class="page-title mb-10">Home</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
      </ol>
    </div>
    <?php include("mini-nav.php");?>
    <div class="page-content">
      <div class="row" data-plugin="matchHeight" data-by-row="true">
        <div class="col-xl-3 col-md-6">
          <!-- Widget Linearea One-->
          <div class="card card-shadow" id="widgetLineareaOne">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-account grey-600 font-size-24 vertical-align-bottom mr-5"></i>                  User
                </div>
                <span class="float-right grey-700 font-size-30">1,253</span>
              </div>
              <div class="mb-20 grey-500">
                <i class="icon md-long-arrow-up green-500 font-size-16"></i> 15%
                From this yesterday
              </div>
              <div class="ct-chart h-50"></div>
            </div>
          </div>
          <!-- End Widget Linearea One -->
        </div>
        <div class="col-xl-3 col-md-6">
          <!-- Widget Linearea Two -->
          <div class="card card-shadow" id="widgetLineareaTwo">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-flash grey-600 font-size-24 vertical-align-bottom mr-5"></i>                  VISITS
                </div>
                <span class="float-right grey-700 font-size-30">2,425</span>
              </div>
              <div class="mb-20 grey-500">
                <i class="icon md-long-arrow-up green-500 font-size-16"></i> 34.2%
                From this week
              </div>
              <div class="ct-chart h-50"></div>
            </div>
          </div>
          <!-- End Widget Linearea Two -->
        </div>
        <div class="col-xl-3 col-md-6">
          <!-- Widget Linearea Three -->
          <div class="card card-shadow" id="widgetLineareaThree">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-chart grey-600 font-size-24 vertical-align-bottom mr-5"></i>                  Total Clicks
                </div>
                <span class="float-right grey-700 font-size-30">1,864</span>
              </div>
              <div class="mb-20 grey-500">
                <i class="icon md-long-arrow-down red-500 font-size-16"></i> 15%
                From this yesterday
              </div>
              <div class="ct-chart h-50"></div>
            </div>
          </div>
          <!-- End Widget Linearea Three -->
        </div>
        <div class="col-xl-3 col-md-6">
          <!-- Widget Linearea Four -->
          <div class="card card-shadow" id="widgetLineareaFour">
            <div class="card-block p-20 pt-10">
              <div class="clearfix">
                <div class="grey-800 float-left py-10">
                  <i class="icon md-view-list grey-600 font-size-24 vertical-align-bottom mr-5"></i>                  Items
                </div>
                <span class="float-right grey-700 font-size-30">845</span>
              </div>
              <div class="mb-20 grey-500">
                <i class="icon md-long-arrow-up green-500 font-size-16"></i> 18.4%
                From this yesterday
              </div>
              <div class="ct-chart h-50"></div>
            </div>
          </div>
          <!-- End Widget Linearea Four -->
        </div>
        <div class="col-xxl-5 col-lg-6">
          <!-- Panel Projects -->
          <div class="panel" id="projects">
            <div class="panel-heading">
              <h3 class="panel-title">Projects</h3>
            </div>
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <td>Projects</td>
                    <td>Completed</td>
                    <td>Date</td>
                    <td>Actions</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>The sun climbing plan</td>
                    <td>
                      <span data-plugin="peityPie" data-skin="red">7/10</span>
                    </td>
                    <td>Jan 1, 2017</td>
                    <td>
                      <button type="button" class="btn btn-sm btn-icon btn-pure btn-default" data-toggle="tooltip"
                      data-original-title="Edit">
                        <i class="icon md-wrench" aria-hidden="true"></i>
                      </button>
                      <button type="button" class="btn btn-sm btn-icon btn-pure btn-default" data-toggle="tooltip"
                      data-original-title="Delete">
                        <i class="icon md-close" aria-hidden="true"></i>
                      </button>
                    </td>
                  </tr>
                  <tr>
                    <td>Lunar probe project</td>
                    <td>
                      <span data-plugin="peityPie" data-skin="blue">3/10</span>
                    </td>
                    <td>Feb 12, 2017</td>
                    <td>
                      <button type="button" class="btn btn-sm btn-icon btn-pure btn-default" data-toggle="tooltip"
                      data-original-title="Edit">
                        <i class="icon md-wrench" aria-hidden="true"></i>
                      </button>
                      <button type="button" class="btn btn-sm btn-icon btn-pure btn-default" data-toggle="tooltip"
                      data-original-title="Delete">
                        <i class="icon md-close" aria-hidden="true"></i>
                      </button>
                    </td>
                  </tr>
                  <tr>
                    <td>Dream successful plan</td>
                    <td>
                      <span data-plugin="peityPie" data-skin="green">9/10</span>
                    </td>
                    <td>Apr 9, 2017</td>
                    <td>
                      <button type="button" class="btn btn-sm btn-icon btn-pure btn-default" data-toggle="tooltip"
                      data-original-title="Edit">
                        <i class="icon md-wrench" aria-hidden="true"></i>
                      </button>
                      <button type="button" class="btn btn-sm btn-icon btn-pure btn-default" data-toggle="tooltip"
                      data-original-title="Delete">
                        <i class="icon md-close" aria-hidden="true"></i>
                      </button>
                    </td>
                  </tr>
                  <tr>
                    <td>Office automatization</td>
                    <td>
                      <span data-plugin="peityPie" data-skin="orange">5/10</span>
                    </td>
                    <td>May 15, 2017</td>
                    <td>
                      <button type="button" class="btn btn-sm btn-icon btn-pure btn-default" data-toggle="tooltip"
                      data-original-title="Edit">
                        <i class="icon md-wrench" aria-hidden="true"></i>
                      </button>
                      <button type="button" class="btn btn-sm btn-icon btn-pure btn-default" data-toggle="tooltip"
                      data-original-title="Delete">
                        <i class="icon md-close" aria-hidden="true"></i>
                      </button>
                    </td>
                  </tr>
                  <tr>
                    <td>Open strategy</td>
                    <td>
                      <span data-plugin="peityPie" data-skin="brown">2/10</span>
                    </td>
                    <td>Jun 2, 2017</td>
                    <td>
                      <button type="button" class="btn btn-sm btn-icon btn-pure btn-default" data-toggle="tooltip"
                      data-original-title="Edit">
                        <i class="icon md-wrench" aria-hidden="true"></i>
                      </button>
                      <button type="button" class="btn btn-sm btn-icon btn-pure btn-default" data-toggle="tooltip"
                      data-original-title="Delete">
                        <i class="icon md-close" aria-hidden="true"></i>
                      </button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <!-- End Panel Projects -->
        </div>
        <div class="col-xxl-7 col-lg-6">
          <!-- Panel Projects Status -->
          <div class="panel" id="projects-status">
            <div class="panel-heading">
              <h3 class="panel-title">
                Projects Status
                <span class="badge badge-pill badge-info">5</span>
              </h3>
            </div>
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <td>ID</td>
                    <td>Project</td>
                    <td>Status</td>
                    <td class="text-left">Progress</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>619</td>
                    <td>The sun climbing plan</td>
                    <td>
                      <span class="badge badge-primary">Developing</span>
                    </td>
                    <td>
                      <span data-plugin="peityLine">5,3,2,-1,-3,-2,2,3,5,2</span>
                    </td>
                  </tr>
                  <tr>
                    <td>620</td>
                    <td>Lunar probe project</td>
                    <td>
                      <span class="badge badge-warning">Design</span>
                    </td>
                    <td>
                      <span data-plugin="peityLine">1,-1,0,2,3,1,-1,1,4,2</span>
                    </td>
                  </tr>
                  <tr>
                    <td>621</td>
                    <td>Dream successful plan</td>
                    <td>
                      <span class="badge badge-info">Testing</span>
                    </td>
                    <td>
                      <span data-plugin="peityLine">2,3,-1,-3,-1,0,2,4,5,3</span>
                    </td>
                  </tr>
                  <tr>
                    <td>622</td>
                    <td>Office automatization</td>
                    <td>
                      <span class="badge badge-danger">Canceled</span>
                    </td>
                    <td>
                      <span data-plugin="peityLine">1,-2,0,2,4,5,3,2,4,2</span>
                    </td>
                  </tr>
                  <tr>
                    <td>623</td>
                    <td>Open strategy</td>
                    <td>
                      <span class="badge badge-default">Reply waiting</span>
                    </td>
                    <td>
                      <span data-plugin="peityLine">4,2,-1,-3,-2,1,3,5,2,4</span>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <!-- End Panel Projects Stats -->
        </div>
      </div>


      </div>
    </div>
  </div>
  <!-- End Page -->
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/slick-carousel/slick.js"></script>
  <script src="../../global/vendor/jquery.dotdotdot/jquery.dotdotdot.js"></script>
  <script src="../../global/vendor/chartist/chartist.min.js"></script>
  <script src="../../global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.min.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/peity/jquery.peity.min.js"></script>
  <script src="../../global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.min.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <!-- Page -->
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/responsive-tabs.js"></script>
  <script src="../../global/js/Plugin/tabs.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../global/js/Plugin/peity.js"></script>
  <script src="../../../global/js/Plugin/bootstrap-tokenfield.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
    $(document).ready(function($) {
      Site.run();
      $(".ellipsis").dotdotdot({
        after: "a.readmore"
      });
      $("#home-announcement").slick({
        dots: true,
        infinite: true,
        adaptiveHeight: true,
        arrows: false,
        speed: 500,
        autoplay: true,
        swipeToSlide: true
      });
      $("#home-news").slick({
        dots: true,
        infinite: true,
        adaptiveHeight: true,
        arrows: false,
        speed: 600,
        autoplay: true,
        swipeToSlide: true
      });

      var wrap = $('.page');
      $(window).scroll(function () {
        if ($(this).scrollTop() > 125) {
          wrap.addClass("fix-mini-nav");
        } else {
          wrap.removeClass("fix-mini-nav");
        }
      });

    });

  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>
