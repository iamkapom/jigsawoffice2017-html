/*!
 * remark (http://getbootstrapadmin.com/remark)
 * Copyright 2015 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */
(function(document, window, $) {
  'use strict';

  window.App = AppInfo2.extend({
    
    //Init SlidePanel
    handlSlidePanelPlugin: function() {
      if (typeof $.slidePanel === 'undefined') return;
      var self = this;
      var defaults = $.components.getDefaults("slidePanel");
      var options = $.extend({}, defaults, {
        template: function(options) {
          return '<div class="' + options.classes.base + ' ' + options.classes.base + '-' + options.direction + '">' +
            '<div class="' + options.classes.base + '-scrollable"><div>' +
            '<div class="' + options.classes.content + '"></div>' +
            '</div></div>' +
            '<div class="' + options.classes.base + '-handler"></div>' +
            '</div>';
        },
        afterLoad: function(object) {
          var _this = this,
            $target = $(object.target),
            info = $target.data('taskInfo');

          this.$panel.find('.' + this.options.classes.base + '-scrollable').asScrollable({
            namespace: 'scrollable',
            contentSelector: '>',
            containerSelector: '>'
          });

          this.$panel.find('#task-description').markdown();
          if (info.duedate.length > 0) {
            this.$panel.find('#taskDatepicker').data('date', info.duedate);
          }
          this.$panel.find('#taskDatepicker').datepicker({
            autoclose: false,
            todayHighlight: true,
          }).on('changeDate', function() {
            $('#taskDatepickerInput').val(
              _this.$panel.find('#taskDatepicker').datepicker('getFormattedDate')
            );
          });;


          this.$panel.data('slidePanel', object);

          $(document).off('click.slidePanelDatepicker');
          $(document).on('click.slidePanelDatepicker', 'span, td, th', function(e) {
            e.stopPropagation();
          });
        },
        afterShow: function() {
          var self = this;
          $(document).on('click.slidePanelShow', function(e) {
            if ($(e.target).closest('.slidePanel').length === 0 && $(e.target).closest('body').length === 1) {
              self.hide();
            }
          });
        },
        afterHide: function() {
          $(document).off('click.slidePanelShow');
          $(document).off('click.slidePanelDatepicker');
        },
        contentFilter: function(data, object) {
          var $target = $(object.target),
            info = $target.data('taskInfo'),
            $panel = $(data),
            $checked;

          $('.stage-name', $panel).html($('.taskboard-stage-title', self.getStage($target)).html());

          $('.task-title', $panel).html(info.title);

          switch (info.priority) {
            case 'high':
              $checked = $('#priorityHigh', $panel);
              break;
            case 'urgent':
              $checked = $('#priorityUrgent', $panel);
              break;
            default:
              $checked = $('#priorityNormal', $panel);
              break;
          }
          $checked.prop("checked", true);

          self.handleSelective($('[data-plugin="jquery-selective"]', $panel), info.members);

          if (info.description.length === 0) {
            $('.description', $panel).addClass('is-empty');
          } else {
            $('.description-content', $panel).html(info.description);
          }

          if (info.subtasks.length !== 0) {
            $.each(info.subtasks, function(n, subtask) {
              var $subtask = $(self.subtaskTpl(subtask));
              $('.subtasks-list', $panel).append($subtask);
            });
            $('.subtasks', $panel).toggleClass('is-show');
          }

          if (info.attachments.length !== 0) {
            $.each(info.attachments, function(n, attachment) {
              var $attachment = $(self.attachmentTpl(attachment));
              $('.attachments-list', $panel).append($attachment);
            });
            $('.attachments', $panel).toggleClass('is-show');
          }

          if (info.comments.length !== 0) {
            $.each(info.comments, function(n, comment) {
              var $comment = $(self.commentTpl(comment.src, comment.user, comment.time, comment.content));
              $('.comments-history', $panel).append($comment);
            });
          }

          return $panel;
        }
      });

      $(document).on('click', '[data-app="slidePanel"]', function(e) {
        alert('123')
        var $target = $(e.target).closest('.list-group-item');
        $.slidePanel.show({
          url: $(this).data('url')
        }, options);

        e.stopPropagation();
      });

    },
    run: function(next) {
      console.log('1232');
      this.handlSlidePanelPlugin();
      next();
    }
  });

})(document, window, jQuery);
