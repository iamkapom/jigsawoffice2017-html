<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition app-documents">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-5">Applications Management</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../home/index.php">Home</a></li>
        <li class="breadcrumb-item">Site Setting</li>
        <li class="breadcrumb-item active">Applications Management</li>
      </ol>
      
      <?php include("mini-nav.php");?>
    </div>

    
    <div class="page-content container-fluid" style="position: relative;">
      
      <div class="row ml-0 mr-0">
        <div class="col-md-12">
          

          <section>
            
            <div class="row">
              <?php
              $_img[]="horizontal-15.jpg";
              $_img[]="horizontal-1.jpg";
              $_img[]="horizontal-12.jpg";
              $_img[]="horizontal-7.jpg";
              $_img[]="horizontal-9.jpg";
              $_img[]="horizontal-14.jpg";
              $_img[]="leads-1.jpg";
              $_img[]="accounts-4.jpg";
              $_img[]="horizontal-10.jpg";
              $_img[]="horizontal-16.jpg";
              $_img[]="horizontal-8.jpg";
              $_img[]="horizontal-11.jpg";

              $_modname[]="Co-Working";
              $_modname[]="Intranet";
              $_modname[]="Knowledgebase";
              $_modname[]="Car booking";
              $_modname[]="Meeting room";
              $_modname[]="Support Tickets";
              $_modname[]="CRM";
              $_modname[]="Sales Force";
              $_modname[]="Learning & Skills Test";
              $_modname[]="Document Workflow";
              $_modname[]="Mind Map";
              $_modname[]="Form Builder";

              $_offmod = 8;
              for($ab=0;$ab<count($_modname);$ab++){
              ?>
              <div class="col-md-4">
                <div class="card card-shadow card-bordered <?=($ab<$_offmod)?"":"bg-grey-100"?>">
                  <div class="card-block p-15 clearfix" style="padding-bottom: 0px;">

                    <span class="hidden-xs-down avatar avatar-100 float-left mr-20 mb-10 img-bordered bg-white" href="javascript:void(0)">
                      <img src="../../assets/examples/img/<?=$_img[$ab]?>" style="width: 100%;height: 100%;" alt="">
                    </span>
                    <div class="font-size-18">
                        <strong>
                      <a class="<?=($ab<$_offmod)?"blue-600":"grey-600"?>" href="javascript:void(0)"><?=$_modname[$ab]?></a>
                      </strong>
                    </div>
                    <p class="<?=($ab<$_offmod)?"":"grey-400"?>">
                      Ex quis excepteur exercitation incididunt ea amet commodo deserunt nulla. Anim sit duis cupidatat ipsum deserunt reprehenderit fugiat cupidatat. deserunt reprehenderit fugiat cupidatat.
                    </p>
                    <p class="pt-20">
                      <h5>On/Off module</h5>
                      <div class="grey-600">
                        <input type="checkbox" id="m1<?=$ab?>" name="m1<?=$ab?>" data-plugin="switchery" data-size="small" <?=($ab<$_offmod)?"checked":""?> />
                        <label for="m1<?=$ab?>" style="cursor: pointer;">
                           On/Off
                        </label>
                      </div>
                    </p>
                    <p class="pt-10">
                      <h5>Admin Modules</h5>
                      <ul class="list-group list-group-full">
                        <li class="list-group-item clearfix">
                          <a class="avatar avatar-lg float-left mr-20" href="javascript:void(0)">
                            <img src="../../../global/portraits/<?=rand(1,20)?>.jpg" alt="">
                          </a>
                          <div class="float-left text-primary">
                            Robin Ahrens
                            <div class="font-size-12 grey-600">robin.a@jigsawoffice.com</div>
                          </div>
                          <div class="float-right">
                            <span class="btn btn-pure btn-icon waves-effect waves-classic" data-toggle="list-delete"><i class="icon md-delete" aria-hidden="true"></i></span>
                          </div>
                        </li>
                      </ul>
                      <a id="btn-admin-frm-<?=$ab?>" class="btn btn-default btn-pure text-primary" href="#" onclick="$('#admin-frm-<?=$ab?>').slideDown();$('#btn-admin-frm-<?=$ab?>').hide();"><i class="icon md-plus" aria-hidden="true"></i>Add Admin</a>
                      <div id="admin-frm-<?=$ab?>" class="pt-20" style="display: none;">
                        <form class="add-item" role="form" method="post" action="#">
                          <div class="form-group">
                            <h5>++ add Admin:</h5>
                            <select id="selectTeam4" class="form-control" multiple style="width:100%">
                            <?php
                            $teams_arr[] = "Ada.Hoppe";
                            $teams_arr[] = "Adrianna_Durgan";
                            $teams_arr[] = "Albin.Kreiger";
                            $teams_arr[] = "Alisa";
                            $teams_arr[] = "August";
                            $teams_arr[] = "Bell.Mueller";
                            $teams_arr[] = "Bret";
                            $teams_arr[] = "Ceasara_Orn";
                            $teams_arr[] = "Chester";
                            $teams_arr[] = "Citlalli_Wehner";
                            $teams_arr[] = "Clementina";
                            $teams_arr[] = "Coby";
                            $teams_arr[] = "Colin";
                            $teams_arr[] = "Damon";
                            $teams_arr[] = "Davin";
                            $teams_arr[] = "Elliott_Becker";
                            $teams_arr[] = "Emerson";
                            $teams_arr[] = "Gerhard";
                            $teams_arr[] = "Gunnar";
                            $teams_arr[] = "Gunner_Jakubowski";
                            $teams_arr[] = "Heath.Ryan";
                            $teams_arr[] = "Herta";
                            $teams_arr[] = "Hubert";
                            $teams_arr[] = "Jarvis.Simonis";
                            $teams_arr[] = "Jennie";
                            $teams_arr[] = "Johanna.Thiel";
                            $teams_arr[] = "Johnathan_Mraz";
                            $teams_arr[] = "Josephine";
                            $teams_arr[] = "Lacey";
                            $teams_arr[] = "Marjorie.Orn";
                            $teams_arr[] = "Mckenna.Herman";
                            $teams_arr[] = "Melany_Gerhold";
                            $teams_arr[] = "Miracle";
                            $teams_arr[] = "Monica";
                            $teams_arr[] = "Monique_Whitea";
                            $teams_arr[] = "Myriam_Nicolas";
                            $teams_arr[] = "Myrtie.Gerhold";
                            $teams_arr[] = "Raina";
                            $teams_arr[] = "Ruben.Reilly";
                            $teams_arr[] = "Sammie";
                            $teams_arr[] = "Shanel";
                            $teams_arr[] = "Stone_Deckow";
                            $teams_arr[] = "Terrance.Borer";
                            $teams_arr[] = "Thea";
                            $teams_arr[] = "Torrey";
                            $teams_arr[] = "Treva";
                            $teams_arr[] = "Wilhelmine";
                            $teams_arr[] = "Yasmine";
                            
                            for($bb=0;$bb<count($teams_arr);$bb++){
                              echo '<option value="'.$teams_arr[$bb].'">'.$teams_arr[$bb].'</option>';
                            }
                            ?>
                            </select>
                          </div>
                          <div class="form-group text-right">
                            <a class="btn btn-default btn-pure mr-10" onclick="$('#admin-frm-<?=$ab?>').slideUp();$('#btn-admin-frm-<?=$ab?>').show();">Cancel</a>
                            <button type="button" class="btn btn-primary" onclick="$('#admin-frm-<?=$ab?>').slideUp();$('#btn-admin-frm-<?=$ab?>').show();">Add</button>
                          </div>
                        </form>
                      </div>
                    </p>
                    
                  </div>
                </div>
              </div>

              <?php }?>
              
            </div>
            
          </section>
        </div>
      </div>
      
    </div>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>