<ul class="nav nav-tabs nav-tabs-line fix-mini-nav" role="tablist">
  <li class="nav-item">
    <a class="nav-link <?=(substr(basename($_SERVER["SCRIPT_FILENAME"]),0,5)=="users"?"active":"")?>" href="users.php" aria-expanded="false">Users</a>
  </li>
  <li class="nav-item hidden-sm-down">
    <a class="nav-link <?=(substr(basename($_SERVER["SCRIPT_FILENAME"]),0,3)=="app"?"active":"")?>" href="application.php" aria-expanded="false">Applications</a>
  </li>
  <li class="nav-item hidden-sm-down">
    <a class="nav-link <?=(substr(basename($_SERVER["SCRIPT_FILENAME"]),0,9)=="diskspace"?"active":"")?>" href="diskspace.php" aria-expanded="false">Disk space</a>
  </li>
</ul>