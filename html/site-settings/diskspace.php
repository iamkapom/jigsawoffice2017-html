<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../global/vendor/aspieprogress/asPieProgress.css">
  <link rel="stylesheet" href="../../assets/examples/css/charts/pie-progress.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition app-documents page-aside-right">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-5">Disk sapce</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../home/index.php">Home</a></li>
        <li class="breadcrumb-item">Site Setting</li>
        <li class="breadcrumb-item active">Disk sapce</li>
      </ol>
      
      <?php include("mini-nav.php");?>
    </div>

    
    <div class="page-content container-fluid" style="position: relative;">

      <div class="row ml-0 mr-0">
        <div class="col-md-12">
          <div class="page-aside">
            <!-- Contacts Sidebar -->
            <div class="page-aside-switch">
              <i class="icon md-chevron-left" aria-hidden="true"></i>
              <i class="icon md-chevron-right" aria-hidden="true"></i>
            </div>
            <div class="page-aside-inner page-aside-scroll">
              <div data-role="container">
                <div data-role="content">
                  <section class="page-aside-section" style="min-height: 1000px;">
                    <div class="card card-block pt-0">
                      <h3 class="mt-0">Setting</h3>
                      <p>Initial disk space :</p>
                      <div class="btn-group bootstrap-select mb-30">
                        <select data-plugin="selectpicker">
                          <option selected="">100 MB</option>
                          <option>200 MB</option>
                          <option>300 MB</option>
                          <option>400 MB</option>
                          <option>500 MB</option>
                        </select>
                      </div>

                      <p>Maximum Upload :</p>
                      <div class="btn-group bootstrap-select">
                        <select data-plugin="selectpicker">
                          <option>500 KB</option>
                          <option selected="">1 MB</option>
                          <option>2 MB</option>
                          <option>5 MB</option>
                          <option>10 MB</option>
                          <option>20 MB</option>
                          <option>30 MB</option>
                          <option>40 MB</option>
                          <option>50 MB</option>
                        </select>
                      </div>
                    </div>
                    
                    <div class="card card-block pt-0" style="min-height: 250px;">
                      <h3 class="mt-0">Total Disk space</h3>
                        <div class="pt-20 pb-20">
                          <div class="pie-progress pie-progress-lg" data-plugin="pieProgress" data-barcolor="#00bcd4"
                          data-goal="34" aria-valuenow="34" data-size="100" data-barsize="10"
                          role="progressbar">
                            <div class="pie-progress-content">
                              <h3>330 GB</h3>
                              <div class="pie-progress-label">free of 500 GB</div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="card card-block pt-0" style="min-height: 250px;">
                      <h3 class="mt-0">Overview of Workspace mode</h3>
                      <div>
                          <div class="contextual-progress">
                            <div class="clearfix">
                              <div class="progress-title">Internal</div>
                              <div class="progress-label">3.2 GB free of 5 GB</div>
                            </div>
                            <div class="progress" data-plugin="progress" style="height: 20px;">
                              <div class="progress-bar progress-bar-info" aria-valuenow="36" role="progressbar" style="width: 36%; line-height: 20px;">
                                <span class="progress-label" style="float: none;">36%</span>
                              </div>
                            </div>
                          </div>
                          <div class="contextual-progress">
                            <div class="clearfix">
                              <div class="progress-title">External</div>
                              <div class="progress-label">1.2 GB free of 2 GB</div>
                            </div>
                            <div class="progress" data-plugin="progress" style="height: 20px;">
                              <div class="progress-bar progress-bar-info" aria-valuenow="42" role="progressbar" style="width: 42%; line-height: 20px;">
                                <span class="progress-label" style="float: none;">42%</span>
                              </div>
                            </div>
                          </div>

                      </div>
                    </div>
                    



                  </section>
                </div>
              </div>
            </div>
          </div>
            <div class="page-main">
        <div class="pt-0 pb-20">
          <div class="search-wrapper">
            <div class="search-box">
              <div class="icon md-search"></div>
              <div class="currently-showing">
                <?=$search['tokenhtml']?>
              </div>
            </div>
            <a href="javascript:void(0);" class="clear_all btn btn-flat btn-default btn-block waves-effect waves-classic">Clear All</a>
            <div class="data_entry">
              <input class="input keyword-input" placeholder="Enter a keyword" type="text">
              <div class="icon md-close-circle close"></div>
            </div>
            <div class="filters">
                <div class="dropdown filter_permission">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      Mode
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu" role="menu">
                      <a class="dropdown-item" href="javascript:void(0)"><i class="icon md-accounts-alt mr-5" aria-hidden="true"></i>Internal Mode</a>
                      <a class="dropdown-item" href="javascript:void(0)"><i class="icon md-globe-alt mr-5" aria-hidden="true"></i>External Mode</a>
                    </div>
                  </div>
                <div class="dropdown filter_permission">
                  <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                    Priority
                    <span class="icon md-chevron-down" aria-hidden="true"></span>
                  </button>
                  <div class="dropdown-menu" role="menu">
                    <a class="dropdown-item text-danger" href="javascript:void(0)"><i class="icon fa-circle mr-5" aria-hidden="true"></i>Highest</a>
                    <a class="dropdown-item text-warning" href="javascript:void(0)"><i class="icon fa-circle mr-5" aria-hidden="true"></i>High</a>
                    <a class="dropdown-item text-success" href="javascript:void(0)"><i class="icon fa-circle mr-5" aria-hidden="true"></i>Normal</a>
                    <a class="dropdown-item text-default" href="javascript:void(0)"><i class="icon fa-circle mr-5" aria-hidden="true"></i>Low</a>
                  </div>
                </div>
                <div class="dropdown filter_permission">
                  <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                    Status
                    <span class="icon md-chevron-down" aria-hidden="true"></span>
                  </button>
                  <div class="dropdown-menu" role="menu">
                    <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-cogs mr-5" aria-hidden="true"></i>Progress</a>
                    <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-check-square-o mr-5" aria-hidden="true"></i>Complete</a>
                    <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-adjust mr-5" aria-hidden="true"></i>Hold</a>
                  </div>
                </div>
                <div class="dropdown filter_permission">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      Category
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu" role="menu">
                      <a class="dropdown-item" href="javascript:void(0)"> Category 1</a>
                      <a class="dropdown-item" href="javascript:void(0)"> Category 2</a>
                      <a class="dropdown-item" href="javascript:void(0)"> Category 3</a>
                    </div>
                  </div>
            </div>
          </div>
        </div>

        <div >
          <div class="pb-20">
            <div class="actions-inner float-right">
              <div class="dropdown">
                <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                  <i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Disk space
                  <span class="icon md-chevron-down" aria-hidden="true"></span>
                </button>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-media w-100" role="menu">
                  <h6 class="pl-10" aria-hidden="false">Sory by</h6>
                  <a class="dropdown-item active" href="javascript:void(0)">Disk space</a>
                  <a class="dropdown-item" href="javascript:void(0)">Workspace name</a>
                  <a class="dropdown-item" href="javascript:void(0)">Status</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-sort-amount-asc" aria-hidden="true"></i> Ascending</a>
                  <a class="dropdown-item active" href="javascript:void(0)"><i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Descending</a>
                </div>
              </div>
            </div>


            <div class="pt-10">Total <strong>26</strong> rows</div>
          </div>

          <section >
              <div class="bg-white p-10">
          <table class="table table-hover" data-plugin="selectable" data-row-selectable="true">
            <thead>
              <tr>
                <th>
                  Project/Progress
                </th>
                <th width="35%" class="hidden-xs-down">
                  Strat/Finish
                </th>
                <th width="25%" class="hidden-md-down">
                  Disk space
                </th>
                <th width="5%">&nbsp;</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $priority_color = array("#f3273c","#ff9800","#4caf50","#757575");
              $pri = array("danger","warning","success","dark");
              $b=1;
              for($a=1;$a<=30;$a++){
                if($a==21) $b=1;
                $progress = rand(20,80);
                $rand_pri = rand(0,3);
              ?>
              <tr class="">
                <td>
                  <div class="clearfix mb-5">
                    <div class="text-<?=$pri[$rand_pri]?> float-left"><i class="icon fa-circle mr-5" aria-hidden="true"></i></div>
                    <div class="progress w-150 mb-0 mt-5 float-left" style="height:12px;" data-labeltype="percentage" data-plugin="progress">
                      <div class="progress-bar progress-bar-<?=$pri[$rand_pri]?>" role="progressbar" style="width: <?=$progress?>%; line-height:12px;">
                        <span class="progress-label font-size-10"><?=$progress?>%</span>
                      </div>
                    </div>
                    <?php
                    if(rand(0,1)){
                    ?>
                    <span class="badge badge-dark float-left ml-10 mt-3">External</span>
                    <?php }?>
                  </div>
                  <p class="mb-10 font-size-16 text-nowrap"><strong>Project Jigsaw Office 2017</strong></p>
                  
                  <div class="hidden-lg-up">
                    <div class="hidden-sm-up">
                      <h6 class="font-size-12 mb-5 mt-3">
                        <span class="w-50 grey-600" style="display: inline-table;">Strat</span>
                        <span style="font-weight: 100;">September 30, 2016</span></h6>
                      <h6 class="font-size-12 mb-10">
                        <span class="w-50 grey-600" style="display: inline-table;">Finish</span>
                        <span style="font-weight: 100;">September 23, 2017</span></h6>
                    </div>
                  </div>

                </td>
                <td class="hidden-xs-down">
                  <h6 class="font-size-12 mb-5 mt-3">
                    <span class="w-50 grey-600" style="display: inline-table;">Strat</span>
                    <span style="font-weight: 100;">September 30, 2016</span></h6>
                  <h6 class="font-size-12 mb-5">
                    <span class="w-50 grey-600" style="display: inline-table;">Finish</span>
                    <span style="font-weight: 100;">September 23, 2017</span></h6>
                </td>
                <td class="hidden-md-down">
                    <?php 
                    $disk = rand(30,95);
                    $bar = 100 - $disk;
                    ?>
                    <h5 class="m-0 mb-5 mt-3"><?=$disk?> MB free of 100 MB</h5>
                    <div class="progress progress-lg mb-0">
                      <div class="progress-bar progress-bar-<?=$bar>80?"danger":"info"?>" style="width: <?=$bar?>%;" role="progressbar"><?=$bar?>%</div>
                    </div>
                    
                </td>
                <td align="center">
                  <div class="dropdown">
                    <button class="btn btn-icon btn-round waves-effect waves-classic p-5 btn-pure" data-toggle="dropdown" href="#" aria-expanded="false">
                    <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                        Edit Disk space
                      </a>
                    </div>
                  </div>
                </td>
              </tr>
            <?php }?>
            </tbody>
          </table>
        </div>
          <div class="data-pagination">
              <div class="pagination-per-page">
                  <span class="page-list">
                    <span class="btn-group dropup">
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <span class="page-size">10</span>
                      </button>
                      <ul class="dropdown-menu dropdown-menu-bullet" role="menu">
                        <li role="menuitem" class="dropdown-item active"><a href="#">10</a></li>
                        <li role="menuitem" class="dropdown-item"><a href="#">25</a></li>
                      </ul>
                  </span> rows per page
                </span>
              </div>
              <nav>
                <ul class="pagination justify-content-center pagination-gap">
                  <li class="disabled page-item">
                    <a class="page-link" href="javascript:void(0)" aria-label="Previous">
                      <span aria-hidden="true">&laquo;</span>
                    </a>
                  </li>
                  <?php
                  $page = 4;
                  for($aa=1;$aa<=7;$aa++){?>
                  <li class="<?=($aa==$page?"active":"")?> page-item <?=($aa<($page-2) || $aa>($page+2)?"hidden-sm-down":"")?> <?=($aa==($page-2) || $aa==($page+2)?"hidden-xs-down":"")?>"><a class="page-link" href="javascript:void(0)"><?=$aa?></a></li>
                  <?php }?>
                  <li class="page-item">
                    <a class="page-link" href="javascript:void(0)" aria-label="Next">
                      <span aria-hidden="true">&raquo;</span>
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
        </section>
</div>

        </div>
        </div>
      </div>
    </div>

  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <script src="../../global/vendor/aspieprogress/jquery-asPieProgress.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../global/js/Plugin/aspieprogress.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>