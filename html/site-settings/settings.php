<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../assets/examples/css/forms/layouts.css">
  <link rel="stylesheet" href="../../global/vendor/icheck/icheck.css">
  <link rel="stylesheet" href="../../assets/examples/css/advanced/animation.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition app-documents">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-5">Applications Management</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../home/index.php">Home</a></li>
        <li class="breadcrumb-item">Site Setting</li>
      </ol>
      
      <?php include("mini-nav.php");?>
    </div>

    <div class="page-content container-fluid bg-white" style="position: relative;">
      <div class="page-main">
        <div class="row pt-20 ml-0 mr-0 justify-content-md-center">
          <div class="col-md-8">
            <h4 class="example-title">General Settings</h4>
            <form class="form-horizontal">
              <div class="form-group row form-material row">
                <label class="col-md-4 form-control-label">Site Name: </label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="name" placeholder="Company Name" autocomplete="off"
                  />
                </div>
              </div>
              <div class="form-group row form-material row">
                <label class="col-md-4 form-control-label">Domain: </label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="name" value="jigsawoffice.com" disabled="" autocomplete="off"
                  />
                </div>
              </div>
              <div class="form-group row form-material row">
                <label class="col-md-4 form-control-label">Description: </label>
                <div class="col-md-8">
                  <textarea class="form-control" placeholder="Description"></textarea>
                </div>
              </div>
              <div class="form-group row form-material row">
                <label class="col-md-4 form-control-label">Industry : </label>
                <div class="col-md-8">
                  <div class="btn-group bootstrap-select">
                    <select data-plugin="selectpicker">
                      <option>Agro & Food Industry</option>
                      <option>Consumer Products</option>
                      <option>Financials</option>
                      <option>Industrials</option>
                      <option>Property & Construction</option>
                      <option>Resources</option>
                      <option>Services</option>
                      <option>Technology</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group row form-material row">
                <label class="col-md-4 form-control-label">Sector : </label>
                <div class="col-md-8">
                  <div class="btn-group bootstrap-select">
                    <select data-plugin="selectpicker">
                      <option>Agribusiness</option>
                      <option>Food & Beverage</option>
                      <option>Fashion</option>
                      <option>Home & Office Products</option>
                      <option>Personal Products & Pharmaceuticals</option>
                      <option>Banking</option>
                      <option>Finance & Securities</option>
                      <option>Insurance</option>
                      <option>Automotive</option>
                      <option>Industrial Materials & Machine</option>
                      <option>Packaging</option>
                      <option>Paper & Printing Materials</option>
                      <option>Petrochemicals & Chemicals</option>
                      <option>Steel</option>
                      <option>Construction Materials</option>
                      <option>Construction Services</option>
                      <option>Property Development</option>
                      <option>Energy & Utilities</option>
                      <option>Mining</option>
                      <option>Commerce</option>
                      <option>Health Care Services</option>
                      <option>Media & Publishing</option>
                      <option>Professional Services</option>
                      <option>Tourisms & Leisure</option>
                      <option>Transportation & Logistics</option>
                      <option>Electronic Components</option>
                      <option>Information & Communication Technology</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group row form-material row">
                <label class="col-md-4 form-control-label">Number of employees: </label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="name" placeholder="Number of employees" autocomplete="off"
                  />
                </div>
              </div>
              <div class="form-group row form-material row">
                <label class="col-md-4 form-control-label">Country : </label>
                <div class="col-md-8">
                  <div class="btn-group bootstrap-select">
                    <select data-plugin="selectpicker">
                      <option value="">- Select -</option>
                      <option value="AF">Afghanistan</option>
                      <option value="AX">Aland Islands</option>
                      <option value="AL">Albania</option>
                      <option value="DZ">Algeria</option>
                      <option value="AS">American Samoa</option>
                      <option value="AD">Andorra</option>
                      <option value="AO">Angola</option>
                      <option value="AI">Anguilla</option>
                      <option value="AQ">Antarctica</option>
                      <option value="AG">Antigua And Barbuda</option>
                      <option value="AR">Argentina</option>
                      <option value="AM">Armenia</option>
                      <option value="AW">Aruba</option>
                      <option value="AU">Australia</option>
                      <option value="AT">Austria</option>
                      <option value="AZ">Azerbaijan</option>
                      <option value="BS">Bahamas</option>
                      <option value="BH">Bahrain</option>
                      <option value="BD">Bangladesh</option>
                      <option value="BB">Barbados</option>
                      <option value="BY">Belarus</option>
                      <option value="BE">Belgium</option>
                      <option value="BZ">Belize</option>
                      <option value="BJ">Benin</option>
                      <option value="BM">Bermuda</option>
                      <option value="BT">Bhutan</option>
                      <option value="BO">Bolivia, Plurinational State Of</option>
                      <option value="BA">Bosnia And Herzegovina</option>
                      <option value="BW">Botswana</option>
                      <option value="BV">Bouvet Island</option>
                      <option value="BR">Brazil</option>
                      <option value="IO">British Indian Ocean Territory</option>
                      <option value="BN">Brunei Darussalam</option>
                      <option value="BG">Bulgaria</option>
                      <option value="BF">Burkina Faso</option>
                      <option value="BI">Burundi</option>
                      <option value="KH">Cambodia</option>
                      <option value="CM">Cameroon</option>
                      <option value="CA">Canada</option>
                      <option value="CV">Cape Verde</option>
                      <option value="KY">Cayman Islands</option>
                      <option value="CF">Central African Republic</option>
                      <option value="TD">Chad</option>
                      <option value="CL">Chile</option>
                      <option value="CN">China</option>
                      <option value="CX">Christmas Island</option>
                      <option value="CC">Cocos (Keeling) Islands</option>
                      <option value="CO">Colombia</option>
                      <option value="KM">Comoros</option>
                      <option value="CG">Congo</option>
                      <option value="CD">Congo, The Democratic Republic Of The</option>
                      <option value="CK">Cook Islands</option>
                      <option value="CR">Costa Rica</option>
                      <option value="CI">Cote D'Ivoire</option>
                      <option value="HR">Croatia</option>
                      <option value="CU">Cuba</option>
                      <option value="CY">Cyprus</option>
                      <option value="CZ">Czech Republic</option>
                      <option value="DK">Denmark</option>
                      <option value="DJ">Djibouti</option>
                      <option value="DM">Dominica</option>
                      <option value="DO">Dominican Republic</option>
                      <option value="EC">Ecuador</option>
                      <option value="EG">Egypt</option>
                      <option value="SV">El Salvador</option>
                      <option value="GQ">Equatorial Guinea</option>
                      <option value="ER">Eritrea</option>
                      <option value="EE">Estonia</option>
                      <option value="ET">Ethiopia</option>
                      <option value="FK">Falkland Islands (Malvinas)</option>
                      <option value="FO">Faroe Islands</option>
                      <option value="FJ">Fiji</option>
                      <option value="FI">Finland</option>
                      <option value="FR">France</option>
                      <option value="GF">French Guiana</option>
                      <option value="PF">French Polynesia</option>
                      <option value="TF">French Southern Territories</option>
                      <option value="GA">Gabon</option>
                      <option value="GM">Gambia</option>
                      <option value="GE">Georgia</option>
                      <option value="DE">Germany</option>
                      <option value="GH">Ghana</option>
                      <option value="GI">Gibraltar</option>
                      <option value="GR">Greece</option>
                      <option value="GL">Greenland</option>
                      <option value="GD">Grenada</option>
                      <option value="GP">Guadeloupe</option>
                      <option value="GU">Guam</option>
                      <option value="GT">Guatemala</option>
                      <option value="GG">Guernsey</option>
                      <option value="GN">Guinea</option>
                      <option value="GW">Guinea-Bissau</option>
                      <option value="GY">Guyana</option>
                      <option value="HT">Haiti</option>
                      <option value="HM">Heard Island And Mcdonald Islands</option>
                      <option value="VA">Holy See (Vatican City State)</option>
                      <option value="HN">Honduras</option>
                      <option value="HK">Hong Kong</option>
                      <option value="HU">Hungary</option>
                      <option value="IS">Iceland</option>
                      <option value="IN">India</option>
                      <option value="ID">Indonesia</option>
                      <option value="IR">Iran, Islamic Republic Of</option>
                      <option value="IQ">Iraq</option>
                      <option value="IE">Ireland</option>
                      <option value="IM">Isle Of Man</option>
                      <option value="IL">Israel</option>
                      <option value="IT">Italy</option>
                      <option value="JM">Jamaica</option>
                      <option value="JP">Japan</option>
                      <option value="JE">Jersey</option>
                      <option value="JO">Jordan</option>
                      <option value="KZ">Kazakhstan</option>
                      <option value="KE">Kenya</option>
                      <option value="KI">Kiribati</option>
                      <option value="KP">Korea, Democratic People's Republic Of</option>
                      <option value="KR">Korea, Republic Of</option>
                      <option value="KW">Kuwait</option>
                      <option value="KG">Kyrgyzstan</option>
                      <option value="LA">Lao People's Democratic Republic</option>
                      <option value="LV">Latvia</option>
                      <option value="LB">Lebanon</option>
                      <option value="LS">Lesotho</option>
                      <option value="LR">Liberia</option>
                      <option value="LY">Libyan Arab Jamahiriya</option>
                      <option value="LI">Liechtenstein</option>
                      <option value="LT">Lithuania</option>
                      <option value="LU">Luxembourg</option>
                      <option value="MO">Macao</option>
                      <option value="MK">Macedonia, The Former Yugoslav Republic Of</option>
                      <option value="MG">Madagascar</option>
                      <option value="MW">Malawi</option>
                      <option value="MY">Malaysia</option>
                      <option value="MV">Maldives</option>
                      <option value="ML">Mali</option>
                      <option value="MT">Malta</option>
                      <option value="MH">Marshall Islands</option>
                      <option value="MQ">Martinique</option>
                      <option value="MR">Mauritania</option>
                      <option value="MU">Mauritius</option>
                      <option value="YT">Mayotte</option>
                      <option value="MX">Mexico</option>
                      <option value="FM">Micronesia, Federated States Of</option>
                      <option value="MD">Moldova, Republic Of</option>
                      <option value="MC">Monaco</option>
                      <option value="MN">Mongolia</option>
                      <option value="ME">Montenegro</option>
                      <option value="MS">Montserrat</option>
                      <option value="MA">Morocco</option>
                      <option value="MZ">Mozambique</option>
                      <option value="MM">Myanmar</option>
                      <option value="NA">Namibia</option>
                      <option value="NR">Nauru</option>
                      <option value="NP">Nepal</option>
                      <option value="NL">Netherlands</option>
                      <option value="AN">Netherlands Antilles</option>
                      <option value="NC">New Caledonia</option>
                      <option value="NZ">New Zealand</option>
                      <option value="NI">Nicaragua</option>
                      <option value="NE">Niger</option>
                      <option value="NG">Nigeria</option>
                      <option value="NU">Niue</option>
                      <option value="NF">Norfolk Island</option>
                      <option value="MP">Northern Mariana Islands</option>
                      <option value="NO">Norway</option>
                      <option value="OM">Oman</option>
                      <option value="PK">Pakistan</option>
                      <option value="PW">Palau</option>
                      <option value="PS">Palestinian Territory, Occupied</option>
                      <option value="PA">Panama</option>
                      <option value="PG">Papua New Guinea</option>
                      <option value="PY">Paraguay</option>
                      <option value="PE">Peru</option>
                      <option value="PH">Philippines</option>
                      <option value="PL">Poland</option>
                      <option value="PT">Portugal</option>
                      <option value="PR">Puerto Rico</option>
                      <option value="QA">Qatar</option>
                      <option value="RE">Reunion</option>
                      <option value="RO">Romania</option>
                      <option value="RU">Russian Federation</option>
                      <option value="RW">Rwanda</option>
                      <option value="SH">Saint Helena, Ascension And Tristan Da Cunha</option>
                      <option value="KN">Saint Kitts And Nevis</option>
                      <option value="LC">Saint Lucia</option>
                      <option value="MF">Saint Martin</option>
                      <option value="PM">Saint Pierre And Miquelon</option>
                      <option value="VC">Saint Vincent And The Grenadines</option>
                      <option value="WS">Samoa</option>
                      <option value="SM">San Marino</option>
                      <option value="ST">Sao Tome And Principe</option>
                      <option value="SA">Saudi Arabia</option>
                      <option value="SN">Senegal</option>
                      <option value="RS">Serbia</option>
                      <option value="SC">Seychelles</option>
                      <option value="SL">Sierra Leone</option>
                      <option value="SG">Singapore</option>
                      <option value="SK">Slovakia</option>
                      <option value="SI">Slovenia</option>
                      <option value="SB">Solomon Islands</option>
                      <option value="SO">Somalia</option>
                      <option value="ZA">South Africa</option>
                      <option value="GS">South Georgia And The South Sandwich Islands</option>
                      <option value="ES">Spain</option>
                      <option value="LK">Sri Lanka</option>
                      <option value="SD">Sudan</option>
                      <option value="SR">Suriname</option>
                      <option value="SJ">Svalbard And Jan Mayen</option>
                      <option value="SZ">Swaziland</option>
                      <option value="SE">Sweden</option>
                      <option value="CH">Switzerland</option>
                      <option value="SY">Syrian Arab Republic</option>
                      <option value="TW">Taiwan</option>
                      <option value="TJ">Tajikistan</option>
                      <option value="TZ">Tanzania, United Republic Of</option>
                      <option selected="selected" value="TH">Thailand</option>
                      <option value="TL">Timor-Leste</option>
                      <option value="TG">Togo</option>
                      <option value="TK">Tokelau</option>
                      <option value="TO">Tonga</option>
                      <option value="TT">Trinidad And Tobago</option>
                      <option value="TN">Tunisia</option>
                      <option value="TR">Turkey</option>
                      <option value="TM">Turkmenistan</option>
                      <option value="TC">Turks And Caicos Islands</option>
                      <option value="TV">Tuvalu</option>
                      <option value="UG">Uganda</option>
                      <option value="UA">Ukraine</option>
                      <option value="AE">United Arab Emirates</option>
                      <option value="UK">United Kingdom</option>
                      <option value="US">United States</option>
                      <option value="UM">United States Minor Outlying Islands</option>
                      <option value="UY">Uruguay</option>
                      <option value="UZ">Uzbekistan</option>
                      <option value="VU">Vanuatu</option>
                      <option value="VE">Venezuela, Bolivarian Republic Of</option>
                      <option value="VN">Viet Nam</option>
                      <option value="VG">Virgin Islands, British</option>
                      <option value="VI">Virgin Islands, U.S.</option>
                      <option value="WF">Wallis And Futuna</option>
                      <option value="EH">Western Sahara</option>
                      <option value="YE">Yemen</option>
                      <option value="ZM">Zambia</option>
                      <option value="ZW">Zimbabwe</option>

                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group row form-material row">
                <label class="col-md-4 form-control-label">State/Province: </label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="name" placeholder="State/Province" autocomplete="off"
                  />
                </div>
              </div>
              <div class="form-group row form-material row">
                <label class="col-md-4 form-control-label">Who can join company: </label>
                <div class="col-md-8">
                  <ul class="list-unstyled example">
                    <li class="mb-15">
                      <input type="radio" class="icheckbox-primary" id="inputRadiosUnchecked11" name="inputRadios11"
                      data-plugin="iCheck" data-radio-class="iradio_flat-blue" checked />
                      <label for="inputRadiosUnchecked11">Anyone in the company with a valid email address (Recommended)</label>
                    </li>
                    <li class="mb-15">
                      <input type="radio" class="icheckbox-primary" id="inputRadiosChecked12" name="inputRadios11"
                      data-plugin="iCheck" data-radio-class="iradio_flat-blue"/>
                      <label for="inputRadiosChecked12">Only people I choose to invite</label>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="form-group row form-material row">
                <label class="col-md-4 form-control-label">Theme Color: </label>
                <div class="col-md-8">
                  <ul class="list-unstyled example">
                    <li class="mb-15 float-left mr-70">
                      <input type="radio" class="icheckbox-primary" id="inputRadiosUnchecked" name="inputRadios"
                      data-plugin="iCheck" data-radio-class="iradio_flat-blue" checked />
                      <label for="inputRadiosUnchecked"><span class="badge badge-lg w-100" style="background-color:#0c84e4;">Blue</span></label>
                    </li>
                    <li class="mb-15 float-left mr-70">
                      <input type="radio" class="icheckbox-primary" id="inputRadiosChecked" name="inputRadios"
                      data-plugin="iCheck" data-radio-class="iradio_flat-blue"/>
                      <label for="inputRadiosChecked"><span class="badge badge-lg w-100" style="background-color:#63453b;">Brown</span></label>
                    </li>
                    <li class="mb-15 float-left mr-70">
                      <input type="radio" class="icheckbox-primary" id="inputRadiosChecked" name="inputRadios"
                      data-plugin="iCheck" data-radio-class="iradio_flat-blue"/>
                      <label for="inputRadiosChecked"><span class="badge badge-lg w-100" style="background-color:#009cb0;">Cyan</span></label>
                    </li>
                    <li class="mb-15 float-left mr-70">
                      <input type="radio" class="icheckbox-primary" id="inputRadiosChecked" name="inputRadios"
                      data-plugin="iCheck" data-radio-class="iradio_flat-blue"/>
                      <label for="inputRadiosChecked"><span class="badge badge-lg w-100" style="background-color:#419645;">Green</span></label>
                    </li>
                    <li class="mb-15 float-left mr-70">
                      <input type="radio" class="icheckbox-primary" id="inputRadiosChecked" name="inputRadios"
                      data-plugin="iCheck" data-radio-class="iradio_flat-blue"/>
                      <label for="inputRadiosChecked"><span class="badge badge-lg w-100" style="background-color:#8c8c8c;">Grey</span></label>
                    </li>
                    <li class="mb-15 float-left mr-70">
                      <input type="radio" class="icheckbox-primary" id="inputRadiosChecked" name="inputRadios"
                      data-plugin="iCheck" data-radio-class="iradio_flat-blue"/>
                      <label for="inputRadiosChecked"><span class="badge badge-lg w-100" style="background-color:#36459b;">Indigo</span></label>
                    </li>
                    <li class="mb-15 float-left mr-70">
                      <input type="radio" class="icheckbox-primary" id="inputRadiosChecked" name="inputRadios"
                      data-plugin="iCheck" data-radio-class="iradio_flat-blue"/>
                      <label for="inputRadiosChecked"><span class="badge badge-lg w-100" style="background-color:#db8300;">Orange</span></label>
                    </li>
                    <li class="mb-15 float-left mr-70">
                      <input type="radio" class="icheckbox-primary" id="inputRadiosChecked" name="inputRadios"
                      data-plugin="iCheck" data-radio-class="iradio_flat-blue"/>
                      <label for="inputRadiosChecked"><span class="badge badge-lg w-100" style="background-color:#cf1454;">Pink</span></label>
                    </li>
                    <li class="mb-15 float-left mr-70">
                      <input type="radio" class="icheckbox-primary" id="inputRadiosChecked" name="inputRadios"
                      data-plugin="iCheck" data-radio-class="iradio_flat-blue"/>
                      <label for="inputRadiosChecked"><span class="badge badge-lg w-100" style="background-color:#822193;">Purple</span></label>
                    </li>
                    <li class="mb-15 float-left mr-70">
                      <input type="radio" class="icheckbox-primary" id="inputRadiosChecked" name="inputRadios"
                      data-plugin="iCheck" data-radio-class="iradio_flat-blue"/>
                      <label for="inputRadiosChecked"><span class="badge badge-lg w-100" style="background-color:#f22314;">Red</span></label>
                    </li>
                    <li class="mb-15 float-left mr-70">
                      <input type="radio" class="icheckbox-primary" id="inputRadiosChecked" name="inputRadios"
                      data-plugin="iCheck" data-radio-class="iradio_flat-blue"/>
                      <label for="inputRadiosChecked"><span class="badge badge-lg w-100" style="background-color:#007268;">Teal</span></label>
                    </li>
                  </ul>
                </div>
              </div>

              <div class="form-group row form-material row">
                <div class="col-md-9 offset-md-3">
                  <button type="button" class="btn btn-primary mr-10">Submit </button>
                  <button type="reset" class="btn btn-warning">Reset</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <script src="../../global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
  <script src="../../global/vendor/icheck/icheck.min.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../assets/js/BaseApp.js"></script>
  <script src="../../assets/js/App/Contacts.js"></script>
  <script src="../../assets/examples/js/apps/contacts.js"></script>
  <script src="../../global/js/Plugin/jquery-placeholder.js"></script>
  <script src="../../global/js/Plugin/icheck.js"></script>
<?php include("../_footer-form.php");?>
</body>
</html>
