
<section>
  <div class="bg-grey-600 white p-15 clearfix" style="padding-bottom: 0px;">
    <span class="hidden-xs-down avatar avatar-100 float-left mr-20 img-bordered bg-white" href="javascript:void(0)">
      <img src="../../assets/examples/graph/graph-4.jpg" alt="">
    </span>
    <span class="hidden-sm-up avatar avatar-lg float-left mr-20 img-bordered bg-white" href="javascript:void(0)">
      <img src="../../assets/examples/graph/graph-4.jpg" alt="">
    </span>
    <div class="font-size-24"><strong>Intranet</strong></div>

    <div style="position:absolute;right:0;top:0;">
        <button type="button" class="btn btn-pure btn-inverse font-size-30 slidePanel-close icon md-close" aria-hidden="true"></button>
    </div>
    <div class="mt-10">
        <p class="">
          Ex quis excepteur exercitation incididunt ea amet commodo deserunt nulla. Anim sit duis cupidatat ipsum deserunt reprehenderit fugiat cupidatat.
        </p>
    </div>
  </div>
</section>
<div class="site-sidebar-tab-content tab-content">
  <div class="tab-pane fade active show">
    <div>
      <div>
        <div class="row">
          <div class="col-md-12">
            <div class="example-wrap">
                <div class="nav-tabs-horizontal" data-plugin="tabs">
                  <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" href="#exampleTabsOne" aria-controls="exampleTabsOne" role="tab" aria-expanded="true">Module Config</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleTabsTwo" aria-controls="exampleTabsTwo" role="tab" aria-expanded="false">On/Off Submenu</a></li>
                  </ul>
                  <div class="tab-content pt-20">
                    <div class="tab-pane active" id="exampleTabsOne" role="tabpanel" aria-expanded="true">
                      <section>
                        
                      </section>
                      
                    </div>
                    <div class="tab-pane" id="exampleTabsTwo" role="tabpanel" aria-expanded="false">
                      <h4>Reviews</h4>
                      <div class="mb-10 p-5">
                        <ul class="sub-menu list-unstyled pb-20">
                            <li style="pb-10"><input type="checkbox" name="n1" data-plugin="switchery" checked />  <span class="pl-20">About Company</span></li>
                            <li style="pb-10"><input type="checkbox" name="n1" data-plugin="switchery" checked />  <span class="pl-20">Org Chart</span></li>
                            <li style="pb-10"><input type="checkbox" name="n1" data-plugin="switchery" checked />  <span class="pl-20">Staff</span></li>
                            <li style="pb-10"><input type="checkbox" name="n1" data-plugin="switchery" checked />  <span class="pl-20">News</span></li>
                            <li style="pb-10"><input type="checkbox" name="n1" data-plugin="switchery" checked />  <span class="pl-20">Announcement</span></li>
                            <li style="pb-10"><input type="checkbox" name="n1" data-plugin="switchery" checked />  <span class="pl-20">Videos</span></li>
                            <li style="pb-10"><input type="checkbox" name="n1" data-plugin="switchery" checked />  <span class="pl-20">Photos</span></li>
                            <li style="pb-10"><input type="checkbox" name="n1" data-plugin="switchery" checked />  <span class="pl-20">Events</span></li>
                            <li style="pb-10"><input type="checkbox" name="n1" data-plugin="switchery" checked />  <span class="pl-20">Calendar</span></li>
                            <li style="pb-10"><input type="checkbox" name="n1" data-plugin="switchery" checked />  <span class="pl-20">Policy</span></li>
                        </ul>
                        <button type="button" class="btn btn-primary mr-10">Save Config</button>
                      </div>

                    </div>
                  </div>
                </div>
              </div>

            

          </div>
          
        </div>
      </div>
    </div>
  </div>

</div>