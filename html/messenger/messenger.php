<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../assets/examples/css/apps/message.css">
  <link rel="stylesheet" href="../../global/vendor/jquery-selective/jquery-selective.css">
  <link rel="stylesheet" href="../../global/vendor/toolbar/toolbar.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition app-message page-aside-scroll page-aside-left">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?> 
  <div class="page">
    <!-- Message Sidebar -->
    <div class="page-aside">
      <div class="page-aside-switch">
        <i class="icon md-chevron-left" aria-hidden="true"></i>
        <i class="icon md-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner pt-20" style="margin-top:-20px;">
        <section style="border-bottom:1px solid #e0e0e0;">
          <h3 class="pl-10 mt-20">Messenger</h3>
          <div style="position:absolute;right:0;top:19px;">
              <button type="button" class="btn btn-pure font-size-20 p-5 icon fa-gear" aria-hidden="true"></button>
              <button type="button" class="btn btn-pure font-size-20 p-5 mr-5 icon fa-edit" aria-hidden="true"></button>
          </div>
        </section>
        <section>
          <div class="input-search input-search-dark m-10 ml-10">
            <i class="input-search-icon md-search" aria-hidden="true" style="left:0;"></i>
            <input class="form-control" name="" placeholder="Search Messenger" type="text" style="border-radius:0.215rem;">
            <button type="button" class="input-search-close icon md-close" aria-label="Close" style="right:0;"></button>
          </div>
          <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item" role="presentation" style="width:32.5%;text-align:center;">
              <a class="nav-link active font-size-20 p-0" data-toggle="tab" href="#exampleTabsIconOne" aria-controls="exampleTabsIconOne" role="tab">
                <i class="icon fa-wechat m-0" aria-hidden="true"></i>
              </a>
            </li>
            <li class="nav-item" role="presentation" style="width:32.5%;text-align:center;">
              <a class="nav-link font-size-20 p-0" data-toggle="tab" href="#exampleTabsIconTwo" aria-controls="exampleTabsIconTwo" role="tab">
                <i class="icon fa-user m-0" aria-hidden="true"></i>
              </a>
            </li>
            <li class="nav-item" role="presentation" style="width:32.5%;text-align:center;">
              <a class="nav-link font-size-20 p-0" data-toggle="tab" href="#exampleTabsIconThree" aria-controls="exampleTabsIconThree" role="tab">
                <i class="icon fa-group m-0" aria-hidden="true"></i>
              </a>
            </li>
          </ul>
        </section>
        <div class="app-message-list page-aside-scroll">
          <div data-role="container">
            <div data-role="content">
              <div class="tab-content pt-5">
                <div class="tab-pane active" id="exampleTabsIconOne" role="tabpanel">
                  <ul class="list-group">
                    <?php 
                    $b=1;
                    for($a=1;$a<=15;$a++){
                      $online = rand(0,1);
                    ?>
                    <li class="list-group-item p-5 pr-10 pl-10 <?php echo ($a==4?"active":"")?>">
                      <div class="media" style="flex-direction:initial;">
                        <div class="pr-10">
                          <a class="avatar avatar-online" href="javascript:void(0)">
                            <img class="img-fluid" src="../../global/portraits/<?=$b++?>.jpg" alt="...">
                            <?php if($online){?>
                            <i></i>
                            <?php }?>
                          </a>
                        </div>
                        <div class="media-body">
                          <?php if($a<4){?>
                          <div class="mt-0 black"><strong>Edward Fletcher</strong></div>
                          <small class="black"><strong>Consectetuorem ipsum dolor sit?</strong></small>
                          <?php }else{?>
                          <div class="mt-0 grey-700">Caleb Richards</div>
                          <small class="grey-400">Consectetuorem ipsum dolor sit?</small>
                          <?php }?>
                        </div>
                        <div class="font-size-10 <?php echo ($a<4?"blue-600":"grey-500");?>" style="position:absolute;right:10px;top:10px;">
                          1:44pm
                        </div>
                      </div>
                    </li>
                    <?php 
                    }
                    ?>
                  </ul>
                </div>
                <div class="tab-pane" id="exampleTabsIconTwo" role="tabpanel">
                  <ul class="list-group">
                    <?php 
                    $b=1;
                    for($a=1;$a<=25;$a++){
                      $online = rand(0,1);
                      if($a==21) $b=1;
                    ?>
                    <li class="list-group-item p-5 pr-10 pl-10">
                      <div class="media" style="flex-direction:initial;">
                        <div class="pr-10">
                          <a class="avatar avatar-online" href="javascript:void(0)">
                            <img class="img-fluid" src="../../global/portraits/<?=$b++?>.jpg" alt="...">
                            <?php if($online){?>
                            <i></i>
                            <?php }?>
                          </a>
                        </div>
                        <div class="media-body">
                          <div class="mt-10 black">Edward Fletcher</div>
                        </div>
                      </div>
                    </li>
                    <?php }?>
                  </ul>
                </div>
                <div class="tab-pane" id="exampleTabsIconThree" role="tabpanel">
                  <ul class="list-group">
                    <?php
                    $b=1;
                    for($a=1;$a<=10;$a++){
                      $online = rand(0,1);
                      $tmpC = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
                    ?>
                    <li class="list-group-item p-5 pr-10 pl-10">
                      <div class="media" style="flex-direction:initial;">
                        <div class="pr-10">
                          <button type="button" style="background:<?php echo $tmpC;?>;" class="btn btn-icon btn-round waves-effect waves-classic">
                          <i class="icon fa-group mr-0 white" aria-hidden="true"></i>
                          </button>
                        </div>
                        <div class="media-body">
                          <div class="black">Jigsaw Office R&D</div>
                          <ul class="addMember-items">
                            <?php for($aa=1;$aa<=rand(3,7);$aa++){?>
                            <li class="addMember-item mr-0">
                              <img style="width:20px;" class="avatar" src="../../global/portraits/<?php echo rand(1,20);?>.jpg" title="Herman Beck">
                            </li>
                            <?php }?>
                          </ul>
                          <div class="addMember-trigger pt-1 font-size-10">
                            <strong>+<?=rand(2,10)?></strong>
                          </div>
                        </div>
                      </div>
                    </li>
                    <?php }?>
                  </ul>
                </div>
              </div>

              
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Message Sidebar -->
    <div class="page-main">
      <!-- Chat Box -->
      <section style="border-bottom: 1px solid rgb(224, 224, 224); height: 61px; position: relative;text-align:center;">
          <div  class="hidden-md-up" style="position:absolute;left:15px;top:10px;">
              <button id="app-message-fullscreen" type="button" class="btn btn-pure font-size-26 p-5 icon md-fullscreen" aria-hidden="true"></button>
          </div>
          <div class="font-size-14 pt-10">
            <strong>Caleb Richards</strong>
            <p class="grey-500"><small>Active 10m ago</small></p>
          </div>
          <div style="position:absolute;right:15px;top:10px;">
              <button data-url="panel.html" data-toggle="slidePanel" type="button" class="btn btn-pure font-size-26 p-5 icon md-info" aria-hidden="true"></button>
          </div>
        </section>
      <div class="app-message-chats">
        <button type="button" id="historyBtn" class="btn btn-round btn-default btn-flat primary-500">History Messages</button>
        <div class="chats mb-20">
          <div class="chat">
            <div class="chat-body">
              <div class="chat-content">
                <p>
                  Hello. What can I do for you?
                </p>
              </div>
            </div>
          </div>
          <div class="chat chat-left">
            <div class="chat-avatar">
              <a class="avatar" data-toggle="tooltip" href="#" data-placement="left" title="">
                <img src="../../global/portraits/5.jpg" alt="Edward Fletcher">
              </a>
            </div>
            <div class="chat-body">
              <div class="chat-content bg-grey-200">
                <p>
                  I'm just looking around.
                </p>
                <p>Will you tell me something about yourself? </p>
              </div>
              <div class="chat-content bg-grey-200">
                <p>
                  Are you there? That time!
                </p>
              </div>
            </div>
          </div>
          <div class="chat">
            <div class="chat-body">
              <div class="chat-content">
                <p>
                  Where?
                </p>
              </div>
              <div class="chat-content">
                <p>
                  OK, my name is Limingqiang. I like singing, playing basketballand so on.
                </p>
              </div>
            </div>
          </div>
          <p class="time">1 hours ago</p>
          <div class="chat chat-left">
            <div class="chat-avatar">
              <a class="avatar" data-toggle="tooltip" href="#" data-placement="left" title="">
                <img src="../../global/portraits/5.jpg" alt="Edward Fletcher">
              </a>
            </div>
            <div class="chat-body">
              <div class="chat-content bg-grey-200">
                <p>You wait for notice.</p>
              </div>
              <div class="chat-content bg-grey-200">
                <p>Consectetuorem ipsum dolor sit?</p>
              </div>
              <div class="chat-content bg-grey-200">
                <p>OK?</p>
              </div>
            </div>
          </div>
          <div class="chat">
            <div class="chat-body">
              <div class="chat-content">
                <p>OK!</p>
              </div>
            </div>
          </div>
          <div class="chat chat-left">
            <div class="chat-avatar">
              <a class="avatar" data-toggle="tooltip" href="#" data-placement="left" title="">
                <img src="../../global/portraits/5.jpg" alt="Edward Fletcher">
              </a>
            </div>
            <div class="chat-body">
              <div class="chat-content bg-grey-200">
                <p>Crud reran and while much withdrew ardent much crab hugely met dizzily that more jeez gent equivalent unsafely far one hesitant so therefore..</p>
              </div>
            </div>
          </div>
          <div class="chat">
            <div class="chat-body">
              <div class="chat-content">
                <p>Far squid and that hello fidgeted and when. As this oh darn but slapped casually husky sheared that cardinal hugely one and some unnecessary factiously hedgehog a feeling one rudely much</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End Chat Box -->
      <!-- Message Input-->
      <form class="app-message-input pt-10" style="border-top: 1px solid rgb(224, 224, 224);">
        <div class="input-group">
          <input class="form-control" style="border:0;" type="text" placeholder="Type message here ...">
          <span class="input-group-btn">
            <button type="button" class="btn btn-pure btn-default icon md-image font-size-24"></button>
            <button type="button" class="btn btn-pure btn-default icon md-attachment-alt font-size-24"></button>
            <button type="button" class="btn btn-pure btn-default icon md-camera font-size-24"></button>
            <button type="button" class="btn btn-pure btn-default icon md-thumb-up font-size-24"></button>
          </span>
        </div>
      </form>
      <!-- End Message Input-->
    </div>
  </div>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/autosize/autosize.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  $('.nav-item a').click(function(){
    $('.scrollable-container').animate({scrollTop: '0px'}, 200);
  });
  $("#app-message-fullscreen").click(function (e) {
        e.preventDefault();
        var $this = $(this);
        if ($this.hasClass('md-fullscreen'))
        {
            $this.removeClass('md-fullscreen');
            $this.addClass('md-fullscreen-exit');
        }
        else if ($this.hasClass('md-fullscreen-exit'))
        {
            $this.removeClass('md-fullscreen-exit');
            $this.addClass('md-fullscreen');
        }
        $(this).closest('.page-main').toggleClass('app-message-fullscreen');
    });
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/jquery-placeholder.js"></script>
  <script src="../../global/js/Plugin/input-group-file.js"></script>
  <script src="../../global/js/Plugin/panel.js"></script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../assets/js/App/Message.js"></script>
  <script src="../../assets/examples/js/apps/message.js"></script>
<?php include("../_footer-form.php");?>
</body>
</html>