<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <style type="text/css">
  .fix-mini-nav + .page-content{
    padding-top: 0;
  }
  .fix-mini-nav .page-header{
    margin-bottom: 93px;
  }
  .fix-mini-nav .fix-mini-nav{
    margin-bottom: 0;
    margin-right: 0;
    margin-top: 0;
    padding: 0;
    position: fixed;
    top: 65px;
    width: 100%;
    z-index: 10;
    left: 0;
  }
  .btn-comment-post .btn-select{
    border: 0px !important;
  }
  .btn-comment-post, .btn-comment-post .bootstrap-select{
    width: 150px !important;
  }
  </style>
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition app-documents page-aside-left">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header">
      <h1 class="page-title mb-10">Network</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item active">Network</li>
      </ol>
    </div>
    <?php include("../workspace/mini-nav-ws.php");?>
    <div class="page-content container-fluid bg-white" style="position: relative;">
      <div class="page-aside">
        <!-- Contacts Sidebar -->
        <div class="page-aside-switch">
          <i class="icon md-chevron-left" aria-hidden="true"></i>
          <i class="icon md-chevron-right" aria-hidden="true"></i>
        </div>
        <div class="page-aside-inner page-aside-scroll">
          <div data-role="container">
            <div data-role="content">
              <div class="page-aside-section">
                <h5 class="page-aside-title">Folders</h5>
                <div class="list-group-item active" data-plugin="editlist">
                  <div class="list-content">
                    <span class="list-text">All</span>
                  </div>
                </div>
                <div class="list-group has-actions">
                  <div class="list-group-item" data-plugin="editlist">
                    <div class="list-content">
                      <span class="item-right">30</span>
                      <span class="list-text">Active</span>
                    </div>
                  </div>
                  <div class="list-group-item" data-plugin="editlist">
                    <div class="list-content">
                      <span class="item-right">55</span>
                      <span class="list-text">Archive</span>
                    </div>
                  </div>
                  <div class="list-group-item" data-plugin="editlist">
                    <div class="list-content">
                      <span class="item-right">10</span>
                      <span class="list-text">My Favorite</span>
                    </div>
                  </div>
                </div>
              </div>
              <section class="page-aside-section">
                <h5 class="page-aside-title">Tags</h5>
                <div class="pl-20 pr-20">
                  <?php
                  $aa[] = "Government";
                  $aa[] = "Agency";
                  $aa[] = "Finance";
                  $aa[] = "Food";
                  $aa[] = "Fashion";
                  $aa[] = "Banking";
                  $aa[] = "Automotive";
                  $aa[] = "Steel";
                  $aa[] = "Property";
                  $aa[] = "Health Care";
                  $aa[] = "Commerce";
                  $aa[] = "Media";
                  $aa[] = "Tourisms";
                  $aa[] = "Electronic";
                  $aa[] = "Technology";
                  for($c=0;$c<15;$c++){
                    $tmpC = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
                  ?>
                  <a href="#">
                    <span class="badge p-5 mb-5 badge-outline" style="border-color:<?php echo $tmpC;?>; color:<?php echo $tmpC;?>;"><?=$aa[$c];?></span>
                  </a>
                  <?php }?>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      <div class="page-main">
        <div class="pt-30 pb-30">
          <form class="mt-20" action="#" role="search">
            <div class="input-search input-search-dark">
              <input type="text" class="form-control w-full" placeholder="Search..." name="">
              <button type="submit" class="input-search-btn">
                <i class="icon md-search" aria-hidden="true"></i>
              </button>
            </div>
          </form>
        </div>
        <section>
          <div class="pb-30 clearfix">
            <div class="float-right">
              <div class="btn-group media-arrangement btn-group-flat" role="group">
                <button onclick="location.href='network.php';" type="button" class="btn btn-icon btn-default waves-effect waves-classic"><i class="icon md-view-module" aria-hidden="true"></i></button>
                <button onclick="location.href='network-list.php';" type="button" class="btn btn-icon btn-default active waves-effect waves-classic"><i class="icon md-view-list" aria-hidden="true"></i></button>
              </div>
            </div>
            <div class="actions-inner  float-left mr-20">
              <div class="dropdown">
                <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                  Location
                  <span class="icon md-chevron-down" aria-hidden="true"></span>
                </button>
                <div class="dropdown-menu" role="menu">
                  <a class="dropdown-item" href="javascript:void(0)">Australia</a>
                  <a class="dropdown-item" href="javascript:void(0)">China</a>
                  <a class="dropdown-item" href="javascript:void(0)">India</a>
                  <a class="dropdown-item active" href="javascript:void(0)">Thailand</a>
                  <a class="dropdown-item" href="javascript:void(0)">United Kingdom</a>
                  <a class="dropdown-item " href="javascript:void(0)">United States</a>
                </div>
              </div>
            </div>
            <div class="actions-inner float-left mr-20">
              <div class="dropdown">
                <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                  Industry
                  <span class="icon md-chevron-down" aria-hidden="true"></span>
                </button>
                <div class="dropdown-menu" role="menu">
                  <a class="dropdown-item" href="javascript:void(0)">Advertising & Marketing</a>
                  <a class="dropdown-item" href="javascript:void(0)">Computer Hardware</a>
                  <a class="dropdown-item" href="javascript:void(0)">Consumer Products & Services</a>
                  <a class="dropdown-item" href="javascript:void(0)">Engineering</a>
                  <a class="dropdown-item" href="javascript:void(0)">Financial Services</a>
                  <a class="dropdown-item active" href="javascript:void(0)">Government Services</a>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="pl-10">
          <table class="table table-hover" data-plugin="selectable" data-row-selectable="true">
            <thead>
              <tr>
                <th class="pl-70">
                  Company Name
                </th>
                <th width="30%" class="hidden-xs-down">
                  Mobile/Email
                </th>
                <th width="25%" class="hidden-md-down">
                  Tags
                </th>
                <th width="5%">&nbsp;</th>
              </tr>
            </thead>
            <tbody>
              <?php 
              $comp[]="Smarking";
              $comp[]="Prefer";
              $comp[]="Paperspace";
              $comp[]="Boost Marketing Group";
              $comp[]="MZ Capital Partners";
              $comp[]="Desktop Metal";
              $comp[]="Kindly Care";
              $comp[]="Ledger Investing";
              $comp[]="Elemeno Health";
              $comp[]="EnviroSolar Power";
              $comp[]="Greenspire";
              $comp[]="ShipHawk";
              $comp[]="DGC International";
              $comp[]="GrowthPlay";
              $comp[]="CalCom Solar";
              $comp[]="Matterport";
              $comp[]="BlackRock Construction";
              $comp[]="Newline Interactive";
              $comp[]="National Merchants Association";
              $comp[]="SEO Werkz";
              $comp[]="Render Media";

              $b=1;
              for($a=1;$a<=30;$a++){
                if($a==21) $b=1;

                $tmpM = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
                $comname = $comp[rand(0,20)];
                $Logo = substr($comname, 0,1);
                $domain = "@".str_replace(" ", "", strtolower($comname)).".com";
              ?>
              <tr class="">
                <td>
                  <a href="javascript:void(0)" data-url="panel2.html" data-toggle="slidePanel" style="text-decoration:none;">
                  <span class="avatar avatar-lg float-left mr-20 text-center" style="background:<?php echo $tmpM;?>; border:1px solid #fff;">
                    <span class="font-size-30 text-white"><?php echo $Logo;?></span>
                  </span>
                  <p class="blue-600 mb-5 font-size-16 text-nowrap"><strong><?php echo $comname;?></strong></p>
                  <p class="grey-600 mb-10 text-nowrap"><?php echo $domain;?></p>
                  </a>
                </td>
                <td class="hidden-xs-down">
                  <p data-info-type="phone" class="mb-10 text-nowrap">
                    <i class="icon fa-mobile mr-10"></i>
                    <span class="text-break">9192372533</span>
                  </p>
                  <p data-info-type="email" class="mb-10 text-nowrap">
                    <i class="icon md-email mr-10"></i>
                    <span class="text-break">info<?php echo $domain;?><span>
                  </p>
                </td>
                <td class="hidden-md-down">
                  <?php
                  $aa[] = "Designer";
                  $aa[] = "jQuery";
                  $aa[] = "Javascript";
                  $aa[] = "HTML5";
                  $aa[] = "CSS";
                  $aa[] = "WordPress";
                  for($ab=0;$ab<3;$ab++){
                    $tmpC = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
                  ?>
                  <a href="#" style="text-decoration:none;">
                    <span class="badge mb-5 badge-outline" style="border-color:<?php echo $tmpC;?>; color:<?php echo $tmpC;?>;"><?=$aa[$ab];?></span>
                  </a>
                  <?php }?>
                </td>
                <td align="center">
                  <div class="dropdown">
                    <button class="btn btn-icon btn-round waves-effect waves-classic p-5 btn-pure" data-toggle="dropdown" href="#" aria-expanded="false">
                    <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                      <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                        Edit
                      </a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item text-danger" href="javascript:void(0)" role="menuitem">
                        Delete
                      </a>
                    </div>
                  </div>
                </td>
              </tr>
            <?php }?>
            </tbody>
          </table>
        </section>
      </div>
      
    </div>
  </div>
  <div class="site-action" data-plugin="actionBtn">
    <button type="button" data-target="#quickAddNetwork" data-toggle="modal" class="btn-raised btn btn-success btn-floating">
      <i class="front-icon md-plus" aria-hidden="true"></i>
    </button>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/GridMenu.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../assets/js/BaseApp.js"></script>
  <script src="../../assets/js/App/Contacts.js"></script>
  <script src="../../assets/examples/js/apps/contacts.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
      var wrap = $('.page');

      $(window).scroll(function () {
        if ($(this).scrollTop() > 125) {
          wrap.addClass("fix-mini-nav");
        } else {
          wrap.removeClass("fix-mini-nav");
        }
      });
    });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>