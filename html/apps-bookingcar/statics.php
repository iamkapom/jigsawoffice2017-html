<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../assets/examples/css/forms/layouts.css">
  <link rel="stylesheet" href="../../global/vendor/icheck/icheck.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
  <link rel="stylesheet" href="../../global/vendor/timepicker/jquery-timepicker.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
  <style type="text/css">
    table a{
      text-decoration: none!important;
    }
  </style>
</head>
<body class="animsition page-aside-right">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-5">Search car</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../home/index.php">Home</a></li>
        <li class="breadcrumb-item"><a href="index.php">Booking Car</a></li>
        <li class="breadcrumb-item active">Search car</li>
      </ol>
      <?php include("mini-nav.php");?>
    </div>
    
    <div class="page-content container-fluid" style="position: relative;">
      <div class="page-aside">
            <!-- Contacts Sidebar -->
            <div class="page-aside-switch">
              <i class="icon md-chevron-left" aria-hidden="true"></i>
              <i class="icon md-chevron-right" aria-hidden="true"></i>
            </div>
            <div class="page-aside-inner page-aside-scroll">
              <div data-role="container">
                <div data-role="content">
                  <section class="page-aside-section">
                    <div class="card card-block py-0 my-0">
                      <h4 class="mt-0">รายงานการจอง</h4>
                      <div class="list-group">
                        <a class="list-group-item" href="javascript:void(0)">
                          <i class="icon fa-car" aria-hidden="true"></i> ภาพรวมการจองรถ ปี <?=date("Y")?>
                        </a>
                        <a class="list-group-item" href="javascript:void(0)">
                          <i class="icon fa-car" aria-hidden="true"></i> การจองรถแยกตามประเภทรถ
                        </a>
                        <a class="list-group-item" href="javascript:void(0)">
                          <i class="icon fa-car" aria-hidden="true"></i> การจองรถแยกตามแผนก/ฝ่าย
                        </a>
                        <a class="list-group-item" href="javascript:void(0)">
                          <i class="icon fa-car" aria-hidden="true"></i> สถานะการจอง ปี <?=date("Y")?>
                        </a>
                      </div>
                    </div>

                  </section>
                  <section class="page-aside-section">
                    <div class="card card-block py-0 my-0">
                      <h4 class="mt-0">รายงานการใช้รถ</h4>
                      <div class="list-group">
                        <a class="list-group-item" href="javascript:void(0)">
                          <i class="icon fa-car" aria-hidden="true"></i> ภาพรวมการใช้รถ ปี <?=date("Y")?>
                        </a>
                        <a class="list-group-item" href="javascript:void(0)">
                          <i class="icon fa-car" aria-hidden="true"></i> การใช้รถแยกตามประเภทรถ
                        </a>
                        <a class="list-group-item" href="javascript:void(0)">
                          <i class="icon fa-car" aria-hidden="true"></i> การใช้รถแยกตามแผนก/ฝ่าย
                        </a>
                      </div>
                    </div>

                  </section>
                  <section class="page-aside-section">
                    <div class="card card-block py-0 my-0">
                      <h4 class="mt-0">รายงานเกี่ยวกับรถ</h4>
                      <div class="list-group">
                        <a class="list-group-item" href="javascript:void(0)">
                          <i class="icon fa-car" aria-hidden="true"></i> ภาพรวมการซ่อมบำรุง ปี <?=date("Y")?>
                        </a>
                      </div>
                    </div>

                  </section>
                  <section class="page-aside-section">
                    <div class="card card-block pt-0" style="min-height: 250px;">
                      <h4 class="mt-0">สถานะรถ</h4>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="card card-block py-10 bg-green-600 mb-10">
                            <div class="card-watermark darker font-size-80 mr-15"><i class="icon fa-car" aria-hidden="true"></i></div>
                            <div class="counter counter-md counter-inverse text-left">
                              <div class="counter-number-group">
                                <span class="counter-number">9</span>
                              </div>
                              <div class="counter-label text-capitalize">ใช้งาน</div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="card card-block py-10 bg-red-600 mb-10">
                            <div class="card-watermark darker font-size-80 mr-15"><i class="icon fa-wrench" aria-hidden="true"></i></div>
                            <div class="counter counter-md counter-inverse text-left">
                              <div class="counter-number-group">
                                <span class="counter-number">2</span>
                              </div>
                              <div class="counter-label text-capitalize">ซ่อมบำรุง</div>
                            </div>
                          </div>
                        </div>
                        
                      </div>
                    </div>
                  </section>
                </div>
              </div>
            </div>
          </div>

      <div class="page-main">
        
        <div class="row pt-20 ml-0 mr-0 bg-white">
          <div class="col-md-12">
            <section>
              <div class="pb-10">
              <div class="search-wrapper">
                <div class="search-box">
                  <div class="icon md-search"></div>
                  <div class="currently-showing">
                    <?=$search['tokenhtml']?>
                  </div>
                </div>
                <a href="javascript:void(0);" class="clear_all btn btn-flat btn-default btn-block waves-effect waves-classic">Clear All</a>
                <div class="data_entry">
                  <input class="input keyword-input" placeholder="Enter a keyword" type="text">
                  <div class="icon md-close-circle close"></div>
                </div>
                <div class="filters">
                    <div class="dropdown filter_permission">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      Car List
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu" role="menu">
                      <a class="dropdown-item" href="javascript:void(0)">รถเก๋ง (4 ที่นั่ง), โตโยต้า แครมรี , กข 1234 กทม</a>
                      <a class="dropdown-item" href="javascript:void(0)">รถเก๋ง (4 ที่นั่ง), โตโยต้า วีออส, กข 1122 กทม</a>
                      <a class="dropdown-item" href="javascript:void(0)">รถแวนด์ (7 ที่นั่ง), โตโยต้า อินโนวา, กข 2345 กทม</a>
                      <a class="dropdown-item" href="javascript:void(0)">รถตู้ (12 ที่นั่ง), โตโยต้า ไฮเอซ, กข 4567 กทม</a>
                      <a class="dropdown-item" href="javascript:void(0)">รถตู้ (12 ที่นั่ง), โตโยต้า ไฮเอซ, กข 4567 กทม</a>
                      <a class="dropdown-item" href="javascript:void(0)">รถตู้ (12 ที่นั่ง), โตโยต้า ไฮเอซ, กข 4567 กทม</a>
                      <a class="dropdown-item" href="javascript:void(0)">รถตู้ (12 ที่นั่ง), โตโยต้า ไฮเอซ, กข 4567 กทม</a>
                      <a class="dropdown-item" href="javascript:void(0)">รถตู้ (12 ที่นั่ง), โตโยต้า ไฮเอซ, กข 4567 กทม</a>
                      <a class="dropdown-item" href="javascript:void(0)">รถตู้ (12 ที่นั่ง), โตโยต้า ไฮเอซ, กข 4567 กทม</a>
                      <a class="dropdown-item" href="javascript:void(0)">รถตู้ (12 ที่นั่ง), โตโยต้า ไฮเอซ, กข 4567 กทม</a>
                    </div>
                  </div>
                  <div class="dropdown filter_permission">
                      <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Car Type
                        <span class="icon md-chevron-down" aria-hidden="true"></span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)">รถเก๋ง</a>
                        <a class="dropdown-item" href="javascript:void(0)">รถแวนด์</a>
                        <a class="dropdown-item" href="javascript:void(0)">รถตู้</a>
                        <a class="dropdown-item" href="javascript:void(0)">รถมินิบัส</a>
                      </div>
                    </div>
                    <div class="dropdown filter_permission">
                      <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Status
                        <span class="icon md-chevron-down" aria-hidden="true"></span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)">Approve</a>
                        <a class="dropdown-item" href="javascript:void(0)">Waiting Approve</a>
                        <a class="dropdown-item" href="javascript:void(0)">Daft</a>
                        <a class="dropdown-item" href="javascript:void(0)">Cancel</a>
                        <a class="dropdown-item" href="javascript:void(0)">Reject</a>
                      </div>
                    </div>
                    <div class="dropdown filter_permission">
                      <button type="button" id="SearchDateRange" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Time <i class="icon md-chevron-down" aria-hidden="true"></i>
                        <span id="daterange-value"></span>
                      </button>
                    </div>
                    
                </div>
              </div>
            </div>

              <div class="py-20 mb-20">
                <div class="float-right">
                  <div class="dropdown">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      <i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Sory by
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-media w-100" role="menu">
                      <h6 class="pl-10" aria-hidden="false">Sory by</h6>
                      <a class="dropdown-item active" href="javascript:void(0)">วัน/เวลา</a>
                      <a class="dropdown-item" href="javascript:void(0)">ข้อมูลรถ</a>
                      <a class="dropdown-item" href="javascript:void(0)">สถานะการจอง</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-sort-amount-asc" aria-hidden="true"></i> Ascending</a>
                      <a class="dropdown-item active" href="javascript:void(0)"><i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Descending</a>
                    </div>
                  </div>
                </div>
                
                <div class="pt-10">Total <strong>30</strong> rows</div>
              </div>
              
            </section>
            <section>
            <div class="tab-pane" id="exampleTabsTwo" role="tabpanel" aria-expanded="false">
              <table class="table table-hover">
                  <thead>
                    <tr>
                      <th width="15%">วัน/เวลาที่จอง</th>
                      <th>จองใช้เพื่อ</th>
                      <th  width="25%">ข้อมูลรถ</th>
                      <th class="text-center" width="10%">สถานะการจอง</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="pt-15">
                        <p class="mb-0 grey-500">
                          <h5 class="mt-0 mb-10">20/06/2018</h5>
                          09:30<span class="ml-5 mr-5">&mdash;</span>16:00
                        </p>
                      </td>
                      <td class="pt-15">
                        <p class="mb-0 grey-500">
                          <h5 class="mt-0 mb-10">
                            <a href="javascript:void(0);" data-target="#booking-detail" data-toggle="modal">รับ-ส่งคณะกรรมการตรวจรับระบบเครื่อข่ายอินเทอร์เน็ต</a>
                          </h5>
                          <small>
                            <span>ผู้โดยสาร: <strong>10</strong> คน</span>
                            <span class="pl-20">การใช้งาน: <strong>ไปรับ/ส่ง</strong></span>
                            <span class="pl-20">โดย: <strong><img class="avatar avatar-xs" src="../../global/portraits/5.jpg" alt="Edward Fletcher">  สมรัก นักกำปั้น</strong></span>
                          </small>
                        </p>
                      </td>
                      <td class="grey-900">
                        <div class="media">
                          <div class="pr-10">
                            <a class="avatar" href="javascript:void(0)">
                              <img class="img-fluid" src="../../global/photos/placeholder.png" alt="...">
                            </a>
                          </div>
                          <div class="media-body pt-5">
                            <h5 class="mt-0 mb-0">ไม่ระบุรถ</h5>
                          </div>
                        </div>
                        
                        </td>
                      
                      
                      <td class="hidden-sm-down pt-20 text-center">
                        <span class="badge badge-info">Waiting Approve</span>
                      </td>
                      
                    </tr>
                    <?php 
                    for($a=1;$a<=15;$a++){
                      $index = rand(0,6);
                    ?>
                    <tr>
                      <td class="pt-15">
                        <p class="mb-0 grey-500">
                          <h5 class="mt-0 mb-10">20/06/2018</h5>
                          09:30<span class="ml-5 mr-5">&mdash;</span>16:00
                        </p>
                      </td>
                      <td class="pt-15">
                        <p class="mb-0 grey-500">
                          <h5 class="mt-0 mb-10">
                            <a href="javascript:void(0);" data-target="#booking-detail" data-toggle="modal">รับ-ส่งคณะกรรมการตรวจรับระบบเครื่อข่ายอินเทอร์เน็ต</a>
                          </h5>
                          <small>
                            <span>ผู้โดยสาร: <strong>10</strong> คน</span>
                            <span class="pl-20">การใช้งาน: <strong>ไปรับ/ส่ง</strong></span>
                            <span class="pl-20">โดย: <strong><img class="avatar avatar-xs" src="../../global/portraits/5.jpg" alt="Edward Fletcher">  สมรัก นักกำปั้น</strong></span>
                          </small>
                        </p>
                      </td>
                      <td class="grey-900">
                        <div class="media">
                          <div class="pr-10">
                            <a class="avatar" href="javascript:void(0)">
                              <img class="img-fluid" src="img/toyota-hiace.jpg" alt="...">
                            </a>
                          </div>
                          <div class="media-body pt-5">
                            <h5 class="mt-0 mb-10">รถตู้ (12 ที่นั่ง)
                            <div data-half="true" data-plugin="rating" class="rating ml-10" data-score="4.5">
                                <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                                <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                              </div>
                            </h5>
                            <small>โตโยต้า สีขาว, กข 4665 กทม</small>
                          </div>
                        </div>
                        
                        </td>
                      
                      
                      <td class="hidden-sm-down pt-20 text-center">
                        <span class="badge badge-info">Waiting Approve</span>
                      </td>
                      
                    </tr>

                    
                    <?php }?>
                  </tbody>
                </table>
            </div>
          </section>
          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="modal fade" id="booking-detail" aria-hidden="true" aria-labelledby="booking-detail"
          role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple modal-full-screen">
              <form class="modal-content form-horizontal" action="#" method="post" role="form">
                <div class="modal-header py-50" style="background: #339966 url(img/bg-car.png);background-size:cover;">
                  <button type="button" class="close white close-bookingcar" data-dismiss="modal" aria-label="Close">
                    <i class="icon md-close" aria-hidden="true"></i>
                  </button>
                  <h1 class="modal-title white w-full text-center">รับ-ส่งคณะกรรมการตรวจรับระบบเครื่อข่ายอินเทอร์เน็ต</h1>
                </div>
                <div class="modal-body py-20">
                  <div class="row">
                      <div class="col-md-4">
                        <h5>รายละเอียดรถ/คนขับ<span class="pl-20">
                          <span class="multibooking" style="display: none;"><a href="javascript:void(0);" onclick="$('.multibooking').toggle();">จองคนเดียว</a></span>
                          <span class="multibooking"><a href="javascript:void(0);" onclick="$('.multibooking').toggle();">จองหลายคัน</a><span>
                            
                          </span></h5>
                        <div class="multibooking" style="display: none;">
                          
                          <table class="table table-striped">
                            <tr>
                              <td>
                                <div class="media pt-10">
                                  <div class="pr-10">
                                    <a class="avatar" style="width: 75px;" href="javascript:void(0)">
                                      <img class="img-fluid" src="img/toyota-hiace.jpg" alt="...">
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <h4 class="mt-5 mb-0">รถตู้  (12 ที่นั่ง)<div data-half="true" data-plugin="rating" class="rating ml-10" data-score="4.5">
                                      <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                      <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                      <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                      <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                                      <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                                    </div></h4>
                                    <p class="m-0">เลขทะเบียน <strong>กข 4665 กทม</strong></p>
                                    <p class="m-0">โตโยต้า สีขาว</p>

                                    <div class="pt-10">
                                      <small>พนักงานขับรถยนต์ </small>
                                      <div class="media pt-5">
                                        <div class="pr-10">
                                          <a class="avatar avatar-sm" href="javascript:void(0)">
                                            <img class="img-fluid" src="../../../global/portraits/6.jpg" alt="...">
                                          </a>
                                        </div>
                                        <div class="media-body">
                                          <h5 class="mt-5 mb-0">นายบุญส่ง แสนปลอดภัย</h5>
                                          <p class="m-0">โทรศัพท์ 08-1122-4455</p>
                                          <div data-half="true" data-plugin="rating" class="rating m-0" data-score="4.5">
                                            <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                            <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                            <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                            <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                                            <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <div class="media pt-10">
                                  <div class="pr-10">
                                    <a class="avatar" style="width: 75px;" href="javascript:void(0)">
                                      <img class="img-fluid" src="img/toyota-hiace.jpg" alt="...">
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <h4 class="mt-5 mb-0">รถตู้  (12 ที่นั่ง)<div data-half="true" data-plugin="rating" class="rating ml-10" data-score="4.5">
                                      <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                      <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                      <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                      <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                                      <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                                    </div></h4>
                                    <p class="m-0">เลขทะเบียน <strong>กข 8855 กทม</strong></p>
                                    <p class="m-0">โตโยต้า สีขาว</p>

                                    <div class="pt-10">
                                      <small>พนักงานขับรถยนต์ </small>
                                      <div class="media pt-5">
                                        <div class="pr-10">
                                          <a class="avatar avatar-sm" href="javascript:void(0)">
                                            <img class="img-fluid" src="../../../global/portraits/13.jpg" alt="...">
                                          </a>
                                        </div>
                                        <div class="media-body">
                                          <h5 class="mt-5 mb-0">นายนิยม ชมชื่นใจ</h5>
                                          <p class="m-0">โทรศัพท์ 08-5544-8899</p>
                                          <div data-half="true" data-plugin="rating" class="rating m-0" data-score="4.5">
                                            <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                            <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                            <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                            <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                                            <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <div class="media pt-10">
                                  <div class="pr-10">
                                    <a class="avatar" style="width: 75px;" href="javascript:void(0)">
                                      <img class="img-fluid" src="img/toyota-hiace.jpg" alt="...">
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <h4 class="mt-5 mb-0">รถตู้  (12 ที่นั่ง)<div data-half="true" data-plugin="rating" class="rating ml-10" data-score="4.5">
                                      <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                      <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                      <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                      <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                                      <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                                    </div></h4>
                                    <p class="m-0">เลขทะเบียน <strong>กข 4455 กทม</strong></p>
                                    <p class="m-0">โตโยต้า สีบรอนซ์เงิน</p>

                                    <div class="pt-10">
                                      <small>พนักงานขับรถยนต์ </small>
                                      <div class="media pt-5">
                                        <div class="pr-10">
                                          <a class="avatar avatar-sm" href="javascript:void(0)">
                                            <img class="img-fluid" src="../../../global/portraits/9.jpg" alt="...">
                                          </a>
                                        </div>
                                        <div class="media-body">
                                          <h5 class="mt-5 mb-0">นายชาลี ใจกล้าหาญ</h5>
                                          <p class="m-0">โทรศัพท์ 08-2244-7766</p>
                                          <div data-half="true" data-plugin="rating" class="rating m-0" data-score="4.5">
                                            <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                            <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                            <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                            <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                                            <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </td>
                            </tr>

                          </table>
                        </div>
                        <div class="multibooking">
                          <div class="card-img-top cover overlay overlay-hover">
                            <img class="cover-image overlay-figure overlay-scale" src="img/toyota-hiace.jpg" alt="">
                          </div>
                          <table class="table table-striped">
                            <tr>
                              <td>
                                  <h3>รถตู้  (12 ที่นั่ง)
                                    <div data-half="true" data-plugin="rating" class="rating ml-10" data-score="4.5">
                                      <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                      <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                      <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                      <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                                      <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                                    </div>
                                  </h3>
                                  <p class="mb-5">เลขทะเบียน <strong>กข 4665 กทม</strong></p>
                                  <p class="mb-5">โตโยต้า สีขาว</p>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <div>พนักงานขับรถยนต์ </div>
                                <div class="media pt-10">
                                  <div class="pr-10">
                                    <a class="avatar avatar-lg" href="javascript:void(0)">
                                      <img class="img-fluid" src="../../../global/portraits/6.jpg" alt="...">
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <h4 class="mt-5 mb-0">นายบุญส่ง แสนปลอดภัย</h4>
                                    <p class="m-0">โทรศัพท์ 08-1122-4455</p>
                                    <div data-half="true" data-plugin="rating" class="rating m-0" data-score="4.5">
                                        <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                        <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                        <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                        <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                                        <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                                      </div>
                                  </div>
                                </div>
                              </td>
                            </tr>
                          </table>
                        </div>
                        
                      </div>
                      <div class="col-md-8">
                        <h5>ข้อมูลการจอง</h5>
                        <table class="table table-striped">
                          <tr>
                            <td class="bg-grey-100" width="25%">จองใช้เพื่อ</td>
                            <td>
                              <h5 class="mt-3 mb-0">รับ-ส่งคณะกรรมการตรวจรับระบบเครื่อข่ายอินเทอร์เน็ต</h5>
                            </td>
                          </tr>
                          <tr>
                            <td class="bg-grey-100" width="25%">วันที่เดินทาง</td>
                            <td>
                              <h5 class="mt-3 mb-0">1 มิถุนายน พ.ศ. 2561</h5>
                              <small>09:00 - 17:00 น.</small>
                            </td>
                          </tr>
                          <tr>
                            <td class="bg-grey-100" width="25%">ลักษณะการใช้งาน</td>
                            <td>
                              <h5 class="mt-3 mb-0">ไป-กลับ</h5>
                            </td>
                          </tr>
                          <tr>
                            <td class="bg-grey-100" width="25%">จำนวนผู้โดยสาร</td>
                            <td>
                              <h5 class="mt-3 mb-0">10 คน</h5>
                            </td>
                          </tr>
                          <tr>
                            <td class="bg-grey-100" width="25%">รายชื่อผู้โดยสาร</td>
                            <td>
                              <h5 class="mt-3 mb-0">ประธานกรรมการตรวจรับระบบเครื่อข่ายอินเทอร์เน็ต พร้อมคณะ</h5>
                              <small>
                                <ul class="list-group list-group-full">
                                  <?php for($ab=1;$ab<=3;$ab++){?>
                                    <li class="list-group-item py-5"><?=$ab?>. คณะกรรมการ ปปปป</li>
                                  <?php }?>
                                  <?php for($ab=4;$ab<=10;$ab++){?>
                                    <li class="list-group-item py-5 listp" style="display: none;"><?=$ab?>. คณะกรรมการ ปปปป</li>
                                  <?php }?>
                                    <a class="listp" href="javascript:void(0);" onclick="$('.listp').toggle();">แสดง</a>
                                    <a class="listp" href="javascript:void(0);" style="display: none;" onclick="$('.listp').toggle();">ซ่อน</a>
                                </ul>
                              </small>
                            </td>
                          </tr>
                          <tr>
                            <td class="bg-grey-100" width="25%">สถานที่จะไป</td>
                            <td>
                              <h5 class="mt-3 mb-0">ศูนย์ราชการแจ้งวัฒนะ</h5>
                              <small><a href="javascript:void(0);" onclick="$('#gmap').toggle();">แสดงแผนที่</a></small>
                              <div id="gmap" style="display: none;">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d123941.07205314329!2d100.4943145!3d13.8894691!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e2832348e717bd%3A0x331bb3741242ce99!2z4Lio4Li54LiZ4Lii4LmM4Lij4Liy4LiK4LiB4Liy4Lij4LmB4LiI4LmJ4LiH4Lin4Lix4LiS4LiZ4Liw!5e0!3m2!1sth!2sth!4v1528310769695" width="400" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td class="bg-grey-100" width="25%">จุดขึ้นรถ </td>
                            <td>
                              <h5 class="mt-3 mb-0">สนามบินดอนเมือง</h5>
                            </td>
                          </tr>
                          <tr>
                            <td class="bg-grey-100" width="25%">สัมภาระ/สิ่งของ </td>
                            <td>
                              <h5 class="mt-3 mb-0">-</h5>
                            </td>
                          </tr>
                          <tr>
                            <td class="bg-grey-100" width="25%">ข้อมูลเพิ่มเติม </td>
                            <td>
                              <h5 class="mt-3 mb-0">-</h5>
                            </td>
                          </tr>
                          <tr>
                            <td class="bg-grey-100" width="25%">ผู้ติดต่อ </td>
                            <td>
                              <div class="media">
                                <div class="pr-10">
                                  <a class="avatar avatar-sm" href="javascript:void(0)">
                                    <img class="img-fluid" src="../../../global/portraits/16.jpg" alt="...">
                                  </a>
                                </div>
                                <div class="media-body">
                                  <h5 class="mt-3 mb-0">นางสาวชลลดาวดี สุวรรณภูมิ </h5>
                                  <small style="display: block;">นักวิชาการเงินและบัญชี <span class="px-10">-</span>การเงินและบัญชี </small>
                                  <small class="grey-700">โทรศัพท์ 08-1234-5678<span class="px-10">-</span>ภายใน 1234</small>
                                </div>
                              </div>
                            </td>
                          </tr>
                        </table>

                        <h5 class=" mt-30">การอนุมัติ</h5>
                        <table class="table table-striped">
                          <tr>
                            <td class="bg-grey-100" width="25%">สถานะการจอง </td>
                            <td>
                             <span class="badge badge-success">อนุมัติ</span> 
                             <small style="display: block;">เมื่อ 31 พฤษภาคม 2561 13:54:25 น.</small>
                            </td>
                          </tr>
                          <tr>
                            <td class="bg-grey-100" width="25%">บันทึกข้อมูลโดย</td>
                            <td>
                              <div class="media">
                                <div class="pr-10">
                                  <a class="avatar avatar-sm" href="javascript:void(0)">
                                    <img class="img-fluid" src="../../../global/portraits/5.jpg" alt="...">
                                  </a>
                                </div>
                                <div class="media-body">
                                  <h5 class="mt-3 mb-0">สมรัก นักกำปั้น</h5>
                                  <small>นักวิชาการเงินและบัญชี <span class="px-10">-</span>การเงินและบัญชี </small>
                                </div>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td class="bg-grey-100" width="25%">วันที่บันทึกการจอง </td>
                            <td>
                              <h5 class="mt-3 mb-0">31 พฤษภาคม พ.ศ. 2561 10:14:12 น.</h5>
                            </td>
                          </tr>
                        </table>
                      </div>
                  </div>
                  

                  
                </div>
              </form>
            </div>
          </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <script src="../../global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
  <script src="../../global/vendor/icheck/icheck.min.js"></script>
  <script src="../../global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
  <script src="../../global/vendor/timepicker/jquery.timepicker.min.js"></script>
  <script src="../../global/vendor/datepair/datepair.min.js"></script>
  <script src="../../global/vendor/datepair/jquery.datepair.min.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../assets/js/BaseApp.js"></script>
  <script src="../../assets/js/App/Contacts.js"></script>
  <script src="../../assets/examples/js/apps/contacts.js"></script>
  <script src="../../global/js/Plugin/jquery-placeholder.js"></script>
  <script src="../../global/js/Plugin/icheck.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-datepicker.js"></script>
  <script src="../../global/js/Plugin/jt-timepicker.js"></script>
  <script src="../../global/js/Plugin/datepair.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();


      function chooseDateRange(start, end) {
          var DrangeValue = start.format('MM/DD/YYYY') + '-' + end.format('MM/DD/YYYY');
          $('#SearchDateRange span#daterange-value').html(DrangeValue);
          var dataKey = "123";
          var dataValue = "xxx";
          var stoken = '';
            $('.search-wrapper .currently-showing .chooseDateRange').remove();
            stoken = '<div class="token chooseDateRange" data-key="'+dataKey+'" data-value="' + dataValue + '"><span class="token-label" >' + DrangeValue + '</span><a  href="#" class="close" tabindex="-1">×</a></div>';
            $('.search-wrapper .currently-showing').append(stoken);
            sysGenUrlSearch();
      }

      $('#SearchDateRange').daterangepicker({
          "autoApply": true,
          "opens": "center",
          ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
      }, chooseDateRange);

    });
  })(document, window, jQuery);
  </script>
  <script>
    // initialize input widgets first
    $('#frm_datepair .time').timepicker({
        'showDuration': true,
        'timeFormat': 'H:i'
    });

    $('#frm_datepair .date').datepicker({
        'format': 'm/d/yyyy',
        'autoclose': true
    });

    // initialize datepair
    var basicExampleEl = document.getElementById('frm_datepair');
    var datepair = new Datepair(basicExampleEl);
</script>
<?php include("../_footer-form.php");?>
</body>
</html>