<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/fullcalendar/fullcalendar.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-touchspin/bootstrap-touchspin.css">
  <link rel="stylesheet" href="../../global/vendor/jquery-selective/jquery-selective.css">
  <link rel="stylesheet" href="../../assets/examples/css/apps/calendar.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
  <link rel="stylesheet" href="../../global/vendor/timepicker/jquery-timepicker.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <style type="text/css">
  .slick-dots li button::before{font-size: 14px;}
  .btn-comment-post .btn-select{
    border: 0px !important;
  }
  .btn-comment-post, .btn-comment-post .bootstrap-select{
    width: 150px !important;
  }
  .fix-mini-nav + .page-content{
    padding-top: 0;
  }
  .fix-mini-nav .page-header{
    margin-bottom: 93px;
  }
  .fix-mini-nav .fix-mini-nav{
    margin-bottom: 0;
    margin-right: 0;
    margin-top: 0;
    padding: 0;
    position: fixed;
    top: 65px;
    width: 100%;
    z-index: 10;
    left: 0;
  }
  .btn-comment-post .btn-select{
    border: 0px !important;
  }
  .btn-comment-post, .btn-comment-post .bootstrap-select{
    width: 150px !important;
  }
  .page-content{
    padding: 0;
  }
  .app-calendar .page{
    margin-top: 0px;
  }
  .close-bookingcar{
    position: absolute;
    right: 20px;
    top: 15px;
  }
  </style>
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition app-calendar page-aside-left">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
      <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title">Booking Car</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../home/">Home</a></li>
        <li class="breadcrumb-item">Booking Car</li>
      </ol>
        <?php include("mini-nav.php");?>
    </div>

    <div class="page-content container-fluid bg-white" style="position: relative;">
      <div class="page-aside">
        <div class="page-aside-switch">
          <i class="icon md-chevron-left" aria-hidden="true"></i>
          <i class="icon md-chevron-right" aria-hidden="true"></i>
        </div>
        <div class="page-aside-inner page-aside-scroll">
          <div data-role="container">
            <div data-role="content">
              <section class="page-aside-section">
                <h5 class="page-aside-title">Car list</h5>
                <div class="pl-30 pr-20">
                  <ul class="list-group list-group-full">
                    <?php
                    $_bg[]="#0033cc";
                    $_carN[]="รถเก๋ง (4 ที่นั่ง)";
                    $_carD[]="โตโยต้า แครมรี , กข 1234 กทม";

                    $_bg[]="#0066ff";
                    $_carN[]="รถเก๋ง (4 ที่นั่ง)";
                    $_carD[]="โตโยต้า วีออส, กข 1122 กทม";

                    $_bg[]="#990099";
                    $_carN[]="รถแวนด์ (7 ที่นั่ง)";
                    $_carD[]="โตโยต้า อินโนวา, กข 2345 กทม";

                    $_bg[]="#cc6699";
                    $_carN[]="รถแวนด์ (5 ที่นั่ง)";
                    $_carD[]="มิตซูบิชิ ปาเจโร่, กข 5678 กทม";

                    $_bg[]="#cc33ff";
                    $_carN[]="รถแวนด์ (5 ที่นั่ง)";
                    $_carD[]="อีซูซุ, กข 5611 กทม";

                    $_bg[]="#00cc66";
                    $_carN[]="รถตู้ (12 ที่นั่ง)";
                    $_carD[]="โตโยต้า ไฮเอซ, กข 4567 กทม";

                    $_bg[]="#669900";
                    $_carN[]="รถตู้ (12 ที่นั่ง)";
                    $_carD[]="โตโยต้า, กข 4455 กทม";

                    $_bg[]="#339966";
                    $_carN[]="รถตู้ (12 ที่นั่ง)";
                    $_carD[]="โตโยต้า, กข 8855 กทม";

                    $_bg[]="#33cc33";
                    $_carN[]="รถตู้  (12 ที่นั่ง)";
                    $_carD[]="โตโยต้า, กข 1155 กทม";

                    $_bg[]="#336600";
                    $_carN[]="รถตู้ (12 ที่นั่ง)";
                    $_carD[]="โตโยต้า, กข 4665 กทม";

                    $_bg[]="#cc6600";
                    $_carN[]="รถมินิบัส (34 ที่นั่ง)";
                    $_carD[]="ฮีโน่, กข 9876 กทม";

                    for($ab=0;$ab<count($_bg);$ab++){
                    ?>
                    <li class="list-group-item p-5" style="background: <?=$_bg[$ab]?>;">
                      <div class="media">
                        <div class="pr-10">
                            <input type="checkbox" id="m1<?=$ab?>" name="m1<?=$ab?>" data-plugin="switchery" data-size="small" checked />
                        </div>
                        <label for="m1<?=$ab?>" class="media-body mb-0 white" style="cursor: pointer;">
                          <h5 class="mt-0 mb-5 white"><?=$_carN[$ab]?></h5>
                          <small><?=$_carD[$ab]?></small>
                        </label>
                      </div>
                    </li>
                  <?php }?>
                    
                  </ul>
                </div>
              </section>
              
              
            </div>
          </div>
        </div>
      </div>
      <div class="page-main">

        <div class="calendar-container">
          <div id="calendar"></div>
          <!--AddEvent Dialog -->
          <div class="modal fade" id="SearchCar" aria-hidden="true" aria-labelledby="SearchCar"
          role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple">
              <form class="modal-content form-horizontal" action="#" method="post" role="form">
                <div class="modal-header">
                  <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                  <h4 class="modal-title">Search Car</h4>
                </div>
                <div class="modal-body px-50" style="background: url(img/car-bg.jpeg) center; background-size:cover;">
                  <h2></h2>
                  <section>
                    <div id="frm_datepair" class="row pb-20">
                      <div class="col-md-12 mb-10">
                        <h5 class="white">Date/Time Start</h5>
                        <div class="input-daterange-wrap">
                          <div id="dStart" class="input-daterange row">
                            <div class="input-group col-md-6 pr-0">
                              <span class="input-group-addon">
                                <i class="icon md-calendar" aria-hidden="true"></i>
                              </span>
                              <input type="text" class="form-control date start">
                            </div>
                            <div class="input-group col-md-6 pl-0">
                              <span class="input-group-addon">
                                <i class="icon md-time" aria-hidden="true"></i>
                              </span>
                              <input type="text" class="form-control time start"/>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12 mb-10">
                        <h5 class="white">Date/Time Stop</h5>
                        <div class="input-daterange-wrap">
                          <div id="dStop" class="input-daterange row">
                            <div class="input-group col-md-6 pr-0">
                              <span class="input-group-addon">
                                <i class="icon md-calendar" aria-hidden="true"></i>
                              </span>
                              <input type="text" class="form-control date end">
                            </div>
                            <div class="input-group col-md-6 pl-0">
                              <span class="input-group-addon">
                                <i class="icon md-time" aria-hidden="true"></i>
                              </span>
                              <input type="text" class="form-control time end"/>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <h5 class="white">Car Type</h5>
                         <div class="bootstrap-select">
                          <select data-plugin="selectpicker">
                            <option value="">แสดงทั้งหมด</option>
                            <option value="AF">รถเก๋ง</option>
                            <option value="AF">รถแวนด์</option>
                            <option value="AF">รถตู้</option>
                            <option value="AF">รถมินิบัส</option>
                          </select>
                        </div>
                        
                      </div>
                      <div class="col-md-6">
                        <h5 class="white">Passengers</h5>
                         <div class="bootstrap-select">
                          <select data-plugin="selectpicker">
                            <option value="">1</option>
                            <option value="AF">2</option>
                            <option value="AF">3</option>
                            <option value="AF">4</option>
                            <option value="AF">3-5</option>
                            <option value="AF">5-12</option>
                            <option value="AF">12+</option>
                          </select>
                        </div>
                        
                      </div>
                      <div class="col-md-12 text-center pt-30">
                        <button type="button" onclick="location.href='search.php';" class="btn btn-success mr-30">Search Car</button>
                      </div>
                    </div>
                  </section>
                  
                </div>
              </form>
            </div>
          </div>
          <!-- End AddEvent Dialog -->
          
          <div class="modal fade" id="booking-detail" aria-hidden="true" aria-labelledby="booking-detail"
          role="dialog" tabindex="-1">
            <div class="modal-dialog modal-simple modal-full-screen">
              <form class="modal-content form-horizontal" action="#" method="post" role="form">
                <div class="modal-header py-50" style="background: #339966 url(img/bg-car.png);background-size:cover;">
                  <button type="button" class="close white close-bookingcar" data-dismiss="modal" aria-label="Close">
                    <i class="icon md-close" aria-hidden="true"></i>
                  </button>
                  <h1 class="modal-title white w-full text-center">รับ-ส่งคณะกรรมการตรวจรับระบบเครื่อข่ายอินเทอร์เน็ต</h1>
                </div>
                <div class="modal-body py-20">
                  <div class="row">
                      <div class="col-md-4">
                        <h5>รายละเอียดรถ/คนขับ<span class="pl-20">
                          <span class="multibooking" style="display: none;"><a href="javascript:void(0);" onclick="$('.multibooking').toggle();">จองคนเดียว</a></span>
                          <span class="multibooking"><a href="javascript:void(0);" onclick="$('.multibooking').toggle();">จองหลายคัน</a><span>
                            
                          </span></h5>
                        <div class="multibooking" style="display: none;">
                          
                          <table class="table table-striped">
                            <tr>
                              <td>
                                <div class="media pt-10">
                                  <div class="pr-10">
                                    <a class="avatar" style="width: 75px;" href="javascript:void(0)">
                                      <img class="img-fluid" src="img/toyota-hiace.jpg" alt="...">
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <h4 class="mt-5 mb-0">รถตู้  (12 ที่นั่ง)<div data-half="true" data-plugin="rating" class="rating ml-10" data-score="4.5">
                                      <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                      <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                      <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                      <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                                      <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                                    </div></h4>
                                    <p class="m-0">เลขทะเบียน <strong>กข 4665 กทม</strong></p>
                                    <p class="m-0">โตโยต้า สีขาว</p>

                                    <div class="pt-10">
                                      <small>พนักงานขับรถยนต์ </small>
                                      <div class="media pt-5">
                                        <div class="pr-10">
                                          <a class="avatar avatar-sm" href="javascript:void(0)">
                                            <img class="img-fluid" src="../../../global/portraits/6.jpg" alt="...">
                                          </a>
                                        </div>
                                        <div class="media-body">
                                          <h5 class="mt-5 mb-0">นายบุญส่ง แสนปลอดภัย</h5>
                                          <p class="m-0">โทรศัพท์ 08-1122-4455</p>
                                          <div data-half="true" data-plugin="rating" class="rating m-0" data-score="4.5">
                                            <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                            <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                            <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                            <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                                            <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <div class="media pt-10">
                                  <div class="pr-10">
                                    <a class="avatar" style="width: 75px;" href="javascript:void(0)">
                                      <img class="img-fluid" src="img/toyota-hiace.jpg" alt="...">
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <h4 class="mt-5 mb-0">รถตู้  (12 ที่นั่ง)<div data-half="true" data-plugin="rating" class="rating ml-10" data-score="4.5">
                                      <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                      <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                      <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                      <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                                      <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                                    </div></h4>
                                    <p class="m-0">เลขทะเบียน <strong>กข 8855 กทม</strong></p>
                                    <p class="m-0">โตโยต้า สีขาว</p>

                                    <div class="pt-10">
                                      <small>พนักงานขับรถยนต์ </small>
                                      <div class="media pt-5">
                                        <div class="pr-10">
                                          <a class="avatar avatar-sm" href="javascript:void(0)">
                                            <img class="img-fluid" src="../../../global/portraits/13.jpg" alt="...">
                                          </a>
                                        </div>
                                        <div class="media-body">
                                          <h5 class="mt-5 mb-0">นายนิยม ชมชื่นใจ</h5>
                                          <p class="m-0">โทรศัพท์ 08-5544-8899</p>
                                          <div data-half="true" data-plugin="rating" class="rating m-0" data-score="4.5">
                                            <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                            <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                            <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                            <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                                            <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <div class="media pt-10">
                                  <div class="pr-10">
                                    <a class="avatar" style="width: 75px;" href="javascript:void(0)">
                                      <img class="img-fluid" src="img/toyota-hiace.jpg" alt="...">
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <h4 class="mt-5 mb-0">รถตู้  (12 ที่นั่ง)<div data-half="true" data-plugin="rating" class="rating ml-10" data-score="4.5">
                                      <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                      <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                      <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                      <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                                      <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                                    </div></h4>
                                    <p class="m-0">เลขทะเบียน <strong>กข 4455 กทม</strong></p>
                                    <p class="m-0">โตโยต้า สีบรอนซ์เงิน</p>

                                    <div class="pt-10">
                                      <small>พนักงานขับรถยนต์ </small>
                                      <div class="media pt-5">
                                        <div class="pr-10">
                                          <a class="avatar avatar-sm" href="javascript:void(0)">
                                            <img class="img-fluid" src="../../../global/portraits/9.jpg" alt="...">
                                          </a>
                                        </div>
                                        <div class="media-body">
                                          <h5 class="mt-5 mb-0">นายชาลี ใจกล้าหาญ</h5>
                                          <p class="m-0">โทรศัพท์ 08-2244-7766</p>
                                          <div data-half="true" data-plugin="rating" class="rating m-0" data-score="4.5">
                                            <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                            <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                            <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                            <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                                            <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </td>
                            </tr>

                          </table>
                        </div>
                        <div class="multibooking">
                          <div class="card-img-top cover overlay overlay-hover">
                            <img class="cover-image overlay-figure overlay-scale" src="img/toyota-hiace.jpg" alt="">
                          </div>
                          <table class="table table-striped">
                            <tr>
                              <td>
                                  <h3>รถตู้  (12 ที่นั่ง)
                                    <div data-half="true" data-plugin="rating" class="rating ml-10" data-score="4.5">
                                      <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                      <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                      <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                      <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                                      <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                                    </div>
                                  </h3>
                                  <p class="mb-5">เลขทะเบียน <strong>กข 4665 กทม</strong></p>
                                  <p class="mb-5">โตโยต้า สีขาว</p>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <div>พนักงานขับรถยนต์ </div>
                                <div class="media pt-10">
                                  <div class="pr-10">
                                    <a class="avatar avatar-lg" href="javascript:void(0)">
                                      <img class="img-fluid" src="../../../global/portraits/6.jpg" alt="...">
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <h4 class="mt-5 mb-0">นายบุญส่ง แสนปลอดภัย</h4>
                                    <p class="m-0">โทรศัพท์ 08-1122-4455</p>
                                    <div data-half="true" data-plugin="rating" class="rating m-0" data-score="4.5">
                                        <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                        <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                        <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                        <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                                        <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                                      </div>
                                  </div>
                                </div>
                              </td>
                            </tr>
                          </table>
                        </div>
                        
                      </div>
                      <div class="col-md-8">
                        <h5>ข้อมูลการจอง</h5>
                        <table class="table table-striped">
                          <tr>
                            <td class="bg-grey-100" width="25%">เลขที่ใบจอง</td>
                            <td>
                              <h5 class="mt-3 mb-0"><?php echo (date("Y")+543)."/".date("m")."-000".rand(10,99)?></h5>
                            </td>
                          </tr>
                          <tr>
                            <td class="bg-grey-100" width="25%">จองใช้เพื่อ</td>
                            <td>
                              <h5 class="mt-3 mb-0">รับ-ส่งคณะกรรมการตรวจรับระบบเครื่อข่ายอินเทอร์เน็ต</h5>
                            </td>
                          </tr>
                          <tr>
                            <td class="bg-grey-100" width="25%">วันที่เดินทาง</td>
                            <td>
                              <h5 class="mt-3 mb-0">1 มิถุนายน พ.ศ. 2561</h5>
                              <small>09:00 - 17:00 น.</small>
                            </td>
                          </tr>
                          <tr>
                            <td class="bg-grey-100" width="25%">ลักษณะการใช้งาน</td>
                            <td>
                              <h5 class="mt-3 mb-0">ไป-กลับ</h5>
                            </td>
                          </tr>
                          <tr>
                            <td class="bg-grey-100" width="25%">จำนวนผู้โดยสาร</td>
                            <td>
                              <h5 class="mt-3 mb-0">10 คน</h5>
                            </td>
                          </tr>
                          <tr>
                            <td class="bg-grey-100" width="25%">รายชื่อผู้โดยสาร</td>
                            <td>
                              <h5 class="mt-3 mb-0">ประธานกรรมการตรวจรับระบบเครื่อข่ายอินเทอร์เน็ต พร้อมคณะ</h5>
                              <small>
                                <ul class="list-group list-group-full">
                                  <?php for($ab=1;$ab<=3;$ab++){?>
                                    <li class="list-group-item py-5"><?=$ab?>. คณะกรรมการ ปปปป</li>
                                  <?php }?>
                                  <?php for($ab=4;$ab<=10;$ab++){?>
                                    <li class="list-group-item py-5 listp" style="display: none;"><?=$ab?>. คณะกรรมการ ปปปป</li>
                                  <?php }?>
                                    <a class="listp" href="javascript:void(0);" onclick="$('.listp').toggle();">แสดง</a>
                                    <a class="listp" href="javascript:void(0);" style="display: none;" onclick="$('.listp').toggle();">ซ่อน</a>
                                </ul>
                              </small>
                            </td>
                          </tr>
                          <tr>
                            <td class="bg-grey-100" width="25%">สถานที่จะไป</td>
                            <td>
                              <h5 class="mt-3 mb-0">ศูนย์ราชการแจ้งวัฒนะ</h5>
                              <small><a href="javascript:void(0);" onclick="$('#gmap').toggle();">แสดงแผนที่</a></small>
                              <div id="gmap" style="display: none;">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d123941.07205314329!2d100.4943145!3d13.8894691!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e2832348e717bd%3A0x331bb3741242ce99!2z4Lio4Li54LiZ4Lii4LmM4Lij4Liy4LiK4LiB4Liy4Lij4LmB4LiI4LmJ4LiH4Lin4Lix4LiS4LiZ4Liw!5e0!3m2!1sth!2sth!4v1528310769695" width="400" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td class="bg-grey-100" width="25%">จุดขึ้นรถ </td>
                            <td>
                              <h5 class="mt-3 mb-0">สนามบินดอนเมือง</h5>
                            </td>
                          </tr>
                          <tr>
                            <td class="bg-grey-100" width="25%">สัมภาระ/สิ่งของ </td>
                            <td>
                              <h5 class="mt-3 mb-0">-</h5>
                            </td>
                          </tr>
                          <tr>
                            <td class="bg-grey-100" width="25%">ข้อมูลเพิ่มเติม </td>
                            <td>
                              <h5 class="mt-3 mb-0">-</h5>
                            </td>
                          </tr>
                          <tr>
                            <td class="bg-grey-100" width="25%">ผู้ติดต่อ </td>
                            <td>
                              <div class="media">
                                <div class="pr-10">
                                  <a class="avatar avatar-sm" href="javascript:void(0)">
                                    <img class="img-fluid" src="../../../global/portraits/16.jpg" alt="...">
                                  </a>
                                </div>
                                <div class="media-body">
                                  <h5 class="mt-3 mb-0">นางสาวชลลดาวดี สุวรรณภูมิ </h5>
                                  <small style="display: block;">นักวิชาการเงินและบัญชี <span class="px-10">-</span>การเงินและบัญชี </small>
                                  <small class="grey-700">โทรศัพท์ 08-1234-5678<span class="px-10">-</span>ภายใน 1234</small>
                                </div>
                              </div>
                            </td>
                          </tr>
                        </table>

                        <h5 class=" mt-30">การอนุมัติ</h5>
                        <table class="table table-striped">
                          <tr>
                            <td class="bg-grey-100" width="25%">สถานะการจอง </td>
                            <td>
                             <span class="badge badge-success">อนุมัติ</span> 
                             <small style="display: block;">เมื่อ 31 พฤษภาคม 2561 13:54:25 น.</small>
                            </td>
                          </tr>
                          <tr>
                            <td class="bg-grey-100" width="25%">บันทึกข้อมูลโดย</td>
                            <td>
                              <div class="media">
                                <div class="pr-10">
                                  <a class="avatar avatar-sm" href="javascript:void(0)">
                                    <img class="img-fluid" src="../../../global/portraits/5.jpg" alt="...">
                                  </a>
                                </div>
                                <div class="media-body">
                                  <h5 class="mt-3 mb-0">สมรัก นักกำปั้น</h5>
                                  <small>นักวิชาการเงินและบัญชี <span class="px-10">-</span>การเงินและบัญชี </small>
                                </div>
                              </div>
                            </td>
                          </tr>
                          <tr>
                            <td class="bg-grey-100" width="25%">วันที่บันทึกการจอง </td>
                            <td>
                              <h5 class="mt-3 mb-0">31 พฤษภาคม พ.ศ. 2561 10:14:12 น.</h5>
                            </td>
                          </tr>
                        </table>
                      </div>
                  </div>
                  

                  
                </div>
              </form>
            </div>
          </div>
          <!--AddCalendar Dialog -->
          
          <!-- End AddCalendar Dialog -->
        </div>


      </div>
    </div>
  </div>
  <!-- Site Action -->
  <div class="site-action" data-plugin="actionBtn" >
    <button type="button" data-target="#SearchCar" data-toggle="modal" class="btn-raised btn btn-success btn-floating">
      <i class="front-icon md-search animation-scale-up" aria-hidden="true"></i>
    </button>
  </div>
  <!-- End Site Action -->
  <!-- Add Calendar Form -->
  
  <!-- End Add Calendar Form -->
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/jquery-ui/jquery-ui.min.js"></script>
  <script src="../../global/vendor/moment/moment.min.js"></script>
  <script src="../../global/vendor/fullcalendar/fullcalendar.js"></script>
  <script src="../../global/vendor/jquery-selective/jquery-selective.min.js"></script>
  <script src="../../global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
  <script src="../../global/vendor/bootstrap-touchspin/bootstrap-touchspin.min.js"></script>
  <script src="../../global/vendor/bootbox/bootbox.js"></script>
  <script src="../../global/vendor/timepicker/jquery.timepicker.min.js"></script>
  <script src="../../global/vendor/datepair/datepair.min.js"></script>
  <script src="../../global/vendor/datepair/jquery.datepair.min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-touchspin.js"></script>
  <script src="../../global/js/Plugin/bootstrap-datepicker.js"></script>
  <script src="../../global/js/Plugin/material.js"></script>
  <script src="../../global/js/Plugin/action-btn.js"></script>
  <script src="../../global/js/Plugin/editlist.js"></script>
  <script src="../../global/js/Plugin/bootbox.js"></script>
  <script src="../../assets/examples/js/apps/calendar.js"></script>
  <script src="../../global/js/Plugin/jt-timepicker.js"></script>
  <script src="../../global/js/Plugin/datepair.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../assets/js/App/bookingcar.js"></script>
  <script>
    $(document).ready(function() {
      var wrap = $('.page');

      $(window).scroll(function () {
        if ($(this).scrollTop() > 125) {
          wrap.addClass("fix-mini-nav");
        } else {
          wrap.removeClass("fix-mini-nav");
        }
      });
    });
    // initialize input widgets first
    $('#frm_datepair .time').timepicker({
        'showDuration': true,
        'timeFormat': 'H:i'
    });

    $('#frm_datepair .date').datepicker({
        'format': 'm/d/yyyy',
        'autoclose': true
    });

    // initialize datepair
    var basicExampleEl = document.getElementById('frm_datepair');
    var datepair = new Datepair(basicExampleEl);
</script>
<?php include("../_footer-form.php");?>
</body>
</html>
