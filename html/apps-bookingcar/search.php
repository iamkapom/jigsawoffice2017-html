<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../assets/examples/css/forms/layouts.css">
  <link rel="stylesheet" href="../../global/vendor/icheck/icheck.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
  <link rel="stylesheet" href="../../global/vendor/timepicker/jquery-timepicker.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition page-aside-right">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-5">Search car</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../home/index.php">Home</a></li>
        <li class="breadcrumb-item"><a href="index.php">Booking Car</a></li>
        <li class="breadcrumb-item active">Search car</li>
      </ol>
      <?php include("mini-nav.php");?>
    </div>
    
    <div class="page-content container-fluid" style="position: relative;">
      <div class="page-aside">
            <!-- Contacts Sidebar -->
            <div class="page-aside-switch">
              <i class="icon md-chevron-left" aria-hidden="true"></i>
              <i class="icon md-chevron-right" aria-hidden="true"></i>
            </div>
            <div class="page-aside-inner page-aside-scroll">
              <div data-role="container">
                <div data-role="content">
                  <section class="page-aside-section">
                    <div class="card card-block pt-0" style="min-height: 250px;">
                      <h4 class="mt-0">รถว่างวันนี้ <?=date('m/d/Y')?></h4>
                      <table class="table table-hover">
                        <thead>
                          <tr>
                            <th >ข้อมูลรถ</th>
                            <th width="25%">เวลาที่ว่าง</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                          for($a=1;$a<=3;$a++){
                            $index = rand(0,6);
                          ?>
                          <tr>
                            <td class="grey-900">
                              <div class="media">
                                <div class="pr-10">
                                  <a class="avatar avatar-sm" href="javascript:void(0)">
                                    <img class="img-fluid" src="img/toyota-hiace.jpg" alt="...">
                                  </a>
                                </div>
                                <div class="media-body">
                                  <h5 class="mt-0 mb-0">รถตู้ (12 ที่นั่ง)</h5>
                                  <small>โตโยต้า, กข 4665 กทม</small>
                                  <div data-half="true" data-plugin="rating" class="rating" data-score="4.5">
                                    <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                    <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                    <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                    <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                                    <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                                  </div>
                                </div>
                              </div>
                              
                              </td>
                            <td class="">
                                <?php 
                                $tt[] = '08:30<span class="ml-5 mr-5">&mdash;</span>11:30';
                                $tt[] = '09:00<span class="ml-5 mr-5">&mdash;</span>12:30';
                                $tt[] = '10:30<span class="ml-5 mr-5">&mdash;</span>16:30';
                                $tt[] = '13:00<span class="ml-5 mr-5">&mdash;</span>16:30';
                                $tt[] = "All day";
                                ?>
                                <h6 class="mt-5 mb-15"><?=$tt[rand(0,2)]?></h6>
                              <button type="button" class="btn btn-xs btn-success">Booking</button>
                            </td>
                          </tr>
                          <?php }?>
                        </tbody>
                      </table>
                    </div>

                  </section>
                  <section class="page-aside-section">
                    <div class="card card-block pt-0" style="min-height: 250px;">
                      <h4 class="mt-0">สถานะรถ</h4>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="card card-block py-10 bg-green-600 mb-10">
                            <div class="card-watermark darker font-size-80 mr-15"><i class="icon fa-car" aria-hidden="true"></i></div>
                            <div class="counter counter-md counter-inverse text-left">
                              <div class="counter-number-group">
                                <span class="counter-number">9</span>
                              </div>
                              <div class="counter-label text-capitalize">ใช้งาน</div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="card card-block py-10 bg-red-600 mb-10">
                            <div class="card-watermark darker font-size-80 mr-15"><i class="icon fa-wrench" aria-hidden="true"></i></div>
                            <div class="counter counter-md counter-inverse text-left">
                              <div class="counter-number-group">
                                <span class="counter-number">2</span>
                              </div>
                              <div class="counter-label text-capitalize">ซ่อมบำรุง</div>
                            </div>
                          </div>
                        </div>
                        
                      </div>
                    </div>
                  </section>
                </div>
              </div>
            </div>
          </div>

      <div class="page-main">
        <div class="card mb-10" style="background: url(img/car-bg.jpeg) center; background-size:cover;">
          <div class="card-block px-30 py-50">
             <section>
              <div id="frm_datepair" class="row pb-20">
                <div class="col-md-6">
                  <h5 class="white">Date/Time Start</h5>
                  <div class="input-daterange-wrap">
                    <div id="dStart" class="input-daterange row">
                      <div class="input-group col-md-6 pr-0">
                        <span class="input-group-addon">
                          <i class="icon md-calendar" aria-hidden="true"></i>
                        </span>
                        <input type="text" class="form-control date start" value="6/20/2018">
                      </div>
                      <div class="input-group col-md-6 pl-0">
                        <span class="input-group-addon">
                          <i class="icon md-time" aria-hidden="true"></i>
                        </span>
                        <input type="text" class="form-control time start" value="09:30"/>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <h5 class="white">Date/Time Stop</h5>
                  <div class="input-daterange-wrap">
                    <div id="dStop" class="input-daterange row">
                      <div class="input-group col-md-6 pr-0">
                        <span class="input-group-addon">
                          <i class="icon md-calendar" aria-hidden="true"></i>
                        </span>
                        <input type="text" class="form-control date end" value="6/20/2018">
                      </div>
                      <div class="input-group col-md-6 pl-0">
                        <span class="input-group-addon">
                          <i class="icon md-time" aria-hidden="true"></i>
                        </span>
                        <input type="text" class="form-control time end" value="16:00"/>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <h5 class="white">Car Type</h5>
                   <div class="bootstrap-select">
                    <select data-plugin="selectpicker">
                      <option value="">แสดงทั้งหมด</option>
                      <option value="AF">รถเก๋ง</option>
                      <option value="AF">รถแวนด์</option>
                      <option value="AF">รถตู้</option>
                      <option value="AF">รถมินิบัส</option>
                    </select>
                  </div>
                  
                </div>
                <div class="col-md-3">
                  <h5 class="white">Passengers</h5>
                   <div class="bootstrap-select">
                    <select data-plugin="selectpicker">
                      <option value="">1</option>
                      <option value="AF">2</option>
                      <option value="AF">3</option>
                      <option value="AF">4</option>
                      <option value="AF">3-5</option>
                      <option value="AF">5-12</option>
                      <option value="AF">12+</option>
                    </select>
                  </div>
                  
                </div>
                <div class="col-md-6 text-left">
                  <h5>&nbsp;</h5>
                  <button type="button" class="btn btn-success">Search Car</button>
                </div>
              </div>
            </section>
          </div>
        </div>
        
        <div class="row mt-20 ml-0 mr-0 bg-white">
          <div class="col-md-12">
            <section>
              <div class="py-20 mb-20">
                <div class="float-right">
                  <div class="dropdown">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      <i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Sory by
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-media w-100" role="menu">
                      <h6 class="pl-10" aria-hidden="false">Sory by</h6>
                      <a class="dropdown-item active" href="javascript:void(0)">วัน/เวลา</a>
                      <a class="dropdown-item" href="javascript:void(0)">ข้อมูลรถ</a>
                      <a class="dropdown-item" href="javascript:void(0)">สถานะการจอง</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-sort-amount-asc" aria-hidden="true"></i> Ascending</a>
                      <a class="dropdown-item active" href="javascript:void(0)"><i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Descending</a>
                    </div>
                  </div>
                </div>
                
                <div class="pt-10">Total <strong>30</strong> rows</div>
              </div>
              
            </section>
            <section>
            <div class="tab-pane" id="exampleTabsTwo" role="tabpanel" aria-expanded="false">
              <table class="table table-hover">
                  <thead>
                    <tr>
                      <th >ข้อมูลรถ</th>
                      <th class="text-center" width="20%">วัน/เวลาที่ว่าง</th>
                      <th class="text-center" width="15%">จำนวนผู้โดยสาร</th>
                      <th class="text-center" width="15%">สถานะการจอง</th>
                      <th class="text-center" width="10%">ทำการจอง</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td class="grey-900">
                        <div class="media">
                          <div class="pr-10">
                            <a class="avatar" style="width: 75px;" href="javascript:void(0)">
                              <img class="img-fluid" src="../../global/photos/placeholder.png" alt="...">
                            </a>
                          </div>
                          <div class="media-body pt-5">
                            <h5 class="mt-0 mb-0">ไม่ระบุรถ</h5>
                            <small>ผู้ดูแลระบบจะเป็นคนเลือกรถให้ตามความเหมาะสม</small>
                          </div>
                        </div>
                        
                        </td>
                      <td class="pt-15 text-center">
                        <p class="mb-0 grey-500">
                          <h5 class="mt-0 mb-0">20/06/2018</h5>
                          09:30<span class="ml-5 mr-5">&mdash;</span>16:00
                        </p>
                      </td>
                      <td class="hidden-sm-down pt-20 text-center">2</td>
                      <td class="hidden-sm-down pt-20 text-center">
                        -
                      </td>
                      <td class="pt-20">
                        <button type="button" onclick="location.href='form.php';" class="btn btn-block btn-<?=$ab=='1'?"info":"success"?> waves-effect waves-classic">Booking</button>
                      </td>
                      
                    </tr>
                    <?php 
                    for($a=1;$a<=15;$a++){
                      $index = rand(0,6);
                    ?>
                    <tr>
                      <td class="grey-900">
                        <div class="media">
                          <div class="pr-10">
                            <a class="avatar" style="width: 75px;" href="javascript:void(0)">
                              <img class="img-fluid" src="img/toyota-hiace.jpg" alt="...">
                            </a>
                          </div>
                          <div class="media-body pt-5">
                            <h5 class="mt-0 mb-0">รถตู้ (12 ที่นั่ง)
                              <div data-half="true" data-plugin="rating" class="rating ml-10" data-score="4.5">
                                <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                                <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                              </div>
                            </h5>
                            <small>โตโยต้า สีขาว, กข 4665 กทม</small>
                            
                          </div>
                        </div>
                        
                        </td>
                      <td class="pt-15 text-center">
                        <p class="mb-0 grey-500">
                          <?php 
                          $tt[] = '08:30<span class="ml-5 mr-5">&mdash;</span>11:30';
                          $tt[] = '13:00<span class="ml-5 mr-5">&mdash;</span>16:30';;
                          $tt[] = "All day";
                          ?>
                          <h5 class="mt-0 mb-0">25/05/2018</h5>
                          <?=$tt[rand(0,2)]?>
                        </p>
                      </td>
                      <td class="hidden-sm-down pt-20 text-center">2</td>
                      <td class="hidden-sm-down pt-10 text-center">
                        <?php $ab = rand(1,2)?>
                        <?php if($ab=='1'){?>
                        <p class="m-0 p-0">
                          <small><?=rand(1,2)?> booking</small>
                        </p>
                        <span class="badge badge-info">Waiting Approve</span>
                        
                        <?php }?>
                        <?php if($ab=='2'){?>
                        <span class="badge badge-default">Emtry</span>
                        <?php }?>
                      </td>
                      <td class="pt-20">
                        <button type="button" onclick="location.href='form.php';" class="btn btn-block btn-<?=$ab=='1'?"info":"success"?> waves-effect waves-classic">Booking</button>
                      </td>
                      
                    </tr>
                    <?php }?>
                  </tbody>
                </table>
            </div>
          </section>
          </div>
        </div>

      </div>
    </div>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <script src="../../global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
  <script src="../../global/vendor/icheck/icheck.min.js"></script>
  <script src="../../global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
  <script src="../../global/vendor/timepicker/jquery.timepicker.min.js"></script>
  <script src="../../global/vendor/datepair/datepair.min.js"></script>
  <script src="../../global/vendor/datepair/jquery.datepair.min.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../assets/js/BaseApp.js"></script>
  <script src="../../assets/js/App/Contacts.js"></script>
  <script src="../../assets/examples/js/apps/contacts.js"></script>
  <script src="../../global/js/Plugin/jquery-placeholder.js"></script>
  <script src="../../global/js/Plugin/icheck.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-datepicker.js"></script>
  <script src="../../global/js/Plugin/jt-timepicker.js"></script>
  <script src="../../global/js/Plugin/datepair.js"></script>
  <script>
    // initialize input widgets first
    $('#frm_datepair .time').timepicker({
        'showDuration': true,
        'timeFormat': 'H:i'
    });

    $('#frm_datepair .date').datepicker({
        'format': 'm/d/yyyy',
        'autoclose': true
    });

    // initialize datepair
    var basicExampleEl = document.getElementById('frm_datepair');
    var datepair = new Datepair(basicExampleEl);
</script>
<?php include("../_footer-form.php");?>
</body>
</html>