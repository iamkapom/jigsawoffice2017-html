<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../assets/examples/css/forms/layouts.css">
  <link rel="stylesheet" href="../../global/vendor/icheck/icheck.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
  <link rel="stylesheet" href="../../global/vendor/timepicker/jquery-timepicker.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
  <style type="text/css">
    table a{
      text-decoration: none!important;
    }
  </style>
</head>
<body class="animsition page-aside-right">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-5">Problems / Suggestions</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../home/index.php">Home</a></li>
        <li class="breadcrumb-item"><a href="index.php">Booking Car</a></li>
        <li class="breadcrumb-item active">Problems / Suggestions</li>
      </ol>
      <?php include("mini-nav.php");?>
    </div>
    
    <div class="page-content container-fluid" style="position: relative;">
      <div class="page-aside">
            <!-- Contacts Sidebar -->
            <div class="page-aside-switch">
              <i class="icon md-chevron-left" aria-hidden="true"></i>
              <i class="icon md-chevron-right" aria-hidden="true"></i>
            </div>
            <div class="page-aside-inner page-aside-scroll">
              <div data-role="container">
                <div data-role="content">
                  <section class="page-aside-section">
                    <div class="card card-block pt-0" style="min-height: 250px;">
                      <h3 class="mt-0">Problems</h3>
                        <div class="pt-20 pb-20">
                          <div id="chart_sstatus" style="width: 100%; height: 200px;"></div>
                        </div>
                    </div>
                  </section>
                  <section class="page-aside-section">
                    <div class="card card-block pt-0" style="min-height: 250px;">
                      <h3 class="mt-0">Suggestions</h3>
                        <div class="pt-20 pb-20">
                          <div id="chart_ssource" style="width: 100%; height: 200px;"></div>
                        </div>
                    </div>
                  </section>

                  
                  
                  <section class="page-aside-section">
                    <div class="card card-block pt-0" style="min-height: 250px;">
                      <h4 class="mt-0">รถว่างวันนี้ <?=date('m/d/Y')?></h4>
                      <table class="table table-hover">
                        <thead>
                          <tr>
                            <th >ข้อมูลรถ</th>
                            <th width="25%">เวลาที่ว่าง</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php 
                          for($a=1;$a<=3;$a++){
                            $index = rand(0,6);
                          ?>
                          <tr>
                            <td class="grey-900">
                              <div class="media">
                                <div class="pr-10">
                                  <a class="avatar avatar-sm" href="javascript:void(0)">
                                    <img class="img-fluid" src="img/toyota-hiace.jpg" alt="...">
                                  </a>
                                </div>
                                <div class="media-body">
                                  <h5 class="mt-0 mb-0">รถตู้ (12 ที่นั่ง)</h5>
                                  <small>โตโยต้า, กข 4665 กทม</small>
                                  <div data-half="true" data-plugin="rating" class="rating" data-score="4.5">
                                    <i data-alt="1" class="icon md-star orange-600 m-0" title="bad"></i>
                                    <i data-alt="2" class="icon md-star orange-600 m-0" title="poor"></i>
                                    <i data-alt="3" class="icon md-star orange-600 m-0" title="regular"></i>
                                    <i data-alt="3" class="icon md-star orange-600 m-0" title="good"></i>
                                    <i data-alt="4" class="icon md-star-half orange-500 m-0" title="gorgeous"></i>
                                  </div>
                                </div>
                              </div>
                              
                              </td>
                            <td class="">
                                <?php 
                                $tt[] = '08:30<span class="ml-5 mr-5">&mdash;</span>11:30';
                                $tt[] = '09:00<span class="ml-5 mr-5">&mdash;</span>12:30';
                                $tt[] = '10:30<span class="ml-5 mr-5">&mdash;</span>16:30';
                                $tt[] = '13:00<span class="ml-5 mr-5">&mdash;</span>16:30';
                                $tt[] = "All day";
                                ?>
                                <h6 class="mt-5 mb-15"><?=$tt[rand(0,2)]?></h6>
                              <button type="button" class="btn btn-xs btn-success">Booking</button>
                            </td>
                          </tr>
                          <?php }?>
                        </tbody>
                      </table>
                    </div>

                  </section>
                  
                </div>
              </div>
            </div>
          </div>

      <div class="page-main">
        <div class="row pt-20 ml-0 mr-0 bg-white">
          <div class="col-md-12">
            <section>
              <div class="pb-10">
              <div class="search-wrapper">
                <div class="search-box">
                  <div class="icon md-search"></div>
                  <div class="currently-showing">
                    <?=$search['tokenhtml']?>
                  </div>
                </div>
                <a href="javascript:void(0);" class="clear_all btn btn-flat btn-default btn-block waves-effect waves-classic">Clear All</a>
                <div class="data_entry">
                  <input class="input keyword-input" placeholder="Enter a keyword" type="text">
                  <div class="icon md-close-circle close"></div>
                </div>
                <div class="filters">
                  <div class="dropdown filter_permission">
                      <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        ประเภท
                        <span class="icon md-chevron-down" aria-hidden="true"></span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)">แจ้งปัญหา</a>
                        <a class="dropdown-item" href="javascript:void(0)">ข้อเสนอแนำ</a>
                      </div>
                    </div>
                    <div class="dropdown filter_permission">
                      <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        หมวดหมู่
                        <span class="icon md-chevron-down" aria-hidden="true"></span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)">การใช้งานระบบ</a>
                        <a class="dropdown-item" href="javascript:void(0)">รถยนต์</a>
                        <a class="dropdown-item" href="javascript:void(0)">พนักงานขับรถยนต์</a>
                      </div>
                    </div>
                    <div class="dropdown filter_permission">
                      <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        สถานะ
                        <span class="icon md-chevron-down" aria-hidden="true"></span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)">ยังไม่ได้อ่าน</a>
                        <a class="dropdown-item" href="javascript:void(0)">อ่านแล้ว</a>
                      </div>
                    </div>
                    
                    
                </div>
              </div>
            </div>

              <div class="py-20 mb-20">
                <div class="float-right">
                  <div class="dropdown">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      <i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Sory by
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-media w-100" role="menu">
                      <h6 class="pl-10" aria-hidden="false">Sory by</h6>
                      <a class="dropdown-item" href="javascript:void(0)">เรื่อง</a>
                      <a class="dropdown-item" href="javascript:void(0)">ประเภท</a>
                      <a class="dropdown-item" href="javascript:void(0)">หมวดหมู่</a>
                      <a class="dropdown-item" href="javascript:void(0)">สถานะ</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-sort-amount-asc" aria-hidden="true"></i> Ascending</a>
                      <a class="dropdown-item active" href="javascript:void(0)"><i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Descending</a>
                    </div>
                  </div>
                </div>
                
                <div class="pt-10">Total <strong>30</strong> rows</div>
              </div>
              
            </section>
            <section>
            <div class="tab-pane" id="exampleTabsTwo" role="tabpanel" aria-expanded="false">
              <table class="table table-hover">
                  <thead>
                    <tr>
                      <th width="10%">ประเภท</th>
                      <th >เรื่อง</th>
                      <th width="30%">โดย</th>
                      <th class="text-center" width="10%">สถานะ</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $_colorb[] = "info";
                    $_type[] = "เสนอแนะ";
                    $_subj[] = "ระบบไม่ควรเปิดให้มีการจองซ้ำ ควรให้สิทธิคนที่จองก่อน";
                    $_cat[] = "การใช้งานระบบ";


                    $_colorb[] = "danger";
                    $_type[] = "แจ้งปัญหา";
                    $_subj[] = "พนักงานขับรถ มารับช้า,ขับรถเร็วไม่คำนึงถึงความปลอดภัย";
                    $_cat[] = "พนักงานขับรถยนต์";

                    $_colorb[] = "danger";
                    $_type[] = "แจ้งปัญหา";
                    $_subj[] = "รถยนต์แอร์ไม่เย็น มีกลิ่นอับและไม่สะอาด";
                    $_cat[] = "รถยนต์";
                    

                    $_color = array("#4caf50","#ff9800");
                    $_status = array("อ่านแล้ว","ยังไม่ได้อ่าน");
                    for($a=1;$a<=15;$a++){
                      $index = rand(0,2);
                      $_index = rand(0,1);
                    ?>
                    <tr class="<?=$_index=="1"?"bg-orange-100":""?>">
                      <td class=" pt-10">
                        <span class="badge badge-<?=$_colorb[$index]?>"><?=$_type[$index]?></span>
                      </td>
                      <td class="grey-700 pt-10">
                        <h5 class="mt-0 mb-0">
                          <a href="javascript:void(0);" data-target="#contact-detail" data-toggle="modal">
                            <?=$_subj[$index]?>
                          </a>
                        </h5>
                        <small class="text-<?=$_colorb[$index]?>">
                          <?=$_cat[$index]?>
                        </small>
                        
                        </td>
                      <td class="hidden-sm-down pt-5">
                        <div class="media">
                            <div class="pr-10">
                              <a class="avatar avatar-xs mt-5" href="javascript:void(0)">
                                <img class="img-fluid" src="../../../global/portraits/6.jpg" alt="...">
                              </a>
                            </div>
                            <div class="media-body">
                               <h6 class="mt-3 mb-0">นางสาวชลลดาวดี สุวรรณภูมิ </h6>
                              <small style="display: block;">นักวิชาการเงินและบัญชี <span class="px-10">-</span>การเงินและบัญชี </small>
                            </div>
                          </div>
                      </td>
                      <td class="hidden-sm-down pt-15 text-center">
                        <span style="color:<?=$_color[$_index]?>;"><?=$_status[$_index]?></span>
                      </td>
                      
                    </tr>
                    <?php }?>
                  </tbody>
                </table>
            </div>
          </section>
          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="modal fade" id="contact-detail" aria-hidden="true" aria-labelledby="booking-detail"
          role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple">
      <form class="modal-content form-horizontal" action="#" method="post" role="form">
        <div class="modal-header py-20" style="background: #660066 url(img/bg-car.png);background-size:cover;">
          <button type="button" class="close white close-bookingcar" data-dismiss="modal" aria-label="Close">
            <i class="icon md-close" aria-hidden="true"></i>
          </button>
          <h1 class="modal-title white w-full text-center">Problems / Suggestions</h1>
        </div>
        <div class="modal-body py-20">
          <div class="row">
              <div class="col-md-12">
                <table class="table table-striped">
                  <tr>
                    <td class="bg-grey-100" width="25%">ประเภท</td>
                    <td>
                      <h5 class="mt-3 mb-0"><span class="badge badge-info">ข้อเสนอแนะ</span></h5>
                    </td>
                  </tr>
                  <tr>
                    <td class="bg-grey-100" width="25%">หมวดหมู่</td>
                    <td>
                      <h5 class="mt-3 mb-0">การใช้งานระบบ</h5>
                    </td>
                  </tr>
                  <tr>
                    <td class="bg-grey-100" width="25%">เรื่อง</td>
                    <td>
                      <h5 class="mt-3 mb-0">ระบบไม่ควรเปิดให้มีการจองซ้ำ ควรให้สิทธิคนที่จองก่อน</h5>
                    </td>
                  </tr>
                  <tr>
                    <td class="bg-grey-100" width="25%">รายละเอียด</td>
                    <td>
                      <p class="mt-3 mb-0">
                        หากมีรายจองรถคันไหนแล้ว ไม่ควรเปิดให้มีการจองซ้ำได้ ควรให้สิทธิ์กับคนที่ทำการจองก่อน
                      </p>
                      <p class="mt-3 mb-0">
                        หากมีรายจองรถคันไหนแล้ว ไม่ควรเปิดให้มีการจองซ้ำได้ ควรให้สิทธิ์กับคนที่ทำการจองก่อน
                      </p>
                    </td>
                  </tr>
                  <tr>
                    <td class="bg-grey-100" width="25%">ไฟล์แนป</td>
                    <td>
                      <h5 class="mt-3 mb-0">-</h5>
                    </td>
                  </tr>
                  <tr>
                    <td class="bg-grey-100" width="25%">โดย</td>
                    <td>
                      <div class="media">
                        <div class="pr-10">
                          <a class="avatar avatar-sm" href="javascript:void(0)">
                            <img class="img-fluid" src="../../../global/portraits/6.jpg" alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                          <h5 class="mt-3 mb-0">นายสมรัก นักกำปั้น</h5>
                          <small style="display: block;">สวัสดิการและบริการ</small>
                          <small class="grey-700">โทรศัพท์ 08-1234-5678<span class="px-10">-</span>ภายใน 1234</small>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td class="bg-grey-100" width="25%">เมื่อ</td>
                    <td>
                      <h5 class="mt-3 mb-0">12/02/2015</h5>
                    </td>
                  </tr>
                </table>

                

              </div>
          </div>
          

          
        </div>
      </form>
    </div>
  </div>

  <div class="modal fade" id="Contact" aria-hidden="true" aria-labelledby="booking-detail"
          role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple">
      <form class="modal-content form-horizontal" action="#" method="post" role="form">
        <div class="modal-header py-20 bg-orange-600" style="background: url(img/bg-car.png);background-size:cover;">
          <button type="button" class="close white close-bookingcar" data-dismiss="modal" aria-label="Close">
            <i class="icon md-close" aria-hidden="true"></i>
          </button>
          <h1 class="modal-title white w-full text-center">Problems / Suggestions</h1>
        </div>
        <div class="modal-body py-20">
            <div class="row justify-content-md-center">
                <div class="col-md-12">
                  <h5>เรื่อง</h5>
                  <input type="text" class="form-control" id="inputnetwork" placeholder="เรื่อง" />
                </div>
            </div>
            <div class="row justify-content-md-center mt-10">
                <div class="col-md-12">
                  <h5>รายละเอียด</h5>
                  <textarea class="form-control" id="textareaDefault" placeholder="รายละเอียด..." rows="3"></textarea>
                </div>
            </div>
            <div class="row justify-content-md-center mt-10">
                <div class="col-md-12 post-quick-form">
                  <h5 class="float-left mt-15">ประเภท</h5>
                  <div class="postfromarea">
                    <ul class="list-unstyled list-inline">
                      <li class="float-left text-danger mb-5">
                        <input type="radio" class="icheckbox-red" id="inputRadiosUnchecked1" name="inputRadios1"
                        data-plugin="iCheck" data-radio-class="iradio_flat-red" checked />
                        <label class="ml-5 mr-30" for="inputRadiosUnchecked1">แจ้งปัญหา</label>
                      </li>
                      <li class="float-left text-info mb-5">
                        <input type="radio" class="icheckbox-primary" id="inputRadiosChecked2" name="inputRadios1"
                        data-plugin="iCheck" data-radio-class="iradio_flat-blue"/>
                        <label class="ml-5 mr-30" for="inputRadiosChecked2">ข้อเสนอแนะ</label>
                      </li>
                    </ul>
                  </div>
                </div>
            </div>
            <div class="row justify-content-md-center mt-10">
                <div class="col-md-12 post-quick-form">
                  <h5 class="float-left mt-15">หมวดหมู่</h5>
                  <div class="postfromarea">
                    <ul class="list-unstyled list-inline">
                      <li class="float-left  mb-5">
                        <input type="radio" class="icheckbox-orange" id="inputRadiosUnchecked3" name="inputRadios2"
                        data-plugin="iCheck" data-radio-class="iradio_flat-orange" checked />
                        <label class="ml-5 mr-30" for="inputRadiosUnchecked3">พนักงานขับรถยนต์</label>
                      </li>
                      <li class="float-left mb-5">
                        <input type="radio" class="icheckbox-orange" id="inputRadiosChecked4" name="inputRadios2"
                        data-plugin="iCheck" data-radio-class="iradio_flat-orange"/>
                        <label class="ml-5 mr-30" for="inputRadiosChecked4">รถยนต์</label>
                      </li>
                      <li class="float-left mb-5">
                        <input type="radio" class="icheckbox-orange" id="inputRadiosChecked5" name="inputRadios2"
                        data-plugin="iCheck" data-radio-class="iradio_flat-orange"/>
                        <label class="ml-5 mr-30" for="inputRadiosChecked5">การใช้งานระบบ</label>
                      </li>
                    </ul>
                  </div>
                </div>
            </div>
            <div class="row mt-10">
                <div class="col-md-12 post-quick-form">
                  <h5 class="float-left mr-20">ไฟล์แนบ</h5>
                  <div class="postfromarea">
                    <button type="button" class="btn btn-dark waves-effect waves-classic mr-20">
                      <i class="icon md-attachment-alt" aria-hidden="true"></i> Attach
                    </button>
                  </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
              <button type="button" class="btn btn-primary waves-effect waves-classic">Submit</button>
              <button type="button" class="btn btn-default btn-pure waves-effect waves-classic" data-dismiss="modal">Close</button>
            </div>
      </form>
    </div>
  </div>
  <!-- Site Action -->
  <div class="site-action" data-plugin="actionBtn" >
    <button type="button" data-target="#Contact" data-toggle="modal" class="btn-raised btn btn-warning btn-floating">
      <i class="front-icon md-edit" aria-hidden="true"></i>
    </button>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <script src="../../global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
  <script src="../../global/vendor/icheck/icheck.min.js"></script>
  <script src="../../global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../assets/js/BaseApp.js"></script>
  <script src="../../assets/js/App/Contacts.js"></script>
  <script src="../../assets/examples/js/apps/contacts.js"></script>
  <script src="../../global/js/Plugin/jquery-placeholder.js"></script>
  <script src="../../global/js/Plugin/icheck.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>

  <script src="../../global/vendor/amcharts/amcharts.js" type="text/javascript"></script>
  <script src="../../global/vendor/amcharts/serial.js" type="text/javascript"></script>
  <script src="../../global/vendor/amcharts/pie.js" type="text/javascript"></script>
  <script>
    var _sstatus;
    var chartData_sstatus = [
        {
            "title": "พนักงานขับรถยนต์",
            "value": 543,
            "color": "#cc0000"
        },
        {
            "title": "รถยนต์",
            "value": 238,
            "color": "#ff3333"
        },
        {
            "title": "การใช้งานระบบ",
            "value": 173,
            "color": "#ff6666"
        }
    ];

    AmCharts.ready(function () {
        // PIE CHART
        _sstatus = new AmCharts.AmPieChart();
        _sstatus.dataProvider = chartData_sstatus;
        _sstatus.titleField = "title";
        _sstatus.valueField = "value";
        _sstatus.colorField = "color";
        _sstatus.radius = "30%";
        _sstatus.innerRadius = "40%";
        _sstatus.labelRadius = 5;
        _sstatus.labelText = "[[title]]";
        // WRITE
        _sstatus.write("chart_sstatus");
    });


    

    var _ssource;
    var chartData_ssource = [
        {
            "title": "การใช้งานระบบ",
            "value": 253,
            "color": "#00acc1"
        },
        {
            "title": "พนักงานขับรถยนต์",
            "value": 138,
            "color": "#00cbe6"
        },
        {
            "title": "รถยนต์",
            "value": 73,
            "color": "#1ae4ff"
        }
    ];

    AmCharts.ready(function () {
        // PIE CHART
        _ssource = new AmCharts.AmPieChart();
        _ssource.dataProvider = chartData_ssource;
        _ssource.titleField = "title";
        _ssource.valueField = "value";
        _ssource.colorField = "color";
        _ssource.radius = "30%";
        _ssource.innerRadius = "40%";
        _ssource.labelRadius = 5;
        _ssource.labelText = "[[title]]";

        // WRITE
        _ssource.write("chart_ssource");
    });

    
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>