<ul class="nav nav-tabs nav-tabs-line fix-mini-nav" role="tablist">
  <li class="nav-item">
    <a class="nav-link <?=(substr(basename($_SERVER["SCRIPT_FILENAME"]),0,5)=="index"?"active":"")?>" href="index.php" aria-expanded="true">Calendar</a>
  </li>
  <li class="nav-item">
    <a class="nav-link <?=(substr(basename($_SERVER["SCRIPT_FILENAME"]),0,6)=="search"?"active":"")?>" href="search.php" aria-expanded="true">Search car</a>
  </li>
  <li class="nav-item">
    <a class="nav-link <?=(substr(basename($_SERVER["SCRIPT_FILENAME"]),0,7)=="booking"?"active":"")?>" href="booking.php" aria-expanded="false">Booking list</a>
  </li><li class="nav-item">
    <a class="nav-link <?=(substr(basename($_SERVER["SCRIPT_FILENAME"]),0,3)=="car"?"active":"")?>" href="car.php" aria-expanded="true">Car Detail</a>
  </li>
  </li><li class="nav-item">
    <a class="nav-link <?=(substr(basename($_SERVER["SCRIPT_FILENAME"]),0,6)=="driver"?"active":"")?>" href="driver.php" aria-expanded="true">Driver Detail</a>
  </li>
  </li><li class="nav-item">
    <a class="nav-link <?=(substr(basename($_SERVER["SCRIPT_FILENAME"]),0,7)=="contact"?"active":"")?>" href="contact.php" aria-expanded="true">Problems / Suggestions</a>
  </li>
  </li><li class="nav-item">
    <a class="nav-link <?=(substr(basename($_SERVER["SCRIPT_FILENAME"]),0,7)=="statics"?"active":"")?>" href="statics.php" aria-expanded="true">Statics</a>
  </li>
</ul>