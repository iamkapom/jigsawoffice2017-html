<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css?v=1">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../assets/examples/css/apps/message-v2.css?v=1">
  <link rel="stylesheet" href="../../global/vendor/jquery-selective/jquery-selective.css">
  <link rel="stylesheet" href="../../global/vendor/toolbar/toolbar.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition app-message page-aside-scroll page-aside-left">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?> 
  <div class="page">
    <!-- Message Sidebar -->
    <div class="page-aside">
      <div class="page-aside-switch">
        <i class="icon md-chevron-left" aria-hidden="true"></i>
        <i class="icon md-chevron-right" aria-hidden="true"></i>
      </div>
      <div class="page-aside-inner pt-20" style="margin-top:-20px;">
        <section style="border-bottom:1px solid #e0e0e0;">
          <h3 class="pl-10 mt-20">Messenger</h3>
          <div style="position:absolute;right:0;top:19px;">
              <button type="button" class="btn btn-pure font-size-20 p-5 mr-5 icon fa-edit" aria-hidden="true"></button>
          </div>
        </section>
        <section>
          <div class="input-search input-search-dark m-10 ml-10">
            <i class="input-search-icon md-search" aria-hidden="true" style="left:0;"></i>
            <input class="form-control" name="" placeholder="Search Messenger" type="text" style="border-radius:0.215rem;">
            <button type="button" class="input-search-close icon md-close" aria-label="Close" style="right:0;"></button>
          </div>
          <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item" role="presentation" style="width:32.5%;text-align:center;">
              <a class="nav-link active font-size-20 p-0" data-toggle="tab" href="#exampleTabsIconOne" aria-controls="exampleTabsIconOne" role="tab">
                <i class="icon fa-wechat m-0" aria-hidden="true"></i>
              </a>
            </li>
            <li class="nav-item" role="presentation" style="width:32.5%;text-align:center;">
              <a class="nav-link font-size-20 p-0" data-toggle="tab" href="#exampleTabsIconTwo" aria-controls="exampleTabsIconTwo" role="tab">
                <i class="icon fa-user m-0" aria-hidden="true"></i>
              </a>
            </li>
            <li class="nav-item" role="presentation" style="width:32.5%;text-align:center;">
              <a class="nav-link font-size-20 p-0" data-toggle="tab" href="#exampleTabsIconThree" aria-controls="exampleTabsIconThree" role="tab">
                <i class="icon fa-group m-0" aria-hidden="true"></i>
              </a>
            </li>
          </ul>
        </section>
        <div class="app-message-list page-aside-scroll">
          <div data-role="container">
            <div data-role="content">
              <div class="tab-content pt-5">
                <div class="tab-pane active" id="exampleTabsIconOne" role="tabpanel">
                  <ul class="list-group">
                    <?php 
                    $b=1;
                    for($a=1;$a<=15;$a++){
                      $online = rand(0,1);
                    ?>
                    <li class="list-group-item p-5 pr-10 pl-10 <?php echo ($a==4?"active":"")?>">
                      <div class="media" style="flex-direction:initial;">
                        <div class="pr-10">
                          <a class="avatar avatar-online" href="javascript:void(0)">
                            <img class="img-fluid" src="../../global/portraits/<?=$b++?>.jpg" alt="...">
                            <?php if($online){?>
                            <i></i>
                            <?php }?>
                          </a>
                        </div>
                        <div class="media-body">
                          <?php if($a<4){?>
                          <div class="mt-0 black"><strong>Edward Fletcher</strong></div>
                          <small class="black"><strong>Consectetuorem ipsum dolor sit?</strong></small>
                          <?php }else{?>
                          <div class="mt-0 grey-700">Caleb Richards</div>
                          <small class="grey-400">Consectetuorem ipsum dolor sit?</small>
                          <?php }?>
                        </div>
                        <div class="font-size-10 <?php echo ($a<4?"blue-600":"grey-500");?>" style="position:absolute;right:10px;top:10px;">
                          1:44pm
                        </div>
                      </div>
                    </li>
                    <?php 
                    }
                    ?>
                  </ul>
                </div>
                <div class="tab-pane" id="exampleTabsIconTwo" role="tabpanel">
                  <ul class="list-group">
                    <?php 
                    $b=1;
                    for($a=1;$a<=25;$a++){
                      $online = rand(0,1);
                      if($a==21) $b=1;
                    ?>
                    <li class="list-group-item p-5 pr-10 pl-10">
                      <div class="media" style="flex-direction:initial;">
                        <div class="pr-10">
                          <a class="avatar avatar-online" href="javascript:void(0)">
                            <img class="img-fluid" src="../../global/portraits/<?=$b++?>.jpg" alt="...">
                            <?php if($online){?>
                            <i></i>
                            <?php }?>
                          </a>
                        </div>
                        <div class="media-body">
                          <div class="mt-10 black">Edward Fletcher</div>
                        </div>
                      </div>
                    </li>
                    <?php }?>
                  </ul>
                </div>
                <div class="tab-pane" id="exampleTabsIconThree" role="tabpanel">
                  <ul class="list-group">
                    <?php
                    $b=1;
                    for($a=1;$a<=10;$a++){
                      $online = rand(0,1);
                      $tmpC = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
                    ?>
                    <li class="list-group-item p-5 pr-10 pl-10">
                      <div class="media" style="flex-direction:initial;">
                        <div class="pr-10">
                          <button type="button" style="background:<?php echo $tmpC;?>;" class="btn btn-icon btn-round waves-effect waves-classic">
                          <i class="icon fa-group mr-0 white" aria-hidden="true"></i>
                          </button>
                        </div>
                        <div class="media-body">
                          <div class="black">Jigsaw Office R&D</div>
                          <ul class="addMember-items">
                            <?php for($aa=1;$aa<=rand(3,7);$aa++){?>
                            <li class="addMember-item mr-0">
                              <img style="width:20px;" class="avatar" src="../../global/portraits/<?php echo rand(1,20);?>.jpg" title="Herman Beck">
                            </li>
                            <?php }?>
                          </ul>
                          <div class="addMember-trigger pt-1 font-size-10">
                            <strong>+<?=rand(2,10)?></strong>
                          </div>
                        </div>
                      </div>
                    </li>
                    <?php }?>
                  </ul>
                </div>
              </div>

              
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- End Message Sidebar -->
    <div class="page-main">
      <!-- Chat Box -->
      <section style="border-bottom: 1px solid rgb(224, 224, 224); height: 61px; position: relative;text-align:center;">
          <div  class="hidden-md-up" style="position:absolute;left:15px;top:10px;">
              <button id="app-message-fullscreen" type="button" class="btn btn-pure font-size-26 p-5 icon md-fullscreen" aria-hidden="true"></button>
          </div>
          <div class="font-size-14 pt-10">
            <strong>Caleb Richards</strong>
            <p class="grey-500"><small>Active 10m ago</small></p>
          </div>
          <div style="position:absolute;right:15px;top:10px;">
              <button type="button" data-target="#search" data-toggle="modal" class="btn btn-pure font-size-26 p-5 icon md-search" aria-hidden="true"></button>
          </div>
        </section>
        <div class="body-message-chats">
          <div class="app-message-chats" data-plugin="scrollable">
            <div data-role="container">
              <div data-role="content">

                <button type="button" id="historyBtn" class="btn btn-round btn-default btn-flat primary-500">History Messages</button>
                <div class="chats mb-20">
                  <div class="chat">
                    <div class="chat-body">
                      <div class="chat-bubble">
                        <div class="chat-content">
                          <div class="chat-action"></div>
                          <p>
                            Hello. What can I do for you?
                          </p>
                          <span class="chat-time">8:30 am</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="chat chat-left">
                    <div class="chat-avatar">
                      <a class="avatar" data-toggle="tooltip" href="#" data-placement="left" title="">
                        <img src="../../global/portraits/5.jpg" alt="Edward Fletcher">
                      </a>
                    </div>
                    <div class="chat-body">
                      <div class="chat-bubble">
                        <div class="chat-content bg-grey-200">
                          <div class="chat-action"></div>
                          <p>
                            I'm just looking around.
                          </p>
                          <p>Will you tell me something about yourself? </p>
                          <span class="chat-time">8:30 am</span>
                        </div>
                      </div>
                      <div class="chat-bubble">
                        <div class="chat-content bg-grey-200">
                          <div class="chat-action"></div>
                          <p>
                            Are you there? That time!
                          </p>
                          <span class="chat-time">8:30 am</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="chat">
                    <div class="chat-body">
                      <div class="chat-bubble">
                        <div class="chat-content">
                          <div class="chat-action"></div>
                          <p>
                            Where?
                          </p>
                          <span class="chat-time">8:30 am</span>
                        </div>
                      </div>
                      <div class="chat-bubble">
                        <div class="chat-content">
                          <div class="chat-action"></div>
                          <p>
                            OK, my name is Limingqiang. I like singing, playing basketballand so on.
                          </p>
                          <span class="chat-time">8:30 am</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="chat-line">
                      <span class="chat-date">October 8th, 2015</span>
                  </div>
                  <div class="chat">
                    <div class="chat-body">
                      <div class="chat-bubble">
                        <div class="chat-content sticker">
                          <img src="http://in1.synerry.com/-jigsaw-chatwork/assets/img/stickers/icon1/icon1-18.png">
                          <span class="chat-time">8:30 am</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="chat chat-left">
                    <div class="chat-avatar">
                      <a class="avatar" data-toggle="tooltip" href="#" data-placement="left" title="">
                        <img src="../../global/portraits/5.jpg" alt="Edward Fletcher">
                      </a>
                    </div>
                    <div class="chat-body">
                      <div class="chat-bubble">
                        <div class="chat-content bg-grey-200">
                          <div class="chat-action"></div>
                          <p>You wait for notice.</p>
                          <span class="chat-time">8:30 am</span>
                        </div>
                      </div>
                      <div class="chat-bubble">
                        <div class="chat-content bg-grey-200">
                          <div class="chat-action"></div>
                          <p>Consectetuorem ipsum dolor sit?</p>
                          <span class="chat-time">8:30 am</span>
                        </div>
                      </div>
                      <div class="chat-bubble">
                        <div class="chat-content bg-grey-200">
                          <div class="chat-action"></div>
                          <p>OK?</p>
                          <span class="chat-time">8:30 am</span>
                        </div>
                      </div>
                      <div class="chat-bubble">
                        <div class="chat-content sticker">
                          <img src="http://in1.synerry.com/-jigsaw-chatwork/assets/img/stickers/icon2/icon2-4.png">
                          <span class="chat-time">8:30 am</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="chat">
                    <div class="chat-body">
                      <div class="chat-bubble">
                        <div class="chat-content sticker">
                          <img src="http://in1.synerry.com/-jigsaw-chatwork/assets/img/stickers/icon3/icon3-6.png">
                          <span class="chat-time">8:30 am</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="chat chat-left">
                    <div class="chat-avatar">
                      <a class="avatar avatar-sm" data-toggle="tooltip" href="#" data-placement="left" title="">
                        <img src="../../global/portraits/5.jpg" alt="Edward Fletcher">
                      </a>
                    </div>
                    <div class="chat-body">
                      <div class="chat-bubble">
                        <div class="chat-content bg-grey-200">
                          <div class="chat-action"></div>
                          <p>Crud reran and while much withdrew ardent much crab hugely met dizzily that more jeez gent equivalent unsafely far one hesitant so therefore..</p>
                          <span class="chat-time">8:30 am</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="chat">
                    <div class="chat-body">
                      <div class="chat-bubble">
                        <div class="chat-content">
                          <div class="chat-action"></div>
                          <p>Far squid and that hello fidgeted and when. As this oh darn but slapped casually husky sheared that cardinal hugely one and some unnecessary factiously hedgehog a feeling one rudely much</p>
                          <span class="chat-time">8:30 am</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          </div>

          </div>
          <!-- Message Input-->
          <form class="app-message-input pt-10" style="border-top: 1px solid rgb(224, 224, 224);">
            <div class="input-group">
              <input class="form-control" style="border:0;" type="text" placeholder="Type message here ...">
              <span class="input-group-btn">
                <button type="button" class="btn btn-pure btn-default icon md-attachment-alt font-size-24"></button>
                <button type="button" data-target="#sticker" data-toggle="modal" class="btn btn-pure btn-default icon md-mood font-size-24"></button>
              </span>
            </div>
          </form>
          <!-- End Message Input-->
      </div>
      <div class="app-message-chats-info"  data-plugin="scrollable">
        <div data-role="container">
            <div data-role="content">

                <div class="panel-group panel-group-continuous" id="Accordion" aria-multiselectable="true" role="tablist">
                  <div class="panel">
                    <div class="panel-heading" id="panel-info" role="tab">
                      <a class="panel-title" data-parent="#Accordion" data-toggle="collapse" href="#panel-info1" aria-expanded="true">
                      Information
                    </a>
                    </div>
                    <div class="panel-collapse collapse show" id="panel-info1">
                      <div class="panel-body chat-info">
                        <div class="chat-profile-img p-0">
                            <center>
                              <div class="avatar avatar-info" style="">
                                <img src="../../../assets/examples/img/accounts-5.jpg" alt="...">
                              </div>
                              <h3 class="user-name m-10">Jigaw Dev</h3>
                              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce velit
                                tortor, dictum in gravida nec</p>
                            </center>
                            <div class="pt-20">
                              <section>
                                  <ul class="list-group list-group-full">
                                    <li class="list-group-item pb-0">
                                      <span style="display:inline-block;" class="w-100 grey-500">Start</span>
                                      <span>15/03/2017</span>
                                    </li>
                                    <li class="list-group-item pb-0">
                                      <span style="display:inline-block;" class="w-100 grey-500">Finish</span>
                                      <span>4/07/2017</span>
                                    </li>
                                    <li class="list-group-item clearfix pb-0">
                                       <span style="display:inline-block;" class="w-100 grey-500">Priority</span>
                                      <span>High  Priority</span>
                                    </li>
                                    <li class="list-group-item clearfix pb-0">
                                       <span style="display:inline-block;" class="w-100 grey-500">Stutus</span>
                                      <span>Progress</span>
                                    </li>
                                    <li class="list-group-item pb-0">
                                      <span style="display:inline-block;" class="w-100 grey-500">Created</span>
                                      <span>May 5 at 11:00 AM</span>
                                    </li>
                                    <li class="list-group-item clearfix pb-0">
                                       <span style="display:inline-block;" class="w-100 grey-500">Admin</span>
                                      <span>
                                          <a class="avatar avatar-sm" style="width: 25px;" href="javascript:void(0)">
                                            <img src="../../global/portraits/8.jpg" alt="">
                                          </a>
                                          <a class="avatar avatar-sm" style="width: 25px;" href="javascript:void(0)">
                                            <img src="../../global/portraits/14.jpg" alt="">
                                          </a>
                                       </span>
                                    </li>
                                    <li class="list-group-item clearfix pb-0">
                                       <span style="display:inline-block;" class="w-100 grey-500">Report to</span>
                                      <span>
                                          <a class="avatar avatar-sm" style="width: 25px;" href="javascript:void(0)">
                                            <img src="../../global/portraits/12.jpg" alt="">
                                          </a>
                                          <a class="avatar avatar-sm" style="width: 25px;" href="javascript:void(0)">
                                            <img src="../../global/portraits/13.jpg" alt="">
                                          </a>
                                       </span>
                                    </li>
                                  </ul>
                                </section>
                              <section>
                                 <a href="#">
                                <span class="badge p-5 mb-5 badge-outline" style="border-color:#F2543B; color:#F2543B;">Designer</span>
                              </a>
                                                  <a href="#">
                                <span class="badge p-5 mb-5 badge-outline" style="border-color:#E8AD73; color:#E8AD73;">jQuery</span>
                              </a>
                                                  <a href="#">
                                <span class="badge p-5 mb-5 badge-outline" style="border-color:#C6EF3D; color:#C6EF3D;">Javascript</span>
                              </a>
                                                  <a href="#">
                                <span class="badge p-5 mb-5 badge-outline" style="border-color:#CF9650; color:#CF9650;">HTML5</span>
                              </a>
                            </section>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="panel">
                    <div class="panel-heading" id="panel-flag" role="tab">
                      <a class="panel-title" data-parent="#Accordion" data-toggle="collapse" href="#panel-flag1">
                      <i class="icon fa-flag" aria-hidden="true"></i> 3 Flag
                    </a>
                    </div>
                    <div class="panel-collapse collapse" id="panel-flag1">
                      <div class="panel-body chat-info">
                        <?php 
                          for($aa=0;$aa<3;$aa++){?>
                          <div class="chat chat-left m-0">
                              <div class="chat-avatar">
                                  <a href="profile.html" class="avatar">
                                      <img src="../../global/portraits/<?=rand(1,20)?>.jpg">
                                  </a>
                              </div>
                              <div class="chat-body">
                                  <div class="chat-bubble m-0">
                                      <div class="chat-content" style="max-width: 80%;">
                                          <p>I'm just looking around. Will you tell me something about yourself? </p>
                                          <span class="chat-time">8:35 am</span>
                                      </div>
                                      <a href="#" class="action-icon pull-right pl-10" aria-expanded="false">
                                          <i class="fa fa-close"></i>
                                      </a>
                                  </div>
                              </div>
                          </div>
                          <div class="chat-line m-0"></div>
                          <?php }?>
                      </div>
                    </div>
                  </div>
                  <div class="panel">
                    <div class="panel-heading" id="panel-Members">
                      <a class="panel-title" data-parent="#Accordion" data-toggle="collapse" href="#panel-Members1">
                      <i class="icon fa-user-circle-o" aria-hidden="true"></i> 5 Members
                    </a>
                    </div>
                    <div class="panel-collapse collapse" id="panel-Members1">
                      <div class="panel-body chat-info">
                        <ul class="list-unstyled list-inline">
                          <?php for($ab=1;$ab<=5;$ab++){?>
                            <li class="list-inline-item m-5">
                              <a class="avatar" href="javascript:void(0)">
                                  <img src="../../global/portraits/<?=rand(1,20)?>.jpg" alt="">
                                </a>
                            </li>
                          <?php }?>
                           </ul>
                      </div>
                    </div>
                  </div>
                  <div class="panel">
                    <div class="panel-heading" id="panel-Files">
                      <a class="panel-title" data-parent="#Accordion" data-toggle="collapse" href="#panel-Files1">
                      <i class="icon fa-paperclip" aria-hidden="true"></i> 6 Files
                    </a>
                    </div>
                    <div class="panel-collapse collapse" id="panel-Files1">
                      <div class="panel-body chat-info">
                        <table class="table table-hover">
                              <?php
                                unset($color);
                                unset($filename);
                                unset($file);
                                $color[] = "3f51b5";
                                $color[] = "00bcd4";
                                $color[] = "004d40";
                                $color[] = "9c27b0";
                                $color[] = "ff9800";
                                $color[] = "ffab00";
                                $color[] = "f44336";
                                $color[] = "4caf50";
                                $color[] = "616161";

                                $filename[] = "ผลการสำรวจเว็บไซต์";
                                $filename[] = "Link example & Research";
                                $filename[] = "Milestone";
                                $filename[] = "New Sitemap";
                                $filename[] = "TOR_Checklist";
                                $filename[] = "WEB Design Trend";
                                $filename[] = "wireframe inner";
                                $filename[] = "เอกสารรวบรวม Data";
                                $filename[] = "Meeting Memo";

                                $file[] = "fa-file-word-o";
                                $file[] = "fa-file-zip-o";
                                $file[] = "fa-file-video-o";
                                $file[] = "fa-file-sound-o";
                                $file[] = "fa-file-powerpoint-o";
                                $file[] = "fa-file-picture-o";
                                $file[] = "fa-file-pdf-o";
                                $file[] = "fa-file-excel-o";
                                $file[] = "fa-file-text-o";

                                for($a=1;$a<=5;$a++){
                                  $index = rand(0,8);
                                  $Fsize = rand(1,5).".".rand(1,9).rand(1,9).(rand(0,1)?" MB":" KB");
                                ?>
                                <tr>
                                  <td>
                                    <div class="media" style="flex-direction:initial;">
                                      <div class="mr-15 mt-5">
                                        <a href="javascript:void(0)">
                                          <i style="color:#<?=$color[$index]?>; font-size: 25px;" class="icon <?=$file[$index]?>" aria-hidden="true"></i>
                                        </a>
                                      </div>
                                      <div class="media-body">
                                        <a style="color:#616161;text-decoration: none;" href="javascript:void(0)"><?=$filename[$index]?></a>
                                        <p class="m-0 font-size-10">Mar 3, 2017<span class="m-l-5 m-r-5">—</span><?=$Fsize?></p>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <?php }?>
                              </table>
                              <a href="#" class="pl-10 pt-20">view more</a>
                      </div>
                    </div>
                  </div>
                  <div class="panel">
                    <div class="panel-heading" id="panel-Photo" role="tab">
                      <a class="panel-title" data-parent="#Accordion" data-toggle="collapse" href="#panel-Photo1">
                      <i class="icon fa-photo" aria-hidden="true"></i> 26 Photo/Video
                    </a>
                    </div>
                    <div class="panel-collapse collapse" id="panel-Photo1" >
                      <div class="panel-body chat-info">
                        <div class="mb-10">
                          <?php for($a=1;$a<=5;$a++){?>
                          <img class="img-circle img-bordered img-bordered-info m-5" src="../../global/photos/placeholder.png" alt="..." width="80" height="80">
                          <?php }?>
                        </div>
                        <a href="#" class="pl-10 pt-20">view more</a>
                      </div>
                    </div>
                  </div>
                  <div class="panel">
                    <div class="panel-heading" id="panel-Links" role="tab">
                      <a class="panel-title" data-parent="#Accordion" data-toggle="collapse" href="#panel-Links1">
                      <i class="icon fa-chain" aria-hidden="true"></i> 8 Links
                    </a>
                    </div>
                    <div class="panel-collapse collapse" id="panel-Links1">
                      <div class="panel-body chat-info">
                        <div class="mb-10">
                        <?php 
                          for($aa=0;$aa<5;$aa++){?>
                          <div class="m-b-10" style="border-bottom: 1px solid #dee2e6; padding-bottom: 5px;">

                            <a href="#">
                              <div class="contact-info" style="padding-left:0;white-space: nowrap; overflow: hidden;text-overflow: ellipsis;">
                                
                                  <div class="contact-name text-ellipsis"><a href="#">JigsawOffice2017.zip</a></div>
                                  <small class="contact-date">https://drive.google.com/file/d/1unB_YGcKrklUpzCoLOZsevRVz68K2bdv/view</small>   
                               
                              </div>
                            </a>
                          </div>
                          <?php }?>
                        </div>
                        <a href="#" class="pl-10 pt-20">view more</a>
                      </div>
                    </div>
                  </div>
                  
                </div>
            </div>
          </div>
      </div>
      <!-- End Chat Box -->
      
    </div>
  </div>
  <div id="chat-action" style="display: none;">
    <a href="#" class="btn btn-flat btn-default btn-block" data-toggle="dropdown">
        <i class="icon fa-ellipsis-h" aria-hidden="true"></i>
    </a>
    <div class="dropdown-menu dropdown-menu-right" role="menu">
        <a class="dropdown-item" href="javascript:void(0)">
            <i class="icon fa-reply" aria-hidden="true"></i> Reply
        </a>
        <a class="dropdown-item" href="javascript:void(0)">
            <i class="icon fa-mail-forward" aria-hidden="true"></i> Forward
        </a>
        <div style="border-bottom: 1px #eaeaea solid;"></div>
        <a class="dropdown-item" href="javascript:void(0)">
            <i class="icon fa-flag" aria-hidden="true"></i> Flag
        </a>
        <div style="border-bottom: 1px #eaeaea solid;"></div>
        <a class="dropdown-item" href="javascript:void(0)">
            <i class="icon fa-check-square-o" aria-hidden="true"></i> add to task
        </a>
        
    </div>
</div>
<div class="modal fade" id="sticker" aria-hidden="true" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple modal-center">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="icon fa-close" aria-hidden="true"></i>
        </button>
        <h4 class="modal-title">Sticker</h4>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs  mb-20">
            <?php
            for($aa=1;$aa<=6;$aa++){
            ?>
            <li class="nav-item">
              <a class="nav-link <?=($aa<=1?"active":"")?>" data-toggle="tab" href="#emo-<?=$aa?>">
                <img src="http://in1.synerry.com/-jigsaw-chatwork/assets/img/stickers/icon<?=$aa?>/icon<?=$aa?>-1.png" width="30">
              </a>
            </li>
            <?php }?>
        </ul>
        <div class="tab-content">
          <div class="content-full tab-pane show active" id="emo-1" style="max-height: 300px; overflow-y: auto;">
              <?php
              for($ab=1;$ab<=29;$ab++){
              ?>
              <a href="#" class="p-5" data-dismiss="modal" aria-label="Close" style="display: inline-block;"><img src="http://in1.synerry.com/-jigsaw-chatwork/assets/img/stickers/icon1/icon1-<?=$ab?>.png" width="50"></a>
              <?php }?>
          </div>
          <div class="content-full tab-pane" id="emo-2" style="max-height: 300px; overflow-y: auto;">
              <?php
              for($ab=1;$ab<=33;$ab++){
              ?>
              <a href="#" class="p-5" data-dismiss="modal" aria-label="Close" style="display: inline-block;"><img src="http://in1.synerry.com/-jigsaw-chatwork/assets/img/stickers/icon2/icon2-<?=$ab?>.png" width="50"></a>
              <?php }?>
          </div>
          <div class="content-full tab-pane" id="emo-3" style="max-height: 300px; overflow-y: auto;">
              <?php
              for($ab=1;$ab<=32;$ab++){
              ?>
              <a href="#" class="p-5" data-dismiss="modal" aria-label="Close"  style="display: inline-block;"><img src="http://in1.synerry.com/-jigsaw-chatwork/assets/img/stickers/icon3/icon3-<?=$ab?>.png" width="50"></a>
              <?php }?>
          </div>
          <div class="content-full tab-pane" id="emo-4" style="max-height: 300px; overflow-y: auto;">
              <?php
              for($ab=1;$ab<=16;$ab++){
              ?>
              <a href="#" class="p-5" data-dismiss="modal" aria-label="Close" style="display: inline-block;"><img src="http://in1.synerry.com/-jigsaw-chatwork/assets/img/stickers/icon4/icon4-<?=$ab?>.png" width="50"></a>
              <?php }?>
          </div>
          <div class="content-full tab-pane" id="emo-5" style="max-height: 300px; overflow-y: auto;">
              <?php
              for($ab=1;$ab<=13;$ab++){
              ?>
              <a href="#" class="p-5" data-dismiss="modal" aria-label="Close" style="display: inline-block;"><img src="http://in1.synerry.com/-jigsaw-chatwork/assets/img/stickers/icon5/icon5-<?=$ab?>.png" width="50"></a>
              <?php }?>
          </div>
          <div class="content-full tab-pane" id="emo-6" style="max-height: 300px; overflow-y: auto;">
              <?php
              for($ab=1;$ab<=9;$ab++){
              ?>
              <a href="#" class="p-5" data-dismiss="modal" aria-label="Close" style="display: inline-block;"><img src="http://in1.synerry.com/-jigsaw-chatwork/assets/img/stickers/icon6/icon6-<?=$ab?>.png" width="50"></a>
              <?php }?>
          </div>
        </div>
      </div>
      
    </div>
  </div>
</div>
<div class="modal fade" id="search" aria-hidden="true" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="icon fa-close" aria-hidden="true"></i>
        </button>
        <h4 class="modal-title">Search</h4>
      </div>
      <div class="modal-body">
        <section>
            <div class="pb-10">
              <div class="search-wrapper">
                <div class="search-box2">
                  <div class="icon md-search"></div>
                  <div class="currently-showing">
                    <?=$search['tokenhtml']?>
                  </div>
                </div>
                <a href="javascript:void(0);" class="clear_all btn btn-flat btn-default btn-block waves-effect waves-classic">Clear All</a>
                <div class="data_entry">
                  <input class="input keyword-input" placeholder="Enter a keyword" type="text">
                  <div class="icon md-close-circle close"></div>
                </div>
                <div class="filters">
                    <div class="dropdown filter_permission">
                      <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Owner
                        <span class="icon md-chevron-down" aria-hidden="true"></span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)">Caleb Richards</a>
                        <a class="dropdown-item" href="javascript:void(0)">Me</a>
                      </div>
                    </div>
                    <div class="dropdown filter_permission">
                      <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Type
                        <span class="icon md-chevron-down" aria-hidden="true"></span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)">Attachments</a>
                        <a class="dropdown-item" href="javascript:void(0)">Photos</a>
                        <a class="dropdown-item" href="javascript:void(0)">Links</a>
                        <a class="dropdown-item" href="javascript:void(0)">Tasks</a>
                        <a class="dropdown-item" href="javascript:void(0)">Flag</a>
                      </div>
                    </div>
                    <div class="dropdown filter_permission">
                      <button type="button" id="SearchAllDateRange" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Time <i class="icon md-chevron-down" aria-hidden="true"></i>
                        <span id="daterange-value"></span>
                      </button>
                    </div>
                    
                </div>
              </div>
            </div>
            
            </section>
            <section class=" bg-white p-10">
            <div class="tab-pane" id="exampleTabsTwo" role="tabpanel" aria-expanded="false">
              <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th width="20%">Owner</th>
                      <th class="hidden-sm-down" width="20%">Created</th>
                      <th class="hidden-sm-down" width="20%">File size</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $color[] = "3f51b5";
                    $color[] = "00bcd4";
                    $color[] = "004d40";
                    $color[] = "9c27b0";
                    $color[] = "ff9800";
                    $color[] = "ffab00";
                    $color[] = "f44336";
                    $color[] = "4caf50";
                    $color[] = "616161";

                    $filename[] = "ผลการสำรวจเว็บไซต์";
                    $filename[] = "Link example & Research";
                    $filename[] = "Milestone";
                    $filename[] = "New Sitemap";
                    $filename[] = "TOR_Checklist";
                    $filename[] = "WEB Design Trend";
                    $filename[] = "wireframe inner";
                    $filename[] = "เอกสารรวบรวม Data";
                    $filename[] = "Meeting Memo";

 

                    ?>
                    <?php
                    $file[] = "fa-file-word-o";
                    $file[] = "fa-file-zip-o";
                    $file[] = "fa-file-video-o";
                    $file[] = "fa-file-sound-o";
                    $file[] = "fa-file-powerpoint-o";
                    $file[] = "fa-file-picture-o";
                    $file[] = "fa-file-pdf-o";
                    $file[] = "fa-file-excel-o";
                    $file[] = "fa-file-text-o";

                    for($a=1;$a<=5;$a++){
                      $index = rand(0,8);
                      $Fsize = rand(1,5).".".rand(1,9).rand(1,9).(rand(0,1)?" MB":" KB");
                    ?>
                    <tr>
                      <td>
                        <div class="media" style="flex-direction:initial;">
                          <div class="pr-20">
                            <a class="font-size-24" href="javascript:void(0)">
                              <i style="color:#<?=$color[$index]?>" class="icon <?=$file[$index]?>" aria-hidden="true"></i>
                            </a>
                          </div>
                          <div class="media-body">
                            <h5><a style="color:#616161;text-decoration: none;" href="javascript:void(0)"><?=$filename[$index]?></a></h5>
                            <p class="hidden-md-up mb-0 grey-500">
                              <small>
                                Mar 3, 2017 <span class="ml-10 mr-10">&mdash;</span> <?=$Fsize?>
                              </small>
                            </p>
                          </div>
                        </div>
                      </td>
                      <td class="pt-15"><?=rand(0,1)?"Caleb Richards":"Me"?></td>
                      <td class="hidden-sm-down pt-15">Mar 3, 2017</td>
                      <td class="hidden-sm-down pt-15"><?=$Fsize?></td>
                    </tr>
                    <?php }?>
                  </tbody>
                </table>
            </div>
          </section>

      </div>
      
    </div>
  </div>
</div>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/autosize/autosize.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  $('.nav-item a').click(function(){
    $('.scrollable-container').animate({scrollTop: '0px'}, 200);
  });
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/jquery-placeholder.js"></script>
  <script src="../../global/js/Plugin/input-group-file.js"></script>
  <script src="../../global/js/Plugin/panel.js"></script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../assets/js/App/Message.js"></script>
  <script src="../../assets/examples/js/apps/message.js"></script>
<?php include("../_footer-form.php");?>
<script>
    $(document).ready(function(){
      $('.chat-bubble').hover(function(){
        $(this).find('.chat-action').html($('#chat-action').html());
      }, function() {
        $(this).find('.chat-action').html("");
      });
    });
</script>
</body>
</html>