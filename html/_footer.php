<footer class="site-footer" style="z-index: 0; position: relative;">
  <div class="site-footer-legal">© 2017 <a href="#">Jigsaw Office</a></div>
  <div class="site-footer-right">
    Power by <a href="#">Synerry</a>
  </div>
</footer>