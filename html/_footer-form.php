<link rel="stylesheet" href="../../global/vendor/select2/select2.css">
<script src="../../global/vendor/select2/select2.full.min.js"></script>
<script src="../../global/js/Plugin/select2.js"></script>
<link rel="stylesheet" href="../../global/css/inputsearch.css">
<script src="../../global/js/System.js"></script>
<script src="../../global/js/Search.js"></script>
<link rel="stylesheet" href="../../global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.css">
<script src="../../global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.min.js"></script>
<script src="../../global/js/Plugin/bootstrap-tokenfield.js"></script>
<link rel="stylesheet" href="../../global/vendor/icheck/icheck.css">
<script src="../../global/vendor/icheck/icheck.min.js"></script>
<script src="../../global/js/Plugin/icheck.js"></script>
<link rel="stylesheet" href="../../../global/vendor/dropify/dropify.css">
<link rel="stylesheet" href="../../global/vendor/jquery-selective/jquery-selective.css">
<link rel="stylesheet" href="../../global/vendor/bootstrap-daterangepicker/daterangepicker.css" />
<script src="../../global/vendor/bootstrap-daterangepicker/moment.min.js"></script>
<script src="../../global/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>


<script>
  
  function formatTeamSelect (state) {
    if (!state.id) {
      //return state.text;
    }
    
    var $state = $(
      '<img src="../../global/portraits/'+Math.floor((Math.random() * 20) + 1)+'.jpg" class="avatar mr-10 float-left" />'
      +' <div class="clearfix text-primary">' + state.text + '<div class="font-size-12 grey-600">'+state.text.toLowerCase()+'@jigsawoffice.com</div></div>'
    );
    return $state;
  }
  function formatWSSelect (state) {
    if (!state.id) {
      return state.text;
    }
    if(state.text2!="External"){
      var $state = $(
        '<button type="button" style="background:#757575;" class="btn btn-icon btn-round float-left mr-10 p-5 font-size-12">'
        +'<i class="icon fa-group mr-0 white"></i></button>'
        +'<h5 class="clearfix m-0 mt-3 text-primary">'+state.text+'</h5>'
      );
    }else{
      var $state = $(
        '<button type="button" style="background:#757575;" class="btn btn-icon btn-round float-left mr-10 p-5 font-size-12">'
        +'<i class="icon fa-group mr-0 white"></i></button>'
        +'<h5 class="clearfix m-0 mt-3">'+state.text+' <span class="badge badge-dark">External</span></h5>'
      );
    }
    return $state;
  }
  function formatPermissSelect (state) {
    if (!state.id) {
      return state.text;
    }
    if(state.type=="Department"){
      var $state = $(
        '<button type="button" style="background:#3f51b5;" class="btn btn-icon btn-round float-left mr-10 p-5 font-size-12">'
        +'<i class="icon fa-group mr-0 white"></i></button>'
        +'<h5 class="clearfix m-0 mt-3 text-primary">'+state.text+'</h5>'
      );
    }else if(state.type=="Position"){
      var $state = $(
        '<button type="button" style="background:#757575;" class="btn btn-icon btn-round float-left mr-10 p-5 font-size-12">'
        +'<i class="icon fa-group mr-0 white"></i></button>'
        +'<h5 class="clearfix m-0 mt-3">'+state.text+'</h5>'
      );
    }else{
      var $state = $(
      '<img style="width:20px;" src="../../global/portraits/'+Math.floor((Math.random() * 20) + 1)+'.jpg" class="avatar  mr-10 float-left" />'
      +' <div class="clearfix grey-600">' + state.text + '</div>'
    );
    }
    return $state;
  }
  $(document).ready(function() {
      $('.dropdown').on('show.bs.dropdown', function(){
          $.slidePanel.hide();
      });
      var wrap = $('.page');
      $(window).scroll(function () {
        if ($(this).scrollTop() > 89 && wrap.hasClass('no-headnav')==false) {
          wrap.addClass("fix-mini-nav-main");
        } else {
          wrap.removeClass("fix-mini-nav-main");
        }
      });

      $('#inputTokenfieldEvents,#inputTokenfieldEvents2')
      .on('tokenfield:createtoken', function(e) {
        var data = e.attrs.value.split('|');
        e.attrs.value = data[1] || data[0];
        e.attrs.label = data[1] ? data[0] + ' (' + data[1] + ')' : data[0];
      })
      .on('tokenfield:createdtoken', function(e) {
        // Über-simplistic e-mail validation
        var re = /\S+@\S+\.\S+/;
        var valid = re.test(e.attrs.value);
        if (!valid) {
          $(e.relatedTarget).addClass('invalid');
        }
      })
      .on('tokenfield:edittoken', function(e) {
        if (e.attrs.label !== e.attrs.value) {
          var label = e.attrs.label.split(' (');
          e.attrs.value = label[0] + '|' + e.attrs.value;
        }
      })
      .tokenfield();

      $('#inputUnchecked_ws').iCheck('uncheck');
      $("#inputUnchecked_ws").on("ifChanged", function(e){
        if(e.target.checked== true){
          $('#externalOption').slideDown(100);
        }else{
          $('#externalOption').slideUp(100);
        }
      });

      $('textarea.autoHeight').bind('keyup keypress focusout focus', function() {
          $(this).height('');
          var brCount = this.value.split('\n').length;
          this.rows = brCount+1; //++ To remove twitching
          var areaH = this.scrollHeight,
              lineHeight = $(this).css('line-height').replace('px',''),
              calcRows = Math.floor(areaH/lineHeight);
          this.rows = calcRows;
          if(this.value==""){
            this.rows = 2;
          }
      });

      $('.action-reply').on('click', function(e){
        var el = $(this).closest('.box-post-comment').find('.display-reply').html();
        if(el!=""){
          $(this).closest('.box-post-comment').find('.display-reply').html('');
        }else{
          $(this).closest('.box-post-comment').find('.display-reply').html($('#frm-comment-reply').html());
        }
      })


      var data_ws = [
          {
              id: 1,
              text: 'Jigsaw Office Intranet',
              text2:''
          },
          {
              id: 2,
              text: 'Jigsaw Office Intranet',
              text2:'External'
          },
          {
              id: 3,
              text: 'Jigsaw Office 2017',
              text2:''
          },
          {
              id: 4,
              text: 'Jigsaw Office 2017',
              text2:'External'
          },
          {
              id: 5,
              text: 'Jigsaw Office Saleforce',
              text2:''
          },
          {
              id: 6,
              text: 'Jigsaw Office Saleforce',
              text2:'External'
          },
          {
              id: 7,
              text: 'Synerry Website 2015',
              text2:''
          },
          {
              id: 5,
              text: 'Synerry Website 2017',
              text2:''
          }
      ];

      var data_org = [
          {
              id: 1,
              type: 'Department',
              text:'Marketing (4)'
          },
          {
              id: 2,
              type: 'Department',
              text:'CRM (3)'
          },
          {
              id: 3,
              type: 'Department',
              text:'Accounting (2)'
          },
          {
              id: 4,
              type: 'Department',
              text:'HR (3)'
          },
          {
              id: 5,
              type: 'Department',
              text:'IT (10)'
          },
          {
              id: 6,
              type: 'Position',
              text:'Position 1'
          },
          {
              id: 7,
              type: 'Position',
              text:'Position 2'
          },
          {
              id: 8,
              type: 'Position',
              text:'Position 2'
          },
          {
              id: 9,
              type: 'Position',
              text:'Position 3'
          },
          {
              id: 10,
              type: 'Position',
              text:'Position 4'
          },
          {
              id: 11,
              type: 'Personal',
              text:'Personal 1'
          },
          {
              id: 12,
              type: 'Personal',
              text:'Personal 2'
          },
          {
              id: 13,
              type: 'Personal',
              text:'Personal 2'
          },
          {
              id: 14,
              type: 'Personal',
              text:'Personal 3'
          },
          {
              id: 15,
              type: 'Personal',
              text:'Personal 4'
          }
      ];

      $('#select_permiss').select2({
        data: data_org,
        templateResult: formatPermissSelect
      });

      $('#select_ws').select2({
        data: data_ws,
        templateResult: formatWSSelect,
        dropdownParent: $("#postFormQuick")
      });

      $('#select_ws1').select2({
        data: data_ws,
        templateResult: formatWSSelect,
        dropdownParent: $("#taskForm")
      });

      $('#select_ws2').select2({
        data: data_ws,
        templateResult: formatWSSelect,
        dropdownParent: $("#CalendarForm")
      });

      $('#select_ws3').select2({
        data: data_ws,
        templateResult: formatWSSelect,
        dropdownParent: $("#inviteFormModal")
      });

      $('#selectTeam').select2({
        templateResult: formatTeamSelect,
        dropdownParent: $("#quickAddWorkspace")
      });

      $('#selectTeam').select2({
        templateResult: formatTeamSelect,
        dropdownParent: $("#FrmOrg")
      });

      $('#selectTeam2').select2({
        templateResult: formatTeamSelect,
        dropdownParent: $("#taskForm")
      });

      $('#selectTeam2').select2({
        templateResult: formatTeamSelect,
        dropdownParent: $("#ticket-form")
      });
      $('#selectTeam22').select2({
        templateResult: formatTeamSelect,
        dropdownParent: $("#ticket-detail")
      });

      $('#selectTeam3').select2({
        templateResult: formatTeamSelect,
        dropdownParent: $("#CalendarForm")
      });

      $('#selectTeam4,#selectTeam5').select2({
        templateResult: formatTeamSelect
      });
      
      $(document).on('show.bs.modal', '.modal', function (event) {
          var zIndex = 1700 + (10 * $('.modal:visible').length);
          $(this).css('z-index', zIndex);
          setTimeout(function() {
              $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
          }, 0);
      });

      $('#select-upload-folder').on('change', function(){
         var selected = $('#select-upload-folder option:selected').val();
         if(selected=="new"){
            $('#cratefolder').modal();
         }
      });

      $('#SearchAllDateRange').daterangepicker({
          "autoApply": true,
          "opens": "center",
          ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
      }, chooseSearchAllDateRange);

      $('#taskDaterange').daterangepicker({
        "autoApply": true,
        "opens": "center",
      });

      $('#calendarDaterange').daterangepicker({
        "autoApply": true,
        "opens": "center",
      });

      $('input[name=inputRadiosInvite]').on('ifChecked', function(event){
        if (this.value == 'Outsource') {
            $("#outsource").show();
            $('#emailOutsource').show();
            $('#emailStaff').hide();
        }
        else if (this.value == 'Staff') {
            $("#outsource").hide();
            $('#emailOutsource').hide();
            $('#emailStaff').show();
        }
      });

      $('.imgv-copy-view2, .imgv-master-view2').css('height',$('.imgv-master-view2').height()+'px');
      $('.imgh-copy-view2, .imgh-master-view2').css('height',$('.imgh-master-view2').height()+'px');
      $('.imgv-first-view3').css('height',$('.imgv-first-view3').height()+'px');
      $('.imgv-bottom-view31, .imgv-bottom-view32, .imgv-bottom-view41, .imgv-bottom-view42, .imgv-bottom-view43').css('height',($('.imgv-first-view3').height()/2)+'px');
      $('.imgh-first-view3, .imgh-first-view3 img').css('height',$('.imgh-first-view3').height()+'px');
      $('.imgh-right-view31, .imgh-right-view32').css('height',($('.imgh-first-view3').height()/2)+'px');
      $('.imgh-right-view41, .imgh-right-view42, .imgh-right-view43').css('height',($('.imgh-first-view3').height()/3)+'px');
      $('.imgh-right-view43 .overlay').css('line-height',($('.imgh-first-view3').height()/3)+'px');
      $('.imgv-bottom-view43 .overlay').css('line-height',($('.imgv-first-view3').height()/2)+'px');
      $('.imgh-first-view3 img').removeAttr('width').addClass('img');
     
  });


</script>
<div class="modal fade" id="postFormQuick" aria-hidden="true" aria-labelledby="postFormQuick" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple ">
        <form class="modal-content modal-content-no-border">
            <div class="modal-header">
              <h4 class="modal-title" id="exampleFillInModalTitle">Post</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="icon md-close" aria-hidden="true"></i>
              </button>
            </div>
            <div class="modal-body">
                <div class="row justify-content-md-center mt-10">
                    <div class="col-md-12 post-quick-form">
                      <div class="avatar avatar-lg float-left mr-20" href="javascript:void(0)">
                          <img src="../../global/portraits/6.jpg" alt="">
                      </div>
                      <div class="postfromarea">
                        <textarea class="autoHeight bg-grey-100" rows="2" id="postFormArea" placeholder="Whats in your mind today?" style="border: 0px; "></textarea>
                      </div>
                    </div>
                </div>
                <div class="row justify-content-md-center mt-10">
                    <div class="col-md-12 post-quick-form">
                      <h5 class="float-left mr-20">Workspace</h5>
                      <div class="postfromarea">
                        <select id="select_ws" class="form-control" style="width:auto"></select>
                      </div>
                    </div>
                </div>
                <div id="quick-attach-postinput" style="display:none;" class="row justify-content-md-center mt-10">
                    <div class="col-md-12 post-quick-form">
                      <h5 class="float-left mr-20">Attach</h5>
                      <div class="postfromarea">
                        <div style="height: 100px; overflow:hidden; overflow-x:scroll;">
                          <div style="white-space: nowrap;">
                          <?php for($aa=0;$aa<=10;$aa++){?>
                          <img class="img-circle img-bordered img-bordered-info m-5" src="../../global/photos/placeholder.png" alt="..." width="80" height="80">
                          <?php }?>
                          </div>
                        </div>
                        <div class="mt-20">
                            <div class="media card card-bordered mb-10 pl-10 pr-10" style="flex-direction:initial;">
                              <div class="pr-20">
                                <a class="font-size-40" href="javascript:void(0)">
                                  <i style="color:#ff9800" class="icon fa-file-powerpoint-o" aria-hidden="true"></i>
                                </a>
                              </div>
                              <div class="media-body">
                                <h5 class="mt-10 mb-0">
                                  <a style="color:#616161;text-decoration: none;" href="javascript:void(0)">Persentation</a>
                                </h5>
                                <p class="mb-0 grey-500">
                                  <small>2.85 MB</small>
                                </p>
                              </div>
                            </div>

                            <div class="media card card-bordered mb-10 pl-10 pr-10" style="flex-direction:initial;">
                              <div class="pr-20">
                                <a class="font-size-40" href="javascript:void(0)">
                                  <i style="color:#4caf50" class="icon fa-file-excel-o" aria-hidden="true"></i>
                                </a>
                              </div>
                              <div class="media-body">
                                <h5 class="mt-10 mb-0">
                                  <a style="color:#616161;text-decoration: none;" href="javascript:void(0)">เอกสารรวบรวม Data</a>
                                </h5>
                                <p class="mb-0 grey-500">
                                  <small>2.85 MB</small>
                                </p>
                              </div>
                            </div>

                            <div class="media card card-bordered mb-10 pl-10 pr-10" style="flex-direction:initial;">
                                <div class="pr-20">
                                  <a class="font-size-40" href="javascript:void(0)">
                                    <i style="color:#f44336" class="icon fa-file-pdf-o" aria-hidden="true"></i>
                                  </a>
                                </div>
                                <div class="media-body">
                                  <h5 class="mt-10 mb-0">
                                    <a style="color:#616161;text-decoration: none;" href="javascript:void(0)">wireframe inner</a>
                                  </h5>
                                  <p class="mb-0 grey-500">
                                    <small>2.85 MB</small>
                                  </p>
                                </div>
                              </div>
                          
                        </div>
                      </div>
                    </div>
                </div>
                <div id="quick-tags-postinput" style="display:none;" class="row justify-content-md-center mt-10">
                    <div class="col-md-12 post-quick-form">
                      <h5 class="float-left mr-20">Tags</h5>
                      <div class="postfromarea">
                        <input type="text" class="form-control" data-limit="5" data-plugin="tokenfield" value=""/>
                      </div>
                    </div>
                </div>
                <div id="quick-embed-postinput" style="display:none;" class="row justify-content-md-center mt-10">
                    <div class="col-md-12 post-quick-form">
                      <h5 class="float-left mr-20">Embed</h5>
                      <div class="postfromarea">
                        <textarea class="autoHeight bg-grey-100" rows="2" id="" placeholder="input your embed code" style="border: 0px; "></textarea>
                      </div>
                    </div>
                </div>
                <div id="quick-tags-postinput" class="row mt-10">
                    <div class="col-md-12 post-quick-form">
                      <h5 class="float-left mr-20">&nbsp;</h5>
                      <div class="postfromarea">
                        <button type="button" class="btn btn-dark waves-effect waves-classic mr-10" data-toggle="modal" href="#open-select-file" >
                          <i class="icon md-attachment-alt" aria-hidden="true"></i> Attach
                        </button>
                        <button type="button" class="btn btn-dark waves-effect waves-classic" onclick="$('#quick-tags-postinput').toggle();">
                        <i class="icon fa-tag" aria-hidden="true"></i> Tags
                      </button>
                      <button type="button" class="btn btn-dark waves-effect waves-classic ml-10" onclick="$('#quick-embed-postinput').toggle();">
                        <i class="icon fa-code" aria-hidden="true"></i> Embed
                      </button>
                      </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary waves-effect waves-classic">Post</button>
              <button type="button" class="btn btn-default btn-pure waves-effect waves-classic" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="inviteFormModal" aria-hidden="true" aria-labelledby="inviteFormModal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple ">
        <form class="modal-content modal-content-no-border">
            <div class="modal-header">
              <h4 class="modal-title" id="exampleFillInModalTitle">Invite Your Teams</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="icon md-close" aria-hidden="true"></i>
              </button>
            </div>
            <div class="modal-body">
              <div class="row justify-content-md-center">
                  <div class="col-md-12">
                    <h5 id="emailStaff">Enter email addresses only <span class="text-danger">@jigsawoffice.com</span>.</h5>
                    <h5 id="emailOutsource" style="display:none;">Enter email addresses invite people in your business.</h5>
                    <input type="text" class="form-control form-control-lg" id="inputTokenfieldEvents" data-tokens="me|me@example.com,you@example.com"/>
                  </div>
              </div>
              <div class="row justify-content-md-center">
                  <div class="col-md-12">
                    <h5>Optional</h5>
                    <textarea class="form-control" id="textareaDefault" placeholder="Include a personal message..." rows="3"></textarea>
                  </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary waves-effect waves-classic">Invite</button>
              <button type="button" class="btn btn-default btn-pure waves-effect waves-classic" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="quickAddWorkspace" aria-hidden="true" aria-labelledby="quickAddWorkspace" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple ">
        <form class="modal-content modal-content-no-border">
            <div class="modal-header">
              <h4 class="modal-title" id="exampleFillInModalTitle">Create a New Workspace</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="icon md-close" aria-hidden="true"></i>
              </button>
            </div>
            <div class="modal-body">
                <div class="row justify-content-md-center">
                    <div class="col-md-12">
                      <h5>Name</h5>
                      <input type="text" class="form-control" id="inputnetwork" placeholder="Workspace Name" />
                    </div>
                </div>
                <div class="row justify-content-md-center mt-10">
                    <div class="col-md-12">
                      <h5>Description</h5>
                      <textarea class="form-control" id="textareaDefault" placeholder="Description..." rows="3"></textarea>
                    </div>
                </div>
                <div class="row justify-content-md-center mt-10">
                    <div class="col-md-12">
                      <h5>Staff</h5>
                      <select id="selectTeam" class="form-control" multiple style="width:100%">
                        <?php
                        $teams_arr[] = "Ada.Hoppe";
                        $teams_arr[] = "Adrianna_Durgan";
                        $teams_arr[] = "Albin.Kreiger";
                        $teams_arr[] = "Alisa";
                        $teams_arr[] = "August";
                        $teams_arr[] = "Bell.Mueller";
                        $teams_arr[] = "Bret";
                        $teams_arr[] = "Ceasara_Orn";
                        $teams_arr[] = "Chester";
                        $teams_arr[] = "Citlalli_Wehner";
                        $teams_arr[] = "Clementina";
                        $teams_arr[] = "Coby";
                        $teams_arr[] = "Colin";
                        $teams_arr[] = "Damon";
                        $teams_arr[] = "Davin";
                        $teams_arr[] = "Elliott_Becker";
                        $teams_arr[] = "Emerson";
                        $teams_arr[] = "Gerhard";
                        $teams_arr[] = "Gunnar";
                        $teams_arr[] = "Gunner_Jakubowski";
                        $teams_arr[] = "Heath.Ryan";
                        $teams_arr[] = "Herta";
                        $teams_arr[] = "Hubert";
                        $teams_arr[] = "Jarvis.Simonis";
                        $teams_arr[] = "Jennie";
                        $teams_arr[] = "Johanna.Thiel";
                        $teams_arr[] = "Johnathan_Mraz";
                        $teams_arr[] = "Josephine";
                        $teams_arr[] = "Lacey";
                        $teams_arr[] = "Marjorie.Orn";
                        $teams_arr[] = "Mckenna.Herman";
                        $teams_arr[] = "Melany_Gerhold";
                        $teams_arr[] = "Miracle";
                        $teams_arr[] = "Monica";
                        $teams_arr[] = "Monique_Whitea";
                        $teams_arr[] = "Myriam_Nicolas";
                        $teams_arr[] = "Myrtie.Gerhold";
                        $teams_arr[] = "Raina";
                        $teams_arr[] = "Ruben.Reilly";
                        $teams_arr[] = "Sammie";
                        $teams_arr[] = "Shanel";
                        $teams_arr[] = "Stone_Deckow";
                        $teams_arr[] = "Terrance.Borer";
                        $teams_arr[] = "Thea";
                        $teams_arr[] = "Torrey";
                        $teams_arr[] = "Treva";
                        $teams_arr[] = "Wilhelmine";
                        $teams_arr[] = "Yasmine";
                        
                        for($bb=0;$bb<count($teams_arr);$bb++){
                          echo '<option value="'.$teams_arr[$bb].'">'.$teams_arr[$bb].'</option>';
                        }
                        ?>
                        </select>
                    </div>
                </div>
                
                <div class="row justify-content-md-center mt-20">
                    <div class="col-md-12">
                      <h5>Who can view conversations and post messages?</h5>
                      <ul class="list-unstyled">
                        <li class="mb-5">
                          <input type="radio" class="icheckbox-primary" id="inputRadiosUnchecked" name="inputRadios"
                          data-plugin="iCheck" data-radio-class="iradio_flat-blue" checked />
                          <label for="inputRadiosUnchecked"><strong>Public Access:</strong> Anyone in this network can view conversations and post</label>
                        </li>
                        <li class="mb-5">
                          <input type="radio" class="icheckbox-primary" id="inputRadiosChecked" name="inputRadios"
                          data-plugin="iCheck" data-radio-class="iradio_flat-blue"/>
                          <label for="inputRadiosChecked"><strong>Private Access:</strong> Only group members can view conversations and post</label>
                        </li>
                      </ul>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary waves-effect waves-classic">Create Workspace</button>
              <button type="button" class="btn btn-default btn-pure waves-effect waves-classic" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="searchFillIn" aria-hidden="true" aria-labelledby="searchFillIn" role="dialog" tabindex="-1">
	<div class="modal-dialog modal-simple ">
	  <div class="modal-content modal-content-no-border">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleFillInModalTitle">Search</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="icon md-close" aria-hidden="true"></i>
        </button>
      </div>
	    <div class="modal-body">
	      <form>
            <div class="row">
              <div class="col-md-12">
                <section>
                  <div class="pb-10">
                    <div class="search-wrapper">
                      <div class="search-box2">
                        <div class="icon md-search"></div>
                        <div class="currently-showing">
                          <?=$search['tokenhtml']?>
                        </div>
                      </div>
                      <a href="javascript:void(0);" class="clear_all btn btn-flat btn-default btn-block waves-effect waves-classic">Clear All</a>
                      <div class="data_entry">
                        <input class="input keyword-input" placeholder="Enter a keyword" type="text">
                        <div class="icon md-close-circle close"></div>
                      </div>
                      <div class="filters">
                          
                          <div class="dropdown filter_permission">
                            <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                              Module
                              <span class="icon md-chevron-down" aria-hidden="true"></span>
                            </button>
                            <div class="dropdown-menu" role="menu">
                              <a class="dropdown-item" href="javascript:void(0)">Workspace</a>
                              <a class="dropdown-item" href="javascript:void(0)">Task</a>
                              <a class="dropdown-item" href="javascript:void(0)">Policy</a>
                              <a class="dropdown-item" href="javascript:void(0)">Staff Directory</a>
                              <a class="dropdown-item" href="javascript:void(0)">News</a>
                              <a class="dropdown-item" href="javascript:void(0)">Announcement</a>
                              <a class="dropdown-item" href="javascript:void(0)">Photo</a>
                              <a class="dropdown-item" href="javascript:void(0)">Video</a>
                              <a class="dropdown-item" href="javascript:void(0)">Drive Center</a>
                              <a class="dropdown-item" href="javascript:void(0)">Calendar</a>
                              <a class="dropdown-item" href="javascript:void(0)">Links</a>
                              <a class="dropdown-item" href="javascript:void(0)">Vote/Poll</a>
                              <a class="dropdown-item" href="javascript:void(0)">Banner</a>
                              <a class="dropdown-item" href="javascript:void(0)">Knowledgebase</a>
                            </div>
                          </div>
                          <div class="dropdown filter_permission">
                            <button type="button" id="SearchAllDateRange" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                              Time <i class="icon md-chevron-down" aria-hidden="true"></i>
                              <span id="daterange-value"></span>
                            </button>
                          </div>
                          
                      </div>
                    </div>
                  </div>
                  
                  </section>
              </div>
            </div>
	      </form>
	    </div>
      <div class="modal-footer">
        <button type="button" onclick="location.href='../search/search.php';" class="btn btn-primary waves-effect waves-classic">Search</button>
        <button type="button" class="btn btn-default btn-pure waves-effect waves-classic" data-dismiss="modal">Close</button>
      </div>
	  </div>
	</div>
</div>

<div class="modal fade" id="SiteMap" aria-hidden="true" aria-labelledby="SiteMap" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-simple modal-full-screen" id="model-sitemap">
    <div class="modal-content modal-content-no-border">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleFillInModalTitle">@jigsawoffice.com</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <i class="icon md-close" aria-hidden="true"></i>
        </button>
      </div>
      <div class="modal-body">
        <div class="sitemap">
              <ul class="sitemap-list sitemap-list-icons list-unstyled sitemap-masonry">
                  <li class="sitemap-masonry-li"><a href="../home/"><i class="icon md-circle" aria-hidden="true"></i> Home</a>
                  <li class="menu-item-has-children sitemap-masonry-li"><i class="icon md-circle" aria-hidden="true"></i> My Space
                      <ul class="sub-menu list-unstyled pb-20">
                          <li><a href="../my-space/index.php"><i class="icon md-collection-item" aria-hidden="true"></i> My Dashboard</a>
                          </li>
                          <li><a href="../workspace/workspace2.php"><i class="icon md-collection-item" aria-hidden="true"></i> My Workspace</a>
                          </li>
                          <li><a href="../tasks/tasks-list.php"><i class="icon md-check-all" aria-hidden="true"></i> My Tasks</a>
                          </li>
                          <li><a href="../drive/drive-grid.php"><i class="icon md-cloud" aria-hidden="true"></i> My Drive</a>
                          </li>
                          <li><a href="../calendar/calendar.php"><i class="icon md-calendar" aria-hidden="true"></i> My Calendar</a>
                          </li>
                      </ul>
                  </li>
                  <li class="menu-item-has-children sitemap-masonry-li"><i class="icon md-circle" aria-hidden="true"></i> Co-Working
                      <ul class="sub-menu list-unstyled pb-20">
                          <li><a href="../workspace/workspace.php"><i class="icon md-collection-item" aria-hidden="true"></i> Workspace</a>
                          </li>
                          <li><a href="../teams/teams-cards.php"><i class="icon md-accounts-list" aria-hidden="true"></i> Teams</a>
                          </li>
                      </ul>
                  </li>
                  <li class="menu-item-has-children sitemap-masonry-li"><i class="icon md-circle" aria-hidden="true"></i> Intranet
                      <ul class="sub-menu list-unstyled pb-20">
                          <li><a href="../apps-intranet/index.php"><i class="icon md-circle-o" aria-hidden="true"></i> Intranet</a>
                          </li>
                          <li><a href="../apps-intranet/about.php"><i class="icon md-circle-o" aria-hidden="true"></i> About Company</a>
                          </li>
                          <li><a href="../apps-intranet/orgchart.php"><i class="icon md-circle-o" aria-hidden="true"></i> Org chart</a>
                          </li>
                          <li><a href="../apps-intranet/staff.php"><i class="icon md-circle-o" aria-hidden="true"></i> Staff</a>
                          </li>
                          <li><a href="../apps-intranet/news.php"><i class="icon md-circle-o" aria-hidden="true"></i> News</a>
                          </li>
                          <li><a href="../apps-intranet/announcement.php"><i class="icon md-circle-o" aria-hidden="true"></i> Announcement</a>
                          </li>
                          <li><a href="../apps-intranet/photos.php"><i class="icon md-circle-o" aria-hidden="true"></i> Photos</a>
                          </li>
                          <li><a href="../apps-intranet/videos.php"><i class="icon md-circle-o" aria-hidden="true"></i> Video</a>
                          </li>
                          <li><a href="../apps-intranet/events-grid.php"><i class="icon md-circle-o" aria-hidden="true"></i> Events</a>
                          </li>
                          <li><a href="../apps-intranet/calendar.php"><i class="icon md-circle-o" aria-hidden="true"></i> Calendar</a>
                          </li>
                          <li><a href="../apps-intranet/policy.php"><i class="icon md-circle-o" aria-hidden="true"></i> Policy</a>
                          </li>
                      </ul>
                  </li>
                  <li class="menu-item-has-children sitemap-masonry-li"><i class="icon md-circle" aria-hidden="true"></i> CRM
                      <ul class="sub-menu list-unstyled pb-20">
                          <li><a href="../apps-crm/index.php"><i class="icon md-circle-o" aria-hidden="true"></i> CRM Dashboard</a>
                          </li>
                          <li><a href="../apps-crm/leads.php"><i class="icon md-circle-o" aria-hidden="true"></i> Leads</a>
                          </li>
                          <li><a href="../apps-crm/accounts.php"><i class="icon md-circle-o" aria-hidden="true"></i> Accounts</a>
                          </li>
                          <li><a href="../apps-crm/contacts.php"><i class="icon md-circle-o" aria-hidden="true"></i> Contacts</a>
                          </li>
                          <li><a href="../apps-crm/campaign.php"><i class="icon md-circle-o" aria-hidden="true"></i> Campaign</a>
                          </li>
                      </ul>
                  </li>
                  <li class="menu-item-has-children sitemap-masonry-li"><i class="icon md-circle" aria-hidden="true"></i> Support Tickets
                      <ul class="sub-menu list-unstyled pb-20">
                          <li><a href="../apps-tickets/index.php"><i class="icon md-circle-o" aria-hidden="true"></i> Tickets</a>
                          </li>
                      </ul>
                  </li>
                  <li class="menu-item-has-children sitemap-masonry-li"><i class="icon md-circle" aria-hidden="true"></i> Knowledgebase
                      <ul class="sub-menu list-unstyled pb-20">
                          <li><a href="../apps-knowledgebase/index.php"><i class="icon md-circle-o" aria-hidden="true"></i> Knowledgebase</a>
                          </li>
                      </ul>
                  </li>
                  <li class="menu-item-has-children sitemap-masonry-li"><i class="icon md-circle" aria-hidden="true"></i> Mind Map
                      <ul class="sub-menu list-unstyled pb-20">
                          <li><a href="../apps-mindmap/mindmap.php"><i class="icon md-circle-o" aria-hidden="true"></i> Mind Map</a>
                          </li>
                      </ul>
                  </li>
                  <li class="menu-item-has-children sitemap-masonry-li"><i class="icon md-circle" aria-hidden="true"></i> Learning & Skills Test
                      <ul class="sub-menu list-unstyled pb-20">
                          <li><a href="../apps-learning/index.php"><i class="icon md-circle-o" aria-hidden="true"></i> Learning & Training</a>
                          </li>
                          <li><a href="../apps-learning/skills.php"><i class="icon md-circle-o" aria-hidden="true"></i> Skills Test</a>
                          </li>
                      </ul>
                  </li>
                  <li class="menu-item-has-children sitemap-masonry-li"><i class="icon md-circle" aria-hidden="true"></i> Workflow
                      <ul class="sub-menu list-unstyled pb-20">
                          <li><a href="../apps-workflow/index.php"><i class="icon md-circle-o" aria-hidden="true"></i> Workflow</a>
                          </li>
                      </ul>
                  </li>
                  
                  <li class="menu-item-has-children sitemap-masonry-li"><i class="icon md-circle" aria-hidden="true"></i> Form Builder
                      <ul class="sub-menu list-unstyled pb-20">
                          <li><a href="../apps-formbuilder/index.php"><i class="icon md-circle-o" aria-hidden="true"></i> Form Builder</a>
                          </li>
                      </ul>
                  </li>
                  <li class="menu-item-has-children sitemap-masonry-li"><i class="icon md-circle" aria-hidden="true"></i> Site Setting
                      <ul class="sub-menu list-unstyled pb-20">
                          <li><a href="../site-settings/settings.php"><i class="icon md-circle-o" aria-hidden="true"></i> Site Setting</a>
                          </li>
                          <li><a href="../site-settings/users.php"><i class="icon md-circle-o" aria-hidden="true"></i> User management</a>
                          </li>
                          <li><a href="../site-settings/application.php"><i class="icon md-circle-o" aria-hidden="true"></i> Applications Sitemap</a>
                          </li>
                      </ul>
                  </li>


              </ul>
          </div>
      </div>
    </div>
  </div>
</div>

<div id="frm-comment-reply" style="display:none;">
  <div class="wall-comment-reply clearfix">
    <div href="#" class="avatar avatar-md float-left">
      <img src="../../../global/portraits/6.jpg">
    </div>
    <div class="ml-60 wall-comment-form">
      <textarea rows="1" class="autoHeight" placeholder="Write a reply..."></textarea>
      <div class="wall-comment-reply-attach">
        <button class="btn btn-icon btn-dark btn-round bg-grey-400">
          <i class="icon md-attachment-alt rotate" aria-hidden="true"></i>
        </button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="open-select-file" aria-hidden="true" aria-labelledby="open-select-file" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple ">
        <form class="modal-content modal-content-no-border">
            <div class="modal-header">
              <h4 class="modal-title" id="exampleFillInModalTitle">Add attachment</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="icon md-close" aria-hidden="true"></i>
              </button>
            </div>
            <div class="modal-body">
                <div>
                  <h5 class="mt-0">
                    JigsawOffice2017 <span class="badge badge-dark">External</span> <span><i class="icon md-chevron-right" aria-hidden="true"></i></span>
                    <div class="btn-group bootstrap-select">
                      <select id="select-upload-folder" data-plugin="selectpicker">
                        <option selected="">No Folder</option>
                        <option>Project Document</option>
                        <option>Design</option>
                        <option>HTML</option>
                        <option>QC Document</option>
                        <option value="new">Create New Folder</option>
                      </select>
                    </div>
                  </h5>
                  <div class="example mb-30">
                    <div class="dropify-wrapper">
                      <div class="dropify-message">
                        <span class="file-icon"></span> 
                        <p>Drag and drop a file here or click</p>
                        <p class="dropify-error">Ooops, something wrong appended.</p>
                      </div>
                      <div class="dropify-loader"></div>
                      <div class="dropify-errors-container">
                      <ul></ul>
                    </div>
                    <input id="input-file-now" data-plugin="dropify" data-default-file="" type="file">
                    <button type="button" class="dropify-clear">Remove</button>
                    <div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p><p class="dropify-infos-message">Drag and drop or click to replace</p></div></div></div></div>
                  </div>
                </div>
                
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary waves-effect waves-classic" data-dismiss="modal" onclick="$('#quick-attach-postinput').show();">Attach</button>
              <button type="button" class="btn btn-default btn-pure waves-effect waves-classic" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="cratefolder" aria-hidden="true" aria-labelledby="cratefolder" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple " style="width:280px;">
        <form class="modal-content modal-content-no-border">
            <div class="modal-header">
              <h4 class="modal-title" id="exampleFillInModalTitle">New Folder</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="icon md-close" aria-hidden="true"></i>
              </button>
            </div>
            <div class="modal-body pt-30">
                <div class="justify-content-md-center">
                    <input class="form-control" id="inputNewFolder" value="Untitled folder" type="text" onclick="$(this).select();">
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary waves-effect waves-classic" data-dismiss="modal">Create</button>
              <button type="button" class="btn btn-default btn-pure waves-effect waves-classic" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="taskForm" aria-hidden="true" aria-labelledby="taskForm" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple ">
        <form class="modal-content modal-content-no-border">
            <div class="modal-header">
              <h4 class="modal-title" id="exampleFillInModalTitle">Task</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="icon md-close" aria-hidden="true"></i>
              </button>
            </div>
            <div class="modal-body">
                <div class="row justify-content-md-center mt-10">
                    <div class="col-md-12 post-quick-form">
                      <h5 class="float-left">Workspace</h5>
                      <div class="postfromarea">
                        <select id="select_ws1" class="form-control" style="width:auto"></select>
                      </div>
                    </div>
                </div>
                <div class="row justify-content-md-center mt-10">
                    <div class="col-md-12 post-quick-form">
                      <h5 class="float-left">Task</h5>
                      <div class="postfromarea">
                        <input type="text" class="form-control" placeholder="What to do..." value=""/>
                      </div>
                    </div>
                </div>
                <div class="row justify-content-md-center mt-10">
                    <div class="col-md-12 post-quick-form">
                      <h5 class="float-left">Detail</h5>
                      <div class="postfromarea">
                        <textarea type="text" class="form-control" rows="2" /></textarea>
                      </div>
                    </div>
                </div>
                <div class="row justify-content-md-center mt-10">
                    <div class="col-md-12 post-quick-form">
                      <h5 class="float-left">Assign to..</h5>
                      <div class="postfromarea">
                        <select id="selectTeam2" class="form-control" style="width:100%">
                          <?php
                          $teams_arr[] = "Ada.Hoppe";
                          $teams_arr[] = "Adrianna_Durgan";
                          $teams_arr[] = "Albin.Kreiger";
                          $teams_arr[] = "Alisa";
                          $teams_arr[] = "August";
                          $teams_arr[] = "Bell.Mueller";
                          $teams_arr[] = "Bret";
                          $teams_arr[] = "Ceasara_Orn";
                          $teams_arr[] = "Chester";
                          $teams_arr[] = "Citlalli_Wehner";
                          $teams_arr[] = "Clementina";
                          $teams_arr[] = "Coby";
                          $teams_arr[] = "Colin";
                          $teams_arr[] = "Damon";
                          $teams_arr[] = "Davin";
                          $teams_arr[] = "Elliott_Becker";
                          $teams_arr[] = "Emerson";
                          $teams_arr[] = "Gerhard";
                          $teams_arr[] = "Gunnar";
                          $teams_arr[] = "Gunner_Jakubowski";
                          $teams_arr[] = "Heath.Ryan";
                          $teams_arr[] = "Herta";
                          $teams_arr[] = "Hubert";
                          $teams_arr[] = "Jarvis.Simonis";
                          $teams_arr[] = "Jennie";
                          $teams_arr[] = "Johanna.Thiel";
                          $teams_arr[] = "Johnathan_Mraz";
                          $teams_arr[] = "Josephine";
                          $teams_arr[] = "Lacey";
                          $teams_arr[] = "Marjorie.Orn";
                          $teams_arr[] = "Mckenna.Herman";
                          $teams_arr[] = "Melany_Gerhold";
                          $teams_arr[] = "Miracle";
                          $teams_arr[] = "Monica";
                          $teams_arr[] = "Monique_Whitea";
                          $teams_arr[] = "Myriam_Nicolas";
                          $teams_arr[] = "Myrtie.Gerhold";
                          $teams_arr[] = "Raina";
                          $teams_arr[] = "Ruben.Reilly";
                          $teams_arr[] = "Sammie";
                          $teams_arr[] = "Shanel";
                          $teams_arr[] = "Stone_Deckow";
                          $teams_arr[] = "Terrance.Borer";
                          $teams_arr[] = "Thea";
                          $teams_arr[] = "Torrey";
                          $teams_arr[] = "Treva";
                          $teams_arr[] = "Wilhelmine";
                          $teams_arr[] = "Yasmine";
                          
                          for($bb=0;$bb<count($teams_arr);$bb++){
                            echo '<option value="'.$teams_arr[$bb].'">'.$teams_arr[$bb].'</option>';
                          }
                          ?>
                          </select>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-md-center mt-10">
                    <div class="col-md-12 post-quick-form">
                      <h5 class="float-left">Priority</h5>
                      <div class="postfromarea">
                        <ul class="list-unstyled list-inline">
                          <li class="float-left text-default mb-5">
                            <input type="radio" class="icheckbox-primary" id="inputRadiosUnchecked" name="inputRadios"
                            data-plugin="iCheck" data-radio-class="iradio_flat-blue" />
                            <label class="ml-5 mr-30" for="inputRadiosUnchecked">Low</label>
                          </li>
                          <li class="float-left text-success mb-5">
                            <input type="radio" class="icheckbox-primary" id="inputRadiosChecked" name="inputRadios"
                            data-plugin="iCheck" data-radio-class="iradio_flat-blue" checked/>
                            <label class="ml-5 mr-30" for="inputRadiosChecked">Normal</label>
                          </li>
                          <li class="float-left text-warning mb-5">
                            <input type="radio" class="icheckbox-primary" id="inputRadiosUnchecked" name="inputRadios"
                            data-plugin="iCheck" data-radio-class="iradio_flat-blue" />
                            <label class="ml-5 mr-30" for="inputRadiosUnchecked">High</label>
                          </li>
                          <li class="float-left text-danger mb-5">
                            <input type="radio" class="icheckbox-primary" id="inputRadiosUnchecked" name="inputRadios"
                            data-plugin="iCheck" data-radio-class="iradio_flat-blue" />
                            <label class="ml-5 mr-30" for="inputRadiosUnchecked">Highest</label>
                          </li>
                        </ul>
                      </div>
                    </div>
                </div>
                <div class="row justify-content-md-center mt-10">
                    <div class="col-md-12 post-quick-form">
                      <h5 class="float-left">Due date</h5>
                      <div class="postfromarea">
                        <input type="text" class="form-control" id="taskDaterange" value=""/>
                      </div>
                    </div>
                </div>
                
                <div id="task-tags-postinput" style="display:none;" class="row justify-content-md-center mt-10">
                    <div class="col-md-12 post-quick-form">
                      <h5 class="float-left mr-20">Tags</h5>
                      <div class="postfromarea">
                        <input type="text" class="form-control" data-limit="5" data-plugin="tokenfield" value=""/>
                      </div>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-12 post-quick-form">
                      <h5 class="float-left mr-20">&nbsp;</h5>
                      <div class="postfromarea">
                        <input type="file" name="" style="width: 98px;overflow: hidden; position: absolute;z-index: 2; height: 34px; opacity: 0;filter: alpha(opacity=0);">
                        <button type="button" class="btn btn-dark waves-effect waves-classic mr-20">
                          <i class="icon md-attachment-alt" aria-hidden="true"></i> Attach
                        </button>
                        <button type="button" class="btn btn-dark waves-effect waves-classic" onclick="$('#task-tags-postinput').toggle();">
                        <i class="icon fa-tag" aria-hidden="true"></i> Tags
                      </button>
                      </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary waves-effect waves-classic">Post</button>
              <button type="button" class="btn btn-default btn-pure waves-effect waves-classic" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="CalendarForm" aria-hidden="true" aria-labelledby="CalendarForm" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple ">
        <form class="modal-content modal-content-no-border">
            <div class="modal-header">
              <h4 class="modal-title" id="exampleFillInModalTitle">Calendar</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="icon md-close" aria-hidden="true"></i>
              </button>
            </div>
            <div class="modal-body">
                <div class="row justify-content-md-center mt-10">
                    <div class="col-md-12 post-quick-form">
                      <h5 class="float-left">Event</h5>
                      <div class="postfromarea">
                        <input type="text" class="form-control" placeholder="What to do..." value=""/>
                      </div>
                    </div>
                </div>
                <div class="row justify-content-md-center mt-10">
                    <div class="col-md-12 post-quick-form">
                      <h5 class="float-left">Detail</h5>
                      <div class="postfromarea">
                        <textarea type="text" class="form-control" rows="2" /></textarea>
                      </div>
                    </div>
                </div>
                <div class="row justify-content-md-center mt-10">
                    <div class="col-md-12 post-quick-form">
                      <h5 class="float-left">Calendar</h5>
                      <div class="postfromarea">
                        <select id="select_ws2" class="form-control" style="width:auto"></select>
                      </div>
                    </div>
                </div>
                <div class="row justify-content-md-center mt-10">
                    <div class="col-md-12 post-quick-form">
                      <h5 class="float-left">People</h5>
                      <div class="postfromarea">
                        <select id="selectTeam3" class="form-control" multiple style="width:100%">
                          <?php
                          $teams_arr[] = "Ada.Hoppe";
                          $teams_arr[] = "Adrianna_Durgan";
                          $teams_arr[] = "Albin.Kreiger";
                          $teams_arr[] = "Alisa";
                          $teams_arr[] = "August";
                          $teams_arr[] = "Bell.Mueller";
                          $teams_arr[] = "Bret";
                          $teams_arr[] = "Ceasara_Orn";
                          $teams_arr[] = "Chester";
                          $teams_arr[] = "Citlalli_Wehner";
                          $teams_arr[] = "Clementina";
                          $teams_arr[] = "Coby";
                          $teams_arr[] = "Colin";
                          $teams_arr[] = "Damon";
                          $teams_arr[] = "Davin";
                          $teams_arr[] = "Elliott_Becker";
                          $teams_arr[] = "Emerson";
                          $teams_arr[] = "Gerhard";
                          $teams_arr[] = "Gunnar";
                          $teams_arr[] = "Gunner_Jakubowski";
                          $teams_arr[] = "Heath.Ryan";
                          $teams_arr[] = "Herta";
                          $teams_arr[] = "Hubert";
                          $teams_arr[] = "Jarvis.Simonis";
                          $teams_arr[] = "Jennie";
                          $teams_arr[] = "Johanna.Thiel";
                          $teams_arr[] = "Johnathan_Mraz";
                          $teams_arr[] = "Josephine";
                          $teams_arr[] = "Lacey";
                          $teams_arr[] = "Marjorie.Orn";
                          $teams_arr[] = "Mckenna.Herman";
                          $teams_arr[] = "Melany_Gerhold";
                          $teams_arr[] = "Miracle";
                          $teams_arr[] = "Monica";
                          $teams_arr[] = "Monique_Whitea";
                          $teams_arr[] = "Myriam_Nicolas";
                          $teams_arr[] = "Myrtie.Gerhold";
                          $teams_arr[] = "Raina";
                          $teams_arr[] = "Ruben.Reilly";
                          $teams_arr[] = "Sammie";
                          $teams_arr[] = "Shanel";
                          $teams_arr[] = "Stone_Deckow";
                          $teams_arr[] = "Terrance.Borer";
                          $teams_arr[] = "Thea";
                          $teams_arr[] = "Torrey";
                          $teams_arr[] = "Treva";
                          $teams_arr[] = "Wilhelmine";
                          $teams_arr[] = "Yasmine";
                          
                          for($bb=0;$bb<count($teams_arr);$bb++){
                            echo '<option data-number="0" value="'.$teams_arr[$bb].'">'.$teams_arr[$bb].'</option>';
                          }

                          ?>
                          </select>
                        </div>
                    </div>
                </div>
                
                <div class="row justify-content-md-center mt-10">
                    <div class="col-md-12 post-quick-form">
                      <h5 class="float-left">Due date</h5>
                      <div class="postfromarea">
                        <input type="text" class="form-control" id="calendarDaterange" value=""/>
                      </div>
                    </div>
                </div>
                
                <div id="task-tags-postinput" style="display:none;" class="row justify-content-md-center mt-10">
                    <div class="col-md-12 post-quick-form">
                      <h5 class="float-left mr-20">Tags</h5>
                      <div class="postfromarea">
                        <input type="text" class="form-control" data-limit="5" data-plugin="tokenfield" value=""/>
                      </div>
                    </div>
                </div>
                <div class="row mt-10">
                    <div class="col-md-12 post-quick-form">
                      <h5 class="float-left mr-20">&nbsp;</h5>
                      <div class="postfromarea">
                        <button type="button" class="btn btn-dark waves-effect waves-classic mr-20">
                          <i class="icon md-attachment-alt" aria-hidden="true"></i> Attach
                        </button>
                        <button type="button" class="btn btn-dark waves-effect waves-classic" onclick="$('#task-tags-postinput').toggle();">
                        <i class="icon fa-tag" aria-hidden="true"></i> Tags
                      </button>
                      </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary waves-effect waves-classic">Post</button>
              <button type="button" class="btn btn-default btn-pure waves-effect waves-classic" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="FeedbackForm" aria-hidden="true" aria-labelledby="FeedbackForm" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple ">
        <form class="modal-content modal-content-no-border">
            <div class="modal-header">
              <h4 class="modal-title" id="exampleFillInModalTitle">Give Us Feedback</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="icon md-close" aria-hidden="true"></i>
              </button>
            </div>
            <div class="modal-body">
                <p class="mb-20 grey-500">Please feel free to provide feedback by selecting a product or feature below and including a detailed description.</p>
                <h5>Product or Feature</h5>
                <div class="row justify-content-md-center mt-10 mb-20">
                    <div class="col-md-12 post-quick-form">
                      <div class="btn-group bootstrap-select w-full">
                        <select data-plugin="selectpicker" class="w-full">
                          <option selected="">--- Please select ---</option>
                          <option>Workspace</option>
                          <option>Teams</option>
                          <option>My Sapce</option>
                          <option>Calendar</option>
                          <option>Task</option>
                          <option>Drive</option>
                          <option>Intranet - Staff</option>
                          <option>Intranet - News</option>
                          <option>Intranet - Announcement</option>
                          <option>Intranet - Photo</option>
                          <option>Intranet - Video</option>
                          <option>Intranet - Event</option>
                          <option>CRM - lead</option>
                          <option>CRM - Account</option>
                          <option>CRM - Contact</option>
                          <option>Search</option>
                          <option>Profile</option>
                          <option>Notification</option>
                          <option>Privacy</option>
                          <option>Other</option>
                        </select>
                      </div>
                    </div>
                </div>
                <h5>Your Feedback</h5>
                <div class="row justify-content-md-center mt-10 mb-20">
                    <div class="col-md-12 post-quick-form">
                      <div class="postfromarea pl-0">
                        <textarea class="autoHeight bg-grey-100" rows="2" id="postFormArea" placeholder="Please include a detailed description of your idea or suggestion." style="border: 0px; "></textarea>
                      </div>
                    </div>
                </div>
                <p class="mb-20 grey-500">Thanks for taking the time to give us feedback. Though we can’t review and respond to every submission, we do use feedback like yours to improve the JigsawOffice experience for everyone.</p>
                
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary waves-effect waves-classic">Send</button>
              <button type="button" class="btn btn-default btn-pure waves-effect waves-classic" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="reportProblemForm" aria-hidden="true" aria-labelledby="reportProblemForm" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-simple ">
        <form class="modal-content modal-content-no-border">
            <div class="modal-header">
              <h4 class="modal-title" id="exampleFillInModalTitle">Report a Problem</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="icon md-close" aria-hidden="true"></i>
              </button>
            </div>
            <div class="modal-body">
                <h5>Where is the problem?</h5>
                <div class="row justify-content-md-center mt-10 mb-20">
                    <div class="col-md-12 post-quick-form">
                      <div class="btn-group bootstrap-select w-full">
                        <select data-plugin="selectpicker" class="w-full">
                          <option selected="">--- Product or Feature ---</option>
                          <option>Workspace</option>
                          <option>Teams</option>
                          <option>My Sapce</option>
                          <option>Calendar</option>
                          <option>Task</option>
                          <option>Drive</option>
                          <option>Intranet - Staff</option>
                          <option>Intranet - News</option>
                          <option>Intranet - Announcement</option>
                          <option>Intranet - Photo</option>
                          <option>Intranet - Video</option>
                          <option>Intranet - Event</option>
                          <option>CRM - lead</option>
                          <option>CRM - Account</option>
                          <option>CRM - Contact</option>
                          <option>Search</option>
                          <option>Profile</option>
                          <option>Notification</option>
                          <option>Privacy</option>
                          <option>Other</option>
                        </select>
                      </div>
                    </div>
                </div>
                <h5>What happened?</h5>
                <div class="row justify-content-md-center mt-10 mb-20">
                    <div class="col-md-12 post-quick-form">
                      <div class="postfromarea pl-0">
                        <textarea class="autoHeight bg-grey-100" rows="2" id="postFormArea" placeholder="Briefly explain what happened and what steps we can take to reproduce the problem ..." style="border: 0px; "></textarea>
                      </div>
                    </div>
                </div>
                
                <div id="quick-tags-postinput" class="row mt-10">
                    <div class="col-md-12 post-quick-form">
                      <div class="postfromarea pl-0">
                        <button type="button" class="btn btn-dark waves-effect waves-classic mr-20" >
                          <i class="icon md-attachment-alt" aria-hidden="true"></i> Upload Screenshot(s)
                        </button>
                      </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary waves-effect waves-classic">Send</button>
              <button type="button" class="btn btn-default btn-pure waves-effect waves-classic" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>

