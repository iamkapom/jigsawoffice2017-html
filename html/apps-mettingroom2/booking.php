<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-daterangepicker/daterangepicker.css" />
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <style type="text/css">
  .fix-mini-nav + .page-content{
    padding-top: 0;
  }
  .fix-mini-nav .page-header{
    margin-bottom: 93px;
  }
  .fix-mini-nav .fix-mini-nav{
    margin-bottom: 0;
    margin-right: 0;
    margin-top: 0;
    padding: 0;
    position: fixed;
    top: 65px;
    width: 100%;
    z-index: 10;
    left: 0;
  }
  .btn-comment-post .btn-select{
    border: 0px !important;
  }
  .btn-comment-post, .btn-comment-post .bootstrap-select{
    width: 150px !important;
  }
  </style>
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-5">Booking list</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../home/index.php">Home</a></li>
        <li class="breadcrumb-item"><a href="index.php">Meeting Room</a></li>
        <li class="breadcrumb-item active">Booking list</li>
      </ol>
      <div class="page-header-actions">
        <button type="button" onclick="location.href='#';" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Setting">
          <i class="icon md-settings" aria-hidden="true"></i>
        </button>
      </div>
      <?php include("mini-nav.php");?>
    </div>
    
    <div class="page-content container-fluid bg-white" style="position: relative;">
      
      <div class="row ml-0 mr-0">
        <div class="col-md-12 pt-30">
          <section>
            <div class="pb-10">
              <div class="search-wrapper">
                <div class="search-box">
                  <div class="icon md-search"></div>
                  <div class="currently-showing">
                    <?=$search['tokenhtml']?>
                  </div>
                </div>
                <a href="javascript:void(0);" class="clear_all btn btn-flat btn-default btn-block waves-effect waves-classic">Clear All</a>
                <div class="data_entry">
                  <input class="input keyword-input" placeholder="Enter a keyword" type="text">
                  <div class="icon md-close-circle close"></div>
                </div>
                <div class="filters">
                    <div class="dropdown filter_permission">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      Meeting Room
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu" role="menu">
                      <a class="dropdown-item" href="javascript:void(0)">Meeting Room 1</a>
                      <a class="dropdown-item" href="javascript:void(0)">Meeting Room 2</a>
                      <a class="dropdown-item" href="javascript:void(0)">Meeting Room 3</a>
                      <a class="dropdown-item" href="javascript:void(0)">Meeting Room 4</a>
                      <a class="dropdown-item" href="javascript:void(0)">Meeting Room 5</a>
                    </div>
                  </div>
                    <div class="dropdown filter_permission">
                      <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Status
                        <span class="icon md-chevron-down" aria-hidden="true"></span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)">Approve</a>
                        <a class="dropdown-item" href="javascript:void(0)">Waiting Approve</a>
                        <a class="dropdown-item" href="javascript:void(0)">Daft</a>
                        <a class="dropdown-item" href="javascript:void(0)">Cancel</a>
                        <a class="dropdown-item" href="javascript:void(0)">Reject</a>
                      </div>
                    </div>
                    <div class="dropdown filter_permission">
                      <button type="button" id="SearchDateRange" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Time <i class="icon md-chevron-down" aria-hidden="true"></i>
                        <span id="daterange-value"></span>
                      </button>
                    </div>
                    
                </div>
              </div>
            </div>
            <div class="pb-20">
                <div class="actions-inner float-right">
                  <div class="dropdown">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      <i class="icon md-view-list" aria-hidden="true"></i> <i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Name
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-media w-100" role="menu">
                      <h6 class="pl-10" aria-hidden="false">View</h6>
                      <a class="dropdown-item active" href="javascript:void(0)"><i class="icon md-view-list" aria-hidden="true"></i> List</a>
                      <a class="dropdown-item" href="javascript:void(0)"><i class="icon md-view-module" aria-hidden="true"></i> Grid</a>
                      <div class="dropdown-divider"></div>
                      <h6 class="pl-10" aria-hidden="false">Sory by</h6>
                      <a class="dropdown-item active" href="javascript:void(0)">Name</a>
                      <a class="dropdown-item" href="javascript:void(0)">Date Created</a>
                      <a class="dropdown-item" href="javascript:void(0)">Activity</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-sort-amount-asc" aria-hidden="true"></i> Ascending</a>
                      <a class="dropdown-item active" href="javascript:void(0)"><i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Descending</a>
                    </div>
                  </div>
                </div>
                

                <div class="pt-10">About <strong>26</strong> results</div>
              </div>
            </section>

          <section>
            <div class="tab-pane" id="exampleTabsTwo" role="tabpanel" aria-expanded="false">
              <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>Subject</th>
                      <th width="20%">Meeting Room</th>
                      <th width="20%">Status</th>
                      <th width="5%">Owner</th>
                      <th width="3%">&nbsp</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $filename[] = "Project Intranet";
                    $filename[] = "Web Design Review";
                    $filename[] = "Customer Support Form";
                    $filename[] = "QC Report";
                    $filename[] = "Marketing Campaign";
                    $filename[] = "Sharing Solutions";
                    $filename[] = "Meeting the Challenge";
                    $filename[] = "Next Generation Leadership";

                    ?>
                    <?php 
                    for($a=1;$a<=15;$a++){
                      $index = rand(0,6);
                    ?>
                    <tr>
                      <td>
                        <h5 class="mb-5"><a class="grey-900" style="text-decoration: none;" href="javascript:void(0)"><?=$filename[$index]?></a></h5>
                        <p class="mb-0 grey-500">
                          <small>
                            <?=rand(3,10)?> People
                          </small>
                        </p>
                      </td>
                      <td class="pt-15 grey-900">
                        Meeting Room <?=rand(1,5)?>
                        <p class="mb-0 grey-500">
                          <small>
                            25/05/2018<span class="ml-10 mr-10">|</span>08:30<span class="ml-5 mr-5">&mdash;</span>11:30
                          </small>
                        </p>
                        </td>
                      <td class="hidden-sm-down pt-15">
                        <?php $ab = rand(1,5)?>
                        <?php if($ab=='1'){?>
                        <span class="badge badge-success">Approve</span>
                        <?php }?>
                        <?php if($ab=='2'){?>
                        <span class="badge badge-info">Waiting Approve</span>
                        <?php }?>
                        <?php if($ab=='3'){?>
                        <span class="badge badge-dark">Daft</span>
                        <?php }?>
                        <?php if($ab=='4'){?>
                        <span class="badge badge-default">Cancel</span>
                        <?php }?>
                        <?php if($ab=='5'){?>
                        <span class="badge badge-danger">Reject</span>
                        <?php }?>
                      </td>
                      <td class="pt-15"><img style="width:20px;" class="avatar" src="../../global/portraits/<?php echo rand(1,20);?>.jpg" title="Herman Beck"></td>
                      <td style="position:relative;">
                        <div class="wall-action" style="top:0;">
                          <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                            <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                          </button>
                          <div class="dropdown-menu dropdown-menu-right" role="menu">
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                              Edit
                            </a>
                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                              Delete
                            </a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                              View detail
                            </a>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <?php }?>
                  </tbody>
                </table>
            </div>
          </section>
        </div>
      </div>
      
    </div>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();


      function chooseDateRange(start, end) {
          var DrangeValue = start.format('MM/DD/YYYY') + '-' + end.format('MM/DD/YYYY');
          $('#SearchDateRange span#daterange-value').html(DrangeValue);
          var dataKey = "123";
          var dataValue = "xxx";
          var stoken = '';
            $('.search-wrapper .currently-showing .chooseDateRange').remove();
            stoken = '<div class="token chooseDateRange" data-key="'+dataKey+'" data-value="' + dataValue + '"><span class="token-label" >' + DrangeValue + '</span><a  href="#" class="close" tabindex="-1">×</a></div>';
            $('.search-wrapper .currently-showing').append(stoken);
            sysGenUrlSearch();
      }

      $('#SearchDateRange').daterangepicker({
          "autoApply": true,
          "opens": "center",
          ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
      }, chooseDateRange);

    });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>