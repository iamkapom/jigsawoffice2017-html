<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../assets/examples/css/forms/layouts.css">
  <link rel="stylesheet" href="../../global/vendor/icheck/icheck.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-5">Scanner Room/People</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../home/index.php">Home</a></li>
        <li class="breadcrumb-item"><a href="index.php">Meeting Room</a></li>
        <li class="breadcrumb-item active">Scanner Room/People</li>
      </ol>
      <div class="page-header-actions">
        <button type="button" onclick="location.href='#';" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Setting">
          <i class="icon md-settings" aria-hidden="true"></i>
        </button>
      </div>
      <?php include("mini-nav.php");?>
    </div>
    
    <div class="page-content container-fluid" style="position: relative;">
      <div class="page-main">
        <div class="row pt-20 ml-0 mr-0 bg-grey-400">
          <div class="col-md-6">
            <div class="card mb-10">
              <div class="card-block p-10">
                 <div class="row justify-content-md-center mt-10">
                    <div class="col-md-12 post-quick-form">
                      <h5>Date Select</h5>
                      <div class="">
                        <input type="text" class="form-control" id="taskDaterange" value=""/>
                      </div>
                    </div>
                </div> 
                <section>
                  <h5>Meeting Room Option</h5>
                  <div class="pl-30 pr-20">
                    <?php for($aa=1;$aa<=5;$aa++){?>
                    <div class="checkbox-custom checkbox-primary">
                      <input id="inputChecked<?=$aa?>" checked="" type="checkbox">
                      <label for="inputChecked<?=$aa?>">Meeting Room <?=$aa?></label>
                    </div>
                    <?php }?>
                  </div>
                </section>
                <section>
                  <h5>Room Status</h5>
                  <div class="pl-30 pr-20 list-inline-item">
                    <div class="checkbox-custom checkbox-primary">
                      <input id="inputChecked<?=$aa?>" checked="" type="checkbox">
                      <label for="inputChecked<?=$aa?>">Emtry</label>
                    </div>
                  </div>
                  <div class="pl-30 pr-20 list-inline-item">
                    <div class="checkbox-custom checkbox-primary">
                      <input id="inputChecked<?=$aa?>" checked="" type="checkbox">
                      <label for="inputChecked<?=$aa?>">Waiting Approve</label>
                    </div>
                  </div>
                </section>
                
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card mb-10">
              <div class="card-block p-10">
                  <div id="admin-frm" class="mt-20">
                    <form class="add-item" role="form" method="post" action="#">
                      <div class="form-group">
                        <h5>Invite People:</h5>
                        <select id="selectTeam4" class="form-control" multiple style="width:100%">
                        <?php
                        $teams_arr[] = "Ada.Hoppe";
                        $teams_arr[] = "Adrianna_Durgan";
                        $teams_arr[] = "Albin.Kreiger";
                        $teams_arr[] = "Alisa";
                        $teams_arr[] = "August";
                        $teams_arr[] = "Bell.Mueller";
                        $teams_arr[] = "Bret";
                        $teams_arr[] = "Ceasara_Orn";
                        $teams_arr[] = "Chester";
                        $teams_arr[] = "Citlalli_Wehner";
                        $teams_arr[] = "Clementina";
                        $teams_arr[] = "Coby";
                        $teams_arr[] = "Colin";
                        $teams_arr[] = "Damon";
                        $teams_arr[] = "Davin";
                        $teams_arr[] = "Elliott_Becker";
                        $teams_arr[] = "Emerson";
                        $teams_arr[] = "Gerhard";
                        $teams_arr[] = "Gunnar";
                        $teams_arr[] = "Gunner_Jakubowski";
                        $teams_arr[] = "Heath.Ryan";
                        $teams_arr[] = "Herta";
                        $teams_arr[] = "Hubert";
                        $teams_arr[] = "Jarvis.Simonis";
                        $teams_arr[] = "Jennie";
                        $teams_arr[] = "Johanna.Thiel";
                        $teams_arr[] = "Johnathan_Mraz";
                        $teams_arr[] = "Josephine";
                        $teams_arr[] = "Lacey";
                        $teams_arr[] = "Marjorie.Orn";
                        $teams_arr[] = "Mckenna.Herman";
                        $teams_arr[] = "Melany_Gerhold";
                        $teams_arr[] = "Miracle";
                        $teams_arr[] = "Monica";
                        $teams_arr[] = "Monique_Whitea";
                        $teams_arr[] = "Myriam_Nicolas";
                        $teams_arr[] = "Myrtie.Gerhold";
                        $teams_arr[] = "Raina";
                        $teams_arr[] = "Ruben.Reilly";
                        $teams_arr[] = "Sammie";
                        $teams_arr[] = "Shanel";
                        $teams_arr[] = "Stone_Deckow";
                        $teams_arr[] = "Terrance.Borer";
                        $teams_arr[] = "Thea";
                        $teams_arr[] = "Torrey";
                        $teams_arr[] = "Treva";
                        $teams_arr[] = "Wilhelmine";
                        $teams_arr[] = "Yasmine";
                        
                        for($bb=0;$bb<count($teams_arr);$bb++){
                          echo '<option value="'.$teams_arr[$bb].'">'.$teams_arr[$bb].'</option>';
                        }
                        ?>
                        </select>
                      </div>
                      
                    </form>
                  </div>

                  <section>
                  <h5>People Status</h5>
                  <div class="pl-30 pr-20 list-inline-item">
                    <div class="checkbox-custom checkbox-primary">
                      <input id="inputChecked<?=$aa?>" checked="" type="checkbox">
                      <label for="inputChecked<?=$aa?>">Can attend</label>
                    </div>
                  </div>
                  <div class="pl-30 pr-20 list-inline-item">
                    <div class="checkbox-custom checkbox-primary">
                      <input id="inputChecked<?=$aa?>" checked="" type="checkbox">
                      <label for="inputChecked<?=$aa?>">Unable to attend</label>
                    </div>
                  </div>
                  <div class="pl-30 pr-20 list-inline-item">
                    <div class="checkbox-custom checkbox-primary">
                      <input id="inputChecked<?=$aa?>" checked="" type="checkbox">
                      <label for="inputChecked<?=$aa?>">Uncertain</label>
                    </div>
                  </div>
                </section>
              </div>
            </div>
            
          </div>
          <div class="col-md-12 pb-20">
            <button type="button" class="btn btn-block btn-lg btn-success waves-effect waves-classic">Scanner</button>
          </div>
        </div>
        
        <div class="row mt-20 ml-0 mr-0 bg-white">
          <div class="col-md-12">
            <section>
              <div class="py-20 mb-20">
                <div class="float-right">
                  <div class="dropdown">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      <i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Sory by
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-media w-100" role="menu">
                      <h6 class="pl-10" aria-hidden="false">Sory by</h6>
                      <a class="dropdown-item active" href="javascript:void(0)">Date time</a>
                      <a class="dropdown-item" href="javascript:void(0)">Meeting Room</a>
                      <a class="dropdown-item" href="javascript:void(0)">People Can attend</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-sort-amount-asc" aria-hidden="true"></i> Ascending</a>
                      <a class="dropdown-item active" href="javascript:void(0)"><i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Descending</a>
                    </div>
                  </div>
                </div>
                
                <div class="pt-10">Total <strong>30</strong> rows</div>
              </div>
              
            </section>
            <section>
            <div class="tab-pane" id="exampleTabsTwo" role="tabpanel" aria-expanded="false">
              <table class="table table-hover">
                  <thead>
                    <tr>
                      <th >Meeting Room</th>
                      <th width="15%">People Can attend</th>
                      <th width="15%">People Unable to attend</th>
                      <th width="15%">People Uncertain</th>
                      <th width="10%">Room Status</th>
                      <th width="10%">Booking</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    for($a=1;$a<=15;$a++){
                      $index = rand(0,6);
                    ?>
                    <tr>
                      <td class="grey-900">
                        <h5 class="mb-5">Meeting Room <?=rand(1,5)?></h5>
                        <p class="mb-0 grey-500">
                          <?php 
                          $tt[] = '08:30<span class="ml-5 mr-5">&mdash;</span>11:30';
                          $tt[] = '13:00<span class="ml-5 mr-5">&mdash;</span>16:30';;
                          $tt[] = "All day";
                          ?>
                          <small>
                            25/05/2018<span class="ml-10 mr-10">|</span><?=$tt[rand(0,2)]?>
                          </small>
                        </p>
                        </td>
                      <td class="pt-15">
                        <?php 
                        for($ab=1;$ab<=rand(3,5);$ab++){
                        ?>
                        <img style="width:30px;" class="avatar" src="../../global/portraits/<?php echo rand(1,20);?>.jpg" title="Herman Beck">
                      <?php }?>
                      </td>
                      <td class="pt-15">
                        <?php 
                        for($ab=1;$ab<=rand(0,3);$ab++){
                        ?>
                        <img style="width:20px;" class="avatar" src="../../global/portraits/<?php echo rand(1,20);?>.jpg" title="Herman Beck">
                      <?php }?>
                      </td>
                      <td class="pt-15">
                        <?php 
                        for($ab=1;$ab<=rand(0,2);$ab++){
                        ?>
                        <img style="width:20px;" class="avatar" src="../../global/portraits/<?php echo rand(1,20);?>.jpg" title="Herman Beck">
                      <?php }?>
                      </td>
                      <td class="hidden-sm-down pt-10">
                        <?php $ab = rand(1,2)?>
                        <?php if($ab=='1'){?>
                        <p class="m-0 p-0">
                          <small><?=rand(1,2)?> booking</small>
                        </p>
                        <span class="badge badge-info">Waiting Approve</span>
                        
                        <?php }?>
                        <?php if($ab=='2'){?>
                        <span class="badge badge-default">Emtry</span>
                        <?php }?>
                      </td>
                      <td class="pt-20">
                        <button type="button" class="btn btn-block btn-<?=$ab=='1'?"info":"success"?> waves-effect waves-classic">Booking</button>
                      </td>
                      
                    </tr>
                    <?php }?>
                  </tbody>
                </table>
            </div>
          </section>
          </div>
        </div>

      </div>
    </div>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <script src="../../global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
  <script src="../../global/vendor/icheck/icheck.min.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../assets/js/BaseApp.js"></script>
  <script src="../../assets/js/App/Contacts.js"></script>
  <script src="../../assets/examples/js/apps/contacts.js"></script>
  <script src="../../global/js/Plugin/jquery-placeholder.js"></script>
  <script src="../../global/js/Plugin/icheck.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
<?php include("../_footer-form.php");?>
</body>
</html>