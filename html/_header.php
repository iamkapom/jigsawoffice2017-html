<?php
function get_img($imType="R"){
  $imgType = array('','vertical-','horizontal-');
  $imgT = rand(1,2);
  $maxR = 16;
  if($imgT===2) $maxR = 21;

  $img = "../../assets/examples/img/horizontal-".rand(1,21).".jpg";
  if($imType=="V"){
    $img = "../../assets/examples/img/vertical-".rand(1,16).".jpg";
  }else if($imType=="R"){
    $img = "../../assets/examples/img/".$imgType[$imgT].rand(1,$maxR).".jpg";
  }
  return $img;
}
function display_imageContainer($Numberimg=1){
  $img1 = get_img("R");
  list($width, $height) = getimagesize($img1);

 $StrImg = '<div class="img-container"><img width="100%" src="'.$img1.'" alt="..."></div>';
 if($Numberimg==2 && $width>=$height){
  $StrImg = '<div class="img-container">';
  $StrImg.= '<div class="imgv-master-view2"><img width="100%" src="'.$img1.'" alt="..."></div>';
  $StrImg.= '<div class="imgv-copy-view2"><img width="100%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '</div>';
 }else if($Numberimg==2 && $width<$height){
  $StrImg = '<div class="img-container">';
  $StrImg.= '<div class="imgh-master-view2"><img width="100%" src="'.$img1.'" alt="..."></div>';
  $StrImg.= '<div class="imgh-copy-view2"><img width="100%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '</div>';
 }else if($Numberimg==3 && $width>=$height){
  $StrImg = '<div class="img-container">';
  $StrImg.= '<div class="imgv-first-view3"><img width="100%" src="'.$img1.'" alt="..."></div>';
  $StrImg.= '<div class="imgv-bottom-view31"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '<div class="imgv-bottom-view32"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '</div>';
 }else if($Numberimg==3 && $width<$height){
  $StrImg = '<div class="img-container">';
  $StrImg.= '<div class="imgh-first-view3"><img width="140%" src="'.$img1.'" alt="..."></div>';
  $StrImg.= '<div class="imgh-right-view31"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '<div class="imgh-right-view32"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '</div>';
 }else if($Numberimg==4 && $width>=$height){
  $StrImg = '<div class="img-container">';
  $StrImg.= '<div class="imgv-first-view3"><img width="100%" src="'.$img1.'" alt="..."></div>';
  $StrImg.= '<div class="imgv-bottom-view41"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '<div class="imgv-bottom-view42"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '<div class="imgv-bottom-view43"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '</div>';
 }else if($Numberimg==4 && $width<$height){
  $StrImg = '<div class="img-container">';
  $StrImg.= '<div class="imgh-first-view3"><img width="140%" src="'.$img1.'" alt="..."></div>';
  $StrImg.= '<div class="imgh-right-view41"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '<div class="imgh-right-view42"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '<div class="imgh-right-view43"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '</div>';
 }else if($Numberimg>4 && $width>=$height){
  $StrImg = '<div class="img-container">';
  $StrImg.= '<div class="imgv-first-view3"><img width="100%" src="'.$img1.'" alt="..."></div>';
  $StrImg.= '<div class="imgv-bottom-view41"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '<div class="imgv-bottom-view42"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '<div class="imgv-bottom-view43"><div class="overlay">+'.($Numberimg-4).'</div><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '</div>';
 }else if($Numberimg>4 && $width<$height){
  $StrImg = '<div class="img-container">';
  $StrImg.= '<div class="imgh-first-view3"><img width="140%" src="'.$img1.'" alt="..."></div>';
  $StrImg.= '<div class="imgh-right-view41"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '<div class="imgh-right-view42"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '<div class="imgh-right-view43"><div class="overlay">+'.($Numberimg-4).'</div><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '</div>';
 }

 return $StrImg;
}
?>
<nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega navbar-inverse"
role="navigation" style="background-image: url(../../assets/images/pattern2.png);background-size:cover;">
  <div class="navbar-header">
    <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
    data-toggle="menubar">
      <span class="sr-only">Toggle navigation</span>
      <span class="hamburger-bar"></span>
    </button>
    <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse"
    data-toggle="collapse">
      <i class="icon md-more" aria-hidden="true"></i>
    </button>
    <div class="navbar-brand navbar-brand-center">
      <a class="p-5 m-0" href="../home/">
        Social Intranet
      </a>
    </div>
    <button type="button" class="navbar-toggler collapsed w-30 mr-5" data-target="#searchFillIn"
    data-toggle="modal">
      <span class="sr-only">Toggle Search</span>
      <i class="icon md-search" aria-hidden="true"></i>
    </button>
  </div>
  <div class="navbar-container container-fluid">
    <!-- Navbar Collapse -->
    <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
      <!-- Navbar Toolbar -->
      <ul class="nav navbar-toolbar">
        <li class="nav-item hidden-float" id="toggleMenubar">
          <a class="nav-link" data-toggle="menubar" href="#" role="button">
            <i class="icon hamburger hamburger-arrow-left">
                <span class="sr-only">Toggle menubar</span>
                <span class="hamburger-bar"></span>
              </i>
          </a>
        </li>
        <li class="nav-item hidden-float">
          <a class="nav-link icon md-search" data-toggle="modal" href="#" data-target="#searchFillIn"
          role="button">
            <span class="sr-only">Toggle Search</span>
          </a>
        </li>
      </ul>
      <!-- End Navbar Toolbar -->
      <!-- Navbar Toolbar Right -->
      <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="javascript:void(0)" title="Quick add"
          aria-expanded="false" data-animation="scale-up" role="button">
            <i class="icon md-apps bg-white text-success icon-circle m-0" aria-hidden="true"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-right dropdown-menu-media" role="menu">
            <div class="dropdown-menu-header">
              <div class="row">
                <div class="col-md-10" style="padding-top: 3px;">
                  <h5 class="p-0 m-0"><i class="icon md-apps" aria-hidden="true"></i> QUICK ADD</h5>
                </div>
              </div>
            </div>
            <div class="list-group" style="max-height:400px;">
              <div data-role="container">
                <div data-role="content">
                  <div class="row p-10 m-0">
                    <a class="list-group-item dropdown-item col-md-4 text-center p-10" href="javascript:void(0)" data-target="#postFormQuick" data-toggle="modal" role="menuitem">
                      <i class="icon md-edit font-size-30 mr-0" aria-hidden="true"></i>
                      <h6 class="mb-0">Post / Upload</h6>
                    </a>
                    <a class="list-group-item dropdown-item col-md-4 text-center p-10" href="javascript:void(0)"  data-target="#taskForm" data-toggle="modal" role="menuitem">
                      <i class="icon md-check-all font-size-30 mr-0" aria-hidden="true"></i>
                      <h6 class="mb-0">Task</h6>
                    </a>
                    <a class="list-group-item dropdown-item col-md-4 text-center p-10" href="javascript:void(0)" data-target="#CalendarForm" data-toggle="modal" role="menuitem">
                      <i class="icon md-calendar font-size-30 mr-0" aria-hidden="true"></i>
                      <h6 class="mb-0">Calendar</h6>
                    </a>
                    <a class="list-group-item dropdown-item col-md-4 text-center p-10" href="javascript:void(0)" data-target="#quickAddWorkspace" data-toggle="modal" role="menuitem">
                      <i class="icon md-collection-item font-size-30 mr-0" aria-hidden="true"></i>
                      <h6 class="mb-0">Workspace</h6>
                    </a>
                    <a class="list-group-item dropdown-item col-md-4 text-center p-10" href="javascript:void(0)" data-target="#inviteFormModal" data-toggle="modal" role="menuitem">
                      <i class="icon md-accounts-add font-size-30 mr-0" aria-hidden="true"></i>
                      <h6 class="mb-0">Invite Teams</h6>
                    </a>
                  </div>
                  <div class="row p-10 m-0">
                    <div class="col-md-12" style="border-bottom:1px #e0e0e0 solid;"></div>
                  </div>
                  <div class="row p-10 m-0">
                    <h5 class="col-md-12">Intranet</h5>
                    <a class="list-group-item dropdown-item col-md-6 text-center p-10" href="javascript:void(0)" role="menuitem">
                      <i class="icon md-font font-size-30 mr-0" aria-hidden="true"></i>
                      <h6 class="mb-0">News</h6>
                    </a>
                    <a class="list-group-item dropdown-item col-md-6 text-center p-10" href="javascript:void(0)" role="menuitem">
                      <i class="icon md-calendar-note font-size-30 mr-0" aria-hidden="true"></i>
                      <h6 class="mb-0">Events</h6>
                    </a>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false"
          data-animation="scale-up" role="button">
            <span class="avatar avatar-online">
              <img src="../../global/portraits/5.jpg" alt="...">
              <i></i>
            </span>
          </a>
          <div class="dropdown-menu" role="menu">
            <a class="dropdown-item" href="../profile/profile.php" role="menuitem"><i class="icon md-account" aria-hidden="true"></i> Profile</a>
            <a class="dropdown-item" href="../account-settings/settings.php" role="menuitem"><i class="icon md-settings" aria-hidden="true"></i> Settings</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../site-settings/users.php" role="menuitem"><i class="icon md-settings-square" aria-hidden="true"></i> Site Settings</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon md-power" aria-hidden="true"></i> Logout</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="site-sidebar" href="javascript:void(0)" title="Bookmark" data-url="../sidebar-bookmark.php">
            <i class="icon md-star" aria-hidden="true"></i>
          </a>
        </li>
        <li class="nav-item" id="toggleChat">
          <a class="nav-link" href="../messenger-v2/messenger.php" title="Chat">
            <i class="icon md-comments" aria-hidden="true"></i>
            <span class="badge badge-pill badge-danger up">3</span>
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="site-sidebar" href="javascript:void(0)" title="Notifications" data-url="../sidebar-notify.php">
            <i class="icon md-notifications" aria-hidden="true"></i>
            <span class="badge badge-pill badge-danger up">5</span>
          </a>
        </li>
        <li class="nav-item mr-10">
          <a class="nav-link" data-toggle="modal" href="javascript:void(0)" title="Sitemap"
          aria-expanded="false" data-target="#SiteMap" role="button">
            <i class="icon fa-sitemap" aria-hidden="true"></i>
          </a>
        </li>
        
      </ul>
      <!-- End Navbar Toolbar Right -->
      <div class="navbar-brand navbar-brand-center">
        <a class="p-5 m-0" href="../home/">
          Social Intranet
        </a>
      </div>
    </div>
    <!-- End Navbar Collapse -->
  </div>
</nav>