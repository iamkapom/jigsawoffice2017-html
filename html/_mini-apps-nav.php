<?php
$ppath = explode("/",getcwd());
if(count($ppath)=="1") $ppath = explode("\\",getcwd());
$cpath = $ppath[count($ppath)-1];
?>
<div class="mb-30 fix-mini-nav">
  <nav class="navbar navbar-default pt-5 pb-5" role="navigation" style="min-height:auto;">
    <div class="text-center">
      <ul class="nav nav-pills" style="display:inline-flex;">
        <li class="nav-item">
          <a class="nav-link <?=($cpath=="apps-knowledgebase"?"active":"")?>" href="../apps-knowledgebase/index.php" aria-expanded="true"><i class="icon fa-lightbulb-o" aria-hidden="true"></i> Knowledgebase</a>
        </li>
        <li class="nav-item hidden-xs-down">
          <a class="nav-link <?=($cpath=="apps-formbuilder"?"active":"")?>" href="../apps-formbuilder/index.php" aria-expanded="false"><i class="icon fa-wpforms" aria-hidden="true"></i> Form Builder</a>
        </li>
        <li class="nav-item hidden-xs-down">
          <a class="nav-link <?=(basename($_SERVER["SCRIPT_FILENAME"])=="workspace2.php"?"active":"")?>" href="javascript:void(0)" aria-expanded="false"><i class="icon fa-sticky-note-o" aria-hidden="true"></i> Note</a>
        </li>
      </ul>
    </div>
  </nav>
</div>