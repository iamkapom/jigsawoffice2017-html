<ul class="nav nav-tabs nav-tabs-line fix-mini-nav" role="tablist">
  <li class="nav-item">
    <a class="nav-link <?=(basename($_SERVER["SCRIPT_FILENAME"])=="settings.php"?"active":"")?>" href="settings.php" aria-expanded="true">Account Settings</a>
  </li>
  <li class="nav-item">
    <a class="nav-link <?=(basename($_SERVER["SCRIPT_FILENAME"])=="passowrd.php"?"active":"")?>" href="passowrd.php" aria-expanded="false">Password</a>
  </li>
  <li class="nav-item hidden-sm-down">
    <a class="nav-link <?=(basename($_SERVER["SCRIPT_FILENAME"])=="bookmark.php"?"active":"")?>" href="bookmark.php" aria-expanded="false">Bookmark</a>
  </li>
  <li class="nav-item hidden-sm-down">
    <a class="nav-link <?=(basename($_SERVER["SCRIPT_FILENAME"])=="notifications.php"?"active":"")?>" href="notifications.php" aria-expanded="false">Notifications</a>
  </li>
</ul>