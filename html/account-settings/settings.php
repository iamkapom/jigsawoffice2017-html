<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../assets/examples/css/forms/layouts.css">
  <link rel="stylesheet" href="../../global/vendor/icheck/icheck.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.css">
  <link rel="stylesheet" href="../../global/vendor/asrange/asRange.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition app-documents">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-5">Account Settings</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../home/index.php">Home</a></li>
        <li class="breadcrumb-item">Account Settings</li>
      </ol>
      
      <?php include("mini-nav.php");?>
    </div>
    
    <div class="page-content container-fluid bg-white" style="position: relative;">
      
      <div class="page-main" style="min-height:500px;">
        <div class="row pt-20 justify-content-md-center">
          <div class="col-md-8 col-lg-10">
            <div class="nav-tabs-horizontal" data-plugin="tabs">
                  <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" href="#Basics"
                      aria-controls="Basics" role="tab">Basics</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#Info"
                      aria-controls="Info" role="tab">Info</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#Contact"
                      aria-controls="Contact" role="tab">Contact</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#Skills"
                      aria-controls="Skills" role="tab">Skills</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#Work"
                      aria-controls="Work" role="tab">Work</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#Education"
                      aria-controls="Education" role="tab">Education</a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#Language"
                      aria-controls="Language" role="tab">Language</a></li>
                    <li class="dropdown nav-item" role="presentation">
                      <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#" aria-expanded="false">Menu </a>
                      <div class="dropdown-menu" role="menu">
                        <a class="active dropdown-item" data-toggle="tab" href="#Basics" aria-controls="Basics"
                        role="tab">Basics</a>
                        <a class="dropdown-item" data-toggle="tab" href="#Info" aria-controls="Info"
                        role="tab">Info</a>
                        <a class="dropdown-item" data-toggle="tab" href="#Contact" aria-controls="Contact"
                        role="tab">Contact</a>
                        <a class="dropdown-item" data-toggle="tab" href="#Skills" aria-controls="Skills"
                        role="tab">Skills</a>
                        <a class="dropdown-item" data-toggle="tab" href="#Work" aria-controls="Work"
                        role="tab">Work</a>
                        <a class="dropdown-item" data-toggle="tab" href="#Education" aria-controls="Education"
                        role="tab">Education</a>
                        <a class="dropdown-item" data-toggle="tab" href="#Language" aria-controls="Language"
                        role="tab">Language</a>
                      </div>
                    </li>
                  </ul>
                  <div class="tab-content pt-20">
                    <div class="tab-pane active" id="Basics" role="tabpanel">
                      <form class="form-horizontal">
                        <div class="form-group row form-material row">
                          <label class="col-md-4 form-control-label">FName: </label>
                          <div class="col-md-8">
                            <input type="text" class="form-control" name="name" placeholder="First Name" autocomplete="off"
                            />
                          </div>
                        </div>
                        <div class="form-group row form-material row">
                          <label class="col-md-4 form-control-label">LName: </label>
                          <div class="col-md-8">
                            <input type="text" class="form-control" name="name" placeholder="Last Name" autocomplete="off"
                            />
                          </div>
                        </div>
                        <div class="form-group row form-material row">
                          <label class="col-md-4 form-control-label">Nick Name: </label>
                          <div class="col-md-8">
                            <input type="text" class="form-control" name="name" placeholder="Nick Name" autocomplete="off"
                            />
                          </div>
                        </div>
                        <div class="form-group row form-material row">
                          <label class="col-md-4 form-control-label">Gender: </label>
                          <div class="col-md-8">
                            <div class="radio-custom radio-default radio-inline">
                              <input id="inputHorizontalMale" name="inputRadiosMale2" checked="" type="radio">
                              <label for="inputHorizontalMale">Male</label>
                            </div>
                            <div class="radio-custom radio-default radio-inline">
                              <input id="inputHorizontalFemale" name="inputRadiosMale2" type="radio">
                              <label for="inputHorizontalFemale">Female</label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group row form-material row">
                          <label class="col-md-4 form-control-label">Email: </label>
                          <div class="col-md-8">
                            <input type="text" class="form-control" name="name" value="myemail@jigsawoffice.com" disabled="" autocomplete="off"/>
                          </div>
                        </div>
                         <div class="form-group row">
                          <label class="col-md-4 form-control-label">Photo: </label>
                          <div class="col-md-8">
                            <span class="avatar avatar-lg float-left mr-30">
                              <img src="../../../global/portraits/5.jpg" alt="">
                            </span>
                            <div class="input-group-file mt-5" data-plugin="inputGroupFile">
                              <span class="input-group-btn">
                                <span class="btn btn-success btn-file">
                                  <i class="icon md-upload" aria-hidden="true"></i>
                                  <input type="file" name="" multiple="">
                                </span>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div class="form-group row form-material row">
                          <div class="col-md-9 offset-md-3">
                            <button type="button" class="btn btn-primary mr-10">Submit </button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                          </div>
                        </div>
                      </form>
                    </div>
                    <div class="tab-pane" id="Info" role="Info">
                      <form class="form-horizontal">
                        <div class="form-group row form-material row">
                          <label class="col-md-4 form-control-label">About: </label>
                          <div class="col-md-8">
                            <textarea class="form-control" placeholder="Description"></textarea>
                          </div>
                        </div>
                        <div class="form-group row form-material row">
                          <label class="col-md-4 form-control-label">Job title: </label>
                          <div class="col-md-8">
                            <input type="text" class="form-control" name="name" placeholder="Job title" autocomplete="off"/>
                          </div>
                        </div>
                        <div class="form-group row form-material row">
                          <label class="col-md-4 form-control-label">Department: </label>
                          <div class="col-md-8">
                            <input type="text" class="form-control" name="name" placeholder="Department Name" autocomplete="off">
                          </div>
                        </div>
                        <div class="form-group row form-material row">
                          <label class="col-md-4 form-control-label">Birthday: </label>
                          <div class="col-md-8">
                            <input type="text" class="form-control" placeholder="mm/dd/YYYY" data-plugin="datepicker">
                          </div>
                        </div>
                        <div class="form-group row form-material row">
                          <label class="col-md-4 form-control-label">Interest: </label>
                          <div class="col-md-8">
                            <input type="text" class="form-control" name="tags" data-plugin="tokenfield" value=""/>
                          </div>
                        </div>
                        <div class="form-group row form-material row">
                          <div class="col-md-9 offset-md-3">
                            <button type="button" class="btn btn-primary mr-10">Submit </button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                          </div>
                        </div>
                      </form>
                    </div>
                    <div class="tab-pane" id="Contact" role="Contact">
                      <form class="form-horizontal">
                        <div class="form-group row form-material row">
                          <label class="col-md-4 form-control-label">Address: </label>
                          <div class="col-md-8">
                            <textarea class="form-control" placeholder="Description"></textarea>
                          </div>
                        </div>
                        <div class="form-group row form-material row">
                          <label class="col-md-4 form-control-label">Work Number: </label>
                          <div class="col-md-8">
                            <div class="row">
                              <div class="col-md-5">
                              <input type="text" class="form-control" name="name" placeholder="Work Number" autocomplete="off"/>
                              </div>
                              <label class="col-md-3 form-control-label">Extension</label>
                              <div class="col-md-4">
                                <input type="text" class="form-control" name="name" placeholder="Extension" autocomplete="off"/>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-group row form-material row">
                          <label class="col-md-4 form-control-label">Mobile Number: </label>
                          <div class="col-md-8">
                            <input type="text" class="form-control" name="name" placeholder="Mobile Number" autocomplete="off"
                            />
                          </div>
                        </div>
                        <div class="form-group row form-material row">
                          <label class="col-md-4 form-control-label">Mobile Number: </label>
                          <div class="col-md-8">
                            <input type="text" class="form-control" name="name" placeholder="Mobile Number" autocomplete="off"
                            />
                          </div>
                        </div>
                        <div class="form-group row form-material row">
                          <label class="col-md-4 form-control-label">Skype Name: </label>
                          <div class="col-md-8">
                            <input type="text" class="form-control" name="name" placeholder="Skype Name" autocomplete="off"
                            />
                          </div>
                        </div>
                        <div class="form-group row form-material row">
                          <label class="col-md-4 form-control-label">Facebook Profile: </label>
                          <div class="col-md-8">
                            <input type="text" class="form-control" name="name" placeholder="Facebook Profile" autocomplete="off"
                            />
                          </div>
                        </div>
                        <div class="form-group row form-material row">
                          <label class="col-md-4 form-control-label">Twitter Profile: </label>
                          <div class="col-md-8">
                            <input type="text" class="form-control" name="name" placeholder="Twitter Profile" autocomplete="off"
                            />
                          </div>
                        </div>
                        <div class="form-group row form-material row">
                          <label class="col-md-4 form-control-label">Instagram Profile: </label>
                          <div class="col-md-8">
                            <input type="text" class="form-control" name="name" placeholder="Instagram Profile" autocomplete="off"
                            />
                          </div>
                        </div>
                        <div class="form-group row form-material row">
                          <label class="col-md-4 form-control-label">LinkedIn Profile: </label>
                          <div class="col-md-8">
                            <input type="text" class="form-control" name="name" placeholder="LinkedIn Profile" autocomplete="off"
                            />
                          </div>
                        </div>

                        <div class="form-group row form-material row">
                          <label class="col-md-4 form-control-label">Other Websites: </label>
                          <div class="col-md-8">
                            <div class="row">
                              <div id="otherwebsite" class="col-md-10">
                                <div id="frminput"><input type="text" class="form-control" name="name" placeholder="" autocomplete="off"/></div>
                              </div>
                              <div class="col-md-2" style="position:relative;">
                                <button style="position:absolute; bottom:5px;" onclick="$('#frminput').clone().attr('id','').find('input').val('').appendTo('#otherwebsite');" type="button" class="btn btn-icon btn-round waves-effect waves-classic">
                                  <i class="icon md-plus" aria-hidden="true"></i>
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="form-group row form-material row">
                          <div class="col-md-9 offset-md-3">
                            <button type="button" class="btn btn-primary mr-10">Submit </button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                          </div>
                        </div>
                      </form>
                    </div>
                    <div class="tab-pane" id="Skills" role="Skills">
                      <form class="form-horizontal">
                        <div id="skillfrm" class="pb-30" style="display:none;">
                              <div class="form-group row form-material">
                                <label class="col-md-4 form-control-label">Skill Name: </label>
                                <div class="col-md-8">
                                  <input type="text" class="form-control" name="name" placeholder="Skill Name" autocomplete="off"/>
                                </div>
                              </div>
                              <div class="form-group row form-material">
                                <label class="col-md-4 form-control-label"></label>
                                <div class="col-md-8 pt-20">
                                  <div class="asRange" data-plugin="asRange" data-namespace="rangeUi" data-step="5"
                      data-min="0" data-tip=true data-max="100" data-value="50"></div>
                                </div>
                              </div>
                            </div>
                        <div class="row">
                          <div id="skillfrmarea" class="col-md-10">
                            <div class="pb-30">
                              <div class="form-group row form-material">
                                <label class="col-md-4 form-control-label">Skill Name: </label>
                                <div class="col-md-8">
                                  <input type="text" class="form-control" name="name" placeholder="Skill Name" autocomplete="off"/>
                                </div>
                              </div>
                              <div class="form-group row form-material">
                                <label class="col-md-4 form-control-label"></label>
                                <div class="col-md-8 pt-20">
                                  <div class="asRange" data-plugin="asRange" data-namespace="rangeUi" data-step="5"
                      data-min="0" data-tip=true data-max="100" data-value="50"></div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                            <button style="position:absolute; bottom:50px;" onclick="$('#skillfrm').clone().attr('id','').attr('style','').appendTo('#skillfrmarea');" type="button" class="btn btn-icon btn-round waves-effect waves-classic">
                              <i class="icon md-plus" aria-hidden="true"></i>
                            </button>
                          </div>
                        </div>
                        <div class="form-group row form-material row">
                          <div class="col-md-9 offset-md-3">
                            <button type="button" class="btn btn-primary mr-10">Submit </button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                          </div>
                        </div>
                      </form>
                    </div>
                    <div class="tab-pane" id="Work" role="Work">
                      <form class="form-horizontal">
                        <div id="frmwork" class="pb-30" style="display:none;">
                              <div class="form-group row form-material">
                                <label class="col-md-4 form-control-label">Employer: </label>
                                <div class="col-md-8">
                                  <input type="text" class="form-control" name="name" placeholder="Employer" autocomplete="off"/>
                                </div>
                              </div>
                              <div class="form-group row form-material">
                                <label class="col-md-4 form-control-label">Title: </label>
                                <div class="col-md-8">
                                  <input type="text" class="form-control" name="name" placeholder="Title" autocomplete="off"/>
                                </div>
                              </div>
                              <div class="form-group row form-material">
                                <label class="col-md-4 form-control-label">Description: </label>
                                <div class="col-md-8">
                                  <input type="text" class="form-control" name="name" placeholder="Description" autocomplete="off"/>
                                </div>
                              </div>
                              <div class="form-group row form-material">
                                <label class="col-md-4 form-control-label">Dates: </label>
                                <div class="col-md-8">
                                      <select id="" name="" data-plugin="selectpicker">
                                          <option value="">Start Year</option>
                                          <?php for($a=date('Y');$a>=1956;$a--){?>
                                            <option value="<?=$a?>"><?=$a?></option>
                                          <?php }?>
                                        </select>

                                      <select id="" name="" data-plugin="selectpicker">
                                          <option value="">End Year</option>
                                            <?php for($a=date('Y');$a>=1956;$a--){?>
                                            <option value="<?=$a?>"><?=$a?></option>
                                          <?php }?>
                                        </select>
                                </div>
                              </div>
                              
                            </div>
                        <div class="row">
                          <div id="frmworkarea" class="col-md-10">
                            <div class="pb-30">
                              <div class="form-group row form-material">
                                <label class="col-md-4 form-control-label">Employer: </label>
                                <div class="col-md-8">
                                  <input type="text" class="form-control" name="name" placeholder="Employer" autocomplete="off"/>
                                </div>
                              </div>
                              <div class="form-group row form-material">
                                <label class="col-md-4 form-control-label">Title: </label>
                                <div class="col-md-8">
                                  <input type="text" class="form-control" name="name" placeholder="Title" autocomplete="off"/>
                                </div>
                              </div>
                              <div class="form-group row form-material">
                                <label class="col-md-4 form-control-label">Description: </label>
                                <div class="col-md-8">
                                  <input type="text" class="form-control" name="name" placeholder="Description" autocomplete="off"/>
                                </div>
                              </div>
                              <div class="form-group row form-material">
                                <label class="col-md-4 form-control-label">Dates: </label>
                                <div class="col-md-8">
                                      <select id="" name="" data-plugin="selectpicker">
                                          <option value="">Start Year</option>
                                          <?php for($a=date('Y');$a>=1956;$a--){?>
                                            <option value="<?=$a?>"><?=$a?></option>
                                          <?php }?>
                                        </select>

                                      <select id="" name="" data-plugin="selectpicker">
                                          <option value="">End Year</option>
                                            <?php for($a=date('Y');$a>=1956;$a--){?>
                                            <option value="<?=$a?>"><?=$a?></option>
                                          <?php }?>
                                        </select>
                                </div>
                              </div>
                              
                            </div>
                          </div>
                          <div class="col-md-2">
                            <button style="position:absolute; bottom:50px;" onclick="$('#frmwork').clone().attr('id','').attr('style','').appendTo('#frmworkarea');" type="button" class="btn btn-icon btn-round waves-effect waves-classic">
                              <i class="icon md-plus" aria-hidden="true"></i>
                            </button>
                          </div>
                        </div>
                        <div class="form-group row form-material row">
                          <div class="col-md-9 offset-md-3">
                            <button type="button" class="btn btn-primary mr-10">Submit </button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                          </div>
                        </div>
                      </form>
                    </div>
                    <div class="tab-pane" id="Education" role="Education">
                      <form class="form-horizontal">
                        <div id="frmedu" class="pb-30" style="display:none;">
                              <div class="form-group row form-material">
                                <label class="col-md-4 form-control-label">School: </label>
                                <div class="col-md-8">
                                  <input type="text" class="form-control" name="name" placeholder="School" autocomplete="off"/>
                                </div>
                              </div>
                              <div class="form-group row form-material">
                                <label class="col-md-4 form-control-label">Degree: </label>
                                <div class="col-md-8">
                                  <input type="text" class="form-control" name="name" placeholder="Degree" autocomplete="off"/>
                                </div>
                              </div>
                              <div class="form-group row form-material">
                                <label class="col-md-4 form-control-label">Description: </label>
                                <div class="col-md-8">
                                  <input type="text" class="form-control" name="name" placeholder="Description" autocomplete="off"/>
                                </div>
                              </div>
                              <div class="form-group row form-material">
                                <label class="col-md-4 form-control-label">Dates: </label>
                                <div class="col-md-8">
                                      <select id="" name="" data-plugin="selectpicker">
                                          <option value="">Start Year</option>
                                          <?php for($a=date('Y');$a>=1956;$a--){?>
                                            <option value="<?=$a?>"><?=$a?></option>
                                          <?php }?>
                                        </select>

                                      <select id="" name="" data-plugin="selectpicker">
                                          <option value="">End Year</option>
                                            <?php for($a=date('Y');$a>=1956;$a--){?>
                                            <option value="<?=$a?>"><?=$a?></option>
                                          <?php }?>
                                        </select>
                                </div>
                              </div>
                              
                            </div>
                        <div class="row">
                          <div id="frmeduarea" class="col-md-10">
                            <div class="pb-30">
                              <div class="form-group row form-material">
                                <label class="col-md-4 form-control-label">School: </label>
                                <div class="col-md-8">
                                  <input type="text" class="form-control" name="name" placeholder="School" autocomplete="off"/>
                                </div>
                              </div>
                              <div class="form-group row form-material">
                                <label class="col-md-4 form-control-label">Degree: </label>
                                <div class="col-md-8">
                                  <input type="text" class="form-control" name="name" placeholder="Degree" autocomplete="off"/>
                                </div>
                              </div>
                              <div class="form-group row form-material">
                                <label class="col-md-4 form-control-label">Description: </label>
                                <div class="col-md-8">
                                  <input type="text" class="form-control" name="name" placeholder="Description" autocomplete="off"/>
                                </div>
                              </div>
                              <div class="form-group row form-material">
                                <label class="col-md-4 form-control-label">Dates: </label>
                                <div class="col-md-8">
                                      <select id="" name="" data-plugin="selectpicker">
                                          <option value="">Start Year</option>
                                          <?php for($a=date('Y');$a>=1956;$a--){?>
                                            <option value="<?=$a?>"><?=$a?></option>
                                          <?php }?>
                                        </select>

                                      <select id="" name="" data-plugin="selectpicker">
                                          <option value="">End Year</option>
                                            <?php for($a=date('Y');$a>=1956;$a--){?>
                                            <option value="<?=$a?>"><?=$a?></option>
                                          <?php }?>
                                        </select>
                                </div>
                              </div>
                              
                            </div>
                          </div>
                          <div class="col-md-2">
                            <button style="position:absolute; bottom:50px;" onclick="$('#frmedu').clone().attr('id','').attr('style','').appendTo('#frmeduarea');" type="button" class="btn btn-icon btn-round waves-effect waves-classic">
                              <i class="icon md-plus" aria-hidden="true"></i>
                            </button>
                          </div>
                        </div>
                        <div class="form-group row form-material row">
                          <div class="col-md-9 offset-md-3">
                            <button type="button" class="btn btn-primary mr-10">Submit </button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                          </div>
                        </div>
                      </form>
                    </div>
                    <div class="tab-pane" id="Language" role="Language">
                      <form class="form-horizontal">
                        <div class="form-group row form-material row">
                          <label class="col-md-4 form-control-label">Site Language: </label>
                          <div class="col-md-8">
                            <div class="btn-group bootstrap-select">
                              <select data-plugin="selectpicker">
                                <option>TH</option>
                                <option>EN</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="form-group row form-material row">
                          <div class="col-md-9 offset-md-3">
                            <button type="button" class="btn btn-primary mr-10">Submit </button>
                            <button type="reset" class="btn btn-warning">Reset</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <script src="../../global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
  <script src="../../global/vendor/icheck/icheck.min.js"></script>
  <script src="../../global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
  <script src="../../global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.min.js"></script>
  <script src="../../global/vendor/asrange/jquery-asRange.min.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/GridMenu.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../assets/js/BaseApp.js"></script>
  <script src="../../assets/js/App/Contacts.js"></script>
  <script src="../../assets/examples/js/apps/contacts.js"></script>
  <script src="../../global/js/Plugin/jquery-placeholder.js"></script>
  <script src="../../global/js/Plugin/icheck.js"></script>
  <script src="../../global/js/Plugin/bootstrap-datepicker.js"></script>
  <script src="../../global/js/Plugin/bootstrap-tokenfield.js"></script>
  <script src="../../global/js/Plugin/responsive-tabs.js"></script>
  <script src="../../global/js/Plugin/tabs.js"></script>
  <script src="../../global/js/Plugin/asrange.js"></script>
<?php include("../_footer-form.php");?>
</body>
</html>