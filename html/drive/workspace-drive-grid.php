<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition app-documents page-aside-left">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header">
      <h1 class="page-title mb-10">Drive Grid</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item"><a href="../workspace/workspace.php">Workspace</a></li>
        <li class="breadcrumb-item"><a href="../workspace/workspace-home.php">Jigsaw Office Co-Working</a></li>
        <li class="breadcrumb-item active">Drive Grid</li>
      </ol>
    </div>
    <div class="page-content container-fluid bg-white" style="position: relative;">
      <div class="page-aside">
        <!-- Contacts Sidebar -->
        <div class="page-aside-switch">
          <i class="icon md-chevron-left" aria-hidden="true"></i>
          <i class="icon md-chevron-right" aria-hidden="true"></i>
        </div>
        <div class="page-aside-inner page-aside-scroll">
          <div data-role="container">
            <div data-role="content">
              <section class="page-aside-section pl-10">
                <a href="../workspace/workspace-home.php">
                <div class="alert alert-alt alert-danger alert-dismissible" role="alert">
                  <strong>Jigsaw Office Co-Working</strong>
                </div>
                </a>
              </section>
              <div class="page-aside-section">
                <div class="list-group">
                  <div class="list-group-item active">
                    <div class="list-content">
                      <i class="icon fa-cloud" aria-hidden="true"></i>
                      <span class="list-text">Drive</span>
                    </div>
                  </div>
                  <div class="list-group-item">
                    <div class="list-content">
                      <i class="icon fa-cloud-upload" aria-hidden="true"></i>
                      <span class="list-text">My Upload</span>
                    </div>
                  </div>
                  <div class="list-group-item">
                    <div class="list-content">
                      <i class="icon fa-clock-o" aria-hidden="true"></i>
                      <span class="list-text">Recent</span>
                    </div>
                  </div>
                  <div class="list-group-item">
                    <div class="list-content">
                      <i class="icon fa-share-alt" aria-hidden="true"></i>
                      <span class="list-text">Shared</span>
                    </div>
                  </div>
                  <div class="list-group-item">
                    <div class="list-content">
                      <i class="icon fa-trash" aria-hidden="true"></i>
                      <span class="list-text">Trash</span>
                    </div>
                  </div>
                </div>
              </div>
              <section class="page-aside-section">
                <h5 class="page-aside-title">Tags</h5>
                <div class="pl-20 pr-20">
                  <?php
                  $a[] = "HTML";
                  $a[] = "Company Profile";
                  $a[] = "Admin Manual";
                  $a[] = "TOR";
                  $a[] = "Photos";
                  $a[] = "Main page";
                  $a[] = "Banner";
                  $a[] = "Top Graphics";
                  $a[] = "Research";
                  $a[] = "Wire frame";
                  $a[] = "Design";
                  $a[] = "Powerpoint";
                  $a[] = "iStockphoto";
                  $a[] = "Comment Design";
                  $a[] = "Code";
                  $a[] = "Project Plan";
                  $a[] = "Mindmap";
                  $a[] = "Sitemap";
                  $a[] = "User Manual";
                  $a[] = "Shutterstock";
                  for($c=0;$c<20;$c++){
                    $tmpC = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
                  ?>
                  <a href="#">
                    <span class="badge p-5 mb-5 badge-outline" style="border-color:<?php echo $tmpC;?>; color:<?php echo $tmpC;?>;"><?=$a[$c];?></span>
                  </a>
                  <?php }?>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      <div class="page-main">
        <div class="page-header">
          <form class="mt-20" action="#" role="search">
            <div class="input-search input-search-dark">
              <input type="text" class="form-control w-full" placeholder="Search..." name="">
              <button type="submit" class="input-search-btn">
                <i class="icon md-search" aria-hidden="true"></i>
              </button>
            </div>
          </form>
        </div>
        <section>
          <div class="pb-30">
            <div class="float-right">
              <div class="btn-group media-arrangement btn-group-flat" role="group">
                <button onclick="location.href='workspace-drive-grid.php';" type="button" class="btn btn-icon btn-default active waves-effect waves-classic"><i class="icon md-view-module" aria-hidden="true"></i></button>
                <button type="button" class="btn btn-icon btn-default waves-effect waves-classic"><i class="icon md-view-list" aria-hidden="true"></i></button>
              </div>
            </div>
            <div class="actions-inner">
              <div class="dropdown">
                <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                  <i class="icon fa-sort-alpha-asc" aria-hidden="true"></i> Sort
                  <span class="icon md-chevron-down" aria-hidden="true"></span>
                </button>
                <div class="dropdown-menu" role="menu">
                  <a class="dropdown-item active" href="javascript:void(0)">Name</a>
                  <a class="dropdown-item" href="javascript:void(0)">Date added</a>
                  <a class="dropdown-item" href="javascript:void(0)">Date modified</a>
                  <a class="dropdown-item" href="javascript:void(0)">Size</a>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-dm-12 mb-10 grey-500">Folders</div>
          </div>
          <div class="row">
            <?php 
            $color[] = "3f51b5";
            $color[] = "00bcd4";
            $color[] = "004d40";
            $color[] = "9c27b0";
            $color[] = "ff9800";
            $color[] = "ffab00";
            $color[] = "f44336";
            $color[] = "4caf50";
            $color[] = "616161";

            $filename[] = "ผลการสำรวจเว็บไซต์";
            $filename[] = "Link example & Research";
            $filename[] = "Milestone";
            $filename[] = "New Sitemap";
            $filename[] = "TOR_Checklist";
            $filename[] = "WEB Design Trend";
            $filename[] = "wireframe inner";
            $filename[] = "เอกสารรวบรวม Data";
            $filename[] = "Meeting Memo";

            for($a=1;$a<=7;$a++){
              $randd = rand(0, 8);
            ?>
            <div class="col-md-3">
              <div class="card card-shadow card-bordered">
                <div class="card-header bg-grey-200 p-10 clearfix" style="padding-bottom: 0px;">
                  <div class="media" style="flex-direction:initial;">
                    <div class="pr-10">
                      <a data-url="panel-folder.html" data-toggle="slidePanel"  class="font-size-30" href="javascript:void(0)">
                        <i style="color:#<?=$color[$randd]?>" class="icon fa-folder" aria-hidden="true"></i>
                      </a>
                    </div>
                    <div class="media-body">
                      <h5 class="mt-15"><a style="color:#616161;" data-url="panel-folder.html" data-toggle="slidePanel" href="javascript:void(0)"><?=$filename[$randd]?></a></h5>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php }?>
          </div>
          <div class="row">
            <div class="col-dm-12 mb-10 grey-500">Files</div>
          </div>
          <div class="row">
            <?php 
            $file[] = "fa-file-word-o";
            $file[] = "fa-file-zip-o";
            $file[] = "fa-file-video-o";
            $file[] = "fa-file-sound-o";
            $file[] = "fa-file-powerpoint-o";
            $file[] = "fa-file-picture-o";
            $file[] = "fa-file-pdf-o";
            $file[] = "fa-file-excel-o";
            $file[] = "fa-file-text-o";

            for($a=1;$a<=30;$a++){
              $index = rand(0,8);
            ?>
            <div class="col-md-3">
              <div class="card card-shadow card-bordered">
                <div class="bg-grey-100 pt-20 pb-20 clearfix text-center" style="padding-bottom: 0px;">
                  <a style="color:#<?=$color[$index]?>;" data-url="panel-file.html" data-toggle="slidePanel" href="javascript:void(0)">
                    <i class="icon <?=$file[$index]?> font-size-60" aria-hidden="true"></i>
                  </a>
                </div>
                <div class="card-block text-center" style="padding: 10px 20px;">
                  <h5>
                  <a data-url="panel-file.html" data-toggle="slidePanel" href="javascript:void(0)" style="color:#<?=$color[$index]?>;"><?=$filename[$index]?></a>
                  </h5>
                  <section>
                    <?php
                    $aa[] = "Code";
                    $aa[] = "HTML";
                    for($ab=0;$ab<2;$ab++){
                      $tmpC = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
                    ?>
                    <a href="#">
                      <span class="badge mb-5 badge-outline" style="border-color:<?php echo $tmpC;?>; color:<?php echo $tmpC;?>;"><?=$aa[$ab];?></span>
                    </a>
                    <?php }?>
                  </section>
                </div>
              </div>
            </div>
            <?php }?>
            
          </div>
        </section>
      </div>
      
    </div>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../assets/js/BaseApp.js"></script>
  <script src="../../assets/js/App/Contacts.js"></script>
  <script src="../../assets/examples/js/apps/contacts.js"></script>
<?php include("../_footer-form.php");?>
</body>
</html>