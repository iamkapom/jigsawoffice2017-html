<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Waypoints in directions</title>
	<style type="text/css">
	#right-panel {
	  font-family: 'Roboto','sans-serif';
	  line-height: 30px;
	  padding-left: 10px;
	}

	#right-panel select, #right-panel input {
	  font-size: 15px;
	}

	#right-panel select {
	  width: 100%;
	}

	#right-panel i {
	  font-size: 12px;
	}
	html, body {
	  height: 100%;
	  margin: 0;
	  padding: 0;
	}
	#map {
	  height: 100%;
	  float: left;
	  width: 70%;
	  height: 100%;
	}
	#right-panel {
	  margin: 20px;
	  border-width: 2px;
	  width: 20%;
	  height: 400px;
	  float: left;
	  text-align: left;
	  padding-top: 0;
	}
	#directions-panel {
	  margin-top: 10px;
	  background-color: #FFEE77;
	  font-size: 11px;
	  padding: 10px;
	  overflow: scroll;
	  line-height: 14px;
	}
	</style>
</head>
  <body>	
	<div id="map"></div>
	<div id="right-panel">
	<div>
		<b>Start:</b>
		<input type="text" id="start" value="ซินเนอร์รี่ คอปอเรชั่น">
		<br>
		<b>End:</b>
		<input type="text" id="end" value="ศูนย์ราชการแจ้งวัฒนะ">
		<br>
	  <input type="button" id="btnok" value="OK">
	</div>
	<div id="directions-panel2"></div>
	<div id="directions-panel"></div>
	</div>

<script type="text/javascript">
	function initMap() {
	  var directionsService = new google.maps.DirectionsService;
	  var directionsDisplay = new google.maps.DirectionsRenderer;
	  var map = new google.maps.Map(document.getElementById('map'), {
	    zoom: 6,
	    center: {lat: 13.898079, lng: 100.638604}
	  });
	  directionsDisplay.setMap(map);

	  document.getElementById('btnok').addEventListener('click', function() {
	    calculateAndDisplayRoute(directionsService, directionsDisplay);
	  });
	}

	function calculateAndDisplayRoute(directionsService, directionsDisplay) {
	  var waypts = [];
	  

	  directionsService.route({
	    origin: document.getElementById('start').value,
	    destination: document.getElementById('end').value,
	    waypoints: waypts,
	    optimizeWaypoints: true,
	    travelMode: 'DRIVING'
	  }, function(response, status) {
	    if (status === 'OK') {
	      directionsDisplay.setDirections(response);
	      var route = response.routes[0];
	      var summaryPanel = document.getElementById('directions-panel');
	      var summaryPanel2 = document.getElementById('directions-panel2');
	      summaryPanel.innerHTML = '';
	      summaryPanel2.innerHTML = '';
	      for (var i = 0; i < route.legs.length; i++) {
	        var routeSegment = i + 1;
	        summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
	        summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
	        summaryPanel2.innerHTML +=route.legs[i].distance.text;
	      }
	    } else {
	      window.alert('Directions request failed due to ' + status);
	    }
	  });
	}
	</script>	
	<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCkUOdZ5y7hMm0yrcCQoCvLwzdM6M8s5qk&callback=initMap">
</script>
<br><br>
<h1>Code Example</h1>
<div>https://jsfiddle.net/1p4hsq5e/10/</div>
<div>http://map.rimnam.com/</div>
</body>
</html>