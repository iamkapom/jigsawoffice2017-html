<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/blueimp-file-upload/jquery.fileupload.css">
  <link rel="stylesheet" href="../../global/vendor/dropify/dropify.css">
  <link rel="stylesheet" href="../../global/vendor/icheck/icheck.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
  <link rel="stylesheet" href="../../global/vendor/timepicker/jquery-timepicker.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-5">Booking form</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../home/index.php">Home</a></li>
        <li class="breadcrumb-item"><a href="index.php">Booking Car</a></li>
        <li class="breadcrumb-item active">Booking form</li>
      </ol>
      <?php include("mini-nav.php");?>
    </div>

    <div class="page-content container-fluid" style="position: relative;">
      
      <div class="row ml-0 mr-0">
        <div class="col-md-12">
          
          <div class="page-main">
          <div class="panel">
            <div class="panel-body container-fluid">
              <div class="row">
                <div class="col-md-12">
                  <div class="example-wrap">
                    <div class="example">
                      <form id="bookingcar" class="form-horizontal">
                        <div class="form-group row">
                          <label class="col-md-3 form-control-label">ข้อมูลรถ: </label>
                          <div class="col-md-9">
                            <div class="bootstrap-select" >
                              <select data-plugin="selectpicker">
                                <option value="AF">จองแบบไม่ระบุรถ</option>
                                <option value="">รถเก๋ง (4 ที่นั่ง), โตโยต้า แครมรี , กข 1234 กทม</option>
                                <option value="AF">รถเก๋ง (4 ที่นั่ง), โตโยต้า แครมรี , กข 1234 กทม</option>
                                <option value="AF">รถเก๋ง (4 ที่นั่ง), โตโยต้า วีออส, กข 1122 กทม</option>
                                <option value="AF">รถเก๋ง (4 ที่นั่ง), โตโยต้า วีออส, กข 1122 กทม</option>
                                <option value="AF">รถตู้ (12 ที่นั่ง), โตโยต้า ไฮเอซ, กข 4567 กทม</option>
                                <option value="AF">รถตู้ (12 ที่นั่ง), โตโยต้า ไฮเอซ, กข 4567 กทม</option>
                                <option value="AF">รถตู้ (12 ที่นั่ง), โตโยต้า , กข 4567 กทม</option>
                                <option value="AF">รถตู้ (12 ที่นั่ง), โตโยต้า , กข 4567 กทม</option>
                                <option value="AF">รถตู้ (12 ที่นั่ง), โตโยต้า , กข 4567 กทม</option>
                                <option value="AF">รถตู้ (12 ที่นั่ง), โตโยต้า , กข 4567 กทม</option>
                                <option value="AF">รถตู้ (12 ที่นั่ง), โตโยต้า , กข 4567 กทม</option>
                                <option value="AF">รถตู้ (12 ที่นั่ง), โตโยต้า , กข 4567 กทม</option>
                                <option value="AF">รถตู้ (12 ที่นั่ง), โตโยต้า , กข 4567 กทม</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-md-3 form-control-label">จองใช้เพื่อ: </label>
                          <div class="col-md-9">
                            <input class="form-control" name="name" placeholder="" autocomplete="off" type="text">
                          </div>
                        </div>
                        <div id="frm_datepair" class="form-group row">
                          <label class="col-md-3 form-control-label">วัน/เวลาที่จอง: </label>
                          <div class="col-md-9">
                            <div class="row">
                                <div class="input-daterange-wrap col-md-5">
                                  <div id="dStart" class="input-daterange row">
                                    <div class="input-group col-md-6 pr-0">
                                      <span class="input-group-addon">
                                        <i class="icon md-calendar" aria-hidden="true"></i>
                                      </span>
                                      <input type="text" class="form-control date start" value="6/20/2018">
                                    </div>
                                    <div class="input-group col-md-6 pl-0">
                                      <span class="input-group-addon">
                                        <i class="icon md-time" aria-hidden="true"></i>
                                      </span>
                                      <input type="text" class="form-control time start" value="09:30"/>
                                    </div>
                                  </div>
                                </div>
                                <label class="col-md-2 form-control-label text-center">ถึง </label>
                                <div class="input-daterange-wrap col-md-5">
                                  <div id="dStop" class="input-daterange row">
                                    <div class="input-group col-md-6 pr-0">
                                      <span class="input-group-addon">
                                        <i class="icon md-calendar" aria-hidden="true"></i>
                                      </span>
                                      <input type="text" class="form-control date end" value="6/20/2018">
                                    </div>
                                    <div class="input-group col-md-6 pl-0">
                                      <span class="input-group-addon">
                                        <i class="icon md-time" aria-hidden="true"></i>
                                      </span>
                                      <input type="text" class="form-control time end" value="16:00"/>
                                    </div>
                                  </div>
                                </div>
                            </div>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-md-3 form-control-label">ลักษณะการใช้งาน: </label>
                          <div class="col-md-9">
                            <ul class="list-unstyled list-inline mt-10">
                              <li class="float-left  mb-5">
                                <input type="radio" class="icheckbox-orange" id="inputRadios1" name="inputRadios1"
                                data-plugin="iCheck" data-radio-class="iradio_flat-blue" checked />
                                <label class="ml-5 mr-30" for="inputRadios1">ไปรับ-ส่ง</label>
                              </li>
                              <li class="float-left mb-5">
                                <input type="radio" class="icheckbox-orange" id="inputRadios2" name="inputRadios1"
                                data-plugin="iCheck" data-radio-class="iradio_flat-blue"/>
                                <label class="ml-5 mr-30" for="inputRadios2">ไปรับอย่างเดียว</label>
                              </li>
                              <li class="float-left mb-5">
                                <input type="radio" class="icheckbox-orange" id="inputRadios3" name="inputRadios1"
                                data-plugin="iCheck" data-radio-class="iradio_flat-blue"/>
                                <label class="ml-5 mr-30" for="inputRadios3">ไปส่งอย่างเดียว</label>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-md-3 form-control-label">จำนวนผู้โดยสาร: </label>
                          <div class="col-md-9">
                            <div class="bootstrap-select">
                              <select data-plugin="selectpicker">
                                <option value="">1</option>
                                <option value="AF">2</option>
                                <option value="AF">3</option>
                                <option value="AF">4</option>
                                <option value="AF">3-5</option>
                                <option value="AF">5-12</option>
                                <option value="AF">12+</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-md-3 form-control-label">สถานที่จะไป: </label>
                          <div class="col-md-9">
                            <textarea class="form-control" rows="3" placeholder=""></textarea>
                            <div class="pt-20" style="display: none;">
                              <script async src="//jsfiddle.net/1p4hsq5e/10/embed/result/"></script>
                            </div>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-md-3 form-control-label">จุดขึ้นรถ/รวมพล: </label>
                          <div class="col-md-9">
                            <textarea class="form-control" rows="3" placeholder=""></textarea>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-md-3 form-control-label">สัมภาระ/สิ่งของ: </label>
                          <div class="col-md-9">
                            <textarea class="form-control" rows="3" placeholder=""></textarea>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-md-3 form-control-label">ข้อมูลเพิ่มเติม: </label>
                          <div class="col-md-9">
                            <textarea class="form-control" rows="3" placeholder=""></textarea>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-md-3 form-control-label">ผู้ติดต่อ: </label>
                          <div class="col-md-9">
                            <input class="form-control" type="text" placeholder="">
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-md-9 offset-md-3">
                            <button type="button" class="btn btn-primary waves-effect waves-classic">Submit </button>
                            <button type="reset" class="btn btn-warning waves-effect waves-classic">Reset</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
        
        </div>
          


        </div>
      </div>
    </div>
  </div>
  
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/imagesloaded/imagesloaded.pkgd.js"></script>
  <script src="../../global/vendor/summernote/summernote.min.js"></script>
  <script src="../../global/vendor/blueimp-canvas-to-blob/canvas-to-blob.js"></script>
  <script src="../../global/vendor/blueimp-load-image/load-image.all.min.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-process.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-image.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-audio.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-video.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-validate.js"></script>
  <script src="../../global/vendor/blueimp-file-upload/jquery.fileupload-ui.js"></script>
  <script src="../../global/vendor/dropify/dropify.min.js"></script>
  <script src="../../global/vendor/icheck/icheck.min.js"></script>
  <script src="../../global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
  <script src="../../global/vendor/timepicker/jquery.timepicker.min.js"></script>
  <script src="../../global/vendor/datepair/datepair.min.js"></script>
  <script src="../../global/vendor/datepair/jquery.datepair.min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/icheck.js"></script>
  <script src="../../global/js/Plugin/dropify.js"></script>
  <script src="../../assets/examples/js/forms/uploads.js"></script>
  <script src="../../global/js/Plugin/bootstrap-datepicker.js"></script>
  <script src="../../global/js/Plugin/jt-timepicker.js"></script>
  <script src="../../global/js/Plugin/datepair.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDIJ9XX2ZvRKCJcFRrl-lRanEtFUow4piM&amp;callback=initMap"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
      
      $('#selectCar').select2({
        templateResult: formatCarSelect,
        dropdownParent: $("#bookingcar")
      });
    });

  })(document, window, jQuery);
  </script>
  <script>
    // initialize input widgets first
    $('#frm_datepair .time').timepicker({
        'showDuration': true,
        'timeFormat': 'H:i'
    });

    $('#frm_datepair .date').datepicker({
        'format': 'm/d/yyyy',
        'autoclose': true
    });

    // initialize datepair
    var basicExampleEl = document.getElementById('frm_datepair');
    var datepair = new Datepair(basicExampleEl);
</script>
<script>
    unction formatCarSelect (state) {
    if (!state.id) {
      //return state.text;
    }
    
    var $state = $(
      '<img src="../../global/portraits/'+Math.floor((Math.random() * 20) + 1)+'.jpg" class="avatar mr-10 float-left" />'
      +' <div class="clearfix text-primary">' + state.text + '<div class="font-size-12 grey-600">'+state.text.toLowerCase()+'@jigsawoffice.com</div></div>'
    );
    return $state;
  };
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>