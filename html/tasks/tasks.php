<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../assets/examples/css/advanced/masonry.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../global/vendor/jquery-selective/jquery-selective.css">
  <link rel="stylesheet" href="../../global/vendor/aspieprogress/asPieProgress.css">
  <link rel="stylesheet" href="../../assets/examples/css/charts/pie-progress.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
  <style type="text/css">
    .task-subject{
      float: left;
      width: 90%;
    }
    .task-content{
      float: right;
      width: 10%;
    }
    .sub-task .task-subject{
      float: left;
      width: 85%;
    }
    .sub-task .task-content{
      float: right;
      width: 15%;
    }
    .task-content .assign{
      float: left;
      padding-top:10px;
    }
    .task-content .task-icon{
      float: right;
      padding-top:10px;
    }
    .success .task-subject, .success .task-subject .grey-900, .success .task-content, .success .sub-task .task-subject, .success .sub-task .task-subject .grey-900,{
      color: #bdbdbd!important;
    }
    .success .task-subject, .success .sub-task .task-subject{
      text-decoration:line-through;
    }
    @media (max-width: 1024px) {
      .task-subject{
        width: 65%;
      }
      .task-content{
        width: 35%;
      }
    }
    @media (max-width: 980px) {
      .task-subject{
        width: 100%;
        float: none;
      }
      .task-content{
        width: 100%;
        float: none;
      }
      .sub-task .task-content .assign{
        float: none;
        text-align: center;
        padding-top:0px;
      }
      .sub-task .task-content .task-icon{
        float: none;
        text-align: center;
        padding-top:0px;
      }
    }
  </style>
</head>
<body class="animsition page-aside-right">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-10">Tasks</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item active">My Space</li>
      </ol>
<?php include("../my-space/mini-nav.php");?>
    </div>


    <div class="page-content container-fluid " style="position: relative;">
      <div class="row ml-0 mr-0">
        <div class="col-md-12 ">
          <div class="page-aside">
            <!-- Contacts Sidebar -->
            <div class="page-aside-switch">
              <i class="icon md-chevron-left" aria-hidden="true"></i>
              <i class="icon md-chevron-right" aria-hidden="true"></i>
            </div>
            <div class="page-aside-inner page-aside-scroll">
              <div data-role="container">
                <div data-role="content">
                  <section class="page-aside-section">

                    <div class="card card-block pt-0" style="min-height: 250px;">
                      <h3 class="mt-0">Summary All Task</h3>
                        <div class="pt-20 pb-20">
                          <div class="pie-progress pie-progress" data-plugin="pieProgress" data-barcolor="#4caf50"
                          data-goal="72" aria-valuenow="72" data-size="100" data-barsize="8"
                          role="progressbar">
                            <div class="pie-progress-content">
                              <div class="pie-progress-number">72 %</div>
                              <div class="pie-progress-label">Progress</div>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="card card-block">
                      <h3 class="mt-0">Tasks in Progress</h3>
                      <div class="counter counter-md text-left">
                        <div class="contextual-progress mb-0 mt-10">
                          <div class="clearfix">
                            <div class="progress-title">Order due</div>
                            <div class="progress-label">2 / 5</div>
                          </div>
                          <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                            <div class="progress-bar progress-bar-danger" aria-valuemin="0" aria-valuemax="100" style="width: 40%;" aria-valuenow="60" role="progressbar">
                            </div>
                          </div>
                        </div>

                      </div>
                      <div class="counter counter-md text-left">
                        <div class="contextual-progress mb-0 mt-10">
                          <div class="clearfix">
                            <div class="progress-title">Today</div>
                            <div class="progress-label">5 / 8</div>
                          </div>
                          <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                            <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 60%;" aria-valuenow="60" role="progressbar">
                            </div>
                          </div>
                        </div>

                      </div>
                      <div class="counter counter-md text-left">
                        <div class="contextual-progress mb-0 mt-10">
                          <div class="clearfix">
                            <div class="progress-title">This week</div>
                            <div class="progress-label">14 / 41</div>
                          </div>
                          <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                            <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 34%;" aria-valuenow="60" role="progressbar">
                            </div>
                          </div>
                        </div>

                      </div>
                      <div class="counter counter-md text-left">
                        <div class="contextual-progress mb-0 mt-10">
                          <div class="clearfix">
                            <div class="progress-title">This month</div>
                            <div class="progress-label">45 / 122</div>
                          </div>
                          <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                            <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 36%;" aria-valuenow="60" role="progressbar">
                            </div>
                          </div>
                        </div>

                      </div>

                    </div>



                  </section>
                </div>
              </div>
            </div>
          </div>
<div class="page-main">
          <section>
            <div class="pb-10">
              <div class="search-wrapper">
                <div class="search-box">
                  <div class="icon md-search"></div>
                  <div class="currently-showing">
                    <?=$search['tokenhtml']?>
                  </div>
                </div>
                <a href="javascript:void(0);" class="clear_all btn btn-flat btn-default btn-block waves-effect waves-classic">Clear All</a>
                <div class="data_entry">
                  <input class="input keyword-input" placeholder="Enter a keyword" type="text">
                  <div class="icon md-close-circle close"></div>
                </div>
                <div class="filters">
                    <div class="dropdown filter_permission">
                      <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Filter
                        <span class="icon md-chevron-down" aria-hidden="true"></span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)">Assign to me</a>
                        <a class="dropdown-item" href="javascript:void(0)">Assign to ...</a>
                      </div>
                    </div>
                    <div class="dropdown filter_permission">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      Priority
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu" role="menu">
                      <a class="dropdown-item text-danger" href="javascript:void(0)"><i class="icon fa-circle mr-5" aria-hidden="true"></i>High</a>
                      <a class="dropdown-item text-warning" href="javascript:void(0)"><i class="icon fa-circle mr-5" aria-hidden="true"></i>Normal</a>
                      <a class="dropdown-item text-info" href="javascript:void(0)"><i class="icon fa-circle mr-5" aria-hidden="true"></i>Low</a>
                    </div>
                  </div>
                    <div class="dropdown filter_permission">
                      <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Status
                        <span class="icon md-chevron-down" aria-hidden="true"></span>
                      </button>
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="javascript:void(0)">Overdue</a>
                        <a class="dropdown-item" href="javascript:void(0)">Progress</a>
                        <a class="dropdown-item" href="javascript:void(0)">Complete</a>
                        <a class="dropdown-item" href="javascript:void(0)">Hold</a>
                      </div>
                    </div>

                    <div class="dropdown filter_permission">
                      <button type="button" id="SearchDateRange" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Time <i class="icon md-chevron-down" aria-hidden="true"></i>
                        <span id="daterange-value"></span>
                      </button>
                    </div>

                </div>
              </div>
            </div>
            <div class="pb-20">
                <div class="actions-inner float-right">
                  <div class="dropdown">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      <i class="icon md-view-module" aria-hidden="true"></i> <i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Due date
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-media w-100" role="menu">
                      <h6 class="pl-10" aria-hidden="false">View</h6>
                      <a class="dropdown-item" href="tasks-list.php"><i class="icon md-view-list" aria-hidden="true"></i> List</a>
                      <a class="dropdown-item active" href="tasks.php"><i class="icon md-view-module" aria-hidden="true"></i> Grid</a>
                      <div class="dropdown-divider"></div>
                      <h6 class="pl-10" aria-hidden="false">Sory by</h6>
                      <a class="dropdown-item active" href="javascript:void(0)">Due date</a>
                      <a class="dropdown-item" href="javascript:void(0)">Start date</a>
                      <a class="dropdown-item" href="javascript:void(0)">Activity</a>
                      <a class="dropdown-item" href="javascript:void(0)">Subject</a>
                      <a class="dropdown-item" href="javascript:void(0)">Status</a>
                      <a class="dropdown-item" href="javascript:void(0)">Priority</a>
                      <a class="dropdown-item" href="javascript:void(0)">Attachments</a>
                      <a class="dropdown-item" href="javascript:void(0)">Comment</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-sort-amount-asc" aria-hidden="true"></i> Ascending</a>
                      <a class="dropdown-item active" href="javascript:void(0)"><i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Descending</a>
                    </div>
                  </div>
                </div>


                <div class="pt-10">About <strong>26</strong> results</div>
              </div>
            </section>
          <section>

            <ul class="blocks blocks-100 blocks-xxl-4 blocks-lg-4 blocks-md-3 blocks-sm-2" data-plugin="masonry">
              <?php
              $Mtask[] = "Update User profile page Update User profile page";
              $Mtask[] = "Add images to the product gallery";
              $Mtask[] = "Create invoice template";
              $Mtask[] = "Google AdWords campain graphics Google AdWords campain graphics";
              $Mtask[] = "Create application Photoshop draft";
              $Mtask[] = "Create landing page for a new app page for a new apppage for a new app";
              $Mtask[] = "Merge latest changes Merge latest changes";
              $Mtask[] = "Add updated responsive styles";
              $Mtask[] = "Fix website issues on mobile Fix website issues on mobile Fix website issues on mobile";
              $Mtask[] = "iOS application design mockups";
              $Mtask[] = "Help Web devs with HTML integration";

              $Color[] = "danger";
              $Color[] = "warning";
              $Color[] = "success";
              $Color[] = "default";
              for($a=1;$a<25;$a++){
              ?>
              <li class="masonry-item">
                <div class="alert alert-alt alert-<?=$Color[rand(0,3)]?> bg-white p-10 clearfix">
                  <div class="media" style="flex-direction:initial;">
                    <div class="pr-20">
                      <div class="checkbox-custom checkbox-success">
                        <input name="inputCheckboxes" type="checkbox"><label></label>
                      </div>
                    </div>
                    <div class="media-body">
                      <div class="task-subject" style="float:none; width:100%;">
                        <div class="collapsed font-size-12">
                          15/05/2017<span class="ml-10 mr-10">&mdash;</span>Due in <?=rand(2,5)?> days
                        </div>
                        <div class="collapsed">
                          <a href="javascript:void(0)" class="grey-900" data-url="../workspace/panel-task.php" data-toggle="slidePanel" >
                          <strong class=""><?=$Mtask[rand(0,10)]?></strong>
                          </a>
                        </div>
                      </div>
                      <div class="task-content" style="float:none; width:100%;">
                        <div class="assign">
                          <ul class="addMember-items pl-0">
                            <li class="addMember-item mr-5 p-0">
                              <img style="width:20px;" class="avatar" src="../../global/portraits/<?php echo rand(1,20);?>.jpg" title="Herman Beck">
                            </li>
                          </ul>
                        </div>
                        <div class="task-icon">
                          <ul class="list-inline float-right mb-0 pl-0">
                            <li class="list-inline-item mr-10 p-0">
                              <i class="icon fa-comments-o"></i> <?=rand(1,10)?>
                            </li>
                            <li class="list-inline-item mr-10 p-0">
                              <i class="icon md-attachment-alt"></i> <?=rand(2,5)?>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  </div>
                </li>
               <?php }?>


              <?php
               for($a=1;$a<5;$a++){
              ?>
              <li  class="masonry-item">
                <div class="success alert alert-alt alert-<?=$Color[rand(0,3)]?> bg-green-100 p-10 clearfix">
                  <div class="media" style="flex-direction:initial;">
                    <div class="pr-20">
                      <div class="checkbox-custom checkbox-success">
                        <input name="inputCheckboxes" checked="" type="checkbox"><label></label>
                      </div>
                    </div>
                    <div class="media-body">
                      <div class="task-subject" style="float:none; width:100%;">
                        <div class="collapsed font-size-12">
                          15/05/2017<span class="ml-10 mr-10">&mdash;</span>Due in <?=rand(2,5)?> days
                        </div>
                        <div class="collapsed">
                          <a href="javascript:void(0)" class="grey-900" data-url="../workspace/panel-task.php" data-toggle="slidePanel" >
                          <strong class=""><?=$Mtask[rand(0,10)]?></strong>
                          </a>
                        </div>
                      </div>
                      <div class="task-content" style="float:none; width:100%;">
                        <div class="assign">
                          <ul class="addMember-items pl-0">
                            <li class="addMember-item mr-5 p-0">
                              <img style="width:20px;" class="avatar" src="../../global/portraits/<?php echo rand(1,20);?>.jpg" title="Herman Beck">
                            </li>
                          </ul>
                        </div>
                        <div class="task-icon">
                          <ul class="list-inline float-right mb-0 pl-0">
                            <li class="list-inline-item mr-10 p-0">
                              <i class="icon fa-comments-o"></i> <?=rand(1,10)?>
                            </li>
                            <li class="list-inline-item mr-10 p-0">
                              <i class="icon md-attachment-alt"></i> <?=rand(2,5)?>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  </div>
                </li>
               <?php }?>

            </ul>

          </section>
            </div>
        </div>
      </div>
    </div>
  </div>
  <div class="site-action" data-plugin="actionBtn">
    <button type="button" data-target="#taskForm" data-toggle="modal" class="btn-raised btn btn-success btn-floating">
      <i class="front-icon md-plus" aria-hidden="true"></i>
    </button>
  </div>
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/stickyfill/stickyfill.min.js"></script>
  <script src="../../global/vendor/masonry/masonry.pkgd.js"></script>
  <script src="../../global/vendor/aspieprogress/jquery-asPieProgress.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/masonry.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../global/js/Plugin/aspieprogress.js"></script>
  <script src="../../assets/examples/js/charts/pie-progress.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();


      function chooseDateRange(start, end) {
          var DrangeValue = start.format('MM/DD/YYYY') + '-' + end.format('MM/DD/YYYY');
          $('#SearchDateRange span#daterange-value').html(DrangeValue);
          var dataKey = "123";
          var dataValue = "xxx";
          var stoken = '';
            $('.search-wrapper .currently-showing .chooseDateRange').remove();
            stoken = '<div class="token chooseDateRange" data-key="'+dataKey+'" data-value="' + dataValue + '"><span class="token-label" >' + DrangeValue + '</span><a  href="#" class="close" tabindex="-1">×</a></div>';
            $('.search-wrapper .currently-showing').append(stoken);
            sysGenUrlSearch();
      }

      $('#SearchDateRange').daterangepicker({
          "autoApply": true,
          "opens": "center",
          ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'This week': [moment().startOf('week'), moment().endOf('week')],
             'Next week': [moment().add(1, 'weeks').startOf('week'), moment().add(1, 'weeks').endOf('week')],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Next Month': [moment().add(1, 'month').startOf('month'), moment().add(1, 'month').endOf('month')]
          }
      }, chooseDateRange);

    });
  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>
