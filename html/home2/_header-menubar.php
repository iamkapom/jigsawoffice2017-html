<div class="site-menubar site-menubar-dark">
  <div class="site-menubar-body">
    <ul class="site-menu" data-plugin="menu">
      <li class="site-menu-item">
        <a class="animsition-link" href="../home/">
          <i class="site-menu-icon md-widgets" aria-hidden="true"></i>
          <span class="site-menu-title">Home</span>
        </a>
      </li>
      <li class="site-menu-item">
        <a href="../messenger/messenger.php" dropdown-badge="false">
          <i class="site-menu-icon md-comments" aria-hidden="true"></i>
          <span class="site-menu-title">Messenger</span>
        </a>
      </li>
    </ul>
    
  </div>
</div>