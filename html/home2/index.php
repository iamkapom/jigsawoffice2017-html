<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.min.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/plyr/plyr.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../global/vendor/slick-carousel/slick.css">
  <link rel="stylesheet" href="../../global/vendor/chartist/chartist.css">
  <link rel="stylesheet" href="../../global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
  <link rel="stylesheet" href="../../assets/examples/css/pages/profile_v3.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.css">
  <link rel="stylesheet" href="../../global/vendor/aspieprogress/asPieProgress.css">
  <link rel="stylesheet" href="../../assets/examples/css/charts/pie-progress.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500">
  
  <!--[if lt IE 9]>
    <script src="../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
</head>
<body class="animsition">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("_header.php");?>
  <?php include("_header-menubar.php");?>
  <!-- Page -->
  <div class="page no-headnav">
    <div class="page-header">
      <h1 class="page-title mb-10">Home <button class="btn btn-icon btn-warning btn-round waves-effect waves-classic btn-xs" data-plugin="toolbar" data-toolbar="#set-04-options" data-toolbar-style="info" data-toolbar-position="right" type="button"><i class="icon md-star m-0" aria-hidden="true"></i></button></h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
      </ol>
    </div>
    <div class="page-content">
      <div class="row ml-0 mr-0">
        <div class="col-md-7">
          <section>
            <div class="card card-shadow wall-posting">
              <div class="card-block post-display" data-target="#postFormQuick" data-toggle="modal">
                <div class="avatar avatar-lg">
                    <img src="../../global/portraits/6.jpg" alt="">
                </div>
                <div class="grey-400 post-area">
                  Whats in your mind today?
                  <div class="post-attach">
                    <button class="btn btn-icon btn-dark btn-round bg-grey-400">
                      <i class="icon md-attachment-alt rotate" aria-hidden="true"></i>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section>
            <div class="pb-10">
              <div class="search-wrapper">
                <div class="search-box">
                  <div class="icon md-search"></div>
                  <div class="currently-showing">
                    <?=$search['tokenhtml']?>
                  </div>
                </div>
                <a href="javascript:void(0);" class="clear_all btn btn-flat btn-default btn-block waves-effect waves-classic">Clear All</a>
                <div class="data_entry">
                  <input class="input keyword-input" placeholder="Enter a keyword" type="text">
                  <div class="icon md-close-circle close"></div>
                </div>
                <div class="filters">
                    <div class="dropdown filter_permission">
                      <button type="button" id="SearchDateRange" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                        Time <i class="icon md-chevron-down" aria-hidden="true"></i>
                        <span id="daterange-value"></span>
                      </button>
                    </div>
                    
                </div>
              </div>
            </div>
            <div class="pb-20">
                <div class="actions-inner float-right">
                  <div class="dropdown">
                    <button type="button" class="btn btn-pure waves-effect waves-classic" data-toggle="dropdown" aria-expanded="false">
                      <i class="icon fa-sort-amount-desc" aria-hidden="true"></i> New Post
                      <span class="icon md-chevron-down" aria-hidden="true"></span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-media w-100" role="menu">
                      <h6 class="pl-10" aria-hidden="false">Sory by</h6>
                      <a class="dropdown-item active" href="javascript:void(0)">New Post</a>
                      <a class="dropdown-item" href="javascript:void(0)">Recent Activity</a>
                      <a class="dropdown-item" href="javascript:void(0)">Top Comments</a>
                      <a class="dropdown-item" href="javascript:void(0)">Top Likes</a>
                      <a class="dropdown-item" href="javascript:void(0)">Attach File</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="javascript:void(0)"><i class="icon fa-sort-amount-asc" aria-hidden="true"></i> Ascending</a>
                      <a class="dropdown-item active" href="javascript:void(0)"><i class="icon fa-sort-amount-desc" aria-hidden="true"></i> Descending</a>
                    </div>
                  </div>
                </div>
                

                <div class="pt-10">About <strong>26</strong> results</div>
              </div>
            </section>

          <div class="">
            <!-- Start Post Section -->
            <div >Text Post</div>
            <div class="card card-shadow card-bordered post-wall">
              <div class="card-block clearfix p-25">
                <div class="media-avatar">
                  <a href="#" class="avatar avatar-lg">
                    <img class="img-fluid" src="../../../global/portraits/6.jpg">
                  </a>
                </div>
                <div class="media-body text-middle">
                  <h5 class="mt-0 mb-0">
                    <a href="#">Mallinda Hollaway</a> to <a href="#">JigsawOffice2017</a> 
                    
                  </h5>
                  <small>30th July 2017</small>
                </div>

                <div class="wall-action">
                  <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                    <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                  </button>
                  
                  <div class="dropdown-menu dropdown-menu-right" role="menu">
                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                      Edit Post
                    </a>
                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                      Bookmark
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item text-danger" href="javascript:void(0)" role="menuitem">
                      Delete
                    </a>
                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                      Report Post
                    </a>
                  </div>
                </div>
              </div>
              <div class="card-block px-25 pt-0 pb-0">
                <p class="card-text mb-20 grey-800">
                  Ex quis excepteur exercitation incididunt ea amet commodo deserunt nulla. Anim
                  sit duis cupidatat ipsum deserunt reprehenderit fugiat cupidatat.
                  In do eu sint Lorem qui eu eu. Id ad non pariatur culpa. Duis proident
                  cupidatat laborum pariatur sit eu eiusmod. Cillum consectetur exercitation
                  ex ipsum. Ullamco commodo anim ut aliqua ex incididunt commodo
                  incididunt reprehenderit. Sit nisi deserunt fugiat eu qui nisi
                  nulla.
                </p>
                
                <ul class="wall-attrs clearfix p-0 m-0 mt-10">
                  <li class="attrs-meta float-left font-size-16">
                    <a href="javascript:void(0)" class="mr-20">
                      <i class="icon md-thumb-up"></i>
                      <span>5</span>
                    </a>
                    <a href="javascript:void(0)" onClick="$('#wall-comment-reply-1').toggle();">
                      <i class="icon md-comment"></i>
                      <span>2</span>
                    </a>
                  </li>
                  <li class="float-right">
                    <div class="btn-group bootstrap-select btn-comment-post">
                      <select data-plugin="selectpicker">
                        <option>Top Comments</option>
                        <option>Most Recent</option>
                      </select>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="wall-comment-attrs">
                <div id="wall-comment-reply-1" class="wall-comment-reply clearfix" style="display: none">
                  <div href="#" class="avatar avatar-md float-left">
                    <img src="../../../global/portraits/6.jpg">
                  </div>
                  <div class="ml-60 wall-comment-form">
                    <textarea rows="1" class="autoHeight" placeholder="Write a comment..."></textarea>
                    <div class="wall-comment-reply-attach">
                      <button class="btn btn-icon btn-dark btn-round bg-grey-400">
                        <i class="icon md-attachment-alt rotate" aria-hidden="true"></i>
                      </button>
                    </div>
                  </div>
                </div>
                <div class="wall-comment">
                  <div class="wall-comment-action">
                  <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                    <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                  </button>
                  
                  <div class="dropdown-menu dropdown-menu-right" role="menu">
                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                      Edit Comment
                    </a>
                    <a class="dropdown-item text-danger" href="javascript:void(0)" role="menuitem">
                      Delete
                    </a>
                  </div>
                </div>
                  <a href="#" class="avatar avatar-md float-left">
                    <img src="../../../global/portraits/3.jpg">
                  </a>
                  <div class="ml-60 box-post-comment">
                    <a href="#">Stacey Hunt</a>
                    <span class="ml-10">Do incididunt elit ex incididunt ut. Aute velit proident cupidatat qui et consectetur
                      do. Consequat esse quis ut cupidatat ea. Sint dolore ea culpa
                      dolore velit enim.</span>
                      <p class="font-size-12 mt-5 grey-600">
                        <a href="javascript:void(0)">
                          Like
                        </a>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <a href="javascript:void(0)" class="action-reply">
                          Reply
                        </a>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <span >
                          <i class="icon md-favorite"></i>
                          <span>5</span>
                        </span>
                        <span class="mr-5 ml-5">&#8226;</span>
                        <span>
                          30th July 2017
                        </span>
                      </p>

                    <div class="wall-comment">
                      <a href="#" class="avatar avatar-md float-left">
                        <img src="../../../global/portraits/11.jpg">
                      </a>
                      <div class="ml-60">
                        <a href="#">Crystal Bates</a>
                        <span class="ml-10">Do incididunt elit ex incididunt ut. Aute velit proident cupidatat qui et consectetur
                          do. Consequat esse quis ut cupidatat ea.</span>
                        <p class="font-size-12 mt-5 grey-500">
                          <a href="javascript:void(0)">
                            Like
                          </a>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <a href="javascript:void(0)" class="action-reply">
                            Reply
                          </a>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <span >
                            <i class="icon md-favorite"></i>
                            <span>5</span>
                          </span>
                          <span class="mr-5 ml-5">&#8226;</span>
                          <span>
                            30th July 2017
                          </span>
                        </p>
                      </div>
                      
                    </div>
                    <div class="display-reply"></div>

                  </div>
                  
                </div>
                <div class="wall-comment">
                  <a href="#" class="avatar avatar-md float-left">
                    <img src="../../../global/portraits/5.jpg">
                  </a>
                  <div class="ml-60 box-post-comment">
                    <a href="#">Sam Anderson</a>
                    <span class="ml-10">Ut velit ipsum elit ut mollit elit. Proident officia eu ex consectetur ullamco
                      magna. Enim cillum voluptate sint ipsum ad voluptate exercitation.
                      Ex est amet magna occaecat eu.</span>
                    <p class="font-size-12 mt-5 grey-500">
                      <a href="javascript:void(0)">
                        Like
                      </a>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <a href="javascript:void(0)" class="action-reply">
                        Reply
                      </a>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <span >
                        <i class="icon md-favorite"></i>
                        <span>5</span>
                      </span>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <span>
                        30th July 2017
                      </span>
                    </p>

                    <div class="display-reply"></div>
                  </div>
                  
                </div>
              </div>
            </div>
            <!-- End Post Section-->

            <!-- Start Post Section -->
            <div class="pt-10">Text Auto Post Weekly Report</div>
            <div class="card card-shadow card-bordered post-wall">
              <div class="card-block media clearfix p-25">
                <div class="media-avatar">
                  <a href="#" class="avatar avatar-lg">
                    <img class="img-fluid" src="../../../global/portraits/6.jpg">
                  </a>
                </div>
                <div class="media-body text-middle">
                  <h5 class="mt-0 mb-0">
                    <a href="#">Mallinda Hollaway</a> to <a href="#">JigsawOffice2017</a> 
                    <?php if(rand(0,2)){?>
                    <span class="badge badge-dark">External</span>
                    <?php }?>
                  </h5>
                  <small>30th July 2017</small>
                </div>

                <div class="wall-action">
                  <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                    <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                  </button>
                  
                  <div class="dropdown-menu dropdown-menu-right" role="menu">
                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                      Bookmark
                    </a>
                  </div>
                </div>
              </div>
              <div class="card-block px-25 pt-0 pb-0">
                <p class="card-text mb-20 text-center text-danger font-size-14">
                  <i class="icon fa-line-chart " style="font-size:50px;" aria-hidden="true"></i><br>
                  Auto Post "<strong>JigsawOffice2017</strong>" Weekly report on <strong>July 30, 2017</strong>.
                </p>
                
                <div class="wall-attach">
                  <div class="row ml-0 mr-0">
                    <div class="col-md-6">
                      <!-- Card -->
                      <div class="card p-20">
                        <h3 class="mt-0">Summary All Task</h3>
                        <div class="pt-20 pb-20">
                          <div class="pie-progress pie-progress" data-plugin="pieProgress" data-barcolor="#4caf50"
                          data-goal="72" aria-valuenow="72" data-size="100" data-barsize="8"
                          role="progressbar">
                            <div class="pie-progress-content">
                              <div class="pie-progress-number">72 %</div>
                              <div class="pie-progress-label">Progress</div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- End Card -->
                    </div>
                    
                    <div class="col-md-6">
                      <!-- Card -->
                      <div class="card p-20">
                        <h3 class="mt-0">Tasks in Progress</h3>
                        <div class="counter counter-md text-left">
                          <div class="contextual-progress">
                            <div class="clearfix">
                              <div class="progress-title">Order due</div>
                              <div class="progress-label">2 / 5</div>
                            </div>
                            <div class="progress" style="height:15px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                              <div class="progress-bar progress-bar-danger" aria-valuemin="0" aria-valuemax="100" style="width: 40%;" aria-valuenow="60" role="progressbar">
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="counter counter-md text-left">
                          <div class="contextual-progress">
                            <div class="clearfix">
                              <div class="progress-title">This week</div>
                              <div class="progress-label">14 / 25</div>
                            </div>
                            <div class="progress" style="height:15px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                              <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 56%;" aria-valuenow="60" role="progressbar">
                              </div>
                            </div>
                          </div>
                          
                        </div>
                      </div>
                      <!-- End Card -->
                    </div>
                    
                  </div>
                  
                </div>

                <ul class="wall-attrs clearfix p-0 m-0 mt-10">
                  <li class="attrs-meta float-left font-size-16">
                    <a href="javascript:void(0)" class="mr-20">
                      <i class="icon md-thumb-up"></i>
                      <span>5</span>
                    </a>
                    <a href="javascript:void(0)" onClick="$('#wall-comment-reply-2').toggle();">
                      <i class="icon md-comment"></i>
                      <span>2</span>
                    </a>
                  </li>
                  <li class="float-right">
                    <div class="btn-group bootstrap-select btn-comment-post">
                      <select data-plugin="selectpicker">
                        <option>Top Comments</option>
                        <option>Most Recent</option>
                      </select>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="wall-comment-attrs">
                <div id="wall-comment-reply-2" class="wall-comment-reply clearfix" style="display:none;">
                  <div href="#" class="avatar avatar-md float-left">
                    <img src="../../../global/portraits/6.jpg">
                  </div>
                  <div class="ml-60 wall-comment-form">
                    <textarea rows="1" class="autoHeight" placeholder="Write a comment..."></textarea>
                    <div class="wall-comment-reply-attach">
                      <button class="btn btn-icon btn-dark btn-round bg-grey-400">
                        <i class="icon md-attachment-alt rotate" aria-hidden="true"></i>
                      </button>
                    </div>
                  </div>
                </div>
                <div class="wall-comment">
                  <a href="#" class="avatar avatar-md float-left">
                    <img src="../../../global/portraits/5.jpg">
                  </a>
                  <div class="ml-60 box-post-comment">
                    <a href="#">Sam Anderson</a>
                    <span class="ml-10">Ut velit ipsum elit ut mollit elit. Proident officia eu ex.</span>
                    <p class="font-size-12 mt-5 grey-500">
                      <a href="javascript:void(0)">
                        Like
                      </a>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <a href="javascript:void(0)" class="action-reply">
                        Reply
                      </a>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <span >
                        <i class="icon md-favorite"></i>
                        <span>5</span>
                      </span>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <span>
                        30th July 2017
                      </span>
                    </p>

                    <div class="display-reply"></div>
                  </div>
                  
                </div>
              </div>
            </div>
            <!-- End Post Section-->

            <!-- Start Post Section -->
            <div class="pt-10">Text Post+Photo</div>
            <div class="card card-shadow card-bordered post-wall">
              <div class="card-block media clearfix p-25">
                <div class="media-avatar">
                  <a href="#" class="avatar avatar-lg">
                    <img class="img-fluid" src="../../../global/portraits/6.jpg">
                  </a>
                </div>
                <div class="media-body text-middle">
                  <h5 class="mt-0 mb-0">
                    <a href="#">Mallinda Hollaway</a> to <a href="#">JigsawOffice2017</a> 
                    <?php if(rand(0,2)){?>
                    <span class="badge badge-dark">External</span>
                    <?php }?>
                  </h5>
                  <small>30th July 2017</small>
                </div>

                <div class="wall-action">
                  <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                    <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                  </button>
                  
                  <div class="dropdown-menu dropdown-menu-right" role="menu">
                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                      Edit Post
                    </a>
                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                      Bookmark
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item text-danger" href="javascript:void(0)" role="menuitem">
                      Delete
                    </a>
                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                      Report Post
                    </a>
                  </div>
                </div>
              </div>
              <div class="card-block px-25 pt-0 pb-0">
                <p class="card-text mb-20 grey-800">
                  Ex quis excepteur exercitation incididunt ea amet commodo deserunt nulla. Anim
                  sit duis cupidatat ipsum deserunt reprehenderit fugiat cupidatat.
                </p>
                <h5 class="mt-20"><a href="#">4 (1-3) Design master</a></h5>
                <div class="wall-attach">
                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-12">
                      <img width="100%" src="<?=get_img("Vx")?>" alt="...">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4 col-sm-4 col-4">
                      <img width="100%" src="<?=get_img("Vx")?>" alt="...">
                    </div>
                    <div class="col-md-4 col-sm-4 col-4">
                      <img width="100%" src="<?=get_img("Vx")?>" alt="...">
                    </div>
                    <div class="col-md-4 col-sm-4 col-4">
                      <img width="100%" src="<?=get_img("Vx")?>" alt="...">
                    </div>
                  </div>
                </div>

                <ul class="wall-attrs clearfix p-0 m-0 mt-10">
                  <li class="attrs-meta float-left font-size-16">
                    <a href="javascript:void(0)" class="mr-20">
                      <i class="icon md-thumb-up"></i>
                      <span>5</span>
                    </a>
                    <a href="javascript:void(0)" onClick="$('#wall-comment-reply-2').toggle();">
                      <i class="icon md-comment"></i>
                      <span>2</span>
                    </a>
                  </li>
                  <li class="float-right">
                    <div class="btn-group bootstrap-select btn-comment-post">
                      <select data-plugin="selectpicker">
                        <option>Top Comments</option>
                        <option>Most Recent</option>
                      </select>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="wall-comment-attrs">
                <div id="wall-comment-reply-2" class="wall-comment-reply clearfix" style="display:none;">
                  <div href="#" class="avatar avatar-md float-left">
                    <img src="../../../global/portraits/6.jpg">
                  </div>
                  <div class="ml-60 wall-comment-form">
                    <textarea rows="1" class="autoHeight" placeholder="Write a comment..."></textarea>
                    <div class="wall-comment-reply-attach">
                      <button class="btn btn-icon btn-dark btn-round bg-grey-400">
                        <i class="icon md-attachment-alt rotate" aria-hidden="true"></i>
                      </button>
                    </div>
                  </div>
                </div>
                <div class="wall-comment">
                  <a href="#" class="avatar avatar-md float-left">
                    <img src="../../../global/portraits/5.jpg">
                  </a>
                  <div class="ml-60 box-post-comment">
                    <a href="#">Sam Anderson</a>
                    <span class="ml-10">Ut velit ipsum elit ut mollit elit. Proident officia eu ex.</span>
                    <p class="font-size-12 mt-5 grey-500">
                      <a href="javascript:void(0)">
                        Like
                      </a>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <a href="javascript:void(0)" class="action-reply">
                        Reply
                      </a>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <span >
                        <i class="icon md-favorite"></i>
                        <span>5</span>
                      </span>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <span>
                        30th July 2017
                      </span>
                    </p>

                    <div class="display-reply"></div>
                  </div>
                  
                </div>
              </div>
            </div>
            <!-- End Post Section-->

            <!-- Start Post Section -->
            <div class="pt-10">Text Post+Video</div>
            <div class="card card-shadow card-bordered post-wall">
              <div class="card-block media clearfix p-25">
                <div class="media-avatar">
                  <a href="#" class="avatar avatar-lg">
                    <img class="img-fluid" src="../../../global/portraits/6.jpg">
                  </a>
                </div>
                <div class="media-body text-middle">
                  <h5 class="mt-0 mb-0">
                    <a href="#">Mallinda Hollaway</a> to <a href="#">JigsawOffice2017</a> 
                    <?php if(rand(0,2)){?>
                    <span class="badge badge-dark">External</span>
                    <?php }?>
                  </h5>
                  <small>30th July 2017</small>
                </div>

                <div class="wall-action">
                  <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                    <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                  </button>
                  
                  <div class="dropdown-menu dropdown-menu-right" role="menu">
                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                      Edit Post
                    </a>
                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                      Bookmark
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item text-danger" href="javascript:void(0)" role="menuitem">
                      Delete
                    </a>
                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                      Report Post
                    </a>
                  </div>
                </div>
              </div>
              <div class="card-block px-25 pt-0 pb-0">
                <p class="card-text mb-20 grey-800">
                  Ex quis excepteur exercitation incididunt ea amet commodo deserunt nulla. Anim
                  sit duis cupidatat ipsum deserunt reprehenderit fugiat cupidatat.
                </p>

                <div class="wall-attach">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="card-header cover player p-0" data-plugin="plyr">
                        <video poster="../../assets/examples/images/poster.jpg" controls>
                          <!-- Video Files -->
                          <source type="video/mp4" src="https://www.w3schools.com/html/movie.mp4">
                          <source type="video/webm" src="http://video.webmfiles.org/big-buck-bunny_trailer.webm">
                            <!-- Fallback For Browsers That Don'T Support The <Video> Element -->
                            <a href="https://www.w3schools.com/html/movie.mp4">Download</a>
                        </video>
                      </div>
                    </div>
                  </div>
                  
                </div>

                <ul class="wall-attrs clearfix p-0 m-0 mt-10">
                  <li class="attrs-meta float-left font-size-16">
                    <a href="javascript:void(0)" class="mr-20">
                      <i class="icon md-thumb-up"></i>
                      <span>5</span>
                    </a>
                    <a href="javascript:void(0)" onClick="$('#wall-comment-reply-2').toggle();">
                      <i class="icon md-comment"></i>
                      <span>2</span>
                    </a>
                  </li>
                  <li class="float-right">
                    <div class="btn-group bootstrap-select btn-comment-post">
                      <select data-plugin="selectpicker">
                        <option>Top Comments</option>
                        <option>Most Recent</option>
                      </select>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="wall-comment-attrs">
                <div id="wall-comment-reply-2" class="wall-comment-reply clearfix" style="display:none;">
                  <div href="#" class="avatar avatar-md float-left">
                    <img src="../../../global/portraits/6.jpg">
                  </div>
                  <div class="ml-60 wall-comment-form">
                    <textarea rows="1" class="autoHeight" placeholder="Write a comment..."></textarea>
                    <div class="wall-comment-reply-attach">
                      <button class="btn btn-icon btn-dark btn-round bg-grey-400">
                        <i class="icon md-attachment-alt rotate" aria-hidden="true"></i>
                      </button>
                    </div>
                  </div>
                </div>
                <div class="wall-comment">
                  <a href="#" class="avatar avatar-md float-left">
                    <img src="../../../global/portraits/5.jpg">
                  </a>
                  <div class="ml-60 box-post-comment">
                    <a href="#">Sam Anderson</a>
                    <span class="ml-10">Ut velit ipsum elit ut mollit elit. Proident officia eu ex.</span>
                    <p class="font-size-12 mt-5 grey-500">
                      <a href="javascript:void(0)">
                        Like
                      </a>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <a href="javascript:void(0)" class="action-reply">
                        Reply
                      </a>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <span >
                        <i class="icon md-favorite"></i>
                        <span>5</span>
                      </span>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <span>
                        30th July 2017
                      </span>
                    </p>

                    <div class="display-reply"></div>
                  </div>
                  
                </div>
              </div>
            </div>
            <!-- End Post Section-->

            <!-- Start Post Section -->
            <div class="pt-10">Text Post+Document</div>
            <div class="card card-shadow card-bordered post-wall">
              <div class="card-block media clearfix p-25">
                <div class="media-avatar">
                  <a href="#" class="avatar avatar-lg">
                    <img class="img-fluid" src="../../../global/portraits/6.jpg">
                  </a>
                </div>
                <div class="media-body text-middle">
                  <h5 class="mt-0 mb-0">
                    <a href="#">Mallinda Hollaway</a> to <a href="#">JigsawOffice2017</a> 
                    <?php if(rand(0,2)){?>
                    <span class="badge badge-dark">External</span>
                    <?php }?>
                  </h5>
                  <small>30th July 2017</small>
                </div>

                <div class="wall-action">
                  <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                    <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                  </button>
                  
                  <div class="dropdown-menu dropdown-menu-right" role="menu">
                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                      Edit Post
                    </a>
                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                      Bookmark
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item text-danger" href="javascript:void(0)" role="menuitem">
                      Delete
                    </a>
                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                      Report Post
                    </a>
                  </div>
                </div>
              </div>
              <div class="card-block px-25 pt-0 pb-0">
                <p class="card-text mb-20 grey-800">
                  Ex quis excepteur exercitation incididunt ea amet commodo deserunt nulla. Anim
                  sit duis cupidatat ipsum deserunt reprehenderit fugiat cupidatat.
                </p>
                
                <div class="wall-attach">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="card card-shadow card-bordered mb-5">
                        <div class="bg-grey-100 pt-10 pb-10 clearfix text-center" style="padding-bottom: 0px;">
                          <a style="color:#f44336;" href="javascript:void(0)">
                            <i class="icon fa-file-pdf-o " style="font-size:34px;" aria-hidden="true"></i>
                          </a>
                          <div class="wall-action" style="top:0;">
                            <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                              <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" role="menu">
                              <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                                Download
                              </a>
                              <a class="dropdown-item" href="javascript:void(0)" role="menuitem" data-url="../drive/panel-file.html" data-toggle="slidePanel">
                                View detail
                              </a>
                            </div>
                          </div>
                        </div>
                        <div class="card-block" style="padding: 10px;">
                          <h5 class="m-0">
                            <a href="javascript:void(0)" style="color:#f44336;">Wireframe inner</a>
                          </h5>
                          <p class="mb-10 grey-500">
                            <small>
                              July 30, 2017 <span class="ml-10 mr-10">&mdash;</span> 4.25 MB</small>
                          </p>
                          <p class="mb-0 grey-500 font-size-12">
                            <a href="#">JigsawOffice2017</a>
                            <i class="icon md-chevron-right" aria-hidden="true"></i>
                            <a href="#">Drive</a>
                            <i class="icon md-chevron-right" aria-hidden="true"></i>
                            <a href="#">Design</a>
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="card card-shadow card-bordered mb-5">
                        <div class="bg-grey-100 pt-10 pb-10 clearfix text-center" style="padding-bottom: 0px;">
                          <a style="color:#4caf50;" href="javascript:void(0)">
                            <i class="icon fa-file-excel-o " style="font-size:34px;" aria-hidden="true"></i>
                          </a>
                          <div class="wall-action" style="top:0;">
                            <button type="button" class="btn btn-pure grey-500 font-size-18" data-toggle="dropdown" aria-hidden="true">
                              <i class="icon md-more-vert font-size-24" aria-hidden="true"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" role="menu">
                              <a class="dropdown-item" href="javascript:void(0)" role="menuitem">
                                Download
                              </a>
                              <a class="dropdown-item" href="javascript:void(0)" role="menuitem" data-url="../drive/panel-file.html" data-toggle="slidePanel">
                                View detail
                              </a>
                            </div>
                          </div>
                        </div>
                        <div class="card-block" style="padding: 10px;">
                          <h5 class="m-0 text-center">
                            <a href="javascript:void(0)" style="color:#4caf50;">TOR Checklist</a>
                          </h5>
                          <p class="mb-10 grey-500">
                            <small>
                              July 30, 2017 <span class="ml-10 mr-10">&mdash;</span> 0.51 MB</small>
                          </p>
                          <p class="mb-0 grey-500 font-size-12">
                            <a href="#">JigsawOffice2017</a>
                            <i class="icon md-chevron-right" aria-hidden="true"></i>
                            <a href="#">Drive</a>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                </div>

                <ul class="wall-attrs clearfix p-0 m-0 mt-10">
                  <li class="attrs-meta float-left font-size-16">
                    <a href="javascript:void(0)" class="mr-20">
                      <i class="icon md-thumb-up"></i>
                      <span>5</span>
                    </a>
                    <a href="javascript:void(0)" onClick="$('#wall-comment-reply-2').toggle();">
                      <i class="icon md-comment"></i>
                      <span>2</span>
                    </a>
                  </li>
                  <li class="float-right">
                    <div class="btn-group bootstrap-select btn-comment-post">
                      <select data-plugin="selectpicker">
                        <option>Top Comments</option>
                        <option>Most Recent</option>
                      </select>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="wall-comment-attrs">
                <div id="wall-comment-reply-2" class="wall-comment-reply clearfix" style="display:none;">
                  <div href="#" class="avatar avatar-md float-left">
                    <img src="../../../global/portraits/6.jpg">
                  </div>
                  <div class="ml-60 wall-comment-form">
                    <textarea rows="1" class="autoHeight" placeholder="Write a comment..."></textarea>
                    <div class="wall-comment-reply-attach">
                      <button class="btn btn-icon btn-dark btn-round bg-grey-400">
                        <i class="icon md-attachment-alt rotate" aria-hidden="true"></i>
                      </button>
                    </div>
                  </div>
                </div>
                <div class="wall-comment">
                  <a href="#" class="avatar avatar-md float-left">
                    <img src="../../../global/portraits/5.jpg">
                  </a>
                  <div class="ml-60 box-post-comment">
                    <a href="#">Sam Anderson</a>
                    <span class="ml-10">Ut velit ipsum elit ut mollit elit. Proident officia eu ex.</span>
                    <p class="font-size-12 mt-5 grey-500">
                      <a href="javascript:void(0)">
                        Like
                      </a>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <a href="javascript:void(0)" class="action-reply">
                        Reply
                      </a>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <span >
                        <i class="icon md-favorite"></i>
                        <span>5</span>
                      </span>
                      <span class="mr-5 ml-5">&#8226;</span>
                      <span>
                        30th July 2017
                      </span>
                    </p>

                    <div class="display-reply"></div>
                  </div>
                  
                </div>
              </div>
            </div>
            <!-- End Post Section-->

            
            


            <a class="btn btn-block btn-default" href="javascript:void(0)" role="button">Show more</a>
          </div>

        </div>
        <div class="col-md-5">
            <div class="panel cover overlay" style="margin-bottom: 1.875rem;">
              <img class="cover-image h-350" src="../../assets/examples/img/horizontal-6.jpg" alt="...">
              <div class="overlay-panel vertical-align" style="background: rgba(103, 58, 183, .8);">
                <div class="vertical-align-middle">
                  <h1 class="mt-0">Start Free!!!</h1>
                  <h4>Free!! E-Office Automation System
For modern leaders modern organizations.

Sharing, Talking, Sharing, Communication & Collaboration within the organization by the main central system.

More than 30 subsystems to serve customers and employees.

The Main Central System will be your Data warehouse to keep all of the information in the organization.

Also with the API for connect with other applications.</h4>
                  <div class="pt-20">
                    <button type="button" class="btn btn-primary waves-effect waves-classic waves-effect waves-classic mr-20">START FREE</button>
                    <button type="button" class="btn btn-success waves-effect waves-classic"><i class="icon md-play-circle" aria-hidden="true"></i> JigsawOffice Tour</button>
                  </div>
                </div>
               </div>
            </div>
            <div class="panel">
                <div class="panel-heading">
                  <h3 class="panel-title">My Tasks</h3>
                </div>
                <div class="panel-body text-center">
                  <div class="row">
                    <div class="col-md-5 pt-20">
                      <div class="pie-progress pie-progress" data-plugin="pieProgress" data-barcolor="#4caf50"
                        data-goal="72" aria-valuenow="72" data-size="100" data-barsize="8"
                        role="progressbar">
                          <div class="pie-progress-content">
                            <div class="pie-progress-number">72 %</div>
                            <div class="pie-progress-label">Progress</div>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="counter counter-md text-left">
                          <div class="contextual-progress mb-0 mt-10">
                            <div class="clearfix">
                              <div class="progress-title">Order due</div>
                              <div class="progress-label">2 / 5</div>
                            </div>
                            <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                              <div class="progress-bar progress-bar-danger" aria-valuemin="0" aria-valuemax="100" style="width: 40%;" aria-valuenow="60" role="progressbar">
                              </div>
                            </div>
                          </div>
                          
                        </div>
                        <div class="counter counter-md text-left">
                          <div class="contextual-progress mb-0 mt-10">
                            <div class="clearfix">
                              <div class="progress-title">Today</div>
                              <div class="progress-label">5 / 8</div>
                            </div>
                            <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                              <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 60%;" aria-valuenow="60" role="progressbar">
                              </div>
                            </div>
                          </div>
                          
                        </div>
                        <div class="counter counter-md text-left">
                          <div class="contextual-progress mb-0 mt-10">
                            <div class="clearfix">
                              <div class="progress-title">This week</div>
                              <div class="progress-label">14 / 41</div>
                            </div>
                            <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                              <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 34%;" aria-valuenow="60" role="progressbar">
                              </div>
                            </div>
                          </div>
                          
                        </div>
                        <div class="counter counter-md text-left">
                          <div class="contextual-progress mb-0 mt-10">
                            <div class="clearfix">
                              <div class="progress-title">This month</div>
                              <div class="progress-label">45 / 122</div>
                            </div>
                            <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                              <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 36%;" aria-valuenow="60" role="progressbar">
                              </div>
                            </div>
                          </div>
                          
                        </div>
                      
                    </div>
                   </div> 
                </div>
              </div>

              <div class="panel">
                <div class="panel-heading">
                  <h3 class="panel-title">My workspace</h3>
                </div>
                <div class="panel-body">
                  <div class="row">
                    <?php 
                    $_subj = array("Project Jigsaw Office 2017","Redesign Corporate Websites");
                    $priority_color = array("#f3273c","#ff9800","#4caf50","#757575");
                    $pri = array("danger","warning","success","dark");
                    for($aa=1;$aa<=2;$aa++){
                      $progress = rand(20,80);
                      $rand_pri = rand(0,3);
                    ?>
                      <a class="list-group-item col-md-6 my-5" target="_blank" href="../workspace/workspace-home2-external2.php">
                        <div class="media">
                          <div class="media-body">
                            <div class="clearfix mb-5">
                              <div class="text-<?=$pri[$rand_pri]?> float-left"><i class="icon fa-circle mr-5" aria-hidden="true"></i></div>
                              <div class="progress w-150 mb-0 mt-5 float-left" style="height:12px;" data-labeltype="percentage" data-plugin="progress">
                                <div class="progress-bar progress-bar-<?=$pri[$rand_pri]?>" role="progressbar" style="width: <?=$progress?>%; line-height:12px;">
                                  <span class="progress-label font-size-10"><?=$progress?>%</span>
                                </div>
                              </div>
                            </div>
                            <h6 class="media-heading mb-0"><?=$_subj[$aa-1]?></h6>
                          </div>
                        </div>
                      </a>
                    <?php }?>
                  </div> 
                </div>
              </div>

              <div class="panel">
                <div class="panel-heading">
                  <h3 class="panel-title">Tickets</h3>
                </div>
                <div class="panel-body">
                  <ul class="list-group list-group-dividered list-group-full">
                    <li class="list-group-item">
                      <small class="badge badge-round badge-info mt-5 mr-10 float-left">Completed</small>
                      <p><a class="hightlight" href="javascript:void(0)">Server unavaible</a>
                        <span>[13060]</span>
                      </p>
                      <small>Opened by
                        <a class="hightlight" href="javascript:void(0)">
                          <span class="avatar avatar-xs">
                            <img src="../../../global/portraits/1.jpg" alt="">
                          </span>
                          <span>Herman Beck</span>
                        </a>
                        <time datetime="2015-07-01T08:55">2 hours ago</time>
                      </small>
                    </li>
                    <li class="list-group-item">
                      <small class="badge badge-round badge-warning mt-5 mr-10 float-left">Pendening</small>
                      <p><a class="hightlight" href="javascript:void(0)">Mobile App Problem</a>
                        <span>[13061]</span>
                      </p>
                      <small>Opened by
                        <a class="hightlight" href="javascript:void(0)">
                          <span class="avatar avatar-xs">
                            <img src="../../../global/portraits/2.jpg" alt="">
                          </span>
                          <span>Mary Adams</span>
                        </a>
                        <time datetime="2015-07-01T07:55">1 hour ago</time>
                      </small>
                    </li>
                    <li class="list-group-item">
                      <small class="badge badge-round badge-primary mt-5 mr-10 float-left">In progress</small>
                      <p><a class="hightlight" href="javascript:void(0)">IE8 problem</a>
                        <span>[13062]</span>
                      </p>
                      <small>Opened by
                        <a class="hightlight" href="javascript:void(0)">
                          <span class="avatar avatar-xs">
                            <img src="../../../global/portraits/3.jpg" alt="">
                          </span>
                          <span>Caleb Richards</span>
                        </a>
                        <time datetime="2015-06-28T21:05">3 days ago</time>
                      </small>
                    </li>
                    <li class="list-group-item">
                      <small class="badge badge-round badge-danger mt-5 mr-10 float-left">Rejected</small>
                      <p><a class="hightlight" href="javascript:void(0)">Respoonsive problem</a>
                        <span>[13063]</span>
                      </p>
                      <small>Opened by
                        <a class="hightlight" href="javascript:void(0)">
                          <span class="avatar avatar-xs">
                            <img src="../../../global/portraits/4.jpg" alt="">
                          </span>
                          <span>June Lane</span>
                        </a>
                        <time datetime="2015-06-27T13:05">4 days ago</time>
                      </small>
                    </li>
                  </ul> 
                </div>
              </div>

              <div class="panel">
                <div class="panel-heading">
                  <h3 class="panel-title">My Contacts</h3>
                </div>
                <div class="panel-body">
                  <?php 
                  $_status = array("online","off");
                  for($aa=1;$aa<=5;$aa++){
                  ?>
                  <a href="javascript:void(0);" class="avatar avatar-sm avatar-<?=$_status[rand(0,1)]?> mr-10 mb-10">
                    <img src="../../../global/portraits/<?=rand(1,20)?>.jpg" alt="">
                    <i></i>
                  </a>
                  <?php }?>
                </div>
              </div>

              <div class="panel cover overlay" style="margin-bottom: 1.875rem;">
              <img class="cover-image h-350" src="../../assets/examples/img/_sales-ovw.jpg" alt="...">
              <div class="overlay-panel vertical-align text-right" style="background: rgba(255,50,50, .5);">
                <div class="vertical-align-bottom">
                  <h3 class="mt-0">What Salesforce Can Do For Your Company</h3>
                </div>
               </div>
            </div>

            

        </div>
          </div>
          </div>
        </div>


      </div>
    </div>
  </div>
  <!-- End Page -->
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/slick-carousel/slick.js"></script>
  <script src="../../global/vendor/jquery.dotdotdot/jquery.dotdotdot.js"></script>
  <script src="../../global/vendor/chartist/chartist.min.js"></script>
  <script src="../../global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.min.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/peity/jquery.peity.min.js"></script>
  <script src="../../global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.min.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/plyr/plyr.js"></script>
  <script src="../../global/vendor/aspieprogress/jquery-asPieProgress.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <!-- Page -->
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/responsive-tabs.js"></script>
  <script src="../../global/js/Plugin/tabs.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../global/js/Plugin/peity.js"></script>
  <script src="../../../global/js/Plugin/bootstrap-tokenfield.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/plyr.js"></script>
  <script src="../../global/js/Plugin/aspieprogress.js"></script>
  <script src="../../assets/examples/js/charts/pie-progress.js"></script>
  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
    $(document).ready(function($) {
      Site.run();
      $(".ellipsis").dotdotdot({
        after: "a.readmore"
      });

      function chooseDateRange(start, end) {
          var DrangeValue = start.format('MM/DD/YYYY') + '-' + end.format('MM/DD/YYYY');
          $('#SearchDateRange span#daterange-value').html(DrangeValue);
          var dataKey = "123";
          var dataValue = "xxx";
          var stoken = '';
            $('.search-wrapper .currently-showing .chooseDateRange').remove();
            stoken = '<div class="token chooseDateRange" data-key="'+dataKey+'" data-value="' + dataValue + '"><span class="token-label" >' + DrangeValue + '</span><a  href="#" class="close" tabindex="-1">×</a></div>';
            $('.search-wrapper .currently-showing').append(stoken);
            sysGenUrlSearch();
      }

      $('#SearchDateRange').daterangepicker({
          "autoApply": true,
          "opens": "center",
          ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
      }, chooseDateRange);
      /*
      $("#home-announcement").slick({
        dots: true,
        infinite: true,
        adaptiveHeight: true,
        arrows: false,
        speed: 500,
        autoplay: true,
        swipeToSlide: true
      });
      $("#home-news").slick({
        dots: true,
        infinite: true,
        adaptiveHeight: true,
        arrows: false,
        speed: 600,
        autoplay: true,
        swipeToSlide: true
      });
      */

    });

  })(document, window, jQuery);
  </script>
<?php include("../_footer-form.php");?>
</body>
</html>
