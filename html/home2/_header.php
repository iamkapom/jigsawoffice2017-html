<?php
function get_img($imType="R"){
  $imgType = array('','vertical-','horizontal-');
  $imgT = rand(1,2);
  $maxR = 16;
  if($imgT===2) $maxR = 21;

  $img = "../../assets/examples/img/horizontal-".rand(1,21).".jpg";
  if($imType=="V"){
    $img = "../../assets/examples/img/vertical-".rand(1,16).".jpg";
  }else if($imType=="R"){
    $img = "../../assets/examples/img/".$imgType[$imgT].rand(1,$maxR).".jpg";
  }
  return $img;
}
function display_imageContainer($Numberimg=1){
  $img1 = get_img("R");
  list($width, $height) = getimagesize($img1);

 $StrImg = '<div class="img-container"><img width="100%" src="'.$img1.'" alt="..."></div>';
 if($Numberimg==2 && $width>=$height){
  $StrImg = '<div class="img-container">';
  $StrImg.= '<div class="imgv-master-view2"><img width="100%" src="'.$img1.'" alt="..."></div>';
  $StrImg.= '<div class="imgv-copy-view2"><img width="100%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '</div>';
 }else if($Numberimg==2 && $width<$height){
  $StrImg = '<div class="img-container">';
  $StrImg.= '<div class="imgh-master-view2"><img width="100%" src="'.$img1.'" alt="..."></div>';
  $StrImg.= '<div class="imgh-copy-view2"><img width="100%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '</div>';
 }else if($Numberimg==3 && $width>=$height){
  $StrImg = '<div class="img-container">';
  $StrImg.= '<div class="imgv-first-view3"><img width="100%" src="'.$img1.'" alt="..."></div>';
  $StrImg.= '<div class="imgv-bottom-view31"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '<div class="imgv-bottom-view32"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '</div>';
 }else if($Numberimg==3 && $width<$height){
  $StrImg = '<div class="img-container">';
  $StrImg.= '<div class="imgh-first-view3"><img width="140%" src="'.$img1.'" alt="..."></div>';
  $StrImg.= '<div class="imgh-right-view31"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '<div class="imgh-right-view32"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '</div>';
 }else if($Numberimg==4 && $width>=$height){
  $StrImg = '<div class="img-container">';
  $StrImg.= '<div class="imgv-first-view3"><img width="100%" src="'.$img1.'" alt="..."></div>';
  $StrImg.= '<div class="imgv-bottom-view41"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '<div class="imgv-bottom-view42"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '<div class="imgv-bottom-view43"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '</div>';
 }else if($Numberimg==4 && $width<$height){
  $StrImg = '<div class="img-container">';
  $StrImg.= '<div class="imgh-first-view3"><img width="140%" src="'.$img1.'" alt="..."></div>';
  $StrImg.= '<div class="imgh-right-view41"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '<div class="imgh-right-view42"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '<div class="imgh-right-view43"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '</div>';
 }else if($Numberimg>4 && $width>=$height){
  $StrImg = '<div class="img-container">';
  $StrImg.= '<div class="imgv-first-view3"><img width="100%" src="'.$img1.'" alt="..."></div>';
  $StrImg.= '<div class="imgv-bottom-view41"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '<div class="imgv-bottom-view42"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '<div class="imgv-bottom-view43"><div class="overlay">+'.($Numberimg-4).'</div><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '</div>';
 }else if($Numberimg>4 && $width<$height){
  $StrImg = '<div class="img-container">';
  $StrImg.= '<div class="imgh-first-view3"><img width="140%" src="'.$img1.'" alt="..."></div>';
  $StrImg.= '<div class="imgh-right-view41"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '<div class="imgh-right-view42"><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '<div class="imgh-right-view43"><div class="overlay">+'.($Numberimg-4).'</div><img width="140%" src="'.get_img("R").'" alt="..."></div>';
  $StrImg.= '</div>';
 }

 return $StrImg;
}
?>
<nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega navbar-inverse"
role="navigation">
  <div class="navbar-header">
    <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
    data-toggle="menubar">
      <span class="sr-only">Toggle navigation</span>
      <span class="hamburger-bar"></span>
    </button>
    <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse"
    data-toggle="collapse">
      <i class="icon md-more" aria-hidden="true"></i>
    </button>
    <div class="navbar-brand navbar-brand-center">
      <a class="p-5 m-0" href="../home/">
        Customer
        <div class="small">@customer.com</div>
      </a>
    </div>
    <button type="button" class="navbar-toggler collapsed w-30 mr-5" data-target="#searchFillIn"
    data-toggle="modal">
      <span class="sr-only">Toggle Search</span>
      <i class="icon md-search" aria-hidden="true"></i>
    </button>
  </div>
  <div class="navbar-container container-fluid">
    <!-- Navbar Collapse -->
    <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
      <!-- Navbar Toolbar -->
      <ul class="nav navbar-toolbar">
        <li class="nav-item hidden-float" id="toggleMenubar">
          <a class="nav-link" data-toggle="menubar" href="#" role="button">
            <i class="icon hamburger hamburger-arrow-left">
                <span class="sr-only">Toggle menubar</span>
                <span class="hamburger-bar"></span>
              </i>
          </a>
        </li>
        <li class="nav-item hidden-float">
          <a class="nav-link icon md-search" data-toggle="modal" href="#" data-target="#searchFillIn"
          role="button">
            <span class="sr-only">Toggle Search</span>
          </a>
        </li>
      </ul>
      <!-- End Navbar Toolbar -->
      <!-- Navbar Toolbar Right -->
      <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
        <li class="nav-item dropdown">
          <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false"
          data-animation="scale-up" role="button">
            <span class="avatar avatar-online">
              <img src="../../global/portraits/5.jpg" alt="...">
              <i></i>
            </span>
          </a>
          <div class="dropdown-menu" role="menu">
            <a class="dropdown-item" href="../profile/index.php" role="menuitem"><i class="icon md-account" aria-hidden="true"></i> Profile</a>
            <a class="dropdown-item" href="../account-settings/settings.php" role="menuitem"><i class="icon md-settings" aria-hidden="true"></i> Settings</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon md-power" aria-hidden="true"></i> Logout</a>
          </div>
        </li>
        
        <li class="nav-item" id="toggleChat">
          <a class="nav-link" data-toggle="site-sidebar" href="javascript:void(0)" title="Chat" data-url="../sidebar-msg.php">
            <i class="icon md-comments" aria-hidden="true"></i>
            <span class="badge badge-pill badge-danger up">3</span>
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="site-sidebar" href="javascript:void(0)" title="Notifications" data-url="../sidebar-notify.php">
            <i class="icon md-notifications" aria-hidden="true"></i>
            <span class="badge badge-pill badge-danger up">5</span>
          </a>
        </li>
        
        <li class="nav-item dropdown mr-10">
          <a class="nav-link" data-toggle="dropdown" href="javascript:void(0)" title="help"
          aria-expanded="false" data-animation="scale-up" role="button">
            <i class="icon md-help" aria-hidden="true"></i>
          </a>
          <div class="dropdown-menu" role="menu">
            <a class="dropdown-item" href="../help/index.php" role="menuitem"><i class="icon md-help" aria-hidden="true"></i> Help</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../help/support.php" role="menuitem"><i class="icon md-email" aria-hidden="true"></i> Support Inbox</a>
            <a class="dropdown-item" href="javascript:void(0)" data-target="#FeedbackForm" data-toggle="modal" role="menuitem"><i class="icon fa-star" aria-hidden="true"></i> Give Us Feedback</a>
            <a class="dropdown-item" href="javascript:void(0)" data-target="#reportProblemForm" data-toggle="modal" role="menuitem"><i class="icon fa-exclamation-triangle" aria-hidden="true"></i> Report a Problem</a>
          </div>
        </li>
      </ul>
      <!-- End Navbar Toolbar Right -->
      <div class="navbar-brand navbar-brand-center">
        <a class="p-5 m-0" href="../home/">
          Customer
          <div class="small">@customer.com</div>
        </a>
      </div>
    </div>
    <!-- End Navbar Collapse -->
  </div>
</nav>