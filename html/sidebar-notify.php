<section>
  <div class="row p-20 bg-grey-600">
    <div class="col-md-10" style="padding-top: 3px;">
      <h5 class="p-0 m-0 white"><i class="icon md-notifications" aria-hidden="true"></i> NOTIFICATIONS</h5>
    </div>
    <div class="col-md-2 text-right ">
      <a style="display:block;" href="javascript:void(0)" onclick="$(this).find('i').toggleClass('md-volume-up md-volume-off')" class="white" role="button">
        <i class="icon md-volume-up" aria-hidden="true"></i>
      </a>
    </div>
  </div>
</section>
<div class="site-sidebar-tab-content tab-content">
  <div class="tab-pane active" >
    <div>
      <div>
        <div class="list-group list-group-dividered pr-10">
          <?php 
          for($aa=1;$aa<=3;$aa++){
          ?>
            <a class="list-group-item pr-10 pl-15" href="javascript:void(0)">
              <div class="media">
                <div class="pr-10">
                  <div class="avatar avatar-online">
                    <img class="img-fluid" src="../../global/portraits/<?=rand(1,15)?>.jpg" alt="...">
                  </div>
                </div>
                <div class="media-body">
                  <h6 class="media-heading <?=$aa>1?"grey-500":"black"?>">A new order has been placed</h6>
                  <time class="media-meta <?=$aa>1?"grey-500":"black"?>" datetime="2017-06-12T20:50:48+08:00">5 hours ago</time>
                </div>
              </div>
            </a>
            <a class="list-group-item pr-10 pl-15" href="javascript:void(0)">
              <div class="media">
                <div class="pr-10">
                  <div class="avatar avatar-online">
                    <img class="img-fluid" src="../../global/portraits/<?=rand(1,15)?>.jpg" alt="...">
                  </div>
                </div>
                <div class="media-body">
                  <h6 class="media-heading <?=$aa>1?"grey-500":"black"?>">Completed the task</h6>
                  <time class="media-meta <?=$aa>1?"grey-500":"black"?>" datetime="2017-06-11T18:29:20+08:00">2 days ago</time>
                </div>
              </div>
            </a>
            <a class="list-group-item pr-10 pl-15" href="javascript:void(0)">
              <div class="media">
                <div class="pr-10">
                  <div class="avatar avatar-online">
                    <img class="img-fluid" src="../../global/portraits/<?=rand(1,15)?>.jpg" alt="...">
                  </div>
                </div>
                <div class="media-body">
                  <h6 class="media-heading <?=$aa>1?"grey-500":"black"?>">Settings updated</h6>
                  <time class="media-meta <?=$aa>1?"grey-500":"black"?>" datetime="2017-06-11T14:05:00+08:00">2 days ago</time>
                </div>
              </div>
            </a>
            <a class="list-group-item pr-10 pl-15" href="javascript:void(0)">
              <div class="media">
                <div class="pr-10">
                  <div class="avatar avatar-online">
                    <img class="img-fluid" src="../../global/portraits/<?=rand(1,15)?>.jpg" alt="...">
                  </div>
                </div>
                <div class="media-body">
                  <h6 class="media-heading <?=$aa>1?"grey-500":"black"?>">Event started</h6>
                  <time class="media-meta <?=$aa>1?"grey-500":"black"?>" datetime="2017-06-10T13:50:18+08:00">3 days ago</time>
                </div>
              </div>
            </a>
            <a class="list-group-item pr-10 pl-15" href="javascript:void(0)">
              <div class="media">
                <div class="pr-10">
                  <div class="avatar avatar-online">
                    <img class="img-fluid" src="../../global/portraits/<?=rand(1,15)?>.jpg" alt="...">
                  </div>
                </div>
                <div class="media-body">
                  <h6 class="media-heading <?=$aa>1?"grey-500":"black"?>">Message received</h6>
                  <time class="media-meta <?=$aa>1?"grey-500":"black"?>" datetime="2017-06-10T12:34:48+08:00">3 days ago</time>
                </div>
              </div>
            </a>
          <?php }?>

            

        </div>
      </div>
    </div>
  </div>
  <section>
    <div class="row p-20 bg-grey-200">
      <div class="col-md-12">
        <a href="javascript:void(0)" role="menuitem">
          All notifications
        </a>
      </div>
    </div>
  </section>
  
</div>