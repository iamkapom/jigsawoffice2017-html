<div class="card card-shadow card-bordered">
  <div class="card-header bg-grey-600 white p-15 clearfix" style="padding-bottom: 0px;">
    <span class="hidden-xs-down avatar avatar-100 float-left mr-20 img-bordered bg-white" href="javascript:void(0)">
      <img src="../../global/portraits/1.jpg" alt="">
    </span>
    <span class="hidden-sm-up avatar avatar-lg float-left mr-20 img-bordered bg-white" href="javascript:void(0)">
      <img src="../../global/portraits/1.jpg" alt="">
    </span>
    <div class="font-size-24"><strong>Robin Ahrens</strong></div>
    <div class="grey-300 font-size-16">Robin</div>

    <div style="position:absolute;right:0;top:0;">
        <button type="button" class="btn btn-pure btn-inverse font-size-30 slidePanel-close icon md-close" aria-hidden="true"></button>
    </div>
    <div class="pl-60">
        <button type="button" class="btn btn-pure font-size-20 btn-inverse icon md-edit" aria-hidden="true"></button>
        <button type="button" class="btn btn-pure font-size-20 btn-inverse icon md-delete" aria-hidden="true"></button>
    </div>
  </div>
  <div class="card-block scrollable is-enabled scrollable-vertical" data-plugin="scrollable" style="padding: 10px 20px; postition;relative;">
    <section>
         <a href="#">
        <span class="badge p-5 mb-5 badge-outline" style="border-color:#F2543B; color:#F2543B;">Designer</span>
      </a>
                          <a href="#">
        <span class="badge p-5 mb-5 badge-outline" style="border-color:#E8AD73; color:#E8AD73;">jQuery</span>
      </a>
                          <a href="#">
        <span class="badge p-5 mb-5 badge-outline" style="border-color:#C6EF3D; color:#C6EF3D;">Javascript</span>
      </a>
                          <a href="#">
        <span class="badge p-5 mb-5 badge-outline" style="border-color:#CF9650; color:#CF9650;">HTML5</span>
      </a>
    </section>
    <section>
      <div class="row pt-10 pb-20 black">
      <div class="col-md-5">
        <p data-info-type="phone" class="mb-10 text-nowrap font-size-20">
          <i class="icon fa-mobile mr-15"></i>
          <span class="text-break">9192372533<span>
        </p>
        <p data-info-type="phone" class="mb-10 text-nowrap font-size-20">
          <i class="icon fa-phone mr-10"></i>
          <span class="text-break">0254312345<span>
        </p>
      </div>
      <div class="col-md-7">
        <p data-info-type="email" class="mb-10 text-nowrap font-size-14">
          <i class="icon md-email mr-10"></i>
          <span class="text-break">malinda.h@gmail.com<span>
        </p>
        <p data-info-type="fb" class="mb-10 text-nowrap font-size-14">
          <i class="icon md-facebook mr-15"></i>
          <span class="text-break">facebook.com/malinda.hollaway<span>
        </p>
        <p data-info-type="twitter" class="mb-10 text-nowrap font-size-14">
          <i class="icon md-twitter mr-10"></i>
          <span class="text-break">@malinda (twitter.com/malinda)<span>
        </p>
      </div>
    </div>
    </section>
    <section>
      <div class="nav-tabs-horizontal" data-plugin="tabs">
        <ul class="nav nav-tabs" role="tablist">
          <li class="nav-item" role="presentation"><a class="nav-link active" data-toggle="tab" href="#exampleTabsOne" aria-controls="exampleTabsOne" role="tab" aria-expanded="true">About</a></li>
          <li class="nav-item" role="presentation"><a class="nav-link" data-toggle="tab" href="#exampleTabsTwo" aria-controls="exampleTabsTwo" role="tab" aria-expanded="false">Workspace</a></li>
          <li class="dropdown nav-item" role="presentation" style="display: none;">
            <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#" aria-expanded="false">Menu </a>
            <div class="dropdown-menu" role="menu">
              <a class="dropdown-item" data-toggle="tab" href="#exampleTabsOne" aria-controls="exampleTabsOne" role="tab">About</a>
              <a class="dropdown-item" data-toggle="tab" href="#exampleTabsTwo" aria-controls="exampleTabsTwo" role="tab">Workspace</a>
            </div>
          </li>
        </ul>
        <div class="tab-content pt-20">
          <div class="tab-pane active" id="exampleTabsOne" role="tabpanel" aria-expanded="true">
            <div class="user-background card">
              <div class="card-block">
                <h5 class="card-title">
                  <i class="icon md-quote"></i>
                  <span>Summary</span>
                </h5>
                <p class="card-text">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                  incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                  commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                  velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                  occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                  mollit anim id est laborum.
                </p>
              </div>
              <div class="card-block">
                <h5 class="card-title">
                  <i class="icon md-badge-check"></i>
                  <span>Experience</span>
                </h5>
                <ul class="timeline timeline-single">
                  <li class="timeline-item mb-30">
                    <div class="timeline-dot"></div>
                    <div class="timeline-content">
                      <span class="block mb-5"><strong>2013 President</strong></span>
                      <span class="block font-size-18 mb-5">Co-founder, Chairman</span>
                      <span class="block mb-5">Company Name</span>
                    </div>
                  </li>
                  <li class="timeline-item mb-30">
                    <div class="timeline-dot"></div>
                    <div class="timeline-content">
                      <span class="block mb-5"><strong>2010 President</strong></span>
                      <span class="block font-size-18 mb-5">Co-founder, Chairman</span>
                      <span class="block mb-5">Company Name</span>
                    </div>
                  </li>
                  <li class="timeline-item mb-30">
                    <div class="timeline-dot"></div>
                    <div class="timeline-content">
                      <span class="block mb-5"><strong>2008 President</strong></span>
                      <span class="block font-size-18 mb-5">Co-founder, Chairman</span>
                      <span class="block mb-5">Company Name</span>
                    </div>
                  </li>
                  <li class="timeline-item mb-30">
                    <div class="timeline-dot"></div>
                    <div class="timeline-content">
                      <span class="block mb-5"><strong>2005 President</strong></span>
                      <span class="block font-size-18 mb-5">Co-founder, Chairman</span>
                      <span class="block mb-5">Company Name</span>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="card-block">
                <h5 class="card-title">
                  <i class="icon md-graduation-cap"></i>
                  <span>Edication</span>
                </h5>
                <ul class="timeline timeline-single">
                  <li class="timeline-item mb-30">
                    <div class="timeline-dot"></div>
                    <div class="timeline-content">
                      <span class="block font-size-15 mb-5">2000 President</span>
                      <span class="block mb-5">BS Computer Science</span>
                      <span class="block mb-5">Harvard University</span>
                    </div>
                  </li>
                  <li class="timeline-item mb-30">
                    <div class="timeline-dot"></div>
                    <div class="timeline-content">
                      <span class="block font-size-15 mb-5">1996 - 2000</span>
                      <span class="block mb-5">B.E</span>
                      <span class="block mb-5">Tsinghua University</span>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="card-block">
                <h5 class="card-title">
                  <i class="icon md-lamp"></i>
                  <span>Skills</span>
                </h5>
                <div class="example-wrap">
                  <h5>Photoshop
                    <span class="float-right">40%</span>
                  </h5>
                  <div class="progress progress-sm">
                    <div class="progress-bar progress-bar-indicating active" style="width: 40%;" role="progressbar"></div>
                  </div>
                  <h5>Html
                    <span class="float-right">80%</span>
                  </h5>
                  <div class="progress progress-sm">
                    <div class="progress-bar progress-bar-indicating active" style="width: 80%;" role="progressbar"></div>
                  </div>
                  <h5>Javascript
                    <span class="float-right">60%</span>
                  </h5>
                  <div class="progress progress-sm">
                    <div class="progress-bar progress-bar-indicating active" style="width: 60%;" role="progressbar"></div>
                  </div>
                </div>
              </div>
              <div class="card-block">
                <h5 class="card-title">
                  <i class="icon md-star"></i>
                  <span>More interest</span>
                </h5>
                <span class="badge mb-5 mr-5 badge-default">Movie</span>
                <span class="badge mb-5 mr-5 badge-default">Games</span>
                <span class="badge mb-5 mr-5 badge-primary">Music</span>
                <span class="badge mb-5 mr-5 badge-warning">Pokemon go</span>
                <span class="badge mb-5 mr-5 badge-success">Football</span>
                <span class="badge mb-5 mr-5 badge-info">Tennis</span>
                <span class="badge mb-5 mr-5 badge-danger">Hiking</span>
                <span class="badge mb-5 mr-5 badge-dark">Skating</span>
                <span class="badge mb-5 mr-5 badge-success">Table tennis</span>
                <span class="badge mb-5 mr-5 badge-default">Reading</span>
                <span class="badge mb-5 mr-5 badge-dark">Blogging/writing</span>
                <span class="badge mb-5 mr-5 badge-warning">DIY</span>
                <span class="badge mb-5 mr-5 badge-default">Singing</span>
                <span class="badge mb-5 mr-5 badge-info">Dancing</span>
              </div>
            </div>
          </div>
          <div class="tab-pane" id="exampleTabsTwo" role="tabpanel" aria-expanded="false">
            <div class="panel" id="projects">
              <div>
                <table class="table table-hover" data-plugin="selectable" data-row-selectable="true">
                  <thead>
                    <tr>
                      <td><strong>Projects</strong></td>
                      <td width="20%" align="center"><strong>Completed</strong></td>
                      <td width="20%" align="center" class="hidden-sm-down"><strong>Date</strong></td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                      <span class="font-size-14"><strong>The sun climbing plan</strong></span>
                      <p class="hidden-md-up font-size-12 mb-0">Jan 1, 2017</p>
                      </td>
                      <td align="center">
                        <span data-plugin="peityPie" data-skin="red" style="display: none;">7/10</span><svg class="peity" height="22" width="22"><path d="M 11 0 A 11 11 0 1 1 0.5383783207533117 14.399186938124423 L 11 11" fill="#d32f2f"/><path d="M 0.5383783207533117 14.399186938124423 A 11 11 0 0 1 10.999999999999998 0 L 11 11" fill="#ef5350"/></svg>
                      </td>
                      <td class="hidden-sm-down">Jan 1, 2017</td>
                    </tr>
                    <tr>
                      <td><span class="font-size-14"><strong>Lunar probe project</strong></span>
                      <p class="hidden-md-up font-size-12 mb-0">Feb 12, 2017</p></td>
                      <td align="center">
                        <span data-plugin="peityPie" data-skin="blue" style="display: none;">3/10</span><svg class="peity" height="22" width="22"><path d="M 11 0 A 11 11 0 0 1 21.46162167924669 14.399186938124421 L 11 11" fill="#1976d2"/><path d="M 21.46162167924669 14.399186938124421 A 11 11 0 1 1 10.999999999999998 0 L 11 11" fill="#42a5f5"/></svg>
                      </td>
                      <td class="hidden-sm-down">Feb 12, 2017</td>
                    </tr>
                    <tr>
                      <td><span class="font-size-14"><strong>Dream successful plan</strong></span>
                      <p class="hidden-md-up font-size-12 mb-0">Apr 9, 2017</p></td>
                      <td align="center">
                        <span data-plugin="peityPie" data-skin="green" style="display: none;">9/10</span><svg class="peity" height="22" width="22"><path d="M 11 0 A 11 11 0 1 1 4.534362224782794 2.100813061875579 L 11 11" fill="#388e3c"/><path d="M 4.534362224782794 2.100813061875579 A 11 11 0 0 1 10.999999999999998 0 L 11 11" fill="#66bb6a"/></svg>
                      </td>
                      <td class="hidden-sm-down">Apr 9, 2017</td>
                    </tr>
                    <tr>
                      <td><span class="font-size-14"><strong>Office automatization</strong></span>
                      <p class="hidden-md-up font-size-12 mb-0">May 15, 2017</p></td>
                      <td align="center">
                        <span data-plugin="peityPie" data-skin="orange" style="display: none;">5/10</span><svg class="peity" height="22" width="22"><path d="M 11 0 A 11 11 0 0 1 11 22 L 11 11" fill="#f57c00"/><path d="M 11 22 A 11 11 0 0 1 10.999999999999998 0 L 11 11" fill="#ffa726"/></svg>
                      </td>
                      <td class="hidden-sm-down">May 15, 2017</td>
                    </tr>
                    <tr>
                      <td><span class="font-size-14"><strong>Open strategy</strong></span>
                      <p class="hidden-md-up font-size-12 mb-0">Jun 2, 2017</p></td>
                      <td align="center">
                        <span data-plugin="peityPie" data-skin="brown" style="display: none;">2/10</span><svg class="peity" height="22" width="22"><path d="M 11 0 A 11 11 0 0 1 21.46162167924669 7.600813061875579 L 11 11" fill="#5d4037"/><path d="M 21.46162167924669 7.600813061875579 A 11 11 0 1 1 10.999999999999998 0 L 11 11" fill="#8d6e63"/></svg>
                      </td>
                      <td class="hidden-sm-down">Jun 2, 2017</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    </div>
  </div>
</div>