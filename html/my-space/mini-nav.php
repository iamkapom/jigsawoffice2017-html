<ul class="nav nav-tabs nav-tabs-line bg-white fix-mini-nav" role="tablist">
  <li class="nav-item" role="presentation">
    <a class="nav-link <?=(substr(basename($_SERVER["SCRIPT_FILENAME"]),0,5)=="index"?"active":"")?>" href="../my-space/index.php" aria-expanded="true"><i class="md-account" aria-hidden="true"></i> My Dashboard</a>
  </li>
  <li class="nav-item hidden-sm-down" role="presentation">
    <a class="nav-link <?=(substr(basename($_SERVER["SCRIPT_FILENAME"]),0,5)=="tasks"?"active":"")?>" href="../tasks/tasks-list.php" aria-expanded="false"><i class="md-check-all" aria-hidden="true"></i> My Tasks</a>
  </li>
  <li class="nav-item hidden-sm-down" role="presentation">
    <a class="nav-link <?=(substr(basename($_SERVER["SCRIPT_FILENAME"]),0,5)=="drive"?"active":"")?>" href="../drive/drive-grid.php" aria-expanded="false"><i class="md-cloud" aria-hidden="true"></i> My Drive</a>
  </li>
  <li class="nav-item hidden-sm-down" role="presentation">
    <a class="nav-link <?=(basename($_SERVER["SCRIPT_FILENAME"])=="calendar.php"?"active":"")?>" href="../calendar/calendar.php" aria-expanded="false"><i class="md-calendar" aria-hidden="true"></i> My Calendar</a>
  </li>
  <li class="nav-item hidden-sm-down" role="presentation">
    <a class="nav-link <?=(basename($_SERVER["SCRIPT_FILENAME"])=="my-contact.php"?"active":"")?>" href="../teams/my-contact.php" aria-expanded="false"><i class="md-calendar" aria-hidden="true"></i> My Contacts</a>
  </li>
  <li class="nav-item dropdown hidden-md-up">
    <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#" aria-expanded="false">Menu </a>
    <div class="dropdown-menu dropdown-menu-right" role="menu">
      <a class="dropdown-item" href="javascript:void(0)" aria-expanded="false"><i class="md-check-all" aria-hidden="true"></i> My Tasks</a>
      <a class="dropdown-item" href="javascript:void(0)" aria-expanded="false"><i class="md-cloud" aria-hidden="true"></i> My Drive</a>
      <a class="dropdown-item" href="javascript:void(0)" aria-expanded="false"><i class="md-calendar" aria-hidden="true"></i> My Calendar</a>
    </div>
  </li>
</ul>