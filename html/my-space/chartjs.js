
(function(document, window, $) {
  'use strict';

  var Site = window.Site;

  $(document).ready(function($) {
    Site.run();
  });

  Chart.defaults.global.responsive = true;

  //Chart
  var now = new Date();
  var months = ["Jan", "Feb", "Mar", "Apr", "May","Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  var _mon = [];
  var _plan = [];
  var _comm = [];
  var _rrand = 0;
  for(var i=0; i<=11;i++){
     now.setMonth(now.getMonth() - 1);
     _mon[i] = months[now.getMonth()]+' '+now.getFullYear();
     _rrand = Math.floor((Math.random() * 20) + 40);
     _plan[i] = _rrand;
     _comm[i] = _rrand - Math.floor((Math.random() * 5) + 0);
  }
  

  // Example Chartjs Line
  // --------------------
  (function() {
    var _ChartData = {
      labels: _mon,
      datasets: [{
        label: "Tasks Completed ",
        backgroundColor: "rgba(76, 175, 80, 1)",
        data: _comm
      },{
        label: "Tasks Planing ",
        backgroundColor: "rgba(244,54,111, 1)",
        data: _plan
      }]
    };

    var myLine = new Chart(document.getElementById("canvas_plan_complete").getContext("2d"), {
      type: 'bar',
      data: _ChartData,
      options: {
          title:{
              display:true,
              text:"Chart.js Bar Chart - Stacked"
          },
          tooltips: {
              mode: 'index',
              intersect: false
          },
          responsive: true,
          scales: {
              xAxes: [{
                  stacked: true,
              }],
              yAxes: [{
                  stacked: true
              }]
          },
          axisY: {
            offset: 70
          }
      }
    });
  })();

})(document, window, jQuery);
