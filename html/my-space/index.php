<?php include("../session.php");?>
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="Jigsaw Office :: Business Co-working Platform">
  <meta name="author" content="">
  <title>Jigsaw Office 2017</title>
  <link rel="apple-touch-icon" href="../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../assets/images/favicon.ico">
  <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="../../assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../global/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../global/css/bootstrap-extend.css">
  <link rel="stylesheet" href="../../assets/css/site.css">
  <link rel="stylesheet" href="../../assets/skins/blue.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="../../global/vendor/waves/waves.css">
  <link rel="stylesheet" href="../../global/vendor/bootstrap-select/bootstrap-select.css">
  <link rel="stylesheet" href="../../assets/examples/css/pages/profile_v3.css">
  <link rel="stylesheet" href="../../assets/examples/css/charts/chartjs.css">
  <link rel="stylesheet" href="../../global/vendor/aspieprogress/asPieProgress.css">
  <link rel="stylesheet" href="../../assets/examples/css/charts/pie-progress.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="../../../../global/fonts/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="../../global/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Prompt:300,300i,400,500'>
  <!--[if lt IE 9]>
    <script src="../../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
</head>
<body class="animsition">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <?php include("../_header.php");?>
  <?php include("../_header-menubar.php");?>
  <!-- Page -->
  <div class="page">
    <div class="page-header page-header-bordered page-header-tabs">
      <h1 class="page-title mb-5">My Space</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="../../index.html">Home</a></li>
        <li class="breadcrumb-item active">My Space</li>
      </ol>
      

      <?php include("mini-nav.php");?>
    </div>

    <div class="page-content container-fluid">
      <div class="row ml-0 mr-0">
        <div class="col-md-12" style="min-height: 1200px;">
          <div class="grid">
              <!-- Box -->
              <div class="grid-item panel mb-0 grid-item--width1 grid-item--height1">
                <div class="panel-heading">
                  <h3 class="panel-title">Annual sales</h3>
                  <div class="panel-actions" style="right: 15px;">
                    <a class="panel-action" data-toggle="site-sidebar" href="javascript:void(0)" data-url="sidebar-widget.php">
                      <i class="icon md-settings" aria-hidden="true"></i>
                    </a>
                  </div>
                </div>
                <div class="panel-body">
                  <div id="chart_annualsales" style="width: 100%; height: 300px;"></div>
                </div>
              </div>
              <!-- Box -->
              <!-- Box -->
              <div class="grid-item panel mb-0 grid-item--width1 grid-item--height1">
                <div class="panel-heading">
                  <h3 class="panel-title">Quarterly sales compare</h3>
                  <div class="panel-actions" style="right: 15px;">
                    <a class="panel-action" data-toggle="site-sidebar" href="javascript:void(0)" data-url="sidebar-widget.php">
                      <i class="icon md-settings" aria-hidden="true"></i>
                    </a>
                  </div>
                </div>
                <div class="panel-body">
                  <div id="chart_quarterlysalescom" style="width: 100%; height: 300px;"></div>
                </div>
              </div>
              <!-- Box -->
              <!-- Box -->
              <div class="grid-item panel mb-0 grid-item--width1 grid-item--height1">
                <div class="panel-heading">
                  <h3 class="panel-title">Company Expenses</h3>
                  <div class="panel-actions" style="right: 15px;">
                    <a class="panel-action" data-toggle="site-sidebar" href="javascript:void(0)" data-url="sidebar-widget.php">
                      <i class="icon md-settings" aria-hidden="true"></i>
                    </a>
                  </div>
                </div>
                <div class="panel-body">
                  <div id="chart_comex" style="width: 100%; height: 300px;"></div>
                </div>
              </div>
              <!-- Box -->
              <!-- Box -->
              <div class="grid-item panel mb-0 grid-item--width1 grid-item--height1">
                <div class="panel-heading">
                  <h3 class="panel-title">Sales of each brand</h3>
                  <div class="panel-actions" style="right: 15px;">
                    <a class="panel-action" data-toggle="site-sidebar" href="javascript:void(0)" data-url="sidebar-widget.php">
                      <i class="icon md-settings" aria-hidden="true"></i>
                    </a>
                  </div>
                </div>
                <div class="panel-body">
                  <div id="chart_eachb" style="width: 100%; height: 300px;"></div>
                </div>
              </div>
              <!-- Box -->
              <!-- Box -->
              <div class="grid-item panel mb-0 grid-item--width1 grid-item--height1">
                <div class="panel-heading">
                  <h3 class="panel-title">Marketing Costs 2017</h3>
                  <div class="panel-actions" style="right: 15px;">
                    <a class="panel-action" data-toggle="site-sidebar" href="javascript:void(0)" data-url="sidebar-widget.php">
                      <i class="icon md-settings" aria-hidden="true"></i>
                    </a>
                  </div>
                </div>
                <div class="panel-body">
                  <div id="chart_eachb1" style="width: 100%; height: 300px;"></div>
                </div>
              </div>
              <!-- Box -->

              <!-- Box -->
              <div class="grid-item panel mb-0 grid-item--width3 grid-item--height1">
                <div class="panel-heading">
                  <h3 class="panel-title">Summary my tasks</h3>
                  <div class="panel-actions" style="right: 15px;">
                    <a class="panel-action" data-toggle="site-sidebar" href="javascript:void(0)" data-url="sidebar-widget.php">
                      <i class="icon md-settings" aria-hidden="true"></i>
                    </a>
                  </div>
                </div>
                <div class="panel-body text-center">
                  <div class="row">
                    <div class="col-md-5">
                      <div class="pie-progress pie-progress" data-plugin="pieProgress" data-barcolor="#4caf50"
                        data-goal="72" aria-valuenow="72" data-size="100" data-barsize="8"
                        role="progressbar">
                          <div class="pie-progress-content">
                            <div class="pie-progress-number">72 %</div>
                            <div class="pie-progress-label">Progress</div>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <h3 class="mt-0">Tasks in Progress</h3>
                        <div class="counter counter-md text-left">
                          <div class="contextual-progress mb-0 mt-10">
                            <div class="clearfix">
                              <div class="progress-title">Order due</div>
                              <div class="progress-label">2 / 5</div>
                            </div>
                            <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                              <div class="progress-bar progress-bar-danger" aria-valuemin="0" aria-valuemax="100" style="width: 40%;" aria-valuenow="60" role="progressbar">
                              </div>
                            </div>
                          </div>
                          
                        </div>
                        <div class="counter counter-md text-left">
                          <div class="contextual-progress mb-0 mt-10">
                            <div class="clearfix">
                              <div class="progress-title">Today</div>
                              <div class="progress-label">5 / 8</div>
                            </div>
                            <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                              <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 60%;" aria-valuenow="60" role="progressbar">
                              </div>
                            </div>
                          </div>
                          
                        </div>
                        <div class="counter counter-md text-left">
                          <div class="contextual-progress mb-0 mt-10">
                            <div class="clearfix">
                              <div class="progress-title">This week</div>
                              <div class="progress-label">14 / 41</div>
                            </div>
                            <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                              <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 34%;" aria-valuenow="60" role="progressbar">
                              </div>
                            </div>
                          </div>
                          
                        </div>
                        <div class="counter counter-md text-left">
                          <div class="contextual-progress mb-0 mt-10">
                            <div class="clearfix">
                              <div class="progress-title">This month</div>
                              <div class="progress-label">45 / 122</div>
                            </div>
                            <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                              <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 36%;" aria-valuenow="60" role="progressbar">
                              </div>
                            </div>
                          </div>
                          
                        </div>
                      
                    </div>
                   </div> 
                </div>
              </div>
              <!-- Box -->
              <!-- Box -->
              <div class="grid-item panel mb-0 grid-item--width3 grid-item--height1">
                <div class="panel-heading">
                  <h3 class="panel-title">Intranet Feed</h3>
                  <div class="panel-actions" style="right: 15px;">
                    <a class="panel-action" data-toggle="site-sidebar" href="javascript:void(0)" data-url="sidebar-widget.php">
                      <i class="icon md-settings" aria-hidden="true"></i>
                    </a>
                  </div>
                </div>
                <div class="panel-body">
                  <div class="panel-collapse in collapse show" id="exampleCollapseDefaultOne" aria-labelledby="exampleHeadingDefaultOne" ole="tabpanel">
                    <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                      <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#exampleTopHome" aria-controls="exampleTopHome" role="tab" aria-expanded="true"> Recent</a></li>
                      <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopComponents" aria-controls="exampleTopComponents" role="tab" aria-expanded="false"> Update</a></li>
                      <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopCss" aria-controls="exampleTopCss" role="tab" aria-expanded="false"> Views</a></li>
                      <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#exampleTopJavascript" aria-controls="exampleTopJavascript" role="tab" aria-expanded="false"> Like</a></li>
                      <li class="dropdown nav-item" style="display: none;">
                        <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#" aria-expanded="false" aria-haspopup="true">Dropdown </a>
                        <div class="dropdown-menu" role="menu">
                          <a class="dropdown-item" data-toggle="tab" href="#exampleTopHome" aria-controls="exampleTopHome" role="tab"> Recent</a>
                          <a class="dropdown-item" data-toggle="tab" href="#exampleTopComponents" aria-controls="exampleTopComponents" role="tab"> Update</a>
                          <a class="dropdown-item" data-toggle="tab" href="#exampleTopCss" aria-controls="exampleTopCss" role="tab"> Views</a>
                          <a class="dropdown-item" data-toggle="tab" href="#exampleTopJavascript" aria-controls="exampleTopJavascript" role="tab"> Like</a>
                        </div>
                      </li>
                    </ul>
                    <div class="text-left">
                      <div class="tab-content">
                        <div class="tab-pane active" id="exampleTopHome" role="tabpanel" aria-expanded="true">
                          <ul class="list-group">
                            <?php 
                            $_mod = array("News","Announcement","Photos","Videos","Events");
                            for($aa=1;$aa<=5;$aa++){
                              $module = $_mod[rand(0,3)];
                              ?>
                            <li class="list-group-item px-0">
                              <div class="media">
                                <div class="pr-20">
                                  <a class="widget-news-img widget-wh-50" href="../apps-intranet/<?=strtolower($module)?>-detail.php" target="_blank">
                                    <img class="" src="<?=get_img('R')?>" alt="...">
                                  </a>
                                </div>
                                <div class="media-body">
                                  <a href="../apps-intranet/<?=strtolower($module)?>-detail.php" target="_blank"><h5 class="mt-0 mb-0"><?=$module?> Recent example title</h5></a>
                                  <small>30th July 2017, <a class="badge badge-warning mr-5" href="javascript:void(0)" title=""> <?=$module?></a></small>
                                  <div>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus.
                                  </div>
                                </div>
                              </div>
                            </li>
                            <?php }?>
                          </ul>
                        </div>
                        <div class="tab-pane" id="exampleTopComponents" role="tabpanel" aria-expanded="false">
                          <ul class="list-group">
                            <?php for($aa=1;$aa<=5;$aa++){
                              $module = $_mod[rand(0,3)];
                              ?>
                            <li class="list-group-item px-0">
                              <div class="media">
                                <div class="pr-20">
                                  <a class="widget-news-img widget-wh-50" href="../apps-intranet/<?=strtolower($module)?>-detail.php" target="_blank">
                                    <img class="" src="<?=get_img('R')?>" alt="...">
                                  </a>
                                </div>
                                <div class="media-body">
                                  <a href="../apps-intranet/<?=strtolower($module)?>-detail.php" target="_blank"><h5 class="mt-0 mb-0"><?=$module?> Update example title</h5></a>
                                  <small>30th July 2017, <a class="badge badge-warning mr-5" href="javascript:void(0)" title=""> <?=$module?></a></small>
                                  <div>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus.
                                  </div>
                                </div>
                              </div>
                            </li>
                            <?php }?>
                          </ul>
                        </div>
                        <div class="tab-pane" id="exampleTopCss" role="tabpanel" aria-expanded="false">
                          <ul class="list-group">
                            <?php for($aa=1;$aa<=5;$aa++){
                              $module = $_mod[rand(0,3)];
                              ?>
                            <li class="list-group-item px-0">
                              <div class="media">
                                <div class="pr-20">
                                  <a class="widget-news-img widget-wh-50" href="../apps-intranet/<?=strtolower($module)?>-detail.php" target="_blank">
                                    <img class="" src="<?=get_img('R')?>" alt="...">
                                  </a>
                                </div>
                                <div class="media-body">
                                  <a href="../apps-intranet/<?=strtolower($module)?>-detail.php" target="_blank"><h5 class="mt-0 mb-0"><?=$module?> View example title</h5></a>
                                  <small>30th July 2017, <a class="badge badge-warning mr-5" href="javascript:void(0)" title=""> <?=$module?></a></small>
                                  <div>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus.
                                  </div>
                                </div>
                              </div>
                            </li>
                            <?php }?>
                          </ul>
                        </div>
                        <div class="tab-pane" id="exampleTopJavascript" role="tabpanel" aria-expanded="false">
                          <ul class="list-group">
                            <?php for($aa=1;$aa<=5;$aa++){
                              $module = $_mod[rand(0,3)];
                              ?>
                            <li class="list-group-item px-0">
                              <div class="media">
                                <div class="pr-20">
                                  <a class="widget-news-img widget-wh-50" href="../apps-intranet/<?=strtolower($module)?>-detail.php" target="_blank">
                                    <img class="" src="<?=get_img('R')?>" alt="...">
                                  </a>
                                </div>
                                <div class="media-body">
                                  <a href="../apps-intranet/<?=strtolower($module)?>-detail.php" target="_blank"><h5 class="mt-0 mb-0"><?=$module?> Like example title</h5></a>
                                  <small>30th July 2017, <a class="badge badge-warning mr-5" href="javascript:void(0)" title=""> <?=$module?></a></small>
                                  <div>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus.
                                  </div>
                                </div>
                              </div>
                            </li>
                            <?php }?>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
              <!-- Box -->
              <!-- Box -->
              <div class="grid-item panel mb-0 grid-item--width3 grid-item--height1">
                <div class="panel-heading">
                  <h3 class="panel-title">My workspace progress</h3>
                  <div class="panel-actions" style="right: 15px;">
                    <a class="panel-action" data-toggle="site-sidebar" href="javascript:void(0)" data-url="sidebar-widget.php">
                      <i class="icon md-settings" aria-hidden="true"></i>
                    </a>
                  </div>
                </div>
                <div class="panel-body">
                  <div class="row">
                    <?php 

                    $priority_color = array("#f3273c","#ff9800","#4caf50","#757575");
                    $pri = array("danger","warning","success","dark");
                    for($aa=1;$aa<=5;$aa++){
                      $progress = rand(20,80);
                      $rand_pri = rand(0,3);
                    ?>
                      <a class="list-group-item col-md-6 my-5" href="javascript:void(0)">
                        <div class="media">
                          <div class="media-body">
                            <div class="clearfix mb-5">
                              <div class="text-<?=$pri[$rand_pri]?> float-left"><i class="icon fa-circle mr-5" aria-hidden="true"></i></div>
                              <div class="progress w-150 mb-0 mt-5 float-left" style="height:12px;" data-labeltype="percentage" data-plugin="progress">
                                <div class="progress-bar progress-bar-<?=$pri[$rand_pri]?>" role="progressbar" style="width: <?=$progress?>%; line-height:12px;">
                                  <span class="progress-label font-size-10"><?=$progress?>%</span>
                                </div>
                              </div>
                              <?php
                              if(rand(0,1)){
                              ?>
                              <span class="badge badge-dark float-left ml-10 mt-3">External</span>
                              <?php }?>
                            </div>
                            <h6 class="media-heading mb-0">Project Jigsaw Office 2017</h6>
                          </div>
                        </div>
                      </a>
                    <?php }?>
                  </div> 
                </div>
              </div>
              <!-- Box -->

              <!-- Box -->
              <div class="grid-item panel mb-0 grid-item--width1 grid-item--height1">
                <div class="panel-heading">
                  <h3 class="panel-title">Team Panel</h3>
                  <div class="panel-actions" style="right: 15px;">
                    <a class="panel-action" data-toggle="site-sidebar" href="javascript:void(0)" data-url="sidebar-widget.php">
                      <i class="icon md-settings" aria-hidden="true"></i>
                    </a>
                  </div>
                </div>
                <div class="panel-body">
                  <?php 
                  $_status = array("online","off");
                  for($aa=1;$aa<=13;$aa++){
                  ?>
                  <a href="javascript:void(0);" class="avatar avatar-sm avatar-<?=$_status[rand(0,1)]?> mr-10 mb-10">
                    <img src="../../../global/portraits/<?=rand(1,20)?>.jpg" alt="">
                    <i></i>
                  </a>
                  <?php }?>
                </div>
              </div>
              <!-- Box -->
              <!-- Box -->
              <div class="grid-item panel mb-0 grid-item--width1 grid-item--height1">
                <div class="panel-heading">
                  <h3 class="panel-title">Phone book</h3>
                  <div class="panel-actions" style="right: 15px;">
                    <a class="panel-action" data-toggle="site-sidebar" href="javascript:void(0)" data-url="sidebar-widget.php">
                      <i class="icon md-settings" aria-hidden="true"></i>
                    </a>
                  </div>
                </div>
                <div class="panel-body">
                  <input class="form-control" placeholder="input staff name..." value="" type="text">
                  <div class="mt-20">
                    <a href="javascript:void(0)" style="text-decoration:none;" class="list-group-item clearfix px-0">
                      <div class="media" style="flex-direction:initial;">
                        <div class="pr-10">
                          <div class="avatar avatar-online">
                            <img class="img-fluid" src="../../global/portraits/2.jpg" alt="...">
                          </div>
                        </div>
                        <div class="media-body">
                          <div class="mt-0 black"><strong>Edward Fletcher</strong></div>
                          <p data-info-type="phone" class="mb-0 text-nowrap">
                              <i class="icon fa-mobile mr-10"></i>
                              <span class="text-break">9192372533</span>
                            </p>
                        </div>
                        
                      </div>
                    </a>
                  </div>
                </div>
              </div>
              <!-- Box -->

              


          </div>



        </div>
      </div>


      <div class="row ml-0 mr-0" style="display: none;">
        <div class="col-lg-6 col-xl-4 ">
          <div class="card card-block bg-white" style="border:1px #e0e0e0 solid;">
              <h3 class="mt-0">Summary All My Task</h3>
                <div class="pt-20 pb-20">
                  <div class="pie-progress pie-progress" data-plugin="pieProgress" data-barcolor="#4caf50"
                  data-goal="72" aria-valuenow="72" data-size="100" data-barsize="8"
                  role="progressbar">
                    <div class="pie-progress-content">
                      <div class="pie-progress-number">72 %</div>
                      <div class="pie-progress-label">Progress</div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xl-4 ">    
            <div class="card card-block bg-white" style="border:1px #e0e0e0 solid;">
              <h3 class="mt-0">Tasks in Progress</h3>
              <div class="counter counter-md text-left">
                <div class="contextual-progress mb-0 mt-10">
                  <div class="clearfix">
                    <div class="progress-title">Order due</div>
                    <div class="progress-label">2 / 5</div>
                  </div>
                  <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                    <div class="progress-bar progress-bar-danger" aria-valuemin="0" aria-valuemax="100" style="width: 40%;" aria-valuenow="60" role="progressbar">
                    </div>
                  </div>
                </div>
                
              </div>
              <div class="counter counter-md text-left">
                <div class="contextual-progress mb-0 mt-10">
                  <div class="clearfix">
                    <div class="progress-title">Today</div>
                    <div class="progress-label">5 / 8</div>
                  </div>
                  <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                    <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 60%;" aria-valuenow="60" role="progressbar">
                    </div>
                  </div>
                </div>
                
              </div>
              <div class="counter counter-md text-left">
                <div class="contextual-progress mb-0 mt-10">
                  <div class="clearfix">
                    <div class="progress-title">This week</div>
                    <div class="progress-label">14 / 41</div>
                  </div>
                  <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                    <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 34%;" aria-valuenow="60" role="progressbar">
                    </div>
                  </div>
                </div>
                
              </div>
              <div class="counter counter-md text-left">
                <div class="contextual-progress mb-0 mt-10">
                  <div class="clearfix">
                    <div class="progress-title">This month</div>
                    <div class="progress-label">45 / 122</div>
                  </div>
                  <div class="progress" style="height:5px;" data-labeltype="steps" data-totalsteps="8" data-goal="80" data-plugin="progress">
                    <div class="progress-bar progress-bar-warning" aria-valuemin="0" aria-valuemax="100" style="width: 36%;" aria-valuenow="60" role="progressbar">
                    </div>
                  </div>
                </div>
                
              </div>
            
            </div>
          </div>
          <div class="col-lg-6 col-xl-4 ">

            <div class="card card-block bg-white" style="border:1px #e0e0e0 solid;">
              <h3 class="mt-0">Tasks Priority</h3>
              <div class="contextual-progress mb-0 mt-10">
                <div class="clearfix">
                  <div class="progress-title">Highest</div>
                  <div class="progress-label">60%</div>
                </div>
                <div class="progress" style="height:5px;" data-labeltype="percentage" data-goal="-40" data-plugin="progress">
                  <div class="progress-bar progress-bar-danger" aria-valuemin="-100" aria-valuemax="0" aria-valuenow="-40" role="progressbar" style="width: 60%;"></div>
                </div>
              </div>
              <div class="contextual-progress mb-0 mt-10">
                <div class="clearfix">
                  <div class="progress-title">High</div>
                  <div class="progress-label">70%</div>
                </div>
                <div class="progress" style="height:5px;" data-labeltype="percentage" data-goal="-40" data-plugin="progress">
                  <div class="progress-bar progress-bar-warning" aria-valuemin="-100" aria-valuemax="0" aria-valuenow="-40" role="progressbar" style="width: 70%;"></div>
                </div>
              </div>
              <div class="contextual-progress mb-0 mt-10">
                <div class="clearfix">
                  <div class="progress-title">Normal</div>
                  <div class="progress-label">80%</div>
                </div>
                <div class="progress" style="height:5px;" data-labeltype="percentage" data-goal="-40" data-plugin="progress">
                  <div class="progress-bar progress-bar-success" aria-valuemin="-100" aria-valuemax="0" aria-valuenow="-40" role="progressbar" style="width: 80%;"></div>
                </div>
              </div>
              <div class="contextual-progress mb-0 mt-10">
                <div class="clearfix">
                  <div class="progress-title">Low</div>
                  <div class="progress-label">50%</div>
                </div>
                <div class="progress" style="height:5px;" data-labeltype="percentage" data-goal="-40" data-plugin="progress">
                  <div class="progress-bar" aria-valuemin="-100" aria-valuemax="0" aria-valuenow="-40" role="progressbar" style="width: 50%;background-color:#757575;"></div>
                </div>
              </div>
            
            </div>


        </div>
        
      </div>
        
    </div>
  </div>
  <div class="site-action" data-plugin="actionBtn">
    <button type="button" onclick="location.href='index-form.php';" class="btn-raised btn btn-success btn-floating">
      <i class="front-icon md-plus" aria-hidden="true"></i>
    </button>
  </div>

  <div id="chartdiv" style="width: 100%; height: 400px;"></div>

  <!-- End Page -->
  <!-- Footer -->
  <?php include("../_footer.php");?>
  <script src="../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
  Breakpoints();
  </script>
  <!-- Core  -->
  <script src="../../global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="../../global/vendor/jquery/jquery.js"></script>
  <script src="../../global/vendor/tether/tether.js"></script>
  <script src="../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../global/vendor/animsition/animsition.js"></script>
  <script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="../../global/vendor/asscrollable/jquery-asScrollable.js"></script>
  <script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <script src="../../global/vendor/waves/waves.js"></script>
  <!-- Plugins -->
  <script src="../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../global/vendor/intro-js/intro.js"></script>
  <script src="../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="../../global/vendor/imagesloaded/imagesloaded.pkgd.js"></script>
  <script src="../../global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="../../global/vendor/aspieprogress/jquery-asPieProgress.js"></script>
  <script src="../../global/vendor/jquery-ui/jquery-ui.min.js"></script>
  <script src="../../global/vendor/packery/packery-docs.min.js"></script>
  <script src="../../global/vendor/chart-js/Chart.js"></script>
  <script src="../../global/vendor/aspieprogress/jquery-asPieProgress.js"></script>
  <!-- Scripts -->
  <script src="../../global/js/State.js"></script>
  <script src="../../global/js/Component.js"></script>
  <script src="../../global/js/Plugin.js"></script>
  <script src="../../global/js/Base.js"></script>
  <script src="../../global/js/Config.js"></script>
  <script src="../../assets/js/Section/Menubar.js"></script>
  <script src="../../assets/js/Section/GridMenu.js"></script>
  <script src="../../assets/js/Section/Sidebar.js"></script>
  <script src="../../assets/js/Section/PageAside.js"></script>
  <script src="../../assets/js/Plugin/menu.js"></script>
  <script src="../../global/js/config/colors.js"></script>
  <script src="../../assets/js/config/tour.js"></script>
  <script>
  Config.set('assets', '../../assets');
  </script>
  <script src="../../assets/js/Site.js"></script>
  <script src="../../global/js/Plugin/asscrollable.js"></script>
  <script src="../../global/js/Plugin/slidepanel.js"></script>
  <script src="../../global/js/Plugin/switchery.js"></script>
  <script src="../../global/js/Plugin/matchheight.js"></script>
  <script src="../../global/js/Plugin/masonry.js"></script>
  <script src="../../global/js/Plugin/bootstrap-select.js"></script>
  <script src="../../global/js/Plugin/aspieprogress.js"></script>
  <script src="../../assets/examples/js/charts/pie-progress.js"></script>
  
  <script src="../../global/vendor/amcharts/amcharts.js" type="text/javascript"></script>
  <script src="../../global/vendor/amcharts/serial.js" type="text/javascript"></script>

  <script src="chartjs.js"></script>
  <script>
    

  (function(document, window, $) {
  'use strict';
  var Site = window.Site;
  $(document).ready(function() {
    Site.run();

    var $grid = $('.grid').packery({
      itemSelector: '.grid-item',
      gutter: 30,
      percentPosition: true,
    });

    var $items = $grid.find('.grid-item').draggable();
    $grid.packery( 'bindUIDraggableEvents', $items );
    /*
    $grid.on( 'dragItemPositioned', function( event, laidOutItems ) {
        $grid.packery('layout');
    });
    */

    function chooseDateRange(start, end) {
          var DrangeValue = start.format('MM/DD/YYYY') + '-' + end.format('MM/DD/YYYY');
          $('#SearchDateRange span#daterange-value').html(DrangeValue);
          var dataKey = "123";
          var dataValue = "xxx";
          var stoken = '';
            $('.search-wrapper .currently-showing .chooseDateRange').remove();
            stoken = '<div class="token chooseDateRange" data-key="'+dataKey+'" data-value="' + dataValue + '"><span class="token-label" >' + DrangeValue + '</span><a  href="#" class="close" tabindex="-1">×</a></div>';
            $('.search-wrapper .currently-showing').append(stoken);
            sysGenUrlSearch();
      }

      $('#SearchDateRange').daterangepicker({
          "autoApply": true,
          "opens": "center",
          ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
             'Last 7 Days': [moment().subtract(6, 'days'), moment()],
             'Last 30 Days': [moment().subtract(29, 'days'), moment()],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
      }, chooseDateRange);


  });
  })(document, window, jQuery);
  </script>
  <script>
    var _annualsales;
    var chartData_annualsales = [
        {
            "year": 2009,
            "income": 23.5,
            "expenses": 22.1
        },
        {
            "year": 2010,
            "income": 26.2,
            "expenses": 22.8
        },
        {
            "year": 2011,
            "income": 23.5,
            "expenses": 24.1
        },
        {
            "year": 2012,
            "income": 29.5,
            "expenses": 25.1
        },
        {
            "year": 2013,
            "income": 23.5,
            "expenses": 25.1
        },
        {
            "year": 2014,
            "income": 26.2,
            "expenses": 25.8
        },
        {
            "year": 2015,
            "income": 30.1,
            "expenses": 27.9
        },
        {
            "year": 2016,
            "income": 29.5,
            "expenses": 28.1
        },
        {
            "year": 2017,
            "income": 30.6,
            "expenses": 28.2,
            "dashLengthLine": 5
        },
        {
            "year": 2018,
            "income": 34.1,
            "expenses": 29.9,
            "dashLengthColumn": 5,
            "alpha":0.2,
            "additional":"(forcash)"
        }

    ];


    AmCharts.ready(function () {
        // SERIAL CHART
        _annualsales = new AmCharts.AmSerialChart();

        _annualsales.dataProvider = chartData_annualsales;
        _annualsales.categoryField = "year";
        _annualsales.startDuration = 1;

        // AXES
        // category
        var categoryAxis_annualsales = _annualsales.categoryAxis;
        categoryAxis_annualsales.gridPosition = "start";

        // value
        var valueAxis_annualsales = new AmCharts.ValueAxis();
        valueAxis_annualsales.axisAlpha = 0;
        _annualsales.addValueAxis(valueAxis_annualsales);

        // GRAPHS
        // column graph
        var graph1_annualsales = new AmCharts.AmGraph();
        graph1_annualsales.type = "column";
        graph1_annualsales.title = "Sales";
        graph1_annualsales.lineColor = "#a668d5";
        graph1_annualsales.valueField = "income";
        graph1_annualsales.lineAlpha = 1;
        graph1_annualsales.fillAlphas = 1;
        graph1_annualsales.dashLengthField = "dashLengthColumn";
        graph1_annualsales.alphaField = "alpha";
        graph1_annualsales.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
        _annualsales.addGraph(graph1_annualsales);

        // line
        var graph2_annualsales = new AmCharts.AmGraph();
        graph2_annualsales.type = "line";
        graph2_annualsales.title = "Goal";
        graph2_annualsales.lineColor = "#fcd202";
        graph2_annualsales.valueField = "expenses";
        graph2_annualsales.lineThickness = 3;
        graph2_annualsales.bullet = "round";
        graph2_annualsales.bulletBorderThickness = 3;
        graph2_annualsales.bulletBorderColor = "#fcd202";
        graph2_annualsales.bulletBorderAlpha = 1;
        graph2_annualsales.bulletColor = "#ffffff";
        graph2_annualsales.dashLengthField = "dashLengthLine";
        graph2_annualsales.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
        _annualsales.addGraph(graph2_annualsales);

        // LEGEND
        var legend_annualsales = new AmCharts.AmLegend();
        legend_annualsales.useGraphSettings = true;
        _annualsales.addLegend(legend_annualsales);

        // WRITE
        _annualsales.write("chart_annualsales");
    });

    
    var _quarterlysalescom;
    var chartData_quarterlysalescom = [
        {
            "year": 2014,
            "q1": 23.5,
            "q2": 18.1,
            "q3": 20.3,
            "q4": 22.7
        },
        {
            "year": 2015,
            "q1": 26.2,
            "q2": 22.8,
            "q3": 20.3,
            "q4": 24.7
        },
        {
            "year": 2016,
            "q1": 30.1,
            "q2": 23.9,
            "q3": 24.3,
            "q4": 25.7
        },
        {
            "year": 2017,
            "q1": 29.5,
            "q2": 25.1,
            "q3": 25.3,
            "q4": 26.7
        }

    ];


    AmCharts.ready(function () {
        // SERIAL CHART
        _quarterlysalescom = new AmCharts.AmSerialChart();

        _quarterlysalescom.dataProvider = chartData_quarterlysalescom;
        _quarterlysalescom.categoryField = "year";
        _quarterlysalescom.startDuration = 1;

        // AXES
        // category
        var categoryAxis_quarterlysalescom = _quarterlysalescom.categoryAxis;
        categoryAxis_quarterlysalescom.gridPosition = "start";

        // value
        var valueAxis_quarterlysalescom = new AmCharts.ValueAxis();
        valueAxis_quarterlysalescom.axisAlpha = 0;
        _quarterlysalescom.addValueAxis(valueAxis_quarterlysalescom);

        // GRAPHS
        // column graph
        var graph1_quarterlysalescom = new AmCharts.AmGraph();
        graph1_quarterlysalescom.type = "column";
        graph1_quarterlysalescom.title = "Q1";
        graph1_quarterlysalescom.valueField = "q1";
        graph1_quarterlysalescom.lineAlpha = 1;
        graph1_quarterlysalescom.fillAlphas = 1;
        graph1_quarterlysalescom.dashLengthField = "dashLengthColumn";
        graph1_quarterlysalescom.alphaField = "alpha";
        graph1_quarterlysalescom.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
        _quarterlysalescom.addGraph(graph1_quarterlysalescom);

        var graph2_quarterlysalescom = new AmCharts.AmGraph();
        graph2_quarterlysalescom.type = "column";
        graph2_quarterlysalescom.title = "Q2";
        graph2_quarterlysalescom.valueField = "q2";
        graph2_quarterlysalescom.lineAlpha = 1;
        graph2_quarterlysalescom.fillAlphas = 1;
        graph2_quarterlysalescom.dashLengthField = "dashLengthColumn";
        graph2_quarterlysalescom.alphaField = "alpha";
        graph2_quarterlysalescom.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
        _quarterlysalescom.addGraph(graph2_quarterlysalescom);

        var graph3_quarterlysalescom = new AmCharts.AmGraph();
        graph3_quarterlysalescom.type = "column";
        graph3_quarterlysalescom.title = "Q3";
        graph3_quarterlysalescom.valueField = "q3";
        graph3_quarterlysalescom.lineAlpha = 1;
        graph3_quarterlysalescom.fillAlphas = 1;
        graph3_quarterlysalescom.dashLengthField = "dashLengthColumn";
        graph3_quarterlysalescom.alphaField = "alpha";
        graph3_quarterlysalescom.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
        _quarterlysalescom.addGraph(graph3_quarterlysalescom);

        var graph4_quarterlysalescom = new AmCharts.AmGraph();
        graph4_quarterlysalescom.type = "column";
        graph4_quarterlysalescom.title = "Q4";
        graph4_quarterlysalescom.valueField = "q4";
        graph4_quarterlysalescom.lineAlpha = 1;
        graph4_quarterlysalescom.fillAlphas = 1;
        graph4_quarterlysalescom.dashLengthField = "dashLengthColumn";
        graph4_quarterlysalescom.alphaField = "alpha";
        graph4_quarterlysalescom.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
        _quarterlysalescom.addGraph(graph4_quarterlysalescom);

       

        // LEGEND
        var legend_quarterlysalescom = new AmCharts.AmLegend();
        legend_quarterlysalescom.useGraphSettings = true;
        _quarterlysalescom.addLegend(legend_quarterlysalescom);

        // WRITE
        _quarterlysalescom.write("chart_quarterlysalescom");
    });

    var _comex;
    var chartData_comex = [
        {
            "month": "Jan",
            "income": 23.5
        },
        {
            "month": "Feb",
            "income": 21.2
        },
        {
            "month": "Mar",
            "income": 25.7
        },
        {
            "month": "Apr",
            "income": 24.1
        },
        {
            "month": "May",
            "income": 23.8
        },
        {
            "month": "Jun",
            "income": 21.9
        },
        {
            "month": "Jul",
            "income": 28.2
        },
        {
            "month": "Aug",
            "income": 27.2
        },
        {
            "month": "Sep",
            "income": 25.2
        },
        {
            "month": "Oct",
            "income": 26.3
        },
        {
            "month": "Nov",
            "income": 25.7
        },
        {
            "month": "Dec",
            "income": 24.8
        },

    ];


    AmCharts.ready(function () {
        // SERIAL CHART
        _comex = new AmCharts.AmSerialChart();

        _comex.dataProvider = chartData_comex;
        _comex.categoryField = "month";
        _comex.startDuration = 1;

        // AXES
        // category
        var categoryAxis_comex = _comex.categoryAxis;
        categoryAxis_comex.gridPosition = "start";

        // value
        var valueAxis_comex = new AmCharts.ValueAxis();
        valueAxis_comex.axisAlpha = 0;
        _comex.addValueAxis(valueAxis_comex);

        // GRAPHS
        // column graph
        var graph1_comex = new AmCharts.AmGraph();
        graph1_comex.type = "column";
        graph1_comex.title = "Expenses";
        graph1_comex.valueField = "income";
        graph1_comex.lineAlpha = 1;
        graph1_comex.fillAlphas = 1;
        graph1_comex.dashLengthField = "dashLengthColumn";
        graph1_comex.alphaField = "alpha";
        graph1_comex.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
        _comex.addGraph(graph1_comex);

        // LEGEND
        var legend_comex = new AmCharts.AmLegend();
        legend_comex.useGraphSettings = true;
        _comex.addLegend(legend_comex);

        // WRITE
        _comex.write("chart_comex");
    });

    
    var _eachb;
    var chartData_eachb = [
        {
            "year": 2014,
            "p1": 21.2,
            "p2": 11.2,
            "p3": 8.2,
            "p4": 15.2
        },
        {
            "year": 2015,
            "p1": 23.3,
            "p2": 14.2,
            "p3": 10.2,
            "p4": 12.2
        },
        {
            "year": 2016,
            "p1": 25.7,
            "p2": 14.8,
            "p3": 9.7,
            "p4": 11.9
        },
        {
            "year": 2017,
            "p1": 25.1,
            "p2": 11.7,
            "p3": 11.4,
            "p4": 12.1
        }
    ];


    AmCharts.ready(function () {
        // SERIAL CHART
        _eachb = new AmCharts.AmSerialChart();
        _eachb.dataProvider = chartData_eachb;
        _eachb.categoryField = "year";
        _eachb.plotAreaBorderAlpha = 0.2;

        // AXES
        // category
        var categoryAxis_eachb = _eachb.categoryAxis;
        categoryAxis_eachb.gridAlpha = 0.1;
        categoryAxis_eachb.axisAlpha = 0;
        categoryAxis_eachb.gridPosition = "start";

        // value
        var valueAxis_eachb = new AmCharts.ValueAxis();
        valueAxis_eachb.stackType = "regular";
        valueAxis_eachb.gridAlpha = 0.1;
        valueAxis_eachb.axisAlpha = 0;
        _eachb.addValueAxis(valueAxis_eachb);

        // GRAPHS
        // column graph
        var graph1_eachb = new AmCharts.AmGraph();
        graph1_eachb.type = "column";
        graph1_eachb.title = "Band 1";
        graph1_eachb.valueField = "p1";
        graph1_eachb.lineAlpha = 1;
        graph1_eachb.fillAlphas = 1;
        graph1_eachb.dashLengthField = "dashLengthColumn";
        graph1_eachb.alphaField = "alpha";
        graph1_eachb.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
        _eachb.addGraph(graph1_eachb);

        graph1_eachb = new AmCharts.AmGraph();
        graph1_eachb.type = "column";
        graph1_eachb.title = "Band 2";
        graph1_eachb.valueField = "p2";
        graph1_eachb.lineAlpha = 1;
        graph1_eachb.fillAlphas = 1;
        graph1_eachb.dashLengthField = "dashLengthColumn";
        graph1_eachb.alphaField = "alpha";
        graph1_eachb.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
        _eachb.addGraph(graph1_eachb);

        graph1_eachb = new AmCharts.AmGraph();
        graph1_eachb.type = "column";
        graph1_eachb.title = "Band 3";
        graph1_eachb.valueField = "p3";
        graph1_eachb.lineAlpha = 1;
        graph1_eachb.fillAlphas = 1;
        graph1_eachb.dashLengthField = "dashLengthColumn";
        graph1_eachb.alphaField = "alpha";
        graph1_eachb.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
        _eachb.addGraph(graph1_eachb);

        graph1_eachb = new AmCharts.AmGraph();
        graph1_eachb.type = "column";
        graph1_eachb.title = "Band 4";
        graph1_eachb.valueField = "p4";
        graph1_eachb.lineAlpha = 1;
        graph1_eachb.fillAlphas = 1;
        graph1_eachb.dashLengthField = "dashLengthColumn";
        graph1_eachb.alphaField = "alpha";
        graph1_eachb.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
        _eachb.addGraph(graph1_eachb);

        // LEGEND
        var legend_eachb = new AmCharts.AmLegend();
        legend_eachb.useGraphSettings = true;
        _eachb.addLegend(legend_eachb);

        // WRITE
        _eachb.write("chart_eachb");
    });

    var _eachb;
    var chartData_eachb1 = [
        {
            "month": "Jan",
            "p1": 31.2,
            "p2": 11.2,
            "p3": 8.2,
            "p4": 15.2
        },
        {
            "month": "Feb",
            "p1": 31.2,
            "p2": 11.2,
            "p3": 8.2,
            "p4": 15.2
        },
        {
            "month": "Mar",
            "p1": 31.2,
            "p2": 11.2,
            "p3": 8.2,
            "p4": 15.2
        },
        {
            "month": "Apr",
            "p1": 26.3,
            "p2": 14.2,
            "p3": 10.2,
            "p4": 12.2
        },
        {
            "month": "May",
            "p1": 23.3,
            "p2": 14.2,
            "p3": 10.2,
            "p4": 12.2
        },
        {
            "month": "Jun",
            "p1": 23.3,
            "p2": 14.2,
            "p3": 10.2,
            "p4": 12.2
        },
        {
            "month": "Jul",
            "p1": 25.7,
            "p2": 14.8,
            "p3": 9.7,
            "p4": 11.9
        },
        {
            "month": "Aug",
            "p1": 25.7,
            "p2": 14.8,
            "p3": 9.7,
            "p4": 11.9
        },
        {
            "month": "Sep",
            "p1": 25.7,
            "p2": 14.8,
            "p3": 9.7,
            "p4": 11.9
        },
        {
            "month": "Oct",
            "p1": 35.3,
            "p2": 14.2,
            "p3": 10.2,
            "p4": 12.2
        },
        {
            "month": "Nov",
            "p1": 35.3,
            "p2": 14.2,
            "p3": 10.2,
            "p4": 12.2
        },
        {
            "month": "Dec",
            "p1": 35.3,
            "p2": 14.2,
            "p3": 10.2,
            "p4": 12.2
        },
    ];


    AmCharts.ready(function () {
        // SERIAL CHART
        _eachb1 = new AmCharts.AmSerialChart();
        _eachb1.dataProvider = chartData_eachb1;
        _eachb1.categoryField = "month";
        _eachb1.plotAreaBorderAlpha = 0.2;

        // AXES
        // category
        var categoryAxis_eachb1 = _eachb1.categoryAxis;
        categoryAxis_eachb1.gridAlpha = 0.1;
        categoryAxis_eachb1.axisAlpha = 0;
        categoryAxis_eachb1.gridPosition = "start";

        // value
        var valueAxis_eachb1 = new AmCharts.ValueAxis();
        valueAxis_eachb1.stackType = "regular";
        valueAxis_eachb1.gridAlpha = 0.1;
        valueAxis_eachb1.axisAlpha = 0;
        _eachb1.addValueAxis(valueAxis_eachb1);

        // GRAPHS
        // column graph
        var graph1_eachb1 = new AmCharts.AmGraph();
        graph1_eachb1.type = "column";
        graph1_eachb1.title = "Band 1";
        graph1_eachb1.valueField = "p1";
        graph1_eachb1.lineAlpha = 1;
        graph1_eachb1.fillAlphas = 1;
        graph1_eachb1.dashLengthField = "dashLengthColumn";
        graph1_eachb1.alphaField = "alpha";
        graph1_eachb1.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
        _eachb1.addGraph(graph1_eachb1);

        graph1_eachb1 = new AmCharts.AmGraph();
        graph1_eachb1.type = "column";
        graph1_eachb1.title = "Band 2";
        graph1_eachb1.valueField = "p2";
        graph1_eachb1.lineAlpha = 1;
        graph1_eachb1.fillAlphas = 1;
        graph1_eachb1.dashLengthField = "dashLengthColumn";
        graph1_eachb1.alphaField = "alpha";
        graph1_eachb1.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
        _eachb1.addGraph(graph1_eachb1);

        graph1_eachb1 = new AmCharts.AmGraph();
        graph1_eachb1.type = "column";
        graph1_eachb1.title = "Band 3";
        graph1_eachb1.valueField = "p3";
        graph1_eachb1.lineAlpha = 1;
        graph1_eachb1.fillAlphas = 1;
        graph1_eachb1.dashLengthField = "dashLengthColumn";
        graph1_eachb1.alphaField = "alpha";
        graph1_eachb1.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
        _eachb1.addGraph(graph1_eachb1);

        graph1_eachb1 = new AmCharts.AmGraph();
        graph1_eachb1.type = "column";
        graph1_eachb1.title = "Band 4";
        graph1_eachb1.valueField = "p4";
        graph1_eachb1.lineAlpha = 1;
        graph1_eachb1.fillAlphas = 1;
        graph1_eachb1.dashLengthField = "dashLengthColumn";
        graph1_eachb1.alphaField = "alpha";
        graph1_eachb1.balloonText = "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span>";
        _eachb1.addGraph(graph1_eachb1);

        // LEGEND
        var legend_eachb1 = new AmCharts.AmLegend();
        legend_eachb1.useGraphSettings = true;
        _eachb1.addLegend(legend_eachb1);

        // WRITE
        _eachb1.write("chart_eachb1");
    });
</script>
  
<?php include("../_footer-form.php");?>
</body>
</html>