<section>
  <div class="row p-20 bg-grey-600">
    <div class="col-md-10" style="padding-top: 3px;">
      <h5 class="p-0 m-0 white"><i class="icon md-widgets" aria-hidden="true"></i> Intranet Feed</h5>
    </div>
    <div class="col-md-2 text-right ">
      <button type="button" class="btn btn-pure btn-inverse font-size-20 py-0 slidePanel-close md-close" aria-hidden="true"></button>
    </div>
  </div>
</section>
<div class="site-sidebar-tab-content tab-content">
  <div class="tab-pane active" >
    <div>
      <div>
        <div class="form-group form-material pt-10">
          <section>
            <label class="form-control-label">On/Off Display thumbnail: </label>
            <div class="pb-10 pl-10">
              <input type="checkbox" name="nnn2" id="nnn2" checked="" data-plugin="switchery" data-size="small" />  <span class="pl-10">Display thumbnail</span>
            </div>

            <label class="form-control-label mt-10">On/Off Modules to feed data: </label>
            <div class="pb-10 pl-10">
              <input type="checkbox" name="nnn3" id="nnn3" checked="" data-plugin="switchery" data-size="small" />  <span class="pl-10">CMS</span>
            </div>
            <div class="pb-10 pl-10">
              <input type="checkbox" name="nnn3" id="nnn31" checked="" data-plugin="switchery" data-size="small" />  <span class="pl-10">Announcement</span>
            </div>
            <div class="pb-10 pl-10">
              <input type="checkbox" name="nnn3" id="nnn32" checked="" data-plugin="switchery" data-size="small" />  <span class="pl-10">Photo</span>
            </div>
            <div class="pb-10 pl-10">
              <input type="checkbox" name="nnn3" id="nnn33" checked="" data-plugin="switchery" data-size="small" />  <span class="pl-10">Video</span>
            </div>
            <div class="pb-10 pl-10">
              <input type="checkbox" name="nnn3" id="nnn34" checked="" data-plugin="switchery" data-size="small" />  <span class="pl-10">Event</span>
            </div>
          </section>

          <label class="form-control-label mt-10">Setting width: </label>
          <div >
            <ul class="list-unstyled example mt-10 pl-10">
              <li class="mb-15">
                <input type="radio" class="icheckbox-primary" id="inputRadiosUnchecked11" name="inputRadios11"
                data-plugin="iCheck" data-radio-class="iradio_flat-blue" />
                <label class="ml-5" for="inputRadiosUnchecked11">width 25%</label>
              </li>
              <li class="mb-15">
                <input type="radio" class="icheckbox-primary" checked="" id="inputRadiosChecked12" name="inputRadios11"
                data-plugin="iCheck" data-radio-class="iradio_flat-blue"/>
                <label class="ml-5" for="inputRadiosChecked12">width 33%</label>
              </li>
              <li class="mb-15">
                <input type="radio" class="icheckbox-primary" id="inputRadiosChecked13" name="inputRadios11"
                data-plugin="iCheck" data-radio-class="iradio_flat-blue"/>
                <label class="ml-5" for="inputRadiosChecked13">width 50%</label>
              </li>
              <li class="mb-15">
                <input type="radio" class="icheckbox-primary" id="inputRadiosChecked14" name="inputRadios11"
                data-plugin="iCheck" data-radio-class="iradio_flat-blue"/>
                <label class="ml-5" for="inputRadiosChecked14">width 75%</label>
              </li>
              
            </ul>
          </div>
          
          
          <label class="form-control-label">On/Off Auto Height: </label>
          <div class="pb-10 pl-10">
            <input type="checkbox" name="nnn1" id="nnn1" data-plugin="switchery" data-size="small" />  <span class="pl-10">Auto Height</span>
          </div>
          <label class="form-control-label">Setting Height: </label>
          <div >
            <ul class="list-unstyled example mt-10 pl-10">
              <li class="mb-15">
                <input type="radio" class="icheckbox-primary" checked="" id="inputRadiosUnchecked21" name="inputRadios21"
                data-plugin="iCheck" data-radio-class="iradio_flat-blue" />
                <label class="ml-5" for="inputRadiosUnchecked21">min-height 220px</label>
              </li>
              <li class="mb-15">
                <input type="radio" class="icheckbox-primary" id="inputRadiosChecked22" name="inputRadios21"
                data-plugin="iCheck" data-radio-class="iradio_flat-blue"/>
                <label class="ml-5" for="inputRadiosChecked22">min-height 470px</label>
              </li>
              
            </ul>
          </div>
          
        </div>

      </div>
    </div>
  </div>
  <div class="px-20 py-10 bg-grey-200 text-center">
    <button type="button" class="btn btn-block btn-danger waves-effect waves-classic">Remove widget</button>
  </div>
</div>